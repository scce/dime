/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.preview;

import java.io.File;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

public class EditorCompound {
	
	private IEditorPart previewEditor;
	
	public EditorCompound()
	{
		this.previewEditor = null;
	}
	
	public void openPreviewEditor(File f, IWorkbenchPage page)
	{
		try {
			IFileStore file = EFS.getLocalFileSystem().getStore(f.toURI());
			if(previewEditor == null){
				this.previewEditor = IDE.openEditorOnFileStore(page, file);		
			}else{
				previewEditor.setFocus();
			}
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}
}
