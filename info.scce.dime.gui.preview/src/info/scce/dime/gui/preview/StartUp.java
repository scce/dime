/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.preview;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

public class StartUp implements IStartup{

	@Override
	public void earlyStartup() {
		System.out.println("GUI Preview start up");
		
		info.scce.dime.gui.preview.ResourceChangeListener guiRCL = new info.scce.dime.gui.preview.ResourceChangeListener();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(
				guiRCL);
		
		UIJob job = new UIJob("reloading GUI Preview") {


			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				PartListner pl = new PartListner();
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.addPartListener(pl);
				return Status.OK_STATUS;
			}
		};
		job.schedule();
		
		
	}

}
