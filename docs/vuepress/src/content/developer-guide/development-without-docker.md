# App Development (without Docker)

TODO: Properly update documentation
* dart-sdk/bin must be on $PATH
* build latest archetype (then app_init?)
* maven-edu credentials required in .m2/settings.xml
 

## Java JDK
Download the latest stable JDK for Java 8 as an archive from the [Oracle website](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).
On Macs we have had good experiences with the [AdoptOpenJDK](https://adoptopenjdk.net).
* Extract the content into you `$HOME/opt/` folder
* The extracted folder will be named "jdk" ending in the version
* Set your environment variable "${JAVA_HOME}" to the extracted folder
 * See [this Thread][environmentVarLink] for information on setting environment variables.
 * Use `echo ${JAVA_HOME}` in a terminal to check the current value
* Add `${JAVA_HOME}` and `${JAVA_HOME}/bin` to your $PATH
 * See [this Thread](https://stackoverflow.com/questions/14637979/how-to-permanently-set-path-on-linux-unix) for information on changing the $PATH variable
 * Use `which java` to ensure that the java command points to your `${JAVA_HOME}/bin/java`

## Maven
Download the latest stable version of Maven from the [Apache website](https://maven.apache.org/download.cgi).
* Extract the content into you `$HOME/opt/` folder
* The extracted folder will be named "apache-maven-" ending in the version
* Set your environment variable "${MAVEN_HOME}" to the extracted folder
 * See [this Thread][environmentVarLink] for information on setting environment variables.
 * Use `echo ${MAVEN_HOME}` in a terminal to check the current value

## Dart SDK
Download version 2.10.x of Dart SDK from the [Dart website](https://www.dartlang.org/tools/sdk/archive).
* Extract the content into you `$HOME/opt/` folder
* The extracted folder will be named "dart-sdk"
* Set your environment variable "${DART_HOME}" to the extracted folder
 * See [this Thread][environmentVarLink] for information on setting environment variables.
 * Use `echo ${DART_HOME}` in a terminal to check the current value

## GIT
Install the latest stable version of GIT from the [GIT website](https://git-scm.com/downloads), following their instructions based on your operating system.

## Wildfly
**Please note** that some steps in this guide require you to have the following installed software:
* Java JDK 1.8 (any version of 1.8)
* Maven 3.3.3 or higher

### Wildfly Installation

* Download the version 14.0.1.Final of Wildfly as an archive from the [Wildfly website](http://wildfly.org/downloads/)
* Extract the content into you `$HOME/opt/` folder
* The extracted folder will be named "wildfly-14.0.1.Final"
* Set your environment variables "${WILDFLY_HOME}" and "${JBOSS_HOME}" to the extracted folder
 * See [this Thread][environmentVarLink] for information on setting environment variables.
 * Use `echo ${WILDFLY_HOME}` or respectively `echo ${JBOSS_HOME}` in a terminal to check the current value
* The extracted folder "wildfly-14.0.1.Final" will be refered to as your `${WILDFLY_HOME}` in this guide

### Wildfly Configuration

#### Configuring native DB scheme (if used)

If you want to use the native DB scheme in an embedded context (i.e. with H2 as RDBMS) you need to make sure to use a recent version of H2 (e.g. 1.4.199).

Wildfly 14 currently only ships version 1.4.193. In order to update your Wildfly installation, download the corresponding JAR of H2 (e.g. form [mvn central](https://mvnrepository.com/artifact/com.h2database/h2/1.4.199)) and copy it into the modules folder of your Wildfly, i.e. `$WFLY_HOME/modules/system/layers/base/com/h2database/h2/main`. Make sure to update the corresponding `module.xml`in the very same folder, too.

#### Configuring PostgresSQL (if used)
By default, Wildfly does not come with a PostgreSQL module, therefore we have to configure it ourselves.

* Download the newest version of the JDBC jar from the [PostgreSQL website](https://jdbc.postgresql.org/download.html), the newest version from the "JDBC 4.2" column - currently "42.2.2 JDBC 42" - should be fine
* Create the directory `${WILDFLY_HOME}/modules/system/layers/base/org/postgresql/main/`
 * Move the downloaded jar file into the new directory
* Create the file `${WILDFLY_HOME}/modules/system/layers/base/org/postgresql/main/module.xml` with following content:

```
<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.0" name="org.postgresql">
        <resources>
                <resource-root path="postgresql-42.2.2.jar"/>
        </resources>
        <dependencies>
                <module name="javax.api"/>
                <module name="javax.transaction.api"/>
        </dependencies>
</module>
```
**Ensure, that the specified path for `<resource-root path="..."/>` matches the actual downloaded file name!**

## Configure CINCO
A Maven integration for eclipse is needed:  
* Go to "_Help_" and then to "_Install New Software..._". 
* In the new window fill in the text field "_Work with:_" with https://download.eclipse.org/releases/photon/ and filter by "_m2e_".
* Select "_m2e - Maven Integration for eclipse_" and click next. Follow this dialog.


# Run a DIME App

## Generate the App

* After a project has been modeled, open your *.dad* file and hit the `G` button. This generates the entire app. You can find all generated sources in the `target` folder of your project

##  Configure your run configurations and run the Backend

If you are working on a shared workstation and thus need to change port numbers, follow [the instructions from below](https://gitlab.com/scce/dime/wikis/DIME-2-development-with-maven#change-ports-for-shared-workstations) now.

* Start DyWA with launch config: Right-Click the _DyWA_Start.launch_ file in the Project-Explorer and select _Run As_ -> _DyWA_start_ (Make sure that the run configurations contain the correct paths to your wildfly folder on your device: _Run As_ -> _Run configurations_  -> Maven -> _Dywa_start_: Edit the values in the lower table  -> _Apply_ -> _Run_)
* Deploy App with launch config:  Right-Click the _App_Deploy.launch_ file in the Project-Explorer and select _Run As_ -> _App Deploy_ (Make sure that the run configurations contain the correct paths to your wildfly and dart folder on your device: _Run As_ -> _Run configurations_  -> Maven -> _App Deploy_: Edit the values in the lower table  -> _Apply_ -> _Run_)
* While Editing the run configurations the error "Base directory doesn't exist or can't be read" may occur. In this case you can change the base directory by traversing the workspace to **dywa-config**(Dywa_start) or **dywa_app** (App_Deploy) via the _Workspace..._-Button.  _Apply_ -> _Run_

##  Run the Frontend

* open a shell and goto `target/webapp`
* run `pub get`
* run `pub global activate webdev 2.6.2` and copy the path of your local webdev installation
* run `path/to/webdev serve web:9090 --hostname=127.0.0.1`.
* open http://127.0.0.1:9090

If you are using **Firefox**, you need to start webdev with `--release`. See #333

# Change Ports for Shared Workstations

When developing on a shared workstation (like ls5metalab), all ports used by Wildfly and frontend need to be made unique. This requires some initial changes to the project's wilfly xml configs and POMs, as well as individual changes made by every user themselves.

## Prepare Project

In `dywa-config/wildfly-starter/pom.xml`, add the following lines into the `java-opts` section of **all wildfly-maven-plugin occurrences**

```
<java-opt>-Djboss.socket.binding.port-offset=${env.DIME_BACKEND_PORT_OFFSET}</java-opt>
<java-opt>-Ddime.frontend.port=${env.DIME_FRONTEND_PORT}</java-opt>
```

Also in `dywa-config/wildfly-starter/pom.xml`, add the following line under the `jboss-home` tag of **all wildfly-maven-plugin occurrences** 
```
<port>${env.DIME_BACKEND_MGMT_PORT}</port>
```

Also in `dywa-config/wildfly-starter/pom.xml`, add the following lines into the `<profiles>` section:

```
<profile>
	<id>dime_backend_mgmt_port default value</id>
	<activation>
		<activeByDefault>true</activeByDefault>
		<property>
			<name>!env.DIME_BACKEND_MGMT_PORT</name>
		</property>
	</activation>
	<properties>
		<env.DIME_BACKEND_MGMT_PORT>9990</env.DIME_BACKEND_MGMT_PORT>
	</properties>
</profile>

<profile>
	<id>dime_backend_port_offset default value</id>
	<activation>
		<activeByDefault>true</activeByDefault>
		<property>
			<name>!env.DIME_BACKEND_PORT_OFFSET</name>
		</property>
	</activation>
	<properties>
		<env.DIME_BACKEND_PORT_OFFSET>0</env.DIME_BACKEND_PORT_OFFSET>
	</properties>
</profile>

<profile>
	<id>dime_frontend_port default value</id>
	<activation>
		<activeByDefault>true</activeByDefault>
		<property>
			<name>!env.DIME_FRONTEND_PORT</name>
		</property>
	</activation>
	<properties>
		<env.DIME_FRONTEND_PORT>9090</env.DIME_FRONTEND_PORT>
	</properties>
</profile>
```

In dywa-config/standalone.xml **and** dywa-config/standalone-embedded.xml, update the `Access-Control-Allow-Origin` header into:

```
header-value="http://127.0.0.1:${dime.frontend.port:9090}"
```

## Apply to Your Installation

You need to reserve a unique port offset for yourself. Let's assume for this example, it is 42.

Edit your shell's config file (e.g. ~/.bashrc or ~/.zshrc) and add the following lines (first one modified according to your reserved offset, of course):

```
export DIME_PORT_OFFSET=42
export DIME_BACKEND_PORT_OFFSET=$DIME_PORT_OFFSET
export DIME_BACKEND_MGMT_PORT=$((9990 + $DIME_PORT_OFFSET))
export DIME_FRONTEND_PORT=$((9090 + $DIME_PORT_OFFSET))
```
Remember to restart DIME in a new shell, so that these values are set. `env | grep DIME` should yield something like this:
```
user@host:~$ env | grep DIME
DIME_PORT_OFFSET=42
DIME_BACKEND_PORT_OFFSET=42
DIME_BACKEND_MGMT_PORT=10032
DIME_FRONTEND_PORT=9132
```

**After DAD generation**, update two of the generated files: 

* target/webapp/lib/src/core/dime_process_service.dart, so that getBaseUrl points to 8122 (=8080+42) instead of 8080

You can place the following shell script into your project root directory to semi-automate the task.

```shell
#!/bin/sh

if [ -e $DIME_BACKEND_PORT_OFFSET ]; then
	echo "DIME_BACKEND_PORT_OFFSET environment variable not set"
	exit 1
fi

if [ -e $DIME_FRONTEND_PORT ]; then
	echo "DIME_FRONTEND_PORT environment variable not set"
	exit 1
fi

BACKEND_PORT=$((8080 + $DIME_BACKEND_PORT_OFFSET))

echo Setting backend port to $BACKEND_PORT in target/webapp/lib/src/core/dime_process_service.dart
sed -i "s,8080,$BACKEND_PORT," target/webapp/lib/src/core/dime_process_service.dart
```

## Validate native scheme is running
To validate that a deployed app is using the native database scheme the following steps can be followed:
1.  Make sure the H2 module is updated within Wildfly as stated [above](https://gitlab.com/scce/dime/wikis/DIME-2-development-with-maven#configuring-native-db-scheme-if-used).
2.  The deployed app has to have the parameter `dime.native=true` present in its `App_Deploy.launch` run configuration.
3.  Run the H2 console on the machine the app is deployed on. To start the console use the following command line: `java -cp /path/to/your/wildfly/wildfly-14.0.1.Final/modules/system/layers/base/com/h2database/h2/main/h2-1.4.199.jar org.h2.tools.Console -web -browser`.
4.  Connect to your running H2 module using the JDBC URL `jdbc:h2:file:/path/to/your/wildfly/wildfly-14.0.1.Final/standalone/data/dywa;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;AUTO_SERVER=TRUE;MVCC=TRUE`. Password and username should be `sa`.
5.  Make sure the connection could be established successfully and multiple relevant tables are present within the inspector. If this is the case you are running your app using the native db scheme.

[environmentVarLink]:https://unix.stackexchange.com/questions/117467/how-to-permanently-set-environmental-variables
