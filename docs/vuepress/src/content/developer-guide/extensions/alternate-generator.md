# Alternate Generator

This guide describes a best-practice approach to create and apply your own generator to a modeled DIME app without changing the DIME source code.

## Approach

* Extend DIME by means of an additional model type
* Write a generator and register it for the new model type

## Requirements

Your extension code is supposed to be written in DIME.
Using the DIME installer is recommended.

## Example Project

The approach is best shown with an example project.
Head over to the [Mobile Application Generator](https://gitlab.com/scce/dime-example-extensions/mad-generator) project and follow the documentation there.
