# Generic SIBs

This guide describes a best-practice approach to create your own types of SIBs without changing the DIME source code.

## Approach

The Process models in DIME support the creation of a `GenericSIB` that references an arbitrary `EObject`.

In order to tell the DIME app generator what to do with such a SIB you need to provide one of the following alternatives.
* A **model transformation** that replaces the generic SIB with something the app generator can handle
* A **code generator** that generates executable code right into the Java file that is generated for the Process model

The support for modeling, model transformation and code generation is defined by an implementation of the interface `info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter` that is registered via the extension point `info.scce.dime.modeltrafo`.

## Requirements

Your extension code is supposed to be written in DIME.
Using the DIME installer is recommended.

## Example Projects

The approach is best shown with example projects.
Head over to the example projects and follow the documentation there.

For each example, two separate projects are provided.
* The **Extension Project** comprises the code implemented against the `IModeltrafoSupporter` interface and its registration for the `info.scce.dime.modeltrafo` extension point.
* The **App Project** shows how to use the respective generic SIBs at runtime.

### ObjectLang-SIBs

This example illustrates how to create a SIB that references an `ObjectLang` model that already is available in DIME.

At generation time, from such an ObjectLang-SIB a Process model is generated and the `GenericSIB` is replaced by a `ProcessSIB` that references this generated Process model.

* Extension Project: [generic-objectlang-sibs](https://gitlab.com/scce/dime-example-extensions/generic-objectlang-sibs)
* App Project: [generic-objectlang-sibs-app](https://gitlab.com/scce/dime-example-apps/generic-objectlang-sibs-app)

### Data-GUI-SIBs

This example illustrates how to create a SIB that references a `Data` model that already is available in DIME.

At generation time, from such a Data-SIB a GUI model is generated and the `GenericSIB` is replaced by a `GUISIB` that references this generated GUI model.

* Extension Project: [generic-datagui-sibs](https://gitlab.com/scce/dime-example-extensions/generic-datagui-sibs)
* App Project: [generic-datagui-sibs-app](https://gitlab.com/scce/dime-example-apps/generic-datagui-sibs-app)

### Math-SIBs

This example illustrates how to create a SIB that references a `Math` model, which is a new type of graph model that is not yet available in DIME.

At generation time, from such a Math-SIB executable code is generated right into the Java file that is generated for the containing Process model.

* Extension Project: [generic-math-sibs](https://gitlab.com/scce/dime-example-extensions/generic-math-sibs)
* App Project: [generic-math-sibs-app](https://gitlab.com/scce/dime-example-apps/generic-math-sibs-app)


