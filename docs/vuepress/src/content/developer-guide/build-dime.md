# Build DIME

This guide describes how to build a standalone DIME application (referred to as **DIME Installer**).


## Prerequisites

This guide assumes you have the following things installed and working on your system:

- Java 11
- Maven >3.8.1


## Step 1: Clone Repository

Clone the DIME repository to a **new folder that is dedicated to building the installer**.
To keep things simple, try not to mix up these sources with your own developments.
In this example, the DIME repository is cloned to folder `dime-ci`.

```
git clone git@gitlab.com:scce/dime.git dime-ci
cd dime-ci
```

## Step 2: Install Libraries

Navigate to your local DIME repository folder `dime-ci` and install the libraries bundle.

```
mvn install -f info.scce.dime.libraries
```

## Step 3: Build and Install Cinco

Building a DIME installer requires a specific version of Cinco that is known to work with the respective DIME version.
This Cinco version needs to be used for both generating the DIME sources and building DIME with Maven.

**Note: You can skip this step if you want to build DIME against the HEAD of Cinco's 'master' branch. In this case, download the latest Cinco nightly build and use it for generating DIME.**

The correct Cinco version is managed via a Git submodule `cinco` in the DIME repository.
Navigate to your local DIME repository folder `dime-ci` and init the submodule.

```
git submodule init cinco
git submodule update cinco
```

Navigate to the `cinco` subfolder and install Cinco.

```
cd cinco
mvn package -f de.jabc.cinco.meta.libraries
mvn install
```

The built version of Cinco can be found in `de.jabc.cinco.meta.product/target/products/`.
Make sure to use exactly this version for the following generation step.

## Step 4: Generate DIME

Run the Cinco version that has been built in step 4.

**CAUTION: For this guide, do NOT follow the best practice in terms of separating repository folder from the actual workspace.
Instead, select the DIME repository folder `dime-ci` directly as your workspace folder when starting Cinco.**

Import the DIME projects: From the menu choose 'File > Import... > Existing Projects into Workspace' and specify the DIME repository folder `dime-ci` as root folder. Select all projects **except** the ones that start with `de.jabc.cinco`.

Execute the necessary tasks to generate DIME as a Cinco product, which essentially means:

* Wait for updating projects and building workspace to finish (do this again after every following step)
* generate the model code for `GUIPlugin.genmodel` and `SIBLibrary.genmodel`
* run the MWE2 workflows for `GenerateGUIPlugin.mwe2`, `GenerateSIBLibrary.mwe2` and `GenerateObjectLang.mwe2`
* run `Generate Cinco Product` for DIME.cpd
* run 'Project > Clean... > Clean all projects'
* make sure there are no errors in the workspace (clean all projects, close and re-open base project)

If you experience problems take care that your Java version and compiler compliance levels are correctly set.

## Step 5: Build Installer

Switch back to the terminal and navigate to your local DIME repository folder `dime-ci`, which now additionally contains all generated sources.

The last step is to get the updated installer resources and build DIME with Maven.

```
git checkout origin/installer-resources-cinco2 dime-package
./dime-package
```

If the build has been successful, you can find the standalone DIME in `info.scce.dime.product/target/products/`.

## Step 6: Deploy to Download Server

If you are allowed to deploy installers to the [ls5download server](https://ls5download.cs.tu-dortmund.de/dime/daily/) (i.e. your ssh key is authorized with cincodaily user), you can deploy your newly created snapshot version:

```
git checkout origin/installer-resources-photon dime-deploy
./dime-deploy
```
