# Common Pitfalls

## Unsupported class file major version

**Appearance:** Building the backend of a generated DIME application.

**Stacktrace:**
```
java.lang.IllegalArgumentException: Unsupported class file major version 58
    at org.eclipse.emf.ecore.resource.impl.ResourceSetImpl.handleDemandLoadException (ResourceSetImpl.java:319)
    at org.eclipse.emf.ecore.resource.impl.ResourceSetImpl.demandLoadHelper (ResourceSetImpl.java:278)
    at org.eclipse.xtext.resource.XtextResourceSet.getResource (XtextResourceSet.java:265)
[...]
```

**Reason:** Maven uses an unsupported Java version.

**Solution:** Investigate which Java version is used by Maven (`mvn --version`) and switch to Java version 1.8.

## Failure of imports

**Appearance**: Importing MGLs from a prebuilt DIME Version.

```
import "platform:/resource/info.scce.dime/model/Data.mgl" as data
import "platform:/resource/info.scce.dime/model/Process.mgl" as process
import "platform:/resource/info.scce.dime/model/GUI.mgl" as gui
```

**Solution**: Use following imports (for Example). Change "resource" to "plugin" and ".mgl" to ".ecore"

```
import "platform:/plugin/info.scce.dime/model/Data.ecore" as data
import "platform:/plugin/info.scce.dime/model/Process.ecore" as process
import "platform:/plugin/info.scce.dime/model/GUI.ecore" as gui
```

Make sure that the following package is include in your MANIFEST.MF under _Require-Bundle_:

`info.scce.dime`

## The exact same code leads to different behaviour on different machines

**Appearance:** When generating with the alternate generator of DIME, there can be some non-deterministic errors.

**Observation:** Sometimes the exact same code leads to different behaviour on different machines.
On some machines the generated code is running, on others there might be errors.

![delete-resources](./assets/delete-resources.png)

**Solution:** The problem can be solved by deleting the generated code (which is generated after using "Generate Cinco Product") from disk and not only from the workspace.
