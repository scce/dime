# Frontend

The Frontend is written in [Dart](https://dart.dev/) and based on the [Angular Dart](https://github.com/dart-lang/angular) package.
Its source code is separated into static and generated files.

## Static Sources

Location: `info.scce.dime.generator/static-dart-resources/webapp`

The static source code follows basically the [Effective Dart Specification](https://dart.dev/guides/language/effective-dart/documentation) and complies with [guidelines and best practices](https://pub.dev/packages/pedantic) of Google.
The directory layout is compatible with the official [Pub Package Layout](https://dart.dev/tools/pub/package-layout).

### Development Environment

**Preliminaries**

1. Install Dart SDK according to [Getting Started](/content/getting-started/DIME-2-development-with-maven.md#dart-sdk).
2. Resolve dependencies: `pub get`
3. Open `info.scce.dime.generator/static-dart-resources/webapp` as a project in your favorite [Dart compatible IDE](https://dart.dev/tools).

**Pre Commit**

Performing the following steps will reduce the likelihood of breaking the [CI/CD Pipeline](https://gitlab.com/scce/dime/-/pipelines).

1. Analyze source code against rules of [Pedantic](https://pub.dev/packages/pedantic): `dartanalyzer .`
2. Check if source code is well formatted: `dartfmt --dry-run .`
3. Run tests: `pub run test .`

## BaseTemplate

Location: `info.scce.dime.generator/gui/BaseTemplate`

The BaseTemplate is the entry point for the frontend generator. It defines the most rudimentary HTML frame. 
The way it works is that it evaluates each object to be generated and calls the corresponding more specific templates. Those more specific templates need to be included as dispatch methods in the BaseTemplate.

## Dart

Location: `info.scce.dime.generator/gui/Dart`

This includes all AngularDart Templates. The directory is split in three parts, depending on the (self explanatory) template classifications.

**Base**

Contains the base Templates for every AngularDart Template. Including Headers, Imports, etc.

**Component**

These Templates are mainly responsible for concrete GUI components. They contains all components that go beyond normal HTML components.

**Functionality**

These blueprints are mainly responsible for functions between components or between back- and frontend. It contains all components that ensure the correct execution of the AngularDart components.

## Data

Contains classes that handle additional needed Data from the graphmodel.

## Enums

Contains all enmus that are used in the Frontend Generator.

## HTML

Contains all Templates that are realised purely by utilising HTML. These are all GUI components.

## REST

Contains all Templates that are necessary for background REST communication.

## Utils

Contains classes that provide further recurring methods used in the Frontend Generator. 