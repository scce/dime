# Second Order GUI modeling

GUI SIBs can not only be used in interaction
processes. Thus every application contains recurring parts, it is an important
task to enable a simple realization of reuse in DIME and GUI models. For the
reuse of component compilations, GUI SIBs can be used in the template of a GUI
model as well. A GUI SIB in a GUI model is almost similar presented to a GUI
SIB in an interaction process, but since the GUI model is data-driven and no
control-flow is defined, the branches of the GUI SIB are propagated to next
super interaction process. The input ports for the embedded GUI SIB can be
connected to variables within the GUI model's data context, to enable
parametrization with variable data. As a result of this, GUI SIBs can be
interlaced to describe a topology, which will be recursively rendered at
runtime.

The parametrization of GUI SIBs with different data comes to their limitations,
when specific parts of the embedded GUI SIB depend on the enclosing GUI model.
The only way to enable a particular behavior is based on a distinction of cases,
which disagrees with the simple reuse of GUI SIBs, since every possible input
value has to be considered. To ensure this problem and reinforce reuse,
second order placeholders can be used to determine this variable parts
of a GUI model. In the model design phase, the placeholder can be replaced in
the embedded GUI SIB, analogous to an input with additional components on the
superordinate GUI model. At runtime the specified component compilation is
injected in the GUI model of the embedded GUI SIB and substitutes the
placeholder. This feature is illustrated in the Figure above.
The GUI model DisplayWhatYouWant is used in two different other GUI
models. The included GUI SIB displays an additional placeholder node, which can
be extended with components. The two GUI models at the top of the Figure.
use different components to fill the placeholder. At
runtime, the specified components will be injected in the placeholder of the
used GUI SIB. Thanks to this second order parametrization, every GUI model
refers solely to the immutable components and describes optional changeable
parts using Placeholders.