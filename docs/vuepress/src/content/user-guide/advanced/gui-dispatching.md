# GUI Dispatching

GUI dispatching works like a switch-case for GUIs that implement different subtypes. Here is a short guide which steps are required to get it working:

1. create a GUI as a fallback, in this case "Base.gui". This GUI receives the abstract type you want to dispatch over as input.
2. create arbitrary GUIs that represent more concrete cases
 1. add an extension context and add the GUI to be extended (e.g. Base.gui, or a more specific GUI) to the context
 2. add input variables to the data context so that at least one variable is more specific than in the extended GUI, while having the variables named _exactly_ according to their more generic pendant
3. add the fallback GUI as a component into another GUI, then right-click and "activate dispatching"