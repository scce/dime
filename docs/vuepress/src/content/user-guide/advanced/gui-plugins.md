# GUI Plugins
GUI Plugins allow users to include their own frontend components in DIME applications.
Users can either:
 - Include JavaScript with optional HTML markup and CSS styling
 - or include whole Angular components using Dart

The following Sections will give an overview about what can be done with these GUI Plugins, how to use and – most importantly - how to include them.

**Please note:** DIME requires you to use `angulardart 5.3.0` for Dart components.

## Simple JavaScript Example
This first example will introduce a very simple JavaScript GUI Plugin that allows users of the web app to scroll to the top of the page.

Feel free to download the example plugin using the following link:
[Scroll to top-example](/dime/assets/downloads/scroll_to_top.zip)

You will notice that the example contains two files:
 1. `scroll_to_top.js`
 2. `plugins.gp`

The `scroll_to_top.js` file contains the JavaScript code that should be included in the webapp by the GUI plugin.
It contains a simple script that retrieves the document of the webbrowser, logs a message into the console, and lets the browser scroll to the top of the webapp.

The `plugins.gp` file contains all the information DIME needs to be able to include the afore-described JS file as a GUI plugin.

```
plugin scroll_to_top {
    scripts ["scroll_to_top/scroll_to_top.js"]
    function scrollToTop {}
}
```

First, one has to state that we will define a "regular" JS `plugin` (in contrast to a `dart_plugin`).
This declaration is then followed by a name (e.g. `scroll_to_top`) under which the GUI plugin will be made available by DIME.
Inside of this declaration the path of all `scripts` that should be included by this plugin are stated.
Please note that these paths are always stated relative to the root of the workspace you currently work in.
Last, the `function` that should be called by the plugin is stated.
The `scrollToTop` function in this example matches the function name inside the `scroll_to_top.js` file.

## Advanced JavaScript Example
This second example will introduce a more complex JavaScript GUI Plugin that integrates a third party JS library in the webapp.

Feel free to download the example plugin using the following link:
[Markdown Editor example](/dime/assets/downloads/markdown_editor.zip)

You will notice that the example contains eight files:
 1. `bootstrap-markdown.js`
    - Third-party JS code for the markdown editor
 2. `bootstrap-markdown.min.css`
    - Third-party CSS for the editor's styling
 3. `markdown.min.js`
    - Additional third-party JS code for the markdown editor
 4. `mdedit.html`
    - A HTML template which displays the markdown editor
 5. `mdedit.js`
    - A script to trigger the creation of the markdown editor
 6. `style.css`
    - Additional styling
 7. `to-markdown.js`
    - Additional third-party JS code for the markdown editor
 8. `plugins.gp`

The `plugins.gp` file contains all the information DIME needs to be able to include the afore-described JS, CSS and HTML files as a GUI plugin.

```
plugin markdown_editor {
    styles [
        "plugins/markdown_editor/bootstrap-markdown.min.css",
        "plugins/markdown_editor/style.css"
    ]
    scripts [
        "plugins/markdown_editor/bootstrap-markdown.js",
        "plugins/markdown_editor/markdown.min.js",
        "plugins/markdown_editor/to-markdown.js",
        "plugins/markdown_editor/mdedit.js"
    ]
    template "plugins/markdown_editor/mdedit.html"
    function MarkdownEditor {
        primitiveParameter "mdText": text sync
    }
}
```

First, one has to state that we will define a "regular" JS `plugin` (in contrast to a `dart_plugin`).
This declaration is then followed by a name (e.g. `markdown_editor`) under which the GUI plugin will be made available by DIME.
Inside of this declaration the path of all `styles`, `scripts` and the `template` that should be included by this plugin are stated.
In contrast to `styles`, and `scripts`, only one `template` can be provided.
Please note that these paths are always stated relative to the root of the workspace you currently work in.
Last, the `function` that should be called by the plugin is stated.
The `MarkdownEditor` function in this example matches the function name inside the `mdedit.js` file.
With this function a `primitiveParameter` called `mdText` is used, that utilizes the primitive type `text`.

This plugin also utilizes the `sync` option for parameters.
Synced parameters will result in a two-way data binding so that manipulations inside the GUI plugin will directly manipulate the attached DIME variable and vice versa.
Please note that synced variables require an additional 

## Simple AngularDart Component GUI Plugin
In addition to regular JS and some styling it is also possible to integrate whole Angular components into your DIME webapp.
This can be done with Dart Plugins.

As the name suggests, the Angular components have to be written in [Dart](https://dart.dev) as DIME utilizes Dart as well.

Feel free to download the example plugin using the following link:
[Affiliation Listing example](/dime/assets/downloads/affiliation_listing.zip)

You will notice that the example contains four files:
 1. `affiliation_listing.css`
    - Styling information for the Affiliation Listing Component
 2. `affiliation_listing.dart`
    - The Angular component itself
 3. `affiliation_listing.html`
    - The Angular component's template
 4. `plugins.gp`

The `plugins.gp` file contains all the information DIME needs to be able to include the afore-described JS, CSS and HTML files as a Dart plugin.

```
dartPlugin affiliation_listing {
    styles ["plugins/affiliation_listing/affiliation_listing.css"]
    scripts ["plugins/affiliation_listing/affiliation_listing.dart"]
    template "plugins/affiliation_listing/affiliation_listing.html"
    component AffiliationListing {
        primitiveParameter "organization" : text
        primitiveParameter "department" : text
        primitiveParameter "street" : text
        primitiveParameter "zipCode" : text
        primitiveParameter "city" : text
        primitiveParameter "state" : text
        primitiveParameter "country" : text
    }
}

```

First, one has to state that we will define a `dart_plugin` (in contrast to a "regular" `plugin`).
This declaration is then followed by a name (e.g. `affiliation_listing`) under which the GUI plugin will be made available by DIME.
Inside of this declaration the path of all `styles`, `scripts` and the `template` needed for the Angular component are stated.
In contrast to `styles`, and `scripts`, only one `template` can be provided.
Please note that these paths are always stated relative to the root of the workspace you currently work in.
Last, the `component` that should be included by the plugin is stated.
The `AffiliationListing` component in this example matches the class name inside the `affiliation_listing.dart` file.
This function utilizes several `primitiveParameter`s that are just stated like for "regular" GUI plugins as well.
Inside the `affiliation_listing.dart` file matching variables have been annotated with, for example, `@Input('street')` to bind the parameter to the component's variable.

The embedded Dart plugin looks like this:

![Affiliation Listing Result](./assets/affiliation_listing_result.png)

## Advanced AngularDart Component GUI Plugin
This Section will give a more complex Dart plugin example, which utilizes output branches and complex types.

Feel free to download the example plugin using the following link:
[Affiliation Listing example](/dime/assets/downloads/toggle_button.zip)

You will notice that the example contains four files:
 1. `toggle_button.css`
    - Styling information for the Affiliation Listing Component
 2. `toggle_button_property.dart`
    - The Angular component itself
 3. `toggle_button.html`
    - The Angular component's template
 4. `plugins.gp`

The `plugins.gp` file contains all the information DIME needs to be able to include the afore-described JS, CSS and HTML files as a Dart plugin.

```
import "dime-models/app.data" as data

dartPlugin toggle_button_property {
    styles ["plugins/toggle_button/toggle_button.css"]
    scripts ["plugins/toggle_button/toggle_button_property.dart"]
    template "plugins/toggle_button/toggle_button.html"
    component ToggleButtonProperty {
        primitiveParameter "onText" : text
        primitiveParameter "offText" : text
        primitiveParameter "value" : boolean sync
        complexParameter "property" : data.BooleanProperty

        output switchedOn {
            complexParameter "property" : data.BooleanProperty
        }
        output switchedOff {
            complexParameter "property" : data.BooleanProperty
        }
    }
}
```

As you might see, the `dartPlugin` declaration looks similar to this of our Affiliation Listing example in the above Section.
However, there are some differences that make use of more advanced features.

First, you might notice the `import` statement in the first line.
This statement references the `data` definition of the DIME webapp and makes it available to use within the `.gp` file under the `data` namespace.
Such imports statements are needed if you want to use complex parameters with types declared in your DIME app.
You can see such a `complexParameter` in line 11.
It looks like a regular `primitiveParameter` but instead of using `text`, or another primitive type, it references the `BooleanProperty` type defined in the `.data` available under the `data` namespace.

Additionally, this example uses a synced parameter.
It has just the same purpose and functionality as described in the advanced JavaScript example above.
However, if you want to use synced parameters in Dart plugin you have to provide an additional output in your dart class like so:
```
@Output('syncvalue') Stream<Map<String,dynamic>> get evt_syncvalue => syncvaluestream.stream;
StreamController<Map<String,dynamic>> syncvaluestream = new StreamController();
```
**Please note** that you have to adjust `syncvalue` to your needs (i.e. `sync${yourParameterName}` for the `@Output` annotation and `evt_sync${YourParameterName}` for the map's key).

Last, `output`s are used in this example.
For each output a new output branch will be created when the GUI plugin is used.
Beside their names (e.g. `switchedOn`), outputs can also possess output ports.
They are also described as `primitiveParameter`s or `complexParameters`, just like inputs as well.
Please note that these output branches also need outputs in your Dart class:
```
final StreamController<Map<String,dynamic>> _onStream = new StreamController();
@Output('switchedOn') Stream<Map<String,dynamic>> get onStream => _onStream.stream;
```
Replace the value of the `@Output` annotation with your parameter's name.


The embedded Dart plugin looks like this:

![Toggle Button Result](./assets/toggle_button_result.png)

## AngularDart GUI Plugin with Placeholders

In the examples above, we covered plugins that had a static HTML template which is rendered to the website.
With *Placeholders* you can embed other components from DIME GUI models into the GUI plugin, thus making it reusable for different application scenarios.
In this example, you see how that works with a simple GUI plugin, which renders a `div` element with custom `class` and `style` properties.
Using this plugin, we created an element with an orange background that centers everything that is projected inside it (a headline in this example) horizontally and vertically.
The result looks like this:

![Placeholder Result](./assets/placeholder_result.png)

Feel free to download the example plugin using the following link:
[Placeholder example](/dime/assets/downloads/custom_div.zip)

In the image above, you can see the DIME GUI plugin `CustomDiv` being used.
It comprises a placeholder, i.e. a rectangular area with the label content that can contain arbitrary GUI model elements.

The placeholder for the plugin is defined in two places: the HTML template of the plugin and the GUI plugin definition file.
At first, let us have a look at the HTML template `custom-div.html`:

```
<div [class]="classes" [style]="styles">
	<ng-content select="[dime-placeholder=content]"></ng-content>
</div>
```

As you can see, a placeholder has to be defined in an `ng-content` tag that has a `select` attribute.
The value of the attribute has to equal `[dime-placeholder=...]` where `...` needs to be replaced by a name of your choice.
In this example, the name is *"content"*.
This name is the identifier for the placeholder, which is also displayed in the GUI model.
Further, note that there can be multiple placeholders for a component but every placeholder needs a unique name.
Once rendered, any content that is projected from the outside into the component will be rendered at the location of the corresponding `ng-content` tag.

Finally, let us look at the plugin definition file `plugin.gp` below:

```
dartPlugin CustomDivPlugin {
	styles ["plugins/CustomDivPlugin/custom-div.css"]
    scripts ["plugins/CustomDivPlugin/custom-div.dart"]
    template "plugins/CustomDivPlugin/custom-div.html"
    
    placeholder [content]
    
    component CustomDiv {
        primitiveParameter "classes": text
        primitiveParameter "styles": text
    }
}
```

As you can see, we define which placeholders exist in the component using the `placeholder [...]` notation.
Here, you can define a comma separated list of placeholders, where the names of the placeholders have to match with the ones specified in the HTML template file.
Such a plugin with multiple placeholders would also have multiple placeholder areas in the GUI model.


## Common Pitfalls
 - Dart plugins have to be stated before any "regular" JS plugins
