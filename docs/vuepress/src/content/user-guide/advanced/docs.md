# GUI

GUI models in DIME are structural declarations of a user interface, including
basic control structural components and data-bindings to
components. One of the main policy is the support for massive reuse,
since DIME is designed for rapid prototyping and agile development.

In the following the data and event-driven concepts regarding to the core components
and directives are described. The GUI models provide a multitude of basic components
for accepting user input and displaying data, advanced components and various elements
to create a unique user interface. In addition to this, every component is highly customizable to
personalize the individual look and feel of the page at runtime.

## Data Flow

The GUI model is exclusively data-driven, which means that no control-flow is
available. Only data changes result in a modification of the viewable page.
The life-cycle of a GUI model starts with a rendering, including the injection of
initially specified data. Similar to the process models, the GUI models take
usage of a data context as well, which can be used analogous. In contrast to
the processes the GUI models cannot receive data from output ports of a start
node. The initially needed data has to be specified as an input in the
properties of a variable in the data context. Every variable, which is
specified as an input, will result in an Input Port of the corresponding GUI
SIB. Depending on the received data and the Directives of the GUI, the
rendering is done at runtime.
[read more](gui-data-flow.md)

## Build-In Template Components

The GUI model of DIME is used to describe the declarative structure of the
rendered page. This user interface structure is called template. The
template can be build up using different components which represent common parts
of a user interface like tables, forms with input fields and parts to render
content. The different components can be nested in each other to customize the
page and create an unique styling. The available components in the GUI models
are divided into multiple groups, according to different requirements of a user
interface. The succinct groups will now be introduced.

### Grid System

The grid system provides the ability to place
the components in an entirely tunable layout. To align the components in a
template, a row and column based grid system is provided. Every row can be
subdivided in at most twelve columns, which can also be vertically redivided by
additional rows. A particular column can be scaled in its relative width, to
fit e.g. the full, half or quarter row width. At runtime the relative grid
system is used in a responsive way to adjust the template components to any page
sizes or device dimensions. For rapid prototyping the grid system is optional.
In this case, the used components are aligned
among each other. The different components are vertically arranged without the use of the grid system and will
accrue in single rows with full-width columns at runtime.

### Content

Content announcement is one of the core tasks of
a user interface. For this reason the GUI model provides multiple components
to display different types of data. Thanks to the automatically immediate
data-binding, a displayed value of a variable is always up to date. The
headline component, is one of the components to display content. At modeling
time, the size and the content of the headline can be specified. Like every
other content in the properties view of a GUI model component, it can be
determined as a composite of static content and expressions to display dynamic
values of variables.

In addition to the headline, a text and a file component are provided as 
well.
The file component is necessary to offer a download option for the user at
runtime, since the access of a file variable using an expression displays only
the file name. Besides this basic content announcement, GUI models offer
specialized components to show e.g. images, thumbnails, alert boxes and
progress bars. Thanks to the simplicity approach of DIME and the GUI model,
the use of a special components is as simple as the data-binding of the text
component.

### Tables

Tables can be used to display interrelated information (i.e. entries of list) in multiple 
columns. To bind a list variable
to the table, a table load edge is provided. As a result of this, the
source of the table is defined, so that the changes of the variable causes a
refresh of the table without any additional effort. Besides the basic
presentation of data, the table component offers features to sort and filter the
displayed rows. To take advantage of this features, columns has to be specified
for the table. The corresponding component, which is available in the palette
of the GUI model, is the table entry component. Multiple properties
can be set on a table entry, like the label and a sorting and filter option.
If the options are enabled, the source variable for the column has to be
specified by a table column load edge. This edge should be connected
to a member of the expanded current variable of the source list.
The filter and sort functions are implicitly defined based on the data type
of the connected variable without any further effort. The actually displayed
content of a table column, represented by a table entry, can be entirely
customized by the use of all available components or just show the value of
the binded variable. Similar to the remaining features of the GUI model, the
developer just decides which features are needed and not how they are
realized.

The table component can be used to select on entry of the source list variable.
The choice property of the table can be set to enable a single or multiple
selection. If a table row is selected at runtime, the value is written to a
target variable, specified by a table target edge. Besides this
functional aspects of the table, there are multiple other properties to
personalize the rendering of the table like the bordering of the entries and a
pagination.

### Forms

Forms encapsulates components which enable the
user to modify the values of variables. In the GUI model only two different
types of modifications has to be differentiated. On the one hand the form field
component, which allows the editing of all primitive variables and on the other
hand the form selection components like radio, combo box and
check box for assignment tasks. As a common feature for all form
components, a target variable has to be specified which will be modified at
runtime, when the user makes changes. The connection between a form component
and the target variable can either be done by a form submit edge for
unidirectional editing and form load submit edge for bidirectional
editing. The bidirectional editing results in the propagation, of the modified
value of the variable to the form components when it is changed in another
component as well.

The selection components can be used to realize an assignment of a selection,
based on a source list variable, to a target variable. Depending on the desired
kind of selection the corresponding component can be chosen. The selection
components differ in the their multiplicity. This means that e.g. the radio
component offers only one entry to be selected, whereas the check box allows
multiple entries to be selected. The combo box component can be adjusted to
accept multiple or just one selection in their properties. Independent of the
concrete multiplicity, every selection component requires a source list variable
and a target variable. The source variable can be connected to the component by
a choice load edge, so that the current variable is available
to be displayed in the inner scope. When an entry is chosen by the user, the
target variable is assigned to the entry's value. The multiplicity of the
target variable depends on the multiplicity of the selection component.

To edit primitive variables in GUI models, the form field component is provided.
Similar to the selection components, a target variable connection is required.
Every primitive data type like text, real or file is supported and furthermore
special input types can be specified in the properties of the component. This
offers the possibilities of additional semantics at modeling time, since the
expected input can be defined as e.g. a color, a phone number or an URL. The
generated input field will autonomously validate and parse the inserted values
and offers special input assistance to the user at runtime, without any
additional effort.

Every form component offers additional optional properties. A form field can be
supplemented by a label and a help text, to clarify the intentions and multiple
visual settings like sizes or colors. Another important aspect, when taking
user input, is validation. Since the primitive form field components specify
input types, a simple validation can be performed. In addition to this, the GUI
model provides other restrictions to be imposed, like a minimal and maximal
amount of inserted characters and a required flag property, which ensures that
the form component is filled out. For highly specialized validations, a regular
expression can be defined to fit particular requirements. The validation is
performed at runtime. The color of the input field will change, to notify the
validation status. In addition, an error text property can be given on the form
component, which is shown if an invalid input is present.

The form components of the GUI model are based on the simplicity policy of DIME
and facilitate the rapid design of user input processing. Even the complex task
of validating the inserted data to guarantee the correct further use is
simplified by predefined validations.

## Build-In Structural Components

Besides the different components placed in the template to display and modify
the values of variables in the data context, structural components can be used to
dynamically adapt the template to variables. The GUI model provides the basic
structural components if and for represented by certain edges.

### IF Edge

The if edge can be used to display or hide a component and all of their inner
components at runtime. To define a conditional indication, the if edge can be
dragged from a variable to an arbitrarily component. An inverted condition can
be created by negating the if edge in the properties. Based on a boolean
evaluation of the variable, the connected component is shown or hidden when the
result is falsely. Similar to the data-binding edges or expressions, the
condition of the if edge is re-evaluated automatically, iff the corresponding
variable is modified. The condition is implicit defined based on the type of
the variable. Numeric primitive variables are considered as truly, if the value
is higher than 0. Textual and list variables are evaluated truly when they are
not empty. When a complex variables is evaluated, the result depends on a
null check. This implicit conditions simplify the ease of use according to the
principles of DIME.

### FOR Edge

Dynamic data structures like lists cannot be displayed in a static manner. To
access every item of a list variable, the iterative for edge structural components is
provided. The connection of a list variable to a component in the template
using a for edge, results in the repetition of the target component for each
element in the list at runtime. The current iteration variable, which
can be expanded form the list variable, is available in the scope of the target
and all inner component. In addition to this, an index variable can be
declared on the properties of the for edge, which is also available in the
iteration scope. A specialty is present when the connected variable is s
primitive list. In this case no iterator variable can be expanded. Instead of
that the name of the iterator variable has to be specified on the edge
properties, to access the value of the iterator.

The structural components enable the developer to define dynamic behavior and
responsiveness, by just dragging edges between the variables and components.
The usage is prepared intuitively to abbreviate the way to meet the target
solution.

## Data-Type Dependent

## Process-Model Dependent

## GUI-Model Dependent