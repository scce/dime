# GUI Data Flow

After the rendering is finished, only changes to the available data are considered. 
Every change starts a re-rendering of the affected components, which are binded to a modified variable. 
Changes to the variables placed in the data context can only be performed by form components. 
Every modification done on a form component's value is immediately propagated to the binded variable.
Variable bindings are described by dedicated data-binding edges, which are dragged from a variable in the data context to a form component. 
This edges can either be used in a uni or bidirectional way, to specify one- or two-way data-binding.

If a variable is changed, every corresponding component which reads the variables (e.g. to display the variables value) is refreshed. 
Variable values can be read in multiple ways. 
Every continuous text in the GUI model components can be extended with expressions. 
Expressions use double curly braces and an interpolation to display the value of a variable at runtime. 
As an example, the name of a currentUser variable is displayed in a headline component using the expression <code v-pre>{{currentUser.name}}</code>.
The dot-operator provides access to the attributes of a complex variable and can be chained up to step through the variable and its members in depth.
Besides the expressions for inline variable announcement, certain GUI model components facilitate the uni directional data-binding, like tables and buttons.

The life-cycle of a GUI model ends, when a button is clicked, since every button of a GUI model results in a branch of the corresponding GUI SIB. 
The super interaction process can react depended on the different possible branches and executes process or displays another GUI model. 
In most cases, it is necessary get specific data from a GUI model, when it is terminated, like a selected entry of a table. 
To meet this goal, the GUI model provides the submit edge, which can be drawn between any variable and a button component. 
The connected variable will be propagated to the branch in super interaction process, as an output port. 
As a result of this the transmitted data of a GUI model, can be subsequent processed.

The data-binding between the variables in the data context and the components simplifies the actualization and redundantize the disadvantage that the user has to take care of the synchronization.
At runtime every part of the page is always up to date without any additional effort. 
For the modeling time, the desired arrangement and features are brought into focus instead of struggling with technical intricacies.