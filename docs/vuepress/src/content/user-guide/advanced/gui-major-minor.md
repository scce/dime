# Navigation with Major Minor

Navigation components can be used to give the user
at runtime an overview of the available parts of the application. Suitable
components for the navigation features are tabbing and top or
bottom navigation bars. These components can be extended with multiple 
buttons
or other components like drop downs. In contrast to the other buttons
placed in the template, the navigation buttons show their state and highlight
which button has been clicked last. As a result of this, the navigation
components can be used for switching between different sub pages, which will be
displayed when the corresponding button is clicked. The clicked button will
then remain in an active state, to visualize the current sub page. In the
context of GUI models, these sub pages are called minor GUIs.

To benefit from the navigation components, it is necessary to keep the buttons
and their state apparent, while the minor GUI is exchanged. This goal is met by
the use of major GUIs. A major GUI is characterized by a
placeholder component, which can be embedded anywhere in the template
or its components. At runtime, the placeholder will be replaced by one of the
available minor GUIs. The concrete correlation between the buttons of the
navigation components in the major GUI and the different minor GUIs is realized
in the super interaction process. This is shown as an example in the Figure.
The interaction process in the middle contains three
GUI SIBs. The major GUI SIB Major is enrolled at the top. The
top navigation bar, which is placed above the placeholder component consist of the two
buttons Show First and Show Second. The corresponding
branches are connected with the minor GUI SIBs for the equally named GUI models
First and Second. At runtime the placeholder will be replaced
with one of the minor GUI models, dependent on the button, which is clicked.
The button in the navigation bar will further remain in an active state to display the
currently injected GUI model.

The adjustment, whether a GUI SIB in an interaction process is treated as a
major or minor GUI can be specified in the properties of the SIB. By default,
every GUI SIB is a major GUI in such a way as to enable complete replacement of
the GUIs at runtime. The navigation buttons of the major GUI are represented by
the equally named branches in the interaction process. Every branch is
connected to a minor GUI, so that the major and all of the connected minor GUIs
declare a potential state in the interaction process. Since the major GUI is
reached first in the control-flow of the interaction process, an additional
property is provided to specify the initial minor GUI, which will replace the
placeholder in the major GUI. Thanks to this principle, the creation of
stateful layout pages with replaceable embedded content can be done rapidly
without any further configuration.