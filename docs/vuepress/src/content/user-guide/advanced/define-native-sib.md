# Native SIB

## Create a Simple External Service Interface

To create an external Service, you can extend the Native SIB Library with a new SIB.

* Import the dywa-app Project as an additional Project in your DIME IDE
** Right-Click the Project-Explorer and select _Import_
** Select _Maven_ -> _Maven Project from existing Source_ 
** Select the _dywa-app_ folder as the Project root, select at least the _app-business_ project and hit finish
* Open the app-bussiness/src Package in the Project-Explorer and create a package. E.g: `info.scce.dime.examples.sibs`
* Create a java class (e.g. `Native.java`)

This class will be the access point to the external service we want to call.
To call a Service we need at least one public static Java method.

<pre>
public static String textAppend(String a, String b) {
  return a + " " + b;
}
</pre>

## Add the Service to the SIB Library

Now let's add the Service to our SIB Library.

Open the _common.sibs_ file in your _dime-models_ folder or create a new file with file extension `.sibs`.
You might need to confirm the Xtext Nature when opening the file for the first time.
Define a new SIB corresponding to the `textAppend` method's signature:

<pre>
sib TextAppend : info.scce.dime.examples.todo.sibs.Atomic#textAppend
  a: text
  b: text
  -> success
    result: text
  -> failure
</pre>

* After the keyword `sib` follows the SIB's name, here `TextAppend`
* After the name and a colon `:` follows the identifier of the static Java method to be executed
* The method is defined by the FQN of the defining Java class followed by `#` and the method name
* The next lines define the signature of the SIB, i.e. its input ports, branches and output ports
	* inputs `a` and `b` of type `text`
	* branch `success` with output port of type `text` for the return value of the `textAppend` method
	* branch `failure` without output port to be taken if an exception occurs

**Note: Always define inputs in the same order as the method parameters**

For a complete overview see [Native SIB Syntax](#native-sib-syntax) as well as [Native SIB Structure](#native-sib-structure) beneath.

## Use the SIB in a Process

Now we are ready to use the new external service as a SIB in a Process

* Open a Process model
* Open the _Control_ View
* Search and expand the _Native_ -> _common.sibs_ entry
* Search for the newly created SIB `TextAppend` and drag it to the workbench.

After having finished modeling you can generate your app and use the external service.

## Native SIB Syntax

The syntax is described in EBNF style.
Basically, everything that is written in single quotes can be considered a keyword.

The special terminal symbol JID denotes a Java Identifier, which is a
String consisting of only ASCII letters and digits, but not starting
with a digit.

```
siblibrary    := ( sib | javaType )*
javaType      := localTypeName ':' classname
localTypeName := JID
sib           := 'sib' sibname ':' classname '#' methodname input* branch+
methodname    := JID
classname     := ( JID | JID.classname )
branch        := '->' branchname output*
branchname    := JID
input         := parameter
output        := parameter
parameter     := parametername ':' parametertype
parametername := JID
parametertype := listType | singleType
listType      := '[' singleType ']'
singleType    := primitiveType | javaTypeReference
primitiveType := 'text' | 'integer' | 'real' | 'boolean' | 'timestamp'
```

The javaTypeReference refers to the lcoalTypeName of a javaType defined in the same .sibs file.

## Native SIB Structure

The code generator supports the following return types of static Java methods.

### Return Type: Boolean

#### Variant 1 (Branches true/false)

* *true* branch, no outputs
* *false* branch, no outputs
* *noresult* branch, no outputs, to represent null (java.lang.Boolean can be null, primitive boolean can't)
* *failure* branch, optional output java.lang.Exception *exception*

**Example:**

```
sib Example : info.scce.dime.examples.Example#example
  a: text
  b: text
  -> true
  -> false
  -> noresult
  -> failure
```

#### Variant 2 (Branch success)

* *success* branch with output "result" of type `boolean`
* *noresult* branch, no outputs, to represent null (java.lang.Boolean can be null, primitive boolean can't)
* *failure* branch, optional output java.lang.Exception *exception*

**Example:**

```
sib Example : info.scce.dime.examples.Example#example
  input1: text
  input2: integer
  -> success
	 	result: boolean
  -> noresult
  -> failure
```

### Return Type: Enum

* *literal name* one branch for each enum literal, named like the literal, no outputs
* *noresult* branch, no outputs, to represent null
* *failure* branch, optional output java.lang.Exception *exception*

**Example:**

```
sib Example : info.scce.dime.examples.Example#example
  input1: text
  input2: integer
  -> Value1
  -> Value2
  -> noresult
  -> failure
```

### Return Type: Void

* *success* branch, no outputs
* *failure* branch, optional output java.lang.Exception *exception*

**Example:**

```
sib Example : info.scce.dime.examples.Example#example
  input1: text
  input2: integer
  -> success
  -> failure
```
