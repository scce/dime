# GUI Generator

## Generated File Structure

```
app-presentation/target/generated-sources/app/
   pubspec.yaml
   web/
      index.html
      main.dart
      css/
      font/
      js/
   lib/
      app.dart
      collapse/
         collapse.dart
         collapseSign.dart
      components/
         <gui-name>.dart
         <gui-name>.html
      directives/
         MaxLength.dart
      filesupport/
         fileitem.dart
         filelikeobject.dart
         fileselect.dart
         fileuploader.dart
      forms/
         <gui-name>/
            Form<gui-id><gui-name>.dart
            Form<gui-id><gui-name>.html
      interactions/
         Interaction<process-name><process.id>Process.dart
         Interaction<process-name><process.id>Process.html
      models/
         FileReference.dart
         Todos.dart
         <interactable-name><process-id>Input.dart
         <interactable-name><process-id>Output.dart
         InteractionWrapper<process-name>.dart
         Resume<process-name><endSib-name>Input.dart
         SelectiveProcess_<process-name>View.dart
         SelectiveGUI_<gui-name>View.dart
      routables/
         Routable<process-name><process-id>Process.dart
         Routable<process-name><process-id>Process.html
      services/
         User<gui-id>Service.dart
      tables/
         <gui-name>/
            Table<gui-id><gui-name>.dart
            Table<gui-id><gui-name>.html

```

## Generated File Purpose and Model Relation

* app/
    * *pubspec.ymal* Basic App configuration, environment, dependencies and transformers
* app/web/
    * *index.html* Root entry point to the app, including the loading screen and main imports for JS and CSS files
    * *main.dart* Bootstrapping of the app and all services
* app/lib/
    * *app.dart* The router-outlet is placed here and all URL-Patterns are defined to access all Interaction Processes, including the route parameters, which depend on the ports of the StartSIB of the Process.
        * app/lib/collapse/
        * *collapse.dart* A Angular Directive to collapse components (e.g. Panels).
        * *collapseSign.dart* A Angular Directive to show an arrow pointing the current collpase-status of the component
* app/lib/components
    * *\<gui-name\>.dart* The Angular component class related to the a GUI model including only the business logic and input/output declaration
    * *\<gui-name\>.html* The Angular component template to the a GUI model including only the visual representation in HTML
* app/lib/directives
    * *MaxLength.dart* An Angular directive to realize the max chars feature of "Static Content":https://projekte.itmc.tu-dortmund.de/projects/scce-documentation/wiki/Static_Content, which displays too long texts in a tooltip.
* app/lib/filesupport
    * *file\<\>.dart* An Angular directive and multiple helper classes to realize the file upload component, when a form field of the input type _file_ is present.
* app/lib/forms
    * \<gui-name\>/
        * *Form\<gui-id\>\<gui-name\>.dart* The Angular Component related to a Form placed in a GUI model, including only the business logic.
        * *Form\<gui-id\>\<gui-name\>.html* The Angular Component related to a Form placed in a GUI model, including only the visual representation in HTML.
* app/lib/interactions
    * *Interaction\<process-name\>\<process.id\>Process.dart* The Angular component class related to the a Interaction Process model including only the business logic and input/output declaration.
    * *Interaction\<process-name\>\<process.id\>Process.html* The Angular component class related to the a Interaction Process model including only the component tags of all available GUISIBs in the Process.
* app/lib/models
    * *FileReference.dart* A dart class represents the variables and attributes of the primitive type _file_.
    * *Todos.dart* A dart class represents the interaction process inputs and additional information used for the "TODOList":https://projekte.itmc.tu-dortmund.de/projects/scce-documentation/wiki/TODOList
    * *\<interactable-name\>\<process-id\>Input.dart* A dart class representing the compound input ports of a Interactable ProcessSIB used in a Interaction Process.
    * *\<interactable-name\>\<process-id\>Output.dart* A dart class representing the compound output ports of all branches of an Interactable ProcessSIB used in a Interaction Process. The taken branch is stored as well.
    * *InteractionWrapper\<process-name\>.dart* A dart class storing all inputs for an Interaction Process used by the Todos.dart class.
    * *Resume\<process-name\>\<endSib-name\>Input.dart* A dart class storing the data of all ports of an EndSIB of an Interaction Process.
    * *SelectiveProcess_\<process-name\>View.dart* Multiple dart classes storing the data of the used view of all DirectDataFlow edges and all Variables in all data contexts of an Interaction Process.
    * *SelectiveGUI_\<gui-name\>View.dart* Multiple dart classes storing the data of the used view of all Variables in all data contexts of a GUI Model.
* app/lib/routables
    * *Routable\<process-name\>\<process-id\>Process.dart* The Angular component class related to the a Interaction Process model URL Pattern. The route parameters are read out, validated and transformed to the corresponding Interaction Process inputs.
    * *Routable\<process-name\>\<process-id\>Process.html* The Angular component class related to the a Interaction Process model component tag including the inputs.
* app/lib/services
    * *User\<gui-id\>Service.dart* An Angular Service fetching the selective user view for a GUI model which uses a variable of an User Type named "currentUser":https://projekte.itmc.tu-dortmund.de/projects/scce-documentation/wiki/Current_User
* app/lib/tables
    * \<gui-name\>/
        * *Table\<gui-id\>\<gui-name\>.dart* The Angular Component related to a Table placed in a GUI model, including only the business logic.
        * *Table\<gui-id\>\<gui-name\>.html* The Angular Component related to a Table placed in a GUI model, including only the visual representation in HTML.