# Performance Optimization

This is a summary of the identified issues that affect runtime performance.

## Database Queries

Using the `RetrieveOfTypeSIB` in models causes a costly database request as it a `domController.findByProperties(searchObject)` call is generated for it.

This is even worse if the RetrieveOfTypeSIB is used in loops or for each item in a list when collecting data for a specific page.

## Processes

Processes are managed instances in the Java EE context. On creation, stuff like the Bean Manager and DOM Controllers are injected. Hence, starting a process costs time. This gets worse for nested processes or loops.

Instead of using process models for the sake of reuse, try to avoid extensive use of processes. In particular, do not use processes in loops or for each item in a list when collecting data for a specific page.

The worst case is using `RetrieveOfTypeSIBs` in nested processes in a loop.

Solution proposal: wherever performance is crucial, try to do expensive work in code via Native SIBs.

## Extension Attributes

Although Extension Attributes are awesome, they are realized by means of processes and hence the same conclusions apply as above.

## Embedded Processes

Embedding ProcessSIBs in GUI models results in separate requests to the backend. Using them in a loop for each item of a list may result in request overload and freezing UI.

The modeler needs to carefully evaluate whether or not to use an embedded ProcessSIB at the respective location. Reuse and encapsulation should not be the driving factor.

Note that ProcessSIBs marked as _modal_ will only then create a request when the modal dialog opens while all others create a request immediately.

## Context Variables

The variables in the context of a running process are serialized, encrypted, passed to the frontend and returned to resume the process after a GUI. The less variables have to be serialized the smaller is the payload that is passed around, which saves bandwidth and time.

A dataflow analysis has been implemented in the DIME generator to sort out those variables that are _skippable_, i.e. they do not have to be serialized because they would never be used again when the process resumes. This way a tremendous decrease of context variables has been achieved.

Example values for Equinocs (the biggest DIME app so far):
```
found 4013 DirectDataFlows
3650 of those are skippable

found 1765 Variables
1565 of those are skippable
```

## Transient Objects

Objects that are not saved in the DB have to be completely serialized and passed around in the context. Hence, the same conclusions apply as above.

Worst case: creating transient objects using RetrieveOfTypeSIBs in nested processes in a loop.

However, transient objects are inevitable atm if you want to filter the data that is sent to the frontend, e.g. undisclosed informtation. The only alternative is using Extension Attributes but due to the performance issues with processes this would be no gain.

## Table Load

Currently, there is no mechanism that supports partial collection of objects to be shown in a table or list, even if the table uses pagination. This feature might decrease payload and hence time efficiently.