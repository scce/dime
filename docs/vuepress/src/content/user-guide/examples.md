# Examples

## TODO App

[![](http://img.youtube.com/vi/xLo2EPsJH_c/0.jpg)](http://www.youtube.com/watch?v=xLo2EPsJH_c "")

* Initialize 'TODO App' Example Project:
    * Right-Click in the Project-Explorer on the left and select _New_ -> _New DIME App_
    * Choose the option _Initialize the project with an example app_
    * Choose the option _TODO-app_ -> _Finish_
* A new Project _TODO-app_ is created.
* open TODO-app/dime-models/todo.dad from the Project-Explorer
* Hit (G)enerate. The eight button from the left in the top menu bar.

### Deploy

* Deploy the DIME App Back-End following the [manual](development-with-docker.md)
* Open [http://127.0.0.1:8080](http://127.0.0.1:8080) to enter your DIME app.
* Login with the following credentials

**Username:** `peter`\
**Password:** `pwd`
