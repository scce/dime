# Text

## Description

The Text component can be used to display static text content, as well as dynamic text content using Expressions.
The styling of this component can be customized via its properties.

## Properties

 - `content`: The displayed content (see [Static Content](gui-docs-static-content))
    - `renderMarkdown`: Write your content in Markdown (see [Markdown Content](gui-docs-md-content))
    - `html`: Write your content in HTML (see [HTML Content](gui-docs-html-content))
    - `rawContent`: Write your text content (see [Static Content](gui-docs-static-content) and [Expressions](gui-docs-expressions))
 - `content·styling`: Add a styling to your text content (see [Styling](gui-docs-styling))
 - `content·icon`: *No effect*
 - `generalStyle`: Additional styling options (see [Styling](gui-docs-styling))
 - `generalStyle·tooltip`: Add a tooltip to your text (see [Tooltip](gui-docs-tooltip))


## Creation

![](./assets/text-palette.png)

Find the *Text* component in the group *Content* of the palette (see 1).
Drag and drop it in the editor to create a Text component (see 2).
Select the Text component to access its properties in the *Cinco Properties* view (see 3).
In the text area `rawContent` under `content` you can add the text content to be displayed (see 4).


## Static Text (Raw Content)

This example illustrates how to create raw text content.

### Modeling (How to use it in DIME)

![](./assets/text-raw-model.png)

In the picture above, the Text contains the content *"Welcome!"*.

### Result (How it looks in the web app)

|![](./assets/text-raw-app.png)
|-

## Static Text (HTML)

This example illustrates how to create text content written in HTML.

### Modeling (How to use it in DIME)

![](./assets/text-html-model.png)

In the *Cinco Properties* view under `content` you can select `html` to write HTML code in the `rawContent` text area.
More details here: [HTML Content](gui-docs-html-content)

In the picture above, the HTML content `<div style="color:red;>Welcome!</div>` is used.

### Result (How it looks in the web app)

|![](./assets/text-html-app.png)
|-

## Static Text (Markdown)

This example illustrates how to create text content written in Markdown.

### Modeling (How to use it in DIME)

![](./assets/text-markdown-model.png)

In the *Cinco Properties* view under `content` you can select `renderMarkdown` to write Markdown content in the `rawContent` text area.
More details here: [Markdown Content](gui-docs-md-content)

In the picture above, the Markdown content `### Welcome!` is used.

### Result (How it looks in the web app)

|![](./assets/text-markdown-app.png)
|-

## Dynamic Text (Expressions)

This example illustrates how to create dynamic text content using Expressions.

### Modeling (How to use it in DIME)

![](./assets/text-expression-model.png)

::: v-pre
In the *Cinco Properties* view under `content` you can access variables in the data context via [Expressions](gui-docs-expressions) by using double mustaches `{{ ... }}` in the `rawContent` text area.

In the picture above, the expression `{{currentUser.baseUser.firstName}}` is used.
At runtime, this will be rendered as the value of the attribute `firstName` of the attribute `baseUser` of the variable `currentUser`.
:::

### Result (How it looks in the web app)

|![](./assets/text-expression-app.png)
|-

## Dynamic Text (Iteration Scope)

This example illustrates how to create dynamic text content in an iteration scope using Expressions.

### Modeling (How to use it in DIME)

![](./assets/text-for-model.png)

::: v-pre
To create a Text component for each element of a list, connect a list variable via a [FOR](gui-docs-for) edge with the Text component.
In the *Cinco Properties* view under `content` you can access the `current` variable in the data context via [Expressions](gui-docs-expressions) by using double mustaches `{{ ... }}` in the `rawContent` text area.

In the picture above, the expression `{{current.baseUser.firstName}}` is used.
At runtime, this will be rendered as the value of the attribute `firstName` of the attribute `baseUser` of the variable `current`.
:::

### Result (How it looks in the web app)

|![](./assets/text-for-app.png)
|-

