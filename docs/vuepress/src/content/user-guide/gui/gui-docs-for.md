# FOR

## Attributes

* *index* The name of the numeric index variable, which can be used in the inner scope.

## Description

The FOR edge can be connected to any component in your templates.
The source of the edge has to be a list of a complex type defined in a data model.
The iterator variable corresponds with the _current_  attribute of the list variable and can be expanded to get access to their attributes.
The iterator variable can be used in the connected component and in every of their inner components.


 