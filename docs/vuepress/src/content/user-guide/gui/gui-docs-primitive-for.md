# Primitive FOR

## Attributes

* *index* The name of the numeric index variable, which can be used in the inner scope.
* *iterator* The name of the iterator variable, which can be used in the inner scope. The type of the iterator variable depends on the List, which is iterated.

## Description

The primitive FOR edge can be connected to any component in your templates.
The source of the edge has to be a list of one of the primitive types: text, integer, real, timestamp or file.
The iterator variable can be used in the connected component and in every of their inner components.


 