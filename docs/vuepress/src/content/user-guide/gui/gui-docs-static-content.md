# Static Content

## Attributes

* *rawContent*: Text which can contain Expressions
* *alignment*: LEFT,RIGHT,CENTER
* *bold*
* *small*
* *color*
* *italic*
* *highlighted*: The background of the text is colored
* *deleted*: The text is striked
* *underline*
* *maxChars*: If greater than 0, the text is displayed in a tooltip if it reaches the defined max length
* *preIcon*: [Icon](gui-docs-icons)
* *postIcon*: [Icon](gui-docs-icons)

## Description

The Static Content is used in many Components.
It's basically used to display static textual content but can be extended with [Expressions](gui-docs-expressions)
to display variable and attribute values.
