# Tabbing

## Attributes

* *tabtype*:
 * *Tab* The tabs are displayed as horizontal tabs
 * *Pill* The tabs are displayed as horizontal blue pills
 * *StackedPills*: The tabs are displayed as vertical blue pills
* *justified*: If true, the tabs or pills are sized to fit the available horizontal space of the parent component

## Description

A tabbing can be used to categorize content in tabs.
When the user selects a tab by clicking it, the dependent content is shown and the rest is hidden.

The tabbing can be used in combination with the [major/minor](gui-major-minor) concept.