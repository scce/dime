# Description

## Attributes

* *inline*: If true, the List-Entries will be rendered in a horizontal row
* *mode*: 
 * Stacked: TheDescription-Entries will be displayed unordered, starting with the key above the value.
 * Listgroup: The Description-Entries will be displayed unordered, starting with the key on the left and the value on the right.

## Description

The Description can be used to display unordered key value content.
To add content to the Description use the Descriptionentry Components.