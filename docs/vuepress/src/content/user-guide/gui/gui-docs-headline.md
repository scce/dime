# Headline

## Attributes

* *size*: The size of the rendered content: _Big_, _Medium_, _Normal_ or _Small_
* *content*: [Static Content](gui-docs-static-content)

## Description

The Headline Component can be used to display important content.
The content can be extended with dynamic values by Expressions