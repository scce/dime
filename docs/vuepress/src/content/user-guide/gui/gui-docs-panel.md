# Panel

## Attributes

* *heading*: If at least one [Static Content](gui-docs-static-content) is added, a heading above the panel is rendered, displaying the content
* *footer*: If at least one [Static Content](gui-docs-static-content) is added, a footer under the panel is rendered, displaying the content
* *color*: The color of the panel border and footer and heading background.
* *collapsable*: If true, the panel is collapsable if it is clicked on the heading. (Should only be used if an heading is added!) The Panel is collapsed by default when the page is initially rendered

## Description

The panel is used to group content in visual separation to other content.

