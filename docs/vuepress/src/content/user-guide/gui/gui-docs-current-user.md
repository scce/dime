# Current User

The current logged in user is accessible in every GUI without any process or input variable.
Just drag a User Type from the Data Components View to one of your DataContexts and (re-)name the appearing variable to currentUser and set isInput to false.

The current user is available now.

