# File

## Attributes

* *label*: The displayed label of the Download Button.
* *size*: The size of the field and its content from XSmall to Large
* *coloring*: The Color of the File Download and its content.
* *disabled*: If true, no download is possible.

## Description

The File Component can be used to Download a File.
First the file name is displayed followed by a button with the specified label.
The file to download can be specified by a file guard process.
