# Listentry

## Attributes

* *content*: [StaticContent](gui-docs-static-content)
## Description

The Listentry Component can be used in Listing Components to render entries.
To use the Listentry to display variable values, use an _Expression_ in the _content_.
To display the values of a list variable connect the list variable to the Listentry Component using an Iteration edge.