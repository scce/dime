# Combobox

## Attributes

* *label*: The displayed label of Field. If the corresponding Form is inline, then the label is on the left, otherwise on top of the field.
* *size*: The size of the field and its content from XSmall to Large
* *coloring*: The Color of the Field and its content.
* *disabled*: If true, no content is displayed and the content can be changed
* *required*: If true, content has to be typed in. Otherwise the field is invalid.
* *min*: The content' length has to be equal or more than _min_. Otherwise the field is invalid.
* *max*: If greater than -1, the content' length has to be equal or less than _max_. Otherwise the field is invalid.
* *emptyValue*: The empty value is displayed, while the field is empty. The empty value can not not submitted.
* *errorText*: This text is displayed at the bottom of the field, if it is invalid.
* *readonly*: If true, the content can be changed
* *helptext*: If filled in, the helptext is displayed under the field.
* *choices*: If not empty the values of the _choices_ list will be used as the source of the selection.
* *inline*: If true, the choices will be rendered inline
* *multiple*: If true, multiple entries can be selected and the target variable has to be a list

## Description

The Combobox is one of the selection inputs.
It is used to select one or multiple entries (depending on the _multiple_ option) from a list (*source variable*) and write it to a *target variable*.
The source has to be a list of a complex or primitive variable and can be connected with a ComplexChoiceData for a complex list variable or with a PrimitiveChoiceData edge for a primitve list.
Depending on the source variable, the target variable has to be of the same complex or primitive type and depending on the _multiple_ option the variable has to be a list.

The target variable can be connect by a data binding (see chapter Data Bindings).

If the the _choices_ list attribute of the Combobox is not empty, no source is needed. The target variable has to be of the primitive type: _Text_.

To decide what is rendered in the Combobox, connect a primitive attribute of the expanded iterator of the source list variable with a _Display_ edge to the radio component.

## Data Bindings

The connection between a variable and the select is created *from the variable to the select*.
To connect a variable in the data context with a select two edges are provided:
* *FormSubmit*: The FormSubmit Edge only writes the selected value(s) of the select field to the variable.
* *FormLoadSubmit*: The FormLoadSubmit Edge selects the value(s) of the variable when the page with the field is initial rendered. The selected value(s) of the input field will be written to the variable on submission or synchronization.
