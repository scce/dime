# TableEntry

## Attributes

* *label*: The label of the column
* *sortable*: If true, the column label is used as a button to enable the sorting.
The currently sorted column is recognizable by a arrow showing the sorting direction.
The sorting functionality requires a TableColumnLoad edge.
* *fullTextSearch*: If true, a text input field is added above the column label.
The fullTextSearch functionality requires a TableColumnLoad edge.

## Description

A TableEntry represents a column in the a given table.

If the sorting or a full text search is enabled, a connection between the TableEntry and an attribute of the iterator variable is required.
The value of the attributed is used to be sort the rows lexicographic.
The same value is filtered by the full text search, whether the given search text is contained in the attribute value.

*The sorting and full text search is only capable for primitive text, integer or real attributes*!