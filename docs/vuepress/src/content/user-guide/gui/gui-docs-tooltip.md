# Tooltip

## Attributes

* *position* The position where the tooltip is rendered when it is visible depending on the component.
 * *TOP*: Above the Component
 * *RIGHT*: To the right of the Component
 * *LEFT*: To the left of the Component
 * *BOTTOM* Under the Component
* *content*: A List of [Static Content](gui-docs-static-content)
* *event*: The event which triggers if the tooltip shows up
 * *HOVER*: If the cursor is above the Component
 * *CLICK*: If the Component is clicked

## Description

The Tooltip can be added to all Components by Right-clicking the Component's entry in the left side of the _properties view_ and select _Add Tooltip_.
After this a new entry _Tooltip_ is displayed in the properties view.
To edit the attributes select the entry.

The Tooltip can be used to provide additional Information for the user which has not be always visible.
