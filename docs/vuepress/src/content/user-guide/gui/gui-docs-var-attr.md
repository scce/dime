# Variables and Attributes

To handle the Data in your GUI model add _DataContext_s.
You can add an arbitrary number of DataContexts to your model and move them to a desired position.

## Primitive Variables
To add a primitive variable add the a PrimitveVariable from the Palette and drag it to one of your DataContexts.

To change the name of the variable, select the Primitive Variable and change the name in the Properties View.
You can also change the isList option to decide whether you want to have a List of the primitive type in your DataContext.

## Complex Variables

To add a complex variable of a defined Type in one of your Data models, open the Data Component View: Window->Show View->Other..->Other..->Data Component.
Search for the desired Type and drag the entry to one of your DataContexts.

To change the name of the variable, select the Complex Variable and change the name in the Properties View.
You can also change the isList option to decide whether you want to have a List of the Type in your DataContext.

Show Attributes of a Complex Variable
To show the Attributes of a Complex variable double click the Variable in your DataContext.
The Variable will expand to display all available Attributes of the corresponding Type form the Data model.
If the Complex Variable is a List, List-Attributes will be displayed like: size: int, first: Type, last:Type and the iterator current:Type.

Outsource Complex Attributes
To access the attributes of complex attributes drag an complex attribute and drop it anywhere in the DataContext.
This leads to a new Complex Variable, equally named like the outsourced attribute and connected to the major variable with an AttributeConnectorEdge

Outsource Complex Iterator List Attributes
To access the attributes of complex Iterator attribute drag an it attribute and drop it anywhere in the DataContext.
This leads to a new Complex Variable, equally named like the outsourced Iterator attribute and connected to the major variable with an ListAttributeConnectorEdge 
The Iterator can be used in the Component and their containables , if the major List is connected with one of Iterator edges: FOR or PrimitiveFOR.
You are free to change the name of the Iterator variable as well, since it is necessary when loops are nested.