# Image

## Attributes

## Image

* *path*: The path to the file on the web server, an external source or an expression to access a variable of type string containing the path to the file as dynamic information.
* *dimensionX*:
 * if dimensionX <= -1 : the image width is not specified
 * if dimensionX = 0 : the image width is set to 100%
 * if dimensionX > 0 : the image width is set to dimensionX in px
* *dimensionY*:
 * if dimensionY <= -1 : the image height is not specified
 * if dimensionY = 0 : the image height is set to 100%
 * if dimensionY > 0 : the image height is set to dimensionY in px

## styling

* *border*:
 * Default: A normal rendered Image (as is) with no border
 * Thumbnail: A soft border with rounded corners
 * Circle: The image is rendered with a circle-shaped mask
 * Rounded: The corners of the image are rounded
* *responsive*: The image resizes dependent on the window and device size

## Description

The Image can be used to render an Image.
The Image source can be used to specify a static resource with a link or a dynamic resource with an expression.

Additionally, a primitive File attributes or variables can be connected with an image component using a <code class="java">Display</code> edge.


