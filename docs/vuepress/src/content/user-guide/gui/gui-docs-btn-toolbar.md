# ButtonToolbar

## Attributes

* *size*: define the size of all containing components
* *role*: 
 * *DEFAULT*: is just for grouping Button groups
 * *MENU*: Is rendered as a menu bar, all Button groups are added in order without any spacing
 * *BREADCRUMB*: Is rendered as a breadcrumb. All Buttons of all groups are rendered in order with a slash ("/") as separator

## Description

The ButtonToolbar is used to group ButtonGroups.