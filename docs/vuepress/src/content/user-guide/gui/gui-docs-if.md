# IF

## Attributes

* *inverted* Inverts the condition result. Can be used as _else_ path to a given condition.

## Description

The IF condition edge decides whether a component is displayed or not.
You can connect any variable or attribute in your dataContext to a component in one of your templates.
If multiple IF edges or connected to the same component their conditions are processed with the logical AND function.

The condition checks whether the value of the variable or attribute is true-ly or false-y dependent on the following schema:

* Text: if the string not null and not empty
* Integer: if the number is greater than 0
* Real: if the number is greater than 0.0
* Boolean: if the boolean is true
* File: if the file is existent. Has been upload.
* Timestamp: always true
* Object of any Type: if the object is not null
* List of any Type: if the List is not null and not empty 