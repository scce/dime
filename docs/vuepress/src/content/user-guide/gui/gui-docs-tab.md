# Tab

## Attributes

## Description

The tab component correlates to one tab in view.
The first top component placed in the tab has to be a button, which will be rendered as specified in the parent tabbing component.
The rest of the embedded components will be displayed, when the button is clicked.