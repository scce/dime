# Expressions

Expressions can be used to access variable and attribute values in text fields of a component's properties view.

## Example

A Text component is dragged to the canvas.
Select the component and look down at the properties view.
You can edit the content attribute with the following text:

::: v-pre
My name is {{currentUser.name}}
:::

The double brackets indicate an _Expression_ .
In this example a variable named _currentUser_, which has to be available in a data context of the current GUI model is accessed.
The _dot_ operator is used to access the attributes of the _currentUser_ variable, like the _name_ attribute.
It is not necessary that the name attribute of the currentUser variable is present in the GUI model, so you do not have to expand the variable's attributes to the same depth in the data context like the expression does.
But the attributes has to be correctly spelled depending on the data type and its associations in the data model

## Additional attributes accessable by Expressions

* Lists
 * _isEmpty_
 * _length_
 * _first_
 * _last_

* Complex Variable/Attribute
 * _dywa_id_
 * _dywa_name_
 * _dywa_version_

## Pipes

TODO
