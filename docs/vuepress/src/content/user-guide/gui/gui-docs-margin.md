# Margin

All Components can get an additional Margin to the Top and Bottom.
The Margin can be set to:
* *None*: no margin (default)
* *Single*: 15 px
* *Double*: 30 px
* *Triple* : 45px