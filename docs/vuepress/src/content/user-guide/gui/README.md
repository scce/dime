# Table of Contents

## The GUI Model

* *title* The title of the GUI corresponds to the label of the GUI-SIB, when the GUI is used in a parent Interaction Process
* *additionalStylesheet*: A List of additional Stylesheets you can use in the _Styling_ of your Components.
* *fullWidth* If true, the entire screen is used for the rendering
* *style* Additional styling [Styling](gui-docs-styling.md)

## Data Context

* [Variables and Attributes](gui-docs-var-attr.md)
* [Input](gui-docs-var-attr.md)
* [Current User](gui-docs-var-attr.md)

## Helper Components

* [Tooltip](gui-docs-tooltip.md)
* [Margin](gui-docs-margin.md)
* [ContainerHeight](gui-docs-container-height.md)
* [Styling](gui-docs-styling.md)
* [Static Content](gui-docs-static-content.md)
* [Icons](gui-docs-icons.md)
* [Expressions](gui-docs-expressions.md)

## Structural Directives

* Iteration
 * [Primitive FOR](gui-docs-primitive-for.md)
 * [FOR](gui-docs-for.md) 
 * [IF](gui-docs-if.md)

## Component

* Table
    * [Table](gui-docs-table.md)
    * [TableEntry](gui-docs-table-entry.md)
* Navigation
    * [Tabbing](gui-docs-tabbing.md)
    * [Tab](gui-docs-tab.md)
    * [TopNavBar](gui-docs-top-nav-bar.md)
    * [BottomNavBar](gui-docs-bottom-nav-bar.md)
* Form
    * [Form](gui-docs-form.md)
    * [Field](gui-docs-field.md)
    * [Radio](gui-docs-radio.md)
    * [Combobox](gui-docs-combobox.md)
    * [Checkbox](gui-docs-checkbox.md)
    * [Button](gui-docs-button.md)
* Interaction
    * [ButtonToolbar](gui-docs-btn-toolbar.md)
    * [ButtonGroup](gui-docs-btn-group.md)
    * [Dropdown](gui-docs-dropdown.md)
    * [Button](gui-docs-button.md)
* Grid
    * [Placeholder](gui-docs-placeholder.md)
    * [Row](gui-docs-row.md)
    * [Col](gui-docs-col.md)
* Data
    * [DataContext](gui-docs-data-context.md)
    * [PrimitveVariable](gui-docs-primitive-variable.md)
* Image
    * [Image](gui-docs-image.md)
    * [Thumbnail](gui-docs-thumbnail.md)
* Content
    * [Headline](gui-docs-headline.md)
    * [Text](gui-docs-text.md)
    * [File](gui-docs-file.md)
* Special
    * [TODOList](gui-docs-todolist.md)
* List
    * [Listing](gui-docs-listing.md)
    * [Listentry](gui-docs-listing.md)
    * [Description](gui-docs-listing.md)
    * [Descriptionentry](gui-docs-listing.md)
* Other
    * [Alert](gui-docs-alert.md)
    * [PageUp](gui-docs-page-up.md)
    * [ProgressBar](gui-docs-progress-bar.md)
    * [Jumbotron](gui-docs-jumbotron.md)
    * [Embedded](gui-docs-embedded.md)
    * [Panel](gui-docs-panel.md)
    * [Box](gui-docs-box.md)