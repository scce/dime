# Placeholder

## Attributes

* name: define a name for the placeholder

## Description

If a Placeholder is placed in a GUI Model it can be filled with Content from the parent Process (major/minor) or GUI model (second order) which embeds the corresponding GUISIB.
It is possible to include multiple named placeholders in GUI model.
If multiple placeholders are equally named, the given content inject from an upper layer gets replaces all placeholders.
