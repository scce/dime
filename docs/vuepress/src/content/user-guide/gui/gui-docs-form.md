# Form

## Attributes

* *inline*: If true, all input fields of the form are rendered inline
* *errorMessage*: The message will be displayed at the bottom of the form if at least one field is invalid

## Description

The Form is the container for further input fields like:
[Fields](gui-docs-fields)
[Radio](gui-docs-radio)
[Combobox](gui-docs-combobox)
[Checkbox](gui-docs-checkbox)

The values of the form fields can be submitted with a [Button](gui-docs-button) if the _buttontype_ is set to _submit_.
If no submit button is placed in the form, the values of the form fields are synchronized with their binded variables and attributes, when they are changed.
