# ProgressBar

## Attributes

* *color*: The Color of the progress bar.
* *percent*: If true, the numeric variable or attribute connected to the Progress Bar is interpreted as a percentage number between 0 and 100, else as a decimal value between 0 and 1.
* *striped*: If true, the progress bar is striped.
* *animate*: If true, the progress bar is striped and animated.

## Description

The Progress Bar Component can visualize a percentage or decimal numeric value.
A numeric variable or attribute can be connected to the Progress Bar by an _Display_ edge.