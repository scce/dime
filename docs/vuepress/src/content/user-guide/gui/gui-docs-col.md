# Col

## Attributes

* *wideness*: Describes how much space of the parent row is used: _Full_, _Half_, _Third_, _Quarter_, _TwoThird_, _ThreeQuarter_, _Sixth_ or _Twelfth_
* *size*: Describes the minimal screen size of the column. If the defined screen size is smaller, then the columns are vertically aligned.
* *alignment*: Describes how the content in the column is aligned.
* *offset*: Describes the offset of the column to the next component to the left: _None_, _Half_, _Third_, _Quarter_, _ThreeQuarter_ or _Twelfth_

## Description

The Column is used to align Components vertically in columns.
Columns can only be used in Rows.