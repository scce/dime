# Embedded

## Attributes

* *source*: A path to an external resource (e.g. YouTube video iframe link)

## Description

The Embedded Component can be used to display external resources.