# ButtonGroup

## Attributes

* *size*: define the size of all containing components
* *alignment*: 
 * *horizontal*: the buttons are horizontal aligned
 * *vertical*:  the buttons are vertical aligned
 * *justified*: the buttons are horizontal aligned and stretched to use the entire horizontal space

## Description

The ButtonGroup is used to group Buttons.