# Headless Generation

This section describes the headless generator function of DIME.
With the DIME Headless Generator it is possible to generate a running DIME Applications out of the shell.

## Setup

1. First, you'll need a DIME installer with the Headless generator functions included
2. Decompress the installer as always.
3. Create a new or checkout an existing DIME Application Project
4. Run the DIME Headless Generator with this project (see below).

## Execution

The headless execution is run from the directory of DIME.
The command will look like this:

    ./dime -data <workspacePath> -application info.scce.dime.headless.application (--importProject <projectPath>) --model <modelpath>

### The Command in detail

- `dime`: Executable file of DIME
- `-data`: Tells DIME which workspace to use (a new one is created, if the given one does not exist.
- `-application info.scce.dime.headless.application`: Tells DIME that the DIME Headless Generator application is used.
- `--importProject <projectPath>` : Absolute path to the DIME Application which is to be generated.
  - Imports the given project into the workspace (*Optional*)
  - Should be omitted if project already exists in given workspace
- `--model` : Workspace relative path to the DAD Model for the generated DIME Application
  - Usually Something like:  

          NewApp/dime-models/app.dad

:warning: Notice the double `-` hyphens in front of `--importProject` and `--model`. These are important.

## Short DIME Headless Command

DIME also provides a shortened command for the DIME Headless Generator:

    ./dime-headless.sh <projectPath> (<modelName>)

### The Command in Detail

- `<projectPath>` : Absolute path to the DIME Application which is to be generated.
- `<modelName>` : *Optional* Filename for the DAD Model
  
   - Can be omited if filename is `app.dad`

## Common Pitfalls

1. The long DIME Headless command shows the DIME splash screen before starting generation

  - **Workaround:** Add the `-nosplash` parameter to the command line

2. Newly Created App does not work properly:

  - Output looks like this without showing errors:  
  
         Reading Parameters
         Setting Up Workspace
         [ReferenceRegistry] Initializing on startup...
         [ReferenceRegistry] Initialized. This took 10732ms of your lifetime.
         Loading DAD Model
         Success.

  - **Workaround:** Change the Entry `<name>ProjectName</name>` of the `<projectDescription>` in the hidden `.project` file to the name of the project directory.
