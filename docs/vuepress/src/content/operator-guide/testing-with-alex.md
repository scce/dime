# Testing with ALEX

This walk-through guides you how to write, manage and execute UI tests for DIME apps with ALEX.
For this to work, deploy your DIME app either locally or remotely. 
In the following, we assume the app is available in a web browser under `<APP_URL>` and the repository is located at `<PROJECT_DIR>`.


## Configuration

All tests should be run with the following browser configuration:

* Chrome v75
* Chromedriver v75
* Browser Resolution 1920x1080px

Further, each DIME app should have have have a **test** directory under `<PROJECT_DIR>` with the following contents:

* **files/** - contains files that uploaded in the tests
* **symbols.json** - contains the input symbols exported by ALEX
* **tests.json** - contains the modeled UI tests exported by ALEX
* **tests.config.json** with the following content
  ```json
  {
    "driverConfig": {
      "name": "remote",
      "headless": true,
      "browser": "chrome",
      "platform": "ANY",
      "width": 1920,
      "height": 1080,
      "implicitlyWait": 0,
      "pageLoadTimeout": 20,
      "scriptTimeout": 20 
    }
  }
  ```


## With Docker

*Make sure that `docker` and `docker-compose` are installed on the machine*

Download the archive (https://ls5download.cs.tu-dortmund.de/alex/docker/alex-chrome-docker-environment.zip) that contains a *docker-compose* file and a *run-tests* script an unzip them.
Navigate into the folder with the unzipped files and perform the following actions:

1. Execute `docker-compose pull`
2. Execute `docker-compose up`
    * ALEX is available at `127.0.0.1:8000`
    * The admin account is email: *admin@alex.example*, password: *admin* 
3. Execute `./run-tests <PATH> <URL>` where
    * `<PATH>` is the path to the `test` directory of the DIME app
    * `<URL>` is the URL where the DIME app is deployed

```bash
# e.g.
sudo ./run-tests /home/.../static-input-testapp/test http://127.0.0.1:9090
```
    
The result of the text execution is printed to the terminal.
The test report is also available in ALEX. 
Open ALEX in a webbrowser, login with the account from above and open the newest project.
Then, navigate to *Testing > Reports*.

### Udating the images

* Update the ALEX CLI with `docker pull learnlib/alex-cli:unstable`
* Update ALEX with `docker-compose pull` in the directory where the docker-compose file and the run-test script are located 

### VNC

You can debug your tests with a VNC viewer that connects to the Chrome instance inside the Docker container.
Install a VNC viewer and establish a connection to `localhost:5900` with the password `secret`.


## Without Docker

### Setting up the testing machine

1. Install Chrome in the version as defined in the test configuration
2. Download the corresponding chromedriver executable from [here][chromedriver]. 
   In the following, we will refer to the absolute path to the executable with `<CHROMEDRIVER_PATH>`
3. Install JRE8 or JRE11


### Setting up ALEX

1. Download the latest nightly build from version 1.8.0 available from [here][alex-nightly]
2. Run ALEX via `java -jar alex-1.8.0-SNAPSHOT.war`
    * Add the argument `--server.port=....` to start ALEX under a custom port
3. Open `http://localhost:8000` (or the custom port) and login as admin:
    * email: admin@alex.example
    * password: admin
4. Navigate to *Admin > Settings* and enter the `<CHROMEDRIVER_PATH>` into the corresponding input field
5. Mark the Chrome browser as default and click the `Save` button


### Creating a project for the DIME app

1. Navigate to *Projects > Overview*
1. Create a new project with the URL of the deployed DIME app without a trailing slash
    * e.g. `http://localhost:8080` but not `http://localhost:8080/`


### Executing existing tests

1. Import symbols
    1. Navigate to *Symbols > Manage*
    2. Click on the import-button in the action bar
       In the dialog, select the `<PROJECT_DIR>/test/symbols.json` file and click on import
2. Import tests
    1. Navigate to *Testing > Manage*
    2. Click on the import-button in the action bar
       In the dialog, select the `<PROJECT_DIR>/test/tests.json` file and click on import
3. Upload test files
    1. Navigate to *Project > Files*
    2. Upload all files from the `<PROJECT_DIR>/test/files` directory
4. Execute tests
    1. Navigate to *Testing > Manage*
    2. Select all test suites and click the *Execute*-button



## Testing workflow

### Creating symbols and tests

1. Open the project for the DIME app and start creating symbols and tests. 
   The following guides might help in doing so:
    * [Symbol management][alex-docs-symbols]
    * [Test management][alex-docs-tests]

### Pushing tests to the repository

As soon as all modeled tests pass, add them to the repository of the DIME app project.
Therefor, proceed according to the following steps:

1. Export symbols
    1. Navigate to *Symbols > Manage*
    2. Select all symbols and symbol groups by clicking on the checkbox in the action bar
    3. Click the export-button in the action bar
    4. In the dialog, make sure the *Export symbols only* checkbox is **not** selected
    5. For the filename, enter `symbols.json`
2. Export tests
    1. Navigate to *Testing > Manage*
    2. Select all test suites and test cases by clicking on the checkbox in the action bar
    3. Click the export-button in the action bar and select *Export for ALEX*
    4. In the dialog, enter `tests.json` for the name of the exported JSON file 
3. Add both JSON files to the repository in the intended *test* directory


## Best practices

### Modeling DIME test apps

- Create new app with template "New -> New DIME APP -> Initialize project with example app -> Test-App"
- Add DIME attributes to elements to interact with during testing for shorter selectors
   1. Select an element
   2. In the Cinco Properties view on the left side click on "General Style"
   3. Right-click on "Attributes" and enter a value
      - e.g. "my-elem", then the element can be accessed with e.g. ```div[dime~="my-elem"]```
- Write the title of the button in the panel body of each test
- Add a description of what the test does in the panel body

### Testing DIME test apps

- File uploads
   - only upload small files and wait for 3 seconds before executing the next action
- Reference elements via DIME attributes
- Use explicit waits to wait for content to be rendered
- Enter descriptive descriptions for failed actions
- Encapsulate shared logic in own symbols

[alex-nightly]: https://ls5download.cs.tu-dortmund.de/alex/builds/nightly/
[chromedriver]: http://chromedriver.chromium.org/downloads
[alex-docs-symbols]: https://learnlib.github.io/alex/book/2.0.0/contents/user-manual/symbol-management/
[alex-docs-tests]: https://learnlib.github.io/alex/book/2.0.0/contents/user-manual/testing/
