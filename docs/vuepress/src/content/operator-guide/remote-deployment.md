# Remote Deployment

The standard solution of deploying DIME apps remotely is based on container virtualization.

## Short Technology Overview

* **[GitLab CI](https://docs.gitlab.com/ce/ci/)**: Generates, builds, packages and delivers DIME apps as container images.
* **[GitLab Container (Image) Registry](https://docs.gitlab.com/ce/user/packages/container_registry/)**: Stores the delivered container images.
* **[Kubernetes](https://kubernetes.io)**: Orchestrates container deployment, scaling and management.
* **[Helm](https://helm.sh)**: Generates infrastructure configuration of DIME apps.

## Directory Layout

This directory layout is typically for DIME apps.
It holds the three major components that are needed during deployment.
The applications source code (`app`), the CI/CD pipeline configuration (`.gitlab-ci.yml`), and the Helm chart for generating and triggering the deployment (`ops/helm`).

```
/project
  ├── app
  │    └── ...
  ├── ops
  │    └── helm
  └── .gitlab-ci.yml
```

## Kubernetes

Kubernetes is a "production-grade container orchestration" [[https://kubernetes.io](https://kubernetes.io)] and the default way to deploy DIME applications.

**Create Docker Registry Secrets**
```sh
USER@LOCAL-HOST:~$ kubectl create secret docker-registry <secret-name> --docker-server=<server> --docker-username=<username> --docker-password=<password> --dry-run=client -o yaml
```


**Print Kubernetes `client.config`**
```sh
USER@REMOTE-HOST:~$ cat /var/snap/microk8s/current/credentials/client.config
```

**Forward Kubernetes API Port**
```sh
USER@LOCAL-HOST:~$ ssh -N -L localhost:16443:localhost:16443 REMOTE-HOST
```

## Helm Chart

Helm is "the package manager for Kubernetes" [[https://helm.sh](https://helm.sh)].
It enables us to define infrastructure as code for DIME applications and provide them as packaged charts.

**Chart Repository**

The packaged helm charts are stored and shared via [GitLab's Package Registry](https://gitlab.com/scce/dime/-/packages).
For an introduction how to use chart repositories, take a look at the official [Helm Docs](https://helm.sh/docs/topics/chart_repository/).

**Sever Tiers**

| Server Tier   | Mailcatcher   | Let's Encrypt |
| -             | -             | -             |
| local         | enabled       | disabled      |
| testing       | enabled       | staging       |
| demo          | enabled       | production    |
| production    | disabled      | production    |

**Perform Deployment**

```sh
USER@LOCAL-HOST:/project$ cd ops/helm
USER@LOCAL-HOST:/project/ops/helm$ helm upgrade APP-NAME .
```



