# DIME Wiki

## Local Development

* Requires Nods.js >= v10
* Execute `npm install` once in this directory to install all dependencies

### Development Environment

1. `npm run dev`
2. `open http://localhost:8080`
3. Edit Markdown files in `src`
4. Edit menu structure in `src/.vuepress/config.js`

### Build Production

*Since the Wiki is build in the CI Pipeline and deployed automatically, there is no need to manually deploy the Wiki.*
*However, for testing purposes, you can proceed according to the following steps:*

1. `npm run build`
2. The generated files can be found at `src/.vuepress/dist

## Style Guide

- One sentence per line for nice Git diffs