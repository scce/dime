import helm_test


class TestIssuer(helm_test.HelmTest):
    template = "issuer.yml"

    def test_local(self):
        actual = self.execute_helm(server_tier="local")
        expected = "# Source: DIME/templates/issuer.yml"
        self.assertNotIn(expected, actual)

    def test_staging(self):
        actual = self.execute_helm(self.template, server_tier="staging")
        expected = """---
# Source: DIME/templates/issuer.yml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-staging
  namespace: default
spec:
  acme:
    # The ACME server URL
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: foo@example.com
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-staging
    # Enable the HTTP-01 challenge provider
    solvers:
      - http01:
          ingress:
            class: nginx
"""
        self.assertEqual(expected, actual)

    def test_demo(self):
        actual = self.execute_helm(self.template, server_tier="demo")
        expected = """---
# Source: DIME/templates/issuer.yml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-production
  namespace: default
spec:
  acme:
    # The ACME server URL
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: foo@example.com
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-production
    # Enable the HTTP-01 challenge provider
    solvers:
      - http01:
          ingress:
            class: nginx
"""
        self.assertEqual(expected, actual)

    def test_production(self):
        actual = self.execute_helm(self.template, server_tier="production")
        expected = """---
# Source: DIME/templates/issuer.yml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-production
  namespace: default
spec:
  acme:
    # The ACME server URL
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: foo@example.com
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-production
    # Enable the HTTP-01 challenge provider
    solvers:
      - http01:
          ingress:
            class: nginx
"""
        self.assertEqual(expected, actual)
