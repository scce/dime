import helm_test


class TestDywaApp(helm_test.HelmTest):
    template = "wildfly.yml"

    def test_with_app_config(self):
        expected = """---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: dywa-app
  labels:
    app: dywa-app
data:
  CLIENT_MAX_BODY_SIZE: "100000000"
  DATASOURCE_DATABASE_NAME: "dywa"
  DATASOURCE_SERVER_NAME: "postgres"
  DIME_APP_ENCRYPT_DISABLE: "false"
  DIME_APP_ENCRYPT_PASSPHRASE: "ohHeipe3fuzi1eSh"
  DIME_APP_MAIL_PORT: "1234"
  DIME_APP_MAIL_SERVER: "yourmailserverhere.example"
  DIME_APP_SERVER_TIER: "production"
  DIME_APP_SERVER_URL: "https://app.example.com/"
  DIME_APP_CONFIG: "{\\"email\\":\\"test@example.org\\"}"
  JAVA_OPTS: "-Xms64m -Xmx2g -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=512m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: dywa-app-pv-claim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: dywa-app
  name: dywa-app
spec:
  ports:
    - port: 8080
      protocol: TCP
      targetPort: 8080
  selector:
    app: dywa-app
  type: NodePort
---
# Source: DIME/templates/wildfly.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dywa-app
  labels:
    app: dywa-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: dywa-app
  template:
    metadata:
      labels:
        app: dywa-app
    spec:
      initContainers:
        - name: test-database-connection
          image: postgres:14.4
          command: [
              "sh",
              "-c",
              "until pg_isready -h $DATASOURCE_SERVER_NAME -p 5432; do echo waiting for database; sleep 2; done;"
          ]
          envFrom:
            - configMapRef:
                name: dywa-app
      containers:
        - name: dywa-app
          image: "registry.example.com/wildfly:aewai0iera3Yong0veighee8oolahrag"
          env:
            - name: DATASOURCE_USER
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: user
            - name: DATASOURCE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: password
            - name: DIME_APP_AUTH_PRIVATE_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: private
            - name: DIME_APP_AUTH_PUBLIC_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: public 
          envFrom:
            - configMapRef:
                name: dywa-app
          volumeMounts:
            - mountPath: /opt/jboss/wildfly/standalone/data/files
              name: dywa-app-volume
          ports:
            - containerPort: 8080
          resources:
            limits:
              cpu: "1000m"
              memory: "2Gi"
      restartPolicy: Always
      imagePullSecrets:
        - name: registry
      volumes:
        - name: dywa-app-volume
          persistentVolumeClaim:
            claimName: dywa-app-pv-claim
"""
        actual = self.execute_helm(
            template=self.template,
            values=[
                "appVersion=aewai0iera3Yong0veighee8oolahrag",
                "encrypt.passphrase=ohHeipe3fuzi1eSh"
            ],
            value_file="test.yaml"
        )
        self.assertEqual(expected, actual)

    def test_without_app_config(self):
        expected = """---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: dywa-app
  labels:
    app: dywa-app
data:
  CLIENT_MAX_BODY_SIZE: "100000000"
  DATASOURCE_DATABASE_NAME: "dywa"
  DATASOURCE_SERVER_NAME: "postgres"
  DIME_APP_ENCRYPT_DISABLE: "false"
  DIME_APP_ENCRYPT_PASSPHRASE: "ohHeipe3fuzi1eSh"
  DIME_APP_MAIL_PORT: "1234"
  DIME_APP_MAIL_SERVER: "yourmailserverhere.example"
  DIME_APP_SERVER_TIER: "production"
  DIME_APP_SERVER_URL: "https://app.example.com/"
  DIME_APP_CONFIG: "{}"
  JAVA_OPTS: "-Xms64m -Xmx2g -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=512m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: dywa-app-pv-claim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: dywa-app
  name: dywa-app
spec:
  ports:
    - port: 8080
      protocol: TCP
      targetPort: 8080
  selector:
    app: dywa-app
  type: NodePort
---
# Source: DIME/templates/wildfly.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dywa-app
  labels:
    app: dywa-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: dywa-app
  template:
    metadata:
      labels:
        app: dywa-app
    spec:
      initContainers:
        - name: test-database-connection
          image: postgres:14.4
          command: [
              "sh",
              "-c",
              "until pg_isready -h $DATASOURCE_SERVER_NAME -p 5432; do echo waiting for database; sleep 2; done;"
          ]
          envFrom:
            - configMapRef:
                name: dywa-app
      containers:
        - name: dywa-app
          image: "registry.example.com/wildfly:aewai0iera3Yong0veighee8oolahrag"
          env:
            - name: DATASOURCE_USER
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: user
            - name: DATASOURCE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: password
            - name: DIME_APP_AUTH_PRIVATE_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: private
            - name: DIME_APP_AUTH_PUBLIC_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: public 
          envFrom:
            - configMapRef:
                name: dywa-app
          volumeMounts:
            - mountPath: /opt/jboss/wildfly/standalone/data/files
              name: dywa-app-volume
          ports:
            - containerPort: 8080
          resources:
            limits:
              cpu: "1000m"
              memory: "2Gi"
      restartPolicy: Always
      imagePullSecrets:
        - name: registry
      volumes:
        - name: dywa-app-volume
          persistentVolumeClaim:
            claimName: dywa-app-pv-claim
"""
        actual = self.execute_helm(
            template=self.template,
            values=[
                "appVersion=aewai0iera3Yong0veighee8oolahrag",
                "encrypt.passphrase=ohHeipe3fuzi1eSh"
            ]
        )
        self.assertEqual(expected, actual)
