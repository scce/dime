import helm_test


class TestIngress(helm_test.HelmTest):
    template = "ingress.yml"

    def test_local(self):
        actual = self.execute_helm(self.template, server_tier="local")
        expected = """---
# Source: DIME/templates/ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: http-ingress
  annotations:
    acme.cert-manager.io/http01-edit-in-place: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: "100000000"
spec:
  rules:
    - host: app.example.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: dywa-app
                port:
                  number: 8080
          - path: /mailcatcher
            pathType: Prefix
            backend:
              service:
                name: mailcatcher
                port:
                  number: 1080
"""
        self.assertEqual(expected, actual)

    def test_staging(self):
        actual = self.execute_helm(self.template, server_tier="staging")
        expected = """---
# Source: DIME/templates/ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: http-ingress
  annotations:
    acme.cert-manager.io/http01-edit-in-place: "true"
    cert-manager.io/issuer: "letsencrypt-staging"
    nginx.ingress.kubernetes.io/proxy-body-size: "100000000"
spec:
  tls:
    - hosts:
        - app.example.com
      secretName: "letsencrypt-secret-staging"
  rules:
    - host: app.example.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: dywa-app
                port:
                  number: 8080
          - path: /mailcatcher
            pathType: Prefix
            backend:
              service:
                name: mailcatcher
                port:
                  number: 1080
"""
        self.assertEqual(expected, actual)

    def test_demo(self):
        actual = self.execute_helm(self.template, server_tier="demo")
        expected = """---
# Source: DIME/templates/ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: http-ingress
  annotations:
    acme.cert-manager.io/http01-edit-in-place: "true"
    cert-manager.io/issuer: "letsencrypt-production"
    nginx.ingress.kubernetes.io/proxy-body-size: "100000000"
spec:
  tls:
    - hosts:
        - app.example.com
      secretName: "letsencrypt-secret-production"
  rules:
    - host: app.example.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: dywa-app
                port:
                  number: 8080
          - path: /mailcatcher
            pathType: Prefix
            backend:
              service:
                name: mailcatcher
                port:
                  number: 1080
"""
        self.assertEqual(expected, actual)

    def test_production(self):
        actual = self.execute_helm(self.template, server_tier="production")
        expected = """---
# Source: DIME/templates/ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: http-ingress
  annotations:
    acme.cert-manager.io/http01-edit-in-place: "true"
    cert-manager.io/issuer: "letsencrypt-production"
    nginx.ingress.kubernetes.io/proxy-body-size: "100000000"
spec:
  tls:
    - hosts:
        - app.example.com
      secretName: "letsencrypt-secret-production"
  rules:
    - host: app.example.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: dywa-app
                port:
                  number: 8080
"""
        self.assertEqual(expected, actual)
