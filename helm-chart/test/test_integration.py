import helm_test


class TestIntegration(helm_test.HelmTest):

    def test(self):
        self.maxDiff = None
        expected = """---
# Source: DIME/templates/database-credentials.yml
apiVersion: v1
kind: Secret
metadata:
  name: database-credentials
type: Opaque
data:
  user: c2E=
  password: c2E=
---
# Source: DIME/templates/dime-auth-key.yml
apiVersion: v1
kind: Secret
metadata:
  name: dime-auth-key
type: Opaque
stringData:
  private: "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDOyw1U7Wx9ZVwb9rVE7wuSgPCa5EUEhe7kqjq7U93yucNzhpth8svz5DgK62iUqEpOcLMO8L+UzZ5XfFHj6+NbYhSTN8LhhhCSuUg6+rXnBPURkwkgFVXVyTLmlF8e+KYCEo4WsqmFxi+wgDJ4L+kXyyO10tn0sDW0PVgm0lGO83PX7DRNufUeZe9aGIYAj2IawMGoq9AhdYHrIP9yy/+5VYJyOUpU07Ju7nudajpOGc9hb4nYYu/Y2ruHdNMZ6ctbQN75kkQ7dH/umRSVUGlN/Nv1+65/OZFuKBf4CtzbBrcUtRZ9A3FnLbyG45lAzXv10GwuL2+QpXmLIHF2PYQfAgMBAAECggEBAIChsdNINJnzuM30MmGEyJADAKd4oq3jmexHL21jB8Z6YuezrWfKHa/CIFI6iL0fycjtNZRvkNxA4uPMgyvhiXVIlz/UCbc7K4f5FMMLPKdNYBvkeGKqos/u7WiclmaviKP1YskfrW93DiceN0anlrikNbxeQhGAoodROUo8vVrLWVUAtgnN/hb8M7bmgriOrinPGzKbNjS1N4KXiUiXjB4eOQjotxIxndD52UZQh50jFvLjaCcqw7+0AaWglz5xh+h3mUvet7996dRFZ0ag80eOCbLr6vwKhAfmHCl/MkUkAeC9fOrNJYT0LoOdS70nCjaenusSJRDZwu68RXXw50ECgYEA/znktRLiNl0ZA/G16G+G424Kf9IeQEfYWk8O3sMtUBFK9OFm7OFRKZXdrdHZsITlgNw7RyytNNvyPkkbQzEDfmwNm5TNmlj6CSMgbzWD+JAwMj22xh3ll4COvO/eeIWIIxf+9Jf9ySgfpB09yCB/bpZZwp2CPqjk50qORl909m8CgYEAz2uQo9menNnTyp7d6hJ80PYaAsdaejXnlLPaFomCWPXbj4sCHcgOWMEA9v1AS86WgkDD6Mdl1XKkbvc5V3LJDBoxyGB10UuHLbKLpr408s3kd8b6jmERQoTz2KeFBpNF1ynELRIcOX8ChNPTqb/X2KmyiU1AItuG0vsPKLn6pVECgYEArzVRGjUycduLVrfSBxS45twd/Q2DkuE/Vw+6x5X5P2P/rwglniw3iXLGUZMj/BffbYzCbOPwq77qF3QccQ3uthT7anjGhFTcoPUqSO3WAQcK7xKKrIrNmCm6011fviD9CgppDgRhRnxy7DjetsoIcSRpzj5OsTFEorr93LWfF6UCgYAzHegkaSIst4X5XlOENxApkyzv2F9U1OoAfJ5XhYXpFJYKdxCLdF0MxGvPcrQguoXfDcT6HgHbq4gYjLbF9VTUtbPlFNiUPs/OlDzUV/XGjLUsS449/m/5e7h4gThIWY0RrIpbLSZliQZ+45E7OTshexizu6T9sio03ohq2gKKMQKBgQDljG/GI+HZsEZ5BqA4RDx3+WscrpEL4keMOnr7i4eoGG7bwb0jcdQjczDJh6Nn+fSZPVOJ90zb8NGpZPSktuotDETXDWRJgeWuEbs298hjeWc9nwrgCBTPaS5ZaqF8SA4Svwr2MfeEki4GtkCmBtdSGgHvOgkzovK/zRJU6WtTFw=="
  public: "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzssNVO1sfWVcG/a1RO8LkoDwmuRFBIXu5Ko6u1Pd8rnDc4abYfLL8+Q4CutolKhKTnCzDvC/lM2eV3xR4+vjW2IUkzfC4YYQkrlIOvq15wT1EZMJIBVV1cky5pRfHvimAhKOFrKphcYvsIAyeC/pF8sjtdLZ9LA1tD1YJtJRjvNz1+w0Tbn1HmXvWhiGAI9iGsDBqKvQIXWB6yD/csv/uVWCcjlKVNOybu57nWo6ThnPYW+J2GLv2Nq7h3TTGenLW0De+ZJEO3R/7pkUlVBpTfzb9fuufzmRbigX+Arc2wa3FLUWfQNxZy28huOZQM179dBsLi9vkKV5iyBxdj2EHwIDAQAB"
---
# Source: DIME/templates/docker-registry-secret.yml
apiVersion: v1
data:
  .dockerconfigjson: 
kind: Secret
metadata:
  name: registry
type: kubernetes.io/dockerconfigjson
---
# Source: DIME/templates/postgres.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgres
  labels:
    app: postgres
data:
  POSTGRES_DB: "dywa"
  PGDATA: /var/lib/postgresql/data/pgdata
---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: dywa-app
  labels:
    app: dywa-app
data:
  CLIENT_MAX_BODY_SIZE: "100000000"
  DATASOURCE_DATABASE_NAME: "dywa"
  DATASOURCE_SERVER_NAME: "postgres"
  DIME_APP_ENCRYPT_DISABLE: "false"
  DIME_APP_ENCRYPT_PASSPHRASE: "ohHeipe3fuzi1eSh"
  DIME_APP_MAIL_PORT: "1234"
  DIME_APP_MAIL_SERVER: "yourmailserverhere.example"
  DIME_APP_SERVER_TIER: "production"
  DIME_APP_SERVER_URL: "https://app.example.com/"
  DIME_APP_CONFIG: "{\\"email\\":\\"test@example.org\\"}"
  JAVA_OPTS: "-Xms64m -Xmx2g -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=512m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
---
# Source: DIME/templates/postgres.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: postgres-pv-claim
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: "local-storage"
  resources:
    requests:
      storage: "1Gi"
---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: dywa-app-pv-claim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
---
# Source: DIME/templates/postgres.yml
kind: Service
apiVersion: v1
metadata:
  name: postgres
spec:
  selector:
    app: postgres
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
---
# Source: DIME/templates/wildfly.yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: dywa-app
  name: dywa-app
spec:
  ports:
    - port: 8080
      protocol: TCP
      targetPort: 8080
  selector:
    app: dywa-app
  type: NodePort
---
# Source: DIME/templates/postgres.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgres
spec:
  replicas: 1
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      containers:
        - name: postgres
          image: postgres:14.4
          args: [
            "-c", "max_connections=300",
            "-c", "max_prepared_transactions=200"
          ]
          imagePullPolicy: IfNotPresent
          ports:
            - containerPort: 5432
          env:
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: user
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: password
          envFrom:
            - configMapRef:
                name: postgres
          volumeMounts:
            - mountPath: /var/lib/postgresql/data
              name: postgres-volume
          resources:
            limits:
              cpu: "1000m"
              memory: "1Gi"
      restartPolicy: Always
      volumes:
        - name: postgres-volume
          persistentVolumeClaim:
            claimName: postgres-pv-claim
---
# Source: DIME/templates/wildfly.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dywa-app
  labels:
    app: dywa-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: dywa-app
  template:
    metadata:
      labels:
        app: dywa-app
    spec:
      initContainers:
        - name: test-database-connection
          image: postgres:14.4
          command: [
              "sh",
              "-c",
              "until pg_isready -h $DATASOURCE_SERVER_NAME -p 5432; do echo waiting for database; sleep 2; done;"
          ]
          envFrom:
            - configMapRef:
                name: dywa-app
      containers:
        - name: dywa-app
          image: "registry.example.com/wildfly:"
          env:
            - name: DATASOURCE_USER
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: user
            - name: DATASOURCE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: password
            - name: DIME_APP_AUTH_PRIVATE_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: private
            - name: DIME_APP_AUTH_PUBLIC_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: public 
          envFrom:
            - configMapRef:
                name: dywa-app
          volumeMounts:
            - mountPath: /opt/jboss/wildfly/standalone/data/files
              name: dywa-app-volume
          ports:
            - containerPort: 8080
          resources:
            limits:
              cpu: "1000m"
              memory: "2Gi"
      restartPolicy: Always
      imagePullSecrets:
        - name: registry
      volumes:
        - name: dywa-app-volume
          persistentVolumeClaim:
            claimName: dywa-app-pv-claim
---
# Source: DIME/templates/ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: http-ingress
  annotations:
    acme.cert-manager.io/http01-edit-in-place: "true"
    cert-manager.io/issuer: "letsencrypt-production"
    nginx.ingress.kubernetes.io/proxy-body-size: "100000000"
spec:
  tls:
    - hosts:
        - app.example.com
      secretName: "letsencrypt-secret-production"
  rules:
    - host: app.example.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: dywa-app
                port:
                  number: 8080
---
# Source: DIME/templates/issuer.yml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-production
  namespace: default
spec:
  acme:
    # The ACME server URL
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: foo@example.com
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-production
    # Enable the HTTP-01 challenge provider
    solvers:
      - http01:
          ingress:
            class: nginx
"""
        actual = self.execute_helm(
            values=[
                "encrypt.passphrase=ohHeipe3fuzi1eSh"
            ],
            value_file="test.yaml"
        )
        self.assertEqual(expected, actual)
