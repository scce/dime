import subprocess
import unittest


class HelmTest(unittest.TestCase):

    @staticmethod
    def execute_helm(template=None, values=None, server_tier=None, value_file=None):
        """
        Executes Helm to render chart templates.
        :param template: Filename of template that should be rendered (e.g. issuer.yml).
        :param values: List of key-value string (e.g. serverTier=local).
        :param server_tier: Parameter to conveniently set the server tier (e.g. local).
        :param value_file: Filename of additional values file (e.g. test.yaml).
        :return: rendered chat templates
        """
        command = HelmTest.__create_command(
            template=template,
            values=values,
            server_tier=server_tier,
            value_file=value_file
        )
        output = subprocess.check_output(command, cwd=r"../")
        return HelmTest.__prepare_output(output)

    @staticmethod
    def __create_command(template=None, values=None, server_tier=None, value_file=None):
        command = [
            "helm",
            "--namespace",
            "default",
            "template",
            "--values",
            "values.yaml",
        ]
        if value_file is not None:
            command += [
                "--values", value_file,
            ]
        if values is None:
            values = []
        if server_tier:
            values += ["serverTier=" + server_tier]
        if values:
            command += ["--set", ",".join(values)]
        if template:
            command += ["--show-only", "templates/" + template]
        command.append(".")
        return command

    @staticmethod
    def __prepare_output(output):
        return output.replace(b"\r\n", b"\n").decode("utf8")

    def test__create_command(self):
        # empty
        self.assertEqual(
            ["helm",
             "--namespace",
             "default",
             "template",
             "--values",
             "values.yaml",
             "."],
            HelmTest.__create_command()
        )
        # template
        self.assertEqual(
            ["helm",
             "--namespace",
             "default",
             "template",
             "--values",
             "values.yaml",
             "--show-only",
             "templates/foo.yaml",
             "."],
            HelmTest.__create_command(template="foo.yaml")
        )
        # values
        self.assertEqual(
            ["helm",
             "--namespace",
             "default",
             "template",
             "--values",
             "values.yaml",
             "--set",
             "foo=bar",
             "."],
            HelmTest.__create_command(values=["foo=bar"])
        )
        # server_tier
        self.assertEqual(
            ["helm",
             "--namespace",
             "default",
             "template",
             "--values",
             "values.yaml",
             "--set",
             "serverTier=demo",
             "."],
            HelmTest.__create_command(server_tier="demo")
        )
        # value_file
        self.assertEqual(
            ["helm",
             "--namespace",
             "default",
             "template",
             "--values",
             "values.yaml",
             "--values",
             "test.yaml",
             "."],
            HelmTest.__create_command(value_file="test.yaml")
        )
        # complete
        self.assertEqual(
            ["helm",
             "--namespace",
             "default",
             "template",
             "--values",
             "values.yaml",
             "--values",
             "test.yaml",
             "--set",
             "foo=bar,serverTier=demo",
             "--show-only",
             "templates/foo.yaml",
             "."],
            HelmTest.__create_command(
                template="foo.yaml",
                values=["foo=bar"],
                server_tier="demo",
                value_file="test.yaml"
            )
        )
