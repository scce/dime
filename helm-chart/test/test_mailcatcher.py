import helm_test


class TestMailcatcher(helm_test.HelmTest):
    template = "mailcatcher.yml"

    expected = """---
# Source: DIME/templates/mailcatcher.yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: mailcatcher
  name: mailcatcher
spec:
  ports:
    - port: 1080
      name: http
      protocol: TCP
      targetPort: 1080
    - port: 1025
      name: smpt
      protocol: TCP
      targetPort: 1025
  selector:
    app: mailcatcher
  type: NodePort
---
# Source: DIME/templates/mailcatcher.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mailcatcher
  labels:
    app: mailcatcher
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mailcatcher
  template:
    metadata:
      labels:
        app: mailcatcher
    spec:
      containers:
        - name: mailcatcher
          image: registry.gitlab.com/scce/docker-images/mailcatcher:0.7.1-024e798bc066c1ca361c3cbde460a92f6cbba964
          ports:
            - containerPort: 1080
          env:
            - name: HTTP_PATH
              value: /mailcatcher
      restartPolicy: Always
"""

    def test_local(self):
        actual = self.execute_helm(self.template, server_tier="local")
        self.assertEqual(self.expected, actual)

    def test_staging(self):
        actual = self.execute_helm(self.template, server_tier="staging")
        self.assertEqual(self.expected, actual)

    def test_demo(self):
        actual = self.execute_helm(self.template, server_tier="demo")
        self.assertEqual(self.expected, actual)

    def test_production(self):
        actual = self.execute_helm(server_tier="production")
        expected = "# Source: DIME/templates/mailcatcher.yml"
        self.assertNotIn(expected, actual)
