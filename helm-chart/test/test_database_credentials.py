import helm_test


class TestDatabaseCredentials(helm_test.HelmTest):
    template = "database-credentials.yml"

    def test(self):
        actual = self.execute_helm(self.template)
        expected = """---
# Source: DIME/templates/database-credentials.yml
apiVersion: v1
kind: Secret
metadata:
  name: database-credentials
type: Opaque
data:
  user: c2E=
  password: c2E=
"""
        self.assertEqual(expected, actual)
