import helm_test


class TestDatabaseCredentials(helm_test.HelmTest):
    template = "dime-auth-key.yml"

    def test(self):
        expected = """---
# Source: DIME/templates/dime-auth-key.yml
apiVersion: v1
kind: Secret
metadata:
  name: dime-auth-key
type: Opaque
stringData:
  private: "MySuperSecretDIMEAuthPrivateKey"
  public: "MyDIMEAuthPublicKey"
"""
        actual = self.execute_helm(template=self.template,
                                   values=["dywaApp.authKey.private=MySuperSecretDIMEAuthPrivateKey",
                                           "dywaApp.authKey.public=MyDIMEAuthPublicKey"])
        self.assertEqual(expected, actual)

