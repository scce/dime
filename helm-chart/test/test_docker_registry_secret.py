import helm_test


class TestDockerRegistrySecret(helm_test.HelmTest):
    template = "docker-registry-secret.yml"

    def test_present(self):
        actual = self.execute_helm(self.template, ["registry.dockerConfigJson=foo"])
        expected = """---
# Source: DIME/templates/docker-registry-secret.yml
apiVersion: v1
data:
  .dockerconfigjson: "foo"
kind: Secret
metadata:
  name: registry
type: kubernetes.io/dockerconfigjson
"""
        self.assertEqual(expected, actual)

    def test_absent(self):
        actual = self.execute_helm(values=["registry.secret=Nil"])
        expected = "# Source: DIME/templates/docker-registry-secret.yml"
        self.assertNotIn(expected, actual)
