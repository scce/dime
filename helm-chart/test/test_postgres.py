import helm_test


class TestPostgres(helm_test.HelmTest):
    template = "postgres.yml"

    def test(self):
        actual = self.execute_helm(self.template)
        expected = """---
# Source: DIME/templates/postgres.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgres
  labels:
    app: postgres
data:
  POSTGRES_DB: "dywa"
  PGDATA: /var/lib/postgresql/data/pgdata
---
# Source: DIME/templates/postgres.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: postgres-pv-claim
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: "local-storage"
  resources:
    requests:
      storage: "1Gi"
---
# Source: DIME/templates/postgres.yml
kind: Service
apiVersion: v1
metadata:
  name: postgres
spec:
  selector:
    app: postgres
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
---
# Source: DIME/templates/postgres.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgres
spec:
  replicas: 1
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      containers:
        - name: postgres
          image: postgres:14.4
          args: [
            "-c", "max_connections=300",
            "-c", "max_prepared_transactions=200"
          ]
          imagePullPolicy: IfNotPresent
          ports:
            - containerPort: 5432
          env:
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: user
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: password
          envFrom:
            - configMapRef:
                name: postgres
          volumeMounts:
            - mountPath: /var/lib/postgresql/data
              name: postgres-volume
          resources:
            limits:
              cpu: "1000m"
              memory: "1Gi"
      restartPolicy: Always
      volumes:
        - name: postgres-volume
          persistentVolumeClaim:
            claimName: postgres-pv-claim
"""
        self.assertEqual(expected, actual)
