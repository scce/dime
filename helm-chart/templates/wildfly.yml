apiVersion: v1
kind: ConfigMap
metadata:
  name: dywa-app
  labels:
    app: dywa-app
data:
  CLIENT_MAX_BODY_SIZE: {{ .Values.ingress.clientMaxBodySize | quote }}
  DATASOURCE_DATABASE_NAME: {{ .Values.database.name | quote }}
  DATASOURCE_SERVER_NAME: {{ .Values.database.server | quote }}
  DIME_APP_ENCRYPT_DISABLE: {{ .Values.encrypt.disable | quote }}
  DIME_APP_ENCRYPT_PASSPHRASE: {{ .Values.encrypt.passphrase | quote }}
  DIME_APP_MAIL_PORT: {{ .Values.mail.port | quote }}
  DIME_APP_MAIL_SERVER: {{ .Values.mail.server | quote }}
  DIME_APP_SERVER_TIER: {{ .Values.serverTier | quote }}
  DIME_APP_SERVER_URL: "https://{{ .Values.ingress.host }}/"
  {{ if .Values.dywaApp.appConfig -}}
  DIME_APP_CONFIG: {{ .Values.dywaApp.appConfig | toJson | quote }}
  {{ else -}}
  DIME_APP_CONFIG: "{}"
  {{ end -}}
  JAVA_OPTS: "-Xms64m -Xmx2g -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=512m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: dywa-app
  name: dywa-app
spec:
  ports:
    - port: 8080
      protocol: TCP
      targetPort: 8080
  selector:
    app: dywa-app
  type: NodePort
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: dywa-app-pv-claim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dywa-app
  labels:
    app: dywa-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: dywa-app
  template:
    metadata:
      labels:
        app: dywa-app
    spec:
      initContainers:
        - name: test-database-connection
          image: postgres:14.4
          command: [
              "sh",
              "-c",
              "until pg_isready -h $DATASOURCE_SERVER_NAME -p 5432; do echo waiting for database; sleep 2; done;"
          ]
          envFrom:
            - configMapRef:
                name: dywa-app
      containers:
        - name: dywa-app
          image: "{{ .Values.registry.base }}/wildfly:{{ .Values.appVersion }}"
          env:
            - name: DATASOURCE_USER
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: user
            - name: DATASOURCE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: database-credentials
                  key: password
            - name: DIME_APP_AUTH_PRIVATE_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: private
            - name: DIME_APP_AUTH_PUBLIC_KEY
              valueFrom:
                secretKeyRef:
                  name: dime-auth-key 
                  key: public 
          envFrom:
            - configMapRef:
                name: dywa-app
          volumeMounts:
            - mountPath: /opt/jboss/wildfly/standalone/data/files
              name: dywa-app-volume
          ports:
            - containerPort: 8080
          resources:
            limits:
              cpu: {{ .Values.dywaApp.cpu | default "1000m" | quote }}
              memory: {{ .Values.dywaApp.memory | default "2Gi" | quote }}
      restartPolicy: Always
      imagePullSecrets:
        - name: {{ .Values.registry.secret | default "registry" }}
      volumes:
        - name: dywa-app-volume
          persistentVolumeClaim:
            claimName: dywa-app-pv-claim
