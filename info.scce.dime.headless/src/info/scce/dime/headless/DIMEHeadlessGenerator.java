/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.headless;

import java.util.Arrays;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import info.scce.dime.headless.generator.HeadlessGenerator;

/**
 * The DIMEHeadlessGenerator executes the DIME Generator class from the a command line. 
 */
public class DIMEHeadlessGenerator implements IApplication {

	private String graphPath;

	private String importPath = null;
	
	private final String USAGE = "Usage: dime -data <workspacePath> -application info.scce.dime.headless [--importProject <importPath>] --model <modelPath>";
	@Override
	public Object start(IApplicationContext context) throws Exception {
		System.out.println("Reading Parameters");
		String[] params = getArgs(context);
		if (params[0] != null && params[1] != null) {
			importPath = params[0];
			graphPath  = params[1];
			
			
		} else if (params[0] != null) {
			graphPath  = params[0];
		} else {
			System.err.println(USAGE);
			throw new RuntimeException(String.format("Can not read parameters: %s",Arrays.toString(params)));
		}
		
		System.out.println("Project Root: " + importPath);
		System.out.println("DAD File: " + graphPath);			
		
		Job job = new HeadlessGenerator().createGenerationJob(importPath, graphPath);
		System.out.println("Scheduling generation job...");
		job.schedule();
		synchronized (job) {
			job.join();
		}
		
		IStatus result = job.getResult();
		if (Status.OK_STATUS.equals(result)) {
			System.out.println("Generation job result: " + result);
			System.out.println("Success.");
			return IApplication.EXIT_OK;
		} else {
			Throwable e = result.getException();
			if (e != null) {
				e.printStackTrace();
			}
			System.out.println("Generation job result: " + result);
			System.out.println("Error.");
			return 1;
		}
	}
	
	/**
	 * Splits app arguments this way example: --model \<modelPath\> --outDir
	 * \<outdir\> => [modelPath,outdir]
	 * 
	 * @param context
	 * @return String array of argument parameters
	 */
	public String[] getArgs(IApplicationContext context) {
		final Map<?, ?> args = context.getArguments();
		final String[] appArgs = (String[]) args.get("application.args");
		final String[] parameters = new String[2];
		if (appArgs.length >= 4) {
			int j = 0;
			for(int i=0;i<appArgs.length-1;i++) {
				if(appArgs[i].equals("--importProject")) {
					parameters[j]= appArgs[i+1];
					j++;
				}
				if(appArgs[i].equals("--model")) {
					parameters[j]=appArgs[i+1];
					break;
				}
			}
		} else {
			System.err.println(Arrays.toString(appArgs));
			System.err.println(USAGE);
		}

		return parameters;
	}

	@Override
	public void stop() {
		// nothing here
	}

}
