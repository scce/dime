/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.wizard.pages;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.osgi.framework.Bundle;

public class MainPage extends WizardPage {

	private final String EXAMPLE_BUNDLE_ID = "info.scce.dime.examples";
	private final String EXAMPLES_PATH = "/examples/";
	private final String DESC_EXTENSION = ".description";
	private final String DESCRIPTION_DEFAULT = "This wizard will create a new DIME project";

	private Map<String,ProjectDescription> projectDescriptions = new LinkedHashMap<>();
	
	private Group mainGroup;
	private Group xmplGroup;
	
	private Button xmplButton;
	private Button mainSelectedButton;
	private Button xmplSelectedButton;
	
	private Text projectNameField;
	
	private SelectionListener mainSelectionListener;
	private SelectionListener xmplSelectionListener;
	
	
	public MainPage(String pageName) {
		super(pageName);
		setTitle("New DIME Application");
		setDescription(DESCRIPTION_DEFAULT);
		initSelectionListeners();
		initPossibleExampleProjects();
	}


	private void initSelectionListeners() {
		mainSelectionListener = new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.widget instanceof Button) {
					mainSelectedButton = (Button) e.widget;
					updatePage();
				}
			}
			
			@Override public void widgetDefaultSelected(SelectionEvent e) {}
		};
		xmplSelectionListener = new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.widget instanceof Button) {
					xmplSelectedButton = (Button) e.widget;
					updatePage();
				}
			}
			
			@Override public void widgetDefaultSelected(SelectionEvent e) {}
		};
	}

	@Override
	public void createControl(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(1, false));
		
		createProjectNameField(comp);
		createSelectNewOrExample(comp);
		createSelectExampleProject(comp);
		
		updatePage();
		setControl(comp);
	}
	
	private void createProjectNameField(Composite parent) {
		Composite inner = new Composite(parent, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		inner.setLayoutData(gridData);
		inner.setLayout(new GridLayout(2, false));
		
	    Label label = new Label(inner, SWT.NULL);
	    label.setText("Project name: ");

	    projectNameField = new Text(inner, SWT.SINGLE | SWT.BORDER);
	    gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
	    projectNameField.setLayoutData(gridData);
	    projectNameField.setText("NewApp");
	    projectNameField.addKeyListener(new KeyListener() {
			
			@Override public void keyReleased(KeyEvent e) {
				updatePage();
			}
			
			@Override public void keyPressed(KeyEvent e) {}
		});
	}
	
	private void createSelectNewOrExample(Composite parent) {
		mainGroup = new RadioGroup(parent, mainSelectionListener)
			.addButton("Create a new application from scratch")
			.addButton("Initialize the project with an example app")
			.selectChild(0)
			.get();
	}
	
	private void createSelectExampleProject(Composite parent) {
		xmplButton = (Button) mainGroup.getChildren()[1];
		
		Label label = new Label(parent, SWT.NONE);
		label.setText("Available example apps:");
		
		xmplGroup = new RadioGroup(parent, xmplSelectionListener)
			.addButtonForEach(getPossibleExampleProjects())
			.get();
	}
	
	private void initPossibleExampleProjects() {
		Bundle bundle = Platform.getBundle(EXAMPLE_BUNDLE_ID);
		if (bundle != null) try {
			final Enumeration<URL> entries = bundle.findEntries(EXAMPLES_PATH, "*", false);
			while (entries.hasMoreElements()) {
				final URL nextElement = entries.nextElement();
				final URL fileURL = FileLocator.toFileURL(nextElement);
				URI uri = URI.createFileURI(fileURL.getPath());
				File f = new File(uri.toFileString());
				
				List<String> desc = null;
				if (f.isDirectory()) {
					if (containsDescription(f)) {
						desc = getProjectDescription(f);
					}
					ProjectDescription pd = null;
					if (desc != null && desc.size() >= 2) {
						pd = new ProjectDescription(f, nextElement, desc.get(0), desc.get(1));
					} else {
						pd = new ProjectDescription(f, nextElement, f.getName(), "No Description available...");
					}
					projectDescriptions.put(pd.getProjectName(), pd);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private Set<String> getPossibleExampleProjects() {
		Set<String> keySet = projectDescriptions.keySet();
		Set<String> sortedKeys = keySet.stream().sorted().collect(Collectors.toSet());
		return sortedKeys;
	}
	
	private List<String> getProjectDescription(File file) {
		File[] files = file.listFiles();
		List<String> desc = new ArrayList<String>();
		for (File f : files)
			if (f.isFile() && f.getName().endsWith(DESC_EXTENSION)){
				desc = readDescription(f);
				
			}
		return desc;
	}


	private List<String> readDescription(File f) {
		List<String> retval = new ArrayList<String>();
		try {
			List<String> lines = Files.readAllLines(f.toPath());
			retval.add(0,lines.remove(0));
			String result = "";
			for (String l : lines)
				result += l;
			retval.add(1, result);
			return retval;
		} catch (IOException e) {
			e.printStackTrace();
			return retval;
		}
		
	}


	private boolean containsDescription(File file) {
		File[] files = file.listFiles();
		for (File f : files)
			if (f.isFile() && f.getName().endsWith(DESC_EXTENSION))
				return true;
		return false;
	}
	
	public String getProjectName() {
		return projectNameField.getText();
	}
	
	public ProjectDescription getSelectedExampleProject() {
		if (xmplSelectedButton != null) {
			return projectDescriptions.get(xmplSelectedButton.getText());
		}
		return null;
	}
	
	public String getSelectedExampleProjectName() {
		ProjectDescription pd = getSelectedExampleProject();
		if (pd != null)
			return pd.getProjectName();
		return null;
	}

	public URL getSelectedExampleProjectURL() {
		ProjectDescription pd = getSelectedExampleProject();
		if (pd != null)
			return pd.getProjectURL();
		return null;
	}
	
	public boolean isCreateExample() {
		return mainSelectedButton == xmplButton;
	}
	
	private void updatePage() {
		updateEnabledStatus();
		String msg = validateProjectName(getProjectName());
		setPageComplete(msg == null);
		setErrorMessage(msg);
		if (msg == null) {
			msg = DESCRIPTION_DEFAULT;
			if (isCreateExample()) {
				ProjectDescription pd = getSelectedExampleProject();
				if (pd != null) {
					msg = pd.getProjectDescription();
				}
			}
			setDescription(msg);
		}
	}
	
	private void updateEnabledStatus() {
		boolean isXmpl = isCreateExample();
		for (Control c : xmplGroup.getChildren()) {
			c.setEnabled(isXmpl);
		}
	}
	
	private static String validateProjectName(String projectName) {
		if (projectName.isEmpty())
			return "Enter project name";
		try {
			IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
			for (IProject p : projects) try {
				if (p.getName().equals(projectName))
					return "Project: " + projectName + " already exists";
			} catch(Exception e) {
				e.printStackTrace();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		if (projectName.matches(".*[:/\\\\\"&<>\\?#,;].*")) {
			return "The project name contains illegal characters (:/\"&<>?#,;)";
		}

		return null;
	}
	
}

class ProjectDescription {
	
	private File projectFile;
	private URL projectURL;
	private String projectName;
	private String projectDescription;
	
	public ProjectDescription(File f, URL url, String name, String desc) {
		this.projectFile = f;
		this.projectURL = url;
		this.projectName = name;
		this.projectDescription = desc;
	}

	public File getProjectFile() {
		return projectFile;
	}

	public void setProjectFile(File projectFile) {
		this.projectFile = projectFile;
	}

	public URL getProjectURL() {
		return projectURL;
	}

	public void setProjectURL(URL projectURL) {
		this.projectURL = projectURL;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	
}

class RadioGroup {
	
	private Group group;
	private SelectionListener listener;
	
	public RadioGroup(Composite parent, SelectionListener listener) {
		this.group = new Group(parent, SWT.BORDER | SWT.SHADOW_IN);
		this.group.setLayout(new RowLayout(SWT.VERTICAL));
		this.listener = listener;
	}
	
	public RadioGroup selectChild(int i) {
		((Button) group.getChildren()[i]).setSelection(true);
		return this;
	}

	public RadioGroup addButton(String text) {
		Button btn = new Button(group, SWT.RADIO);
		btn.setText(text);
		btn.addSelectionListener(listener);
		return this;
	}
	
	public RadioGroup addButtonForEach(Iterable<String> texts) {
		texts.forEach(this::addButton);
		return this;
	}
	
	public Group get() {
		return this.group;
	}
}
