/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.wizard.pages

import info.scce.dime.ui.wizard.NativeFrontendSIBLibraryWizard
import java.io.File
import java.net.URI
import org.eclipse.core.resources.IContainer
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.CoreException
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.swt.SWT
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.DirectoryDialog
import org.eclipse.swt.widgets.Event
import org.eclipse.swt.widgets.FileDialog
import org.eclipse.swt.widgets.Group
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import java.net.URL

class NativeFrontendSIBLibraryWizardPage extends WizardPage {
	IContainer container
	Text dirText
	Text fileNameText
	Text jarDirText
	Text rootURLText
	Button browsFileButton
	Button browsJarFileButton
	Button isRest

	new(String pageName) {
		super(pageName)
		setTitle = "Create new NFSIB Library diagram"
		setMessage = "Create a new diagram in an existing or new project"
	}

	override createControl(Composite parent) {
		val composite = new Composite(parent, SWT.NONE)
		val layout = new GridLayout(3, false)
		composite.layout = layout

		composite.addLabel("Di&rectory: ")
		dirText = composite.addTextField(false, false)
		browsFileButton = composite.addButton("Brows&e...", SWT.PUSH)
		browsFileButton.addListener(SWT.Selection, [
			val dialog = new DirectoryDialog(shell)
			dialog.text = "Select a directory"
			val rootLocation = ResourcesPlugin.workspace.root.location.toOSString()
			dialog.filterPath = rootLocation
			val dirName = dialog.open()
			if (dirName != null)
				dirText.text = dirName;
			dialogChanged
		])

		composite.addLabel("Fi&le name: ")
		fileNameText = composite.addTextField(true, true)
		fileNameText.text = ""
		composite.addLabel("")

		isRest = composite.addButton("&Import REST Service from jar", SWT.CHECK)
		(isRest.layoutData as GridData).horizontalSpan = 3
		(isRest.layoutData as GridData).horizontalAlignment = SWT.LEFT

		val restComposite = new Group(composite, SWT.SHADOW_ETCHED_IN)
		val l = new GridData(GridData.FILL_BOTH)
		l.horizontalSpan = 3

		restComposite.layoutData = l
		restComposite.layout = new GridLayout(3, false)
		restComposite.text = "Generate NFSIBs from REST service"
		restComposite.visible = false

		isRest.addListener(SWT.Selection, [ Event e |
			l.exclude = !l.exclude
			restComposite.visible = l.exclude
			composite.redraw
			dialogChanged
		])

		restComposite.addLabel("&JAR File")
		jarDirText = restComposite.addTextField(false, false)
		browsJarFileButton = restComposite.addButton("Brows&e...", SWT.PUSH)
		browsJarFileButton.addListener(SWT.Selection, [
			val fileDialog = new FileDialog(shell, SWT.OPEN)
			fileDialog.filterExtensions = #["*.jar"]
			fileDialog.text = "Choose jar file containing REST Service"
			val fileLocation = fileDialog.open
			if (fileLocation != null)
				jarDirText.text = fileLocation
			dialogChanged
		])

		restComposite.addLabel("R&oot URL")
		rootURLText = restComposite.addTextField(true, true)
		rootURLText.addListener(SWT.KeyUp,[dialogChanged])
		fileNameText.addListener(SWT.KeyUp,[dialogChanged])
		var IStructuredSelection selection = null
		if (wizard instanceof NativeFrontendSIBLibraryWizard)
			selection = (wizard as NativeFrontendSIBLibraryWizard).getSelection()

		if (selection != null && selection.getFirstElement() instanceof IContainer) {
			container = selection.firstElement as IContainer
			dirText.setText(container.getLocation().toOSString())
		}

		dialogChanged

		control = composite

	}

	def addLabel(Composite parent, String label) {
		val labelComponent = new Label(parent, SWT.NONE)
		labelComponent.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false))
		labelComponent.setText(label)
		return labelComponent
	}

	def addButton(Composite parent, String lable, int style) {
		val button = new Button(parent, style)
		button.setText(lable)
		button.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false))
		return button
	}

	def addTextField(Composite parent, boolean editable, boolean enabled) {
		val textField = new Text(parent, SWT.BORDER)
		textField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false))
		textField.setEditable(editable)
		textField.setEnabled(enabled)
		return textField
	}

	private def String validateModelLocation() {
		val directory = dirText.getText()
		if (directory.empty)
			return "Select existing project or enter new project name"
		return null
	}

	private def String validateFileName() {
		val fileName = fileNameText.getText()
		val fileExtension = "nfsl.process"
		if (fileName.empty) {
			return "Enter file name"
		} else if (fileName.contains(".") && !fileName.endsWith(fileExtension)) {
			return "Wrong file extension"
		}	 else
			return null
	}

	private def boolean checkFileExists() {
		val fileName = if (fileNameText.getText().contains("."))
				fileNameText.text
			else
				fileNameText.text.concat(".nfsl.process")
		try {
			if (container == null)
				return false
			for (IResource res : container.members()) {
				if (res.getName().equals(fileName))
					return true
			}
		} catch (CoreException e) {
			e.printStackTrace()
		}
		return false
	}

	def boolean checkIsREST() {
		val isGenerateRest = isRest.selection
		isGenerateRest
	}

	private def String checkJarFile() {
		if (checkIsREST) {
			jarDirText.text != null
			val file = new File(jarDirText.text)
			if (file.exists && file.canRead)
				return null
			return "Enter JAR File Location"
		}
		return null
	}

	private def String checkRootUrl() {
		if (checkIsREST) {
			try {
				new URL(rootURLText.text)
				val uri = URI.create(rootURLText.text).normalize
				if (uri.isOpaque || !uri.isAbsolute )
					return "Please enter a valid URL \n (Protocol Missing)"
				if(uri.authority==null)
					return "Please enter a valid URL \n (Authority Missing)"
				rootURLText.text = uri.toURL.toExternalForm
			} catch (Exception e) {
				return "Please enter a valid URL\n(" + e.message + ")"
			}
		}
		return null
	}

	private def dialogChanged() {
		null.updateStatus
		checkRootUrl?.updateStatus
		checkJarFile?.updateStatus
		if(checkFileExists) "File already exists".updateStatus
		validateFileName?.updateStatus
		validateModelLocation?.updateStatus
	}

	private def updateStatus(String msg) {
		setErrorMessage(msg)
		if (getContainer.currentPage != null) {
			wizard.container.updateMessage
			wizard.container.updateButtons
		}
		setPageComplete(errorMessage == null)
	}
	def getDirecory(){
		dirText?.text
	}
	def getFileName(){
		fileNameText?.text
	}
	def getJarFile(){
		new File(jarDirText?.text)
	}
	def getRootURL(){
		rootURLText?.text
	}
}
