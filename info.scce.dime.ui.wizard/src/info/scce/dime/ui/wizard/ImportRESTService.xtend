/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.wizard

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.type.TypeFactory
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.jsonSchema.JsonSchema
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator
import com.fasterxml.jackson.module.jsonSchema.types.AnySchema
import com.fasterxml.jackson.module.jsonSchema.types.ArraySchema
import com.fasterxml.jackson.module.jsonSchema.types.ArraySchema.SingleItems
import com.fasterxml.jackson.module.jsonSchema.types.BooleanSchema
import com.fasterxml.jackson.module.jsonSchema.types.IntegerSchema
import com.fasterxml.jackson.module.jsonSchema.types.NumberSchema
import com.fasterxml.jackson.module.jsonSchema.types.ObjectSchema
import com.fasterxml.jackson.module.jsonSchema.types.ReferenceSchema
import com.fasterxml.jackson.module.jsonSchema.types.StringSchema
import de.jabc.cinco.meta.core.utils.job.CompoundJob
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import info.scce.common.singleobject.generator.rest.annotations.WrapperObject
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.Association
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.InheritorType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.factory.DataFactory
import info.scce.dime.process.helper.PortUtils
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.NativeFrontendBranch
import info.scce.dime.process.process.NativeFrontendSIB
import info.scce.dime.process.process.NativeFrontendSIBLibrary
import info.scce.dime.process.process.NativeFrontendSibType
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.SIB
import java.io.File
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.net.URL
import java.net.URLClassLoader
import java.util.Arrays
import java.util.Collection
import java.util.Date
import java.util.Map
import java.util.Set
import java.util.function.Function
import javax.ws.rs.DELETE
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.PUT
import javax.ws.rs.Path
import javax.ws.rs.ext.ContextResolver
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.IPath
import org.eclipse.emf.ecore.EObject
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.FileDialog
import org.reflections.Reflections

import static de.jabc.cinco.meta.core.utils.job.JobFactory.job

import static extension info.scce.dime.data.helper.JavaIdentifierUtils.*
import static extension info.scce.dime.process.actions.CreateDartTemplate.*

class ImportRESTService {

	protected extension WorkspaceExtension = new WorkspaceExtension
	protected extension WorkbenchExtension = new WorkbenchExtension

	Process process
	NativeFrontendSIBLibrary sib
	FileDialog fileDialog
	int x = -200
	int y = 50
	int dataX = -100
	int dataY = 0
	var ObjectMapper mapper;
	var JsonSchemaGenerator schemaGenerator
	var dataModelIsDirty = false;
	val ignoredClasses = newHashSet();
	URLClassLoader ulc
	Data dataContainer;
	val Map<Type, TypeWrapper> typeMap = newHashMap
	val Set<String> filledProperties = newHashSet
	val Set<SIB> addedSibs = newHashSet
	var Map<String, Class<? extends Enum>> enumMap
	val jobcontrol = job("Generating Sibs and Data Model")
	val pastDataJobs = job("Adding Input and Output ports")
	val pastDataWorkload =pastDataJobs.consumeConcurrent(100)
	val restInvocationJobs = job("creating REST invocation methods")
	val restInvocationWorkload =restInvocationJobs.consumeConcurrent(100)
	var  restTemplate = ""
	var Reflections reflection

	val Set<String> imports = newHashSet

	def private void init(NativeFrontendSIBLibrary nfsl) {
		this.sib = nfsl
		process = sib.getRootElement
	}

	public def CompoundJob execute(NativeFrontendSIBLibrary nfsl, File jarFile) {
		init(nfsl)
		fileDialog = new FileDialog(display?.activeShell, SWT.OPEN)
		fileDialog.filterExtensions = #["*.jar"];

		val Set<Class<?>> annotated = newHashSet;
		try {
			ulc = new URLClassLoader(#[jarFile.toURI().toURL()], Thread.currentThread.contextClassLoader)
			reflection = new Reflections(ulc)
			try {
				val objectMapperProvider = reflection.getSubTypesOf(ContextResolver).findFirst [
					genericInterfaces.filter(ParameterizedType).findFirst [
						rawType == ContextResolver && actualTypeArguments.head == ObjectMapper
					] != null
				]
				mapper = objectMapperProvider.newInstance.getContext(null) as ObjectMapper
				System.err.println("Using provided ObjectMapper from: " + objectMapperProvider.name)
			} catch (Exception e) { // provide somewhat "default" ObjectMapper 
				System.err.println("Using default ObjectMapper since jar does not contain ObjectMapper provider")
				mapper = new ObjectMapper;
				mapper.enable(SerializationFeature.WRAP_ROOT_VALUE)
				mapper.enable(SerializationFeature.INDENT_OUTPUT)
				mapper.registerModule(new JavaTimeModule()).registerModule(new Jdk8Module())
				mapper.findAndRegisterModules()
			}
			// add custom classloader to provide classes
			mapper.typeFactory = TypeFactory.defaultInstance.withClassLoader(ulc);
			schemaGenerator = new JsonSchemaGenerator(mapper)
			enumMap = reflection.getSubTypesOf(Enum).filter[enumConstants != null].sortBy[name].toMap [
				Arrays.deepToString(enumConstants)
			]
			annotated.addAll(reflection.getTypesAnnotatedWith(Path, true));
		} catch (Exception e) {
			e.printStackTrace
		}

		// TODO: perform action in parallel
		jobcontrol.consume(150,"Handling classes").taskForEach(annotated, [handleClass], [simpleName]).consume(50)
		.ifCanceled(
			[
				restInvocationJobs.consume(1)
				.task(restInvocationJobs)
				.task(saveGeneratedDartCode)
				.onDone([sib.transact([sib.height=y+150;sib.width=1300+50]);process.save])
				.schedule
			])
	//.task([ReferenceRegistry.instance.reinitialize(process.resource.parent)])
	.task(pastDataJobs)
	.task(restInvocationJobs)
	.consume(50)
	.task(saveGeneratedDartCode)
	.onDone([sib.transact([sib.height=y+150;sib.width=1300+50]);process.save])
	return jobcontrol;
	}

	def private addPath(String s) throws Exception {
		if(s == null) throw new RuntimeException("aborted");
		val Method method = URLClassLoader.getDeclaredMethod("addURL", typeof(URL));
		method.setAccessible(true);
		method.invoke(ulc, #[new File(s).toURI().toURL()]);
	}
    def private Runnable saveGeneratedDartCode() {
		return [
			val classNamesss = new File(sib.className).name
			var classTemplate = '''
				import 'dart:async';
				import 'dart:convert';
				import 'dart:html';
				import 'package:angular2/core.dart';
							«FOR imp : imports SEPARATOR '\n'»import 'package:app/models/«imp.escapeDart».dart';«ENDFOR»
				
				class «classNamesss.replace(".dart","").escapeDart» {
					
				static String ROOT_URL="«sib.rootUrl»";
					
			'''
			sib.file.project.createFile(sib.className, classTemplate + restTemplate + "\n}", true)
		]
	}
	def private handleClass(Class<?> it) {
		System.out.println(simpleName + " " + annotations.join(", "))
		var success = false
		do {
			try {
				methods.filter[isAnnotationPresent(Path)].forEach[handleMethod]
				success = true;
			} catch (Throwable e) {
				var temp = ""
				if (e instanceof NoClassDefFoundError) {
					temp = e?.cause?.message
				} else if (e instanceof ClassNotFoundException) {
					temp = e?.message
				} else if (e instanceof TypeNotPresentException) {
					temp = e?.cause?.message
				} else {
					throw e
				}
				val missingClass = temp
				if (ignoredClasses.contains(missingClass)) {
					return
				}
				System.out.println("Missing class in jar: " + missingClass)
				display.syncExec([
					fileDialog = new FileDialog(display?.activeShell, SWT.OPEN)
					fileDialog.filterExtensions = #["*.jar"];
					fileDialog.text = "Add missing jar containing " + missingClass + ", cancel will ignore this class."
					val path = fileDialog.open()
					if (path == null) {
						ignoredClasses.add(missingClass)
						return;
					}
					addPath(path)
				])
			}
		} while (!success)
		process.save
		save
	}

	def private handleMethod(Method it) {
		//System.out.println(name + annotations.join("\n") + parameters.map[type + " " + name].join(", "))
		val finalWrapper = newArrayList()
		var NativeFrontendSIB createdSib

		createdSib = finalWrapper.head;

		addInputPorts(createdSib)
		addedSibs.add(createdSib)
		sib.transact[finalWrapper.head.methodName = getAnnotation(Path).value.escapeDart]
		restInvocationWorkload.task(createdSib.methodName, [
			val methodBody= generateRestInvocationCode(finalWrapper.head)
			synchronized(restTemplate){
			restTemplate +=methodBody
			}
		])
	}

	def private addInputPorts(Method it, NativeFrontendSIB cnfSib) {
		val time = time
		var i = 0;
		var types = genericParameterTypes
		var names = parameters.map[name]
		if (types.size == 1) {
			val type = types.head
			if (type instanceof Class<?>) {
				if (type.isAnnotationPresent(WrapperObject)) {
					getType(type)
					val declaredFields = type.declaredFields
					types = declaredFields.map[genericType].toList
					names = declaredFields.map[name].toList
				}
			}
		}
		for (type : types) {
			val parameterName = names.get(i++);
			var InputPort port
			switch type {
				case Boolean.TYPE: {
					port = cnfSib.inTransactionMagic [
						val p = cnfSib.newPrimitiveInputPort(0, 0)
						p.dataType = info.scce.dime.process.process.PrimitiveType.BOOLEAN;
						p
					]
				}
				case Float.TYPE,
				case Double.TYPE: {
					port = cnfSib.inTransactionMagic [
						val p = cnfSib.newPrimitiveInputPort(0, 0)
						p.dataType = info.scce.dime.process.process.PrimitiveType.REAL;
						p
					]
				}
				case Short.TYPE,
				case Integer.TYPE,
				case Byte.TYPE,
				case Long.TYPE: {
					port = cnfSib.inTransactionMagic [
						val p = cnfSib.newPrimitiveInputPort(0, 0)
						p.dataType = info.scce.dime.process.process.PrimitiveType.INTEGER;
						p
					]
				}
				case Void.TYPE: {
				}
				case Character.TYPE,
				case String: {
					port = cnfSib.inTransactionMagic [
						val p = cnfSib.newPrimitiveInputPort(0, 0)
						p.dataType = info.scce.dime.process.process.PrimitiveType.TEXT;
						p
					]
				}
				default: {
					// preload datamodel
					getType(type)
					// schedule adding of complex ports after dataModel is completed to save excessive amount of time used for saving incomplete datamodel
					pastDataWorkload.task(
						"InputPort for " + it.name + " " + parameterName,
						[
							cnfSib.transact [
								val innerTime = time()
								var InputPort inPort
								val TypeWrapper wrapped = getType(type)
								if (wrapped.type != null) {
									save
									inPort = cnfSib.newComplexInputPort(wrapped.type, 0, 0)
								} else {
									inPort = cnfSib.newPrimitiveInputPort(0, 0);
									(inPort as PrimitiveInputPort).dataType = PortUtils.toPrimitiveType(
										wrapped.primitiveType)
								}
								if (wrapped instanceof CollectionWrapper || wrapped instanceof MapWrapper) {
									inPort.isList = true
								} else if (wrapped instanceof ClassWrapper) {
								} else {
								//	System.err.println("WoopWoop" + wrapped)
								}
								inPort.name = parameterName

								cnfSib.update
								innerTime.logTime("addInputPort inner")
							]
						]
					)
				}
			}
			if (port != null) {
				val finalPort = port
				port.transact[finalPort.name = parameterName]
			}
		}
		cnfSib.transact [
			cnfSib.update
		]
		time.logTime("addInputPort")
	}


	private def TypeWrapper getType(Type type) {
		val time = time
		val TypeWrapper existing = typeMap.get(type)
		if (existing != null) {
			System.out.println("existing TYPE: " + type.typeName)
			time.logTime("getType existing type")
			return existing
		} else if(type !=null){
			synchronized (type) {
				if (type instanceof ParameterizedType) {
					System.err.println(type + ": " + type.rawType + type.actualTypeArguments.join(", "))
					if (Collection.isAssignableFrom(type.rawType as Class<?>)) {
						System.err.println("collection type detected")
						val Type innerType = type.actualTypeArguments.get(0)
						val PrimitiveType innerPType = innerType.primitiveTypeOrNull
						if (innerPType == null) {
							val cType = innerType.getType.type
							val wrapped = new CollectionWrapper(cType);
							typeMap.put(type, wrapped)
							time.logTime("getType Collection Parametrized(innerType==null) Type type")
							return wrapped
						}
						val wrapped = new CollectionWrapper(null);
						wrapped.primitiveType = innerPType
						time.logTime("getType Collection Parametrized Type type")
						return wrapped
					}
					if (Map.isAssignableFrom(type.rawType as Class<?>)) {
						System.err.println(
							"Map type detected, key type: " + type.actualTypeArguments.get(0) + " value type: " +
								type.actualTypeArguments.get(1))
						val Type keyType = type.actualTypeArguments.get(0)
						val Type valueType = type.actualTypeArguments.get(1)
						val PrimitiveType keyPType = keyType.primitiveTypeOrNull
						val PrimitiveType valPType = valueType.primitiveTypeOrNull

						val ConcreteType newType = data.newConcreteType(DataX, dataY)

						newType.name = type.mapName
						

						if (keyPType == null) {
							val TypeWrapper keyWrapper = keyType.getType
							if (keyWrapper.type != null) {
								newType.addAssociation(keyWrapper, "key", false)
							} else {
								val PrimitiveAttribute keyAttribute = newType.newPrimitiveAttribute(0, 0)
								keyAttribute.dataType = keyWrapper.primitiveType
								keyAttribute.name = "key"
								keyAttribute.isList = true
							}
						} else {
							val PrimitiveAttribute keyAttribute = newType.newPrimitiveAttribute(0, 0)
							keyAttribute.dataType = keyPType
							keyAttribute.name = "key"
						}

						if (valPType == null) {
							val TypeWrapper valueWrapper = valueType.getType
							if (valueWrapper.type != null) {
								newType.addAssociation(valueWrapper, "value", false)
							} else {
								val PrimitiveAttribute valueAttribute = newType.newPrimitiveAttribute(0, 0)
								// save
								valueAttribute.dataType = valueWrapper.primitiveType
								valueAttribute.isList = true
								valueAttribute.name = "value"
							}
						} else {
							val PrimitiveAttribute valueAttribute = newType.newPrimitiveAttribute(0, 0)
							valueAttribute.dataType = valPType
							valueAttribute.name = "value"
						}

						val wrapped = new MapWrapper(newType);
						typeMap.put(type, wrapped)
						// save
						time.logTime("getType Map Parametrized Type type")
						return wrapped

					}

				} else if (type instanceof Class<?>) {
					if (Enum.isAssignableFrom(type)) {
						System.err.println("ENUM TYPE DETECTED: " + type);
						val finalHelper = newArrayList()

						data.transact([
							finalHelper.add(0, data.newEnumType(DataX, dataY))
							finalHelper.head.name = type.simpleName
						]);
						val EnumType newType = finalHelper.head
						val x = time()
						type.enumConstants.forEach [
							val y = time()
							newType.transact([newType.newEnumLiteral(0, 0).name = toString])
							y.logTime("single Item")
						]
						time.logTime("foreach Enum time")
						val TypeWrapper wrapped = new EnumWrapper(newType)
						typeMap.put(type, wrapped)
						// save
						time.logTime("getType Enum  Type type")
						return wrapped;
					} else {
						time.logTime("getType Class Type type")
						val x = newArrayList()
						data.transact([x.add(type.addAttributes)])
						return x.head
					}
				}
			}}
	}

	private def synchronized addAttributes(Type type) {
		val time = time
		val ClassWrapper primitiveWrapper = new ClassWrapper(null);
		switch type {
			case Boolean.TYPE: {
				primitiveWrapper.primitiveType = PrimitiveType.BOOLEAN
				return primitiveWrapper
			}
			case Float.TYPE,
			case Double.TYPE: {
				primitiveWrapper.primitiveType = PrimitiveType.REAL
				return primitiveWrapper
			}
			case Short.TYPE,
			case Integer.TYPE,
			case Byte.TYPE,
			case Long.TYPE: {
				primitiveWrapper.primitiveType = PrimitiveType.INTEGER
				return primitiveWrapper
			}
			case Void.TYPE: {
			}
			case Character.TYPE,
			case String: {
				primitiveWrapper.primitiveType = PrimitiveType.TEXT
				return primitiveWrapper
			}
			default: {
				if (type instanceof Class<?>) {
					val JsonSchema schema = schemaGenerator.generateSchema(type)
					time.logTime("addAdttribute generate JsonSchema")
					System.out.println("Creating Type: " + type);
					//System.err.println(mapper.writeValueAsString(schema))
					var info.scce.dime.data.data.Type newType
					var TypeWrapper wrapped
					if (Modifier.isAbstract(type.modifiers) && !Modifier.isInterface(type.modifiers)) {
						newType = data.newAbstractType(DataX, dataY)
						wrapped = new AbstractClassWrapper(newType as AbstractType)
					} else {
						newType = data.newConcreteType(DataX, dataY)
						wrapped = new ClassWrapper(newType as ConcreteType)
					}
					val superClass = type.superclass
					if (superClass != null && superClass != Object && superClass.package.name != "java.lang") {
						val superWrapper = getType(superClass)
						(newType as InheritorType).newInheritance(superWrapper.cType as InheritorType)
					}
					newType.name = type.simpleName

					typeMap.put(type, wrapped)
					newType.addAttributesRecursive(schema, type.simpleName, type, false)
					// save
					time.logTime("addAttributes")
					return wrapped
				}
				throw new IllegalStateException("addAttributes no class?!")
			}
		}
	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, JsonSchema schema,
		String attributeName, Type type, boolean list) {
		val it = cType.newPrimitiveAttribute(0, 0)
		dataType = PrimitiveType.FILE
		isList = list
		name = attributeName + " " + schema.class.simpleName
		System.err.println("unable to handle " + attributeName + schema.class.simpleName + "\n" +
			mapper.writeValueAsString(schema))
	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, AnySchema schema,
		String attributeName, Type type, boolean list) {
		val TypeWrapper wrapped = getType(type)
		cType.addAssociation(wrapped, attributeName.toFirstLower, (wrapped instanceof CollectionWrapper ||
			wrapped instanceof MapWrapper))

	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, ReferenceSchema schema,
		String attributeName, Type type, boolean list) {
		val loadedClass = schema.$ref.ToClass
		if (loadedClass != null) {
			val loadedWrapper = getType(loadedClass)
			if (loadedWrapper != null) {
				cType.addAssociation(loadedWrapper, attributeName.toFirstLower, list)
			} else {
				throw new RuntimeException("woopWoop");
			}
		}
	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, ArraySchema schema,
		String attributeName, Type type, boolean list) {
		val items = schema.items
		if (items instanceof SingleItems) {
			cType.addAttributesRecursive(items.schema, attributeName.toFirstLower, type, true)
		} else {
			throw new RuntimeException("woopWoop");
		}
	}

	// synchronized??
	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, ObjectSchema schema,
		String attributeName, Type type, boolean list) {
		if (schema.id != null) {
			val loadedClass = schema.id.ToClass
			getType(loadedClass)
			if (!typeMap.containsKey(loadedClass)) {
				var info.scce.dime.data.data.Type newType
				var TypeWrapper wrapped
				if (Modifier.isAbstract(loadedClass.modifiers) && !Modifier.isInterface(loadedClass.modifiers)) {
					newType = data.newAbstractType(DataX, dataY)
					wrapped = new AbstractClassWrapper(newType as AbstractType)
				} else {
					newType = data.newConcreteType(DataX, dataY)
					wrapped = new ClassWrapper(newType as ConcreteType)
				}
				newType.name = loadedClass.simpleName
				// save
				typeMap.put(loadedClass, wrapped)
			}
			val existing = typeMap.get(loadedClass).type
			if (!filledProperties.contains(loadedClass.name)) {
				filledProperties.add(loadedClass.name)
				schema.properties.entrySet.forEach [
					existing.addAttributesRecursive(value, key, loadedClass.typeRetriever(key), false)
				]
			// save
			} else {
				cType.addAssociation(typeMap.get(loadedClass), attributeName.toFirstLower, list)
			}

		} else {
			//System.err.println(mapper.writeValueAsString(schema))
			val wrappedType = getType(type)
			cType.addAssociation(wrappedType, attributeName.toFirstLower, list)
		}
	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, BooleanSchema schema,
		String attributeName, Type type, boolean list) {
		val it = cType.newPrimitiveAttribute(0, 0)
		dataType = PrimitiveType.BOOLEAN
		isList = list
		name = attributeName.toFirstLower
	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, StringSchema schema,
		String attributeName, Type type, boolean list) {
		if (!schema.enums.empty) {
			System.err.println("we have an enum :)))")
			cType.addAssociation(getType(enumMap.get(Arrays.deepToString(schema.enums.toArray))),
				attributeName.toFirstLower, list)
		} else {
			val it = cType.newPrimitiveAttribute(0, 0)
			dataType = PrimitiveType.TEXT
			isList = list
			name = attributeName.toFirstLower
		}
	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, IntegerSchema schema,
		String attributeName, Type type, boolean list) {
		val it = cType.newPrimitiveAttribute(0, 0)
		dataType = PrimitiveType.INTEGER
		isList = list
		name = attributeName.toFirstLower
	}

	private def dispatch synchronized addAttributesRecursive(info.scce.dime.data.data.Type cType, NumberSchema schema,
		String attributeName, Type type, boolean list) {
		val it = cType.newPrimitiveAttribute(0, 0)
		dataType = PrimitiveType.REAL
		isList = list
		name = attributeName.toFirstLower
	}

	private def synchronized Data data() {
		dataModelIsDirty = true;
		if (dataContainer == null) {
			val IFile file = process.file;
			val IPath path = file.fullPath.removeLastSegments(1)
			dataContainer = DataFactory.eINSTANCE.createData(path.toString, process.modelName + ".nfsl")
			dataContainer.transact[dataContainer.allContainers.forEach[delete]]
		}
		return dataContainer
	}

	private def synchronized save() {
		val time = time
		if (dataModelIsDirty) {
			data.save
		}
		time.logTime("Data Save")
		dataModelIsDirty = false
	}

	private static abstract class TypeWrapper {
		public PrimitiveType primitiveType
		private info.scce.dime.data.data.Type cType;

		new(info.scce.dime.data.data.Type cType) {
			this.cType = cType
		}

		public def info.scce.dime.data.data.Type getType() {
			this.cType
		}
	}

	private static class ClassWrapper extends TypeWrapper {
		new(ConcreteType cType) {
			super(cType)
		}
	}

	private static class AbstractClassWrapper extends TypeWrapper {
		new(AbstractType cType) {
			super(cType)
		}
	}

	private static class MapWrapper extends TypeWrapper {
		new(info.scce.dime.data.data.Type cType) {
			super(cType)
		}

	}

	private static class CollectionWrapper extends TypeWrapper {
		new(info.scce.dime.data.data.Type cType) {
			super(cType)
		}
	}

	private static class EnumWrapper extends TypeWrapper {
		new(EnumType cType) {
			super(cType)
		}
	}

	private def synchronized int DataX() {
		if (dataX < 1300) {
			dataX += 200
		} else {
			dataY += 150
			dataX = 100
		}
		return dataX
	}

	private def synchronized int X() {
		if (x < 1000) {
			x += 250
		} else {
			y += 150
			x = 50
		}
		return x
	}

	private def PrimitiveType getPrimitiveTypeOrNull(Type it) {
		switch (it) {
			case Short.TYPE,
			case Long.TYPE,
			case Integer.TYPE: return PrimitiveType.INTEGER
			case Character.TYPE,
			case String: return PrimitiveType.TEXT
			case Date: return PrimitiveType.TIMESTAMP
			case File: return PrimitiveType.FILE
			case Boolean.TYPE: return PrimitiveType.BOOLEAN
			case Double.TYPE: return PrimitiveType.REAL
			default: null
		}
	}

	private def String mapName(Type type) {

		if (type instanceof ParameterizedType) {
			if (Map.isAssignableFrom(type.rawType as Class<?>)) {
				return type.actualTypeArguments.get(0).mapName + ":" + type.actualTypeArguments.get(1).mapName +
					"Entry";
			}
			if (Collection.isAssignableFrom(type.rawType as Class<?>)) {
				return type.actualTypeArguments.get(0).mapName + "Coll";
			}
		} else if (type instanceof Class<?>) {
			type.simpleName
		}

	}

	private def addAssociation(info.scce.dime.data.data.Type it, TypeWrapper wrapper, String name, boolean isList) {
		if (wrapper != null) {
			data.transact [
				val cAssociation = newCAssociation(wrapper.type)

				if (wrapper instanceof CollectionWrapper || wrapper instanceof MapWrapper || isList) {
					cAssociation.sourceAttr.isList = true
				}
				cAssociation.sourceAttr.name = name
				cAssociation.isHidden = true
				cAssociation.update
			]
		}
	}

	private def Association newCAssociation(info.scce.dime.data.data.Type it, info.scce.dime.data.data.Type cType) {
		it.getClass().getMethod("newAssociation", cType.class.superclass.interfaces)?.invoke(it, cType) as Association
//		switch it{
//			AbstractType case: 
//			)?.
//			ConcreteType case:
//			EnumType case:
//			InheritorType case:
//			UserType case:
//		}		
	}

	def time() {
		System.currentTimeMillis;
	}

	def logTime(long start, String what) {
		val current= System.currentTimeMillis()
		val timeTook = current- start
		if (timeTook > 1) {
			println(what + " took: " + (timeTook) + "ms " + Thread.currentThread() + " Start "+start+ " End "+current);
		}
	}

	private def ToClass(String it) {
		try {
			ulc.loadClass(replace("urn:jsonschema:", "").replace(":", "."))
		} catch (Exception e) {
			throw e;
		}
	}

	// reconstruct actual Type from schema definition
	private def Type typeRetriever(Type clazz, String attributeName) {

		if (clazz instanceof Class<?>) {
			try {
				val Type typeFromConstructor = clazz?.constructors?.
					findFirst[annotations?.filter(JsonCreator) != null]?.parameters?.findFirst [
						annotations?.filter(JsonProperty)?.findFirst[value == attributeName] != null
					]?.parameterizedType
				if (typeFromConstructor == null) {
					val Type typeFromAnnotatedMethod = clazz.methods.findFirst [
						annotations.filter(JsonProperty).findFirst[value == attributeName] != null
					]?.genericReturnType
					if (typeFromAnnotatedMethod == null) {
						val Type typeFromField = clazz.fields.findFirst [
							annotations.filter(JsonProperty).findFirst[value == attributeName] != null
						]?.type
						if (typeFromField == null) {
							val toFirstUpper = attributeName.toFirstUpper
							val Type typeFromGetter = clazz.methods.findFirst [
								parameterCount == 0 && (name == "get" + toFirstUpper || name == "is" + toFirstUpper)
							]?.genericReturnType
							if (typeFromGetter == null) {
								return clazz.fields.findFirst[name == attributeName]?.type
							}
							return typeFromGetter;
						}
						return typeFromField
					}
					return typeFromAnnotatedMethod
				}
				return typeFromConstructor
			} catch (NullPointerException npe) {
				throw npe
			}
		} else {
			return clazz
		}
	}

	// / GENERATE REST DART INVOCATION CODE
	private  def synchronized CharSequence generateRestInvocationCode(Method it, NativeFrontendSIB csib) {
		val sib = csib;

		val returnType = switch (sib.sibType) {
			case BOOL:
				"bool"
			case VOID:
				"void"
			case COMPLEX: {
				sib.abstractBranchSuccessors.findFirst[name.compareToIgnoreCase("success") == 0].outputs.head.
					getVariableTypeFromOutput(imports)
			}
		}

		val type = genericReturnType?.type
		var responseHandle = "request.responseText"
		if (type?.cType != null)
			if (type instanceof ClassWrapper) {
				responseHandle = "completer.complete("
				(type.type as info.scce.dime.data.data.Type).name.escapeDart + "fromJSON(request.responseText));"
			}

		var httpTransport = "OPTIONS";
		if (isAnnotationPresent(POST))
			httpTransport = "POST"
		else if (isAnnotationPresent(PUT))
			httpTransport = "PUT"
		else if (isAnnotationPresent(GET))
			httpTransport = "GET"
		else if(isAnnotationPresent(
			DELETE)) httpTransport = "DELETE"

		'''
			
			«returnType» «sib.methodName.escapeDart»(«FOR input : sib.inputs SEPARATOR ',' AFTER ''»«input.getParameterType(imports)» «input.name.escapeDart»«ENDFOR»){
			String submissionContext="";
				«{	
			val genericparameter=genericParameterTypes.head
			if(genericparameter  !=null){
			if(parameters.head.isAnnotationPresent(WrapperObject)){
			val ctype = genericParameterTypes.head.type.type
			val nameOfData =(ctype as info.scce.dime.data.data.Type).name.escapeDart	
			'''«nameOfData» _requestData = new «nameOfData»();
			«FOR input: sib.inputs»_requestData.«input.name.escapeDart»=«input.name.escapeDart»;
			«ENDFOR»
			submissionContext= _requestData.toJSON();
			'''}
			else if(getPrimitiveTypeOrNull(genericparameter)== null){
				'''submissionContext= «sib.inputs.head.name.escapeDart».toJSON();'''
			}
			else{
				'''submissionContext= «sib.inputs.head.name.escapeDart»;'''
			}
			
					}						
	}»			«IF sib.sibType== NativeFrontendSibType.COMPLEX»
										var completer = new Completer<«returnType»>();
				«ENDIF»
				HttpRequest request = new HttpRequest();
					request.onLoadEnd.listen((_) {
						if(request.status == 401 || request.status ==403) {
							throw new StateError('currently only request without authentication possible');
					}
					if (request.readyState == HttpRequest.DONE && (request.status == 200))
					{
						«IF sib.sibType== NativeFrontendSibType.COMPLEX»
							«{
							val genericReturnType= genericReturnType;
							if(genericReturnType instanceof Class<?>){
								if(genericReturnType.isAnnotationPresent(WrapperObject)){
									'''completer.complete(«(genericReturnType.type.cType as info.scce.dime.data.data.Type).name».fromJSON(request.responseText).wrapped);'''
								}
								else{
								'''completer.complete(«returnType».fromJSON(request.responseText));'''
								}
							}else{
								'''completer.complete(«returnType».fromJSON(request.responseText));'''
							}
							
						}
						
						»
						«ENDIF»
					}
				});
			
				request.open("«httpTransport»", ROOT_URL+"«class.getAnnotation(Path) +"/"+  getAnnotation(Path).value»", async: false);
				request.setRequestHeader('Content-Type', 'application/json');
				request.send(submissionContext);
				«IF sib.sibType== NativeFrontendSibType.COMPLEX»
					return completer;
				«ELSEIF sib.sibType== NativeFrontendSibType.BOOL»
					return request.responseText.toLowerCase()=='true'
				«ENDIF»	
				
			}
			
		'''

	}

	private def <Y extends EObject, Z extends EObject> inTransactionMagic(Y it, Function<?, Z> f) {
		val returnValueHelper = newArrayList()
		transact[returnValueHelper.add(0, f.apply(null))]
		return returnValueHelper.head
	}

}
