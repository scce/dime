/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.wizard;

import info.scce.dime.ui.wizard.pages.MainPage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.osgi.framework.Bundle;

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;

/**
 * @author kopetzki
 *
 */
public class DIMEAppWizard extends Wizard implements INewWizard {
	

	class DIMEAppWizardException extends Exception{

		private static final long serialVersionUID = 1L;

		public DIMEAppWizardException(String message, Exception cause) {
			super(message, cause);
		}
		
		public DIMEAppWizardException(String message) {
			super(message);
		}
	}

	private final String EXAMPLE_BUNDLE_ID = "info.scce.dime.examples";
	private final String EMPTY_PROJECT_PATH = "/emptyproject/";
	private final String COMMONS_PATH = "/commons/";

	MainPage main = new MainPage("New DIME Application");

	public DIMEAppWizard() {

	}

	@Override
	public void addPages() {
		addPage(main);
		super.addPages();
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("New DIME Application");
	}

	@Override
	public boolean performFinish() {
		String projectName = projectName();
		System.out.println("project.name: " + projectName);

		try {
			URL projectURL = projectURL();
			System.out.println("project.url: " + projectURL);
			
			URL commonsURL = projectURL(COMMONS_PATH);
			System.out.println("commons.url: " + commonsURL);
			
			IProject project = createProject(projectName);
			
			File projectDirectory = project.getLocation().toFile();
			copyFiles(projectDirectory, projectURL, commonsURL); 
			
			generateGitLabCI(projectDirectory, "latest");
			generateDotProject(projectDirectory, projectName);
			refreshProject(project);
		} catch (DIMEAppWizardException e) {
			throw new RuntimeException("Failed to create new DIME App");
		}

		return true;
	}

	private void refreshProject(IProject project) throws DIMEAppWizardException {
		try {
			project.refreshLocal(
					IResource.DEPTH_INFINITE,
					new NullProgressMonitor()
			);
		} catch (CoreException e) {
			throw new DIMEAppWizardException("Failed to refresh project", e);
			
		}
	}

	private URL projectURL() throws DIMEAppWizardException {
		URL projectURL = projectURL(EMPTY_PROJECT_PATH);
		if (main.isCreateExample()) {
			projectURL = main.getSelectedExampleProjectURL();
		}
		return projectURL;
	}

	private URL projectURL(String path) throws DIMEAppWizardException {
		Bundle bundle = Platform.getBundle(EXAMPLE_BUNDLE_ID);
		if (bundle == null) {
			throw new DIMEAppWizardException("Failed to get bundle");
		}
		final Enumeration<URL> entries = bundle.findEntries(path, "*", false);
		if (!entries.hasMoreElements()) {
			throw new DIMEAppWizardException("No Entries in bundle");
		}
		URL url = entries.nextElement();
		System.out.println("URL: " + url);
		return url;
	}

	private String projectName() {
		String projectName = main.getProjectName();
		if (projectName == null || projectName.isEmpty()) {
			projectName = main.getSelectedExampleProjectName();
		}
		if (projectName == null || projectName.isEmpty()) {
			projectName = "NewApp";
		}
		return projectName;
	}

	private IProject createProject(String projectName) throws DIMEAppWizardException {
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		if (root == null) {
			throw new DIMEAppWizardException("Workspace Root is null...");
		}

		final NullProgressMonitor monitor = new NullProgressMonitor();

		List<String> srcFolders = new ArrayList<String>();
		srcFolders.add("src");
		Set<String> reqBundle = new HashSet<String>();
		reqBundle.add("org.eclipse.core.runtime");

		IProject project = ProjectCreator.createProject(
				projectName,
				srcFolders,
				Collections.emptyList(),
				reqBundle,
				Collections.emptyList(),
				Collections.emptyList(),
				monitor,
				true
		);
		
		if (project == null) {
			throw new DIMEAppWizardException("Failed to create project");
		}

		return project;
	}

	private void generateDotProject(File destination, String projectName) throws DIMEAppWizardException {
		generate(destination, new DotProjectGenerator(projectName), ".project");
	}

	private void generateGitLabCI(File destination, String dimeVersion) throws DIMEAppWizardException {
		generate(destination, new GitLabCIGenerator(dimeVersion), ".gitlab-ci.yml");
	}

	private void generate(File destination, SimpleGeneratorInterface generator, String filename)
			throws DIMEAppWizardException {
		try {
			writeFile(
				new ByteArrayInputStream(generator.generate().toString().getBytes()),
				new File(destination, filename)
			);
		} catch (IOException e) {
			throw new DIMEAppWizardException("Failed to generate " + filename + " file", e);
		}
	}

	private void copyFiles(File destinationFile, URL selectedProjectURL, URL commons) throws DIMEAppWizardException {
		try {
			File sourceFile = sourceFile(selectedProjectURL);
			File commonsFile = sourceFile(commons);
			copy(sourceFile, destinationFile);
			copy(commonsFile, destinationFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new DIMEAppWizardException("Failed to copy static files", e);
		}
	}

	private void writeFile(InputStream fis, File destinationFile) throws IOException {
		byte[] buffer = new byte[1000];
		destinationFile.createNewFile();
		BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(destinationFile));
		int count;
		while ((count = fis.read(buffer)) != -1) {
			fos.write(buffer, 0, count);
		}
		fis.close();
		fos.close();
	}

	private void copy(File sourceFile, File destinationFile) throws IOException {
		if (sourceFile.isDirectory()) {
			destinationFile.mkdir();
			for (File file : sourceFile.listFiles()) {
				File newDest = new File(destinationFile, file.getName());
				copy(file, newDest);
			}
		} else {
			if (!destinationFile.exists()) {
				BufferedInputStream fis = new BufferedInputStream(new FileInputStream(sourceFile));
				writeFile(fis, destinationFile);
			}
		}
	}

	private File sourceFile(URL selectedProjectURL) throws IOException {
		URL url = FileLocator.toFileURL(selectedProjectURL);
		URI uri = URI.createFileURI(url.getPath());
		return new File(uri.toFileString());		
	}

}
