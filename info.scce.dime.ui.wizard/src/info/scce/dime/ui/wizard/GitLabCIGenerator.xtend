/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.wizard

class GitLabCIGenerator implements SimpleGeneratorInterface {

	String dimeVersion;

	new(String dimeVersion) {
		this.dimeVersion = dimeVersion;
	}

	override generate() '''
{
  "stages": [
      "generate",
      "webapp-build",
      "dywa-app-build",
      "package",
      "deliver"
  ],
  ".docker-job": {
    "image": "docker:19.03.0",
    "services": [
        "docker:19.03.0-dind"
    ],
    "variables": {
      "DOCKER_DRIVER": "overlay",
      "DOCKER_TLS_CERTDIR": "/certs"
    }
  },
  "generate": {
    "stage": "generate",
    "image": "registry.gitlab.com/scce/dime:«dimeVersion»",
    "script": [
        "cd /DIME",
        "./dime-headless.sh $CI_PROJECT_DIR/"
    ],
    "artifacts": {
      "expire_in": "1 days",
      "paths": [
          "target"
      ]
    }
  },
  "webapp-build": {
    "image": "google/dart:2.10.5",
    "stage": "webapp-build",
    "needs": [
        "generate"
    ],
    "script": [
        "cd target/webapp",
        "pub global activate webdev 2.6.2",
        "pub get",
        "/root/.pub-cache/bin/webdev build"
    ],
    "artifacts": {
      "expire_in": "1 days",
      "paths": [
          "target/webapp/build"
      ]
    }
  },
  "dywa-app-build": {
    "image": "maven:3.8.1-jdk-8",
    "stage": "dywa-app-build",
    "needs": [
        "generate",
        "webapp-build"
    ],
    "script": [
        "mkdir -p target/dywa-app/app-presentation/target/pub-build",
        "cp -r target/webapp/build/* target/dywa-app/app-presentation/target/pub-build/web/",
        "cd target/dywa-app",
        "mvn install -P!frontend"
    ],
    "artifacts": {
      "expire_in": "1 days",
      "paths": [
          "target/dywa-app/app-ear/target/app-1.0-SNAPSHOT.ear",
          "target/docker",
          "target/production.Dockerfile"
      ]
    }
  },
  "package": {
    "extends": ".docker-job",
    "stage": "package",
    "needs": [
        "dywa-app-build"
    ],
    "script": [
        "cd target",
        "docker build -t $CI_REGISTRY_IMAGE/wildfly:$CI_COMMIT_SHA -f production.Dockerfile .",
        "docker save $CI_REGISTRY_IMAGE/wildfly:$CI_COMMIT_SHA | gzip > image.tar.gz"
    ],
    "artifacts": {
      "expire_in": "1 days",
      "paths": [
          "target/image.tar.gz"
      ]
    }
  },
  "deliver": {
    "extends": ".docker-job",
    "stage": "deliver",
    "needs": [
        "package"
    ],
    "before_script": [
        "docker login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    ],
    "script": [
        "docker load < target/image.tar.gz",
        "docker push $CI_REGISTRY_IMAGE/wildfly:$CI_COMMIT_SHA"
    ],
    "after_script": [
        "docker logout $CI_REGISTRY"
    ],
    "except": [
        "schedules"
    ]
  }
}
		'''

}
