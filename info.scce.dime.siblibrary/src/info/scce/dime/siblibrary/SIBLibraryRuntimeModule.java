/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.siblibrary;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.linking.ILinker;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.linking.impl.DefaultLinkingService;
import org.eclipse.xtext.linking.impl.IllegalNodeException;
import org.eclipse.xtext.linking.impl.Linker;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.parser.DefaultEcoreElementFactory;
import org.eclipse.xtext.parser.IAstFactory;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;

import com.google.inject.Inject;

import graphmodel.internal.InternalIdentifiableElement;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class SIBLibraryRuntimeModule extends info.scce.dime.siblibrary.AbstractSIBLibraryRuntimeModule {
	
	@Override
	public Class<? extends IAstFactory> bindIAstFactory() {
		return MyAstFactory.class;
	}
	
	static class MyAstFactory extends DefaultEcoreElementFactory {

		@Override
		public void add(EObject object, String feature, Object value, String ruleName, INode node) throws ValueConverterException {
			if (node.getSemanticElement() instanceof SIB) try {
				SIB sib = (SIB) node.getSemanticElement();
				if (object instanceof SIBLibrary) {
					SIBLibrary sibLib = (SIBLibrary) object;
					Package pkg = sibLib.getPackage();
					String sibId = pkg.getName() + "." + sib.getName();
					sib.setId(sibId);
				}
			} catch(Exception e) {
		    		e.printStackTrace();
		    }
			super.add(object, feature, value, ruleName, node);
		}
		
    }

	@Override
    public Class<? extends IValueConverterService> bindIValueConverterService() {
        return SIBLibraryValueConverterService.class;
    }

	public java.lang.Class<? extends ILinker> bindILinker() {
		return GUIPluginLinker.class;
	}
	
	public static class GUIPluginLinker extends Linker {
		@Override
		protected boolean isClearAllReferencesRequired(Resource resource) {
			return false;
		}
	}
	
	@Override
	public Class<? extends ILinkingService> bindILinkingService() {
		return SIBLibraryPluginLinkingService.class;
	}
	
	public static class SIBLibraryPluginLinkingService extends DefaultLinkingService {
		
		@Inject
		private IQualifiedNameConverter qualifiedNameConverter;
		
		@Override
		public List<EObject> getLinkedObjects(EObject context, EReference ref, INode node) throws IllegalNodeException {
			String crossRefString = getCrossRefNodeAsString(node);
			if (crossRefString == null || crossRefString.isEmpty())
				return Collections.emptyList();
			
			IScope scope = getScope(context, ref);
			QualifiedName qualifiedLinkName =  qualifiedNameConverter.toQualifiedName(crossRefString);
			IEObjectDescription eObjectDescription = scope.getSingleElement(qualifiedLinkName);
			
			if (eObjectDescription == null) {
				crossRefString += "_INTERNAL";
				qualifiedLinkName = qualifiedNameConverter.toQualifiedName(crossRefString);
				eObjectDescription = scope.getSingleElement(qualifiedLinkName);
			}
			
			if (eObjectDescription != null) {
				EObject eObj = eObjectDescription.getEObjectOrProxy();
				if (eObj instanceof InternalIdentifiableElement) {
					eObj = ((InternalIdentifiableElement)eObj).getElement();
				}
				return Collections.singletonList(eObj);
			}
			
			return Collections.emptyList();
		}
	}
}
