/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.siblibrary.validation

import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import info.scce.dime.siblibrary.Import
import info.scce.dime.siblibrary.SIB
import info.scce.dime.siblibrary.SIBLibraryPackage
import org.eclipse.xtext.validation.Check

/**
 * Custom validation rules.
 * 
 * @author Steve Bosselmann
 */
class SIBLibraryValidator extends AbstractSIBLibraryValidator {

	protected extension ResourceExtension = new ResourceExtension

	@Check
	def checkImport(Import it) {
		if (path.nullOrEmpty)
			error("No path specified", SIBLibraryPackage.Literals.IMPORT__PATH)
		else if (!eResource?.project?.getFile(path)?.exists)
			error("The specified file does not exist", SIBLibraryPackage.Literals.IMPORT__PATH)
	}
	
	@Check
	def checkSIB(SIB sib) {
		if (!sib.iconPath.nullOrEmpty) {
			val file = sib.eResource.project.getFile(sib.iconPath)
			if (!file.exists) {
				error("The specified file does not exist", SIBLibraryPackage.Literals.SIB__ICON_PATH)
			}
		}
	}
}
