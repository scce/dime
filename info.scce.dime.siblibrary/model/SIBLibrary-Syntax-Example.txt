sib Add: info.scce.product.process.atomicsibs.IntegerMath#add
   summand1: int
   summand2: int
   -> success
      sum: int
   -> error
      errorCode: int

sib Mult: info.scce.product.process.atomicsibs.IntegerMath#mult
   factor1: int
   factor2: int
   -> success
      product: int
   -> error
      errorCode: int

sib Equals: info.scce.product.process.atomicsibs.IntegerMath#equals
   a: int
   b: int
   -> yes
   -> no

sib Negate: info.scce.product.process.atomicsibs.IntegerMath#negate
   number: int
   -> success
      negated: int
   -> error
      errorCode: int
