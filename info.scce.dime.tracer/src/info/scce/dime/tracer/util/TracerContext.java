/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.util;

import info.scce.dime.tracer.interfaces.ModelAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class TracerContext {

	private HashMap<String, ContextVariable> context = new HashMap<String, ContextVariable>();

	private ModelAdapter modelAdapter = null;

	private List<ContextVariable> inputs = null;
	private List<ContextVariable> outputs = null;

	private TracerNode activeNode = null;
	private TracerNode nextNode = null;

	private boolean running = false;
	private String exitBranch = null;

	public List<ContextVariable> getInputs() {
		return inputs;
	}

	public void setInputs(List<ContextVariable> inputs) {
		this.inputs = inputs;
	}

	public List<ContextVariable> getOutputs() {
		return outputs;
	}

	public void setOutputs(List<ContextVariable> outputs) {
		this.outputs = outputs;
	}

	public String getExitBranch() {
		return exitBranch;
	}

	public void setExitBranch(String exitBranch) {
		this.exitBranch = exitBranch;
	}

	public HashMap<String, ContextVariable> getContext() {
		return context;
	}

	public ModelAdapter getModelAdapter() {
		return modelAdapter;
	}

	public TracerNode getActiveNode() {
		return activeNode;
	}

	public TracerNode getNextNode() {
		return nextNode;
	}

	public boolean isRunning() {
		return running;
	}

	public TracerContext(ModelAdapter modelAdapter) {
		super();
		this.modelAdapter = modelAdapter;
	}

	public ContextVariable getById(String id) {
		return context.get(id);
	}

	public ContextVariable getByName(String name) {
		for (ContextVariable var : context.values()) {
			if (var.getName().equals(name))
				return var;
		}
		return null;
	}

	public List<String> getContextVariableNames() {
		List<String> names = new ArrayList<>();
		for (ContextVariable var : context.values()) {
			names.add(var.getName());
		}
		return names;
	}

	public void createContextVariable(String id, String name, Class<?> type) {
		ContextVariable var = new ContextVariable();
		var.setId(id);
		var.setName(name);
		var.setType(type);
		context.put(id, var);
	}

	public Collection<ContextVariable> getVariables() {
		return context.values();
	}

	public void initializeRun() throws Exception {
		context = new HashMap<String, ContextVariable>();
		running = true;
		modelAdapter.createContextVariables(this);
		activeNode = modelAdapter.getStartNode();
	}

	public void executeNextStep(TracerEnvironment env, boolean stepInto)
			throws Exception {
		nextNode = activeNode.execute(env, stepInto);
		activeNode = nextNode;
		if (nextNode == null)
			running = false;
	}
}
