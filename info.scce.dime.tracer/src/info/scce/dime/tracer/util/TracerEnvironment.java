/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.util;

import java.util.Stack;

import info.scce.dime.tracer.interfaces.ModelAdapter;
import info.scce.dime.tracer.interfaces.TracerUIAdapter;

public class TracerEnvironment {

	private Stack<TracerContext> callStack = new Stack<>();
	private TracerContext activeContext = null;

	private TracerUIAdapter uiAdapter = null;

	public TracerEnvironment(TracerUIAdapter uiAdapter) {
		super();
		this.uiAdapter = uiAdapter;
	}

	public Stack<TracerContext> getCallStack() {
		return callStack;
	}

	public TracerContext getActiveContext() {
		return activeContext;
	}

	public TracerUIAdapter getUiAdapter() {
		return uiAdapter;
	}

	public boolean isRunning() {
		return !callStack.isEmpty();
	}

	public void initializeRun(ModelAdapter modelAdapter) throws Exception {
		TracerContext initContext = new TracerContext(modelAdapter);
		initContext.initializeRun();
		callStack.push(initContext);
		activeContext = initContext;
		uiAdapter.postInitialize(this);
	}

	public void executeNextStep(boolean stepInto) throws Exception {
		if (callStack.size() <= 0)
			return;
		
		activeContext = callStack.pop();

		if (activeContext.isRunning()) {
			uiAdapter.preExecute(this);
			activeContext.executeNextStep(this, stepInto);
			uiAdapter.postExecute(this);
		}
	}

	public void reset() {
		callStack = new Stack<TracerContext>();
		activeContext = null;
	}

	public TracerContext getNextContext() {
		if (!callStack.isEmpty())
			return callStack.peek();
		return null;
	}

}
