/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.views;

import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.impl.TracerConsoleAdapter;
import info.scce.dime.tracer.impl.TracerEclipseAdapter;
import info.scce.dime.tracer.util.TracerEnvironment;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.internal.EditorReference;
import org.eclipse.ui.part.*;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.*;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.*;
import org.eclipse.swt.widgets.Menu;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the model. The sample creates a dummy model on the
 * fly, but a real implementation would connect to the model available either in
 * this or another plug-in (e.g. the workspace). The view is connected to the
 * model using a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 */

@SuppressWarnings({ "restriction" })
public class TracerView extends ViewPart implements IPartListener2 {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "info.scce.dime.process.tracer.views.TracerView";

	private Composite parent = null;

	private TracerEnvironment tracerEnvironment = null;
	private TracerEnvironment tracerConsoleEnvironment = null;

	private TracerViewInformation activeTracerView = null;

	private HashMap<File, TracerViewInformation> tracerInfoMap = new HashMap<>();

	private TableViewer viewer;
	private Action start_stop_action;
	private Action run_action;
	private Action step_over_action;
	private Action step_into_action;

	private Action start_console_action;

	private Bundle bundle = FrameworkUtil.getBundle(this.getClass());
	private URL urlStartTrace = FileLocator.find(bundle, new Path(
			"icons/run_obj.gif"), null);
	private URL urlProcessTrace = FileLocator.find(bundle, new Path(
			"icons/resume_co.gif"), null);
	private URL urlStepOverTrace = FileLocator.find(bundle, new Path(
			"icons/stepover_co.gif"), null);
	private URL urlStopTrace = FileLocator.find(bundle, new Path(
			"icons/terminatedlaunch_obj.gif"), null);
	private URL urlStepIntoTrace = FileLocator.find(bundle, new Path(
			"icons/stepinto_co.gif"), null);

	// private Action doubleClickAction;

	/**
	 * The constructor.
	 */
	public TracerView() {
		TracerEclipseAdapter uiAdapter = new TracerEclipseAdapter(this);
		tracerEnvironment = new TracerEnvironment(uiAdapter);

		TracerConsoleAdapter consoleAdapter = new TracerConsoleAdapter();
		tracerConsoleEnvironment = new TracerEnvironment(consoleAdapter);
	}

	public Composite getParent() {
		return parent;
	}

	public HashMap<File, TracerViewInformation> getTracerInfoMap() {
		return tracerInfoMap;
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		this.parent = parent;

		this.parent.setLayout(new GridLayout(1, true));
		this.parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.addPartListener(this);

		// Create the help context id for the viewer's control
		// PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(),
		// "info.scce.dime.process.tracer.viewer");
		makeActions();
		// hookContextMenu();
		// hookDoubleClickAction();
		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				TracerView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		// manager.add(action1);
		// manager.add(new Separator());
		// manager.add(action2);
		
		manager.add(start_console_action);
	}

	private void fillContextMenu(IMenuManager manager) {
		// manager.add(action1);
		// manager.add(action2);
		// // Other plug-ins can contribute there actions here
		// manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(start_stop_action);
		manager.add(run_action);
		manager.add(step_over_action);
		manager.add(step_into_action);
	}

	private void refreshView() {
		if (activeTracerView != null) {
			if (!tracerEnvironment.isRunning()) {
				run_action.setEnabled(false);
				step_over_action.setEnabled(false);
				step_into_action.setEnabled(false);
				start_stop_action.setImageDescriptor(ImageDescriptor
						.createFromURL(urlStartTrace));
			} else {
				run_action.setEnabled(true);
				step_over_action.setEnabled(true);
				step_into_action.setEnabled(true);
				start_stop_action.setImageDescriptor(ImageDescriptor
						.createFromURL(urlStopTrace));
			}
			activeTracerView.refresh();

			if (!parent.isDisposed()) {
				parent.layout(true);
				parent.redraw();
				parent.update();
			}
		}
	}

	private void showError(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();

		e.printStackTrace();

		MessageDialog.openError(parent.getShell(), "Error: "
				+ e.getClass().getSimpleName(), e.getMessage());
	}

	private void makeActions() {
		start_stop_action = new Action() {
			public void run() {
				if (activeTracerView != null) {
					if (!tracerEnvironment.isRunning()) {
						try {
							ProcessModelAdapter modelAdapter = new ProcessModelAdapter();
							modelAdapter.setModel(
									activeTracerView.getResource(),
									activeTracerView.getFile());
							tracerEnvironment.initializeRun(modelAdapter);
						} catch (Exception e) {
							showError(e);
						}
					} else {
						tracerEnvironment.reset();
					}
					refreshView();
				}
			}
		};
		start_stop_action.setText("|>");
		start_stop_action.setToolTipText("Start/Stop Tracer");
		start_stop_action.setImageDescriptor(ImageDescriptor
				.createFromURL(urlStartTrace));

		run_action = new Action() {
			public void run() {
				if (activeTracerView != null) {
					while (tracerEnvironment.isRunning()) {
						try {
							tracerEnvironment.executeNextStep(false);
						} catch (Exception e) {
							showError(e);
						}
					}
					refreshView();
				}
			}
		};
		run_action.setText(">>");
		run_action.setToolTipText("Execute complete run");
		run_action.setImageDescriptor(ImageDescriptor
				.createFromURL(urlProcessTrace));
		run_action.setEnabled(false);

		step_over_action = new Action() {
			public void run() {
				if (activeTracerView != null) {
					try {
						tracerEnvironment.executeNextStep(false);
					} catch (Exception e) {
						showError(e);
					}
					refreshView();
				}
			}
		};
		step_over_action.setText("Step over");
		step_over_action.setToolTipText("Execute next Step");
		step_over_action.setImageDescriptor(ImageDescriptor
				.createFromURL(urlStepOverTrace));
		step_over_action.setEnabled(false);

		step_into_action = new Action() {
			public void run() {
				if (activeTracerView != null) {
					try {
						tracerEnvironment.executeNextStep(true);
					} catch (Exception e) {
						showError(e);
					}
					refreshView();
				}
			}
		};
		step_into_action.setText("Step into");
		step_into_action.setToolTipText("Step into");
		step_into_action.setImageDescriptor(ImageDescriptor
				.createFromURL(urlStepIntoTrace));
		step_into_action.setEnabled(false);

		start_console_action = new Action() {
			public void run() {
				if (!tracerConsoleEnvironment.isRunning()) {
					try {
						ProcessModelAdapter modelAdapter = new ProcessModelAdapter();
						modelAdapter.setModel(activeTracerView.getResource(),
								activeTracerView.getFile());
						tracerConsoleEnvironment.initializeRun(modelAdapter);
						while (tracerConsoleEnvironment.isRunning()) {
							tracerConsoleEnvironment.executeNextStep(false);
						}
					} catch (Exception e) {
						showError(e);
					}
				} else {
					tracerConsoleEnvironment.reset();
				}
				refreshView();
			}
		};
		start_console_action.setText("trace on console");
		start_console_action.setToolTipText("Start Console Tracer");
		start_console_action.setImageDescriptor(ImageDescriptor
				.createFromURL(urlStartTrace));

		// doubleClickAction = new Action() {
		// public void run() {
		// ISelection selection = viewer.getSelection();
		// Object obj = ((IStructuredSelection)selection).getFirstElement();
		// showMessage("Double-click detected on "+obj.toString());
		// }
		// };
	}

	private void hookDoubleClickAction() {
		// viewer.addDoubleClickListener(new IDoubleClickListener() {
		// public void doubleClick(DoubleClickEvent event) {
		// doubleClickAction.run();
		// }
		// });
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		// viewer.getControl().setFocus();
	}

	@Override
	public void partActivated(IWorkbenchPartReference partRef) {
		// System.out.println("Part active: " + partRef.getTitle());
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference partRef) {
		// System.out.println("Part to top: " + partRef.getTitle());
	}

	@Override
	public void partClosed(IWorkbenchPartReference partRef) {
		if (partRef instanceof EditorReference) {
			IFile iFile = (IFile) ((EditorReference) partRef).getEditor(false)
					.getEditorInput().getAdapter(IFile.class);

			if (iFile != null) {
				String path = iFile.getRawLocation().toOSString();
				File file = new File(path);

				// System.out.println("Closed File: " + origFile.getName());

				if (tracerInfoMap.keySet().contains(file)) {
					TracerViewInformation closingTracerView = tracerInfoMap
							.get(file);
					closingTracerView.closeView();
					tracerInfoMap.remove(file);

					if (activeTracerView == closingTracerView)
						activeTracerView = null;

					if (!parent.isDisposed()) {
						parent.layout(true);
						parent.redraw();
						parent.update();
					}
				}
			}
		}
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference partRef) {
		// System.out.println("Part deactivated: " + partRef.getTitle());
	}

	@Override
	public void partOpened(IWorkbenchPartReference partRef) {
		// System.out.println("Part opened: " + partRef.getTitle());
	}

	@Override
	public void partHidden(IWorkbenchPartReference partRef) {
		// System.out.println("Part hidden: " + partRef.getTitle());
	}

	@Override
	public void partVisible(IWorkbenchPartReference partRef) {
		if (partRef instanceof EditorReference) {
			IEditorPart editor = ((EditorReference) partRef).getEditor(false);

			IFile file = (IFile) editor.getEditorInput()
					.getAdapter(IFile.class);

			Resource res = null;

			if (editor instanceof DiagramEditor) {
				DiagramEditor deditor = (DiagramEditor) editor;
				TransactionalEditingDomain ed = deditor.getEditingDomain();
				ResourceSet rs = ed.getResourceSet();
				res = rs.getResources().get(0);
			}

			if (parent.isDisposed())
				return;

			for (Control child : parent.getChildren()) {
				child.setVisible(false);
				((GridData) child.getLayoutData()).exclude = true;
			}

			if (file != null && res != null) {
				File origFile = new File(file.getRawLocation().toOSString());
				if (origFile.exists()) {

					if (!tracerInfoMap.keySet().contains(origFile)) {
						TracerViewInformation traceInfo = new TracerViewInformation(
								origFile, res, tracerEnvironment);
						traceInfo.createTableView(parent);
						tracerInfoMap.put(origFile, traceInfo);
					}

					activeTracerView = tracerInfoMap.get(origFile);
					activeTracerView.getViewer().getTable().setVisible(true);
					((GridData) activeTracerView.getViewer().getTable()
							.getLayoutData()).exclude = false;
					refreshView();
					parent.layout();
				}
			}
		}

		if (!parent.isDisposed()) {
			parent.layout(true);
			parent.redraw();
			parent.update();
		}
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference partRef) {
		// System.out.println("Part input changed: " + partRef.getTitle());
	}

}
