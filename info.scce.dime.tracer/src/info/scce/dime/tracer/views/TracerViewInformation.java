/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.views;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import info.scce.dime.tracer.impl.ProcessTypeMapper;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerEnvironment;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class TracerViewInformation {

	private File file = null;
	private Resource resource = null;
	private TableViewer viewer = null;
	private TracerEnvironment environment = null;

	/*
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */

	class ViewContentProvider implements IStructuredContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}

		public void dispose() {
		}

		public Object[] getElements(Object parent) {
			return new String[] { "One", "Two", "Three" };
		}
	}

	class ViewLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}

		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}

		public Image getImage(Object obj) {
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}
	}

	class NameSorter extends ViewerSorter {
	}

	public File getFile() {
		return file;
	}

	public Resource getResource() {
		return resource;
	}

	public TableViewer getViewer() {
		return viewer;
	}

	public TracerEnvironment getEnvironment() {
		return environment;
	}

	public void closeView() {
		viewer.getControl().dispose();
	}

	public TracerViewInformation(File file, Resource resource,
			TracerEnvironment environment) {
		super();
		this.file = file;
		this.resource = resource;
		this.environment = environment;
	}

	private Collection<ContextVariable> getContextVariables() {
		if (environment.getNextContext() != null)
			return environment.getNextContext().getVariables();
		if (environment.getActiveContext() != null)
			return environment.getActiveContext().getVariables();
		return new ArrayList<ContextVariable>();
	}

	public void refresh() {
		viewer.setInput(getContextVariables());
		viewer.refresh();
		viewer.getTable().layout();
	}

	public void createTableView(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL);
		createColumns(parent);

		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setComparator(new ViewerComparator() {
			public int compare(Viewer viewer, Object e1, Object e2) {
				ContextVariable cv1 = (ContextVariable) e1;
				ContextVariable cv2 = (ContextVariable) e2;
				return cv1.getName().compareTo(cv2.getName());
			};
		});
		viewer.setInput(getContextVariables());
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setLayout(new GridLayout(1, false));
		viewer.getTable().setLayoutData(
				new GridData(SWT.FILL, SWT.FILL, true, true));
	}

	private void createColumns(Composite parent) {
		// create a column for the first name
		TableViewerColumn colVarName = new TableViewerColumn(viewer, SWT.NONE);
		colVarName.getColumn().setWidth(150);
		colVarName.getColumn().setText("Name");
		colVarName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				ContextVariable var = (ContextVariable) element;
				return var.getName();
			}
		});

		TableViewerColumn colVarValue = new TableViewerColumn(viewer, SWT.NONE);
		colVarValue.getColumn().setWidth(150);
		colVarValue.getColumn().setText("Value");
		colVarValue.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				ContextVariable var = (ContextVariable) element;
				if (var.getValue() != null)
					return var.getValue().toString();
				else
					return "";
			}
		});

		TableViewerColumn colVarType = new TableViewerColumn(viewer, SWT.NONE);
		colVarType.getColumn().setWidth(150);
		colVarType.getColumn().setText("Type");
		colVarType.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				ContextVariable var = (ContextVariable) element;
				if (var.getType() != null)
					return ProcessTypeMapper.getPrimitiveTypeForClass(var.getType()).getLiteral();
				else
					return "";
			}
		});
	}

}
