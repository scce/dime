/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.AtomicSIB;
import info.scce.dime.process.process.Branch;
import info.scce.dime.siblibrary.SIB;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class AtomicNode extends DataFlowTargetNode<AtomicSIB> {

	public AtomicNode(AtomicSIB sib) {
		super(sib);
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		adapter = (ProcessModelAdapter) env.getActiveContext()
				.getModelAdapter();

		TracerContext context = env.getActiveContext();

		List<ContextVariable> inputParameters = new ArrayList<ContextVariable>();
		inputParameters.addAll(mapInputPort(context));
		inputParameters.addAll(mapInputStatic(context));
		Object returnValue = null;

		SIB atomicSib = (SIB) sib.getSib();

		if (atomicSib == null)
			throw new Exception("Could not find Sib (EObject) for AtomicSib!");

		returnValue = invokeAtomicSIB(inputParameters, atomicSib);
		TracerNode nextNode = getNextNode(env, returnValue);
		env.getCallStack().push(context);
		return nextNode;
	}

	@SuppressWarnings("rawtypes")
	private Object invokeAtomicSIB(List<ContextVariable> inputParameters,
			SIB sib) throws ClassNotFoundException, NoSuchMethodException,
			SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		ArrayList<Object> inputParameterValues = new ArrayList<>();
		Class[] inputParameterClasses = new Class[inputParameters.size()];
		int i = 0;

		for (ContextVariable inputParameter : inputParameters) {
			inputParameterValues.add(inputParameter.getValue());
			inputParameterClasses[i] = inputParameter.getType(); // ProcessTypeMapper.getSimpleType(inputParameter.getType());
			i++;
		}

		// System.out.println("Trying to invoke: " + sib.getExecutorClass() +
		// " - " + sib.getExecutorMethod());

		Class<?> exeClass;
		exeClass = Class.forName(sib.getExecutorClass());
		Method exeMethod = exeClass.getMethod(sib.getExecutorMethod(),
				inputParameterClasses);
		return exeMethod.invoke(this, inputParameterValues.toArray());
	}

	private TracerNode getNextNode(TracerEnvironment env, Object returnValue)
			throws Exception {
		Branch branch = null;
		String branchName = "";
		ContextVariable cv = null;

		if (returnValue instanceof Boolean) {
			Boolean value = (Boolean) returnValue;
			if (value) {
				branchName = "yes";
				branch = getBranchForBranchName(branchName);

			} else {
				branchName = "no";
				branch = getBranchForBranchName(branchName);
			}
		}

		if (returnValue != null)
			if (returnValue.getClass() != null) {
				if (returnValue.getClass().isEnum()) {
					branch = getBranchForBranchName(returnValue.toString());
				}
			}

		if (returnValue instanceof Exception) {
			branchName = "error";
			branch = getBranchForBranchName(branchName);
			cv = new ContextVariable();
			cv.setValue(returnValue);
		} else {
			if (branch == null) {
				branchName = "success";
				branch = getBranchForBranchName(branchName);
				cv = new ContextVariable();
				cv.setValue(returnValue);
			}
		}

		if (branch == null)
			throw new Exception("Could not find branch '" + branchName + "'!");

		BranchNode bn = new BranchNode(branch);
		return bn.executeAfterAtomicSib(env, cv);
	}

	private Branch getBranchForBranchName(String branchName) {
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if (branch.getName().equals(branchName)) {
				return branch;
			}
		}
		return null;
	}

}
