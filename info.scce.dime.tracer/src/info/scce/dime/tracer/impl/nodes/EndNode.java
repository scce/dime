/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.EndSIB;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

import java.util.ArrayList;
import java.util.List;

public class EndNode extends DataFlowTargetNode<EndSIB> {

	public EndNode(EndSIB sib) {
		super(sib);
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		adapter = (ProcessModelAdapter) env.getActiveContext()
				.getModelAdapter();

		TracerContext context = env.getActiveContext();

		List<ContextVariable> outputs = new ArrayList<ContextVariable>();
		outputs.addAll(mapInputPort(context));
		outputs.addAll(mapInputStatic(context));

		context.setOutputs(outputs);
		context.setExitBranch(sib.getBranchName());
		return null;
	}


}
