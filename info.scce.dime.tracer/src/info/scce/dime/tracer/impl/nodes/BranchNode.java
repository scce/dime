/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

import java.util.List;

public class BranchNode extends DataFlowSourceNode<Branch> {

	public BranchNode(Branch sib) {
		super(sib);
		// TODO Auto-generated constructor stub
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public TracerNode executeAfterAtomicSib(TracerEnvironment env,
			ContextVariable returnValue) throws Exception {
		adapter = (ProcessModelAdapter) env.getActiveContext()
				.getModelAdapter();
		
		TracerContext context = env.getActiveContext();

		for (Output output : sib.getOutputs()) {
			if (output instanceof PrimitiveOutputPort) {
				writePrimitive(context, (PrimitiveOutputPort) output,
						returnValue);
			}
			if (output instanceof ComplexOutputPort) {
				writeComplex(context, (ComplexOutputPort) output, returnValue);
			}
		}
		
		return adapter.createTracerNode(sib.getDataFlowTargetSuccessors()
				.get(0));
	}
	
	public TracerNode executeAfterProcessSib(TracerEnvironment env,
			List<ContextVariable> returnValues) throws Exception {
		adapter = (ProcessModelAdapter) env.getActiveContext()
				.getModelAdapter();
		
		TracerContext context = env.getActiveContext();

		for (ContextVariable returnValue : returnValues) {
			for (Output output : sib.getOutputs()) {
				if (!returnValue.getName().equals(output.getName()))
					continue;
				
				if (output instanceof PrimitiveOutputPort) {
					writePrimitive(context, (PrimitiveOutputPort) output,
							returnValue);
				}
				if (output instanceof ComplexOutputPort) {
					writeComplex(context, (ComplexOutputPort) output, returnValue);
				}
			}
		}
		return adapter.createTracerNode(sib.getDataFlowTargetSuccessors()
				.get(0));
	}

}
