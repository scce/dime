/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramsPackage;

import graphmodel.Container;
import graphmodel.Edge;
import graphmodel.Node;
import info.scce.dime.process.process.AtomicSIB;
import info.scce.dime.process.process.BooleanInputStatic;
import info.scce.dime.process.process.CreateSIB;
import info.scce.dime.process.process.DataContext;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.IntegerInputStatic;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessPackage;
import info.scce.dime.process.process.ProcessSIB;
import info.scce.dime.process.process.RealInputStatic;
import info.scce.dime.process.process.SetAttributeValueSIB;
import info.scce.dime.process.process.StartSIB;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;
import info.scce.dime.process.process.UnsetAttributeValueSIB;
import info.scce.dime.tracer.impl.nodes.AtomicNode;
import info.scce.dime.tracer.impl.nodes.CreateNode;
import info.scce.dime.tracer.impl.nodes.DataFlowSourceNode;
import info.scce.dime.tracer.impl.nodes.DataFlowTargetNode;
import info.scce.dime.tracer.impl.nodes.EndNode;
import info.scce.dime.tracer.impl.nodes.ProcessNode;
import info.scce.dime.tracer.impl.nodes.SetAttributeNode;
import info.scce.dime.tracer.impl.nodes.StartNode;
import info.scce.dime.tracer.impl.nodes.UnsetAttributeNode;
import info.scce.dime.tracer.interfaces.ModelAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;

public class ProcessModelAdapter implements ModelAdapter {

	private String modelName;
	private File modelFile;
	private Diagram diagram;
	private Process model;
	private Process modelWrapper;

	@Override
	public String getModelName() {
		return modelName;
	}

	@Override
	public File getModelFile() {
		return modelFile;
	}

	public Diagram getDiagram() {
		return diagram;
	}

	public Process getModel() {
		return model;
	}

	@Override
	public EObject getModelEObject() {
		return model;
	}

	@Override
	public TracerNode getStartNode() {
		return new StartNode(model.getStartSIBs().get(0));
	}

	private Object getObjectById(String id) {
		for (Node node : model.getAllNodes()) {
			if (node.getId().equals(id))
				return node;
		}
		for (Container container : model.getAllContainers()) {
			if (container.getId().equals(id))
				return container;
		}
		for (Edge edge : model.getAllEdges()) {
			if (edge.getId().equals(id))
				return edge;
		}
		return null;
	}

	@Override
	public void createContextVariables(TracerContext context)
			throws ClassNotFoundException {
		for (DataContext dContext : model.getDataContexts()) {
			for (PrimitiveVariable pVar : dContext.getPrimitiveVariables()) {
				createContextVarForDataNode(pVar, context);
			}
		}
	}

	private void createContextVarForDataNode(PrimitiveVariable pVar,
			TracerContext context) throws ClassNotFoundException {
		String id = pVar.getId();
		String name = pVar.getName();
		Class<?> type = ProcessTypeMapper.getClassForPrimitiveVariable(pVar);
		context.createContextVariable(id, name, type);
	}


	public TracerNode createTracerNode(DataFlowTarget sib) {
		if (sib instanceof StartSIB) {
			return new StartNode((StartSIB) sib);
		}
		if (sib instanceof EndSIB) {
			return new EndNode((EndSIB) sib);
		}
		if (sib instanceof ProcessSIB) {
			return new ProcessNode((ProcessSIB) sib);
		}
		if (sib instanceof AtomicSIB) {
			return new AtomicNode((AtomicSIB) sib);
		}
		
		if (sib instanceof CreateSIB) {
			return new CreateNode((CreateSIB) sib);
		}
		if (sib instanceof SetAttributeValueSIB) {
			return new SetAttributeNode((SetAttributeValueSIB) sib);
		}
		if (sib instanceof UnsetAttributeValueSIB) {
			return new UnsetAttributeNode((UnsetAttributeValueSIB) sib);
		}
		
		return null;
	}
	
	public Object getValueForInputStatic(InputStatic input) {
		if (input instanceof BooleanInputStatic)
			return ((BooleanInputStatic) input).isValue();
		if (input instanceof IntegerInputStatic)
			return ((IntegerInputStatic) input).getValue();
		if (input instanceof RealInputStatic)
			return ((RealInputStatic) input).getValue();
		if (input instanceof TextInputStatic)
			return ((TextInputStatic) input).getValue();
		if (input instanceof TimestampInputStatic)
			return ((TimestampInputStatic) input).getValue();
		return null;
	}
	
	@Override
	public void readModel(File arg0) {
		modelFile = arg0;

		// Initialize the model
		ProcessPackage.eINSTANCE.eClass();
		PictogramsPackage.eINSTANCE.eClass();

		// Register the XMI resource factory for the .family extension
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("process", new XMIResourceFactoryImpl());

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		// Get the resource
		Resource resource = resSet.getResource(
				URI.createFileURI(arg0.getAbsolutePath()), true);
		// Get the first model element and cast it to the right type, in my
		// example everything is hierarchical included in this first node
		for (EObject obj : resource.getContents()) {
			if ("Diagram".equals(obj.eClass().getName()))
				diagram = (Diagram) obj;
			if ("Process".equals(obj.eClass().getName()))
				model = (Process) obj;
		}

		modelName = model.getModelName();
	}

	@Override
	public void setModel(Resource resource, File file) throws Exception {
		modelFile = file;

		for (EObject obj : resource.getContents()) {
			if ("Diagram".equals(obj.eClass().getName()))
				diagram = (Diagram) obj;
			if ("Process".equals(obj.eClass().getName()))
				model = (Process) obj;
		}

		modelName = model.getModelName();
		modelWrapper = model;
	}

	public void setModel(Process newModel) throws Exception {
		modelName = newModel.getModelName();

		URI uri = EcoreUtil.getURI(newModel);
		modelFile = new File(uri.path());
		// Resource resource = new ResourceSetImpl().getResource(uri, true);
		for (EObject obj : newModel.eResource().getContents()) {
			// for (EObject obj : resource.getContents()) {
			if ("Diagram".equals(obj.eClass().getName()))
				diagram = (Diagram) obj;
			if ("ProcessModel".equals(obj.eClass().getName()))
				model = (Process) obj;
		}

		modelWrapper = model;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void highlight(TracerNode node) {
		if (node instanceof DataFlowSourceNode<?>) {
			DataFlowSourceNode sibNode = (DataFlowSourceNode) node;
			sibNode.getSib().highlight();
		}
		
		if (node instanceof DataFlowTargetNode<?>) {
			DataFlowTargetNode sibNode = (DataFlowTargetNode) node;
			sibNode.getSib().highlight();
		}
	}

	@Override
	public List<ContextVariable> createInputList()
			throws ClassNotFoundException {
		List<ContextVariable> inputs = new ArrayList<>();

		StartSIB start = model.getStartSIBs().get(0);
		for (Output output : start.getOutputs()) {
			Class<?> outputClass = null;
			if (output instanceof PrimitiveOutputPort) {
				outputClass = ProcessTypeMapper
						.getClassForPrimitiveOutputPort((PrimitiveOutputPort) output);
			}
			
			ContextVariable cv = new ContextVariable();
			cv.setName(output.getName());
			cv.setType(outputClass);
			inputs.add(cv);
		}
		return inputs;
	}

}
