/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.tracer.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import info.scce.dime.tracer.interfaces.TracerUIAdapter;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

public class TracerConsoleAdapter implements TracerUIAdapter {

	@Override
	public void preExecute(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();

		System.out.println("- " + context.getModelAdapter().getModelName()
				+ ": " + context.getActiveNode().getClass().getSimpleName());

		if (env.getActiveContext().getInputs() == null) {
			List<ContextVariable> inputs = context.getModelAdapter()
					.createInputList();
			inputs = this.getInputs(inputs);
			env.getActiveContext().setInputs(inputs);
		}

	}

	@Override
	public void postExecute(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();
		TracerContext nextContext = env.getNextContext();
		
		if (nextContext == null && context.getOutputs() != null) {
			System.out.println("< Execution finished!");
			showOutputs(env.getActiveContext().getOutputs());
		}
	}

	@Override
	public void postInitialize(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();
		System.out.println("> initializing model '" + context.getModelAdapter().getModelName() + "' ...");
	}
	
	
	private List<ContextVariable> getInputs(List<ContextVariable> inputs) {
		for (ContextVariable cv : inputs) {
			boolean done = false;

			while (!done) {
				String msg = "Please enter value for parameter '" + cv.getName()
						+ "' (" + ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()).getLiteral() + "), " + ProcessTypeMapper.getInputPattern(ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()));

				
				System.out.println(msg);
				
				try {
					BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
					String valueString=buffer.readLine();

					if (valueString == null)
						return null;
					
					Object value = ProcessTypeMapper.convertInput(cv.getType(),
							valueString);
					cv.setValue(value);
					done = true;
				} catch (Exception e) {
					System.err.println(e.getClass().getSimpleName() + ": " + e.getMessage());
				}
			}
		}
		return inputs;
	}

	private void showOutputs(List<ContextVariable> outputs) {
		if (outputs.size() <= 0)
			return;
		
		String output = "Results: \n";
		for (ContextVariable cv : outputs) {
			String value = "null";
			if (cv.getValue() != null)
				value = cv.getValue().toString();

			output += cv.getName() + ": (" + ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()).getLiteral()
					+ ") " + value + "\n";
		}
		System.out.println(output);
	}

}
