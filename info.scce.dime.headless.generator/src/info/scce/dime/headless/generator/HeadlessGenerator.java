/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.headless.generator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;

import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.generator.dad.Generator;

public class HeadlessGenerator {
	
	class HeadlessGeneratorException extends Exception{

		private static final long serialVersionUID = 1L;

		public HeadlessGeneratorException(String message, Throwable cause) {
			super(message, cause);
		}
		
		public HeadlessGeneratorException(String message) {
			super(message);
		}
	}
	
	public String TARGET_FOLDER = "target";
	
	public enum Status{
		OK,
		ERROR
	}
	
	private IWorkspaceRoot workspaceRoot;
	private IWorkspace workspace;
	private IProgressMonitor monitor = new NullProgressMonitor();
	private WorkspaceExtension workEx = new WorkspaceExtension();
	private String graphPath;
	private IPath outlet;
	private DAD graph;
	private String importPath;
	
	
	public Job createGenerationJob(String importPath, String modelPath) {
		graphPath = modelPath;
		this.importPath = importPath;
		
		try {
			setUpWorkspace();
			loadDADModel();
			CompoundJob job = JobFactory.job("DIME Headless Generator", false);
			job.consume(100, "Starting Code Generation");
			collectCodeGenerationTasks(job);
			
			job.onFinished(this::finish);
			
			return job;
		} catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException("Headless generation failed...", e);
		}
			
	}
	
	private void collectCodeGenerationTasks(CompoundJob job) throws HeadlessGeneratorException {
		if (graph == null) {
			throw new HeadlessGeneratorException(String.format("Could not load model: %s", graphPath));
		}
		Generator dimeGraphGen = new Generator();
		dimeGraphGen.setHeadless(true);
		dimeGraphGen.collectTasks(graph, outlet, job);
	}
	
	private void finish() {
		System.out.println("Code Generation Finished");
		try {
			System.out.print("Refreshing workspace... ");
			workspaceRoot.refreshLocal(IWorkspaceRoot.DEPTH_INFINITE, monitor);
			System.out.println("done.");
			System.out.print("Saving workspace... ");
			workspace.save(true, monitor);
			System.out.println("done.");
		} catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Loads DAD Model from given parameter model Path Path is relative to given
	 * Workspace
	 * 
	 * @return DAD-Model
	 * @throws Exception
	 */
	@SuppressWarnings("restriction")
	private void loadDADModel() throws HeadlessGeneratorException {
		System.out.print("Loading DAD Model... ");

		URI uri = URI.createURI("platform:/resource/" + graphPath);
		System.out.println("URI to be loaded: " + uri.toString());
		IFile modelFile = workEx.getFile(uri);
		try {
			graph = workEx.getGraphModel(modelFile, DAD.class);
			System.out.println("done.");
		} catch (NullPointerException e) {
			throw new HeadlessGeneratorException(
				"NullPointerExcpetion caught. Check if the app's source folder matches the app name in the .project file! Compare https://gitlab.com/scce/dime/-/issues/823",
				e
			);
		}
	}

	@SuppressWarnings("restriction")
	private void setUpWorkspace() throws HeadlessGeneratorException {
		System.out.println("Setting up workspace...");
		workspace = workEx.getWorkspace();
		workspaceRoot = workspace.getRoot();
		importProjects();
		openProjects();
		initOutlet();
		refreshWorkspace();
		System.out.println("done.");
	}

	private void refreshWorkspace() throws HeadlessGeneratorException {
		System.out.print("  Refreshing workspace... ");
		try {
			workspaceRoot.refreshLocal(IWorkspaceRoot.DEPTH_INFINITE, monitor);
		} catch (CoreException e) {
			throw new HeadlessGeneratorException("Failed to refresh workspace", e);
		}
		System.out.println("done.");
	}

	private void openProjects() throws HeadlessGeneratorException {
		System.out.println("  Opening projects in workspace...");
		for (IProject project: workspaceRoot.getProjects()) {
			System.out.println("    Opening project " + project.getName());
			try {
				project.open(monitor);
			} catch (CoreException e) {
				throw new HeadlessGeneratorException("Failed to open pojects in workspace", e);
			}
		}
		System.out.println("  done.");
	}
	
	private void initOutlet() {
		System.out.println("  Initializing outlet...");
		IFile modelFile = workspaceRoot.getFile(Path.fromOSString(graphPath));
		IProject project = modelFile.getProject();
		outlet = project.getFolder(TARGET_FOLDER).getLocation();
		System.out.println("    Resolved outlet folder: " + outlet + " (exists: " + outlet.toFile().exists() + ")");
		if (!outlet.toFile().exists()) {
			System.out.println("    Creating outlet folder...");
			outlet.toFile().mkdirs();
			System.out.println("    done. (exists: " + outlet.toFile().exists() + ")");
		} else if (!outlet.toFile().isDirectory()) {
			throw new RuntimeException("Outlet exists, but is no directory: " + outlet);
		}
		System.out.println("  done.");
	}

	private void importProjects() throws HeadlessGeneratorException {
		if (importPath != null) {
			System.out.print("  Importing projects into workspace... ");
			try {
				ProjectImport.importProjectFromString(importPath);
			} catch (CoreException e) {
				throw new HeadlessGeneratorException("Failed to imports pojects in workspace", e);
			}
			System.out.println("done.");
		}
	}
	
}
