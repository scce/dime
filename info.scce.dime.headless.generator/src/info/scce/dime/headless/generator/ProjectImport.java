/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.headless.generator;

import java.io.File;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;

import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;

public class ProjectImport {

	public static Runnable importProject(String importPath,IProgressMonitor monitor) {
		
		
		return new Runnable() {
			
			@Override
			public void run() {
				try {
					//importOperation(importPath).run(monitor);
					importProjectFromString(importPath);
				} catch ( CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		};
	}
	
	
	public static IProject importProjectFromString(String importPath) throws CoreException{
		File baseDirectory = new File(importPath);
		IProjectDescription description = new WorkspaceExtension().getWorkspace().loadProjectDescription(
				new Path(baseDirectory.getAbsolutePath() + "/.project"));
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(description.getName());
		project.create(description, null);
		project.open(null);
		return project;
	}

//	
//	public static ImportOperation importOperation(String importPath) throws InvocationTargetException, InterruptedException {
//		IOverwriteQuery overwriteQuery = new IOverwriteQuery() {
//			public String queryOverwrite(String file) { return ALL; }
//		};
//		
//	IWorkspaceRoot wsRoot = new WorkspaceExtension().getWorkspaceRoot();
//	ImportOperation importOperation = new ImportOperation(wsRoot.getFullPath(),
//	        new File(importPath), FileSystemStructureProvider.INSTANCE, overwriteQuery);
//	importOperation.setCreateContainerStructure(false);
//	return importOperation;
//	}
}
