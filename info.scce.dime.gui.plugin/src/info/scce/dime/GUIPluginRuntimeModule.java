/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.linking.ILinker;
import org.eclipse.xtext.linking.impl.Linker;

/**
 * Registers components to be used at runtime / without the Equinox extension registry.
 * 
 * @author Steve Bosselmann
 */
public class GUIPluginRuntimeModule extends info.scce.dime.AbstractGUIPluginRuntimeModule {

	@Override
    public Class<? extends IValueConverterService> bindIValueConverterService() {
        return GUIPluginValueConverterService.class;
    }

	public java.lang.Class<? extends ILinker> bindILinker() {
		return GUIPluginLinker.class;
	}

	public static class GUIPluginLinker extends Linker {
		@Override
		protected boolean isClearAllReferencesRequired(Resource resource) {
			return false;
		}
	}
	
}
