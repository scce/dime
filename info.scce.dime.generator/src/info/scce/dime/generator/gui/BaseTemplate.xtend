/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui

import graphmodel.Node
import info.scce.dime.generator.gui.dart.functionality.AngularIncludedTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartControlSIBTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartGUIPluginTemplate
import info.scce.dime.generator.gui.dart.component.AngularFormHTMLTemplate
import info.scce.dime.generator.gui.dart.component.AngularTODOList
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.html.HTMLAlertTemplate
import info.scce.dime.generator.gui.html.HTMLBadgeTemplate
import info.scce.dime.generator.gui.html.HTMLBoxTemplate
import info.scce.dime.generator.gui.html.HTMLButtonGroupTemplate
import info.scce.dime.generator.gui.html.HTMLButtonTemplate
import info.scce.dime.generator.gui.html.HTMLButtonToolbarTemplate
import info.scce.dime.generator.gui.html.HTMLChoiceTemplate
import info.scce.dime.generator.gui.html.HTMLDescriptionTemplate
import info.scce.dime.generator.gui.html.HTMLEmbeddedTemplate
import info.scce.dime.generator.gui.html.HTMLFieldTemplate
import info.scce.dime.generator.gui.html.HTMLFileTemplate
import info.scce.dime.generator.gui.html.HTMLFormTemplate
import info.scce.dime.generator.gui.html.HTMLHeadlineTemplate
import info.scce.dime.generator.gui.html.HTMLImageTemplate
import info.scce.dime.generator.gui.html.HTMLJumbotronTemplate
import info.scce.dime.generator.gui.html.HTMLListTemplate
import info.scce.dime.generator.gui.html.HTMLNavBarTemplate
import info.scce.dime.generator.gui.html.HTMLPageUp
import info.scce.dime.generator.gui.html.HTMLPanelTemplate
import info.scce.dime.generator.gui.html.HTMLPlaceholderTemplate
import info.scce.dime.generator.gui.html.HTMLProcessSIBTemplate
import info.scce.dime.generator.gui.html.HTMLProgressBarTemplate
import info.scce.dime.generator.gui.html.HTMLRowTemplate
import info.scce.dime.generator.gui.html.HTMLTabbingTemplate
import info.scce.dime.generator.gui.html.HTMLTableTemplate
import info.scce.dime.generator.gui.html.HTMLTextTemplate
import info.scce.dime.generator.gui.html.HTMLThumbnailTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Alert
import info.scce.dime.gui.gui.Badge
import info.scce.dime.gui.gui.Bar
import info.scce.dime.gui.gui.BaseElement
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.ButtonToolbar
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.Description
import info.scce.dime.gui.gui.Embedded
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.File
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.Headline
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.gui.gui.Listing
import info.scce.dime.gui.gui.PageUp
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.ProgressBar
import info.scce.dime.gui.gui.Row
import info.scce.dime.gui.gui.Select
import info.scce.dime.gui.gui.SpecialElement
import info.scce.dime.gui.gui.TODOList
import info.scce.dime.gui.gui.Tabbing
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Text
import info.scce.dime.gui.gui.Thumbnail
import info.scce.dime.gui.helper.ElementCollector
import info.scce.dime.process.process.Process
import java.util.List
import info.scce.dime.generator.gui.html.HTMLLinkSIBTemplate
import graphmodel.Container
import info.scce.dime.gui.gui.SIB

/**
 * The Base Template is used to render the HTML code for all available components placed in a given component.
 * The rendering process used recursion to build up the entire HTML document.
 */
class BaseTemplate extends GUIGenerator {
	
	/**
	 * Creates the entire HTML Angular code for a given GUI model.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def String create(GUI container){
		var html ="";
		html += preContainer(container).toString;
		val bodies = ElementCollector.getElementsV(
			container.templates.filter[blockName.nullOrEmpty || blockName == "body"]
		)
		for (Template body : bodies) {
			html += createTemplate(body);
		}
		html += postContainer(container);
		return html;
	}
	
	
	/**
	 * Creates the entire HTML Angular code for a given Template in a GUI model.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def createTemplate(Template ct)
	{
		return baseContent(ElementCollector.getElementsV(ct.allNodes),!ct.disableAutoLayout);
	}
	
	
	/**
	 * Creates the opening tags HTML code for a bootstrap row
	 */
	def preRow()
	'''
	<div class="row">
		<div class="col-sm-12">
	'''
	
	/**
	 * Creates the closing tags HTML code for a bootstrap row
	 */
	def postRow()
	'''
		</div>
	</div>
	'''
	
	/**
	 * Creates the HTML code depended of the type and the order of every component.
	 * The rowing defines whether every component in the list should placed in its own row.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def String baseContent(List<Node> nodes)
	{
		baseContent(nodes,false)
	}
	
	/**
	 * Creates the HTML code depended of the type and the order of every component.
	 * The rowing defines whether every component in the list should placed in its own row.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def String baseContent(List<Node> nodes,boolean rowing)
	{
		var html ="";
		for(BaseElement node:nodes.filter(BaseElement)){
			if(node instanceof Row){
				var row = new HTMLRowTemplate();
				html += row.create(node);
			}
			else{
				if(rowing && (!node.rootElement.disableAutoLayout)){
					html += preRow;
				}
				html += TemplateSelection(node)
				if(rowing && (!node.rootElement.disableAutoLayout)){
					html += postRow;					
				}
			}
			
		}
		return html;
	}
	
	def dispatch TemplateSelection(Headline node){
		var headline = new HTMLHeadlineTemplate();
		return headline.create(node);
	}
	
	def dispatch TemplateSelection(Listing node){
		var list = new HTMLListTemplate();
		return list.create(node);		
	}
	
	def dispatch TemplateSelection(Bar node){
		var list = new HTMLNavBarTemplate();
		return list.create(node);
	}
	
	def dispatch TemplateSelection(Description node){
		var description = new HTMLDescriptionTemplate();
		return description.create(node);
	}
	
	def dispatch TemplateSelection(Table node){
		var table = new HTMLTableTemplate();
		val gcv = genctx.getCompoundView(node.rootElement)
		return table.create(node,gcv).toString;
	}
	
	def dispatch TemplateSelection(Form node){
		var form = new HTMLFormTemplate();
		val gcv = genctx.getCompoundView(node.rootElement)
		return form.create(node,gcv);
	}
	
	def dispatch TemplateSelection(ButtonToolbar node){
		return new HTMLButtonToolbarTemplate(node).create
	}
	
	def dispatch TemplateSelection(Button node){
		val form = node.findFirstParent(Form)
		var output = ""
		if (form !== null && node.options?.submitsForm) {
			output += new AngularFormHTMLTemplate().preButton(form)
		}
		return output += new HTMLButtonTemplate().create(node as Button,form);
	}
	
	def dispatch TemplateSelection(ButtonGroup node){
		var group = new HTMLButtonGroupTemplate();
		return group.create(node);
	}
	
	def dispatch TemplateSelection(Image node){
		var group = new HTMLImageTemplate();
		return group.create(node,true);
	}
	
	def dispatch TemplateSelection(Thumbnail node){
		var thumbnail = new HTMLThumbnailTemplate();
		return thumbnail.create(node).toString;
	}
	
	def dispatch TemplateSelection(Jumbotron node){
		var jumbotron = new HTMLJumbotronTemplate();
		return jumbotron.create(node).toString;
	}
	
	def dispatch TemplateSelection(Panel node){
		var panel = new HTMLPanelTemplate();
		return panel.create(node);
	}
	
	def dispatch TemplateSelection(Box node){
		var box = new HTMLBoxTemplate();
		return box.create(node).toString;
	}
	
	def dispatch TemplateSelection(PageUp node){
		var box = new HTMLPageUp();
		return box.create(node);
	}
	
	def dispatch TemplateSelection(Alert node){
		var alert = new HTMLAlertTemplate();
		return alert.create(node);
	}
	
	def dispatch TemplateSelection(Tabbing node){
		return new HTMLTabbingTemplate().create(node);
	}
	
	def dispatch TemplateSelection(Text node){
		var text = new HTMLTextTemplate();
		return text.create(node);
	}
	
	def dispatch TemplateSelection(Embedded node){
		return new HTMLEmbeddedTemplate().create(node);
	}
	
	def dispatch TemplateSelection(SpecialElement node) {
		if(node instanceof TODOList) {
			var row = new AngularTODOList();
			return row.createTemplate(node as TODOList);				
		}
	}
	
	def dispatch TemplateSelection(Placeholder node) {
		var row = new HTMLPlaceholderTemplate();
		return row.create(node);				
	}
	
	def dispatch TemplateSelection(File node) {
		var row = new HTMLFileTemplate();
		return row.createColContent(node as File);				
	}
	
	def dispatch TemplateSelection(Field node) {
		var row = new HTMLFieldTemplate();
		return row.create(node as Field, node.findFirstParent(Form));				
	}
	
	def dispatch TemplateSelection(Select node) {
		var row = new HTMLChoiceTemplate();
		return row.create(node as Select, node.findFirstParent(Form));				
	}
	
	def dispatch TemplateSelection(ProgressBar node) {
		var row = new HTMLProgressBarTemplate();
		return row.create(node as ProgressBar);				
	}
	
	def dispatch TemplateSelection(Badge node){
		var row = new HTMLBadgeTemplate();
		return row.create(node as Badge);	
	}
	
	def dispatch TemplateSelection(GUIPlugin node){
		return new AngularDartGUIPluginTemplate().create(node).toString;
	}
	
	def dispatch TemplateSelection(ControlSIB node){
		return new AngularDartControlSIBTemplate().create(node).toString;
	}
	
	def dispatch TemplateSelection(GUISIB node){
		var row = new AngularIncludedTemplate();
		return row.create(node as GUISIB)	
	}
	
	def dispatch TemplateSelection(ProcessSIB node) {
		var ei = node as ProcessSIB;
		var p = ei.proMod as Process;
		return new HTMLProcessSIBTemplate().create(node,p);
	}
	
	def dispatch TemplateSelection(LinkProcessSIB node) {
		var ei = node as LinkProcessSIB;
		return new HTMLLinkSIBTemplate().create(ei);
	}

	
	
	/**
	 * Creates the opening tags for the GUI model HTML code.
	 * This includes the code for the header template if available.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def preContainer(GUI gui)
	'''
	«IF gui.generalStyle != null»
	<div
		«IF !preview»
		[id]="getContainer«ConventionHelper.cincoID(gui)»Id()"
		[class]="getContainer«ConventionHelper.cincoID(gui)»Class()"
		«ENDIF»
		style="«gui.generalStyle.rawContent»">
	«ENDIF»
	«FOR node:gui.templates.filter[n | n.blockName.equals("header")] »
		<header «IF node.generalStyle != null»style="«node.generalStyle.rawContent»"«ENDIF»>
				«this.createTemplate(node)»
		</header>
	«ENDFOR»
	«IF gui.generalStyle != null»
	</div>
	«ENDIF»
	'''
	
	/**
	 * Creates the closing tags for the GUI model HTML code.
	 * This includes the code for the footer template if available.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def postContainer(GUI gui)
	'''
	«FOR node:gui.templates.filter[n | n.blockName.equals("footer")] »
		<footer style="width: 100%;bottom: 0px;position: fixed;«IF node.generalStyle != null»«node.generalStyle.rawContent»«ENDIF»">
				«this.createTemplate(node)»
		</footer>
	«ENDFOR»
	«IF gui.generalStyle != null»
	</div>
	«ENDIF»
	'''
}
