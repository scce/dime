/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.process.process.Process;

import java.util.HashMap;
import java.util.Map;

public class LongRunningProcessCompoundView extends Parent {
	
	private Process process;
	private Map<String, CompoundView> resumePoints;

	public LongRunningProcessCompoundView(Process process) {
		this.process = process;
		this.resumePoints = new HashMap<String, CompoundView>();
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public Map<String, CompoundView> getResumePoints() {
		return resumePoints;
	}

	public void setResumePoints(Map<String, CompoundView> resumePoints) {
		this.resumePoints = resumePoints;
	}

	@Override
	public boolean isPrimitive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPrimitiveBoolean() {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
