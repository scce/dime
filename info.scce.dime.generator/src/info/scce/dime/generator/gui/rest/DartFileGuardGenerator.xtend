/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest

import graphmodel.Container
import graphmodel.ModelElementContainer
import info.scce.dime.data.data.Data
import info.scce.dime.generator.gui.dart.component.DartHTMLFileTemplate
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.File
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GuardSIB
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import java.util.LinkedList
import java.util.List
import java.util.Set

class DartFileGuardGenerator extends GUIGenerator{
	
	def getImports(GUI gui)'''«IF gui.isFileGuardPresent»
	import 'package:app/src/models/«gui.fileName»';
	«ENDIF»'''
	

	def getFileRestURL(MovableContainer cmc)'''"${getRestBaseUrl()}/«cmc.restURL»"'''
	
	def getRestURL(MovableContainer cmc)'''rest/files/«cmc.rootElement.id.escapeJava»/get/«cmc.id.escapeJava»/public'''
	
	def getImageURL(MovableContainer cmc)'''rest/files/«cmc.rootElement.id.escapeJava»/read/«cmc.id.escapeJava»/public'''
	
	def getGuardCompoundName(Container container)'''FileGuard«container.id.escapeDart»Compound'''
	
	def getGuardType(GuardSIB container)'''«container.label.escapeDart.toFirstUpper»«container.id.escapeDart»'''
	
	def getGuardName(GuardSIB container)'''«container.label.escapeDart»«container.id.escapeDart»'''
	
	def String fileName(GUI gui)'''FileGuards«gui.id.escapeDart»«gui.title.escapeDart».dart'''
	
	def isFileGuardPresent(ModelElementContainer gui)
	{
		!gui.find(File, Image).isEmpty
	}
	
	def generate(GUI gui,GUICompoundView gcv,Set<Data> datas)
	{
		var fileContent = File.concatContent(gui,gcv)
		var imageContent = Image.concatContent(gui,gcv)
		var list = new LinkedList;
		list.addAll(fileContent)
		list.addAll(imageContent)
		return '''
		import 'dart:convert';
		import 'package:app/src/models/FileReference.dart';
		«FOR data:datas»
		import 'package:app/src/data/«data.modelName.escapeDart».dart' as «DyWASelectiveDartGenerator.prefix(data)»;
		«ENDFOR»
		«list.join("\n")»
		'''
	}
	
	def List<String> concatContent(Class<? extends MovableContainer> t,GUI gui,GUICompoundView gcv)
	{
		gui.find(t).map[container|container.generateFileGuardClass(container.allNodes.filter(GuardSIB).toList,gcv)];
	}	
	
	
	def String generateFileGuardClass(Container container,List<GuardSIB> guards,GUICompoundView gcv)
	{
		return
		'''	
		class «container.guardCompoundName» {
				
				«IF guards.filter[n|DartHTMLFileTemplate.getIsFileFetch(n)].empty»
				FileReference context;
				«ENDIF»
				
				SecurityInputs«container.guardCompoundName» securityInputs;
				
				«FOR guard:guards.filter[n|DartHTMLFileTemplate.getIsFileFetch(n)]»
				«guard.guardType» context;
				«ENDFOR»
				
				«container.guardCompoundName»() {
					securityInputs = new SecurityInputs«container.guardCompoundName»();
					«FOR guard:guards.filter[n|DartHTMLFileTemplate.getIsFileFetch(n)]»
						context = new «guard.guardType»();
					«ENDFOR»
				}
				
				String toQueryParams() {
					List<String> params = new List();
					«FOR guard:guards.filter[n|DartHTMLFileTemplate.getIsFileFetch(n)]»
						«FOR input:guard.inputPorts.filter(PrimitiveInputPort).filter[n|!n.getIncoming(Read).empty]»
							«{
								val value = gcv.pairs.get(input.getIncoming(Read).get(0).sourceElement)
								'''
								«IF DyWASelectiveDartGenerator.isEncoding(value)»
									if(this.context.«input.name.escapeDart» != null) {
										params.add("«input.name.escapeString»=${this.context.«input.name.escapeDart».dywa_id}");
									} else {
										params.add("«input.name.escapeString»=");
									}
							 	«ELSEIF DyWASelectiveDartGenerator.isTimeStamp(value)»
									if(this.context.«input.name.escapeDart» != null) {
										params.add("«input.name.escapeString»=${DateConverter.toJSON(this.context.«input.name.escapeDart»)}");
									} else {
										params.add("«input.name.escapeString»=");
									}
							 	«ELSE»
									params.add("«input.name.escapeString»=${context.«input.name.escapeDart».toString()}");
							 	«ENDIF»
								'''				
							}»
						«ENDFOR»
					«ENDFOR»
					return "?" + params.join("&");
				}
				
				String toJSON() {
					return jsonEncode(this.toJSOG());
				}
				
				Map<String,dynamic> toJSOG() {
					Map<String,dynamic> jsonObj = new Map();
					Map<Object,dynamic> objects = new Map();
					if(context!=null) {
						jsonObj["context"] = context.toJSOG(objects);
					}
					else{
						jsonObj["context"] = null;
					}
					jsonObj["securityInputsForInteractable"] = securityInputs.toJSOG(objects);
					return jsonObj;
				}
		}
		
		class SecurityInputs«container.guardCompoundName» {
			«FOR guard:guards.filter[n|DartHTMLFileTemplate.getIsSecurity(n)]»
				«guard.guardType» «guard.guardName»;
			«ENDFOR»
			SecurityInputs«container.guardCompoundName»() {
				«FOR guard:guards.filter[n|DartHTMLFileTemplate.getIsSecurity(n)]»
					«guard.guardName» = new «guard.guardType»();
				«ENDFOR»
			}
			Map<String,dynamic> toJSOG(Map<Object,dynamic> objects) {
				Map<String,dynamic> jsonObj = new Map();
				«FOR guard:guards.filter[n|DartHTMLFileTemplate.getIsSecurity(n)]»
					jsonObj["«guard.guardName»"] = «guard.guardName».toJSOG(objects);
				«ENDFOR»
				return jsonObj;
			}
		}
		
		«FOR guard:guards»
		class «guard.guardType» {
			«FOR input:guard.allNodes.filter[n|n instanceof IO].map[n|n as IO].filter[n|!n.getIncoming(Read).empty]»
				«{
					val value = gcv.pairs.get(input.getIncoming(Read).get(0).sourceElement)					
					'''
					«IF input instanceof ComplexInputPort»
							«value.selectiveClassName((input as ComplexInputPort).isList)» «input.name.escapeDart»;
					«ELSEIF input instanceof PrimitiveInputPort»
							«value.selectiveClassName((input as PrimitiveInputPort).isList)» «input.name.escapeDart»;
					«ENDIF»
					'''
				}»
				
			«ENDFOR»
			«FOR input:guard.allNodes.filter[n|n instanceof InputStatic].map[n|n as InputStatic]»
				«{
					val name = input.name.escapeDart;
					var type = "";
					var staticValue = "";
					switch(input){
						TextInputStatic:{staticValue="'"+(input as TextInputStatic).value+"'";type="String"}
						IntegerInputStatic:{staticValue=""+(input as IntegerInputStatic).value;type="int"}
						RealInputStatic:{staticValue=""+(input as RealInputStatic).value;type="double"}
						BooleanInputStatic:{staticValue=""+(input as BooleanInputStatic).value.toString;type="bool"}
						TimestampInputStatic:{staticValue="DateConverter.fromSecondsSinceEpoch("+(input as TimestampInputStatic).value+")";type="DateTime"}
					}
					'''«type» «name» = «staticValue»;'''
				}»
			«ENDFOR»
			«guard.guardType»()
			{
			«FOR input:guard.allNodes.filter[n|n instanceof InputPort].map[n|n as InputPort].filter[n|!n.getIncoming(Read).empty]»
			«{
				val value = gcv.pairs.get(input.getIncoming(Read).get(0).sourceElement)
				'''
				«IF input instanceof ComplexInputPort»
					«IF (input as ComplexInputPort).isIsList»
						this.«input.name.escapeDart» = new DIMEList();
					«ENDIF»
				«ELSEIF input instanceof PrimitiveInputPort»
					«IF value.isList»
						this.«input.name.escapeDart» = new DIMEList();
					«ELSE»
						this.«input.name.escapeDart» = «DyWADartGenerator.getDefaultPrimitiveValue(value)»;
					«ENDIF»
				«ENDIF»
				'''
			}»
			«ENDFOR»
			}
			
			Map<String,dynamic> toJSOG(Map<Object,dynamic> objects) {
				Map<String,dynamic> jsonObj = new Map();
				«FOR input:guard.allNodes.filter[n|n instanceof IO].map[n|n as IO].filter[n|!n.getIncoming(Read).empty]»
				«{
					val value = gcv.pairs.get(input.getIncoming(Read).get(0).sourceElement)
					'''
					«IF input instanceof InputPort»
						«IF input.isList»
							if(this.«input.name.escapeDart».isEmpty){
								jsonObj["«input.name.escapeString»"] = [];
							}
							else{
								jsonObj["«input.name.escapeString»"] = this.«input.name.escapeDart»«IF DyWASelectiveDartGenerator.isEncoding(value)».map((n)=>n.toJSOG(objects)).toList()«ENDIF»;
							}
							
						 «ELSE»
						 	«IF DyWASelectiveDartGenerator.isEncoding(value)»
						 		if(this.«input.name.escapeDart» != null) {
						 			jsonObj["«input.name.escapeString»"] = this.«input.name.escapeDart».toJSOG(objects);
						 		}
						 		else {
						 			jsonObj["«input.name.escapeString»"] = null;
						 		}
						 	«ELSEIF DyWASelectiveDartGenerator.isTimeStamp(value)»
						 		if(this.«input.name.escapeDart» != null) {
						 			jsonObj["«input.name.escapeString»"] = DateConverter.toJSON(this.«input.name.escapeDart»);
						 		}
						 		else {
						 			jsonObj["«input.name.escapeString»"] = null;
						 		}
						 	«ELSE»
						 		jsonObj["«input.name.escapeString»"] = this.«input.name.escapeDart»;
						 	«ENDIF»
						«ENDIF»
					«ELSE»
						if(this.«input.name.escapeDart» != null) {
							jsonObj["«input.name.escapeString»"] = this.«input.name.escapeDart»;
						}
					«ENDIF»
					'''
				}»
				«ENDFOR»
				return jsonObj;
			}
			
		}
		«ENDFOR»
		'''
	}
}
