/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.data.data.Type;
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator;
import info.scce.dime.gui.gui.GUI;

public class BaseComplexTypeView extends TypeView {
	
	protected Type type;
	
 
	public BaseComplexTypeView(Type type) {
		super();
		this.type = type;
		if(type == null) {
			throw new IllegalStateException("Type not null");
		}
	}

	/**
	 * Copy constructor.
	 *
	 * @param p the copy
	 */
	protected BaseComplexTypeView(BaseComplexTypeView tv) {
		super(tv);
		this.type = tv.type;
		if(type == null) {
			throw new IllegalStateException("Type not null");
		}
	}

	@Override
	public boolean isPrimitive() {
		return false;
	}

	@Override
	public boolean isPrimitiveBoolean() {
		return false;
	}

	public Type getType() {
		return type;
	}
	
	public void setType(Type t) {
		type = t;
	}


	public String getTypeName() {
		return type.getName();
	}

	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BaseComplexTypeView)) {
			return false;
		}
		final BaseComplexTypeView that = (BaseComplexTypeView) obj;
		String thatId = DyWASelectiveDartGenerator.getSelectiveHashCodeJava(that).toString();
		return DyWASelectiveDartGenerator.getSelectiveHashCodeJava(this).toString().equals(thatId);
	}
	
	@Override
    public int hashCode() {
		return DyWASelectiveDartGenerator.getSelectiveHashCodeJava(this).toString().hashCode();
    }

}
