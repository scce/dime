/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import graphmodel.GraphModel;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.DataExtension;
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.process.process.GuardContainer;


public class ComplexTypeView extends BaseComplexTypeView{
	
	private List<ViewImplementation> interfaces = new LinkedList<ViewImplementation>();
	
	private List<FieldView> displayedFields = new LinkedList<>();
	
	private Set<GuardContainer> guardContainers;
	

	
	private ComplexTypeView compositionView;
	
	private boolean isCompositionToSubType = false;

	/**
	 * Copy constructor.
	 *
	 * @param p the copy
	 */
	public ComplexTypeView(ComplexTypeView ctv) {
		super(ctv);
		this.interfaces = new ArrayList<>(ctv.interfaces);
		this.displayedFields = new ArrayList<>(ctv.displayedFields);
		this.guardContainers = new HashSet<>(ctv.guardContainers);
		this.compositionView = ctv.compositionView;
		this.isCompositionToSubType = ctv.isCompositionToSubType;
	}

	public ComplexTypeView(Type type,Set<GuardContainer> guardContainers,GraphModel g)
	{
		super(type);
		this.guardContainers = guardContainers;
		this.gModel = g;
	}
	
	public ComplexTypeView getCompositionView() {
		return compositionView;
	}

	public void setCompositionView(ComplexTypeView compositionView) {
		this.compositionView = compositionView;
	}
	

	public Set<GuardContainer> getGuardContainers() {
		return guardContainers;
	}



	public void setGuardContainers(Set<GuardContainer> guardContainers) {
		this.guardContainers = guardContainers;
	}

	public List<FieldView> getDisplayedFields() {
		return displayedFields;
	}

	public void setDisplayedFields(List<FieldView> displayedFields) {
		this.displayedFields = displayedFields;
	}

	public List<ViewImplementation> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(List<ViewImplementation> interfaces) {
		this.interfaces = interfaces;
	}
	
	public boolean identically(ComplexTypeView ctv) {
		return this.equals(ctv) && this.getData().equals(ctv.getData());
	}
	
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ComplexTypeView)) {
			return false;
		}
		final ComplexTypeView that = (ComplexTypeView) obj;
		String thatId = DyWASelectiveDartGenerator.getSelectiveHashCodeJava(that).toString();
		return DyWASelectiveDartGenerator.getSelectiveHashCodeJava(this).toString().equals(thatId);
	}
	
	@Override
    public int hashCode() {
		return DyWASelectiveDartGenerator.getSelectiveHashCodeJava(this).toString().hashCode();
    }



	@Override
	public boolean isPrimitive() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public boolean isPrimitiveBoolean() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void setToSameType(ComplexTypeView ctv) {
		//setId(ctv.getId());
		//setName(ctv.getName());
		//setTypeName(ctv.getTypeName());
		setCompositionView(ctv);
		if(this.compositionView!=null) {
			//get data type of composition partner
			Type compostionType = this.compositionView.getType();
			DataExtension de = DataExtension.getInstance();
			//is the composition partner type a sub type of the
			//current type of this complex type view?
			if(de.getSuperTypes(compostionType).contains(this.getType())) {
				isCompositionToSubType= true;
			}
		}
	}
	
	
	public Set<ComplexTypeView> getAllSubTypes() {
		Set<ComplexTypeView> result = new HashSet<ComplexTypeView>();
		result.add(this);
		result.addAll(getDisplayedFields().stream().filter(n->n instanceof ComplexFieldView).map(n->(ComplexFieldView)n).flatMap(n->((ComplexTypeView)n.getView()).getAllSubTypes().stream()).collect(Collectors.toSet()));
		return result;
	}
	
	public boolean isCompositionToSameOrParentType() {
		return !isCompositionToSubType() && compositionView!=null;
	}
	
	public boolean isCompositionToSubType() {
		return isCompositionToSubType;
	}

	public String getTypeName() {
		String name = "";
		if(gModel instanceof GUI) {
			name = ((GUI) gModel).getTitle();
		}
		if(gModel instanceof info.scce.dime.process.process.Process) {
			name = ((info.scce.dime.process.process.Process) gModel).getModelName();
		}
		return name+""+(type==null?"":type.getName());
	}
	
	
}
