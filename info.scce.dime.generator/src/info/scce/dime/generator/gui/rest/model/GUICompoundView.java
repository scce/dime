/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.GUI;

/**
 * The GUI compound view represents the root of the tree of selective
 * compound views and holds a reference to the given GUI model.
 * @author zweihoff
 *
 */
public class GUICompoundView extends CompoundView{
	private GUI cgui;
	
	
	public GUICompoundView(GUI gui)
	{
		super();
		this.cgui = gui;
		this.gModel = gui;
	}
	
	public GUI getGui() {	
		return (GUI) cgui;
	}

	public void setGui(GUI gui) {
		this.cgui = gui;
	}
	
	public GUI getGUI()
	{
		return this.cgui;
	}

	public GUI getCgui() {
		return cgui;
	}

	public void setCgui(GUI cgui) {
		this.cgui = cgui;
	}
	
	public String toString(){
		return "gui."+this.cgui.getTitle();
	}

	
	
}
