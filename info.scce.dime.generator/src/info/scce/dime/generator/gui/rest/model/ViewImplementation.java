/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import graphmodel.GraphModel;

public class ViewImplementation {
	
	private GraphModel sourceGraph;
	
	private ComplexTypeView ctv;
	
	public ViewImplementation(GraphModel sourceGraph,ComplexTypeView ctv) {
		if(sourceGraph==null||ctv==null){
			throw new IllegalStateException();
		}
		this.sourceGraph = sourceGraph;
		this.ctv=ctv;
		
	}

	
	public Map<GraphModel,ViewImplementation> getTransitivSubTypeViews(Set<GraphModel> cache){
		if(cache.contains(sourceGraph)){
			return Collections.emptyMap();
		}
		cache.add(sourceGraph);
		Map<GraphModel,ViewImplementation> gs = new HashMap<>();
		gs.put(sourceGraph,this);
		if(ctv!=null){
			// find all ctv reachable and ctv itself by implementation
			ctv.getAllSubTypes().stream().flatMap(n->n.getInterfaces().stream()).forEach(n->gs.putAll(n.getTransitivSubTypeViews(cache)));
		}
		return gs;
	}




	public ComplexTypeView getParent() {
		return ctv;
	}


	public GraphModel getSourceGraph() {
		return sourceGraph;
	}

	
	
	
	
}
