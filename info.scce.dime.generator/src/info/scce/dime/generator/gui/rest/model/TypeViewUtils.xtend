/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest.model

import graphmodel.Node
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.data.Type
import info.scce.dime.gui.gui.ComplexRead
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.PrimitiveRead
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.SIB
import java.util.ArrayList
import java.util.Collections
import java.util.Comparator
import java.util.LinkedHashMap
import java.util.LinkedList
import java.util.List
import java.util.Map

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class TypeViewUtils {
	
	static def toTypeView(Parent p){
		if(p instanceof ComplexFieldView)return p.view
		p
	}
	
	/**
	 * Helper method to build a relation from an input port of a given security process
	 * to the containing ports and the corresponding selective type views, which are located in gcv.
	 * The resulting map is used to create the compound type classes when the security process starts and
	 * the data of the ports has to be send to the backend.
	 * @param p
	 * @param pcv
	 * @return
	 */
	static def Map<IO,Parent> buildInputTypeViewMap(SecuritySIB sib,GUICompoundView gcv)
	{
		val tvs = new LinkedHashMap<IO,Parent>();

		// We need to sort the input by their name, because the generator for the process, does this as well
		val sortedInputs = new ArrayList(sib.getIOs());
		Collections.sort(sortedInputs, new Comparator<IO>() {
			override int compare(IO o1, IO o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		for(IO i:sortedInputs){
			if(i instanceof InputPort){
				val ip = i as InputPort;
				val parents = new LinkedList<Parent>();
				parents.addAll(ip.getIncoming(ComplexRead).map[n|gcv.getPairs().get(n.getSourceElement())]);
				parents.addAll(ip.getIncoming(PrimitiveRead).map[n|gcv.getPairs().get(n.getSourceElement())]);
				if(parents.isEmpty()){
					tvs.put(ip, null);
				} else {
					tvs.put(ip,parents.get(0));					
				}
			}
			else{
				tvs.put(i, null);
			}
		}
		return tvs;
	}
	
	
	private static def buildInputMap(List<Input> sortedInputs,GUIExtension guiExtension) {
		val tvs = new LinkedList<Parent>();
		for(Input i:sortedInputs){
			if(i instanceof ComplexInputPort){
				val ctv = new ComplexTypeView(i.dataType,Collections.EMPTY_SET,i.rootElement)
				ctv.data = i
				ctv.id = i.id
				ctv.list = i.isIsList
				ctv.name = i.name
				tvs.add(ctv)
			}
			if(i instanceof PrimitiveInputPort) {
				val ptv = new PrimitiveTypeView()
				ptv.data = i
				ptv.id = i.id
				ptv.list = i.isIsList
				ptv.name = i.name
				ptv.primitiveType = guiExtension.dataExtension.toData(i.dataType)
				tvs.add(ptv)
			}
			else{
				val ptv = new PrimitiveTypeView()
				ptv.data = i
				ptv.id = i.id
				ptv.list = false
				ptv.name = i.name
				ptv.primitiveType = guiExtension.dataExtension.toData(i as InputStatic)
				tvs.add(ptv)
			}
		}
		tvs
	}
	
	/**
	 * Helper method to create default selectives for the input ports of a given SIB
	 * @param p
	 * @param pcv
	 * @return
	 */
	static def Iterable<Parent> buildInputTypeViewMap(SIB sib,GUIExtension guiExtension)
	{

		// We need to sort the input by their name, because the generator for the process, does this as well
		val sortedInputs = new ArrayList(sib.getInputs());
		Collections.sort(sortedInputs, new Comparator<Input>() {
			override int compare(Input o1, Input o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		buildInputMap(sortedInputs,guiExtension)
	}
	
	
	static def isReferencedType(Type t){
		return
		t instanceof ReferencedType ||
		t instanceof ReferencedEnumType ||
		t instanceof ReferencedUserType
	}
	
	static def dispatch referencedDataModel(ReferencedType t){
		t.referencedType.rootElement
	}
	static def dispatch referencedDataModel(ReferencedEnumType t){
		t.referencedType.rootElement
		
	}
	static def dispatch referencedDataModel(ReferencedUserType t){
		t.referencedType.rootElement
	}
	
	static def Parent getTypeViewForVariable(GUICompoundView gcv,Node node)
	{
		if(gcv.pairs.containsKey(node))
		{
			return gcv.pairs.get(node);
		}
		return null;
	}
	
	static def String prefix(Type type)
	{
		if(type.referencedType){
			return type.referencedDataModel.prefix
		}
		return type.rootElement.prefix
	}
	
	def static prefix(Data data) {
		return data.modelName.escapeDart
	}
	

}
