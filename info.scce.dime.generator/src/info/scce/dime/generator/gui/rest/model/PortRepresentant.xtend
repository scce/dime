/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.rest.model

import graphmodel.Node
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PrimitiveOutputPort

class PortRepresentant {
	
	extension GUIExtension
	extension DataExtension
	
	public String  name
	
	public boolean isList             = false
	public boolean isStaticBoolean    = false
	public boolean isStaticText       = false
	public boolean isStaticInt        = false
	public boolean isStaticReal       = false
	public boolean isStaticTimestamp  = false
	
	public boolean staticBooleanValue = false
	public String  staticTextValue
	public long    staticIntValue
	public double  staticRealValue
	public long    staticTimestampValue
	
	public Type          complexType
	public PrimitiveType primitivetype
	
	def static PortRepresentant create(Node node, GUIExtension ext) {
		if(node instanceof IO){
			return new PortRepresentant(node, ext)
		}
		if(node instanceof Input){
			return new PortRepresentant(node, ext)
		}
		if(node instanceof Output){
			return new PortRepresentant(node, ext)
		}
	}
	
	private new(String name, GUIExtension ext) {
		this.name = name
		this._gUIExtension = ext
	}
	
	new(IO guiIO, GUIExtension ext) {
		this(guiIO.name, ext)
		if(guiIO instanceof InputStatic){
			isList=false
			if(guiIO instanceof TextInputStatic){
				isStaticText=true
				staticTextValue = guiIO.value
			}
			if(guiIO instanceof IntegerInputStatic){
				isStaticInt=true
				staticIntValue = guiIO.value
			}
			if(guiIO instanceof RealInputStatic){
				isStaticReal=true
				staticRealValue = guiIO.value
			}
			if(guiIO instanceof TimestampInputStatic){
				isStaticTimestamp=true
				staticTimestampValue = guiIO.value
			}
			if(guiIO instanceof BooleanInputStatic){
				isStaticBoolean=true
				staticBooleanValue = guiIO.value
			}
		}
		if(guiIO instanceof PrimitiveInputPort) {
			isList = guiIO.isIsList
			primitivetype = guiIO.dataType.toData
		}
		if(guiIO instanceof ComplexInputPort) {
			isList = guiIO.isIsList
			complexType = guiIO.dataType
		}
	}
	
	new(Input processIO, GUIExtension ext) {
		this(processIO.name, ext)
		if(processIO instanceof info.scce.dime.process.process.InputStatic){
			isList=false
			if(processIO instanceof info.scce.dime.process.process.TextInputStatic){
				isStaticText=true
				staticTextValue = processIO.value
			}
			if(processIO instanceof info.scce.dime.process.process.IntegerInputStatic){
				isStaticInt=true
				staticIntValue = processIO.value
			}
			if(processIO instanceof info.scce.dime.process.process.RealInputStatic){
				isStaticReal=true
				staticRealValue = processIO.value
			}
			if(processIO instanceof info.scce.dime.process.process.TimestampInputStatic){
				isStaticTimestamp=true
				staticTimestampValue = processIO.value
			}
			if(processIO instanceof info.scce.dime.process.process.BooleanInputStatic){
				isStaticBoolean=true
				staticBooleanValue = processIO.value
			}
		}
		if(processIO instanceof info.scce.dime.process.process.PrimitiveInputPort) {
			isList = processIO.isIsList
			primitivetype = processIO.dataType.toData
		}
		if(processIO instanceof info.scce.dime.process.process.ComplexInputPort) {
			isList = processIO.isIsList
			complexType = processIO.dataType
		}
	}
	
	new(Output processIO, GUIExtension ext) {
		this(processIO.name, ext)
		if(processIO instanceof PrimitiveOutputPort) {
			isList = processIO.isIsList
			primitivetype = processIO.dataType.toData
		}
		if(processIO instanceof ComplexOutputPort) {
			isList = processIO.isIsList
			complexType = processIO.dataType
		}
	}

	def boolean isStatic() {
		isStaticBoolean || isStaticInt || isStaticReal || isStaticText || isStaticText
	}

	def boolean isPrimitive() {
		isStatic || primitivetype !== null
	}

	def boolean isComplex() {
		complexType != null
	}
	
}
