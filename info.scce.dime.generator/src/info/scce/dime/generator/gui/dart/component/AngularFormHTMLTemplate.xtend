/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.html.HTMLFieldTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular HTML template for a form component
 */
class AngularFormHTMLTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular HTML template code for a given form component
	 */
	def create(Form form,GUI g)
	'''
	<form
	 «IF !preview»
	 	[ngFormModel]="«ConventionHelper.getFormName(form)»"
	 	[attr.id]="'form'+«ConventionHelper.getFormName(form)».hashCode.toString()"
	 	data-cinco-id="«form.id.escapeString»"
 	«ENDIF»
	 «form.printStyle»
	 «form.printNgIfFor» «IF form.inline» class="form-horizontal"«ENDIF»
«««	 «FOR primitiveVar:g.dataContexts.map[primitiveVariables].flatten.filter[!isIsList]»
«««	(primitive_«primitiveVar.name.escapeDart»_update)="«primitiveVar.name.escapeDart» = $event"
«««	«ENDFOR»
	 >
	 «new BaseTemplate().baseContent(ElementCollector.getElementsV(form.allNodes))»
	</form>
	'''
	
	/**
	 * Helper method to generate the form submit button validation surrounding
	 * error message
	 */
	def preButton(Form form){
		return '''
			«IF !form.errorMessage.nullOrEmpty»
				<div
					«IF !preview»*ngIf="«form.getFormHasErrors(true)»"«ENDIF»
					class="alert alert-danger">«form.errorMessage»</div>
			«ENDIF»
			'''
	}
	
	
	def getFormHasErrors(Form form,boolean considerTouched){
		return '''
			(!«ConventionHelper.getFormName(form)».valid
				«IF considerTouched» && !«ConventionHelper.getFormName(form)»
					.pristine 
				«ENDIF»
			)
			«form.getFileValidation(considerTouched)»
			'''
	}
		
		
	/**
	 * Checks if a form contains a validating file input field
	 */
	def getFileValidation(Form form,boolean considerTouched)
	{
		return form.find(Field).map[n|HTMLFieldTemplate.fileValidation(n,form,"||",considerTouched)].join
	}
}