/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import info.scce.dime.generator.gui.dart.base.AngularDartCommonImports
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.TreeIteratorExtension
import info.scce.dime.gui.gui.GUI

/**
 * Helper class for the creation of the HTML template code for an Angular component.
 */
class AngularComponentHTMLTemplate extends GUIGenerator{
	
	extension TreeIteratorExtension<GUI> treeEx = new TreeIteratorExtension
	
	/**
	 * Creates the HTML template code for an Angular component.
	 * The template is surrounded by an security DIV tag which hides the
	 * component HTML code if a user has to be loaded.
	 * In this case the login component is displayed.
	 */
	def create(GUI engine)
	'''
	«IF new AngularDartCommonImports().getCurrentUser(engine) != null»
		<login-form 
			*ngIf="currentUser==null && showLogin == true"
			(signedin)="loadCurrentUser()"
			[modal]="modalDialog"
			></login-form>
		<template [ngIf]="currentUser!=null && showLogin == false">
	«ENDIF»
	«engine.getTemplate(genctx)»
	«IF new AngularDartCommonImports().getCurrentUser(engine) != null»
		</template>
	«ENDIF»
	'''
	
	/**
	 * Creates the plain HTML5 code for an Angular component in preview mode.
	 */
	def preview(GUI engine){
		//check if GUI is known
		if(treeEx.cache.contains(engine)) {
			return '''<p>«engine.title» RECURSION</p>'''
		}
		treeEx.cache.add(engine)
		'''«engine.getTemplate(genctx)»'''
	}
}
