/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.base

import info.scce.dime.generator.gui.dart.functionality.AngularIncludedTemplate
import info.scce.dime.generator.gui.dart.component.AngularDartComponentTemplate
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.TemplateHelper
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB

/**
 * Template for creating the Angular Dart code for an embedded GUI SIB in a GUI model.
 */
class AngularDartIncludedTemplate extends AngularIncludedTemplate{
	
	/**
	 * Generate the import statement for the Angular Dart component of the GUI model referenced by the GUI SIB
	 */
	def CharSequence createImports(GUISIB cgs)
	{
		cgs.gui.createImports
	}
	
	/**
	 * Generate the import statement for the Angular Dart component of the GUI model referenced by the GUI SIB
	 */
	def CharSequence createImports(GUI cgs)
	'''
	import 'package:app/src/gui/«TemplateHelper.classFormat(new AngularDartComponentTemplate().getGUIName(cgs).toString).toFirstUpper».dart';
	'''
	
	
	/**
	 * Generate the on change code, to refresh recursively all embedded GUI models
	 */
	def createOnChange(GUI g)
	'''
	«FOR sp:distinct(g.find(GUISIB))»
		this.refresh«ConventionHelper.cincoID(sp)»Trigger = new Refresher();
	«ENDFOR»
	'''
	
}