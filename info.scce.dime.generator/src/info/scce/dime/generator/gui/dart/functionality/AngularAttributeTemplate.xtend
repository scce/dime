/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import info.scce.dime.generator.gui.dart.base.AngularDartCommonImports
import info.scce.dime.generator.gui.dart.component.AngularCommonComponentTemplate
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.livevariable.LiveVariableDartTemplate
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.TimestampInputStatic
import info.scce.dime.gui.gui.Variable

/**
 * Template for the generation of Dart class fields,
 * the input variable declaration by annotations
 * and the fields and methods for the user service
 */
class AngularAttributeTemplate extends GUIGenerator {

	/**
	 * Creates the class field declaration.
	 * Input variables are additionally annotated with @Input
	 * The variables are collected from all data context in the given GUI.
	 * The selective data types are inferred from the given compound view.
	 * If the isInput flag is set, all variables are annotated as input.
	 */
	
	def createAttributeDeclaration(GUI gui,GUICompoundView gcv,boolean isInput)
	'''
	«IF new AngularDartCommonImports().getCurrentUser(gui) != null»
		bool showLogin = false;
	«ENDIF»
	//DATA CONTEXT
	  «FOR variable:gcv.compounds»
		  «IF variable instanceof ComplexTypeView»
		  //«variable.type.name» «variable.name»
		  	«IF (variable.data as ComplexVariable).isInput || isInput»
		  		@Input()
		  	«ENDIF»
	  		«val dataType = (variable.data as ComplexVariable).dataType»
		  	«IF variable.list»
		  		DIMEList<
		  	«ENDIF»
		  	«dataType.rootElement.modelName.escapeDart».«dataType.name.escapeDart»
		  	«IF variable.list»
		  		>
		  	«ENDIF»
		  	«variable.name.escapeDart»;
		  «ELSE»
		  	«IF (variable.data as PrimitiveVariable).isInput || isInput»@Input()«ENDIF»
		  	«(variable.data as PrimitiveVariable).variableType» «variable.name.escapeDart»;
		  «ENDIF»
	  «ENDFOR»
	  «FOR staticTimestamp:gui.find(TimestampInputStatic)»
	  final staticDate«staticTimestamp.id.escapeDart» = new DateTime.fromMillisecondsSinceEpoch(«staticTimestamp.value.toString»000);
	  «ENDFOR»
	'''
	
	/**
	 * Creates the input variable name declaration for the 
	 * Angular component interface description.
	 * All top level variables of the given compound view are rendered
	 */
	def createInputDeclaration(GUICompoundView gcv)
	'''
	  «FOR variable:gcv.compounds.filter[n|n.data instanceof Variable].filter[n|(n.data as Variable).isInput] SEPARATOR ','»
	  '«variable.name.escapeDart»'
	  «ENDFOR»
	'''
	
	/**
	 * Creates the variable initialization, placed in the Angular Dart class constructor.
	 * Every complex variable is initialized by the new operator and the primitive
	 * variables are initialized by their default value.
	 */
	def createAttributeInit(GUI gui,GUICompoundView gcv)
	'''
	//DATA CONTEXT
	«FOR variable:gcv.compounds»
		«IF variable instanceof ComplexTypeView»
			// «(variable as ComplexTypeView).type.name» «variable.name»
			this.«variable.name.escapeDart» = «IF variable.list»new DIMEList()«ELSE»null«ENDIF»;
		«ELSEIF variable instanceof PrimitiveTypeView»
			// «(variable as PrimitiveTypeView).primitiveType.literal» «variable.name»
			this.«variable.name.escapeDart» = «(variable.data as PrimitiveVariable).defaultValue»;
		«ENDIF»
	«ENDFOR»
	'''
	
	/**
	 * Creates a method call to load the current user,
	 * if one is used in the given GUI model.
	 */
	def createUserService(GUI gui,GUICompoundView gcv) {
		val adci = new AngularDartCommonImports
		'''
		«IF adci.getCurrentUser(gui) != null»
			«IF gcv.pairs.get(adci.getCurrentUser(gui))!=null»
			this.loadCurrentUser();
			«ENDIF»
		«ENDIF»
		'''
	}
	/**
	 * Creates the method loadCurrentUser to load the current user,
	 * with the injected user service if one is used in the given GUI model.
	 * The user is loaded asynchronously by the service.
	 * After the load, a refresh is triggered.
	 * If the user cannot be loaded because of an authentication error,
	 * the log in form component is displayed instead of the GUI model.
	 */
	def createUserServiceMethods(GUI gui,GUICompoundView gcv, LiveVariableDartTemplate liveVariableFrontendTemplate) {
		val adci = new AngularDartCommonImports
		
		'''
		«IF adci.getCurrentUser(gui) != null»
			«IF gcv.pairs.get(adci.getCurrentUser(gui)) !== null»
			«val userType = (gcv.pairs.get(adci.getCurrentUser(gui)) as ComplexTypeView).type»
		void loadCurrentUser()
		{
			this.«(AngularDartCurrentUserService.getName(gcv.pairs.get(adci.getCurrentUser(gui)) as ComplexTypeView)).toString.toFirstLower»Service.syncUser().then((value){
				this.«(gcv.pairs.get(adci.getCurrentUser(gui)) as ComplexTypeView).name.escapeDart» = «userType.rootElement.modelName.escapeDart».«userType.name.escapeDart».fromJSON(value);
				this.showLogin = false;
				updateImageHash();
				openWebsockets();
				«new AngularCommonComponentTemplate().afterLogin(gui)»
			}).catchError((error){
				if(error.currentTarget.status != 200){
					this.showLogin = true;
				}
			});
		}
			«ENDIF»
		«ENDIF»
		'''
	}
}
