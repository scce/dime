/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.utils.GUIGenerator

/**
 * Template to create an Angular Dart service class file.
 * The service is used to fetch the current user of the application.
 */
class AngularDartCurrentUserService extends GUIGenerator{
	
	/**
	 * Generates the Angular Dart service class file.
	 * Used to fetch the current user of the given complex type view.
	 */
	def create(ComplexTypeView ctv)
	'''
	import 'dart:async';
	import 'dart:html';
	import 'package:angular/core.dart';
	import 'package:app/src/core/dime_process_service.dart';
	
	class «getName(ctv)»Service {
		
		final DIMEProcessService _processService;
		«getName(ctv)»Service(this._processService);
		
		Future<String> syncUser() async {
			final url = '${DIMEProcessService.getBaseUrl()}/rest/user/current/«ctv.type.name.escapeJava»/«DyWASelectiveDartGenerator.getSelectiveNameJava(ctv)»/private';
			
			return (await HttpRequest.request(
				url,
				method: 'GET',
				requestHeaders: _processService.getDIMERequestHeaders(),
				responseType: 'text',
				withCredentials: false
			)
			.then((response) => response.responseText));
			
		}
		
	}
	'''
	
	/**
	 * Helper method to get the file name
	 */
	static def getName(ComplexTypeView ctv)'''User«ctv.typeName»«ctv.id»'''
	
}
