/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.base

import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.dad.dad.DAD
import info.scce.dime.gUIPlugin.DartPlugin
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.generator.gui.html.HTMLLoadingScreen
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.GUIPlugin
import java.nio.file.Path
import java.util.Collection
import java.util.Set

/**
 * Template for the main index.html file
 */
class HTMLDartIndexTemplate extends GUIGenerator {
	
	extension DimeIOExtension = new DimeIOExtension
	
	val static Set<String> APPLE_FAVICONS = #{
		"57", "60", "72", "76", "114", "120", "144", "152", "180"
	}
	
	val static Set<String> ANDROID_FAVICONS = #{
		"192"
	}
	
	val static Set<String> FAVICONS = #{
		"32", "96", "16"
	}
	
	val static Set<String> MS_FAVICONS = #{
		"144"
	}
	
	/**
	 * Generates the index.html file code
	 */
	def create(String appTitle, Collection<GUIPlugin> plugins, String timeStamp, boolean compressedCSS, boolean preCompressedJS, boolean postCompressedJS) '''
		<!doctype html>
		<!-- production mode -->
		<html lang="en">
			<head>
				<meta charset="UTF-8">
				<base href="/">
				<title>«appTitle»</title>
				«IF faviconDir !== null»
					«FOR fav: APPLE_FAVICONS.filter[ fav | isFilePresent('''apple-icon-«fav»x«fav».png''', faviconDir) ]»
						<link rel="apple-touch-icon" sizes="«fav»x«fav»" href="favicon/apple-icon-«fav»x«fav».png">
					«ENDFOR»
					«FOR fav: ANDROID_FAVICONS.filter[ fav | isFilePresent('''android-icon-«fav»x«fav».png''', faviconDir) ]»
						<link rel="icon" type="image/png" sizes="«fav»x«fav»"  href="favicon/android-icon-«fav»x«fav».png">
					«ENDFOR»
					«FOR fav: FAVICONS.filter[ fav | isFilePresent('''favicon-«fav»x«fav».png''',faviconDir) ]»
						<link rel="icon" type="image/png" sizes="«fav»x«fav»"  href="favicon/favicon-«fav»x«fav».png">
					«ENDFOR»
					«FOR fav: MS_FAVICONS.filter[ fav | isFilePresent('''ms-icon-«fav»x«fav».png''',faviconDir) ]»
						<meta name="msapplication-TileImage" content="favicon/ms-icon-«fav»x«fav».png">
					«ENDFOR»
				«ENDIF»
				<meta name="msapplication-TileColor" content="#da532c">
				<meta name="theme-color" content="#ffffff">
				<link type="text/css" rel="stylesheet" href="css/dime.min.css" />
				<link type="text/css" rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
				«IF compressedCSS»
					<link type="text/css" rel="stylesheet" href="css/combined/combined.css" />
				«ENDIF»
				«FOR s: genctx.dad.additionalStylesheets.filter[startsWith("http")]»
					<link type="text/css" rel="stylesheet" href="«s»" />
				«ENDFOR»
				«FOR s: genctx.dad.preScripts.filter[startsWith("http")]»
					<script type="script" src="«s»"></script>
				«ENDFOR»
				«FOR path: plugins.scriptPaths»
					<link type="text/css" rel="stylesheet" href="«path»"/>
				«ENDFOR»
				«IF preCompressedJS»
					<script type="text/javascript" src="js/precombined.min.js"></script>
				«ENDIF»
				<!-- Workarround SubmitEvent Bug -->
				<script>
					if (typeof dartNativeDispatchHooksTransformer == "undefined") dartNativeDispatchHooksTransformer=[];
					dartNativeDispatchHooksTransformer.push(
						function(hooks) {
							var getTag = hooks.getTag;
							var quickMap = {
								"SubmitEvent": "Event",
							};
							function getTagFixed(o) {
								var tag = getTag(o);
								return quickMap[tag] || tag;
							}
							hooks.getTag = getTagFixed;
						}
					);
				</script>
				<!-- Workarround Chrome 85/86 Bug -->
				<script>
					if (typeof window.MemoryInfo == "undefined") {
						if (typeof window.performance.memory != "undefined") {
							window.MemoryInfo = function () {};
							window.MemoryInfo.prototype = window.performance.memory.__proto__;
						}
					}
				</script>
				<script defer src="main.dart.js"></script>
				<meta name="viewport" content="width=device-width, initial-scale=1">
			</head>
			<body«genctx.dad.settings»>
				<app>«new HTMLLoadingScreen().create()»</app>
				<script type="text/javascript" src="js/dime.min.js"></script>
				<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js" ></script>
				«IF postCompressedJS»
					<script type="text/javascript" src="js/postcombined/postcombined.js"></script>
				«ENDIF»
				«FOR s: genctx.dad.postScripts.filter[isHTTP]»
					<script type="text/javascript" src="«s»"></script>
				«ENDFOR»
				«FOR gui: genctx.usedGUIs»
					<!-- additional javascript for gui «gui.title» -->
					«FOR scriptPath: gui.additionalJavaScript.filter[isHTTP]»
						<script type="text/javascript" src="«scriptPath»"></script>
					«ENDFOR»
				«ENDFOR»
				«FOR path: plugins.scriptPaths»
					<script type="text/javascript" src="«path»"></script>
				«ENDFOR»
			</body>
		</html>
	'''
	
	def private Path getFaviconDir() {
		if (genctx.dad.faviconDirectory.nullOrEmpty) {
			null
		}
		else {
			genctx.dad.projectPath.resolve(genctx.dad.faviconDirectory)
		}
	}
	
	def private Iterable<String> getScriptPaths(Collection<GUIPlugin> plugins) {
		plugins
			.map[function]
			.filter(Function)
			.map[eContainer]
			.filter(Plugin)
			.reject(DartPlugin)
			.map[script]
			.filterNull
			.flatMap[files]
			.map[path]
			.filter[isHTTP]
	}
	
	def private boolean isFilePresent(String fav, Path path) {
		if (!path.isDirectory) {
			return false
		}
		return path.list.toList.exists[fileNameWithExtension == fav]
	}
	
	def private String getSettings(DAD dad) {
		if (dad.advacedSettings !== null) {
			return '''«IF !dad.advacedSettings.bodyClass.nullOrEmpty» class="«dad.advacedSettings.bodyClass»" «ENDIF»«IF !dad.advacedSettings.bodyStyle.nullOrEmpty» style="«dad.advacedSettings.bodyStyle»" «ENDIF»'''
		}
		return ''''''
	}
	
}
