/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import graphmodel.Node
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularIFFORTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.BaseElement
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexExtensionAttribute
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DataTarget
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveExtensionAttribute
import info.scce.dime.gui.gui.PrimitiveListAttribute
import info.scce.dime.gui.gui.PrimitiveListAttributeName
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.TableChoice
import info.scce.dime.gui.gui.TableColumnLoad
import info.scce.dime.gui.gui.TableEntry
import info.scce.dime.gui.gui.TableLoad
import info.scce.dime.gui.helper.ElementCollector
import java.util.ArrayList

/**
 * Template to generate an Angular HTML template for a table Angular Dart component class
 */
class AngularTableHTMLTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular HTML template code for a table component
	 */
	def create(Table table,GUI gui)
	{
	'''
	«IF !table.responsive»
	<!-- table is responsive -->
	<div «table.printStyle» class="table-responsive" >
	«ENDIF»
		<table «table.printStyle» [attr.id]="'table'+hashCode.toString()" class="table«getClasses(table)»">
			«IF table.tableEntrys.exists[!label.nullOrEmpty]»
				<thead>
					<tr>
				«IF table.reSortable»
					<!-- table resorting is enabled -->
					<th>&nbsp;</th>
				«ENDIF»
				«IF table.numbering»
					<!-- table numbering is enabled -->
					<th>#</th>
				«ENDIF»
				«IF table.choices != TableChoice.NONE»
					<!-- table choices is enabled -->
					<th></th>
				«ENDIF»
					«FOR node:ElementCollector.getElementsH(table.allNodes).filter(TableEntry)»
						<!-- table column «node.label» -->
						«getTableHeader(node,table)»
					«ENDFOR»
					</tr>
				</thead>
			«ENDIF»
				<tbody>
				«IF isFilterIncluded(table)»
					<!-- table filtering is enabled -->
					<tr>
					«IF table.numbering»
						<td></td>
					«ENDIF»
					«IF table.choices != TableChoice.NONE»
						<td></td>
					«ENDIF»
					«FOR node:ElementCollector.getElementsH(new ArrayList<Node>(table.tableEntrys)).filter(TableEntry)»
						<!-- table filter for column «node.label» -->
						«IF node.fullTextSearch»
							<td
								«_iffor.create(node)»
								«node.printStyle»
							>
								«node.filter()»
							</td>
						«ELSE»
							<td
								«_iffor.create(node)»
							></td>
						«ENDIF»
					«ENDFOR»
					</tr>
				«ENDIF»
			«{
				var end = 0;
				
				// render 4 rows statically in preview mode
				if(preview)end = 3;
				'''
				«FOR i:0..end»
					<tr
						«IF table.reSortable»
							draggable="true"
							(mousedown)="handleMousedown($event)"
							(dragstart)="handleDragstart(«getIteratorName(table,_iffor)», $event)"
							(dragover)="handleDragover($event)"
							(dragenter)="handleDragenter($event)"
							(dragleave)="handleDragleave($event)"
							(dragend)="handleDragend()"
							(drop)="handleDrop(«ConventionHelper.cincoID(table)»Num, $event)"
						«ENDIF»
						class="«IF table.reSortable»draggable«ENDIF»"
						data-table-id="«table.id»"
						«table.rowStyle.printStyle»
						«IF !preview»
							*ngFor="let «getIteratorName(table,_iffor)» of table«ConventionHelper.cincoID(table)»getElements(source«table.id.escapeDart»); let «ConventionHelper.cincoID(table)»Num = index«IF table.isComplex»; trackBy: trackSelective«ENDIF»"
						«ENDIF»
						>
						«IF table.reSortable»
							<!-- table is resortable -->
							<td width="1">
								<span class="drag-handle glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
							</td>
						«ENDIF»
						«IF table.numbering»
							<!-- table index column -->
							<td>{{(«ConventionHelper.cincoID(table)»Num+1)«IF table.pagination>0»+(table«ConventionHelper.cincoID(table)»CurrentPage*«table.pagination»)«ENDIF»}}</td>
						«ENDIF»
						«IF table.choices != TableChoice.NONE»
							<!-- table choice column -->
							<td>
								<input
								«IF !preview»
									data-cinco-id="«table.id.escapeString»"
									[value]="«getIteratorName(table,_iffor)»"
									(click)="choiceClicked(«getIteratorName(table,_iffor)»)"
								«ENDIF»
								name="table«ConventionHelper.cincoID(table)»"
								«IF table.choices == TableChoice.SINGLE»
									type="radio"
									«IF !preview»
									[checked]="«getIteratorName(table,_iffor)»==«DataDartHelper.getBindedDataName(table,DataAccessType.CHOICE,MODE.GET_INIT,DataTarget)»"
									«ENDIF»
								«ELSE»
									type="checkbox"
									«IF !preview»
									[checked]="«DataDartHelper.getBindedDataName(table,DataAccessType.CHOICE,MODE.GET_INIT,DataTarget)».indexOf(«getIteratorName(table, _iffor)»)>-1"
									«ENDIF»
								«ENDIF»
									>
							</td>
						«ENDIF»
						«FOR TableEntry node:ElementCollector.getElementsH(new ArrayList<Node>(table.allNodes)).filter[n|n instanceof TableEntry].map[n|n as TableEntry]»
							<!-- table column «node.label»-->
							«getTableCell(node, table, _iffor)»
						«ENDFOR»
					</tr>
				«ENDFOR»
				'''
				}»
			
		</tbody>
	</table>
	«IF isDropTarget(table)»
	<div *ngIf="source«table.id.escapeDart».length == 0" class="alert alert-info drop-placeholder" (dragover)="handleDragover($event)" (drop)="handleDropFromPlaceholder($event)">
		Drop items here to add them to the table
	</div>
	«ENDIF»
	
	«IF table.pagination > 0»
		<!-- table pagination enabled -->
		<nav *ngIf="table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length>1">
		  <ul class="pagination">
		    
		    <li
		    	«IF !preview»
		    		[ngClass]="table«ConventionHelper.cincoID(table)»CurrentPage<=0?'disabled':''"
		        «ENDIF»
	    	>
		    	<a href
			    	«IF !preview»
			    		data-cinco-id="«table.id.escapeString»"
			    		(click)="table«ConventionHelper.cincoID(table)»changePage(table«ConventionHelper.cincoID(table)»CurrentPage-1,$event)"
			        «ENDIF»
		    		aria-label="Previous"
		    	>
		    		<span aria-hidden="true">&laquo;</span>
		    	</a>
			</li>
	    	<template [ngIf]="table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length<5">
			    <li 
				    «IF !preview»
				    	*ngFor="let page«ConventionHelper.cincoID(table)» of table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size)"
				    	[class]="page«ConventionHelper.cincoID(table)»==table«ConventionHelper.cincoID(table)»CurrentPage?'active':''"
				    «ENDIF»
			    >
				    <a href
					    «IF !preview»
					    	data-cinco-id="«table.id.escapeString»"
					    	(click)="table«ConventionHelper.cincoID(table)»changePage(page«ConventionHelper.cincoID(table)»,$event)"
					    «ENDIF»
				    >{{ page«ConventionHelper.cincoID(table)»+1 }}</a>
		    	</li>     
	    	</template>
	    	<template [ngIf]="table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length>=5">
			    <li *ngIf="(table«ConventionHelper.cincoID(table)»CurrentPage+1)>1">
			    <a href data-cinco-id="«table.id.escapeString»"
		    		(click)="table«ConventionHelper.cincoID(table)»changePage(0,$event)"
		        >1</a>
		        </li>
		        	<li class="disabled" *ngIf="(table«ConventionHelper.cincoID(table)»CurrentPage+1)>3" ><a href="#">...</a></li>
		        	<li
		        		*ngIf="(table«ConventionHelper.cincoID(table)»CurrentPage+1)>2"
		        	>
		    	    	<a href
		    	    		data-cinco-id="«table.id.escapeString»"
		    	    	    (click)="table«ConventionHelper.cincoID(table)»changePage(table«ConventionHelper.cincoID(table)»CurrentPage-1,$event)"
		        	    >{{ (table«ConventionHelper.cincoID(table)»CurrentPage+1)-1 }}</a>
		        </li>
		        	<li class="active">
		    	    	<a href
		    	    		data-cinco-id="«table.id.escapeString»"
		    	    	    (click)="table«ConventionHelper.cincoID(table)»changePage(table«ConventionHelper.cincoID(table)»CurrentPage,$event)"
		        	    >{{ table«ConventionHelper.cincoID(table)»CurrentPage+1 }}</a>
		        </li>
		        <li
		        	    *ngIf="table«ConventionHelper.cincoID(table)»CurrentPage<(table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length-2)"
		        	>
		    	    	<a href
		    	    		data-cinco-id="«table.id.escapeString»"
		    	    	    (click)="table«ConventionHelper.cincoID(table)»changePage(table«ConventionHelper.cincoID(table)»CurrentPage+1,$event)"
		        	    >{{ (table«ConventionHelper.cincoID(table)»CurrentPage+1)+1 }}</a>
		        </li>
		        <li class="disabled" *ngIf="(table«ConventionHelper.cincoID(table)»CurrentPage+1)<(table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length-2)" ><a href="#">...</a></li>
		        	<li *ngIf="(table«ConventionHelper.cincoID(table)»CurrentPage+1)<(table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length)">
		    	    	<a href
		    	    		data-cinco-id="«table.id.escapeString»"
		    	    	    (click)="table«ConventionHelper.cincoID(table)»changePage(table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length-1,$event)"
		        	    >{{ table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length }}</a>
		        </li>
	    	</template>
	    	<li
	    	    «IF !preview»
	    	    		[ngClass]="table«ConventionHelper.cincoID(table)»CurrentPage>=table«ConventionHelper.cincoID(table)»getPageCount(table«ConventionHelper.cincoID(table)»_size).length-1?'disabled':''"
	    	    «ENDIF»
			>
	    		<a href
	    	    «IF !preview»
		    	   	data-cinco-id="«table.id.escapeString»"
		    	   	(click)="table«ConventionHelper.cincoID(table)»changePage(table«ConventionHelper.cincoID(table)»CurrentPage+1,$event)"
	    	    «ENDIF»
	    	    aria-label="Next">
	    	        <span aria-hidden="true">&raquo;</span>
	    	    </a>
			</li>
	  </ul>
	</nav>
	«ENDIF»
	«IF !table.responsive»
	</div>
	«ENDIF»
	'''
	}
	
	private def isDropTarget(Table table) {
		if (table.reSortable) return true;
		
		for (t: ReferenceRegistry.instance.lookup(Table)) {
			for (dt: t.dropTargets) {
				if (dt.tableID == table.id) return true;
			}
		}
		return false;
	}
	
	def filter(TableEntry entry)
	'''
	«IF entry.getIncoming(TableColumnLoad).get(0).sourceElement.isNumber»
	<div class="row">
		<div class="col-xs-6 div-filter-from">
			<input type="number" class="form-control from-filter" placeholder="«entry.filterLowerHint.orIfEmpty("from")»"
					«IF !preview»
					data-cinco-id="«entry.id.escapeString»"
					(keyup)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					(click)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					(search)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					(input)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					«ENDIF»
				/>
		</div>
		<div class="col-xs-6 div-filter-to">
		<input type="number" class="form-control to-filter" placeholder="«entry.filterUpperHint.orIfEmpty("to")»"
				«IF !preview»
				data-cinco-id="«entry.id.escapeString»"
				(keyup)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				(click)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				(search)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				(input)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				«ENDIF»
				/>
		</div>
	</div>
	«ELSEIF entry.getIncoming(TableColumnLoad).get(0).sourceElement.isDate»
	<div class="row">
		<div class="col-xs-6 div-filter-from">
			<input data-date-column="«ConventionHelper.cincoID(entry)»_from" type="text" class="form-control from-filter form_datetime" placeholder="«entry.filterLowerHint.orIfEmpty("from")»"
					«IF !preview»
					data-cinco-id="«entry.id.escapeString»"
					(keyup)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					(click)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					(input)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					(search)="columnFilter«ConventionHelper.cincoID(entry)»SubmitFrom($event.target.value)"
					«ENDIF»
				/>
		</div>
		<div class="col-xs-6 div-filter-to">
		<input data-date-column="«ConventionHelper.cincoID(entry)»_to" type="text" class="form-control to-filter form_datetime" placeholder="«entry.filterUpperHint.orIfEmpty("to")»"
				«IF !preview»
				data-cinco-id="«entry.id.escapeString»"
				(keyup)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				(click)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				(input)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				(search)="columnFilter«ConventionHelper.cincoID(entry)»SubmitTo($event.target.value)"
				«ENDIF»
				/>
		</div>
	</div>
	«ELSEIF entry.getIncoming(TableColumnLoad).get(0).sourceElement.isEnum»
	<select
		class="form-control"
		data-cinco-id="«entry.id.escapeString»"
		(change)="columnFilter«ConventionHelper.cincoID(entry)»Submit($event.target.value)"
	>
		<option value="" selected></option>
		«FOR literal: entry.getIncoming(TableColumnLoad).get(0).sourceElement.getEnum.enumLiterals»
		«val value = if (literal.displayName.nullOrEmpty) literal.name else literal.displayName»
		<option value="«value»">«value»</option>
		«ENDFOR»
	</select>
	«ELSEIF entry.getIncoming(TableColumnLoad).get(0).sourceElement.isBoolean»
	<select
		class="form-control filter-select"
		data-cinco-id="«entry.id.escapeString»"
		(change)="columnFilter«ConventionHelper.cincoID(entry)»Submit($event.target.value)"
	>
		<option value="" selected>All</option>
		<option value="true">True</option>
		<option value="false">False</option>
	</select>
	«ELSE»
	<input type="search" class="form-control filter-input" placeholder="«entry.filterPlaceholder.orIfEmpty("🔍 Search...")»"
		«IF !preview»
			data-cinco-id="«entry.id.escapeString»"
			(keyup)="columnFilter«ConventionHelper.cincoID(entry)»Submit($event.target.value)"
			(click)="columnFilter«ConventionHelper.cincoID(entry)»Submit($event.target.value)"
			(search)="columnFilter«ConventionHelper.cincoID(entry)»Submit($event.target.value)"
			(input)="columnFilter«ConventionHelper.cincoID(entry)»Submit($event.target.value)"
			«ENDIF»
		/>
	«ENDIF»
	'''
	
	def dispatch EnumType getEnum(ComplexAttribute node){
		return (node as ComplexAttribute).attribute.dataType as EnumType
	}
	def dispatch EnumType getEnum(ComplexExtensionAttribute node){
		return node.attribute.complexDataType as EnumType
	}
	def dispatch EnumType getEnum(ComplexVariable node){
		return (node as ComplexVariable).dataType as EnumType
	}
	
	def dispatch boolean getIsText(PrimitiveAttribute node){
		return (node as PrimitiveAttribute).attribute.dataType==PrimitiveType.TEXT
	}
	def dispatch boolean getIsText(PrimitiveExtensionAttribute node){
		return node.attribute.primitiveDataType==PrimitiveType.TEXT
	}
	def dispatch boolean getIsText(Node node){
		return false
	}
	
	def dispatch boolean getIsEnum(ComplexAttribute node){
		return (node as ComplexAttribute).attribute.dataType instanceof EnumType
	}
	def dispatch boolean getIsEnum(ComplexExtensionAttribute node){
		return node.attribute.complexDataType instanceof EnumType
	}
	def dispatch boolean getIsEnum(ComplexVariable node){
		return (node as ComplexVariable).dataType instanceof EnumType
	}
	def dispatch boolean getIsEnum(Node node){
		return false
	}

	static def dispatch boolean getIsNumber(PrimitiveAttribute node){
		return (node as PrimitiveAttribute).attribute.dataType==PrimitiveType.INTEGER ||
		(node as PrimitiveAttribute).attribute.dataType==PrimitiveType.REAL
	}
	static def dispatch boolean getIsNumber(PrimitiveListAttribute node){
		return node.attributeName==PrimitiveListAttributeName.SIZE
	}
	static def dispatch boolean getIsNumber(PrimitiveExtensionAttribute node){
		return node.attribute.dataType=="Integer" ||
		node.attribute.dataType=="Real"
	}
	static def dispatch boolean getIsNumber(Node node){
		return false
	}
	static def dispatch boolean getIsBoolean(PrimitiveAttribute node){
		return node.attribute.dataType==PrimitiveType.BOOLEAN
	}
	static def dispatch boolean getIsBoolean(PrimitiveExtensionAttribute node){
		return node.attribute.dataType=="Boolean"
	}
	static def dispatch boolean getIsBoolean(ComplexExtensionAttribute node){
		return false
	}
	static def dispatch boolean getIsBoolean(ComplexAttribute node){
		return false
	}
	static def dispatch boolean getIsBoolean(PrimitiveVariable node){
		return node.dataType==PrimitiveType.BOOLEAN
	}
	static def dispatch boolean getIsDate(PrimitiveAttribute node){
		return node.attribute.dataType==PrimitiveType.TIMESTAMP
	}
	static def dispatch boolean getIsDate(PrimitiveExtensionAttribute node){
		return node.attribute.dataType=="Timestamp"
	}
	
	static def dispatch boolean getIsDate(PrimitiveListAttribute node){
		return false
	}
	static def dispatch boolean getIsDate(Node node){
		return false
	}
	
	def boolean getIsComplex(Table table){
		var source = table.getIncoming(TableLoad).map[sourceElement].filter[n|n instanceof PrimitiveVariable||n instanceof PrimitiveAttribute||n instanceof PrimitiveExtensionAttribute];
		return source.empty;
	}
	
	/**
	 * Generates the sorting table header for a given column
	 */
	private def getTableHeader(TableEntry entry,Table table)
	'''
	«IF entry.sortable»
		<!-- sotring enabled for column «entry.label» -->
		<th «entry.printNgIfFor» «entry.printStyle»><a href
		«IF !preview»
			data-cinco-id="«entry.id.escapeString»"
			(click)="table«ConventionHelper.cincoID(entry)»Sort($event)"
		«ENDIF»
		>«entry.label» <span
		«IF !preview»
			[ngClass]="table«ConventionHelper.cincoID(table)»IsSorted('colId«ConventionHelper.cincoID(entry)»')"
		«ENDIF»
		></span></a></th>
	«ELSE»
		<!-- no sotring for column «entry.label» -->
		<th «entry.printNgIfFor» «entry.printStyle»>«entry.label»</th>
	«ENDIF»
	'''
	
	def getIteratorName(Table table,AngularIFFORTemplate iffor)
	{
		for(edge:table.getIncoming(TableLoad))
		{
			return iffor.getIteratorVariable(edge);
		}
	}
	
	def isFilterIncluded(Table table) {
		for(Node node:table.allNodes){
			if(node instanceof TableEntry){
				if(node.fullTextSearch)return true;
			}
		}
		return false;
	}
	
	def getTableCell(TableEntry entry,Table table,AngularIFFORTemplate iffor){
		return 
		'''
		<td «iffor.create(entry,preview)» «entry.printStyle»>
			«IF !entry.allNodes.exists[n|n instanceof BaseElement]»
					{{tableValue«ConventionHelper.cincoID(entry)»Provider(«getIteratorName(table,iffor)») }}
			«ELSE»
				«new BaseTemplate().baseContent(ElementCollector.getElementsV(entry.allNodes))»
			«ENDIF»
		</td>
		'''
	}
	
	def getClasses(Table table){
		var css = "";
		if (table.striped) css += " table-striped";
		if (table.bordered) css += " table-bordered";
		if (table.hover) css += " table-hover";
		if (table.condensed) css += " table-condensed";
		if (table.reSortable) css += " table-reSortable";
		return css;
	}
}
