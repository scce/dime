/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.ISSIB
import info.scce.dime.gui.gui.SecuritySIB

class AngularDartControlSIBTemplate extends GUIGenerator {
	
	def dispatch String create(IFSIB sib){
		_iffor.createIFSIB(sib).toString
	}
	
	def dispatch String create(ISSIB sib){
		_iffor.createISSIB(sib).toString
	}
	
	def dispatch String create(SecuritySIB sib){
		new AngularDartSecuritySIBTemplate().create(sib).toString
	}
	
	def dispatch String create(FORSIB sib){
		_iffor.createFORSIB(sib).toString
	}
	
	//TODO: check empty methods
	
	def dispatch String createMethod(IFSIB sib){
		""
	}
	
	def dispatch String createMethod(FORSIB sib){
		""
	}
	
	def dispatch String createMethod(SecuritySIB sib){
		""
	}
	
	def dispatch String createMethod(ISSIB sib){
		_iffor.createISSIBMethod(sib).toString
	}
}
