/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.base

import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.gui.gui.GUI

/**
 * Template to generate general methods important for every GUI model Angular Dart component class file
 */
class DartBaseTemplate extends info.scce.dime.generator.gui.BaseTemplate {
	
	/**
	 * Generates general methods for the GUI model Angular Dart component class file
	 */
	
	def createMethods(GUI gui)
	'''
	/// returns the surrounding container class for major GUI models
	String getContainer«ConventionHelper.cincoID(gui)»RootClass()
	{
		if(this.ismajorpage)return "";
		return "";
	}
	
	/// returns the surrounding wrapper class for major GUI models
	String getContainer«ConventionHelper.cincoID(gui)»Id()
	{
		if(this.ismajorpage)return "wrapper";
		return "«ConventionHelper.cincoID(gui)»";
	}
	
	/// returns the surrounding container class for major GUI models
	String getContainer«ConventionHelper.cincoID(gui)»Class()
	{
		if(this.ismajorpage)return "container-display";
		return "";
	}
	
	/// callback, to go back to the root interaction
	void redirect«ConventionHelper.cincoID(gui)»ToHome(dynamic e)
	{
		e.preventDefault();
		this.router.navigate(Routes.root.toUrl());
	}
	'''
}
