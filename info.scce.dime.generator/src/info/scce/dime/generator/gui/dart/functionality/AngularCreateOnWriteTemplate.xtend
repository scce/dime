/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Variable

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

/**
 * Template for method generation to enable the data handling in an Angular Dart component.
 */
class AngularCreateOnWriteTemplate extends GUIGenerator {
	
	extension DataExtension = DataExtension.instance
	
	/**
	 * Creates the init on demand methods placed in an selective Dart class.
	 * For every given variable, a set value and an init on demand method is generated.
	 * The init on demand methods can be used to initialize a variable if needed and return it.
	 * The set value method is a simple getter method with the static prefix "setValue". 
	 */
	def createMethods(Iterable<Variable> vars,GUI gui,GUICompoundView gcv,boolean isForm)
	'''
	«FOR variable:gcv.compounds»
		«variable.selectiveClassName(variable.list)» initOnDemand«variable.name.escapeDart»()
		{
			«IF variable instanceof ComplexTypeView && !(variable as ComplexTypeView).type.abstract»
			if(this.«variable.name.escapeDart»==null){
				this.«variable.name.escapeDart» = new «variable.selectiveClassName(variable.list)»();
				«IF isForm && !variable.isList»
				this._complex_«variable.name.escapeDart»_update.add(this.«variable.name.escapeDart»);
				«ENDIF»
			}
			«ENDIF»
			return this.«variable.name.escapeDart»;
		}
		void setValue«variable.name.escapeDart»(«variable.selectiveClassName(variable.list)» value)
		{
			this.«variable.name.escapeDart» = value;
			«IF isForm && variable instanceof ComplexTypeView && !variable.isList»
			this._complex_«variable.name.escapeDart»_update.add(this.«variable.name.escapeDart»);
			«ENDIF»
		}
		void «variable.name.escapeDart»setValue(«variable.selectiveClassName(variable.list)» value)
		{
			this.setValue«variable.name.escapeDart»(value);
		}
		«IF variable.list»
		void «variable.name.escapeDart»add(«variable.selectiveClassName(false)» value)
		{
			this.«variable.name.escapeDart».add(value);
		}
		«ENDIF»
	«ENDFOR»
	'''
}
