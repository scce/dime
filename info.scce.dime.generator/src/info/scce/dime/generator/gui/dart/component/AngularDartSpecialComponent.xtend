/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.SpecialElement
import info.scce.dime.gui.gui.TODOList

/**
 * Template for special elements. E.g the todo list component
 */
class AngularDartSpecialComponent extends GUIGenerator{
	
	/**
	 * Generates the needed variable declarations
	 */
	def createDeclaration(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createDeclaration(element);
	}
	
	/**
	 * Generates the needed variable initialization
	 */
	def createInitialization(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createInitialization(element);
	}
	
	/**
	 * Generates the needed variable initialization after the inputs are loaded
	 */
	def createOnInit(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createOnInit(element);
	}
	
	/**
	 * Generates the needed variable initialization after the user has logged in
	 */
	def afterLogin(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createOnInit(element);
	}
	
	/**
	 * Generates the needed methods
	 */
	def createMethods(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createMethods(element);
	}
	
	/**
	 * Generates the needed import statements
	 */
	def createImports(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createImports(element);
	}
}
