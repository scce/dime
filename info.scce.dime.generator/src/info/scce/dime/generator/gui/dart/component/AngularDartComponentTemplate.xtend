/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import info.scce.dime.generator.gui.dart.base.DartBaseTemplate
import info.scce.dime.generator.gui.dart.base.AngularDartCommonImports
import info.scce.dime.generator.gui.dart.functionality.AngularAttributeTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartCurrentUserService
import info.scce.dime.generator.gui.dart.functionality.AngularDartEventTemplate
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.TemplateHelper
import info.scce.dime.generator.livevariable.LiveVariableDartTemplate
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.PrimitiveVariable

/**
 * Template for the creation of an Angular Dart component class for a given GUI model.
 */
class AngularDartComponentTemplate extends GUIGenerator {
	
	/**
	 * Helper method to get the Angular Dart component class file name for a given GUI model
	 */
	
	def getGUIName(GUI gui)'''«TemplateHelper.classFormat(gui.title).escapeDart.toFirstUpper»«gui.id.escapeDart»'''
	
	/**
	 * Generates the code for an Angular Dart component class file for a given GUI model
	 */
	def create(GUI engine,GUICompoundView gcv)
	'''
		«new AngularDartCommonImports().createImports(false,engine,gcv)»
		«new AngularCommonComponentTemplate().imports(engine,engine,gcv)»
		«val liveVariableDartTempalte = new LiveVariableDartTemplate(gcv)»
		«liveVariableDartTempalte.generateImports»
		import 'dart:js' as js;
		import 'package:app/src/core/AbstractRoutes.dart';

		@Component(
		  selector: "«TemplateHelper.tagFormat(engine.title)»",
		  «IF new AngularDartCommonImports().getCurrentUser(engine) != null»
		  providers: const [ClassProvider(«AngularDartCurrentUserService.getName(gcv.pairs.get(new AngularDartCommonImports().getCurrentUser(engine)) as ComplexTypeView)»Service)],
		  «ENDIF»
		  «new AngularCommonComponentTemplate().otherSpecifications(engine,engine)»
		  templateUrl: '«getGUIName(engine)».html'
		)
		
		class «getGUIName(engine)» extends dime.DIMEComponent implements OnInit, OnDestroy, AfterViewInit {
		  
		  	«new AngularCommonComponentTemplate().declaration(engine,engine,gcv)»
		  	bool hasToSignIn;
			«new AngularAttributeTemplate().createAttributeDeclaration(engine,gcv,false)»
			
			«liveVariableDartTempalte.generateVariablesDeclaration»
		  
			«getGUIName(engine)»(DIMEProcessService this.processService, Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes«new AngularDartCommonImports().createConstructorParameters(engine,gcv)») : super(domSanitizationService,processService,routes)
			{
				restartComponent();
			}
			
			void restartComponent() {
				
				this.hasToSignIn = false;
				
				«new AngularAttributeTemplate().createAttributeInit(engine,gcv)»
								
								
				«new AngularCommonComponentTemplate().constructor(engine)»
				«new AngularCommonComponentTemplate().restart(engine,engine,gcv)»
				updateImageHash();
			}
			
			«new AngularDartEventTemplate().createUpdateMethod(engine,engine,gcv,'''
				«new AngularAttributeTemplate().createUserService(engine,gcv)»
			''')»
			
			
			@override
			void ngOnInit() async
			{
				«new AngularAttributeTemplate().createUserService(engine,gcv)»
				«new AngularCommonComponentTemplate().onInit(engine)»
				
				«liveVariableDartTempalte.generateOnInit»
				openWebsockets();
			}
			
			«liveVariableDartTempalte.generateOpenWebsockets»
			
			@override
			void ngOnDestroy()
			{
				«liveVariableDartTempalte.generateOnDestroy»
			}
			
			
			void ngAfterViewInit() {
				html.window.document.dispatchEvent(new html.CustomEvent('dime-component-ready'));
				js.context.callMethod("enableTooltip",[]);
			}
			
		  	«new AngularCommonComponentTemplate().methods(engine,engine,gcv)»
		  
		  	«new AngularAttributeTemplate().createUserServiceMethods(engine,gcv,liveVariableDartTempalte)»
			«new DartBaseTemplate().createMethods(engine)»
		}
	'''
	
	def getIsInput(TypeView tv) {
		val data = tv.data
		if(data instanceof ComplexVariable){
			return data.isIsInput
		}
		if(data instanceof PrimitiveVariable){
			return data.isIsInput
		}
	}
	
}
