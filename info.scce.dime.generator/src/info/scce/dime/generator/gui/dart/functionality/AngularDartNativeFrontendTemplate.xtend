/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.NativeFrontendSIB
import info.scce.dime.process.process.NativeFrontendSIBLibrary
import info.scce.dime.process.process.NativeFrontendSIBReference
import info.scce.dime.process.process.SIB
import java.io.File

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import graphmodel.Node

class AngularDartNativeFrontendTemplate extends GUIGenerator{
	
	extension FrontEndProcessExtension = new FrontEndProcessExtension
	extension RESTExtension = new RESTExtension

	def getMethodInvocation(NativeFrontendSIBReference it) '''
	objectToCall.«referencedSib.methodName.escapeDart»(
		«FOR input : it.inputs SEPARATOR ","»
			«IF input instanceof InputStatic»
				«input.portParamParserStatic»
			«ELSE»
				input.«input.name.escapeDart»
			«ENDIF»
		«ENDFOR»)'''

	static def getClassName(NativeFrontendSIB it) {
		 new File((container as NativeFrontendSIBLibrary).className).name.replace(".dart", "").escapeDart
	}
	
	/**
	 * Returns the id of a Node without - and _
	 * @param node
	 * @return
	 */
	def cincoID(Node node){
		return node.getId().replace("-", "").replace("\\_", "");
	}

	def nativeFrontend(NativeFrontendSIBReference it) {
		'''
			void triggerNativeFrontend«cincoID»(«getSimpleTypeNameDart»Input input){
					«referencedSib.className» objectToCall = new «referencedSib.className»();
					ContinueProcessRequest result = null;
		''' +
			switch referencedSib.sibType {
				case BOOL: {
					'''
						// boolean sib
						bool outcome = «it.getMethodInvocation()»;
						if(outcome){
							result = «getSimpleTypeNameDart»Branch.fortrueBranch(
							);
						}
						else{
							result = «getSimpleTypeNameDart»Branch.forfalseBranch(
							);
						}
					'''
				}
				case COMPLEX: {
					val output = getBranchByName("success").outputs.head as ComplexOutputPort;
					var typeOfResult = output.portType
					'''
						// complex sib
						try{
							«typeOfResult» invocationResult = «it.getMethodInvocation»;
						
							if(invocationResult == null){
								result = «getSimpleTypeNameDart»Branch.fornoresultBranch();
							
							}
							else{
								result = «getSimpleTypeNameDart»Branch.forsuccessBranch(
									«output.name»:invocationResult
								);
							}
						}catch(exception){
							result = «getSimpleTypeNameDart»Branch.forfailureBranch();
						}
					'''
				}
				case VOID: {
					'''
						// void sib
						try{
							«it.getMethodInvocation»;
							result = «getSimpleTypeNameDart»Branch.forsuccessBranch();
						}catch(exception){
							result = «getSimpleTypeNameDart»Branch.forfailureBranch();
						}
					'''
				}
			} + '''		
				_processService.continueProcess(
					parentRuntime,
					majorStack,
					result
				);
			}
			'''
	}


	def getBranchByName(SIB it, String branchName) {
		successors.filter(Branch).filter(it|name.compareToIgnoreCase(branchName) == 0).head
	}
	

}
