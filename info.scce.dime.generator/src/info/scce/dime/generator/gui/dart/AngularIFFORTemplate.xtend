/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import graphmodel.Container
import graphmodel.Edge
import graphmodel.Node
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexExtensionAttribute
import info.scce.dime.gui.gui.ComplexListAttribute
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeName
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.^FOR
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.gui.Gate
import info.scce.dime.gui.gui.^IF
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.IS
import info.scce.dime.gui.gui.ISSIB
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.Iteration
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveExtensionAttribute
import info.scce.dime.gui.gui.PrimitiveFOR
import info.scce.dime.gui.gui.PrimitiveListAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.TableLoad
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.ElementCollector
import info.scce.dime.gui.helper.GUIExtension
import java.util.LinkedList
import java.util.List

import static extension info.scce.dime.generator.gui.utils.GUIGenerator.*
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.BaseTemplate

/**
 * Template for the rendering of the NG-IF and NG-FOR directives
 * which can be used on every component.
 */
class AngularIFFORTemplate {
	
	extension GUIExtension _guiExtension
	extension DataExtension _dataExtension
	
	new(GenerationContext genctx) {
		this._guiExtension = genctx.guiExtension
		this._dataExtension = genctx.guiExtension.dataExtension()
	}
	
	def create(MovableContainer mc){
		create(mc, false)
	}
	
	private def ifCondition(MovableContainer mc)
	'''«FOR isEdge : mc.getIncoming(IS) SEPARATOR " && "»
					«getInstanceof(isEdge,mc)»
				«ENDFOR»
				«IF !(mc.getIncoming(^IF).empty || mc.getIncoming(IS).empty)»&&«ENDIF»
				«FOR ifEdge : mc.getIncoming(^IF) SEPARATOR " && "»
					«IF ifEdge.negate»!«ENDIF»«getCondition(ifEdge,mc)»
				«ENDFOR»'''
	
	def printNgIf(MovableContainer mc) {
		create(mc, true)
	}
	
	def printNgIfFor(MovableContainer mc) {
		create(mc, false)
	}
	
	/**
	 * Checks if the directives has to be added to the component.
	 * If preview mode is activated, no directive is added.
	 * If onlyIf flag is true, only NG-If directives are added.
	 * NG-For is ignored.
	 */
	def create(MovableContainer mc,boolean onlyIf)
	'''
	«IF !preview»
		«IF isIF(mc)»
		*ngIf="«mc.ifCondition»" 
		«ENDIF» 
		«IF !onlyIf»
			«FOR f:mc.incoming»
				«IF f instanceof ^FOR»
					*ngFor="let «getIteratorVariable(f)» of «DataDartHelper.getBindedDataName(mc,DataAccessType.^FOR,MODE.GET_INIT,Iteration)»«IF !f.index.nullOrEmpty»; let «f.index.escapeDart» = index«ENDIF»; trackBy: trackSelective"  
				«ENDIF»
				«IF f instanceof PrimitiveFOR»
					*ngFor="let «f.iterator» of «DataDartHelper.getBindedDataName(mc,DataAccessType.^FOR,MODE.GET_INIT,Iteration)»«IF !f.index.nullOrEmpty»; let «f.index.escapeDart» = index«ENDIF»" 
				«ENDIF»
			«ENDFOR»
		«ENDIF»
		«IF isIF(mc) || !mc.getIncoming(Iteration).empty»
		aria-live="polite" aria-atomic="true" 
		«ENDIF»
	«ENDIF»
	
	'''
	
	def createFORSIB(FORSIB sib){
		if(sib.complexInputPorts.empty)return ""
		if(sib.complexInputPorts.get(0).getIncoming(Read).empty)return ""
		val port = sib.complexInputPorts.get(0)
		val f = port.getIncoming(Read).get(0)
		'''
			<div «this.create(sib)»>
			<template ngFor let-«getIteratorVariable(f)» [ngForOf]="«DataDartHelper.getBindedDataName(port,DataAccessType.^FOR,MODE.GET_INIT,Read)»«sib.listOperation»" [ngForTrackBy]="trackSelective">
				«new BaseTemplate().baseContent(ElementCollector.getElementsV(sib.arguments.findFirst[blockName.equals("FOR")].allNodes))»
				«IF sib.arguments.exists[blockName.equals("JOIN")]»
				<template [ngIf]="«getIteratorVariable(f)»!=«DataDartHelper.getBindedDataName(port,DataAccessType.^FOR,MODE.GET_INIT,Read)»«sib.listOperation».last">
					«new BaseTemplate().baseContent(ElementCollector.getElementsV(sib.arguments.findFirst[blockName.equals("JOIN")].allNodes))»
				</template>
				«ENDIF»
			</template>
			«IF sib.arguments.exists[blockName.equals("EMPTY")]»
			<template [ngIf]="«DataDartHelper.getBindedDataName(port,DataAccessType.^FOR,MODE.GET_INIT,Read)».isEmpty">
			«new BaseTemplate().baseContent(ElementCollector.getElementsV(sib.arguments.findFirst[blockName.equals("EMPTY")].allNodes))»
			</template>
			«ENDIF»
			</div>
		'''
	}
	
	def String getListOperation(FORSIB cforsib)
	'''«IF cforsib.isSublist».sublist(«cforsib.start»«cforsib.end»)«ENDIF»«IF cforsib.reverse».reversed«ENDIF»'''
	
	def boolean getIsSublist(FORSIB cforsib){
		return cforsib.from>0||cforsib.to>-1
	}
	
	def int getStart(FORSIB cforsib){
		if(cforsib.from<0)return 0
		cforsib.from
	}
	def String getEnd(FORSIB cforsib){
		if(cforsib.to>-1)return ''',«cforsib.to»'''
		""
	}
	
	
	def createISSIBMethod(ISSIB sib)
	{
		if(sib.complexInputPorts.empty)return '''bool checkISSIB«sib.id.escapeDart»(dynamic obj) => false;'''
		val port = sib.complexInputPorts.get(0)
		val type = port.dataType
		'''
		bool checkISSIB«sib.id.escapeDart»(dynamic obj){
			if(obj ==null)return false;
			return (obj is«IF sib.negate»!«ENDIF» «DyWASelectiveDartGenerator.prefix(type)».«type.name.escapeDart»);
		}
		'''
	}
	def createISSIB(ISSIB sib)
	'''
	<div «this.create(sib)»>
	<template [ngIf]="«sib.condition»">
		«new BaseTemplate().baseContent(ElementCollector.getElementsV(sib.arguments.findFirst[blockName.equals("THEN")].allNodes))»
	</template>
	«IF sib.containsElse»
	<template [ngIf]="!(«sib.condition»)">
		«new BaseTemplate().baseContent(ElementCollector.getElementsV(sib.arguments.findFirst[blockName.equals("ELSE")].allNodes))»
	</template>
	«ENDIF»
	</div>
	'''
	
	def String condition(ISSIB cissib){
		if(cissib.complexInputPorts.empty)return "false"
		val port = cissib.complexInputPorts.get(0)
		val type = port.dataType
		return cissib.conditionIS(port,Read,type,cissib.exact,cissib.negate)
	}
	
	def <T> String conditionIS(ISSIB sib,Node dataTarget,Class<T> c,Type type,boolean exact,boolean negate)
	{
		if(exact){
			return '''«IF negate»!(«ENDIF»«DataDartHelper.getBindedDataName(dataTarget,DataAccessType.^FOR,MODE.GET,c)»?.runtimeType=='«type.name.escapeDart.toFirstUpper»'«IF negate»)«ENDIF»'''
		}
		else{
			return '''checkISSIB«sib.id.escapeDart»(«DataDartHelper.getBindedDataName(dataTarget,DataAccessType.^FOR,MODE.GET,c)»)'''
		}
	}
	
	def createIFSIB(IFSIB sib)
	'''
	<div «this.create(sib)»>
	<template [ngIf]="«sib.condition»">
		«new BaseTemplate().baseContent(ElementCollector.getElementsV(sib.arguments.findFirst[blockName.equals("THEN")].allNodes))»
	</template>
	«IF sib.containsElse»
	<template [ngIf]="!(«sib.condition»)">
		«new BaseTemplate().baseContent(ElementCollector.getElementsV(sib.arguments.findFirst[blockName.equals("ELSE")].allNodes))»
	</template>
	«ENDIF»
	</div>
	'''
	
	def String condition(IFSIB cifsib){
		if(cifsib.gate==Gate.XOR){
			return '''«IF cifsib.negate»!(«ENDIF»«FOR port:cifsib.IOs SEPARATOR "||"»(«port.getToPortCondition(cifsib)» && !(«cifsib.IOs.filter[id != port.id].map[getToPortCondition(cifsib)].join(cifsib.gate.toSeparator)»))«ENDFOR»«IF cifsib.negate»)«ENDIF»'''
		}
		else{
			return '''«IF cifsib.negate»!(«ENDIF»«cifsib.IOs.map[getToPortCondition(cifsib)].join(cifsib.gate.toSeparator)»«IF cifsib.negate»)«ENDIF»'''
		}
	}
	
	def getToPortCondition(IO n,IFSIB cifsib){
		if(n instanceof InputStatic){
			return n.staticCondition
		}
		if(n instanceof InputPort){
			if(n.getIncoming(Read).empty){
				return "false"
			}
			return this.createDataBindingCondition(n.getIncoming(Read).get(0).sourceElement,cifsib)
		}
	}
	
	
	def String getToSeparator(Gate gate) {
		switch(gate){
			case AND: return "&&"
			case OR: return "||"
			case XOR: return "||"
		}
	}
	
	def dispatch String getStaticCondition(TextInputStatic static1){
		(!static1.value.nullOrEmpty).toString
	}
	def dispatch String getStaticCondition(IntegerInputStatic static1){
		(static1.value!=0).toString
	}
	def dispatch String getStaticCondition(BooleanInputStatic static1){
		static1.value.toString
	}
	def dispatch String getStaticCondition(RealInputStatic static1){
		(static1.value!=0.0).toString
	}
	def dispatch String getStaticCondition(TimestampInputStatic static1){
		(static1.value!=0).toString
	}
	
	 def boolean containsElse(ISSIB cifsib){
		cifsib.arguments.exists[blockName.equals("ELSE")]
	}
	
	def boolean containsElse(IFSIB cifsib){
		cifsib.arguments.exists[blockName.equals("ELSE")]
	}
	
	def printOpeningNgForTemplateTag(MovableContainer mc) {
		preCreateForTemplateTag(mc)
	}
	
	/**
	 * Renders the opening NG-For template tag for the given component, if an iteration edge is presenet.
	 * The template tags are used for components which cannot be extended by the NG-For directive and has to be surrounded.
	 */
	def preCreateForTemplateTag(MovableContainer mc)
	'''
	«IF !preview»
		«FOR f:mc.incoming»
			«IF f instanceof ^FOR»
				<template ngFor let-«getIteratorVariable(f)» [ngForOf]="«DataDartHelper.getBindedDataName(mc,DataAccessType.^FOR,MODE.GET_INIT,Iteration)»" [ngForTrackBy]="trackSelective"«IF !f.index.nullOrEmpty» let-«f.index.escapeDart»="index"«ENDIF»>
			«ENDIF»
			«IF f instanceof PrimitiveFOR»
				<template ngFor let-«f.iterator» [ngForOf]="«DataDartHelper.getBindedDataName(mc,DataAccessType.^FOR,MODE.GET_INIT,Iteration)»"«IF !f.index.nullOrEmpty» let-«f.index.escapeDart»="index"«ENDIF»>
			«ENDIF»
		«ENDFOR»
	«ENDIF»
	'''

	def printOpeningNgIfTemplateTag(MovableContainer mc) {
		preCreateIfTemplateTag(mc)
	}
	
	def preCreateIfTemplateTag(MovableContainer mc)
	'''
	«IF !preview»
		«IF isIF(mc)»<template [ngIf]="«mc.ifCondition»">«ENDIF»
	«ENDIF»
	'''
	
	def printClosingNgIfTemplateTag(MovableContainer mc) {
		postCreateIfTemplateTag(mc)
	}
	
	def postCreateIfTemplateTag(MovableContainer mc)
	'''
	«IF !preview»
		«IF isIF(mc)»</template>«ENDIF»
	«ENDIF»
	'''
	
	def printClosingNgForTemplateTag(MovableContainer mc) {
		postCreateForTemplateTag(mc)
	}
	
	/**
	 * Renders the closing NG-For template tag for the given component, if an iteration edge is presenet.
	 * The template tags are used for components which cannot be extended by the NG-For directive and has to be surrounded.
	 */
	def postCreateForTemplateTag(MovableContainer mc)
	'''
	«IF !preview»
		«FOR f:mc.incoming»
			«IF f instanceof ^FOR»
			</template>
			«ENDIF»
			«IF f instanceof PrimitiveFOR»
			</template>
			«ENDIF»
		«ENDFOR»
	«ENDIF»
	'''
	
	/**
	 * Determines the list variable in the data context at the source of the given edge.
	 * Returns the name of the list variable.
	 */
	def String getIteratorVariable(Edge cf) {
		if(!(cf.sourceElement instanceof Variable)) return "ERROR";
		var listVariable = cf.sourceElement as Variable;
		if (!listVariable.getOutgoing(ComplexListAttributeConnector).empty) {
			var ccac = listVariable.getOutgoing(ComplexListAttributeConnector).get(0);
			if (ccac.attributeName == ComplexListAttributeName.CURRENT) {
				return (ccac.targetElement as Variable).name.escapeDart;
			}
		}
		return "current";
	}
	
	def getClassName(IS isEdge, boolean prefix)
	{
		val typeId = isEdge.matchingType
		val type = ReferenceRegistry.instance.getEObject(typeId)
		if (type === null) {
			throw new IllegalStateException(
				'''[IS Edge] matching type «typeId» unknown (GUI «isEdge.rootElement.title»)'''
			)
		}
		if (type instanceof Type)
			'''«IF prefix»«DyWASelectiveDartGenerator.prefix(type)».«ENDIF»«type.name.escapeDart»'''
		else "Object"
	}
	
	def getLiteralName(IS isEdge)
	{
		val literalId = isEdge.matchingType
		var source = isEdge.sourceElement
		var Data datamodel = null
		if(source instanceof ComplexAttribute){
			datamodel = (source as ComplexAttribute).attribute.rootElement
		}
		if(source instanceof ComplexExtensionAttribute){
			datamodel = (source as ComplexExtensionAttribute).attribute.rootElement
		}
		if(source instanceof ComplexVariable){
			datamodel = source.dataType.rootElement
		}
		
		val literal = datamodel.enumTypes.map[enumLiterals].flatten.findFirst[n|n.id.equals(literalId)||n.name.equals(literalId)]
		if(literal==null){
			throw new IllegalStateException('''[IS Edge] «literalId» Literal is unkonw in GUI «isEdge.rootElement.title»''')
		}
		return '''«literal.name»'''
		
	}
	
	def getIsList(IS isEdge)
	{
		var source = isEdge.sourceElement
		if(source instanceof ComplexVariable){
			return source.isIsList
		}
		if(source instanceof ComplexAttribute){
			return (source as ComplexAttribute).attribute.isIsList
		}
		if(source instanceof ComplexExtensionAttribute){
			return (source as ComplexExtensionAttribute).attribute.isIsList
		}
		return false;
	}
	
	def getInstanceof(IS isEdge,MovableContainer cmc)
	{
		var condition = DataDartHelper.getDataAccess(isEdge.getSourceElement(),DataAccessType.^IF,MODE.GET);
		if(isEdge.sourceElement.isEnum) {
			var literalName = isEdge.literalName;
			if(isEdge.isList) {
				condition += '''.where((n)=>n.dywa_name=='«literalName»').isNotEmpty'''
			} else {
				condition += '''?.dywa_name=='«literalName»' '''			
			}
			if(isEdge.negate){
				return "!("+condition+")";
			}
			return condition
		}
		
		
		
		condition += "";
		var className = isEdge.getClassName(false);
		if(isEdge.isList) {
			condition += "s";	
			className = "DIMEList<"+className+">"
		}
		if(isEdge.exactMatch) {
			condition += "?.runtimeType?.toString() == '" + className + "'"
			if(isEdge.negate){
				return "!("+condition+")";
			}
		}
		else{
			return "check"+isEdge.id.escapeDart+"Type("+condition+")";
		}
		return condition;
	}
	
	def boolean getIsEnum(Node data) {
		if(data instanceof ComplexVariable) {
			return data.dataType instanceof EnumType
		}
		if(data instanceof ComplexAttribute) {
			return data.attribute.dataType instanceof EnumType
		}
		if(data instanceof ComplexExtensionAttribute) {
			return data.attribute.complexDataType instanceof EnumType
		}
		false
	}
	
	def printIFCondition(MovableContainer cmc, Edge ifEdge) {
		getCondition(ifEdge, cmc)
	}
	
	/**
	 * Creates the condition string for a given conditional edge and the targeted component.
	 * The condition contains the variable access as well as the equation, which depends on the data type.
	 */
	def getCondition(Edge ifEdge,MovableContainer cmc)
	{
		var condition = "";
		if(ifEdge instanceof ^IF){
			if(ifEdge.isNegate)condition += "(";
		}
		condition += ifEdge.sourceElement.createDataBindingCondition(cmc)
		if(ifEdge instanceof ^IF){
			if(ifEdge.isNegate)condition += ")";
		}
		return condition
	}
	
	def createDataBindingCondition(Node data,MovableContainer cmc)
	{
		var condition = ""
		if(data instanceof Variable) {
			if((data as Variable).isList){
				var variable = DataDartHelper.getVariableDataName(data as Variable, cmc.rootElement,DataAccessType.^IF,MODE.GET_INIT);
				condition += variable+".length>0";
			}
			else{
				var variable = DataDartHelper.getVariableDataName(data as Variable, cmc.rootElement,DataAccessType.^IF,MODE.GET);
				if(data instanceof PrimitiveVariable){
					if((data as PrimitiveVariable).dataType == info.scce.dime.gui.gui.PrimitiveType.TEXT){
						condition += "checkText("+variable+")";	
					}
					else{
						condition += variable+getPrimitveCondition((data as PrimitiveVariable).dataType.toData);						
					}
				}
				else{
					condition +=  variable+"!=null";
				}
			}
		}
		else if(data instanceof Attribute){
			var variable = DataDartHelper.getAttributeDataName(data as Attribute, cmc.rootElement,DataAccessType.^IF,MODE.GET);
			if(data instanceof ComplexListAttribute){
				condition += variable+"!=null";
			}
			else if(data instanceof PrimitiveListAttribute){
				condition += variable+">0";
			}
			else if(data instanceof PrimitiveAttribute){
				var cpa = data as PrimitiveAttribute;
				var pa = (cpa as PrimitiveAttribute).attribute;
				if(pa.isIsList){
					condition += variable+"?.length>0";
				}
				else{
					if(pa.dataType==PrimitiveType.TEXT){
						condition += "checkText("+variable+")";
					}
					else{
						condition += variable+getPrimitveCondition(pa.dataType);
					}
				}
			}
			else if(data instanceof PrimitiveExtensionAttribute){
				var pa = data.attribute;
				if(pa.isIsList){
					condition += variable+"?.length>0";
				}
				else{
					if(pa.primitiveDataType==PrimitiveType.TEXT){
						condition += "checkText("+variable+")";
					}
					else{
						condition += variable+getPrimitveCondition(pa.primitiveDataType);
					}
				}
			}
			else if(data instanceof ComplexExtensionAttribute){
				var pa = data.attribute;
				if(pa.isIsList){
					condition += variable+"?.length>0";
				}
				else{
					condition += variable+"!=null";
				}
			}
			else{
				var cpa = data as ComplexAttribute;
				var pa = (cpa as ComplexAttribute).attribute;
				if(pa.isIsList){
					condition += variable+"?.length>0";
				}
				else{
					condition += variable+"!=null";
				}
			}
			
		}
		return condition
	}
	
	/**
	 * Determines the equation used for the condition of an NG-If directive
	 * depended on a primitive type.
	 */
	private def getPrimitveCondition(PrimitiveType pt){
		switch(pt){
			case BOOLEAN: return "==true"
			case FILE: return "!=null"
			case INTEGER: return "!=0"
			case REAL: return "!=0.0"
			case TEXT: return "!=null"
			case TIMESTAMP: return "!=null"
		}
	}
	
	/**
	 * Checks if an conditional edge is present on the given component
	 */
	private def isIF(MovableContainer cmc)
	{
		return !(cmc.incoming.filter[n|n instanceof ^IF].empty && cmc.incoming.filter[n|n instanceof IS].empty)
	}
	
	def List<Node> allIterationScopeVariables(MovableContainer cmc) {
		val iterationListVars = new LinkedList
		iterationListVars.addAll(cmc.getIncoming(Iteration).map[sourceElement].toList)
		iterationListVars.addAll(cmc.getIncoming(TableLoad).map[sourceElement].toList)
		if(cmc.container instanceof MovableContainer){
			iterationListVars.addAll(allIterationScopeVariables(cmc.container as MovableContainer))
		}
		iterationListVars
	}
	
	def List<Node> iteratorVars(MovableContainer cmc) {
		val listVars = cmc.allIterationScopeVariables
		val expandedIterators = listVars.filter[!getOutgoing(ComplexListAttributeConnector).empty]
		.map[getOutgoing(ComplexListAttributeConnector)].flatten
		.filter[attributeName==ComplexListAttributeName.CURRENT]
		.map[targetElement]
		val embeddedIterators = listVars.filter(Container).map[allNodes].flatten.filter(ComplexListAttribute).filter[attributeName==ComplexListAttributeName.CURRENT].filter(Node)
		return (expandedIterators+embeddedIterators).toList
	
	}
	
	def List<String> iteratorNames(MovableContainer cmc) {
		cmc.iteratorVars.map[node|{
			if(node instanceof ComplexListAttribute){
				return "current"
			}
			if(node instanceof Variable){
				return node.name
			}
		}]
	}
	
	
}
