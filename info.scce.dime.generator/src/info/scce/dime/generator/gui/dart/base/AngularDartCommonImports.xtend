/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.base

import info.scce.dime.generator.gui.dart.functionality.AngularDartCurrentUserService
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Variable

/**
 * Template for the creation of common imports, declarations and specifications.
 */
class AngularDartCommonImports extends GUIGenerator{
	
	/**
	 * The common import statements needed for every Angular Dart component.
	 */
	
	def createImports(boolean subModule,GUI gui,GUICompoundView gcv)
	'''
	import 'dart:async';
	import 'dart:convert';
	import 'dart:html' as html;

	import 'package:angular_router/angular_router.dart';
	import 'package:angular/angular.dart';
	import 'package:angular/security.dart';
	import 'package:angular_forms/angular_forms.dart';
	
	import 'package:app/src/app.dart';
	
	import 'package:app/src/core/dime_process_service.dart';
	//Notifications
	import 'package:app/src/notification/notification_component.dart';
	//Login
	import 'package:app/src/login/Login.dart' as login;
	import 'package:app/src/core/DIMEComponent.dart' as dime;
	
	//Data
	«FOR data : genctx.usedDatas»
	import 'package:app/src/data/«data.modelName.escapeDart».dart' as «DyWASelectiveDartGenerator.prefix(data)»;
	«ENDFOR»
	import 'package:app/src/models/FileReference.dart';
	import 'package:app/src/filesupport/fileuploader.dart';
	import 'package:app/src/filesupport/fileselect.dart';
	import 'package:app/src/models/Selectives.dart';
	import 'package:app/src/models/Todos.dart';
	//Directives
	import 'package:app/src/directives/MaxLength.dart';
	import 'package:app/src/directives/DimeCustomeAttributes.dart';
	
	//Import Services
	«IF gui != null»
		«IF this.getCurrentUser(gui) != null»
			«IF gcv.pairs.get(this.getCurrentUser(gui)) !== null»
			import 'package:app/src/services/«AngularDartCurrentUserService.getName(gcv.pairs.get(this.getCurrentUser(gui)) as ComplexTypeView)»Service.dart';
			«ENDIF»
		«ENDIF»
	«ENDIF»
	'''
	
	/**
	 * The common directive declarations needed for every Angular Dart component.
	 */
	def createDirectives()
	'''coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective'''
	
	/**
	 * The common constructor parameters needed for every Angular Dart component.
	 */
	def createConstructorParameters(GUI gui,GUICompoundView gcv)
	'''
	«IF gui != null»
		«IF this.getCurrentUser(gui) != null»
			«IF gcv.pairs.get(this.getCurrentUser(gui))!==null»
			,«getCurrentUserAsTypeView(gui,gcv)»Service this.«getCurrentUserAsTypeView(gui,gcv).toString.toFirstLower»Service
			«ENDIF»
		«ENDIF»
	«ENDIF»
	'''
	
	/**
	 * The common variable declaration needed for every Angular Dart component.
	 */
	def createDeclarations(GUI gui,GUICompoundView gcv)
	'''
	final Router router;
	final DIMEProcessService processService;
	final DomSanitizationService domSanitizationService; 
	«IF gui != null»
		«IF this.getCurrentUser(gui) != null»
			«IF gcv.pairs.get(this.getCurrentUser(gui)) !== null»
			«getCurrentUserAsTypeView(gui,gcv)»Service «getCurrentUserAsTypeView(gui,gcv).toString.toFirstLower»Service;
			«ENDIF»
		«ENDIF»
	«ENDIF»
	'''
	/**
	 * TODO: Check if helper methods can be refactored into a Helper class
	 * Check for other classes that use similar methods
	 */
	
	
	/**
	 * Helper method to collect the currentUser variable or null if none is present.
	 */
	def getCurrentUser(GUI gui)
	{
		return gui.find(Variable).findFirst[n|n.name.equals("currentUser")&&!n.isIsInput]
	}
	
	/**
	 * Helper method to collect the currentUser name for the given type view based on the currently active user
	 */
	def getCurrentUserAsTypeView(GUI gui, GUICompoundView gcv)
	{
		return AngularDartCurrentUserService.getName(gcv.pairs.get(this.getCurrentUser(gui))as ComplexTypeView)
	}
	
	/**
	 * Helper method which checks if currentUser variable is present.
	 */
	def isCurrentUserVariablePresent(GUI gui)
	{
		return getCurrentUser(gui)!=null;
	}
	
	/**
	 * The common Angular Pipe declaration needed for every Angular Dart component.
	 */
	def createPipes() {
		return '''commonPipes,SecondsPipe,AuthImagePipe'''
	}
	
	
}
