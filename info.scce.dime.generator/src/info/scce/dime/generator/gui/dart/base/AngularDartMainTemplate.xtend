/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.base

import info.scce.dime.generator.gui.utils.GUIGenerator

/**
 * Template to create the main.dart file. It has been extracted from the static Dart sources to decouple static and
 * and generated code. Some imports of the main.dart file reference generated files. Since those files aren't
 * present in the static sources, the analysis reports errors and fails.
 */
class AngularDartMainTemplate extends GUIGenerator {
		
	/**
	 * Creates the static Angular Dart main.dart file.
	 */
	def create() '''
	import 'package:angular/angular.dart';
	import 'package:angular_router/angular_router.dart';
	import 'package:app/src/app.template.dart' as ng;
	
	import 'main.template.dart' as self;
	
	@GenerateInjector(routerProviders)
	final InjectorFactory injector = self.injector$Injector;
	
	@GenerateInjector(routerProvidersHash)
	final InjectorFactory injectorLocal = self.injectorLocal$Injector;
	
	void main() {
	  final local = const bool.fromEnvironment('local', defaultValue: true);
	  print(local);
	  if (local) {
	    runApp(ng.AppComponentNgFactory, createInjector: injectorLocal);
	  } else {
	    runApp(ng.AppComponentNgFactory, createInjector: injector);
	  }
	}
	'''

}
