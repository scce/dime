/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.base

import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.ProcessEntryPointComponent
import info.scce.dime.dad.dad.RootInteractionPointer
import info.scce.dime.dad.dad.StartupProcessPointer
import info.scce.dime.dad.dad.URLProcess
import info.scce.dime.generator.gui.dart.functionality.FrontEndProcessExtension
import info.scce.dime.generator.gui.utils.GUIGenerator
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import static extension info.scce.dime.generator.gui.dart.functionality.AngularDartDADProcessTemplate.className
import static extension info.scce.dime.generator.gui.dart.functionality.AngularDartDADProcessTemplate.templateName

/**
 * Template to create main.dart Angular Component.
 * The root Angular Component "App" holds the top level rooting.
 * For each interaction process, a root is given to access the interaction process by an URL and GET parameters for
 * every input port.
 */
class AngularDartAppTemplate extends GUIGenerator {
	
	extension FrontEndProcessExtension = new FrontEndProcessExtension
	
	/**
	 * Creates the Angular Dart component code for the main Angular App.
	 */
	def create(DAD dad) {
		
		val routableSibs = dad.URLProcesss.filter[getIncoming(StartupProcessPointer).isEmpty]
		val routbaleProcesses = routableSibs.map[model].toSet
		val startURL = dad.URLProcesss.findFirst[!getIncoming(RootInteractionPointer).isEmpty]
		val timestamp = DateTimeFormatter
			.ofPattern("yyyy/MM/dd HH:mm:ss")
			.format(LocalDateTime.now)
		
		return '''
			/*
			 * Angular
			 */
			import 'package:angular/core.dart';
			import 'package:angular_router/angular_router.dart';
			
			import 'package:app/src/core/Helper.template.dart' as ng;
			import 'package:app/src/core/AbstractRoutes.dart';
			import 'package:app/src/core/dime_process_service.dart';
			import 'package:app/src/notification/notification_component.dart';
			import 'package:app/src/progress-bar/progress_bar_component.dart';
			import 'package:app/src/services/TableDndService.dart';
			import 'package:app/src/services/ProgressService.dart';
			import 'package:app/src/services/AuthService.dart';
			
			/*
			 * Routables
			 */
			«FOR p:routbaleProcesses»
				// Route to «p.modelName»
				import 'package:app/src/dad/«p.templateName»' as ng;
			«ENDFOR»
			
			@Component(
			    selector: 'app',
			    template: «"'''"»
			    	  <notification #notification></notification>
			    	  <progress-bar></progress-bar>
			    	  <router-outlet [routes]="Routes.all"></router-outlet>
			    «"'''"»,
			    directives: const [routerDirectives,NotificationComponent,ProgressBarComponent],
			    exports: [Routes],
			    providers: const [
			    	ClassProvider(DIMEProcessService),
			    	ClassProvider(NotificationService),
			    	ClassProvider(ProgressService), 
					ClassProvider(TableDndService),
					ClassProvider(AuthService),
			        ClassProvider(AbstractRoutes, useClass: Routes),
				]
			)
			class AppComponent implements OnInit {
				
				@ViewChild('notification')
				NotificationComponent notificationComponent;
				
			    final NotificationService _notificationService;
			    final ProgressService _progressService;
			    
			    AppComponent(this._notificationService, this._progressService){}
				
			    @override
				void ngOnInit() async {
			    	this._notificationService.component = notificationComponent;
			    	print("GENERATED AT «timestamp»");
				}
				
			}
			
			class Routes implements AbstractRoutes {
				
				«createRoute(startURL,"root")»
				
				«FOR p:routableSibs»
				  	«createRoute(p)»
			    «ENDFOR»
			    
				static final Error = RouteDefinition(
					routePath: RoutePath(path: 'error'),
					component: ng.ErrorFoundNgFactory,
				);
				
				static final Maintenance = RouteDefinition(
					routePath: RoutePath(path: 'maintenance'),
					component: ng.MaintenanceNgFactory,
				);
				
				static final Logout = RouteDefinition(
					routePath: RoutePath(path: 'logout'),
					component: ng.LogoutNgFactory,
				);
				
				static final NotFound = RouteDefinition(
					path: '.+',
					component: ng.NotFoundNgFactory,
				);
				
				static final all = <RouteDefinition>[
					root,
					RouteDefinition.redirect(
						path: '/',
						redirectTo: «startURL.routeName».toUrl()
					),
					«FOR p: routableSibs»
						«p.routeName»,
					«ENDFOR»
					Logout,
					Maintenance,
					NotFound
				];
				
				@override
				RouteDefinition getByName(String typeName) {
					switch(typeName) {
						«FOR p:routableSibs»
							case '«p.routeName»': return «p.routeName»;
						«ENDFOR»
						case 'root': return root;
						case 'Maintenance': return Maintenance;
						case 'Logout': return Logout;
						default: return NotFound;
					}
				}
				
			}
		'''
		
	}
	
	/**
	 * Helper method to create an Angular route definition for the given interaction process compound view.
	 * If the interaction process is guarded, the guard inputs has to passed as well to access the interaction process.
	 */
	private def createRoute(URLProcess pc) {
		pc.createRoute(null)
	}
	
	private def createRoute(URLProcess pc,String root) '''
		static final «if (root.nullOrEmpty) pc.routeName else root» = RouteDefinition(
			routePath: RoutePath(path: '«if (pc.url.nullOrEmpty) pc.model.modelName else pc.url»«pc.startRouteParameter.join("/", "/", null) [it]»'),
			component: ng.«pc.model.className»«if (pc instanceof ProcessEntryPointComponent) pc.entryPoint.id.escapeDart»NgFactory
		);
	'''
	
	static def routeName(URLProcess up) {
		'''Process«up.id.escapeDart»URL'''
	}
	
}
