/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EntryPointProcessSIB
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process

/**
 * Template for the root process Angular Dart component class file.
 * The root process component encapsulates every process reachable in a GUI or the DAD
 * The main task of the root process is the start REST call
 */
class AngularDartRootProcessTemplate extends GUIGenerator{
	
	extension FrontEndProcessExtension = new FrontEndProcessExtension
	extension RESTExtension = new RESTExtension
	
	/**
	 * Returns the file name of the routable Angular Dart Component class
	 * for the given interaction process
	 */
	static def String fileName(Process p)'''«p.className».dart'''
	static def String className(Process p)'''RootProcess«p.id.escapeDart»Component'''
	static def String classImport(Process p)'''import 'package:app/src/root/«p.fileName»';'''
	static def String classTag(Process p)'''root-«p.modelName.escapeDart»-«p.id.escapeDart»-process'''
	
	/**
	 * Generates the routable Angular Dart component class file
	 */
	def create(Process p)
	{
		val adpt = new AngularDartProcessTemplate()
	'''
	// routing wrapper for the «p.modelName» process
	import 'package:angular/angular.dart';	
	import 'package:angular_router/angular_router.dart';
	import 'dart:html';
	import 'dart:convert';
	import 'package:app/src/core/AbstractRoutes.dart';
	import 'package:app/src/core/dime_process_service.dart';
	import 'package:app/src/login/Login.dart' as login;
	import 'package:app/src/notification/notification_component.dart';
	
	//Data
	import 'package:app/src/models/FileReference.dart';
	import 'package:app/src/models/Selectives.dart';
	«FOR data : genctx.usedDatas»
	import 'package:app/src/data/«data.modelName.escapeDart».dart' as «DyWASelectiveDartGenerator.prefix(data)»;
	«ENDFOR»
	//Process
	«adpt.classImport(p)»
	//Deserializer
	import 'package:app/src/models/UserInteraction«p.simpleTypeNameDart»ResponseDeserializer.dart';
	
	@Component(
	  	selector: '«classTag(p)»',
	  	directives: const [coreDirectives,«adpt.className(p)»,login.Login],
		template: «"'''"»
		<login-form 
			*ngIf="showLogin"
			(signedin)="retry()"
			[modal]="false"
		></login-form>
		<«p.modelName.escapeDart»-«p.id.escapeDart»-process
			*ngIf="!showLogin&&runtime!=null"
			[runtimeId]="runtimeId"
			[parentRuntimeId]="parentRuntimeId"
			[majorSIB]="runtime.majorSIB"
			[minorSIB]="runtime.minorSIB"
			[majorInput]="runtime.majorInput"
			[minorInput]="runtime.minorInput"
			[deserializer]="deserializer"
		>
		</«p.modelName.escapeDart»-«p.id.escapeDart»-process>
		«"'''"»
	)
	class «p.className» extends RootProcess implements OnInit {
		
		«FOR port:p.startSIBs.get(0).outputPorts»
		@Input()
		«port.portParamType» «port.name.escapeDart»;
		«ENDFOR»
		«FOR entryPoint:p.entryPointProcessSIBs»
			«FOR port:entryPoint.inputPorts»
			@Input()
			«port.portParamType» «entryPoint.label.escapeDart»_«port.name.escapeDart»;
			«ENDFOR»
		«ENDFOR»
		
		@Input()
		String startPointId;
		
		@Input()
		String parentRuntimeId;
		
		final DIMEProcessService _processService;
		
		final Router _router;
		
		final UserInteraction«p.simpleTypeNameDart»ResponseDeserializer deserializer = new UserInteraction«p.simpleTypeNameDart»ResponseDeserializer();
		
		final NotificationService _notificationService;
		
		@override
		UserInteractionResponseDeserializer getDeserializer() => deserializer;
		
		«p.className»(this._processService,this._router,this._notificationService, AbstractRoutes routes): super(routes);
		
		void retry() {
			retryAfterLogin(_processService,"«p.simpleTypeName»");
		}
		
		@override
		ngOnInit() {
			
			if(startPointId==null||startPointId=='«p.startSIBs.get(0).id.escapeDart»') {
				Map<Object,dynamic> cache = new Map();
				_processService.startProcess(
					'«p.simpleTypeName»/public',
					deserializer,
					{
						«p.startSIBs.get(0).outputPorts.map[parseURLInput].join(",")»
					},
					parentRuntimeId:parentRuntimeId
				)
				.then((sr)=>processResponse(_processService,sr))
				.catchError((e)=>processError(e));
			
			}
			«FOR entryPoint:p.entryPointProcessSIBs»
				else if(startPointId=='«entryPoint.id.escapeDart»') {
					Map<Object,dynamic> cache = new Map();
					_processService.startProcess(
						'«p.simpleTypeName»/«entryPoint.id.escapeJava»/public',
						deserializer,
						{
							«entryPoint.inputPorts.map[parseURLInput(entryPoint)].join(",")»
							
						},
						parentRuntimeId:parentRuntimeId
					)
					.then((sr)=>processResponse(_processService,sr))
					.catchError((e)=>processError(e));
				}
			«ENDFOR»
			 else {
				print("Cannot start process «p.modelName» with start point id ${startPointId}");
			}
		}
		
		@override
		Map<String,ActiveProcess> getActiveProcesses() => _processService.activeProcesses;
		
		@override
		String getParentRuntimeId() => parentRuntimeId;
		
		@override
		String getGUIId() => null;
		
		@override
		Router getRouter() => _router;
		
		@override
		NotificationService getNotificationService() => _notificationService;
	}
	
	@Component(
		  	selector: '«classTag(p)»',
		  	directives: const [coreDirectives,«adpt.className(p)»,login.Login],
			template: «"'''"»
			<login-form 
				*ngIf="showLogin"
				(signedin)="retry()"
				[modal]="false"
			></login-form>
			<«p.modelName.escapeDart»-«p.id.escapeDart»-process
				*ngIf="!showLogin&&runtime!=null"
				[runtimeId]="runtimeId"
				[parentRuntimeId]="parentRuntimeId"
				[majorSIB]="runtime.majorSIB"
				[minorSIB]="runtime.minorSIB"
				[majorInput]="runtime.majorInput"
				[minorInput]="runtime.minorInput"
				[guiId]="guiId"
				[sibId]="sibId"
				[deserializer]="deserializer"
			>
			</«p.modelName.escapeDart»-«p.id.escapeDart»-process>
			«"'''"»
		)
		class «p.className»SIB extends RootProcess implements AfterChanges {
			
			
			@Input()
			String startPointId;
			
			@Input()
			String sibId;
			
			@Input()
			String guiId;
			
			@Input()
			String parentRuntimeId;
			
			«FOR port:p.startSIBs.get(0).outputPorts»
			@Input()
			«port.getInputPortType(true)» «port.name.escapeDart»;
			«ENDFOR»
			
			final DIMEProcessService _processService;
			
			final NotificationService _notificationService;
			
			final Router _router;
			
			final UserInteraction«p.simpleTypeNameDart»ResponseDeserializer deserializer = new UserInteraction«p.simpleTypeNameDart»ResponseDeserializer();
			
			@override
			UserInteractionResponseDeserializer getDeserializer() => deserializer;
			
			«p.className»SIB(this._processService,this._router,this._notificationService, AbstractRoutes routes): super(routes);
			
			void retry() {
				retryAfterLogin(_processService,"«p.simpleTypeName»",sibId:sibId);
			}
			
			@override
			ngAfterChanges() {
				restart();
			}
			
			
			void restart() {
				if(startPointId==null||startPointId=='«p.startSIBs.get(0).id.escapeDart»') {
					Map<Object,dynamic> cache = new Map();
					_processService.startProcess(
						"«p.simpleTypeName»/${sibId}/public",
						deserializer,
						{
							«p.startSIBs.get(0).outputPorts.map[parseSIBInput].join(",")»
						},
						parentRuntimeId:parentRuntimeId,
						guiId:guiId
					)
					.then((sr)=>processResponse(_processService,sr))
					.catchError((e)=>processError(e));
				
				} else {
					print("Cannot start process «p.modelName» with start point id ${startPointId}");
				}
			}
			
			@override
			Map<String,ActiveProcess> getActiveProcesses() => _processService.activeProcesses;
			
			@override
			String getParentRuntimeId() => parentRuntimeId;
			
			@override
			String getGUIId() => guiId;
			
			@override
			Router getRouter() => _router;
			
			@override
			NotificationService getNotificationService() => _notificationService;
		}
	
	'''
	}
	
	def isEncoding(OutputPort port,PrimitiveType pt) {
		if(port instanceof PrimitiveOutputPort) {
			return port.dataType == pt
		}
		false
	}
		
	def getInputPortType(OutputPort port,boolean considerList)
	'''«IF port.primitve»
			«port.portParamType»
		«ELSE»
			«val output = port as ComplexOutputPort»
			«IF port.isIsList && considerList»DIMEList<«ENDIF»«DyWASelectiveDartGenerator.prefix(output.dataType)».«output.dataType.name.escapeDart»«IF port.isIsList && considerList»>«ENDIF»
		«ENDIF»'''
	
	def getInputPortType(InputPort port,boolean considerList)
	'''«IF port.primitve»
			«port.portParamType»
		«ELSE»
			«val output = port as ComplexInputPort»
			«IF port.isIsList && considerList»DIMEList<«ENDIF»«DyWASelectiveDartGenerator.prefix(output.dataType)».«output.dataType.name.escapeDart»«IF port.isIsList && considerList»>«ENDIF»
		«ENDIF»'''
		
	def parseInput(OutputPort port)
	''''«port.name.escapeString»' : «port.name.escapeDart»«IF !port.isPrimitve»«IF port.isIsList».map((n)=>n«ENDIF».toJSOG(cache)«IF port.isIsList»).toList()«ENDIF»«ENDIF»'''
		
	def parseURLInput(InputPort port,EntryPointProcessSIB entryPoint)
	''''«port.name.escapeString»' : «IF port instanceof ComplexInputPort»«complexParseInput(port.getInputPortType(false),'''«entryPoint.label.escapeDart»_«port.name.escapeDart»''',port.isIsList,true)»«ELSE»«primitiveParseInput('''«entryPoint.label.escapeDart»_«port.name.escapeDart»''',port.isIsList,(port as PrimitiveInputPort).dataType)»«ENDIF»'''
	
	def parseURLInput(OutputPort port) {
		port.parseInput(true)
	}
	
	
	def parseSIBInput(OutputPort port) 
		''''«port.name.escapeString»' : «IF port instanceof ComplexOutputPort»«complexParseSIBInput(port.getInputPortType(false),'''«port.name.escapeDart»''',port.isIsList)»«ELSE»«primitiveParseInput('''«port.name.escapeDart»''',port.isIsList,(port as PrimitiveOutputPort).dataType)»«ENDIF»'''
		
	
	def parseInput(OutputPort port,boolean fromId)
	''''«port.name.escapeString»' : «IF port instanceof ComplexOutputPort»«complexParseInput(port.getInputPortType(false),'''«port.name.escapeDart»''',port.isIsList,fromId)»«ELSE»«primitiveParseInput('''«port.name.escapeDart»''',port.isIsList,(port as PrimitiveOutputPort).dataType)»«ENDIF»'''
	
	
	
	def complexParseSIBInput(CharSequence type,CharSequence name,boolean isList) {
		if(isList) {
			return '''«name».map((n)=>n.toJSOG(cache)).toList()'''
		}
		'''«name»==null?null:«name».toJSOG(cache)'''
	}
	
	def complexParseInput(CharSequence type,CharSequence name,boolean isList,boolean fromId) {
		if(isList) {
			return '''«name».map((n)=>«IF fromId»«type».fromId(«ENDIF»n«IF fromId»).toJSOG(cache)«ENDIF»).toList()'''
		}
		'''«name»==null?null:«IF fromId»«type».fromId(«ENDIF»«name»«IF fromId»).toJSOG(cache)«ENDIF»'''
	}
	
	
	def primitiveParseInput(String name,boolean isList,PrimitiveType pt)
	'''«IF isList»«name».map((n)=>«"n".primitiveEncoding(pt)»).toList()«ELSE»«primitiveEncoding(name,pt)»«ENDIF»'''
	
	def primitiveEncoding(String varName,PrimitiveType pt)
	'''«IF pt==PrimitiveType.FILE»«varName»==null?null:«varName».toJSOG()«ELSEIF pt==PrimitiveType.TIMESTAMP»«varName»==null?null:DateConverter.toJSON(«varName»)«ELSE»«varName»«ENDIF»'''
	
}
