/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import info.scce.dime.generator.gui.dart.base.AngularDartCommonImports
import info.scce.dime.generator.gui.dart.functionality.AngularAttributeTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartEventTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Checkbox
import info.scce.dime.gui.gui.ChoiceData
import info.scce.dime.gui.gui.Combobox
import info.scce.dime.gui.gui.DataBinding
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.FormLoadSubmit
import info.scce.dime.gui.gui.FormSubmit
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Input
import info.scce.dime.gui.gui.InputType
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Radio
import info.scce.dime.gui.gui.Select
import info.scce.dime.gui.gui.Variable

/**
 * Template to create Angular Dart component class files for from components.
 */
class AngularDartFormTemplate extends GUIGenerator {
	
	/**
	 * Helper method to get the Angular Dart component class file name.
	 */
	def getFormName(Form form,GUI g)'''Form«ConventionHelper.cincoID(form)»«g.title.escapeDart.toFirstUpper»'''
	
	/**
	 * Generates the Angular Dart component class code for the given from component placed in the given GUI model.
	 */
	def create(Form form,GUI g,GUICompoundView gcv)
	'''
		import 'dart:js' as js;
		import 'package:app/src/core/AbstractRoutes.dart';
		«new AngularDartCommonImports().createImports(true,g,gcv)»
		
		«new AngularCommonComponentTemplate().imports(form,g,gcv)»
		import 'package:app/src/core/Validators.dart' as validators;
		import 'package:app/src/services/AuthService.dart';
		
		// Form placed in GUI model «g.title»
		@Component(
		  selector: 'form-«ConventionHelper.cincoID(form)»-«g.title.escapeDart.toFirstLower»',
		  viewProviders: const [FORM_BINDINGS],
		  «new AngularCommonComponentTemplate().otherSpecifications(form,g)»
		  templateUrl: '«getFormName(form,g)».html'
		)
		class «getFormName(form,g)» extends dime.DIMEComponent implements OnInit, AfterViewChecked {
		  	«new AngularCommonComponentTemplate().declaration(form,g,gcv)»
		  	
		  	«IF!form.getIncoming(DataBinding).empty»
		  	//Form load variable
		  		«IF form.getIncoming(DataBinding).get(0).sourceElement instanceof Variable»
		  		@Input()
		  		«DataDartHelper.getVariableDataType(form.getIncoming(DataBinding).get(0).sourceElement as Variable,true,gcv, _selectiveExtension)» formLoad;
				«ELSEIF form.getIncoming(DataBinding).get(0).sourceElement instanceof Attribute»
		  		@Input()
		  		«DataDartHelper.getAttributeDataType(form.getIncoming(DataBinding).get(0).sourceElement as Attribute,gcv, _selectiveExtension)» formLoad;
				«ENDIF»
		  	«ENDIF»
		  	//Global Scope
		  	«new AngularAttributeTemplate().createAttributeDeclaration(g,gcv,true)»
		  	//Extra input variables
		  	«FOR node : form.inputsVariables»
				«IF node instanceof Variable»
			  		@Input() «DataDartHelper.getVariableDataType(node,true,gcv, _selectiveExtension)» «DataDartHelper.getComponentInputName(node)»;
			  	«ELSEIF node instanceof Attribute»
			  		@Input() «DataDartHelper.getAttributeDataType(node as Attribute,gcv, _selectiveExtension)» «DataDartHelper.getComponentInputName(node)»;
			  	«ENDIF»
		  	«ENDFOR»
		  	
		  	final AuthService authService;
			
			//FORM
			ControlGroup «ConventionHelper.getFormName(form)»;
			bool formLoaded = false;
			//Form Inputs:
			«FOR Input input : form.formInputs»
				«IF input instanceof Field»
					// input field: «input.label»
					«IF input.inputType == InputType.ADVANCED_FILE || input.inputType == InputType.SIMPLE_FILE»
						FileUploader uploader«ConventionHelper.cincoID(input)» = new FileUploader({'url': '${DIMEProcessService.getBaseUrl()}/rest/files/create'}«IF input.maxMB>0»,maxMB:«input.maxMB»«ENDIF»«IF !input.accept.nullOrEmpty»,accept:'«input.accept»'«ENDIF»«IF input.inputType == InputType.SIMPLE_FILE»,autoUpload:true«ENDIF»);
					«ENDIF»
					«IF input.isDateTime»
						String «ConventionHelper.getFormInputName(input)»;
«««						String «ConventionHelper.getFormInputName(input)»_date;
					«ELSE»
						«val dataTypeName = DataDartHelper.getBindedDataType(input,true,gcv,DataBinding, _selectiveExtension)»
						«IF dataTypeName !== null»
							«dataTypeName» «ConventionHelper.getFormInputName(input)»«IF input.inputType==InputType.CHECKBOX» = false«ENDIF»;
						«ENDIF»
					«ENDIF»
				«ELSE»
					«DataDartHelper.getBindedDataType(input,true,gcv,DataBinding, _selectiveExtension)» «ConventionHelper.getFormInputName(input)»;
				«ENDIF»
				«IF input instanceof Select»
					«IF !input.getIncoming(DataBinding).empty»
						//Select «input.label» Choice values
						«DataDartHelper.getChoicesDataType(input,gcv, _selectiveExtension)» «ConventionHelper.getFormInputName(input)»Iteratable;
			  		«ENDIF»
			  	«ENDIF»
			«ENDFOR»
		
			«FOR primitiveVar:g.dataContexts.map[primitiveVariables].flatten.filter[!isIsList]»
			@Output('primitive_«primitiveVar.name.escapeDart»_update') Stream<dynamic> get primitive_«primitiveVar.name.escapeDart»_update => _primitive_«primitiveVar.name.escapeDart»_update.stream;
			StreamController<dynamic> _primitive_«primitiveVar.name.escapeDart»_update = new StreamController();
			«ENDFOR»
			
			«FOR complexVar:g.dataContexts.map[complexVariables].flatten.filter[complexVariablePredecessors.empty].filter[!isIsList]»
			@Output('complex_«complexVar.name.escapeDart»_update') Stream<dynamic> get complex_«complexVar.name.escapeDart»_update => _complex_«complexVar.name.escapeDart»_update.stream;
			StreamController<dynamic> _complex_«complexVar.name.escapeDart»_update = new StreamController();
			«ENDFOR»
		
		  	«getFormName(form,g)»(DIMEProcessService this.processService, this.authService, Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes«new AngularDartCommonImports().createConstructorParameters(g,gcv)») : super(domSanitizationService,processService,routes)
			{
				restartComponent();
			}
			
			void restartComponent() {
				
				«new AngularCommonComponentTemplate().constructor(form)»
				«new AngularCommonComponentTemplate().restart(form,g,gcv)»
			}
			
			«FOR primitiveVar:g.dataContexts.map[primitiveVariables].flatten.filter[!isIsList]»
			«primitiveVar.dataType.toData.primitiveType» propagate«primitiveVar.name.escapeDart»Primitives(«primitiveVar.dataType.toData.primitiveType» value) {
				_primitive_«primitiveVar.name.escapeDart»_update.add(value);
				return value;
			}
			«ENDFOR»
			
			«new AngularDartEventTemplate().createUpdateMethod(g,form,gcv,'''
				loadFormFieldValues();

			''')»
			
			void loadFormFieldValues() {
				«FOR Field input : form.formInputs.filter(Field).filter[!it.getIncoming(FormLoadSubmit).isEmpty]»
«««					if(this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)»!=null){
						«IF input.isDateTime»
							this.«ConventionHelper.getFormInputName(input)» = DateConverter.toFORM«input.inputType.dateParser»(this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)»);
						«ELSE»
							«IF !input.isFile»
							this.«ConventionHelper.getFormInputName(input)» = this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)»;
							«ENDIF»
						«ENDIF»
						«IF !input.isFile»
						if(«ConventionHelper.getFormName(form)»!=null) {
							(«ConventionHelper.getFormName(form)».controls['«ConventionHelper.getFormInputName(input)»'] as Control).updateValue(this.«ConventionHelper.getFormInputName(input)»);
						}
						«ENDIF»
«««					}
				«ENDFOR»
				
				«FOR Select input : form.formInputs.filter(Select).filter[n| DataDartHelper.isEdgePresent(n,ChoiceData)]»
						//Select Choice values
						this.«ConventionHelper.getFormInputName(input)»Iteratable = this.«DataDartHelper.getChoicesDataName(input,MODE.GET)»;
						«IF input.isSingleSubmittingOrRequired»
							if(this.«DataDartHelper.getChoicesDataName(input,MODE.GET)».length > 0) {
								«IF input.getIncoming(FormSubmit).empty || input.getIncoming(FormLoadSubmit).empty»
									«IF input.multiple»
								if(this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET_INIT,DataBinding)».length <= 0){
									this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT,DataBinding)»(this.«ConventionHelper.getFormInputName(input)»Iteratable);	
									«ELSE»
								if(this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET,DataBinding)»==null){
									this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT,DataBinding)»(this.«ConventionHelper.getFormInputName(input)»Iteratable[0]);	
									«ENDIF»
								}				
								«ELSE»
									«IF input.multiple»
									this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT,DataBinding)»(this.«ConventionHelper.getFormInputName(input)»Iteratable);
									«ELSE»
									this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT,DataBinding)»(this.«ConventionHelper.getFormInputName(input)»Iteratable[0]);
									«ENDIF»
								«ENDIF»
							}
						«ENDIF»
				«ENDFOR»
			}
			
			void ngAfterViewChecked() {
				if(html.querySelector("#form${this.«ConventionHelper.getFormName(form)».hashCode}")!=null&&!formLoaded) {
					formLoaded = true;
				«FOR field : form.formInputs.filter(Field).filter[isDateTime]»
					var c«field.id.escapeDart» = «ConventionHelper.getFormName(form)».controls['«ConventionHelper.getFormInputName(field)»'].hashCode;
					js.context.callMethod('showPicker',[«field.inputType.pickerParameter»,cb_«field.id.escapeDart»,'[data-date-field="${c«field.id.escapeDart»}"]']);
				«ENDFOR»
				}
			}
			
			«FOR field : form.formInputs.filter(Field).filter[isDateTime]»
			void cb_«field.id.escapeDart»(String d) {
				«IF isSubmitButtonPresent(form)»
					if(d != null) {
						try {
							var dt = DateConverter.toFORM«field.inputType.dateParser»(DateTime.parse(d));
							this.«ConventionHelper.getFormInputName(field)» = dt;
							(«ConventionHelper.getFormName(form)».controls['«ConventionHelper.getFormInputName(field)»'] as Control).updateValue(dt);
						} catch(e) {}
					}
					
				«ELSE»
					«val dataBinding = field.getIncoming(DataBinding).get(0).sourceElement»
					«IF !field.disabled»
						«IF dataBinding instanceof PrimitiveVariable»
							«DataDartHelper.getDataAccess(dataBinding,DataAccessType.^FOR,MODE.SET_INIT)»(propagate«(dataBinding as PrimitiveVariable).name.escapeDart»Primitives(DateTime.parse(d)));
						«ELSE»
							«DataDartHelper.getDataAccess(dataBinding,DataAccessType.^FOR,MODE.SET_INIT)»(DateTime.parse(d));
						«ENDIF»
					«ENDIF»
				«ENDIF»
			}
			«ENDFOR»
			
			/// called after the input variables are loaded, but before the template rendering
			void ngOnInit()
			{
				«new AngularCommonComponentTemplate().onInit(form)»
				
				
				
				//FORM Field grouping and vaalidation
				this.«ConventionHelper.getFormName(form)» = FormBuilder.controlGroup({
				«FOR Input input : form.formInputs SEPARATOR ","»
					«IF input instanceof Select»
						"«ConventionHelper.getFormInputName(input)»": [«IF DataDartHelper.isEdgePresent(input,ChoiceData)»this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET,ChoiceData)»«ELSE»""«ENDIF»,«createValidators(input)»]
					«ELSE»
					«val field = input as Field»
						"«ConventionHelper.getFormInputName(input)»": [
							«IF DataDartHelper.isEdgePresent(input,FormLoadSubmit)»
								«IF field.inputType == InputType.SIMPLE_FILE || field.inputType == InputType.ADVANCED_FILE»
								""
								«ELSEIF field.inputType == InputType.TEXT_AREA»
									this.getTextarea«ConventionHelper.cincoID(input)»Change()
								«ELSE»
									«IF field.isDateTime»
									DateConverter.toFORM«field.inputType.dateParser»(this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)»)
									«ELSE»
										this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)»
									«ENDIF»
								«ENDIF»
							«ELSE»
							""
							«ENDIF»,
							«createValidators(input)»
							]
					«ENDIF»
				«ENDFOR»
				});
				
				//FORM Field loading
				this.loadFormFieldValues();
			}
		  
		  	«new AngularCommonComponentTemplate().methods(form,g,gcv)»
			«createFormMethods(form)»
		}
	'''
		
	def dateParser(InputType type) {
		return switch(type) {
			case DATE:'''Date'''
			case MONTH:'''Month'''
			case TIME:'''Time'''
			case WEEK: '''Date'''
			default: ""
		}
	}
		
	def getPickerParameter(InputType type) {
		return switch(type) {
			case DATE:'''"month","month","yyyy-mm-dd"'''
			case DATETIME:'''"month","hour","yyyy-mm-dd hh:ii"'''
			case MONTH:'''"year","year","yyyy-mm"'''
			case TIME:'''"day","hour","hh:ii"'''
			case WEEK: '''"month","month","yyyy-mm-dd"'''
			default: ""
		}
	}
	
	
	
	
	/**
	 * Helper method to determine if at least one entry of a given select form field has to be initially selected.
	 * This is necessary if the select form field is required or if it is not multiple.
	 */
	private def boolean getIsSingleSubmittingOrRequired(Select select)
	{
		if(select.validation != null){
			if(select.validation?.required)return true;
		}
		//return !select.multiple && ((select.getIncoming(FormSubmit).empty) || select.getIncoming(FormLoadSubmit).empty);
		return false;
	
	}
	
	/**
	 * Generates all methods for the given form component.
	 * Includes validation and input receiving and parsing methods placed
	 * in the Angular component class file.
	 */
	private def createFormMethods(Form form)
	'''	
	// Triggered on Form Submit
	void «ConventionHelper.getFormName(form)»Submit(Map formValues) {
	// Store Form Data in Attributes
	«FOR Field input : form.formInputs.filter(Field).filter[!isDisabled]»
		// input field «input.label» with type: «input.inputType.literal»
		«IF input.inputType!=InputType.ADVANCED_FILE || input.inputType != InputType.SIMPLE_FILE»
				if(this.«ConventionHelper.getFormInputName(input)»!=null){
					if(this.«ConventionHelper.getFormInputName(input)».toString().isNotEmpty){
						this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT)»(
						«IF input.isDateTime»
						DateConverter.fromForm(«ConventionHelper.getFormInputName(input)»)
«««						«IF input.inputType==InputType.MONTH»
«««							DateTime.parse("${«ConventionHelper.getFormInputName(input)».toString()}")
«««						«ELSEIF input.inputType==InputType.WEEK»
«««							DateTime.parse("${«ConventionHelper.getFormInputName(input)».toString()}")
««««««							.add(
««««««								new Duration(days: int.parse(«ConventionHelper.getFormInputName(input)».toString().split("W")[1])*7 )
««««««							)
«««						«ELSEIF input.inputType==InputType.DATE»
«««							DateTime.parse(«ConventionHelper.getFormInputName(input)»)
«««						«ELSEIF input.inputType==InputType.DATETIME»
«««							DateTime.parse(«ConventionHelper.getFormInputName(input)»)
«««						«ELSEIF input.inputType==InputType.TIME»
«««							DateTime.parse(«ConventionHelper.getFormInputName(input)»)
««««««							new DateTime(new DateTime.now().year).add(
««««««									new Duration(
««««««											hours: int.parse(«ConventionHelper.getFormInputName(input)».toString().split(":")[0]),
««««««											minutes: int.parse(«ConventionHelper.getFormInputName(input)».toString().split(":")[1])
««««««									)
««««««							)
						«ELSE»
							«ConventionHelper.getFormInputName(input)»
						«ENDIF»
						);
						«input.propagatePrimtiveVaribale»
					} else {
						this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT)»(«DataDartHelper.getBindedDataNode(input).toPrimitve.defaultValue(false)»);
						«input.propagatePrimtiveVaribale»
					}
				} else {
					this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT)»(«DataDartHelper.getBindedDataNode(input).toPrimitve.defaultValue(false)»);
					«input.propagatePrimtiveVaribale»
				}
		«ENDIF»
		«IF input.inputType==InputType.ADVANCED_FILE || input.inputType == InputType.SIMPLE_FILE»
			if(this.uploader«ConventionHelper.cincoID(input)».getFileReferences().length>0){
				this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT)»(this.uploader«ConventionHelper.cincoID(input)».getFileReferences().last);
				«input.propagatePrimtiveVaribale»
			}
		«ENDIF»
	«ENDFOR»
	}
		
	«FOR Field input : form.formInputs.filter(Field)»
			«IF input.inputType==InputType.ADVANCED_FILE || input.inputType == InputType.SIMPLE_FILE»
				/// file upload field «input.label» has changed callback
				void setFileUpload«ConventionHelper.cincoID(input)»Change(dynamic event)
				{
					this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT)»(event.target.files.last);
					«input.propagatePrimtiveVaribale»
				}
			«ELSEIF input.inputType == InputType.NUMBER»
				static Map<String,dynamic> value«input.id.escapeDart»Validator(AbstractControl control)
				{
					if(control.value != null) {
						if(control.value.toString().isNotEmpty) {
							RegExp regexp = new RegExp('^-*[0-9,\.]+');
							var test = regexp.hasMatch("${control.value.toString()}");
							if (!test){
								return {'value«input.id.escapeDart»Validator': true};
							}
							if(!(num.parse(control.value.toString())>=«input.validation.min» && num.parse(control.value.toString())<=«input.validation.max»)){
								return {'value«input.id.escapeDart»Validator': true};
							}
						}
					}
					return null;
				}
			«ELSEIF input.inputType == InputType.TEXT_AREA»
				/// textarea field «input.label» has changed callback
				void setTextarea«ConventionHelper.cincoID(input)»Change(dynamic event) {
					«IF form.isSubmitButtonPresent»
					«ConventionHelper.getFormInputName(input)» = event.target.value;
					«ELSE»
					this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.SET_INIT)»(event.target.value);
					«input.propagatePrimtiveVaribale»
					«ENDIF»
				}
				
				/// textarea field «input.label» getter
				///
				/// parses the value with nl2br if the input is list
				String getTextarea«ConventionHelper.cincoID(input)»Change() {
					if(this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)» == null)return "";
					return this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)»;
				}
			«ENDIF»
	«ENDFOR»
	
	«FOR Select input : form.formInputs.filter(Select)»
		// methods for the select form field «input.label»
«««		«IF SelectDartHelper.isComplexSelect(input)»
			// dynamic data source for the select from field
			
			/// checks if a given complex selection entry is selected
			bool is«ConventionHelper.cincoID(input)»Selected(dynamic element)
			{
				if(this.«input.bindTargetGET» == null)return false;
			«IF (input).multiple»
				for (int index = 0; index < this.«input.bindTargetGET».length; index++) {
					if(this.«input.bindTargetGET»[index] == element)return true;
				}		
				return false;
			«ELSE»
				return element==this.«input.bindTargetGET»;
			«ENDIF»
			}
			
			/// callback to update the variables binded to the selection form component
			void submitSelect«ConventionHelper.cincoID(input)»Box(dynamic event) {
   			«IF (input).multiple»
   				«IF input instanceof Checkbox»
					// checkbox
					int index = int.parse(event.target.value);
					if(index>=0) {
						if (this.«input.bindTargetGETINIT».contains(this.«input.bindSourceGETINIT»[index])) {
							this.«input.bindTargetGETINIT».remove(this.«input.bindSourceGETINIT»[index]);
						}
						else {
							this.«input.bindTargetGETINIT».add(this.«input.bindSourceGETINIT»[index]);
						}
					}
					else{
						this.«input.bindTargetGETINIT».clear();
					}
				«ELSE» 
					// combobox
					this.«input.bindTargetGETINIT».clear();
					for (int index = 0; index < event.target.selectedOptions.length; index++) {
						int numIndex = int.parse(event.target.selectedOptions[index].value);
						if(numIndex>=0){
					    		this.«input.bindTargetGETINIT».add(this.«input.bindSourceGETINIT»[numIndex]);
					    }
					}
    				«ENDIF»
			«ELSE»
				«IF  input instanceof Radio»
					// radio
					int index = int.parse(event.target.value);
				«ELSE»
					// combobox
					int index = int.parse(event.target.selectedOptions[0].value);
    				«ENDIF»
				if(index < 0) {
					this.«input.bindTargetSETINIT»(null);
					«input.propagatePrimtiveVaribale»
				}
				else {
					this.«input.bindTargetSETINIT»(this.«input.bindSourceGETINIT»[index]);
					«input.propagatePrimtiveVaribale»
				}
			«ENDIF»
			}
«««		«ELSE»
«««			// static source for the select form field
«««			
«««			«IF input instanceof Checkbox»
«««				/// checkbox changed callback to update data target
«««				void checkBox«ConventionHelper.cincoID(input)»Clicked(dynamic value) {
«««					if (this.«input.bindTargetGETINIT».contains(value)) {
«««						this.«input.bindTargetGETINIT».remove(value);
«««					}
«««					else {
«««						this.«input.bindTargetGETINIT».add(value);
«««					}
«««				}
«««			«ELSEIF input instanceof Combobox && input.multiple»
«««				/// combobox changed callback to update data target
«««				void submitSelect«ConventionHelper.cincoID(input)»Box(dynamic event) {
«««				    this.«input.bindTargetGETINIT».clear();
«««				    for (int index = 0; index < event.target.selectedOptions.length; index++) {
«««				        this.«input.bindTargetGETINIT».add(event.target.selectedOptions[index].value);
«««				    }
«««				}
«««			«ELSEIF input instanceof Radio»
«««				/// radio changed callback to update data target
«««				void submitSelect«ConventionHelper.cincoID(input)»Box(dynamic value) {
«««				    this.«input.bindTargetSETINIT»(value);
«««				}
«««			«ENDIF»
«««		«ENDIF»
	«ENDFOR»
	'''
	
	/**
	 * Helper method to check if a given select form field accepts multiple selected entries.
	 */
	private def boolean multiple(Select select){
		if(select instanceof Checkbox)return true;
		if(select instanceof Combobox){
			return (select as Combobox).isMultiple;
		}
		return false;
	}
	
	def PrimitiveVariable getPrimitiveVariableBinding(Input input) {
		input.getIncoming(DataBinding).map[sourceElement].filter(PrimitiveVariable).findFirst[true]
	}
	
	def propagatePrimtiveVaribale(Input input) {
		val pv = input.primitiveVariableBinding
		'''
		«IF pv != null»
		«IF !pv.isIsList»
		propagate«pv.name.escapeDart»Primitives(this.«DataDartHelper.getBindedDataName(input,DataAccessType.FORM,MODE.GET)»);
		«ENDIF»
		«ENDIF»
		'''
	}
	
	/**
	 * Helper method to get getInit target data binding variable or attribute access.
	 */
	def CharSequence bindTargetGETINIT(Select select)
	'''«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET_INIT,DataBinding)»'''
	
	/**
	 * Helper method to get getInit source choice data variable or attribute access.
	 */
	def CharSequence bindSourceGETINIT(Select select)
	'''«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET_INIT,ChoiceData)»'''
	
	/**
	 * Helper method to get GET source choice data variable or attribute access.
	 */
	def CharSequence bindSourceGET(Select select)
	'''«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET,ChoiceData)»'''
	
	/**
	 * Helper method to get GET target data binding variable or attribute access.
	 */
	def CharSequence bindTargetGET(Select select)
	'''«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET,DataBinding)»'''
	
	/**
	 * Helper method to get SET INIT source choice data variable or attribute access.
	 */
	def CharSequence bindSourceSETINIT(Select select)
	'''«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.SET_INIT,ChoiceData)»'''
	
	/**
	 * Helper method to get SET INIT target data binding variable or attribute access.
	 */
	def CharSequence bindTargetSETINIT(Select select)
	'''«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.SET_INIT,DataBinding)»'''

//	/**
//	 * Generates a regex validation method
//	 */
//	def createRegex(String regex,String name)
//	'''
//	static «name»(Control control)
//	{
//		if(control.value != null) {
//			if(control.value.toString().isNotEmpty) {
//				RegExp regexp = new RegExp('«regex»');
//				var test = regexp.hasMatch("${control.value.toString()}");
//				if (!test){
//					return {'«name»': true};
//				}
//			}
//		}
//		return null;
//	}
//	'''
	
	/**
	 * Returns the import statement to import the Angular Dart component for the given form component
	 */
	def createImports(Form form, GUI gui)
	'''
	import 'package:app/src/forms/«gui.title.escapeDart.toFirstUpper»/«getFormName(form,gui)».dart' as «form.id.escapeDart»;
	'''
	
	/**
	 * Generates the validator declaration for a given input form field
	 */
	private def createValidators(Input input)
	'''
	Validators.compose([
		«IF input instanceof Field»
			«IF input.validation != null»
				«IF input.inputType!=InputType.ADVANCED_FILE && input.inputType != InputType.SIMPLE_FILE»
					«IF input.validation.isRequired»Validators.required,«ENDIF»
					«IF !input.validation.regex.nullOrEmpty»Validators.pattern(r'«input.validation.regex»'),«ENDIF»
						«IF input.inputType==InputType.NUMBER»
							«IF input.validation.min<=input.validation.max»
								value«input.id.escapeDart»Validator,
							«ENDIF»
						«ELSE»
							«IF input.validation?.min>0»Validators.minLength(«input.validation?.min»),«ENDIF»
							«IF input.validation?.max >-1»Validators.maxLength(«input.validation?.max»),«ENDIF»
						«ENDIF»
					«IF isValidatableType(input)»validators.DIMEValidators.type«input.inputType.literal»Validator«ENDIF»
				«ENDIF»
			«ENDIF»
		«ELSE»
			«IF input.validation?.isRequired»Validators.required,«ENDIF»
			«IF input.validation?.min>0»Validators.minLength(«input.validation?.min»),«ENDIF»
			«IF input.validation?.max >-1»Validators.maxLength(«input.validation?.max»),«ENDIF»
		«ENDIF»
		
	])
	'''
	
	/**
	 * Checks if a given form field component needs validation depend on its input type.
	 */
	private def isValidatableType(Field field)
	{
		return field.inputType==InputType.COLOR
		|| field.inputType==InputType.DATE 
		|| field.inputType==InputType.DATETIME
		|| field.inputType==InputType.MONTH
		|| field.inputType==InputType.NUMBER
		|| field.inputType==InputType.TEL
		|| field.inputType==InputType.TIME
		|| field.inputType==InputType.URL
		|| field.inputType==InputType.WEEK 
		|| field.inputType==InputType.EMAIL       	
	}
	
	
	/**
	 * Checks if a button component is placed in the given form
	 */
	private def isSubmitButtonPresent(Form form)
	{
		return form.find(Button).exists[it.options?.submitsForm]
	}
	
}
