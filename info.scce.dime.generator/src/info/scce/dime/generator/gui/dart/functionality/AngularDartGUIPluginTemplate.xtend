/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import info.scce.dime.generator.util.DimeIOExtension
import graphmodel.Node
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.gUIPlugin.AbstractParameter
import info.scce.dime.gUIPlugin.ComplexInputParameter
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.gUIPlugin.DartPlugin
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.GenericInputParameter
import info.scce.dime.gUIPlugin.GenericParameter
import info.scce.dime.gUIPlugin.InputParameter
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.gUIPlugin.PrimitiveInputParameter
import info.scce.dime.gUIPlugin.PrimitiveParameter
import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.ElementCollector
import java.nio.file.Path

/**
 * Template to create an Angular Dart component class file for a GUI plug in component.
 */
class AngularDartGUIPluginTemplate extends GUIGenerator {
	
	extension DimeIOExtension = new DimeIOExtension
	
	/**
	 * Creates the import statement for a given GUI plug in to import the corresponding GUI plug in Angular component class file
	 */
	def getImports(GUIPlugin gp) {
		'''import 'package:app/src/plugin/«gp.pluginClassName».dart';'''
	}
	
	/**
	 * Creates the class name for a given GUI plug in to import the corresponding GUI plug in Angular component class
	 */
	def getPluginClassName(GUIPlugin cgp) {
		'''Plugin«cgp.label.escapeDart»«cgp.id.escapeDart»«cgp.rootElement.title.escapeDart»«cgp.rootElement.id.escapeDart»'''
	}
	
	/**
	 * Creates the HTML tag name for a given GUI plug in to use the corresponding GUI plug in Angular component template code
	 */
	def getPluginSelector(GUIPlugin cgp) {
		'''plugin-«cgp.id.escapeDart»«cgp.rootElement.id.escapeDart»'''
	}
	
	/**
	 * Generates the Angular Dart component class file for a given GUI plug in
	 */
	def createComponents(GUIPlugin gp, GUICompoundView gcv) '''
		// GUI plug in «gp.label»
		//import 'dart:html' as html;
		import 'dart:async';
		import 'dart:js' as js;
		import 'package:app/src/models/Selectives.dart';
		«FOR data : genctx.usedDatas»
			import 'package:app/src/data/«data.modelName.escapeDart».dart' as «DyWASelectiveDartGenerator.prefix(data)»;
		«ENDFOR»
		
		import 'package:angular/angular.dart';
		«IF gp.plugin.dartPlugin»
			import '«gp.plugin.path»/«gp.funct.functionName».dart';
		«ENDIF»
		@Component(
				selector: '«getPluginSelector(gp)»',
				directives: const [
					coreDirectives
					«IF gp.plugin.dartPlugin»
						,«gp.funct.functionName»
					«ENDIF»
				],
				templateUrl: '«getPluginClassName(gp)».html'
		)
		class «getPluginClassName(gp)»«IF !gp.plugin.dartPlugin» implements AfterViewChecked«IF gp.funct.isReCalled», OnChanges«ENDIF»«ENDIF» {
			
			«IF gp.plugin.dartPlugin»
				//events
				@ViewChildren(«gp.funct.functionName»)
				List<«gp.funct.functionName»> children;
			«ELSE»
				bool isLoaded = false;
			«ENDIF»
			// outputs
			«FOR out: gp.funct.outputs»
				// branch: «out.outputName»
				@Output('action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin')
				Stream<Map<String,dynamic>> get evt_action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin => action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.stream;
				StreamController<Map<String,dynamic>> action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin;
			«ENDFOR»
			// sync callbacks
			«FOR param: gp.funct.parameters.filter(InputParameter).filter[n|n.isIsSync].map[toParameter]»
				// synchronized input port: «param.name»
				@Output('sync«gp.id.escapeDart»«param.name»event')
				Stream<dynamic> get evt_sync«gp.id.escapeDart»«param.name»event => sync«gp.id.escapeDart»«param.name»event.stream;
				StreamController<dynamic> sync«gp.id.escapeDart»«param.name»event;
			«ENDFOR»
			//Input Parameter
			«FOR param: gp.funct.parameters.filter(InputParameter).map[toParameter]»
				// input port «param.name»
				@Input()
				«IF param instanceof PrimitiveParameter»
					«IF param.isIsList»List<«ENDIF»«param.type.toData.dartType»«IF param.isIsList»>«ENDIF» «param.name.escapeDart»;
				«ELSE»
					«IF param.isDataForParameter(gp)»
						«param.readDataForParameter(gp).selective(param.isList,gcv)» «param.name.escapeDart»;
					«ELSE»
						dynamic «param.name.escapeDart»;
					«ENDIF»
				«ENDIF»
			«ENDFOR»
		
			«gp.pluginClassName»()
			{
				// sync callbacks
				«FOR param: gp.funct.parameters.filter(InputParameter).filter[n|n.isIsSync].map[toParameter]»
					this.sync«gp.id.escapeDart»«param.name»event = new StreamController<dynamic>();
				«ENDFOR»
				// outputs
				«FOR out: gp.funct.outputs»
					this.action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin = new StreamController<Map<String,dynamic>>();
				«ENDFOR»
				// input params
				«FOR param: gp.funct.parameters.filter(PrimitiveInputParameter).map[toParameter].filter(PrimitiveParameter)»
					this.«param.name.escapeDart» = «IF param.isIsList»new List()«ELSE»«param.type.toData.defaultValue»«ENDIF»;
				«ENDFOR»
				«FOR param: gp.funct.parameters.filter(ComplexInputParameter).filter[isIsList]»
					this.«param.parameter.name.escapeDart» = new DIMEList();
				«ENDFOR»
			}
			«IF !gp.plugin.dartPlugin»
				/// triggers the connected java script function
				///
				/// passes all input variables and callbacks to the js function
				@override
				void ngAfterViewChecked()
				{
					if(!isLoaded) {
						isLoaded = true;
						callPluginFunction();
					}
				}
				
				Map<String,dynamic> _toDartSimpleObject(thing) {
					if (thing is js.JsObject) {
						Map<String,dynamic> res = new Map();
						js.JsObject o = thing as js.JsObject;
						Iterable<dynamic> k = (js.context['Object'].callMethod('keys', [o]));
						k.where((k)=>k is String).forEach((k) {
							res[k] = o[k];
						});
						return res;
					} else {
						return {};
					}
				}
				
				«IF gp.funct.isReCalled»
					/// triggers the connected java script function again
					/// if one of the binded input data changes
					///
					/// passes all input variables and callbacks to the js function
					@override
					ngOnChanges(Map<String, SimpleChange> changes) {
						callPluginFunction();
					}
				«ENDIF»
				
				/// triggers the connected java script function
				///
				/// passes all input variables and callbacks to the js function
				void callPluginFunction()
				{
					Map<String,dynamic> cache = new Map();
					js.context.callMethod('«gp.label»',[
					«FOR param: gp.funct.parameters.filter(InputParameter) SEPARATOR ","»
						«param.toParameter.name.escapeDart»
						«IF param.isIsSync»
							,sync«gp.id.escapeDart»«param.toParameter.name»Function
						«ENDIF»
					«ENDFOR»
					«IF hasOutputs(gp)»
						«IF !gp.funct.parameters.filter(InputParameter).empty»,«ENDIF»terminate«gp.id.escapeDart»
					«ENDIF»
					]);
				}
				
				«FOR param: gp.funct.parameters.filter(PrimitiveInputParameter).filter[isIsSync].map[toParameter]»
					/// synchronization callback
					///
					/// to update the variable in the component
					void sync«gp.id.escapeDart»«param.name»Function(dynamic value)
					{
						this.sync«gp.id.escapeDart»«param.name»event.add({'«param.name»':value});
					}
				«ENDFOR»
				
				«FOR param: gp.funct.parameters.filter(ComplexInputParameter).filter[isIsSync].map[toParameter].filter(ComplexParameter)»
					/// synchronization callback
					///
					/// to update the variable in the component
					void sync«gp.id.escapeDart»«param.name.escapeDart»Function({'«param.name»':value})
					{
						«IF param.toInputPort(gp) !== null && param.isDataForParameter(gp)»
							if(value!=null) {
								«IF param.isList»
									«param.readDataForParameter(gp).selective(param.isIsList,gcv)» selective = new «param.readDataForParameter(gp).selective(param.isIsList,gcv)»();
									value.forEach((n) => selective.add(«param.readDataForParameter(gp).selective(param.isIsList,gcv)».fromJSOG(value)));
								«ELSE»
									«param.readDataForParameter(gp).selective(param.isIsList,gcv)» selective = «param.readDataForParameter(gp).selective(param.isIsList,gcv)».fromJSOG(value);
								«ENDIF»
								this.sync«gp.id.escapeDart»«param.name»event.add({
									'«param.name.escapeString»':selective
								});
							}
						«ENDIF»
					}
				«ENDFOR»
			«ELSE»
				«FOR param: gp.funct.parameters.filter(InputParameter).filter[isIsSync].map[toParameter]»
					/// synchronization callback for dart plugin
					///
					/// to update the variable in the component
					void sync«gp.id.escapeDart»«param.name»Function(Map<String,dynamic> value)
					{
						this.sync«gp.id.escapeDart»«param.name»event.add(value);
					}
				«ENDFOR»
			«ENDIF»
			
			«IF gp.hasOutputs && !gp.plugin.dartPlugin»
				«IF gp.plugin.dartPlugin»
					«FOR out: gp.funct.outputs»
						void triggeraction«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin(Map<String,dynamic> values) {
							Map<String,dynamic> map = new Map();
							«FOR p: out.parameters»
								«IF p.isComplex»
									if(values['«p.name.escapeString»']!=null) {
										«IF p.isList»
											if(values['«p.name»'] is! BaseSelectiveList) {
												map['«p.name.escapeString»']=BaseSelectiveList.fromDelegates(values['«p.name.escapeString»']);
											} else {
												map['«p.name.escapeString»']=values['«p.name.escapeString»'];
											}
										«ELSE»
											if(values['«p.name.escapeString»'] is! BaseSelective){
												map['«p.name.escapeString»']=BaseSelective.fromDelegate(values['«p.name.escapeString»']);
											} else {
												map['«p.name.escapeString»']=values['«p.name.escapeString»'];
											}
										«ENDIF»
									}
								«ELSE»
									map['«p.name.escapeString»']=values['«p.name.escapeString»'];
								«ENDIF»
							«ENDFOR»
							this.action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.add(map);
						}
					«ENDFOR»
				«ELSE»
					/// termination callback
					///
					/// to update the variable in the component and propagate
					/// the event to the parent component
					void terminate«gp.id.escapeDart»(dynamic jsValue)
					{
						Map<String,dynamic> value = _toDartSimpleObject(jsValue);
						String branchName = value["branchName"];
						«FOR out: gp.funct.outputs»
							if('«out.outputName»'== branchName){
								var values = value['values'];
								Map<String,dynamic> map = new Map();
								«FOR p: out.parameters»
									«IF p.isComplex»
										if(values['«p.name»']!=null) {
											«IF p.isList»
											map['«p.name.escapeString»']=BaseSelectiveList.fromDelegates(values['«p.name.escapeString»']);
											«ELSE»
											map['«p.name.escapeString»']=BaseSelective.fromDelegate(values['«p.name.escapeString»']);
											«ENDIF»
										}
									«ELSE»
										map['«p.name.escapeString»']=values['«p.name.escapeString»'];
									«ENDIF»
								«ENDFOR»
								this.action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.add(map);
							}
						«ENDFOR»
					}
				«ENDIF»
			«ENDIF»
			
			«FOR listener: gp.funct.events»
				/// receives the events for the «listener.name» listener
				void recieve«listener.name»Event(Map<String,dynamic> map)
				{
					«IF gp.plugin.dartPlugin»
						this.children.forEach((n)=>n.«listener.name»(
							«FOR param: listener.parameters SEPARATOR ","»
								map['«param.name.escapeString»']
							«ENDFOR»
						));
					«ELSE»
						//write the received data
						Map<String,dynamic> cache = new Map();
						(js.context['«listener.name»'] as js.JsFunction).apply([
						«FOR param: listener.parameters SEPARATOR ","»
							map['«param.name.escapeString»']«IF param.isComplex»«IF param.isList».map((n)=>n.toJSOG(cache)).toList()«ELSE»!=null?map['«param.name.escapeString»'].toJSOG(cache):null«ENDIF»«ENDIF»
						«ENDFOR»
						]);
					«ENDIF»
				}
			«ENDFOR»
		}
	'''
	
	def toInputPort(ComplexParameter parameter, GUIPlugin plugin) {
		plugin.inputPorts.findFirst[it?.name == parameter?.name]
	}
	
	def getToParameter(InputParameter ip) {
		switch (ip) {
			ComplexInputParameter:   ip.parameter
			PrimitiveInputParameter: ip.parameter
			GenericInputParameter:   ip.parameter
		}
	}
	
	def boolean getIsComplex(InputParameter parameter) {
		parameter instanceof ComplexInputParameter ||
		parameter instanceof GenericInputParameter
	}
	
	def boolean getIsComplex(AbstractParameter parameter) {
		parameter instanceof ComplexParameter ||
		parameter instanceof GenericParameter
	}
	
	def CharSequence selective(Node data, boolean isList, GUICompoundView gcv) {
		switch (v: gcv.pairs.get(data)) {
			ComplexTypeView:  return v.selectiveClassName(isList)
			ComplexFieldView: return v.selectiveClassName(isList)
		}
		switch (data) {
			PrimitiveAttribute: return data.attribute.dataType.dartType(isList)
			PrimitiveVariable:  return data.dataType.toData.dartType(isList)
		}
		throw new IllegalStateException('''No fitting data: «data»''')
	}
	
	def String dartType(PrimitiveType type, boolean isList) {
		if (isList) {
			return '''DIMEList<«type.dartType»>'''
		}
		else {
			return type.dartType
		}
	}
	
	/**
	 * Helper method to get the default value for a GUI plug in primitive type
	 */
	private def defaultValue(PrimitiveType type) {
		// FIXME: Missing switch case for PrimitiveType.FILE, default would be null.
		switch (type) {
			case BOOLEAN:   "false"
			case INTEGER:   "0"
			case REAL:      "0.0"
			case TEXT:      '""'
			case TIMESTAMP: "new DateTime.now()"
//			case FILE:      ???
		}
	}
	
	/**
	 * Helper method to get the type for a GUI plug in primitive type
	 */
	private def dartType(PrimitiveType type) {
		switch(type) {
			case BOOLEAN:   "bool"
			case INTEGER:   "int"
			case REAL:      "double"
			case TEXT:      "String"
			case TIMESTAMP: "DateTime"
			case FILE:      "FileReference"
		}
	}
	
	/**
	 * Generates the HTML template code for the GUI plug in Angular Dart component class
	 */
	def String createHTML(GUIPlugin gp) {
		gp.HTMLContent
	}
	
	/**
	 * Generates the HTML tag for the GUI plug in used in another Angular component template code
	 */
	def CharSequence create (GUIPlugin gp) {
		if (preview) {
			return gp.HTMLContent
		}
		return '''
			<«gp.pluginSelector»
				«gp.printStyle»
			 	«gp.printNgIfFor»
				«gp.guiPluginInputs»
				«FOR out:gp.funct.outputs»
					(action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin)="terminate«gp.id.escapeDart»«out.outputName.escapeDart»($event)"
				«ENDFOR»
				«FOR param:gp.funct.parameters.filter(InputParameter).filter[isIsSync].map[toParameter].reject[inputForParameter(gp) instanceof InputStatic]»
					(sync«gp.id.escapeDart»«param.name»event)="«DataDartHelper.getBindedDataName(param.inputForParameter(gp), DataAccessType.^FOR, MODE.SET_INIT)»($event['«param.name.escapeString»'])"
				«ENDFOR»
			>
			«FOR arg: gp.arguments»
				<ph «arg.blockName.escapeDart.argumentTransclusion(if (gp.plugin.isDartPlugin) gp.id.escapeDart else "")»>
					«new BaseTemplate().baseContent(ElementCollector.getElementsV(arg.allNodes), true)»
				</ph>
			«ENDFOR»
			</«gp.pluginSelector»>
		'''
	}
	
	/**
	 * Returns the list of all variables and attributes connected to the given button by submission edges
	 */
	def Variable getParentVariable(IO port) {
		if (port.getIncoming(Read).empty) {
			return null
		}
		val node = port.getIncoming(Read).get(0).sourceElement
		if (node instanceof Attribute) {
			return node.container as Variable
		}
		else {
			return node as Variable
		}
	}
	
	/**
	 * Generates the methods used in the parent Angular Dart component 
	 */
	def createMethods(GUIPlugin gp, boolean isForm) '''
		«FOR out: gp.funct.outputs»
			/// branch callback for branch «out.outputName»
			void terminate«gp.id.escapeDart»«out.outputName.escapeDart»(Map<String,dynamic> values) {
				Map<String,dynamic> map = new Map();
				«FOR p:out.parameters»
					map['«p.name.escapeString»']=values['«p.name.escapeString»'];
				«ENDFOR»
				this.«ConventionHelper.getEventName(out.outputName)».add(map);
			}
		«ENDFOR»
	'''
	
	/**
	 * Helper method to receive the GUI plug in function
	 */
	static def Function funct(GUIPlugin plugin) {
		plugin.function as Function
	}
	
	/**
	 * Helper method to receive the GUI plug in function
	 */
	static def Plugin plugin(GUIPlugin plugin) {
		plugin.funct.eContainer as Plugin
	}
	
	/**
	 * Helper method to receive the GUI plug in function
	 */
	static def boolean isDartPlugin(Plugin plugin) {
		plugin instanceof DartPlugin
	}
	
	/**
	 * Helper method to receive HTML content of the GUI plug in, if available
	 */
	private def getHTMLContent(GUIPlugin cgp) {
		val plugin = cgp.plugin
		if (plugin.template !== null) {
			if (plugin instanceof DartPlugin) {
				val placeholder = !plugin.template.placholders?.placeholders.nullOrEmpty
				return cgp.funct.dartPluginHTML(placeholder, cgp)
			}
			val relativePath = plugin.template.file.path
			val absolutePath = cgp.projectPath.resolve(relativePath)
			return absolutePath.readString()
		}
	}
	
	def String dartPluginHTML(Function function, boolean placeholder, GUIPlugin gp) '''
		<«function.functionName.escapeString»
			«FOR out: function.outputs»
				(«out.outputName.escapeString»)="action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.add($event)"
			«ENDFOR»
			«FOR param: function.parameters.filter(InputParameter).map[toParameter]»
				[«param.name.escapeString»]="«param.name.escapeDart»"
			«ENDFOR»
			
			«FOR param:gp.funct.parameters.filter(InputParameter).filter[isIsSync].map[toParameter]»
				«IF !(param.inputForParameter(gp) instanceof InputStatic) && !param.inputForParameter(gp).incoming.empty»
				(sync«param.name»)="sync«gp.id.escapeDart»«param.name»Function($event)"
				«ENDIF»
			«ENDFOR»
		>
		«IF placeholder»
			«FOR arg: gp.arguments»
				<ph «arg.blockName.escapeString.argumentTransclusion("")»>
					<ng-content «arg.blockName.escapeDart.placeholderTransclusion(gp.id.escapeDart)»></ng-content>
				</ph>
			«ENDFOR»
		«ENDIF»
		</«function.functionName.escapeString»>
	'''
	
	/**
	 * Helper method to copy the JS and CSS resources, defined in the given GUI plug in
	 * to the defined resourcePath
	 */
	def void copyResources(GUIPlugin cgp, Path pluginPath) {
		val plugin = cgp.plugin
		if (plugin instanceof DartPlugin) {
			val projectPath = plugin.projectPath
			val target = pluginPath.resolve(plugin.path.escapeDart)
			if (plugin.style !== null) {
				for (path: plugin.style.files.map[path].reject[isHTTP]) {
					val sourceFile = projectPath.resolve(path)
					copyFile(sourceFile, target)
				}
			}
			if (plugin.script !== null) {
				for (path: plugin.script.files.map[path].reject[isHTTP]) {
					val sourceFile = projectPath.resolve(path)
					if (!plugin.functions.empty) {
						copyFile(sourceFile, target, '''«plugin.functions.head.functionName».dart''')							
					}
					else {
						copyFile(sourceFile, target)
					}
				}
			}
			if (plugin.template !== null) {
				var sourceFile = projectPath.resolve(plugin.template.file.path)
				copyFile(sourceFile, target)
			}
		}
	}
	
	/**
	 * Helper method to copy the additional JS and CSS resources, defined in the given GUI model in
	 * to the defined resourcePath
	 */
	def void copyResources(GUI gui, Path cssPath) {
		val projectPath = gui.projectPath
		val targetCSS = cssPath.resolve("css", gui.title.escapeDart)
		for (path: gui.additionalStylesheets.reject[isHTTP]) {
			val sourceFile = projectPath.resolve(path)
			copyFile(sourceFile, targetCSS)
		}
	}
	
	/**
	 * Helper method to copy a file to the defined path
	 */
	def void copyFile(Path sourceFile, Path targetDir, String newFileName) {
		try {
			val targetFile = targetDir
				.createDirectories()
				.resolve(newFileName)
			sourceFile.copy(targetFile)
		}
		catch(Exception e) {
			e.printStackTrace
		}
	}
	
	/**
	 * Helper method to copy a file to the defined path
	 */
	def copyFile(Path sourceFile, Path target) {
		copyFile(sourceFile, target, sourceFile.fileName.toString)
	}
	
	private def IO inputForParameter(AbstractParameter param, GUIPlugin gp) {
		gp.IOs.filter[name == param.name].head
	}
	
	private def boolean isDataForParameter(AbstractParameter param,GUIPlugin gp) {
		!inputForParameter(param, gp)?.getIncoming(Read).nullOrEmpty
	}
	
	private def Node readDataForParameter(AbstractParameter param, GUIPlugin gp) {
		inputForParameter(param, gp).getIncoming(Read).head.sourceElement
	}
	
	private def boolean hasOutputs(GUIPlugin gp) {
		!gp.funct.outputs.nullOrEmpty
	}
	
}
