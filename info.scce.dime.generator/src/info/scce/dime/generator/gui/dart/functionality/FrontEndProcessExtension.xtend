/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import info.scce.dime.dad.dad.ProcessComponent
import info.scce.dime.dad.dad.ProcessEntryPointComponent
import info.scce.dime.dad.dad.URLProcess
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ControlFlow
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.LinkProcessSIB
import info.scce.dime.process.process.NativeFrontendSIBReference
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import java.util.LinkedList
import java.util.List
import java.util.Set

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class FrontEndProcessExtension {
	
	protected extension RESTExtension = new RESTExtension
	protected extension GraphModelExtension = new GraphModelExtension

	
	def getRouterName(Process p)
	'''Routable«p.modelName.escapeDart»«p.id.escapeDart»Process'''
	
	def dispatch startRouteParameter(ProcessComponent p){
		p.model.startSIBs.get(0).outputPorts.map[portParam]
	}
	
	def dispatch startRouteParameter(ProcessEntryPointComponent p){
		p.entryPoint.inputPorts.map[portParam]
	}
	
	
	
	def dispatch portParamType(InputStatic port) {
		switch(port){
			case BooleanInputStatic: return '''bool'''
			case TextInputStatic: return '''String'''
			case IntegerInputStatic: return '''int'''
			case RealInputStatic: return '''double'''
			case TimestampInputStatic: return '''DateTime'''
		}
	}
	
	def complexPortParamParser(CharSequence s,boolean isList) {
		if(isList) {
			return '''new DIMEList<int>.from(«s».split(",").map((n)=>Uri.decodeComponent(n)).map((n)=>int.parse(n)).toList().cast<int>())'''
		}
		'''int.parse(«s»)'''
	}
	
	def primitivePortParamParser(CharSequence s,PrimitiveType dataType, boolean isList) {
		if(isList) {
			return '''new DIMEList<«dataType.portParamPrimitiveType»>.from(«s».split(",").map((n)=>Uri.decodeComponent(n)).map((n)=>«"n".portParamParserPrimitiveType(dataType)»).toList().cast<«dataType.portParamPrimitiveType»>())'''
		}
		'''«s.portParamParserPrimitiveType(dataType)»'''
	}
	
	
	def dispatch portParamParser(CharSequence s,ComplexInputPort port) {
		s.complexPortParamParser(port.isIsList)
	}
	def dispatch portParamParser(CharSequence s,ComplexOutputPort port) {
		s.complexPortParamParser(port.isIsList)
	}
	
	def dispatch portParamParser(CharSequence s,PrimitiveInputPort port) {
		s.primitivePortParamParser(port.dataType,port.isIsList)
	}
	
	def dispatch portParamParser(CharSequence s,PrimitiveOutputPort port) {
		s.primitivePortParamParser(port.dataType,port.isIsList)
	}
	
	def portParamParserPrimitiveType(CharSequence s,PrimitiveType pt){
		switch(pt){
			case BOOLEAN: return '''«s»=="true"'''
			case TEXT: return '''«s»'''
			case INTEGER: return '''int.parse(«s»)'''
			case REAL: return '''double.parse(«s»)'''
			case TIMESTAMP: return '''«s»==null?null:DateConverter.fromJSON(«s»)'''
			case FILE: return '''«s»==null?null:FileReference.fromId(int.parse(«s»))'''
			
		}
	}
	
	def dispatch generateElementImport(GUI g)'''gui/«g.simpleTypeNameDart»'''
	def dispatch generateElementImport(NativeFrontendSIBReference nfs)'''native/«nfs.simpleTypeNameDart»'''
	def dispatch generateElementImport(Process p)'''process/«p.simpleTypeNameDart»'''
	
	def dispatch generateElementDeserializer(GuardContainer g)'''case '«g.id»': return new GuardContainerInput(map);'''
	def dispatch generateElementDeserializer(GUISIB g)'''case '«g.id»': return new «g.gui.simpleTypeNameDart»Input(map,cache);'''
	def dispatch generateElementDeserializer(LinkProcessSIB g)'''case '«g.id»': return new «g.getModel.simpleTypeNameDart»LinkSIBInput(map,cache);'''
	def dispatch generateElementDeserializer(NativeFrontendSIBReference nfs)'''case '«nfs.id»': return new «nfs.simpleTypeNameDart»Input(map,cache);'''
	
	def portParamParserStatic(InputStatic pt){
		switch(pt){
		 	BooleanInputStatic: return '''«pt.value.toString»'''
			TextInputStatic: return '''"«pt.value»"'''
			IntegerInputStatic: return '''«pt.value»'''
		 	RealInputStatic: return '''«pt.value»'''
			TimestampInputStatic: return '''DateConverter.fromSecondsSinceEpoch(«pt.value»)'''
			
		}
	}
	
	
	def portParamPrimitiveType(PrimitiveType pt){
		switch(pt){
			case BOOLEAN: return '''bool'''
			case TEXT: return '''String'''
			case INTEGER: return '''int'''
			case REAL: return '''double'''
			case TIMESTAMP: return '''DateTime'''
			case FILE: return '''FileReference'''
			
		}
	}
	
	def portPrimitiveType(PrimitiveType pt){
		switch(pt){
			case BOOLEAN: return '''bool'''
			case TEXT: return '''String'''
			case INTEGER: return '''int'''
			case REAL: return '''double'''
			case TIMESTAMP: return '''DateTime'''
			case FILE: return '''FileReference'''
			
		}
	}
	
	def portListStatus(CharSequence s, boolean list)'''«IF list»DIMEList<«ENDIF»«s»«IF list»>«ENDIF»'''
	
	def dispatch portParamType(ComplexOutputPort port) {
		'''int'''.portListStatus(port.isIsList)
	}
	
	def dispatch portParamType(ComplexInputPort port) {
		'''int'''.portListStatus(port.isIsList)
	}
	
	def dispatch portParamType(PrimitiveInputPort port) {
		port.dataType.portParamPrimitiveType.portListStatus(port.isIsList)
	}
	
	def dispatch portParamType(PrimitiveOutputPort port) {
		port.dataType.portParamPrimitiveType.portListStatus(port.isIsList)
	}
	
	def dispatch portType(ComplexOutputPort port) {
		'''«port.dataType.rootElement.modelName.escapeDart».«port.dataType.name.escapeDart.toFirstUpper»'''.portListStatus(port.isIsList)
	}
	
	def dispatch portType(ComplexInputPort port) {
		'''«port.dataType.rootElement.modelName.escapeDart».«port.dataType.name.escapeDart.toFirstUpper»'''.portListStatus(port.isIsList)
	}
	
	def dispatch portType(PrimitiveOutputPort port) {
		port.dataType.portParamPrimitiveType
	}
	
	def dispatch portParam(ComplexOutputPort port) {
		''':«port.name.escapeDart»'''
	}
	
	def dispatch portParam(PrimitiveOutputPort port) {
		''':«port.name.escapeDart»'''
	}
	
	def dispatch portParam(PrimitiveInputPort port) {
		''':«port.name.escapeDart»'''
	}
	
	def dispatch portParam(ComplexInputPort port) {
		''':«port.name.escapeDart»'''
	}
	
	def List<SIB> findFrontEndSibs(Process p,GenerationContext genctx,Set<String> found) {
		if(found.contains(p.id)){
			return new LinkedList
		}
		found.add(p.id)
		val sibs = (p.GUISIBs + p.nativeFrontendSIBReferences + p.linkProcessSIBs).toList
		p.processSIBs.filter[genctx.visibleProcesses.contains(it.proMod)].forEach[n|
			sibs += n.proMod.findFrontEndSibs(genctx, found)
		]
		p.guardContainers.map[guardedProcessSIBs.get(0)].filter[genctx.visibleProcesses.contains(it.proMod)].forEach[n|
			sibs += n.proMod.findFrontEndSibs(genctx, found)
		]
		return sibs;
	}
	
	def List<GuardContainer> findGuardContainersSibs(Process p,GenerationContext genctx,Set<String> found) {
		if(found.contains(p.id)){
			return new LinkedList
		}
		found.add(p.id)
		val sibs = new LinkedList(p.guardContainers)
		p.processSIBs.filter[genctx.isVisible(it.proMod)].forEach[n|
			sibs += n.proMod.findGuardContainersSibs(genctx, found)
		]
		p.guardContainers.map[guardedProcessSIBs.get(0)].filter[genctx.visibleProcesses.contains(it.proMod)].forEach[n|
			sibs += n.proMod.findGuardContainersSibs(genctx, found)
		]
		return sibs;
	}
	
	def Iterable<Process> findFrontProcesses(Process p, GenerationContext genctx, Set<String> found) {
		val processes = new LinkedList
		if(found.contains(p.id)) {
			return processes
		}
		processes.add(p)
		found.add(p.id)
		p.processSIBs.filter[genctx.isVisible(it.proMod)].forEach[n|
			processes += n.proMod.findFrontProcesses(genctx, found)
		]
		p.guardContainers.map[guardedProcessSIBs.get(0)].filter[genctx.isVisible(it.proMod)].forEach[n|
			processes += n.proMod.findFrontProcesses(genctx, found)
		]
		return processes;
		
	}
	
	def dispatch Process getModel(LinkProcessSIB link) {
		(link.proMod as URLProcess).model
	}
	
	def dispatch Process getModel(ProcessComponent process) {
		process.model
	}
	def dispatch Process getModel(ProcessEntryPointComponent process) {
		process.entryPoint.rootElement
	}
	
	def getInjectableSIBs(SIB it) {
		findSuccessorsVia(BranchConnector, ControlFlow)
			.filter(GUISIB)
			.map[switch it {
				case !isMajorPage: it
				case defaultContent instanceof GUISIB: defaultContent
			}]
			.filterNull
			.toSet
	}
}
