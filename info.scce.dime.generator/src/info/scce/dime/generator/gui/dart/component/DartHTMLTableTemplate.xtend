/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.html.HTMLTableTemplate
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.gui.gui.Table

/**
 * Template to generate the methods and declarations for a table component
 */
class DartHTMLTableTemplate extends HTMLTableTemplate{

	/**
	 * Generates the refresh flag declarations for a table component 
	 */
	def createTableDeclaration(ModelElementContainer gui)
	'''
	// table refresh flags
	«FOR Table table : gui.find(Table)»
		bool refresh«ConventionHelper.cincoID(table)»Table;
	«ENDFOR»
	'''	
}
