/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.functionality

import graphmodel.Node
import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.rest.model.TypeViewUtils
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.helper.ElementCollector
import info.scce.dime.process.process.Process

class AngularDartSecuritySIBTemplate extends GUIGenerator {
	
	protected extension RESTExtension = new RESTExtension
	
	def tag(SecuritySIB sib)'''sec-«sib.id.escapeDart»-tag'''
	
	def component(SecuritySIB sib)'''Security«sib.id.escapeDart»SIB'''
	
	def imports(SecuritySIB sib){
		"import 'package:app/src/security/"+sib.component+".dart';"
	}
	
	
	def create(SecuritySIB sib){
		val granted = sib.arguments.findFirst[blockName.equals("GRANTED")]
		val denied = sib.arguments.findFirst[blockName.equals("DENIED")]
		'''
		<!-- Security SIB «((sib as SecuritySIB).proMod as Process).modelName» -->
		<«sib.tag»
			«sib.controlSIBInputs»
		>
			<div dime-sec-sib="granted">
				«new BaseTemplate().baseContent(ElementCollector.getElementsV(granted.allNodes))»
			</div>
			«IF denied != null»
			<div dime-sec-sib="denied">
				«new BaseTemplate().baseContent(ElementCollector.getElementsV(denied.allNodes))»
			</div>
			«ENDIF»
		</«sib.tag»>
		<!-- END Security SIB «((sib as SecuritySIB).proMod as Process).modelName» -->
		'''
	}
	 
	def createComponent(SecuritySIB sib,Process p,GUICompoundView gcv)
	'''
	import 'dart:html';
	import 'dart:convert';
	
	import 'package:angular/core.dart';
	
	//Data
	«FOR data : genctx.usedDatas»
	import 'package:app/src/models/«data.modelName.escapeDart».dart' as «DyWASelectiveDartGenerator.prefix(data)»;
	«ENDFOR»
	
	// Input
	import 'package:app/src/models/«p.getSimpleTypeNameDart»«sib.id.escapeDart»Input.dart';
	// Output
	import 'package:app/src/models/«p.getSimpleTypeNameDart»«sib.id.escapeDart»Output.dart';
	
	import 'package:app/src/models/Selectives.dart';
	
	@Component(
	  selector: "«sib.tag»",
	  template: «"'''"»
	  <template [ngIf]="loaded">
	  	<template [ngIf]="granted">
	  		<ng-content select="[dime-sec-sib=granted]" ></ng-content>
	  	</template>
	  	«IF sib.arguments.exists[blockName.equals("DENIED")]»
		<template [ngIf]="denied">
			<ng-content select="[dime-sec-sib=denied]" ></ng-content>
	  	</template>
	  	«ENDIF»
	  </template>
	  «"'''"»
	)
	
	class «sib.component» implements OnInit, OnChanges {
		
		bool loaded = false;
		bool loading = true;
		bool granted = false;
		bool denied = false;
		
		«FOR input:sib.IOs.filter(ComplexInputPort)»
		@Input()
		«IF input.hasData»«new AngularDartGUIPluginTemplate().selective(input.data as Node,input.isList,gcv)»«ELSE»dynamic«ENDIF» «input.name.escapeDart»;
		«ENDFOR»
		
		«FOR input:sib.IOs.filter(PrimitiveInputPort)»
		@Input()
		«IF input.hasData»«new AngularDartGUIPluginTemplate().dartType(input.dataType.toData,input.isList)»«ELSE»dynamic«ENDIF» «input.name.escapeDart»;
		«ENDFOR»
		
		«sib.component»()
		{
			«FOR input:sib.IOs.filter(PrimitiveInputPort)»
				this.«input.name.escapeDart» = «input.dataType.toData.defaultValue»;
			«ENDFOR»
			«FOR input:sib.IOs.filter(ComplexInputPort).filter[isIsList]»
				this.«input.name.escapeDart» = new DIMEList();
			«ENDFOR»
		}
		
		@override
		void ngOnInit async ()
		{
			evaluate();
		}
		
		@override
		void ngOnChanges(Map<String, SimpleChange> changes) {
		    evaluate();
		}
		
		void evaluate() {
			if(loading) {
				loading = false;
				loaded = false;
				var url = "rest/process/«p.modelName.escapeJava.toFirstUpper»/«sib.id.escapeJava»/public";
				Map<String,dynamic> processMap = new Map();
				var sendData = new «p.getSimpleTypeNameDart»«sib.id.escapeDart»Input();
				«{
					val inputTVMap = TypeViewUtils.buildInputTypeViewMap(sib as SecuritySIB,gcv)
					'''
					«FOR inputTv:inputTVMap.entrySet.filter[value!=null]»
					// input port «inputTv.key.name»
					sendData.«inputTv.value.name.escapeDart»«inputTv.value.id.escapeDart» = this.«inputTv.key.name.escapeDart»«IF !inputTv.value.primitive».dywa_id«ENDIF»;
					«ENDFOR»
					'''
				}»
				
				var requestHeaders = {'Content-Type':'application/json'};
				HttpRequest.request(url,method: "POST",sendData: jsonEncode({'processInputs':sendData.toJSOG(map:processMap)}),requestHeaders: requestHeaders)
				.then((response){
					loaded = true;
					if (response.readyState == HttpRequest.DONE && (response.status == 200)) {
						var processResponse = «p.getSimpleTypeNameDart»«sib.id.escapeDart»Output.fromJSON(response.responseText);
						if(processResponse.branchName=='granted') {
							granted = true;
							denied = false;
						}
						if(processResponse.branchName=='denied') {
							granted = false;
							denied = true;
						}
					}
					loading = true;
				})
				.catchError((ProgressEvent pe){
					granted = false;
					denied = false;
					loaded = true;
					loading = true;
				});
			}
		}
	}
	'''
	def boolean hasData(IO cio){
		!cio.getIncoming(Read).empty
	}
	
	def Node data(IO cio){
		cio.getIncoming(Read).get(0).sourceElement
	}
	
}
