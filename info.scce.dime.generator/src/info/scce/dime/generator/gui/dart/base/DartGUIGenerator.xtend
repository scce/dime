/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.base

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator2
import info.scce.dime.generator.util.DimeIOExtension
import graphmodel.GraphModel
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.StartupProcessPointer
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.Inheritance
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserAssociation
import info.scce.dime.gUIPlugin.DartPlugin
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.data.DartDataGenerator
import info.scce.dime.generator.gui.dart.component.AngularComponentHTMLTemplate
import info.scce.dime.generator.gui.dart.component.AngularDartComponentTemplate
import info.scce.dime.generator.gui.dart.component.AngularDartFormTemplate
import info.scce.dime.generator.gui.dart.component.AngularDartLoginComponent
import info.scce.dime.generator.gui.dart.component.AngularDartTableTemplate
import info.scce.dime.generator.gui.dart.component.AngularFormHTMLTemplate
import info.scce.dime.generator.gui.dart.component.AngularTableHTMLTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartCurrentUserService
import info.scce.dime.generator.gui.dart.functionality.AngularDartDADProcessTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartGUIPluginTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartProcessTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartRootProcessTemplate
import info.scce.dime.generator.gui.dart.functionality.AngularDartSecuritySIBTemplate
import info.scce.dime.generator.gui.html.HTMLImageTemplate
import info.scce.dime.generator.gui.html.HTMLNotificationComponent
import info.scce.dime.generator.gui.rest.DartFileGuardGenerator
import info.scce.dime.generator.gui.rest.DartTOGeneratorHelper
import info.scce.dime.generator.gui.rest.DyWADartGenerator
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.livevariable.LiveVariableGenerator
import info.scce.dime.generator.process.BackendProcessGeneratorHelper
import info.scce.dime.generator.rest.DownloadTokenGenerator
import info.scce.dime.generator.rest.DyWAAbstractGenerator
import info.scce.dime.generator.rest.GUIProgressGenerator
import info.scce.dime.generator.rest.SelectiveControllerGenerator
import info.scce.dime.generator.rest.SelectiveGenerator
import info.scce.dime.generator.rest.SelectiveInteractableProcessGenerator
import info.scce.dime.generator.rest.SelectiveUserGenerator
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.gui.Table
import info.scce.dime.process.process.NativeFrontendSIBReference
import info.scce.dime.process.process.Process
import java.io.IOException
import java.nio.file.Path
import java.sql.Timestamp
import java.util.Collection
import java.util.List
import java.util.Map
import java.util.Set
import org.apache.commons.io.FileUtils
import org.eclipse.core.runtime.IProgressMonitor

import static extension info.scce.dime.generator.data.DartDataGenerator.getName
import static extension info.scce.dime.generator.gui.data.SelectiveExtension.collectComplexTypeViews
import static extension info.scce.dime.generator.gui.data.SelectiveExtension.computeBranchTypeViews
import static extension info.scce.dime.generator.gui.data.SelectiveExtension.computeProcessTypeViews
import static extension info.scce.dime.generator.gui.rest.model.TypeViewUtils.buildInputTypeViewMap
import static extension info.scce.dime.generator.gui.utils.GUIGenerator.isHTTP
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.escapeDart
import static extension info.scce.dime.gui.helper.GUIExtension.lastModified2

/** 
 * Starts the generation of all front end code.
 * Including the entire Angular application.
 * Transforms all GUI models interaction Processes to code.
 * @author zweihoff
 */
class DartGUIGenerator implements IGenerator2<DAD> {
	
	extension RESTExtension = new RESTExtension()
	extension DartTOGeneratorHelper = new DartTOGeneratorHelper()
	extension DimeIOExtension = new DimeIOExtension()
	
	val GenerationContext genctx
	val BackendProcessGeneratorHelper backendProcessGeneratorHelper
	
	var Path outlet
	var Path dyawApp
	var Path webappPath
	var Path webPath
	var Path libPath
	var Path guiPath
	var Path pluginPath
	var Path processPath
	var Path processRootPath
	var Path dadPath
	var Path servicePath

	/** 
	 * Initializes the Generator.
	 * Requires a Map from every interaction processes to
	 * all guard container which contain a process SIB of this process
	 */
	new (GenerationContext genctx, BackendProcessGeneratorHelper helper) {
		this.genctx = genctx
		this.backendProcessGeneratorHelper = helper
	}

	/** 
	 * Starts the generation.
	 */
	override generate(DAD dad, Path outlet, IProgressMonitor monitor) {
		
		var long debugTime = System.currentTimeMillis
		
		println("[INFO] GUI Generator started ...")
		
		// Define the paths where the generated files will be placed
		this.outlet          = outlet
		this.dyawApp         = outlet.resolve("dywa-app")
		this.webappPath      = outlet.resolve("webapp")
		this.webPath         = webappPath.resolve("web")
		this.libPath         = webappPath.resolve("lib", "src")
		this.guiPath         = libPath.resolve("gui")
		this.pluginPath      = libPath.resolve("plugin")
		this.processPath     = libPath.resolve("process")
		this.processRootPath = libPath.resolve("root")
		this.dadPath         = libPath.resolve("dad")
		this.servicePath     = libPath.resolve("services")
		
		val timeStamp = new Timestamp(System.currentTimeMillis).time.toString
		val dadModified = dad.lastModified2
		
		copyAdditionalResources()
		fillSelectiveCache()
		
		for (data: genctx.usedDatas) {
			try data.generateDataModel(genctx)
			catch (Exception e) e.printErr(data)
		}
		println("[INFO] All Data Models generated")
		
		// Create DAD processes
		for (comp: dad.processComponents) {
			if (comp.getIncoming(StartupProcessPointer).isEmpty) {
				try comp.model.generateProcesses(genctx, true)
				catch (Exception e) e.printErr(comp.model)
			}
		}
		println("[INFO] All DAD processes generated")
		
		// Create Processes
		for (process: genctx.visibleProcesses) {
			try process.generateProcessDartTOs(genctx)
			catch (Exception e) e.printErr(process)
		}
		
		// Create GUI Process
		for (process: genctx.GUIProcesses) {
			try process.generateGUIProcess(genctx)
			catch (Exception e) e.printErr(process)
		}
		println("[INFO] All visible processes generated")
		
		// Create GUIs
		val Map<String, GUIPlugin> guiPlugins = newHashMap
		val Set<String> services = newHashSet
		for (gui: genctx.usedGUIs) {
			try gui.generateGUI(guiPlugins, services, genctx)
			catch (Exception e) e.printErr(gui)
		}
		println("[INFO] All GUIs generated")
		
		// Generate deserializer
		for (process: genctx.rootProcesses) {
			try {
				val contents = process.generateFrontEndInputDeserializer()
				val fileName = '''UserInteraction«process.simpleTypeNameDart»ResponseDeserializer.dart'''
				DyWAAbstractGenerator.generate(contents, ".models.", fileName, libPath, 0)
			}
			catch (Exception e) {
				e.printErr(process)
			}
		}
		
		// Create notification component
		createFile(
			new HTMLNotificationComponent().create(),
			libPath.resolve("notification", "notification_component.html"),
			dadModified
		)
		
		val selectiveGenerator = new SelectiveGenerator(genctx)
		selectiveGenerator.generate(dyawApp)
		genctx
			.usedDatas
			.flatMap[types]
			.forEach [ type |
				selectiveGenerator.generate(type, dyawApp)
			]
		new SelectiveControllerGenerator().generate(genctx, dyawApp)
		println("[INFO] Selective Controller generated")
		
		new SelectiveUserGenerator().generate(genctx, dyawApp)
		println("[INFO] Selective User generated")
		
		new GUIProgressGenerator().generate(genctx, dyawApp)
		println("[INFO] GUI Resumers generated")
		
		// Create App with possible routes
		try {
			generateApp(guiPlugins.values, services, timeStamp, genctx, dadModified)
			println('''[INFO] GUI «dad.appName» App generated''')
		}
		catch (IOException e) {
			e.printStackTrace()
		}
		
		new LiveVariableGenerator(genctx.compoundViews).generate(dad, dyawApp, monitor)
		
		println('''[INFO] GUI successfully generated in «System.currentTimeMillis - debugTime»''')
		
	}
	
	def private void copyAdditionalResources() {
		
		val projectPath = genctx.dad.projectPath
		for (extResource: genctx.dad.externalResources) {
			try {
				val targetFolder = webPath
					.resolve(extResource.subfolder)
					.createDirectories()
				for (resource: extResource.resources) {
					val sourceDir = projectPath.resolve(resource).toFile
					val targetDir = targetFolder.toFile
					println("Copy " + sourceDir + " to " + targetDir)
					FileUtils.copyDirectory(sourceDir, targetDir)
				}
			}
			catch (Exception e) {
				e.printStackTrace()
			}
		}
		
		// Copy favicon
		if (genctx.dad.faviconDirectory !== null && !genctx.dad.faviconDirectory.isEmpty) {
			try {
				val targetFolder = webPath
					.resolve("favicon")
					.createDirectories()
				val resource = genctx.dad.faviconDirectory
				val sourceDir = projectPath.resolve(resource).toFile
				val targetDir = targetFolder.toFile
				println("Copy " + sourceDir + " to " + targetDir)
				FileUtils.copyDirectory(sourceDir, targetDir)
			}
			catch (Exception e) {
				e.printStackTrace()
			}
		}
		
	}

	def private void printErr(Exception e, GraphModel g) {
		println('''Error in «genctx.guiExtension.graphModelFileName(g)»''')
		throw new RuntimeException(e)
	}

	/** 
	 * Generate a Dart file containing dart classes for each type defined in the Data model
	 * @param data
	 * @throws IOException
	 */
	def private void generateDataModel(Data data, GenerationContext genctx) {
		val contents = new DartDataGenerator().create(data).toString()
		val fileName = libPath.resolve("data", '''«data.name».dart''')
		createFile(contents, fileName, data.lastModified2)
	}

	def private boolean combineFiles(Iterable<String> projectRelativePaths, Path targetPath, DAD dad) {
		if (projectRelativePaths.isEmpty) {
			return false
		}
	   	val projectPath = dad.projectPath
		val stringBuilder = new StringBuilder
		for (relativePath: projectRelativePaths) {
			try {
				val absolutePath = projectPath.resolve(relativePath)
				val content = absolutePath.readString()
				stringBuilder.append(content)
				stringBuilder.append("\n")
			}
			catch (IOException e) {
				System.err.println('''Could not read file at "«relativePath»". The file will be skipped.''')
				e.printStackTrace()
			}
		}
		try {
			targetPath.parent.createDirectories()
			targetPath.writeString(stringBuilder)
			return true
		}
		catch (IOException e) {
			e.printStackTrace()
			return false
		}
	}
	
	/** 
	 * Creates the needed root files for the Angular app.
	 * 
	 * @throws IOException
	 */
	def private void generateApp(Collection<GUIPlugin> guiPlugins,
		                         Set<String> services,
		                         String timeStamp,
		                         GenerationContext genctx,
		                         long dadChanged) throws IOException {
		
		// Combine files
		val Set<String> cssFiles = newHashSet
		val Set<String> preJSFiles = newHashSet
		val Set<String> postJSFiles = newHashSet
		
		
		cssFiles += genctx
			.dad
			.additionalStylesheets
			.reject[isHTTP]
		
		cssFiles += guiPlugins
			.map[function]
			.filter(Function)
			.map[eContainer]
			.filter(Plugin)
			.reject(DartPlugin)
			.map[style]
			.filterNull
			.flatMap[files]
			.map[path]
			.reject[isHTTP]
		
		preJSFiles += genctx
			.dad
			.preScripts
			.reject[isHTTP]
		
		postJSFiles += guiPlugins
			.map[function]
			.filter(Function)
			.map[eContainer]
			.filter(Plugin)
			.reject(DartPlugin)
			.map[script]
			.filterNull
			.flatMap[files]
			.map[path]
			.reject[isHTTP]
		
		postJSFiles += genctx
			.dad
			.postScripts
			.reject[isHTTP]
			
		postJSFiles += genctx
			.usedGUIs
			.flatMap[additionalJavaScript]
			.reject[isHTTP]
		
		// index.html
		val index = new HTMLDartIndexTemplate().create(
			genctx.dad.appName,
			guiPlugins,
			timeStamp,
			cssFiles.combineFiles(webPath.resolve("css", "combined", "combined.css"), genctx.dad),
			preJSFiles.combineFiles(webPath.resolve("js", "precombined", "precombined.js"), genctx.dad),
			postJSFiles.combineFiles(webPath.resolve("js", "postcombined", "postcombined.js"), genctx.dad)
		)
		createFile(index, webPath.resolve("index.html"), 0)
		
		// main.dart
		val main = new AngularDartMainTemplate().create()
		createFile(main, webPath.resolve("main.dart"), dadChanged)
		
		// app.dart
		val app = new AngularDartAppTemplate().create(genctx.dad)
		createFile(app, libPath.resolve("app.dart"), dadChanged)
		
		new DyWADartGenerator().generate(libPath)
		
		// Create login form
		if (!genctx.dad.loginComponents.isEmpty) {
			generateLogin()
			println("[INFO] Custom Login GUI")
		}
		
		// Generate root prcesses
		for (process: genctx.rootProcesses) {
			process.generateRootProcesses(genctx)
		}
		
	}
	
	/** 
	 * Creates an input and an output dart file for each GUI model, Native
	 * Front-EndSIB and GUI Events, placed in the given visible process cip.
	 */
	def private void generateProcessDartTOs(Process process, GenerationContext genctx) {
		val dartGenerator = new DartTOGeneratorHelper()
		for (nfSIB : process.allNodes.filter(NativeFrontendSIBReference)) {
			val nfSibId = nfSIB.id.escapeDart
			
			// Inputs
			val inputTypeViewMap = nfSIB.buildInputTypeViewMap(genctx.guiExtension)
			val inputs = dartGenerator.generateNativeFrontEndSIBInputsDart(nfSIB, inputTypeViewMap, genctx.usedDatas, genctx.guiExtension).toString()
			val inputsFileName = '''«nfSIB.simpleTypeNameDart»«nfSibId»Input.dart'''
			DyWAAbstractGenerator.generate(inputs, ".rest.native.", inputsFileName, libPath, process.lastModified2)
			
			// Outputs
			val outputs = dartGenerator.generateNativeFrontEndSIBOutputsDart(nfSIB, genctx.getUsedDatas(), genctx.guiExtension).toString()
			val outputsFileName = '''«nfSIB.simpleTypeNameDart»«nfSibId»Branch.dart'''
			DyWAAbstractGenerator.generate(outputs, ".rest.native.", outputsFileName, libPath, process.lastModified2)
		}
		println('''[INFO] Visible Process «process.modelName» Dart TOs generated''')
	}
	
	/** 
	 * Generates the routable wrapper Angular component for a given  process,
	 * placed in the application DAD.
	 * @throws IOException
	 */
	def private void generateProcesses(Process process, GenerationContext genctx, boolean isInDAD) {
		val adrt = new AngularDartDADProcessTemplate()
		val routable = adrt.create(process)
		val fileName = AngularDartDADProcessTemplate.fileName(process)
		createFile(routable, dadPath.resolve(fileName), process.lastModified2)
		
		new SelectiveInteractableProcessGenerator(backendProcessGeneratorHelper)
			.generate(process, dyawApp, genctx, isInDAD)
	}

	/** 
	 * Generates the routable wrapper Angular component for a given interaction process.
	 * @throws IOException
	 */
	def private void generateRootProcesses(Process process, GenerationContext genctx) {
		val adrt = new AngularDartRootProcessTemplate()
		val routable = adrt.create(process)
		val fileName = AngularDartRootProcessTemplate.fileName(process)
		createFile(routable, processRootPath.resolve(fileName), process.lastModified2)
		
		// Process Dart File
		val adpt = new AngularDartProcessTemplate()
		val processDart = adpt.create(process)
		createFile(processDart, processPath.resolve('''«adpt.className(process)».dart'''), process.lastModified2)
		
		val contents = new DartTOGeneratorHelper()
			.generateRootProcessInputsDart(process, genctx.guiExtension)
		DyWAAbstractGenerator.generate(contents, ".rest.process.", '''«process.simpleTypeNameDart»Input.dart''', libPath, process.lastModified2)
	}

	def private void generateLogin() {
		val extension adlc = new AngularDartLoginComponent()
		val gui = genctx.dad.loginComponents.head.model
		val ngComponent = gui.createComponent.toString()
		createFile(ngComponent, libPath.resolve("login", "Login.dart"), 0)
		val ngHTMLComponent = gui.createHTML.toString()
		createFile(ngHTMLComponent, libPath.resolve("login", "Login.html"), 0)
	}
	
	/** 
	 * Generates the GUI model Angular component class and HTML file for a
	 * given GUI model. For every found form, table and GUI plug in component
	 * placed in the GUI model another specific Angular component class and
	 * HTML file are generated. In addition to this, the used images, css and
	 * js files are copied to the app and if the current user is used in the
	 * GUI model, a user Angular service is generated as well.
	 * @throws IOException
	 */
	def private void generateGUI(GUI gui, Map<String, GUIPlugin> guiPLugins, Set<String> services, GenerationContext genctx) {
		
		// Selective-View
		val gcv = genctx.getCompoundView(gui)
		
		// GUI Inputs / Outputs
		{
			val extension dartGenerator = new DartTOGeneratorHelper()
			val guiInputs = gui.generateGUIInputsDart(gcv, genctx.usedDatas)
			DyWAAbstractGenerator.generate(guiInputs, ".rest.gui.", '''«gui.simpleTypeNameDart»Input.dart''', libPath, gui.lastModified2)
			val guiOutputs = gui.generateGUIOutputsDart(gcv, genctx.usedDatas, genctx.guiExtension)
			DyWAAbstractGenerator.generate(guiOutputs, ".rest.gui.", '''«gui.simpleTypeNameDart»Branch.dart''', libPath, gui.lastModified2)
		}
		
		// NG-Components
		{
			val extension adct = new AngularDartComponentTemplate()
			val ngComponent = gui.create(gcv)
			createFile(ngComponent, guiPath.resolve('''«gui.GUIName.escapeDart».dart'''), gui.lastModified2)
			
			val htmlComponent = new AngularComponentHTMLTemplate().create(gui)
			createFile(htmlComponent, guiPath.resolve('''«gui.GUIName.escapeDart».html'''), gui.lastModified2)
			println('''[INFO] GUI «gui.title» Component generated''')
		}
		
		// File guard compounds
		{
			val extension dfgg = new DartFileGuardGenerator()
			if (gui.isFileGuardPresent) {
				val fileGuard = gui.generate(gcv, genctx.usedDatas)
				createFile(fileGuard, libPath.resolve("models", gui.fileName), gui.lastModified2)
				new DownloadTokenGenerator().generate(gui, gcv, dyawApp, genctx)
			}
		}
		
		// Security SIBs
		{
			val extension adsst = new AngularDartSecuritySIBTemplate()
			for (sib: genctx.guiExtension.find(gui, SecuritySIB)) {
				val process = sib.proMod
				val component = sib.createComponent(process, gcv)
				createFile(component, libPath.resolve("security", '''«sib.component».dart'''), process.lastModified2)
			}
		}
		
		{
			val extension adgpt = new AngularDartGUIPluginTemplate()
			
			// Additional CSS and JS
			gui.copyResources(guiPath)
	
			// GUI Plugins
			for (cgp: genctx.guiExtension.find(gui, GUIPlugin)) {
				if (!guiPLugins.containsKey(cgp.id)) {
					guiPLugins.put(cgp.id, cgp)
				}
				val guiPluginComponent = cgp.createComponents(gcv)
				createFile(guiPluginComponent, pluginPath.resolve('''«cgp.pluginClassName».dart'''), gui.lastModified2)
				val guiPluginHTML = cgp.createHTML
				createFile(guiPluginHTML, pluginPath.resolve('''«cgp.pluginClassName».html'''), gui.lastModified2)
				println('''[INFO] GUI «gui.title» Plugin Component generated''')
				cgp.copyResources(pluginPath)
			}
		}
		
		// GUI Image
		for (img: genctx.guiExtension.find(gui, Image)) {
			new HTMLImageTemplate().copyImage(img, webPath)
		}
//		println('''[INFO] GUI «gui.title» Images moved''')
		
		// Table-Components
		{
			val extension adtt = new AngularDartTableTemplate()
			for (table: genctx.guiExtension.find(gui, Table)) {
				val tablePath = libPath.resolve("tables", gui.title.toFirstUpper)
				val tableComponentName = table.getTableName(gui)
				val tableComponent = table.create(gui, gcv)
				val tableComponentPath = tablePath.resolve('''«tableComponentName».dart''')
				createFile(tableComponent, tableComponentPath, gui.lastModified2)
				val tableHtmlComponent = new AngularTableHTMLTemplate().create(table, gui)
				val tableComponentHtmlPath = tablePath.resolve('''«tableComponentName».html''')
				createFile(tableHtmlComponent, tableComponentHtmlPath, gui.lastModified2)
			}
//			println('''[INFO] GUI «gui.title» Tables generated''')
		}
		
		// Form-Components
		{
			val extension adft = new AngularDartFormTemplate()
			for (form: genctx.guiExtension.find(gui, Form)) {
				val formPath = libPath.resolve("forms", gui.title.toFirstUpper)
				val formComponentName = form.getFormName(gui)
				val formComponent = form.create(gui, gcv)
				val formComponentPath = formPath.resolve('''«formComponentName».dart''')
				createFile(formComponent, formComponentPath, gui.lastModified2)
				val formHtmlComponent = new AngularFormHTMLTemplate().create(form, gui)
				val formHtmlComponentPath = formPath.resolve('''«formComponentName».html''')
				createFile(formHtmlComponent, formHtmlComponentPath, gui.lastModified2)
			}
//			println('''[INFO] GUI «gui.title» Forms generated''')
		}
		
		// User Service
		val currentUserVar = new AngularDartCommonImports().getCurrentUser(gui) as ComplexVariable
		if (currentUserVar !== null) {
			// Find TypeView for current User
			val ctv = gcv.pairs.get(currentUserVar) as ComplexTypeView
			if (ctv !== null) {
				val userService = new AngularDartCurrentUserService().create(ctv)
				val serviceName = AngularDartCurrentUserService.getName(ctv)
				services += serviceName.toString
				createFile(userService, servicePath.resolve('''«serviceName»Service.dart'''), gui.lastModified2)
//				println('''[INFO] GUI «gui.title» User Service generated''')
			}
		}
		
		println('''[INFO] GUI «gui.title» Selective TOs generated''')
		
	}

	/** 
	 * Generated the needed compound classes for all security SIBs placed in a GUI model
	 * @param cip
	 * @throws IOException
	 */
	def private void generateGUIProcess(Process p, GenerationContext genctx) {
		// Dart Inputs
		val securityProcessInputs = new DartTOGeneratorHelper().generateSecurityProcessInputsDart(p, genctx.usedDatas).toString()
		DyWAAbstractGenerator.generate(securityProcessInputs, ".security.", '''«p.simpleTypeNameDart»Input.dart''', libPath, p.lastModified2)
		val backendGenerator = new SelectiveInteractableProcessGenerator(backendProcessGeneratorHelper)
		for (gcv: genctx.compoundViews) {
			for (processSib: genctx.guiExtension.find(gcv.GUI, ProcessSIB)) {
				val Map<InputPort, TypeView> typeMap = newHashMap
				if (processSib.proMod.equals(p)) {
					for (inputPort: processSib.inputPorts) {
						// TODO Static ports
						if (!inputPort.incoming.isEmpty) {
							val node = inputPort.incoming.head.sourceElement
							switch (portView: gcv.pairs.get(node)) {
								PrimitiveFieldView: typeMap.put(inputPort, portView.primitiveTypeView)
								ComplexFieldView:   typeMap.put(inputPort, portView.view)
								TypeView:           typeMap.put(inputPort, portView)
								default:            throw new ClassCastException('''Unhandled type: «portView.class.name»''')
							}
						}
						else {
							typeMap.put(inputPort, null)
						}
					}
					backendGenerator.generate(p, typeMap, dyawApp, processSib, genctx)
				}
			}
		}
		println('''[INFO] Security SIB Interactable «p.modelName» REST Service generated''')
	}
	
	/** 
	 * Helper method to create a file with the given content on the given path.
	 * @throws IOException
	 */
	def private void createFile(CharSequence content, Path path, long modified) {
		if (path.exists && modified > 0 && path.lastModifiedTime.toMillis >= modified) {
			return
		}
		try {
			path.toFile.parentFile.mkdirs
			path.writeString(content ?: '')
		}
		catch (IOException e) {
			e.printStackTrace()
		}
	}

	def private void fillSelectiveCache() {
		val cache = genctx.collectComplexTypeViews.filter(BaseComplexTypeView).toList
		for (gui: genctx.usedGUIs) {
			val Set<Type> typeCache = newHashSet
			val Set<BaseComplexTypeView> basicCache = newHashSet
			val btvs = genctx.computeBranchTypeViews(gui, typeCache, basicCache)
			val ptvs = gui.computeProcessTypeViews(typeCache)
			for (e: btvs.values) {
				for (tvs: e.values) {
					cache.addIfBaseComplexTypeView(tvs)
				}
			}
			for (e : ptvs.values) {
				for (tvs: e.values) {
					cache.addIfBaseComplexTypeView(tvs)
				}
			}
			cache.addIfBaseComplexTypeView(basicCache)
		}
		for (c: cache) {
			genctx.selectiveCache.registerTypeView(c)
		}
		for (t: genctx.usedDatas.flatMap[types]) {
			genctx.selectiveCache.registerBlankTypeView(t)
		}
		genctx.selectiveCache.printStats()
	}

	def private void addIfBaseComplexTypeView(List<BaseComplexTypeView> target, Iterable<? extends TypeView> source) {
		val superSelective = DyWAAbstractGenerator.getCommonSuperTypeView(source)
		if (superSelective instanceof BaseComplexTypeView) {
			target.add(superSelective)
		}
	}
	
}
