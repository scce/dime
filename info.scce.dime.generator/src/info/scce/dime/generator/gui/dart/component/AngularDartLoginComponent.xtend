/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.dart.component

import info.scce.dime.generator.gui.dart.base.AngularDartIncludedTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.TemplateHelper
import info.scce.dime.gui.gui.GUI

class AngularDartLoginComponent extends GUIGenerator{
	
	def createComponent(GUI gui)
	'''
	import 'dart:html';
	
	import 'package:angular/angular.dart';
	import 'dart:async';
	import 'package:app/src/core/dime_process_service.dart';
	import 'package:app/src/services/AuthService.dart';
	«new AngularDartIncludedTemplate().createImports(gui)»
	
	@Component(
			selector: 'login-form',
			directives: const [
				coreDirectives,«TemplateHelper.classFormat(new AngularDartComponentTemplate().getGUIName(gui).toString).toFirstUpper»
			],
			templateUrl: 'Login.html'
	)
	class Login{
	
		@Output('signedin')
		Stream<dynamic> get evt_signedin => signedin.stream;
		
		final StreamController<dynamic> signedin = new StreamController<dynamic>();
		
		@Input()
		bool modal = false;
		
		bool notCorrect = false;
		
		final AuthService authService;
			
		Login(this.authService);
			
		// Triggered on Form Submit
		void submit(Map formValues) async {
		  var username = formValues['username'];
		  var password = formValues['password'];
		  var correct = await authService.auth(username, password);
		  notCorrect = !correct;
		  if (correct) {
		    signedin.add(true);
		  }
		}
	
	}
	
	'''
	
	def createHTML(GUI gui)
	'''
	<«TemplateHelper.tagFormat(gui.title)»
		[modalDialog]="modal"
	  	[currentbranch]="'_login_'"
	  	[ismajorpage]="true"
	  	[notCorrect]="notCorrect"
	  	(«ConventionHelper.getEventName("login")»)="submit($event)"
	  >
	  </«TemplateHelper.tagFormat(gui.title)»>
	'''
	
}
