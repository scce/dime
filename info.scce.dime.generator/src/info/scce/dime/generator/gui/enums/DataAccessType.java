/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.enums;

/**
 * The data access type defines for specific components and control edges
 * how the variables and their attributes can be accessed.
 * 
 * TABLE: should be used for variables and attributes used in relation to a table component
 * FORM: should be used for variables and attributes used in relation to a form component
 * CHOICE: should be used for variables and attributes used in relation to one of the selection components like radio, combobox or checkbox
 * FOR: should be used for variables and attributes connected by an iteration edge
 * IF: should be used as default
 * @author zweihoff
 *
 */
public enum DataAccessType {
	TABLE, FORM, IF, FOR, CHOICE
}
