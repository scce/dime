/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import info.scce.dime.gui.gui.Badge;
import info.scce.dime.gui.gui.Button;
import info.scce.dime.gui.gui.Descriptionentry;
import info.scce.dime.gui.gui.Dropdown;
import info.scce.dime.gui.gui.Embedded;
import info.scce.dime.gui.gui.Field;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.FormEntry;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.GUIPlugin;
import info.scce.dime.gui.gui.GUISIB;
import info.scce.dime.gui.gui.Headline;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.gui.Input;
import info.scce.dime.gui.gui.Iteration;
import info.scce.dime.gui.gui.Listentry;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.Panel;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.gui.gui.Select;
import info.scce.dime.gui.gui.TODOList;
import info.scce.dime.gui.gui.Tab;
import info.scce.dime.gui.gui.Table;
import info.scce.dime.gui.gui.TableEntry;
import info.scce.dime.gui.gui.Text;
import info.scce.dime.gui.gui.TextInputStatic;
import info.scce.dime.gui.helper.GUIExtension;
/**
 * The expression equalizer is used to match expressions to
 * variables and their attributes present in the data contexts
 * of a given GUI model.
 * This information is used to check, if a variable or attribute
 * is used by an expression
 * @author zweihoff
 *
 */
public class ExpressionEqualizer {
	
	/**
	 * Bit-vector to store which expression has been matched to a variable
	 * or attribute
	 */
	private Map<Expression,Boolean> expressions = new HashMap<>();

	/**
	 * Initializes the expression equalizer and
	 * collects all expressions from all available
	 * movable container placed in the given GUI model
	 * @param gui
	 */
	public ExpressionEqualizer(Iterable<MovableContainer> moveableContainers) {
		moveableContainers.forEach(n->collect(n));
	}
	
	public Map<Expression, Boolean> getExpressions() {
		return expressions;
	}

	/**
	 * Checks, if a given (partial) expression is already
	 * known. If so, the expression is marked as matched in
	 * the bit vector
	 * @param s
	 * @return
	 */
	public boolean isUsed(String s)
	{
		for(Entry<Expression, Boolean> entry:expressions.entrySet())
		{
			if(entry.getKey().getExpression().startsWith(s)){
				if(entry.getKey().equals(s)){
					entry.setValue(true);
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns the component, which creates the next surrounding scope
	 * by an iteration or table edge. If no scope is present, null
	 * @param cm
	 * @return
	 */
	private MovableContainer getParentScopeComponent(MovableContainer cm){
		if(!cm.getIncoming(Iteration.class).isEmpty()) return cm;
		if(cm.getContainer() != null)
		{
			if(cm.getContainer() instanceof MovableContainer){
				if(cm.getContainer() instanceof Table)return (MovableContainer) cm.getContainer();
				return getParentScopeComponent((MovableContainer) cm.getContainer());
			}
		}
		return null;
	}
	
	/**
	 * Collects all expressions, present in any attribute of the given component
	 * @param cm
	 */
	private void collect(MovableContainer cm)
	{
		
		if(cm.getGeneralStyle()!=null){
			collectExpression(cm.getGeneralStyle().getRawContent(),cm);				
			if(cm.getGeneralStyle().getTooltip()!= null){
				collectExpression(cm.getGeneralStyle().getTooltip().getContent().stream().map(n->n.getRawContent()).collect(Collectors.toList()),cm);				
			}
		}
		if(cm instanceof TODOList){
			collectExpression(((TODOList) cm).getEmptyValue(),cm);
			collectExpression(((TODOList) cm).getCaption(),cm);
		}
		if(cm instanceof Headline){
			collectExpression(((Headline) cm).getContent().stream().map(n->n.getRawContent()).collect(Collectors.toList()),cm);
		}
		if(cm instanceof Text){
			collectExpression(((Text) cm).getContent().stream().map(n->n.getRawContent()).collect(Collectors.toList()),cm);
		}
		if(cm instanceof File){
			collectExpression(((File) cm).getLabel(),cm);
		}
		if(cm instanceof Listentry){
			collectExpression(((Listentry) cm).getContent().stream().map(n->n.getRawContent()).collect(Collectors.toList()),cm);
		}
		if(cm instanceof Tab){
			collectExpression(((Tab) cm).getLabel(),cm);
		}
		if(cm instanceof Descriptionentry){
			collectExpression(((Descriptionentry) cm).getContent().stream().map(n->n.getRawContent()).collect(Collectors.toList()),cm);
			collectExpression(((Descriptionentry) cm).getDescription().stream().map(n->n.getRawContent()).collect(Collectors.toList()),cm);
		}
		if(cm instanceof TableEntry){
			collectExpression(((TableEntry) cm).getLabel(),cm);
		}
		if(cm instanceof Table){
			if(((Table) cm).getRowStyle()!=null){
				collectExpression(((Table) cm).getRowStyle().getRawContent(),cm);
			}
		}
		if(cm instanceof Form){
			collectExpression(((Form) cm).getErrorMessage(),cm);
		}
		if(cm instanceof FormEntry){
			if(cm instanceof Button){
				collectExpression(((Button) cm).getLabel(),cm);				
			}
			if(cm instanceof Field){
				collectExpression(((Field) cm).getLabel(),cm);				
			}
			if(cm instanceof Select){
				collectExpression(((Select) cm).getLabel(),cm);				
			}
		}
		if(cm instanceof Input){
			if(((Input) cm).getAdditionalOptions()!=null){
				collectExpression(((Input) cm).getAdditionalOptions().getHelpText(),cm);				
				collectExpression(((Input) cm).getAdditionalOptions().getEmptyValue(),cm);
			}
			if(((Input) cm).getValidation()!=null){
				collectExpression(((Input) cm).getValidation().getErrorText(),cm);
			}
		}
		if(cm instanceof Button){
			collectExpression(((Button) cm).getDisplayLabel(),cm);
		}
		if(cm instanceof Badge){
			collectExpression(((Badge) cm).getContent().stream().map(n->n.getRawContent()).collect(Collectors.toList()),cm);
		}
		if(cm instanceof Embedded){
			collectExpression(((Embedded) cm).getSource(),cm);
		}
		if(cm instanceof Panel){
			collectExpression(((Panel) cm).getDefaultOpen(), cm);
			if(((Panel)cm).getHeading() != null) {
				collectExpression(((Panel) cm).getHeading().getRawContent(),cm);
			}
			if(((Panel)cm).getFooter() != null) {
				collectExpression(((Panel) cm).getFooter().getRawContent(),cm);
			}
			
			
		}
		if(cm instanceof Image){
			collectExpression(((Image) cm).getPath(),cm);
		}
		if(cm instanceof Dropdown){
			collectExpression(((Dropdown) cm).getLabel(),cm);
		}
		if(cm instanceof GUISIB){
			GUISIB sib = (GUISIB) cm;
			collectExpression(sib.getName(), cm);
			collectExpression(sib.getIOs().stream().filter(n->n instanceof TextInputStatic).map(n->((TextInputStatic) n).getValue()).collect(Collectors.toList()) ,cm);
		}
		if(cm instanceof GUIPlugin){
			GUIPlugin sib = (GUIPlugin) cm;
			collectExpression(sib.getIOs().stream().filter(n->n instanceof TextInputStatic).map(n->((TextInputStatic) n).getValue()).collect(Collectors.toList()) ,cm);
		}
		if(cm instanceof ProcessSIB){
			ProcessSIB sib = (ProcessSIB) cm;
			collectExpression(sib.getIOs().stream().filter(n->n instanceof TextInputStatic).map(n->((TextInputStatic) n).getValue()).collect(Collectors.toList()) ,cm);
		}
	}
	
	/**
	 * Collects the expressions from a list of string
	 * @param expr
	 * @param cmc
	 */
	private void collectExpression(List<String> expr,MovableContainer cmc)
	{
		if(expr.isEmpty())return;
		expr.forEach(n->collectExpression(n,cmc));
	}
	
	/**
	 * Collects the expressions from a given string, found in one of the attributes
	 * of the given component
	 * @param expr
	 * @param cmc
	 */
	private void collectExpression(String expr,MovableContainer cmc)
	{
		if(expr == null)return;
		Pattern p = Pattern.compile("\\{\\{(.+?)\\}\\}");
		Matcher m = p.matcher(expr);
		while(m.find()){
			int start = m.start();
			int end = m.end();
		    String exprValue = expr.substring(start+2, end-2).trim();
		    if(exprValue.indexOf("|")>-1){
			    	exprValue = exprValue.substring(0, exprValue.indexOf("|")); 	
		    }
		    while(exprValue.indexOf("[")>-1){
		    		int startIndx = exprValue.indexOf("[");
		    		int endIndx = exprValue.indexOf("]");
		    		String startPart = "";
		    		if(startIndx>0){
		    			startPart = exprValue.substring(0, startIndx-1);
		    		}
		    		String endPart = "";
		    		if(endIndx<exprValue.length()-1){
		    			endPart = exprValue.substring(endIndx+1, exprValue.length());
		    		}
		    		exprValue = startPart + endPart;
		    }
		    exprValue = removeKeyWords(exprValue);
		    String finalXPBValue = exprValue.trim();
		    if(!this.expressions.entrySet().stream().filter(n->n.getKey().getExpression().equals(finalXPBValue)).findAny().isPresent()){
		    	Expression xp = new Expression();
		    		xp.setExpression(finalXPBValue);
		    		xp.setScope(getParentScopeComponent(cmc));
		    		this.expressions.put(xp,false);
		    }
		}
	}
	
	/**
	 * Removes parts of the given expression, which has not to be considered during the
	 * matching process
	 * @param exp
	 * @return
	 */
	private String removeKeyWords(String exp)
	{
		String[] keywords = {"dywa_name","get","dywa_id","dywa_version","size"};
		for(String key:keywords) {
			exp.replaceAll("."+key, "");			
		}
		return exp;
	}
}
