/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.data

import graphmodel.ModelElement
import info.scce.dime.generator.gui.rest.model.Parent
import java.util.HashMap
import java.util.LinkedList

class SelectiveMap {
	
	private HashMap<String, Parent> map = newHashMap
	private HashMap<String, ModelElement> elements = newHashMap
	
	
	def containsKey(ModelElement elm) {
		map.containsKey(elm.id)
	}
	
	def get(ModelElement elm) {
		map.get(elm.id)
	}
	
	def getElements() {
		new LinkedList(elements.values)
	}
	
	def keySet(){
		map.keySet
	}
	
	def entrySet(){
		map.entrySet
	}
	
	def put(ModelElement elm, Parent value)
	{
		val id = elm.id
		if(this.containsKey(elm)){
			val preValue = map.get(id)
			if (preValue.id != value.id){
				println("Adding already known key: "+id+"\npre:"+preValue+"\n replacedby: "+value+"\n---")
			}
			if (map.remove(id) == null){
				throw new IllegalStateException("Could not remove contained key "+id)
			}
			map.put(id,value)
			elements.put(id,elm)
			return preValue
		}
		elements.put(id,elm)
		return map.put(id,value)
	}
	
}
