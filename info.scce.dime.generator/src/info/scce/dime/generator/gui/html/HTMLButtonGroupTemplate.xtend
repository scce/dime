/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import graphmodel.Node
import info.scce.dime.generator.gui.enums.HTMLSizeTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.ButtonGroupAlignment
import info.scce.dime.gui.gui.ButtonOptions
import info.scce.dime.gui.gui.Dropdown
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.Size
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template for the button group component Angular HTML template content
 */
class HTMLButtonGroupTemplate extends GUIGenerator{
	
	//TODO: restructure into dispatch
	/**
	 * Generates the button group component Angular HTML template code
	 */
	def create(ButtonGroup buttonGroup){
		var html = this.pre(buttonGroup).toString;
		for(Node node:ElementCollector.getElementsH(buttonGroup.allNodes)){
			if(node instanceof Button){
				var button = new HTMLButtonTemplate();
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += node.preButton();
				}
				html += button.create(node);
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += postButton;
				}
			}
			if(node instanceof LinkProcessSIB){
				var button = new HTMLLinkSIBTemplate();
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += node.preButton();
				}
				html += button.create(node);
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += postButton;
				}
			}
			if(node instanceof Dropdown){
				var dropdown = new HTMLDropdownTemplate();
				html += node.preButton();
				html += dropdown.create(node);
				html += postButton;
			}
		}
		html += this.post(buttonGroup);
		return html;
	}
	
	/**
	 * Checks if a button should be displayed as a link
	 */
	def boolean getIsIsLink(ButtonOptions options){
		if(options != null)return options.isIsLink;
		return false;
	}
	
	
	
	private def preButton(MovableContainer button)
	'''<div class="btn-group" «button.printNgIf» role="navigation" aria-label="button group">'''
	
	private def postButton()
	'''</div>'''
	
	private def pre(ButtonGroup buttonGroup)
	'''
	<!-- beginn button group -->
	<div «buttonGroup.printNgIfFor»«buttonGroup.printStyle» class="«getAlignment(buttonGroup)»«getSize(buttonGroup)»" role="navigation" aria-label="button group">
	'''
	
	private def post(ButtonGroup buttonGroup)
	'''
	</div>
	<!-- end button group -->
	'''
	
	/**
	 * Generates the CSS classes for the defined size
	 */
	private def getSize(ButtonGroup buttonGroup){
		if(buttonGroup.size == Size.DEFAULT || buttonGroup.alignment == ButtonGroupAlignment.JUSTIFIED)return "";
		return " btn-group-"+ HTMLSizeTemplate.create(buttonGroup.size);
	}
	
	/**
	 * Generates the CSS classes for the defined alignment
	 */
	private def getAlignment(ButtonGroup buttonGroup){
		if(buttonGroup.alignment == ButtonGroupAlignment.VERTICAL)return " btn-group-vertical"
		if(buttonGroup.alignment == ButtonGroupAlignment.JUSTIFIED)return " btn-group btn-group-justified"
		return " btn-group"
	}
}
