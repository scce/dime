/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.util.DimeIOExtension
import graphmodel.Node
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.rest.DartFileGuardGenerator
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import java.nio.file.Path

/**
 * Template to generate the Angular component HTML template code for a image component
 */
class HTMLImageTemplate extends GUIGenerator {
	
	extension DimeIOExtension = new DimeIOExtension
	
	/**
	 * Generates the Angular component HTML template code for a image component
	 */
	def create(Image image, boolean considerLink) {
		
		val button = image.findThe(Button)
		val parameters = HTMLFileTemplate
			.getParameters(image)
			.map[ n | DataDartHelper.getBindedDataName(n, DataAccessType.^FOR, MODE.GET) ]
			
		return '''
			«IF button !== null && considerLink»
				<a «button.printStyleWith(null)»
				«IF !preview»(click)="«new HTMLButtonTemplate().getAction(button, button.findFirstParent(Form))»"«ENDIF»
				role="button"«IF button.isDisabled» disabled«ENDIF»>
			«ENDIF»
			<img
				«image.printNgIfFor»
				«image.printStyleWith(image.getSize(null,null))»
				alt="«image.rootElement.title» image"
				class="«getClass(image)»«IF image.styling!=null»«IF image.styling.responsive» img-responsive«ENDIF»«ENDIF»"
				«IF image.path.nullOrEmpty»
					«IF parameters.empty»
						[src]="getRestBaseUrl() + '/«new DartFileGuardGenerator().getImageURL(image)»'«IF !image.guardSIBs.get(0).cachable» + '?cache=' + imageHash.toString()«ENDIF» | authImage | async"
					«ELSE»
						[src]="getRestBaseUrl() + '/«new DartFileGuardGenerator().getImageURL(image)»' + load«image.id.escapeDart»Image(«parameters.join(",")»)«IF !image.guardSIBs.get(0).cachable» + '&cache=' + imageHash.toString()«ENDIF» | authImage | async"
					«ENDIF»
				«ELSEIF preview && !image.path.containsExpression»
					src="«if (image.path.isHTTP) image.path else image.projectPath.resolve(image.path)»"
				«ELSEIF !image.path.containsExpression»
					src="«image.getStaticImagePath(false)»"
				«ELSEIF image.guardSIBs.empty»
					[src]="'«ConventionHelper.replaceDataBindingString(image.path)»' | authImage | async"
				«ENDIF»
			>
			«IF button !== null && considerLink»
				</a>
			«ENDIF»
		'''
	}
	
	def boolean getIsText(Node node) {
		switch (node) {
			PrimitiveAttribute: node.attribute.dataType == PrimitiveType.TEXT
			PrimitiveVariable:  node.dataType == info.scce.dime.gui.gui.PrimitiveType.TEXT
			default:            false
		}
	}
	
	/**
	 * Returns the CSS class for the image styling type constant
	 */
	private def getClass(Image image) {
		switch (image.styling?.border) {
			case null:      "img"
			case CIRCLE:    "img-circle"
			case ROUNDED:   "img-rounded"
			case THUMBNAIL: "img-thumbnail"
			default:        "img"
		}
	}
	
	/**
	 * Returns the static image path. This can either be a URL or a local resource which is moved to the server
	 */
	private def String getStaticImagePath(Image image, boolean preview) {
		if (image.path.isHTTP || preview) {
			return image.path
		}
		val sourceFileName = image.path.toNIOPath.fileName
		return newPath("img").resolve(image.rootElement.title).resolve(sourceFileName).toString
		
	}
	
	/**
	 * Copies an image from the specified path to the application resources
	 * which will be moved to the server
	 */
    def copyImage(Image image, Path resourcePath) {
		if (image.path.nullOrEmpty || image.path.isHTTP || image.path.containsExpression) {
			return
		}
		val sourceFile = image.projectPath.resolve(image.path)
		val targetFile = resourcePath
			.resolve("img")
			.resolve(image.rootElement.title)
			.resolve(sourceFile.fileName)
		targetFile.parent.createDirectories()	
		sourceFile.copy(targetFile)		
	}
	
	private def boolean containsExpression(String path) {
		path.trim.startsWith("{{") && path.trim.endsWith("}}")
	}
	
	/**
	 * Returns the width and height CSS properties depended on the given image component
	 */
	private def getSize(Image image, String encapsulation, String separator) {
		val e = if (encapsulation === null) "" else encapsulation
		val s = if (separator === null) ";" else separator
		
		var css = ""
		if (image.dimensionX > 0) {
			css += ''' «e»width«e»:«e»«image.dimensionX»px«e»«s»'''
		}
		else if (image.dimensionX == 0) {
			// Do nothing
		}
		else {
			css += ''' «e»width«e»:«e»100%«e»«s»'''
		}
		
		if (image.dimensionY > 0) {
			css += ''' «e»height«e»:«e»«image.dimensionY»px«e»«s»'''
		}
		else if (image.dimensionY == 0) {
			// Do nothing
		}
		else {
			css += ''' «e»height«e»:«e»100%«e»«s»'''
		}
		
		return css
	}
}
