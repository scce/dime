/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.ButtonToolbar
import info.scce.dime.gui.gui.GuiFactory
import graphmodel.Node

/**
 * Template for the button tool bar component Angular HTML template content
 */
class HTMLButtonToolbarTemplate extends GUIGenerator {
	
	ButtonToolbar toolbar
		
	new(ButtonToolbar toolbar) {
		super()
		this.toolbar = toolbar
	}
	
	/**
	 * Generates the button tool bar component Angular HTML template code
	 */
	def create() {
		switch toolbar.role {
			case MENU: createMenu
			case BREADCRUM: createBreadcrum
			default: createDefault
		}
	}
	
	/**
	 * Generates the tool bar default component Angular HTML template code
	 */
	private def createDefault()
	'''
	<!-- beginn button tool bar -->
	<div «ngIfFor» «style» class="btn-toolbar" role="menubar" aria-label="button group">
		«toolbar.allNodes.filter(ButtonGroup).sortBy[x].map[new HTMLButtonGroupTemplate().create(it)].join»
	</div>
	<!-- end button tool bar -->
	'''

	/**
	 * Returns the id of a Node without - and _
	 * @param node
	 * @return
	 */
	def cincoID(Node node){
		return node.getId().replace("-", "").replace("\\_", "");
	}
	
	/**
	 * Generates the tool bar menu component Angular HTML template code
	 */
	private def createMenu()
	'''
	<!-- beginn button tool bar menu -->
	<nav «ngIfFor» «style» class="navbar navbar-default" role="menubar" aria-label="menu">
		<div class="container-fluid">
			<div class="navbar-header">
			    <button type="button" class="navbar-toggle collapsed"
			            data-toggle="collapse"
			            data-target="#«toolbar.cincoID»"
			            aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			</div>
		    <div class="collapse navbar-collapse"  id="«toolbar.cincoID»">
		        <ul class="nav navbar-nav">
					«toolbar.toHtml»
		        </ul>
		    </div>
		</div>
	</nav>
	<!-- end button tool bar menu -->
	'''
	
	/**
	 * Generates the tool bar bread crumb component Angular HTML template code
	 */
	private def createBreadcrum()
	'''
	<!-- beginn button tool bar breadcrumb -->
	<ol «ngIfFor» «styleWith("background: #F8F8F8;")» class="breadcrumb" role="menubar" aria-label="breadcrumb">
		«toolbar.toHtml»
	</ol>
	<!-- end button tool bar breadcrumb -->
	'''
	
	def toHtml(ButtonToolbar toolbar) {
		toolbar.buttonGroups.sortBy[x].map[
			buttons.sortBy[x].map[toHtml].join
		].join
	}
	
	def toHtml(Button button) {
		new HTMLButtonTemplate().create(button.asLink)
	}
	
	def style() {
		styleWith(null)
	}
	
	def styleWith(String additionalCss) {
		toolbar.printStyleWith(additionalCss)
	}
	
	def ngIfFor() {
		if (preview) toolbar.printNgIf else toolbar.printNgIfFor
	}
	
	/**
	 * Converts a button to a link button
	 */
	static def asLink(Button button) {
		button => [
			options = options ?: GuiFactory.eINSTANCE.createButtonOptions
			=> [ isLink = true ]
		]
	}
}
