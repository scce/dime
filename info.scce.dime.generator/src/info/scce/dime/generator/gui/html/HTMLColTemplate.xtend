/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.ColOffsetSize
import info.scce.dime.gui.gui.ColSize
import info.scce.dime.gui.gui.Layout
import info.scce.dime.gui.gui.Size
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate Angular HTML template code for the Column component
 */
class HTMLColTemplate extends GUIGenerator {
	
	
	/**
	 * Generates the Angular HTML template code for the given column component
	 */
	def create(Col col) {
		var html = this.pre(col).toString;
		html += new BaseTemplate().baseContent(ElementCollector.getElementsV(col.allNodes));
		html += this.post(col).toString;
		return html;
	}
	
	/**
	 * Generates the opening HTML tags for the given column component
	 */
	private def pre(Col container)'''
	<div
	«container.printNgIfFor»
	«container.printStyle»
	class="col-«getSize(container.layout)»-«getWidth(container.wideness)»«IF container.isOffset» col-«getSize(container.layout)»-offset-«getOffWidth(container.layout)»«ENDIF»«IF container.layout!=null» text-«container.layout.alignment.literal.toLowerCase»«ENDIF»"
	>
	'''
	
	/**
	 * Checks if the given column has an offset
	 */
	private def boolean getIsOffset(Col col){
		if(col.layout!=null){
			if(col.layout.offset!=ColOffsetSize.NONE)return true;
		}
		return false;
	}
	
	/**
	 * Generates the closing HTML tags for the given column component
	 */
	private def post(Col container)'''
	</div>
	'''
	
	/**
	 * Determines the CSS class suffix for the defined size on the given layout
	 */
	private def getSize(Layout layout) {
		if(layout==null)return "xs";
		var size = layout.size;
		if(size == Size.DEFAULT)return "md";
		if(size == Size.LARGE)return "lg";
		if(size == Size.SMALL)return "sm";
		return "xs";
	}
	
	/**
	 * Determines the CSS class suffix for the defined width
	 */
	private def getWidth(ColSize width) {
		switch (width) {
		case FULL:return 12
		case ELEVEN_TWELFTH:	return 11
		case FIVE_SIXTH:	return 10
		case THREE_QUARTER:return 9
		case TWO_THIRD:return 8
		case SEVEN_TWELFTH:return 7
		case HALF:return 6
		case FIVE_TWELFTH:return 5
		case THIRD:return 4
		case QUARTER:
			return 3
		case SIXTH:
			return 2
		case TWELFTH:
			return 1
			
		}
	}
	
	/**
	 * Determines the CSS class suffix for the defined offset width in the given layout
	 */
	private def getOffWidth(Layout layout) {
		if(layout==null)return 12;
		switch(layout.offset){
			case FULL: return 12
			case THREE_QUARTER: return 9
			case TWO_THIRD: return 8
			case HALF: return 6
			case THIRD: return 4
			case QUARTER: return 3
			case SIXTH: return 2
			case TWELFTH: return 1
			case NONE: { return 12;
			}
			
		}
	}
}
