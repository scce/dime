/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.dime.generator.gui.dart.component.AngularFormHTMLTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.HTMLSizeTemplate
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.AddToSubmission
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.Glyphicon
import info.scce.dime.gui.gui.Size
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.helper.GUIBranch

import static info.scce.dime.gui.gui.ButtonToolbarType.BREADCRUM

import static extension info.scce.dime.gui.helper.GUIBranchPort.getPortName

/**
 * Template for the button component Angular HTML template content
 */
class HTMLButtonTemplate extends GUIGenerator {
	
	
	/**
	 * Generates the button component Angular HTML template content.
	 * If present, the surrounding form can be provided.
	 * No additional CSS is provided.
	 */
	def create(Button button,Form validatingForm)
	{
		return create(button,validatingForm,null)
	}
	
	/**
	 * Generates the button component Angular HTML template content.
	 * If present, the surrounding form can be provided.
	 * Additional CSS can be provided.
	 */
	def create(Button button,Form validatingForm,String additionalCss)
	'''
	<!-- beginn button «button.label» -->
	«IF button.hasType(BREADCRUM)»
		<li role="option" «button.printNgIfFor» «IF button.disabled» style="pointer-events: none;color: gray;cursor: default;"«ENDIF»>
		«IF button.disabled&&button.hasTooltip»<div «button.generalStyle.tooltip.createTooltip»>«ENDIF»
		<a «button.printStyleWith(additionalCss)»
			data-cinco-id="«button.id.escapeString»"
			tabindex="0" aria-label="«button.label»"
			«IF button.disabled»class="disabled" disabled«ELSE»[class.disabled]="isBusy()"«ENDIF»
			«IF !preview»
				«IF button.isStatic»
					[attr.href]="'«ConventionHelper.replaceDataBindingString(button.staticURL)»'"
				«ELSE»
					«IF !button.disabled»(click)="«getAction(button,validatingForm)»"«ENDIF»
				«ENDIF»
			«ENDIF»
			role="button">
	«ELSEIF new HTMLButtonGroupTemplate().getIsIsLink(button.options)»
		«IF button.disabled&&button.hasTooltip»<div style="display: inline-block;" role="tooltip" aria-label="tooltip" «button.generalStyle.tooltip.createTooltip»>«ENDIF»
		<a «button.printNgIfFor» «button.printStyleWith(additionalCss)»
			data-cinco-id="«button.id.escapeString»"
			tabindex="0" aria-label="«button.label»"
		«IF button.isStatic»
			href="«ConventionHelper.replaceDataBinding(button.staticURL)»"
		«ELSEIF !preview»
				«IF !button.disabled»[class.not-active]="«IF validatingForm!=null && button.options?.submitsForm»«new AngularFormHTMLTemplate().getFormHasErrors(validatingForm,false)»||«ENDIF»isBusy()"«ENDIF»
			«IF !button.disabled»(click)="«getAction(button,validatingForm)»"«ENDIF»
		«ENDIF»
		role="button"«IF button.disabled» disabled«ENDIF»>
	«ELSE»
		«IF button.disabled&&button.hasTooltip»<div «button.generalStyle.tooltip.createTooltip»>«ENDIF»
		<button «if (preview) button.printNgIf else button.printNgIfFor» «button.printStyle»
			data-cinco-id="«button.id.escapeString»"
			tabindex="0" aria-label="«button.label»"
			«IF !button.disabled»
			[disabled]="«IF validatingForm!=null && button.options?.submitsForm && !preview»«new AngularFormHTMLTemplate().getFormHasErrors(validatingForm,false)»||«ENDIF»isBusy()"
			«ENDIF»
		«IF button.disabled» disabled="disabled"«ENDIF»
		type="«getType(button,validatingForm)»"
		«IF button.isStatic»
			(click)="window.location.href='«ConventionHelper.replaceDataBindingString(button.staticURL)»'"
		«ELSEIF !preview»
			«IF !button.disabled»(click)="«getAction(button,validatingForm)»"«ENDIF»
		«ENDIF»
		class="btn btn-«getColor(button)»«getSize(button)»«IF button.fullWidth» btn-block«ENDIF»">
	«ENDIF»
	«getButtonLabel(button)»
	«IF button.hasType(BREADCRUM)»
		</a>
		«IF button.disabled&&button.hasTooltip»</div>«ENDIF»
		</li>
	«ELSEIF new HTMLButtonGroupTemplate().getIsIsLink(button.options)»
		</a>
		«IF button.disabled&&button.hasTooltip»</div>«ENDIF»
	«ELSE»
		</button>
		«IF button.disabled&&button.hasTooltip»</div>«ENDIF»
	«ENDIF»
	<!-- end button «button.label» -->
	'''
	
	/**
	 * Checks if a button contains a static URL
	 */
	private def boolean getIsStatic(Button button){
		if(button.options!=null){
			return !button.options?.staticURL.nullOrEmpty
		}
		return false;
	}
	
	
	
	/**
	 * Checks if a button should be displayed in full width
	 */
	private def boolean fullWidth(Button button){
		if(button.styling != null)return button.styling.fullWidth;
		return false;
	}
	
	/**
	 * Returns the button display label.
	 * If no display label is provided, the label is returned
	 */
	def getButtonLabel(Button button){
		return new HTMLIconTemplate().icons(button.icon,
		'''
		«IF button.displayLabel.nullOrEmpty && button.icon.postIcon==Glyphicon.NONE && button.icon.preIcon==Glyphicon.NONE»
			«button.label»
		«ELSE»
			«ConventionHelper.replaceDataBinding(button.displayLabel)»
		«ENDIF»
		'''
		)
	}
	
	/**
	 * Returns the method call for the given button.
	 * This includes the parameter list for all binded variables
	 */
	def getAction(Button button,Form validatingForm)
	'''
	«IF !preview»
		«IF button.surroundingLoop == null»
			«ConventionHelper.getEventName(GUIBranch.getBranchName(button))»«button.id.escapeDart»EventTrigger(«IF validatingForm!= null && button.options?.submitsForm»«ConventionHelper.getFormName(validatingForm)».value«ENDIF»)
		«ELSE»
			«ConventionHelper.getEventName(GUIBranch.getBranchName(button))»«button.id.escapeDart»EventTrigger(«IF validatingForm!= null && button.options?.submitsForm»«ConventionHelper.getFormName(validatingForm)».value«IF!getEventTriggerParams(button).empty»,«ENDIF»«ENDIF»«FOR node:getEventTriggerParams(button) SEPARATOR ","»«DataDartHelper.getDataAccess(node,DataAccessType.^FOR,MODE.GET)»«ENDFOR»)
		«ENDIF»
	«ENDIF»
	'''
	
	/**
	 * Returns the list of all variables and attributes connected to the given button by submission edges
	 */
	def Iterable<Node> getEventTriggerParams(Button button)
	{
		button.getIncoming(AddToSubmission).filter[n|n.sourceElement.isPartOfAnyIteration].map[sourceElement];
	}
	
	/**
	 * Generates the button Angular HTML template code
	 */
	def create(Button button)
	'''
	«create(button, button.findFirstParent(Form))»
	'''
	
	
	/**
	 * Returns the HTML button type
	 */
	def getType(Button button,Form validatingForm){
		//check if button is the only button to submit the form
		if(button.options!=null&&validatingForm!=null){
			if(button.options.submitsForm){
				if (validatingForm.find(Button).filter[options?.submitsForm].size==1){
					return "submit"
				}
			}
		}
		return "button";
	}
	
	/**
	 * Returns the defined button size CSS class
	 */
	private def CharSequence getSize(Button button){
		if(button.styling == null)return "";
		if(button.styling.size != Size.DEFAULT)return " btn-"+HTMLSizeTemplate.create(button.styling.size);
		return ""
	}
	
	/**
	 * Returns the defined button color CSS class
	 */
	private def CharSequence getColor(Button button){
		if(button.styling == null)return HTMLColoringTemplate.createClass(Coloring.DEFAULT);
		return HTMLColoringTemplate.createClass(button.styling.color);
	}
	
	/**
	 * Generates the Stream Controller declaration for all buttons found in cmc
	 * without prefix
	 */
	def createEventHandlers(ModelElementContainer cmc,GUICompoundView gcv)
	{
		return createEventHandlers(cmc,"",gcv);
	}
	
	/**
	 * Generates the Stream Controller declaration for all buttons found in cmc
	 */
	def createEventHandlers(ModelElementContainer cmc,String prefix,GUICompoundView gcv)
	'''
	«FOR button : cmc.find(Button).filter[!disabled]»
		«createEventHandler(button, cmc, button.findFirstParent(Form), prefix, gcv)»
	«ENDFOR»
	'''
	
	
	/**
	 * Generates the methods for the given button
	 */
	def createEventHandler(Button button,ModelElementContainer gui,Form validatingForm,String prefix,GUICompoundView gcv)
	'''
	«IF !button.disabled»
		/// callback, if the button «button.label» is clicked
		«IF (button.isEmbeddedInForm && !(gui instanceof Form)) || (button.isEmbeddedInTable && !(gui instanceof Table))»
			void «ConventionHelper.getEventName(GUIBranch.getBranchName(button))»«button.id.escapeDart»EventTrigger(Map<String,dynamic> data)
			{
				«IF !button.staticURL.nullOrEmpty»
					// static button redirect
					html.window.location.href='«ConventionHelper.replaceDataBindingString(button.staticURL)»';
				«ELSE»
					«IF button.outgoing.empty»
					this.«prefix»«ConventionHelper.getEventName(GUIBranch.getBranchName(button))».add(data);
					«ENDIF»
				«ENDIF»
			}
		«ELSE»
			«IF button.surroundingLoop != null»
			void «ConventionHelper.getEventName(GUIBranch.getBranchName(button))»«button.id.escapeDart»EventTrigger(
				«IF validatingForm != null && button.options?.isSubmitsForm && gui.isInSameForm(validatingForm)»dynamic formValues«IF !getEventTriggerParams(button).empty»,«ENDIF»«ENDIF»
				«FOR node:getEventTriggerParams(button) SEPARATOR ","»
					p«ConventionHelper.cincoID(node)»
				«ENDFOR»
				
				)
			«ELSE»
			void «ConventionHelper.getEventName(GUIBranch.getBranchName(button))»«button.id.escapeDart»EventTrigger(«IF validatingForm != null && button.options?.submitsForm && gui.isInSameForm(validatingForm)»dynamic formValues«ENDIF»)
			«ENDIF»
		
		{
			«IF validatingForm != null && button.options?.isSubmitsForm && gui.isInSameForm(validatingForm)»
			//Submit Form Values
			this.«ConventionHelper.getFormName(validatingForm)»Submit(formValues);
			«ENDIF»
			«IF !button.staticURL.nullOrEmpty»
				// static button redirect
				html.window.location.href='«ConventionHelper.replaceDataBindingString(button.staticURL)»';
			«ELSE»
				«IF button.isEmbeddedInPlaceholderTab»
				// button is placed in a tab and changes the state
				this.currentbranch = '«ConventionHelper.cincoID(button)»';
				«ENDIF»
				Map<String,dynamic> data = new Map();
				«FOR edge:button.getIncoming(AddToSubmission)»
					«IF edge.sourceElement.isPartOfAnyIteration»
						data['«edge.portName.escapeString»'] = p«ConventionHelper.cincoID(edge.sourceElement)»;
					«ELSE»
						data['«edge.portName.escapeString»'] = this.«DataDartHelper.getDataAccess(edge.sourceElement,DataAccessType.^FOR,MODE.GET)»;
					«ENDIF»
				«ENDFOR»
				«IF button.outgoing.empty»
				this.«prefix»«ConventionHelper.getEventName(GUIBranch.getBranchName(button))».add(data);
				«ENDIF»
			«ENDIF»
		}
	«ENDIF»
	«ENDIF»
	'''
	
	
	
	/**
	 * Returns the static URL of the given button if one is present
	 */
	private def hasTooltip(Button button){
		button.generalStyle?.tooltip != null	
	}
	
	private def isInSameForm(ModelElementContainer gui,Form validatingForm) {
		if(validatingForm !=null){
			return gui.id.equals(validatingForm.id)
		}
		false;
	}
	
	
}
