/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.ComponentColoring

/**
 * Template to convert color constants
 */
class HTMLColoringTemplate extends GUIGenerator {
	
	//TODO: should also be a util method
	/**
	 * Converts a color constant to the corresponding HEX code
	 */
	static def createHex(Coloring coloring){
		if(coloring == Coloring.BLUE)return "#337AB7";
		if(coloring == Coloring.DEFAULT)return "#000000";
		if(coloring == Coloring.GREEN)return "#DFF0D8";
		if(coloring == Coloring.LIGHTBLUE)return "#D9EDF7"
		if(coloring == Coloring.RED)return "#F2DEDE"
		if(coloring == Coloring.YELLOW)return "#FCF8E3"
	}
	
	
	//TODO: should also be a util method
	/**
	 * Converts a color constant to the corresponding CSS class
	 */
	static def createClass(Coloring coloring){
		if(coloring == Coloring.BLUE)return "primary";
		if(coloring == Coloring.DEFAULT)return "default";
		if(coloring == Coloring.GREEN)return "success";
		if(coloring == Coloring.LIGHTBLUE)return "info"
		if(coloring == Coloring.RED)return "danger"
		if(coloring == Coloring.YELLOW)return "warning"
	}
	
	/**
	 * Converts a color constant to the corresponding CSS class
	 * or the default class, if no color constant is given
	 */
	def createClass(ComponentColoring coloring){
		if(coloring != null)return createClass(coloring.color);
		return createClass(Coloring.DEFAULT);
	}
	
	/**
	 * Converts a color constant to the corresponding HEX code
	 * or the default HEX code, if no color constant is given
	 */
	def createHex(ComponentColoring coloring){
		if(coloring!=null)return createHex(coloring.color);
		return createHex(Coloring.DEFAULT);
	}
}
