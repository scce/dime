/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator

class HTMLLoadingScreen extends GUIGenerator {
	
	def create()
	'''
	<div aria-busy="true" aria-live="polite" aria-atomic="true" class="show-delayed" style="width:100%;height:100%;text-align:center;padding-top: 5%;">
	    	<p style="color: #000000;">Loading..</p>
	    	<div class="progress" style="
	    	    width: 30%;
	    	    margin-top: 20px;
	    	    position: relative;
	    	    left: 35%;">
	    	    <div class="progress-bar progress-bar-striped active" style="width: 100%;"></div>
	    	    </div>
	</div>
	'''
}
