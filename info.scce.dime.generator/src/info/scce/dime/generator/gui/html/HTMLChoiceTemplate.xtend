/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.HTMLSizeTemplate
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Checkbox
import info.scce.dime.gui.gui.ChoiceData
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.Combobox
import info.scce.dime.gui.gui.ComplexChoiceData
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DataBinding
import info.scce.dime.gui.gui.Display
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.FormSubmit
import info.scce.dime.gui.gui.PrimitiveChoiceData
import info.scce.dime.gui.gui.Select
import info.scce.dime.gui.gui.Size

/**
 * Template for the choice component Angular HTML template content
 */
class HTMLChoiceTemplate extends GUIGenerator {
	
	/**
	 * Generates the choice component Angular HTML template code
	 */
	def create(Select select,Form form){
		return content(select,form);
	}
	
	/**
	 * Returns the iterator variable name of the given select component
	 */
	private def getIterator(Select select)
	{
		if(!select.getIncoming(ComplexChoiceData).isEmpty)
		{
			var ccv = select.getIncoming(ComplexChoiceData).get(0).sourceElement as ComplexVariable;
			if(!ccv.getOutgoing(ComplexListAttributeConnector).empty)
			{
				var target = ccv.getOutgoing(ComplexListAttributeConnector).get(0).targetElement;
				return target.name.escapeDart;
			}
		}
		else if(!select.getIncoming(PrimitiveChoiceData).empty)
		{
			return select.id.escapeDart+"Iterator";
		}
		throw new IllegalStateException("No ChoiceData Selected: "+select.label+" in GUI "+select.rootElement.title);
	}
	
	/**
	 * Returns the source variable name of the given select component
	 */
	private def getValue(Select select)
	{
		if(!select.getIncoming(Display).empty){
			return DataDartHelper.getBindedDataName(select,DataAccessType.^FOR,MODE.GET,Display)			
		}
		return select.id.escapeDart+"Iterator";
	}
	
	/**
	 * Generates the choice component Angular HTML template code
	 */
	def content(Select select,Form form)
	{
		val field = new HTMLFieldTemplate()
	'''
		<!-- beginn select component «select.label» -->
			«IF select.isComplexSelect()»
				«IF select instanceof Combobox»
					<!-- beginn combobox «select.label» -->
					<div «select.printNgIfFor» «select.printStyle» class="form-group«getColoring(select)»">
						<label for="«ConventionHelper.cincoID(select)»">«ConventionHelper.replaceDataBinding(select.label)»</label>
						«IF !form.inline»«field.helpText(select)»«ENDIF»
						<select«IF select.disabled» disabled«ELSE» [disabled]="isBusy()"«ENDIF»
						data-cinco-id="«select.id.escapeString»"
						id="«ConventionHelper.cincoID(select)»"
						«IF select.multiple»
						multiple
						«ENDIF»
						«IF !preview»(change)="submitSelect«ConventionHelper.cincoID(select)»Box($event)"«ENDIF»
						class="form-control«getSize(select)»">
							«IF !select.required»
							<option
							value="-1"
								«IF isDefaultNull(select)»
									selected
								«ENDIF»
							>«select.emptyValue»</option>
							«ENDIF»
							<option
							«IF !preview»
							 *ngFor="let «getIterator(select)» of «ConventionHelper.getFormInputName(select)»Iteratable; let «ConventionHelper.cincoID(select)»Num = index"
							 [value]="«ConventionHelper.cincoID(select)»Num.toString()"
							 [selected]="is«ConventionHelper.cincoID(select)»Selected(«getIterator(select)»)"
							 «ENDIF»
							>
								«IF preview»Option«ELSE»{{«getValue(select)»}}«ENDIF»
							</option>
						</select>
						«IF form.inline»«field.helpText(select)»«ENDIF»
					</div>
					<!-- end combobox «select.label» -->
				«ELSE»
					<!-- beginn «IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF» «select.label» -->
					«IF !select.label.nullOrEmpty»<p><strong>«ConventionHelper.replaceDataBinding(select.label)»</strong></p>«ENDIF»
					«IF !form.inline»«field.helpText(select)»«ENDIF»
					<div «select.printNgIfFor» «select.printStyle»
						class="«IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF»"
						«IF !preview»
						(change)="submitSelect«ConventionHelper.cincoID(select)»Box($event)"
						*ngFor="let «getIterator(select)» of «ConventionHelper.getFormInputName(select)»Iteratable; let «ConventionHelper.cincoID(select)»Num = index"
						«ENDIF»
						>
					  <label #select«select.id.escapeDart» [attr.for]="select«select.id.escapeDart».hashCode" «IF select.inline» class="«IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF»-inline"«ENDIF»>
					    <input«IF select.disabled» disabled«ELSE» [disabled]="isBusy()"«ENDIF»
					    name="«ConventionHelper.cincoID(select)»"
					    data-cinco-id="«select.id.escapeString»"
					    aria-label="«select.label»"
					    «IF !preview»
					    [attr.id]="select«select.id.escapeDart».hashCode"
					    [value]="«ConventionHelper.cincoID(select)»Num.toString()"
					    [checked]="is«ConventionHelper.cincoID(select)»Selected(«getIterator(select)»)"
					    «ENDIF»
					    type="«IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF»"
					    >
					    «IF preview»Option«ELSE»{{«getValue(select)»}}«ENDIF»
					  </label>
					</div>
					«IF form.inline»«field.helpText(select)»«ENDIF»
					<!-- end «IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF» «select.label» -->
				«ENDIF»
			«ELSE»
				«IF select instanceof Combobox»
					<!-- beginn combobox «select.label» -->
					<div «select.printNgIfFor» «select.printStyle» class="form-group«getColoring(select)»">
						<label for="«ConventionHelper.cincoID(select)»">«ConventionHelper.replaceDataBinding(select.label)»</label>
						«IF !form.inline»«field.helpText(select)»«ENDIF»
						<select «IF select.disabled» disabled«ELSE» [disabled]="isBusy()"«ENDIF»
						data-cinco-id="«select.id.escapeString»"
						id="«ConventionHelper.cincoID(select)»"
						«IF select.multiple»
							multiple
							«IF !preview»(change)="submitSelect«ConventionHelper.cincoID(select)»Box($event)"«ENDIF»
						«ELSE»
							«IF !preview»
							[ngFormControl]="«ConventionHelper.getFormName(select.findFirstParent(Form))».controls['«ConventionHelper.getFormInputName(select)»']"
							[ngModel]="«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET_INIT,ChoiceData)»"
							(ngModelChange)="«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.SET_INIT,DataBinding)»($event)"
							«ENDIF»
						«ENDIF»
						class="form-control«getSize(select)»">
							«IF !select.required»
								<option
								value="-1"
									«IF isDefaultNull(select)»
										selected
									«ENDIF»
								>«select.emptyValue»</option>
							«ENDIF»
								<option
								«IF !preview»
								 *ngFor="let «getIterator(select)» of «ConventionHelper.getFormInputName(select)»Iteratable"
								 [value]="«getValue(select)»"
								 «ENDIF»
								 «IF select.multiple»
								 	«IF !preview»
							 		[selected]="«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET_INIT,ChoiceData)».indexOf(«getIterator(select)»)>-1"
							 		«ENDIF»
						 		 «ELSE»
						 		 	«IF !preview»
						 		 	[selected]="«getIterator(select)»==«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET_INIT,ChoiceData)»"
						 		 	«ENDIF»
								 «ENDIF»
							>
							«IF preview»Option«ELSE»{{«getValue(select)»}}«ENDIF»
							</option>
						</select>
						«IF form.inline»«field.helpText(select)»«ENDIF»
					</div>
					<!-- end combobox «select.label» -->
				«ELSE»
					<!-- beginn «IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF» «select.label» -->
					«IF !select.label.nullOrEmpty»<p><strong>«ConventionHelper.replaceDataBinding(select.label)»</strong></p>«ENDIF»
					«IF !form.inline»«field.helpText(select)»«ENDIF»
					<div «select.printNgIfFor» «select.printStyle»
						class="«IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF»"
						«IF !preview»
							*ngFor="let «getIterator(select)» of «ConventionHelper.getFormInputName(select)»Iteratable; let «getIterator(select)»Index = index"
						«ENDIF»
						>
					  <label«IF select.inline» class="«IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF»-inline"«ENDIF»>
					    <input «IF select.disabled» disabled«ELSE» [disabled]="isBusy()"«ENDIF»
					    data-cinco-id="«select.id.escapeString»"
					    name="«ConventionHelper.cincoID(select)»"
					    «IF !preview»
					    	[value]="«getIterator(select)»Index"
							«IF select instanceof Checkbox»
								(click)="submitSelect«ConventionHelper.cincoID(select)»Box($event)"
								[checked]="«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET_INIT,DataBinding)».indexOf(«getIterator(select)»)>-1"
							«ELSE»
								(click)="submitSelect«ConventionHelper.cincoID(select)»Box($event)"
								[checked]="«getIterator(select)»==«DataDartHelper.getBindedDataName(select,DataAccessType.CHOICE,MODE.GET_INIT,DataBinding)»"
							«ENDIF»
						«ENDIF»
					    type="«IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF»"
					    >
					    «IF preview»Option«ELSE»{{«getValue(select)»}}«ENDIF»
					  </label>
					</div>
					«IF form.inline»«field.helpText(select)»«ENDIF»
					<!-- end «IF select instanceof Checkbox»checkbox«ELSE»radio«ENDIF» «select.label» -->
				«ENDIF»
			«ENDIF»
		<!-- end select component «select.label» -->
	'''
	}
	
	def boolean isComplexSelect(Select it) {
		!findSourceOf(DataBinding)?.isPrimitve
	}
	
	/**
	 * Checks if in line style is enabled
	 */
	private def boolean getInline(Select select){
		if(select.styling==null)return false;
		return select.styling.inline;
	}
	
	/**
	 * Returns the empty value of the given select
	 */
	private def String emptyValue(Select select) {
		if(select.additionalOptions==null)return "";
		return select.additionalOptions.emptyValue;
	}
	
	/**
	 * Checks if the required validation is enableds
	 */
	private def boolean getRequired(Select select){
		if(select.validation==null)return false;
		return select.validation.isRequired;
	}
	
	/**
	 * Returns the CSS class for the defined coloring
	 */
	private def getColoring(Select field){
		if(field.styling==null)return "";
		if(field.styling.color == Coloring.DEFAULT)return "";
		return " has-"+HTMLColoringTemplate.createClass(field.styling.color);
	}
	
	/**
	 * Returns the CSS class for the defined size
	 */
	public def getSize(Select field){
		if(field.styling==null)return "";
		if(field.styling.size == Size.DEFAULT)return "";
		return " input-"+HTMLSizeTemplate.create(field.styling.size);
	}
	
	/**
	 * Checks if initially no entry of the select is selected
	 */
	private def isDefaultNull(Select select)
	{
		for(edge : select.incoming) {
			if(edge instanceof FormSubmit)return true;
		}
		return false;
	}
}
