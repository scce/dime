/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template for the box component Angular HTML template content
 */
class HTMLBoxTemplate extends GUIGenerator{
	
	/**
	 * Generates the box component Angular HTML template code
	 */
	def create(Box box)
	{
		var html = this.pre(box).toString;
		html += new BaseTemplate().baseContent(ElementCollector.getElementsV(box.allNodes));
		html += this.post(box).toString;
		return html;
	}
	
	private def pre(Box box)'''
	<!-- beginn box -->
	<div «box.printNgIfFor» «box.printStyleWith(box.padding)» class="container«IF !box.isInvisible» page-content«ENDIF»">
	'''
	
	private def post(Box box)'''
	</div>
	<!-- end box -->
	'''
	
	// TODO: might be useful for more components, integrate into guigenerator?
	 
	private def getPadding(Box box) {
		if (box.isInvisible) "padding:0px;" else ""
	}
}
