/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.generator.gui.dart.component.AngularComponentHTMLTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import java.nio.file.Path

/**
 * Template to generate the preview plain HTML 5 file
 */
class HTMLPreviewTemplate extends GUIGenerator {
		
	extension DimeIOExtension = new DimeIOExtension
	
	/**
	 * Generates the preview plain HTML 5 file for a given GUI model
	 */
	def String create(GUI gui) {
		
		val projectPath = gui.projectPath
		
		val additionalStyles = gui
			.additionalStylesheets
			.map[toAbsolutePathString(projectPath)]
		
		val additionalScripts = gui
			.additionalJavaScript
			.map[toAbsolutePathString(projectPath)]
		
		val plugins = gui
			.find(GUIPlugin)
			.map[function]
			.filter(Function)
			.map[eContainer]
			.filter(Plugin)
		
		val guiPluginStyles = plugins
			.map[style]
			.filterNull
			.flatMap[files]
			.map[path]
			.map[toAbsolutePathString(projectPath)]
		
		val guiPluginScripts = plugins
			.map[script]
			.filterNull
			.flatMap[files]
			.map[path]
			.map[toAbsolutePathString(projectPath)]
		
		return '''
			<!doctype html>
			<html>
				<head>
					<title>Preview «gui.title»</title>
					<meta charset="utf-8"> 
					<link type="text/css" rel="stylesheet" href="../target/webapp/app/web/css/bootstrap.min.css" />
					<link type="text/css" rel="stylesheet" href="../target/webapp/app/web/css/dime.css" />
					«FOR style: additionalStyles + guiPluginStyles»
						<link type="text/css" rel="stylesheet" href="«style»" />
					«ENDFOR»
					<script type="text/javascript" src="../target/webapp/app/web/js/jquery-1.12.2.min.js"></script>
					<script type="text/javascript" src="../target/webapp/app/web/js/bootstrap.min.js"></script>
					<script>
						$.noConflict();
					</script>
					<meta name="viewport" content="width=device-width, initial-scale=1">
				</head>
				<body>
					«new AngularComponentHTMLTemplate().preview(gui)»
					«FOR script: additionalScripts + guiPluginScripts»
						<script type="text/javascript" src="«script»"></script>
					«ENDFOR»
				</body>
			</html>
		'''
		
	}
	
	def private String toAbsolutePathString(String pathString, Path projectPath) {
		if (pathString.isHTTP) {
			pathString
		}
		else {
			projectPath.resolve(pathString).toString
		}
	}
	
}
