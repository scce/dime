/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.dart.component.AngularCommonComponentTemplate
import info.scce.dime.generator.gui.dart.component.AngularTableHTMLTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.DataTarget
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.TableChoice
import info.scce.dime.gui.gui.TableLoad

/**
 * Template to generate the Angular component HTML template code for a table component
 */
class HTMLTableTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given table component
	 */
	def create(Table table,GUICompoundView gcv) {
		if(preview){
			return new AngularTableHTMLTemplate().create(table,table.rootElement);
		}
		return getTabelTag(table,table.rootElement,gcv);
		
	}
	
	/**
	 * Generates the Angular component HTML template code for the given table Angular component tag
	 */
	private def getTabelTag(Table table,GUI gui,GUICompoundView gcv)
	'''
	<table-«ConventionHelper.cincoID(table)»-«gui.title.escapeDart.toFirstLower»
			«new AngularCommonComponentTemplate().tagInputs(table,gui,true,gcv)»
		
			[source«table.id.escapeDart»]="«DataDartHelper.getDataAccess(table.getIncoming(TableLoad).get(0).sourceElement,DataAccessType.^FOR,MODE.GET)»"
		  	(source_update)="«DataDartHelper.getDataAccess(table.getIncoming(TableLoad).get(0).sourceElement,DataAccessType.^FOR,MODE.GET)».replaceAll($event)"
		  	«IF table.choices == TableChoice.SINGLE»
		  	(dime_single_choice)="«DataDartHelper.getBindedDataName(table,DataAccessType.CHOICE,MODE.SET_INIT,DataTarget)»($event)"
		  	«ENDIF»
			«table.componentOutputs("table")»
	>
	</table-«ConventionHelper.cincoID(table)»-«gui.title.escapeDart.toFirstLower»>
	'''
	
	/**
	 * Generates the Angular component dependency declaration, for all
	 * table components in the given container
	 */
	def createDependencies(ModelElementContainer cmc,GUI gui)
	'''
	«FOR Table table : cmc.find(Table).filter[it != cmc]»
		«table.id.escapeDart».Table«ConventionHelper.cincoID(table)»«gui.title.escapeDart.toFirstUpper»,
	«ENDFOR»
	'''

}
