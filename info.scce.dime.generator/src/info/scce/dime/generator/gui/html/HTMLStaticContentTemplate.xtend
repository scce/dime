/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.StaticContent

import static info.scce.dime.generator.gui.utils.helper.ConventionHelper.replaceDataBinding
import static info.scce.dime.generator.gui.utils.helper.ConventionHelper.replaceDataBindingString

/**
 * Generates the Angular template for StaticContent
 */
class HTMLStaticContentTemplate extends GUIGenerator {
	
	val StaticContent sc
	
	new(StaticContent staticContent) {
		this.sc = staticContent
	}
	
	static def render(StaticContent staticContent) {
		new HTMLStaticContentTemplate(staticContent).render()
	}
	
	def render() {
		val html = '''
			<span «renderAttributes»>
				«renderContent()»
			</span>
		'''
		wrapWithStyling(
			wrapWithIcons(html)
		)
	}
	
	def renderAttributes() {
		'''aria-live="polite" aria-atomic="true"'''
		+ renderClasses()
		+ renderSafeInnerHtml()
		+ renderMaxLength()
	}
	
	def renderClasses() {
		''' class="«renderAlignmentClass()» «renderColorClass()»"'''
	}
	
	def renderAlignmentClass() {
		'''text-«renderAlignment()»'''
	}
	
	def renderColorClass() {
		'''text-«new HTMLColoringTemplate().createClass(sc.styling)»'''
	}
	
	def wrapWithStyling(CharSequence html) {
		html?.wrapWith(switch it : sc.styling {
			case it?.deleted: "del"
			case it?.bold: "strong"
			case it?.highlight: "mark"
			case it?.underline: "u"
			case it?.small: "small"
			case it?.italic: "em"
			default: null
		})
	}
	
	def wrapWithIcons(CharSequence html) {
		new HTMLIconTemplate().icons(sc.icon, html)
	}
	
	def wrapWith(CharSequence html, String tagName) {
		if (!tagName.nullOrEmpty) {
			'''<«tagName»>«html ?: ''»</«tagName»>'''
		} else html
	}
	
	def renderSafeInnerHtml() {
		switch sc {
			case sc.isRenderMarkdown: {
				val content = "'" + replaceDataBindingString(sc.rawContent) + "'"
				''' [safeInnerHtml]="getSafeHtml(renderMarkdown(«content»))"'''
			}
			case sc.isHtml: {
				val content = "'" + replaceDataBindingString(sc.rawContent) + "'"
				''' [safeInnerHtml]="getSafeHtml(«content»)"'''
			}
			default: ''
		}
	}
	
	def renderMaxLength() {
		if (sc.maxChars > 0) {
			''' [maxLength]="'«sc.maxChars»'"'''
		} else ''
	}
	
	def renderContent() {
		if (!sc.isHtml && !sc.isRenderMarkdown) {
			'''«replaceDataBinding(sc.rawContent)»'''
		} else ''
	}
	
	private def renderAlignment() {
		switch sc.styling?.alignment {
			case CENTER: "center"
			case JUSTIFIED: "justify"
			case RIGHT: "right"
			case NO_WRAP: "nowrap"
			default: "left"
		}
	}
}
