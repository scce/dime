/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Headline
import info.scce.dime.gui.gui.HeadlineSize
import info.scce.dime.gui.gui.StaticContent

import static info.scce.dime.generator.gui.html.HTMLStaticContentTemplate.render

/**
 * Template to generate the Angular component HTML template code for a headline component
 */
class HTMLHeadlineTemplate extends GUIGenerator {

	/**
	 * Generates the Angular component HTML template code for the given headline component
	 */
	def create(Headline node) '''
		<h«getHeadlineSize(node.size)»
		«node.printNgIfFor»
		«_htmlcssTemplate.printStyle(node)»> 
		«FOR StaticContent sc:node.content SEPARATOR "<br />"»
			«render(sc)»
		«ENDFOR»
		</h«getHeadlineSize(node.size)»>
	'''
	
	/**
	 * Returns the headline HTML tag type for the defined headline size
	 */
	private def getHeadlineSize(HeadlineSize hs)
	{
		if(hs == HeadlineSize.BIG) return 1;
		if(hs == HeadlineSize.MEDIUM)return 2;
		if(hs == HeadlineSize.NORMAL)return 3;
		if(hs == HeadlineSize.SMALL)return 4;
	}
	
}
