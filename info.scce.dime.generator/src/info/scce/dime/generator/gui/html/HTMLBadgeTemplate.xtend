/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Badge
import info.scce.dime.gui.gui.StaticContent

import static info.scce.dime.generator.gui.html.HTMLStaticContentTemplate.render

class HTMLBadgeTemplate extends GUIGenerator {
	
	/**
	 * Generates the box component Angular HTML template code
	 */
	def create(Badge badge)
	{
		var html = this.pre(badge,preview).toString;
		html += '''
		«FOR StaticContent sc : badge.content»
			«render(sc)»
		«ENDFOR»
		'''
		html += this.post(badge).toString;
		return html;
	}
	
	private def pre(Badge badge,boolean preview)'''
	<!-- beginn badge -->
	<span «badge.printNgIfFor» «badge.printStyle» class="label label-«badge.coloring»">
	'''
	
	private def post(Badge badge)'''
	</span>
	<!-- end badge -->
	'''
	
	private def getColoring(Badge badge)
	{
		switch badge.color {
			case BLUE: return '''primary'''
			case DEFAULT: return '''default'''
			case GREEN: return '''success'''
			case LIGHTBLUE: return '''info'''
			case RED: return '''danger'''
			case YELLOW: return '''warning'''
		}
	}
}
