/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Display
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.helper.ElementCollector

import static info.scce.dime.generator.gui.html.HTMLStaticContentTemplate.render

/**
 * Template to generate the Angular component HTML template code for a panel component
 */
class HTMLPanelTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for a panel component
	 */
	def  CharSequence create(Panel panel){
		var html = this.pre(panel).toString;
		html += new BaseTemplate().baseContent(ElementCollector.getElementsV(panel.allNodes));
		html += this.post(panel).toString;
		return html;
	}
	
	/**
	 * Generates the Angular component HTML template code for a panel heading
	 */
	private def pre(Panel panel)'''
	<dime-panel
		«panel.printNgIfFor»
		#dime«panel.id.escapeDart»panel
		[styling]="«panel.justStyle(null)»"
		[cincoId]="'«panel.id.escapeString»'"
		«panel.generalStyle.customAttributes("attributes")»
		[coloring]="'«new HTMLColoringTemplate().createClass(panel.coloring)»'"
		[hasHeading]="«IF panel?.heading == null»false«ELSE»true«ENDIF»"
		[collapsable]="«IF panel.collapsable»true«ELSE»false«ENDIF»"
		«IF panel.collapsable»
			«IF !preview»
				«val displayEdge = panel.getIncoming(Display).head»
				«IF displayEdge !== null»
					[initial]="«panel.printIFCondition(displayEdge)»"
					«IF DataDartHelper.isBindedToBoolean(panel,DataAccessType.^FOR,MODE.GET)»
						(clickHeading)="«DataDartHelper.getBindedDataName(panel,DataAccessType.^FOR,MODE.SET_INIT)»($event)"
					«ENDIF»
				«ELSE»
					[initial]="«IF panel.defaultOpen.nullOrEmpty»true«ELSE»«panel.defaultOpen»«ENDIF»"
				«ENDIF»
			«ENDIF»
		«ELSE»
		[initial]="«IF panel.defaultOpen.nullOrEmpty»true«ELSE»«panel.defaultOpen»«ENDIF»"
		«ENDIF»
	>
	«IF panel.heading !== null»
			<span dime-panel-heading«IF !panel.collapsable»-false«ENDIF»>
				«render(panel.heading)»
			</span>
	«ENDIF»
		<div *ngIf="!dime«panel.id.escapeDart»panel.isCollapsed||dime«panel.id.escapeDart»panel.isLoaded" class="panel-body">
	'''
	
	/**
	 * Generates the Angular component HTML template code for a panel footer
	 */
	private def post(Panel panel)
	'''
		</div>
		«IF panel.footer !== null»
				<div class="panel-footer">
					«render(panel.footer)»
				</div>
		«ENDIF»
	</dime-panel>
	'''
}
