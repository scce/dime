/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.dart.functionality.AngularDartRootProcessTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Glyphicon
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.process.process.Process
import info.scce.dime.gui.gui.GUISIBModal
import java.lang.RuntimeException

/**
 * Template to generate the Angular component HTML template code for an embedded interaction process SIB component
 */
class HTMLProcessSIBTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for an embedded interaction process SIB component
	 */
	def create(ProcessSIB sib,Process cp)
	'''
	«IF preview»
		<div class="jumbotron"><h1>«cp.modelName»</h1></div>
	«ELSE»
		«IF sib.modal != null»
				<dime-modal
					«sib.printNgIf»
					#dime«sib.id.escapeDart»modal
					[isLink]="«IF sib.modal.options?.isIsLink»true«ELSE»false«ENDIF»"
					[isDisbaled]="«IF sib.modal?.styling?.disabled»true«ELSE»false«ENDIF»"
					[label]="'«IF sib.modal.buttonLabel.nullOrEmpty»«cp.modelName»«ELSE»«ConventionHelper.replaceDataBindingString(sib.modal.buttonLabel)»«ENDIF»'"
					[title]="'«ConventionHelper.replaceDataBindingString(sib.modal.modalTitle)»'"
					[reload]="«IF sib.modal.refreshContent»true«ELSE»false«ENDIF»"
					[modalSizeClass]="'«modalSizeClass(sib.modal)»'"
					[modalClass]="'btn btn-«sib.modal.printColorClass»«sib.modal.printSizeClass»«IF sib.modal?.styling?.fullWidth» btn-block«ENDIF»'"
					«IF sib.modal.icon!==null»
						«IF sib.modal.icon.preIcon!==Glyphicon.NONE»
						[hasBeforeIcon]="true"
						[beforeIconClass]="'«new HTMLIconTemplate().cssClass(sib.modal.icon.preIcon)»'"
						«ENDIF»
						«IF sib.modal.icon.preIcon!==Glyphicon.NONE»
						[hasAfterIcon]="true"
						[afterIconClass]="'«new HTMLIconTemplate().cssClass(sib.modal.icon.postIcon)»'"
						«ENDIF»
					«ENDIF»
					>
					<template [ngIf]="dime«sib.id.escapeDart»modal.isShown||dime«sib.id.escapeDart»modal.isLoaded">
		«ENDIF»
		<«AngularDartRootProcessTemplate.classTag(cp)»
			#process«sib.id.escapeDart»SIB
			«sib.printNgIfFor»
				[startPointId]="'«cp.startSIBs.get(0).id.escapeDart»'"
				[sibId]="'«sib.id.escapeJava»'"
				[guiId]="guiId"
				[parentRuntimeId]="runtimeId"
			«FOR input:sib.IOs»
				[«input.name.escapeDart»]="«input.inputPort»"
				
			«ENDFOR»
		>
		</«AngularDartRootProcessTemplate.classTag(cp)»>
		«IF sib.modal != null»
			</template>
		</dime-modal>
		«ENDIF»
	«ENDIF»
	'''
	
	def static modalSizeClass(GUISIBModal modal) {
		switch modal.modalSize {
			case DEFAULT: ""
			case SMALL: "modal-sm"
			case LARGE: "modal-lg"
			default: throw new RuntimeException("Default case in exhaustive switch - implementation broken?")
		}
	}
	
	def inputPort(IO input)
	{
		if(input instanceof InputPort && !input.incoming.empty){
			return '''«DataDartHelper.getBindedDataName(input,DataAccessType.^FOR,MODE.GET)»'''
		}
		else {
			return '''«input.defaultValue»'''
		}
	}
}
