/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.dart.component.AngularCommonComponentTemplate
import info.scce.dime.generator.gui.dart.component.AngularFormHTMLTemplate
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.DataBinding
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.helper.GUIBranch

class HTMLFormTemplate extends GUIGenerator {
	
	def create(Form form,GUICompoundView gcv)
	'''
	«IF preview»
	«new AngularFormHTMLTemplate().create(form,form.rootElement)»
	«ELSE»
	<form-«ConventionHelper.cincoID(form)»-«form.rootElement.title.escapeDart.toFirstLower»
		«new AngularCommonComponentTemplate().tagInputs(form,form.rootElement,true,gcv)»
		«IF!form.getIncoming(DataBinding).empty»
			[formLoad]="«DataDartHelper.getDataAccess(form.getIncoming(DataBinding).get(0).sourceElement,DataAccessType.^FOR,MODE.GET)»"
		«ENDIF»
	  	«form.componentOutputs("form")»
	  	«FOR primitiveVar:form.rootElement.dataContexts.map[primitiveVariables].flatten»
	  	(primitive_«primitiveVar.name.escapeDart»_update)="«primitiveVar.name.escapeDart» = $event"
	  	«ENDFOR»
	  	«FOR complexVar:form.rootElement.dataContexts.map[complexVariables].flatten.filter[complexVariablePredecessors.empty].filter[!isIsList]»
	  	(complex_«complexVar.name.escapeDart»_update)="«complexVar.name.escapeDart» = $event"
	  	«ENDFOR»
	  	role="form"
	>
	</form-«ConventionHelper.cincoID(form)»-«form.rootElement.title.escapeDart.toFirstLower»>
	«ENDIF»
	'''

	def createDependencies(ModelElementContainer cmc,GUI gui)
	'''
	«FOR Form form : cmc.find(Form).filter[it != cmc]»
		«form.id.escapeDart».Form«ConventionHelper.cincoID(form)»«gui.title.escapeDart.toFirstUpper»,
	«ENDFOR»
	'''
	

	
	def createFormMethods(ModelElementContainer gui) {
		val buttons = gui.find(Form).flatMap[it.find(Button)].filter[!disabled].groupBy[label].values.map[head]
	'''
		«FOR button:buttons»
		void formEvent«ConventionHelper.getEventName(GUIBranch.getBranchName(button))»Trigger(Map<String,dynamic> data)
		{
			«IF button.outgoing.empty»
			this.«ConventionHelper.getEventName(GUIBranch.getBranchName(button))».add(data);
			«ENDIF»
		}
	  	«ENDFOR»
	'''
	}
	
	
}
