/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import graphmodel.Node
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.Row
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular component HTML template code for a row component
 */
class HTMLRowTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given row component
	 */
	def create(Row row) {
		var html = this.pre(row).toString;
		for(Node node:ElementCollector.getElementsH(row.allNodes)){
			if(node instanceof Col){
				var col = new HTMLColTemplate();
				html += col.create(node);
			}
		}
		html += this.post(row).toString;
		return html;
	}
	
	/**
	 * Generates the opening HTML tag for the given row component
	 */
	def pre(Row container)'''
	<div «container.printNgIfFor» «container.printStyle»class="row">
	'''
	
	/**
	 * Generates the closing HTML tag for the given row component
	 */
	def post(Row container)'''
	</div>
	'''
	
}
