/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.enums.HTMLSizeTemplate
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.ContainerHeight
import info.scce.dime.gui.gui.GUISIBModal
import info.scce.dime.gui.gui.GeneralStyling
import info.scce.dime.gui.gui.Margin
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.Size
import info.scce.dime.gui.gui.Tooltip
import java.util.LinkedList

import static info.scce.dime.generator.gui.utils.GUIGenerator.*

/**
 * Template to generate HTML style attributes for all components
 */
class HTMLCSSTemplate {
	
	/**
	 * Generates the style attribute for the given component
	 */
	def CharSequence printStyle(MovableContainer cmc)
	'''«printStyleWith(cmc,null)»'''
		
	/**
	 * Generates the style attribute for the given component
	 * and includes the given additional CSS code to the attribute value
	 */
	def printStyleWith(MovableContainer cmc,String additionalCss)
	'''
	«IF isStyled(cmc) || (!additionalCss.nullOrEmpty)»
		«IF preview»
		style="«cmc.cssForCMC(additionalCss)»"
		«ELSE»
		[ngStyle]="«cmc.justStyle(additionalCss)»"
		«ENDIF»
		«IF cmc.generalStyle.tooltip != null»«createTooltip(cmc.generalStyle.tooltip)»«ENDIF»
	«ENDIF»
	«cmc.generalStyle.customAttributes»
	'''
	
	/**
	 * Generates the style attribute for the given component
	 * and includes the given additional CSS code to the attribute value
	 */
	def printStyle(GeneralStyling styling)
	'''
	«IF styling != null»
		«IF preview»
		style="«styling.cssForGS(null)»"
		«ELSE»
		[ngStyle]="«styling.justStyle(null)»"
		«ENDIF»
		«styling.customAttributes»
	«ENDIF»
	'''
	
	/**
	 * Generates just the CSS attribute for the given component
	 * and includes the given additional CSS code to the attribute value
	 */
	def justStyle(MovableContainer cmc,String additionalCss)
	'''«cmc.generalStyle.justStyle(additionalCss)»'''
	
	def justStyle(GeneralStyling gs,String additionalCss)
	'''{«parseCSS(gs.cssForGS(additionalCss).toString)»}'''
	
	def cssForCMC(MovableContainer cmc,String additionalCss)
	'''«IF cmc.generalStyle != null» «cmc.generalStyle.cssForGS(additionalCss)» «ENDIF»'''
		
	def cssForGS(GeneralStyling gs, String additionalCss)
	'''«height(gs)»«margin(gs)»«ConventionHelper.replaceDataBindingString(gs.rawContent)»«IF !additionalCss.nullOrEmpty» «additionalCss»«ENDIF»'''
		
	
	
	def parseCSS(String s)
	{
		var output = new LinkedList;
		//split in single statements
		var statements = s.split(";");
		for(statement:statements)
		{
			var parts = statement.split(":");
			if(parts.size==2){
				output += ''' '«parts.get(0).trim»':'«parts.get(1).trim»' '''
			}
			else if(parts.size==1){
				if(!parts.get(0).trim.empty){
					output += ''' '«parts.get(0).trim»':null '''					
				}
			}
		}
		return output.join(",").replace("\n", "");
	}
	
//	/**
//	 * Generates the additional attribute for the given component
//	 */
//	def additionalAttributes(MovableContainer cmc,String additionalCss)
//	'''
//	«IF isStyled(cmc) || (!additionalCss.nullOrEmpty) || (cmc.generalStyle != null)»
//		«cmc.customAttributes»
//	«IF cmc.generalStyle.tooltip != null»«createTooltip(cmc.generalStyle.tooltip)»«ENDIF»
//	«ENDIF»
//	'''
	
	/**
	 * Generates the dime attribute for the given component
	 * and includes the given attributes of the general style.
	 * The CSS selector for the defined attributes is $('*[dime="defined-attribute:value"]') 
	 * The selector can be used in different equation types:
	 * Equal dime="value"
	 * Contains dime~="value"
	 * Starts with dime|="value"
	 * Ends with dime$="value"
	 */
	def customAttributes(GeneralStyling gs,String property)
	'''
	«IF gs != null && !gs.attributes.empty»
		«" "»[«property»]="{«FOR attr:gs.attributes.map[n|ConventionHelper.replaceDataBindingString(n).parseCSS] SEPARATOR ","»«attr»«ENDFOR»}"
	«ENDIF»
	'''
	
	/**
	 * Generates the dime attribute for the given component
	 * and includes the given attributes of the general style.
	 * The CSS selector for the defined attributes is $('*[dime="defined-attribute:value"]') 
	 * The selector can be used in different equation types:
	 * Equal dime="value"
	 * Contains dime~="value"
	 * Starts with dime|="value"
	 * Ends with dime$="value"
	 */
	private def customAttributes(GeneralStyling gs)
	{
		return gs.customAttributes("dimeAttributes")
	}
	
	/**
	 * Checks, if the given component is styled
	 */
	private def boolean isStyled(MovableContainer cmc)
	{
		if(cmc.generalStyle==null)return false;
		return 
		cmc.generalStyle.marginBottom != Margin.NONE
		|| cmc.generalStyle.tallness != ContainerHeight.FITTING
		|| cmc.generalStyle.marginTop != Margin.NONE
		|| !cmc.generalStyle.rawContent.nullOrEmpty
		|| cmc.generalStyle.tooltip!=null;
	}
	
	/**
	 * Adds style attributes, if the tallness is set.
	 * This option restricts the maximal height and adds a scroll bar
	 */
	private def height(GeneralStyling gs){
		if(gs==null)return "";
		return '''«IF gs.tallness != ContainerHeight.FITTING» overflow: auto; height:«getPixelHeight(gs.tallness)»vh; «ENDIF»'''
	}
	
	/**
	 * Converts the given height constant to the relative height
	 */
	private def getPixelHeight(ContainerHeight ch)
	{
		switch(ch)
		{
			case ContainerHeight.FIFTH : return "20"
			case ContainerHeight.QUARTER : return "25"
			case ContainerHeight.THIRD : return "33"
			case ContainerHeight.HALF : return "50"
			case ContainerHeight.FULL : return "100"
			case ContainerHeight.FITTING : return "0"
		}
	}
	
	/**
	 * Returns the margin style attributes for the given component
	 */
	private def margin(GeneralStyling gs) {
		if(gs==null)return "";
		return '''«IF gs.marginTop != Margin.NONE» margin-top: «getMatginPixel(gs.marginTop)»;«ENDIF»«IF gs.marginBottom != Margin.NONE» margin-bottom: «getMatginPixel(gs.marginBottom)»;«ENDIF»'''
	}
	
	/**
	 * Generates the attributes placed in the tag to enable
	 * a tooltip with the specified content
	 */
	def createTooltip(Tooltip tt)
	'''
	data-toggle="tooltip" [title]="'«FOR s:tt.content SEPARATOR " "»«ConventionHelper.replaceDataBindingString(s.rawContent)»«ENDFOR»'" data-placement="«tt.position.literal.toLowerCase»" data-trigger="«tt.event.literal.toLowerCase»" 	
	'''
	
	def CharSequence printColorClass(GUISIBModal modal){
		if(modal.styling == null)return HTMLColoringTemplate.createClass(Coloring.DEFAULT);
		return HTMLColoringTemplate.createClass(modal.styling.color);
	}
	
	/**
	 * Returns the defined button size CSS class
	 */
	def CharSequence printSizeClass(GUISIBModal modal){
		if(modal.styling == null)return "";
		if(modal.styling.size != Size.DEFAULT)return " btn-"+HTMLSizeTemplate.create(modal.styling.size);
		return ""
	}
	
	/**
	 * Converts the given margin constant to pixel
	 */
	private def getMatginPixel(Margin m)
	{
		switch(m)
		{
			case m == Margin.TRIPLE: return "45px"
			case m == Margin.DOUBLE: return "30px"
			case m == Margin.SINGLE: return "15px"
			
		}
		return "";
	}
	
}
