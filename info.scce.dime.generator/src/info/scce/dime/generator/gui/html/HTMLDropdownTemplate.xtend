/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.enums.HTMLSizeTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.ConventionHelper
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.Dropdown
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.Size

/**
 * Template to generate the Angular component HTML template code for a drop down component
 */
class HTMLDropdownTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given drop down component
	 */
	def create(Dropdown dropdown){
		var html = this.pre(dropdown).toString;
		html += dropdown.createEntries
		html += this.post(dropdown);
		return html;
	}
	
	def createEntries(Dropdown dropdown)
	'''
	«FOR node : dropdown.nodes.filter(MovableContainer).sortBy[y]»
	<li>
		
			«IF node instanceof Button»
				<a
				data-cinco-id="«node.id.escapeString»"
				«node.printNgIfFor»«node.printStyle»
				«IF node.isDisabled»class="disabled"«ENDIF»
				«IF !preview»(click)="«new HTMLButtonTemplate().getAction(node,node.findFirstParent(Form))»"«ENDIF»
				>«new HTMLButtonTemplate().getButtonLabel(node)»</a>
			«ENDIF»
			«IF node instanceof LinkProcessSIB»
				<a
				data-cinco-id="«node.id.escapeString»"
				«node.printNgIfFor»«node.printStyle»
				«IF node.isDisabled»class="disabled"«ENDIF»
				«new HTMLLinkSIBTemplate().createHref(node)»
				>«new HTMLLinkSIBTemplate().getButtonLabel(node)»</a>
			«ENDIF»
	</li>
	«ENDFOR»
	'''

	/**
	 * Generates the opening HTML tags for the given drop down component.
	 * This includes the label of the drop down
	 */
	private def pre(Dropdown dropdown)'''
	<div «dropdown.printNgIfFor»«dropdown.printStyle» class="btn-group«IF dropdown.dropUp» dropup«ENDIF»«getSize(dropdown)»" role="group">
		<button type="button" class="btn btn-«getColor(dropdown)» dropdown-toggle" «dropdown.printStyle» [disabled]="isBusy()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-cinco-id="«dropdown.id.escapeString»">
	    	«new HTMLIconTemplate().icons(dropdown.icon,ConventionHelper.replaceDataBinding(dropdown.label))»
	    	<span class="caret"></span>
	    </button>
	    <ul class="dropdown-menu">
	'''
	
	/**
	 * Generates the closing HTML tags for the given drop down component.
	 */
	private def post(Dropdown dropdown)
	'''
		</ul>
	</div>
	'''
	
	/**
	 * Returns the CSS class depended on the defined size of the given drop down
	 */
	private def getSize(Dropdown dropdown){
		if(dropdown.styling==null)return "";
		if(dropdown.styling.size != Size.DEFAULT)return " btn-group-"+HTMLSizeTemplate.create(dropdown.styling.size);
		return ""
	}
	
	/**
	 * Returns the CSS class depended on the defined color of the given drop down
	 */
	private def getColor(Dropdown dropdown){
		if(dropdown.styling==null)return HTMLColoringTemplate.createClass(Coloring.DEFAULT);
		return HTMLColoringTemplate.createClass(dropdown.styling.color);
	}
}
