/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.gui.utils.helper.DataDartHelper
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.ProgressBar

/**
 * Template to generate the Angular component HTML template code for a progress bar component
 */
class HTMLProgressBarTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given progress bar component
	 */
	def create(ProgressBar cpb)
	'''
	<div «cpb.printNgIfFor» «cpb.printStyle» class="progress">
		<div class="progress-bar «getColor(cpb)»«IF cpb.styling!=null»«IF cpb.styling.striped» progress-bar-striped«ENDIF»«IF cpb.styling.animate» active«ENDIF»«ENDIF»" role="progressbar"
		«IF preview»
			style="width:50%;min-width:2em;"
		«ELSE»
			[ngStyle]="{ 'width': («DataDartHelper.getBindedDataName(cpb,DataAccessType.^FOR,MODE.GET)»«IF !cpb.percent»*100.0«ENDIF»).toString() + '%', 'min-width':'2em' }"
		«ENDIF»
		>
		«IF preview»
			50%
		«ELSE»
			{{«DataDartHelper.getBindedDataName(cpb,DataAccessType.^FOR,MODE.GET)»«IF !cpb.percent»*100.0«ENDIF» | number:'1.2-2'}}%
		«ENDIF»
		</div>
	</div>
	'''
	
	/**
	 * Returns the CSS class for the color of the given progress bar component
	 */
	private def CharSequence getColor(ProgressBar cpb)
	{
		if(cpb.styling==null)return '''progress-bar-«HTMLColoringTemplate.createClass(Coloring.DEFAULT)»'''
		return
		'''
		«IF cpb.styling.color != Coloring.DEFAULT || cpb.styling.color != Coloring.BLUE»
		 progress-bar-«HTMLColoringTemplate.createClass(cpb.styling.color)»
		«ENDIF»
		'''
	}
	
}
