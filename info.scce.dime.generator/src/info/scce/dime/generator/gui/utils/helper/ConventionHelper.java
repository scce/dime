/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.utils.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EObject;

import graphmodel.Edge;
import graphmodel.GraphModel;
import graphmodel.Node;
import info.scce.dime.generator.util.JavaIdentifierUtils;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.Input;

/**
 * The convention helper provides multiple utility methods
 * for names, ids and parsing of angular expressions
 * @author zweihoff
 *
 */
public class ConventionHelper {
	
	public final static int SEP_MODE_MUSTAGE = 0;
	public final static int SEP_MODE_NONE = 1;
	public final static int SEP_MODE_STRING = 2;
	
	/**
	 * Checks if a given string can be interpreted as an integer
	 * @param s inspected string
	 * @return boolean
	 */
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
	
	/**
	 * Returns the input output name for a given node
	 * Is used for embedded interactions outputs and inputs in GUIs
	 * @param node of a GUI model
	 * @return String
	 */
	public static String getIOName(Node node){
		return "io"+getCompilingString(cincoID(node)).toLowerCase();
	}
	
	/**
	 * Return the event name for a given EObject, which has can be a GUI button, Process endSIB or GUI Plugin output.
	 * The name contains the id of the object and the label
	 * @param node Button | EndSIB | Output
	 * @return String
	 */
	public static String getEventName(String branchName){
		return "action"+JavaIdentifierUtils.escapeDart(getCompilingString(branchName)).toLowerCase()+"event";
	}
	
	
	/**
	 * Removes invalid characters for a Dart compiler from the given string s.
	 * Sets the first char to lower case
	 * @param s String
	 * @return String
	 */
	private static String getCompilingString(String s)
	{
		if(s == null)return "";
		s = s.trim();
		s = s.replace(" ", "");
		s = s.replace("ä","ae");
        s = s.replace("ö","oe");
        s = s.replace("ü","ue");
        s = s.replace("Ä","AE");
        s = s.replace("Ü","UE");
        s = s.replace("Ö","OE");
        s = s.replace("!", "");
        s = s.replace("?", "");
        s = s.replace("_", "xx");
        s = s.replace("-", "zZz");
        return Character.toLowerCase(s.charAt(0)) + s.substring(1);
	}
	
	/**
	 * Returns the class name of a GUI Angular component class.
	 * Removes invalid chars and sets the first char to upper case.
	 * @param variableName String
	 * @return valid String
	 */
	public static String getComponentClassName(String variableName)
	{
		String s = getCompilingString(variableName);
		return Character.toUpperCase(s.charAt(0)) + s.substring(1);
	}
	
	/**
	 * Returns the id of a Node without - and _
	 * @param node
	 * @return
	 */
	public static String cincoID(Node node)
	{
		return node.getId().replace("-", "").replace("\\_", "");
	}
	
	/**
	 * Returns the id of a EObject without - and _
	 * @param node
	 * @return
	 */
	public static String cincoID(EObject node)
	{
		if(node instanceof Node)return cincoID((Node)node);
		if(node instanceof Edge)return cincoID((Edge)node);
		return new String(""+node.hashCode()).replace("-", "").replace("\\_", "");
	}
	
	/**
	 * Returns the id of a Edge without - and _
	 * @param edge
	 * @return
	 */
	public static String cincoID(Edge edge)
	{
		return edge.getId().replace("-", "").replace("\\_", "");
	}
	
	/**
	 * Returns the id of a GraphModel without - and _
	 * @param graphModel
	 * @return
	 */
	public static String cincoID(GraphModel graphModel)
	{
		return graphModel.getId().replace("-", "").replace("\\_", "");
	}
	
	/**
	 * Removes invalid chars for a URL from the given String s
	 * @param s
	 * @return
	 */
	public static String toLink(String s)
	{
		String name = s.replace("/", "").replace("#", "").replace("$", "").replace("%", "").trim();
		return Character.toUpperCase(name.charAt(0)) + name.substring(1);
	}
	
	/**
	 * Parses the expressions present in the given String rawContent
	 * and eliminates the double curly brackets.
	 * @param rawContent
	 * @return
	 */
	public static String replaceDataBinding(String rawContent){
		return replaceDataBinding(rawContent, SEP_MODE_MUSTAGE);
	}
	
	/**
	 * Parses the expressions present in the given String rawContent
	 * and replaces the double curly brackets with string concatenation.
	 * @param rawContent
	 * @return
	 */
	public static String replaceDataBindingString(String rawContent){
		
		return replaceDataBinding(rawContent, SEP_MODE_STRING);
	}
	
	/**
	 * Parses all expressions present in the given String rawContent.
	 * An expression has to placed between double curly brackets.
	 * @param rawContent
	 * @param mustage If true, the double curly brackets are removed
	 * @return
	 */
	public static String replaceDataBinding(String rawContent,int speratorMode)
	{
		String refinedData = "";
		if(rawContent == null)return refinedData;
		Pattern p = Pattern.compile("\\{\\{(.+?)\\}\\}");
		Matcher m = p.matcher(rawContent);
		int actIndex = 0;
		while(m.find()){
			String pipe ="";
			int start = m.start();
			int end = m.end();
		    String exprValue = rawContent.substring(start+2, end-2);
		    if(exprValue.indexOf("|")>-1) {
		    		pipe = exprValue.substring(exprValue.indexOf("|"),exprValue.length());
		    		exprValue = exprValue.substring(0,exprValue.indexOf("|")).trim();    	
		    }
		    String[] parts = exprValue.split("\\.");
		    String[] compiledExpression = new String[parts.length];
		    for(int i=0;i<parts.length;i++)
		    {
		    	//System.out.println(JavaIdentifierUtils.escape(parts[i]));
		    		compiledExpression[i] = JavaIdentifierUtils.escapeDart(parts[i].trim());
		    }
		    if(speratorMode==SEP_MODE_MUSTAGE){
			    	refinedData += rawContent.substring(actIndex, start)+"{{" + String.join("?.",compiledExpression) + pipe +"}}";		    	
		    }
		    else if(speratorMode==SEP_MODE_STRING){
		    	refinedData += escapeStaticText(rawContent.substring(actIndex, start))+"'+" + String.join("?.",compiledExpression) + pipe +".toString()+'";		    	
		    }
		    else{
		    		refinedData += rawContent.substring(actIndex, start) + String.join("?.",compiledExpression) + pipe;
		    }
		    actIndex = end;
		}
		if(refinedData.isEmpty()){
			if(speratorMode==SEP_MODE_STRING){
				return escapeStaticText(rawContent);
			}
			return rawContent;
		}
		if(actIndex < rawContent.length()){
			if(speratorMode==SEP_MODE_STRING){
				return refinedData + escapeStaticText(rawContent.substring(actIndex));
			}
			return refinedData + rawContent.substring(actIndex);
		}
		return refinedData;
	}
	
	private static String escapeStaticText(String s) {
		return s.replace("'", "\\'");
	}
	
	/**
	 * Return the tag name for a given form component
	 * @param form Form
	 * @return tag name
	 */
	public static String getFormName(Form form)
	{
		return "form"+cincoID(form)+"Form";
	}
	
	/**
	 * Returns the name of an form field for a form component
	 * @param input Input, superclass for all textual form fields
	 * @return name of the Angular Form Control variable
	 */
	public static String getFormInputName(Input input)
	{
		return "input_"+cincoID(input);
	}
}
