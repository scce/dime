/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.utils

import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.dart.functionality.AngularIFFORTemplate
import info.scce.dime.generator.gui.data.SelectiveExtension
import info.scce.dime.generator.gui.html.HTMLCSSTemplate
import info.scce.dime.generator.gui.utils.helper.IOHelper
import info.scce.dime.generator.gui.utils.helper.TemplateHelper
import info.scce.dime.generator.util.JavaIdentifierUtils
import info.scce.dime.gui.helper.GUIExtension
import java.util.regex.Pattern

abstract class GUIGenerator {
	
	val static HTTP_PATTERN = Pattern.compile('''^https?:\/\/''')
	
	protected GenerationContext genctx
	
	protected extension GUIExtension _guiExtension
	protected extension DataExtension _dataExtension
	protected extension SelectiveExtension _selectiveExtension
	protected extension TemplateHelper _templateHelper
	protected extension IOHelper _ioHelper
	protected extension AngularIFFORTemplate _iffor
	protected extension HTMLCSSTemplate _htmlcssTemplate = new HTMLCSSTemplate
	
	
	new() {
		setGenerationContext(GenerationContext.instance)
	}
	
	def setGenerationContext(GenerationContext genctx) {
		this.genctx = genctx
		this._guiExtension = genctx.guiExtension
		this._dataExtension = _guiExtension.dataExtension
		this._selectiveExtension = new SelectiveExtension(genctx)
		this._templateHelper = new TemplateHelper(genctx)
		this._ioHelper = new IOHelper(genctx)
		this._iffor = new AngularIFFORTemplate(genctx)
	}
	
	static def escapeDart(CharSequence s) {
		JavaIdentifierUtils.escapeDart(s)
	}
	
	static def escapeString(CharSequence s) {
		JavaIdentifierUtils.escapeString(s.toString)
	}
	
	static def escapeJava(CharSequence s) {
		JavaIdentifierUtils.escapeJava(s)
	}
	
	static def preview() {
		false
	}
	
	static def boolean isHTTP(String pathString) {
		HTTP_PATTERN.matcher(pathString).find
	}
	
}
