/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.utils.helper

import graphmodel.ModelElementContainer
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.enums.DataAccessType
import info.scce.dime.generator.gui.enums.MODE
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.GUIExtension

import static extension info.scce.dime.generator.gui.utils.GUIGenerator.*

/**
 * Utility class to create the inputs and outputs for Angular components representing
 * SIBs
 */
class IOHelper {
	
	protected extension GUIExtension _guiExtension
	protected extension DataExtension _dataExtension
	
	new(GenerationContext genctx) {
		this._guiExtension = genctx.guiExtension
		this._dataExtension = genctx.guiExtension.dataExtension
	}
	
	/**
	 * Creates the input declaration for a event listener used in an interaction process
	 * to trigger an event of a GUI SIB.
	 * Declares an input for every port. Static as well as dynamic.
	 * If no data flow reaches the port, the default value for the specified type are passed.
	 */
	def processEventInputs(info.scce.dime.process.process.EventListener p,CharSequence map)
	{
		return 
		'''
	  	«FOR input :p.inputs»
		  	«IF input instanceof info.scce.dime.process.process.InputStatic || input.incoming.empty»
		  	«map»['«input.name.escapeString»']=«input.defaultValue»;
		  	«ELSE»
		  	«map»['«input.name.escapeString»']=input.«input.name.escapeDart»;
		  	«ENDIF»
	  	«ENDFOR»
		'''
	}
	
	
	
	
	/**
	 * Creates the input declaration for a GUI SIB used in an GUI model
	 * placed in the corresponding tag for the GUI SIB Angular component.
	 * Declares an input for every port. Static as well as dynamic.
	 * If no data flow reaches the port, the default value for the specified type are passed.
	 */
	def guiInputs(GUISIB p,GUI dispatchedGUI)
	'''
	«FOR inputVariable:dispatchedGUI.inputVariables»
		«{
			val input = inputVariable.findCorrespondingInputPort(p)
			if(input!=null){
				if(!input.incoming.empty) {
					var needsExplicitCast = false
					if(inputVariable instanceof ComplexVariable) {
						if(p instanceof DispatchedGUISIB) {
							needsExplicitCast = true
						} else {
							
							val connectedNode = (input as ComplexInputPort).incomingComplexReads.get(0).sourceElement
							if(connectedNode instanceof ComplexAttribute) {
								needsExplicitCast = !connectedNode.attribute.dataType.originalType.equals((input as ComplexInputPort).dataType.originalType)
							}
							if(connectedNode instanceof ComplexVariable) {
								needsExplicitCast = !connectedNode.dataType.originalType.equals((input as ComplexInputPort).dataType.originalType)
							}
							
						}
					}
					'''[«input.name.escapeDart»]="«IF needsExplicitCast»«DyWASelectiveDartGenerator.prefix((inputVariable as ComplexVariable).dataType.rootElement)».«(inputVariable as ComplexVariable).dataType.rootElement.modelName.escapeDart.toFirstUpper»CastUtil.castTo«IF inputVariable.isIsList»List«ENDIF»«(inputVariable as ComplexVariable).dataType.name.escapeDart.toFirstUpper»(«ENDIF»«DataDartHelper.getBindedDataName(input,DataAccessType.^FOR,MODE.GET)»«IF needsExplicitCast»)«ENDIF»"'''
				}
				else if(input instanceof InputStatic)'''[«input.name.escapeDart»]="«input.defaultValue»"'''
//				else'''[«input.name.escapeDart»]="«input.defaultValue»"'''
			}
//			else{
//				if(inputVariable instanceof PrimitiveVariable)'''[«inputVariable.name.escapeDart»]="«inputVariable.dataType.toData.defaultValue»"'''
//				else'''[«inputVariable.name.escapeDart»]="«(inputVariable as ComplexVariable).defaultValue»"'''
//			}
		}»
	«ENDFOR»
	'''
	
	def IO findCorrespondingInputPort(Variable variable, GUISIB cguisib)
	{
		cguisib.IOs.findFirst[name.equals(variable.name)]
	}
	
	/**
	 * Creates the input declaration for a GUI SIB used in an GUI model
	 * placed in the corresponding tag for the GUI SIB Angular component.
	 * Declares an input for every port. Static as well as dynamic.
	 * If no data flow reaches the port, the default value for the specified type are passed.
	 */
	def Iterable<String> guiDispatchableInputsData(GUISIB p)
	{
		p.guiDispatchableInputs.map[inputGUIData]
	}
	
	def Iterable<ComplexInputPort> guiDispatchableInputs(GUISIB p)
	{
		p.IOs.filter(ComplexInputPort)
	}
	
	def String getInputGUIData(IO input)
	{
		if(!input.incoming.empty)
		  	DataDartHelper.getBindedDataName(input,DataAccessType.^FOR,MODE.GET)
		else 
		  	input.defaultValue
	  	
	}
	
	/**
	 * Creates the input declaration for a GUI SIB event used in an GUI model
	 * placed in the corresponding tag for the GUI SIB Angular component.
	 * Declares an input for every port. Static as well as dynamic.
	 * If no data flow reaches the port, the default value for the specified type are passed.
	 */
	def eventInputs(EventListener p)
	'''
  	«FOR input :p.IOs»
	  	«IF !input.incoming.empty»
	  	evtMap['«input.name.escapeString»']=«DataDartHelper.getBindedDataName(input,DataAccessType.^FOR,MODE.GET)»;
	  	«ELSE»
	  	evtMap['«input.name.escapeString»']=«input.defaultValue»;
	  	«ENDIF»
  	«ENDFOR»
	'''
	
	/**
	 * Creates the input declaration for a GUI plug in SIB used in an GUI model
	 * placed in the corresponding tag for the GUI plug in SIB Angular component.
	 * Declares an input for every port. Static as well as dynamic.
	 * If no data flow reaches the port, the default value for the specified type are passed.
	 */
	def guiPluginInputs(GUIPlugin p)
	'''
  	«FOR input :p.IOs»
	  	«IF !input.incoming.empty»
	  		[«input.name.escapeDart»]="«DataDartHelper.getBindedDataName(input,DataAccessType.^FOR,MODE.GET)»"
	  	«ELSE»
	  		[«input.name.escapeDart»]="«input.defaultValue»"
	  	«ENDIF»
  	«ENDFOR»
	'''
	
	/**
	 * Creates the input declaration for a GUI plug in SIB used in an GUI model
	 * placed in the corresponding tag for the GUI plug in SIB Angular component.
	 * Declares an input for every port. Static as well as dynamic.
	 * If no data flow reaches the port, the default value for the specified type are passed.
	 */
	def controlSIBInputs(ControlSIB p)
	'''
  	«FOR input :p.IOs»
	  	«IF !input.incoming.empty»
	  		[«input.name.escapeDart»]="«DataDartHelper.getBindedDataName(input,DataAccessType.^FOR,MODE.GET)»"
	  	«ELSE»
	  		[«input.name.escapeDart»]="«input.defaultValue»"
	  	«ENDIF»
  	«ENDFOR»
	'''
	
	/**
	 * Creates the output declaration for a GUI SIB used in an interaction process
	 * Declares an output for every branch and connects it to a method in the IP Angular component
	 * which is called when the branch is reached.
	 */
	def guiOutputs(info.scce.dime.process.process.GUISIB p)
	'''
	«FOR branch: p.abstractBranchSuccessors»
  	(«ConventionHelper.getEventName(branch.name)») = "event«branch.id.escapeDart»«branch.name.escapeDart»Trigger(\$event)"
  	«ENDFOR»
	'''
	
	/**
	 * Creates the output declaration for a component used in a GUI model
	 * Declares an output for every button, embedded IP, embedded GUI SIB, embedded GUI plug in and connects it to a method in the GUI model Angular component
	 * which is called when the event is thrown.
	 * The string s defines a prefix to the Stream Controller variable name for buttons in the component.
	 */
	def CharSequence componentOutputs(ModelElementContainer cec,String s)
	'''
	«FOR branchRep: cec.getGUIBranches(true).map[name].toSet»
  	(«ConventionHelper.getEventName(branchRep)») = "«ConventionHelper.getEventName(branchRep)».add($event)"
  	«ENDFOR»
	'''
	
	/**
	 * Creates the output declaration for a GUI SIB used in a GUI model
	 * Declares an output for every button, embedded IP, embedded GUI SIB, embedded GUI plug in placed in the corresponding GUI model 
	 * and connects it to a method in the GUI model Angular component
	 * which is called when the event is thrown.
	 * The string s defines a prefix to the Stream Controller variable name for buttons in the component.
	 */
	def CharSequence guiOutputs(GUI parentGUI,GUI dispatchedGUI,boolean isModal,GUISIB sib)
	'''
	«FOR branchName:parentGUI.dispatchedOutputs(dispatchedGUI).map[name].toSet»
		
		(«ConventionHelper.getEventName(branchName)»)="«IF sib.modalCloseButton(branchName)»closeModalTrigger(null)«ELSE»«ConventionHelper.getEventName(branchName)».add(«IF isModal»closeModalTrigger(«ENDIF»$event«IF isModal»)«ENDIF»)«ENDIF»"
		
	«ENDFOR»
	'''
		
	
	
	/**
	 * Creates the output declaration for a GUI SIB used in a GUI model
	 * Declares an output for every button, embedded IP, embedded GUI SIB, embedded GUI plug in placed in the corresponding GUI model 
	 * and connects it to a method in the GUI model Angular component
	 * which is called when the event is thrown.
	 * The string s defines a prefix to the Stream Controller variable name for buttons in the component.
	 */
	def CharSequence guiOutputs(GUISIB parentGUI,GUI dispatchedGUI,GUISIB sib)
	{
		return parentGUI.gui.guiOutputs(dispatchedGUI,parentGUI.modal!=null,sib)
	}
	
	/**
	 * Creates the output declaration for a GUI SIB used in a GUI model
	 * Declares an output for every button, embedded IP, embedded GUI SIB, embedded GUI plug in placed in the corresponding GUI model 
	 * and connects it to a method in the GUI model Angular component
	 * which is called when the event is thrown.
	 * The string s defines a prefix to the Stream Controller variable name for buttons in the component.
	 */
	def CharSequence guiOutputs(GUI parentGUI,boolean isModal,GUISIB sib)
	'''
	«FOR branchName:parentGUI.dispatchedOutputs.map[name].toSet»
		(«ConventionHelper.getEventName(branchName)»)="«IF sib.modalCloseButton(branchName)»closeModalTrigger(null)«ELSE»«ConventionHelper.getEventName(branchName)».add(«IF isModal»closeModalTrigger(«ENDIF»$event«IF isModal»)«ENDIF»)«ENDIF»"
	«ENDFOR»
	'''
	
	
	
}
