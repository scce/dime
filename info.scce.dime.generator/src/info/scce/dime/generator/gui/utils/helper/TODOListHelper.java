/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.gui.utils.helper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
/**
 * A dedicated helper class for the todolist special component
 * @author zweihoff
 *
 */
public class TODOListHelper {
	
	/**
	 * The given descriptions are parsed to be written in Dart code.
	 * The description is scanned and all expressions are collected.
	 * Then the double curly brackets are removed and the single expressions are
	 * used in simple string concatenation.
	 * Example: {{a}} and {{b}} -> a+' and '+b
	 * @param description
	 * @param prefix The prefix is written before every parsed expression
	 * @return
	 */
	public static String parseDescription(String description,String prefix)
	{
		String refinedData = "";
		if(description == null)return refinedData;
		Pattern p = Pattern.compile("\\{\\{(.+?)\\}\\}");
		Matcher m = p.matcher(description);
		int actIndex = 0;
		while(m.find()){
			int start = m.start();
			int end = m.end();
		    String exprValue = description.substring(start+2, end-2);
		   
		    String refinedExpression = "";
		    boolean even = true;
		    for (String element: exprValue.split("'")) {
		    	if (even) {
		    		refinedExpression += Arrays.stream(element.split("\\s+"))
				    		.map(s -> (s.matches("^[_a-zA-Z].*") ? prefix + s : s))
				    		.collect(Collectors.joining(" "));
		    	}
		    	else {
		    		refinedExpression += "'" + element + "'";
		    	}
		    	even = !even;
		    }
		    		    		    
		    refinedData += description.substring(actIndex,start);
		    refinedData += "' + (" + refinedExpression + ") + '";
		    actIndex = end;
		}
		if(refinedData.isEmpty())return description;

	    refinedData += description.substring(actIndex,description.length());

		return refinedData;
	}

}
