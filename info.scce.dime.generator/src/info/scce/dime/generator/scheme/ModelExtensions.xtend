/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.scheme

import de.ls5.dywa.entities.DBIdentified
import de.ls5.dywa.entities.object.DBField
import de.ls5.dywa.entities.object.DBPackage
import de.ls5.dywa.entities.object.DBType
import de.ls5.dywa.entities.property.PropertyType
import java.util.Collections
import java.util.LinkedList
import java.util.List
import java.util.Map
import java.util.stream.Collectors

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class ModelExtensions {
	val String packageName;
	val Map<DBType, List<DBType>> inheritance;
	val Map<Long,String> dywaIdToCincoIdMap;

	new(String packageName, Map<DBType, List<DBType>> inheritance) {
		this.packageName = packageName;
		this.inheritance = inheritance;
		this.dywaIdToCincoIdMap = Collections.emptyMap();
	}

	new(String packageName, Map<DBType, List<DBType>> inheritance,Map<Long,String> dywaIdToCincoIdMap) {
		this.packageName = packageName;
		this.inheritance = inheritance;
		this.dywaIdToCincoIdMap=dywaIdToCincoIdMap;
	}

	def renderClassName(DBType type) '''«type.getName.escapeJava»'''

	def renderFullClassName(DBType type) '''«type.renderClassName»«IF type.isEnumerable»Entity«ELSEIF !type.abstractType»Impl«ELSE»Abs«ENDIF»'''

	def renderCanonicalClassName(DBType type, String entityOrController) {
		var joined = type.renderPackageSuffix;
		if (joined.isEmpty) {
			'''«packageName».«entityOrController».«type.renderClassName»'''
		} else {
			'''«packageName».«entityOrController».«joined».«type.renderClassName»'''
		}
	}

	def renderFullCanonicalClassName(DBType type, String entityOrController) {
		var joined = type.renderPackageSuffix;
		if (joined.isEmpty) {
			'''«packageName».«entityOrController».«type.renderFullClassName»'''
		} else {
			'''«packageName».«entityOrController».«joined».«type.renderFullClassName»'''
		}
	}

	def renderFullPackageName(DBType type, String entityOrController) {
		var suffix = type.renderPackageSuffix;
		var prefix = packageName + '.' + entityOrController;
		if (suffix.isEmpty) {
			'''«prefix»'''
		} else {
			'''«prefix».«suffix»'''
		}
	}

	def renderPackageSuffix(DBType type) {
		var packageNames = new LinkedList<String>;
		var DBPackage current = type.getDyWAPackage;
		while (current !== null && !current.getName.isEmpty) {
			packageNames.addFirst(current.getName.escapeJava);
			current = current.parentPackage;
		}
		return String.join(".", packageNames);
	}

	def renderInterfaceExtensions(DBType type) {
		var concat = inheritance.get(type).stream().map[t | if (t.isEnumerable) t.renderCanonicalClassName("entity") + "Interface" else t.renderCanonicalClassName("entity") ].collect(Collectors.joining(", "));
		if (!concat.isEmpty) {
			'''«concat» '''
		} else {
			'''info.scce.dime.util.Identifiable'''
		}
	}

	def renderMethodSuffix(DBField field) '''«IF field.isList && field.isComplex»«field.rootOverriddenField.name.escapeJava»_«field.typeConstraint.name.escapeJava»«ELSE»«field.rootOverriddenField.name.escapeJava»«ENDIF»'''

	def renderSimpleTableName(DBType type) '''«type.getName.replaceAll("\\W", "_")»'''

	def renderTableName(DBType type){
		if(type.isEnumerable){
		return NameGenerator.generateIdentifierName("e_",type.cincoId,type.name)
		}
		return NameGenerator.generateIdentifierName("t_",type.cincoId,type.name)
	}

	def renderTableName(DBType type, String additionalPrefix){
    		if(type.isEnumerable){
    		return NameGenerator.generateIdentifierName("e_",additionalPrefix+"_"+type.cincoId,type.name)
    		}
    		return NameGenerator.generateIdentifierName("t_",additionalPrefix+"_"+type.cincoId,type.name)
    	}

	def renderJoinTableName(DBField field, DBType implementer) {
        return NameGenerator.generateIdentifierName("r_",field.cincoId,implementer.name.replaceAll("\\W+","*")+"_"+field.getName)
 	}

	def renderJoinTableName(DBField field){
    	return NameGenerator.generateIdentifierName("r_",field.cincoId,field.type.name.replaceAll("\\W+","*") +"_"+field.getName)
    }

	def renderFileJoinTableName(DBField field, DBType implementer){
     	return NameGenerator.generateIdentifierName("r_",field.cincoId,implementer.name.replaceAll("\\W+","*") +"_"+field.getName)
    }

	def renderFileJoinTableName(DBField field){
     	return NameGenerator.generateIdentifierName("r_",field.cincoId,field.type.name.replaceAll("\\W+","*") +"_"+field.getName)
    }

	def renderCollectionTableName(DBField field, DBType implementer){
    return NameGenerator.generateIdentifierName("c_",implementer.cincoId+"_"+field.cincoId,field.getName)
    }

	def renderCollectionTableName(DBField field) {
	return NameGenerator.generateIdentifierName("c_",field.getType.cincoId+"_"+field.cincoId,field.getName)
	}

	def renderColumnName(DBField field) {
	return NameGenerator.generateIdentifierName("a_",field.cincoId,field.getName)
	}

	def getCincoId(DBIdentified field) {
		return dywaIdToCincoIdMap.getOrDefault(field.id,field.id.toString)
	}
	def renderAttributeName(String name) {
		var String[] split = name.split("\\W+");
		if (split.get(0).length > 1 && Character.isUpperCase(split.get(0).charAt(0)) && Character.isLowerCase(split.get(0).charAt(1))) {
			split.set(0, Character.toLowerCase(split.get(0).charAt(0)) + split.get(0).substring(1));
		}
		return split.join.escapeJava;
	}

	def renderMethodPropertyName(DBField field, boolean suffix) {
		switch (field.getPropertyType) {
			case PropertyType.FILE:
				'''info.scce.dime.util.FileReference'''
			case PropertyType.FILE_LIST:
				'''java.util.List<info.scce.dime.util.FileReference>'''
			default:
				return renderPropertyName(field, suffix)
		}
	}

	def renderPropertyName(DBField field, boolean suffix) {
		switch (field.getPropertyType) {
			case PropertyType.BOOLEAN:
				'''java.lang.Boolean'''
			case PropertyType.DOUBLE:
				'''java.lang.Double'''
			case PropertyType.LONG:
				'''java.lang.Long'''
			case PropertyType.STRING:
				'''java.lang.String'''
			case PropertyType.OBJECT:
				'''«field.getTypeConstraint.renderFullPackageName("entity")».«IF suffix»«field.getTypeConstraint.renderFullClassName»«ELSE»«field.getTypeConstraint.renderClassName»«ENDIF»'''
			case PropertyType.TIMESTAMP:
				'''java.util.Date'''
			case PropertyType.FILE:
				'''info.scce.dime.util.DomainFile'''
			case PropertyType.BOOLEAN_LIST:
				'''java.util.List<java.lang.Boolean>'''
			case PropertyType.DOUBLE_LIST:
				'''java.util.List<java.lang.Double>'''
			case PropertyType.LONG_LIST:
				'''java.util.List<java.lang.Long>'''
			case PropertyType.STRING_LIST:
				'''java.util.List<java.lang.String>'''
			case PropertyType.OBJECT_LIST:
				'''java.util.List<«field.getTypeConstraint.renderFullPackageName("entity")».«IF suffix»«field.getTypeConstraint.renderFullClassName»«ELSE»«field.getTypeConstraint.renderClassName»«ENDIF»>'''
			case PropertyType.TIMESTAMP_LIST:
				'''java.util.List<java.util.Date>'''
			case PropertyType.FILE_LIST:
				'''java.util.List<info.scce.dime.util.DomainFile>'''
			default:
				''''''
		}
	}

	def renderAttribute(DBField field) {
		return field.renderAttribute(field.getType);
	}

	def renderAttribute(DBField field, DBType implementer) {
		switch (field.getPropertyType) {
			case PropertyType.BOOLEAN: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.Column(name = "«field.renderColumnName»", columnDefinition="boolean")
				private java.lang.Boolean «field.getName.trim.renderAttributeName»;
			'''
			case PropertyType.DOUBLE: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.Column(name = "«field.renderColumnName»", columnDefinition="double precision")
				private java.lang.Double «field.getName.trim.renderAttributeName»;
			'''
			case PropertyType.LONG: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.Column(name = "«field.renderColumnName»", columnDefinition="bigint")
				private java.lang.Long «field.getName.trim.renderAttributeName»;
			'''
			case PropertyType.STRING: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.Column(name = "«field.renderColumnName»", columnDefinition="varchar")
				private java.lang.String «field.getName.trim.renderAttributeName»;
			'''
			case PropertyType.OBJECT:
			'''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.PERSIST)
				@javax.persistence.JoinColumn(name = "«field.renderColumnName»")
				«IF !field.getTypeConstraint.isEnumerable»
				@org.hibernate.annotations.Any(metaColumn=@javax.persistence.Column(name="«NameGenerator.generateIdentifierName("m_","Type_"+field.cincoId,field.getName)»"))
				@org.hibernate.annotations.AnyMetaDef(
					idType="long", metaType="string",
					metaValues={
						«FOR type : field.getTypeConstraint.getTransitiveSubTypes.stream.filter[!isEnumerable].filter[!isDeleted].collect(Collectors.toList) SEPARATOR ', '»
						@org.hibernate.annotations.MetaValue(targetEntity=«type.renderFullCanonicalClassName("entity")».class, value="«type.cincoId»")
						«ENDFOR»
					}
				)
				private «field.renderPropertyName(false)» «field.getName.trim.renderAttributeName»;
				«ELSE»
				@javax.persistence.ManyToOne
				private «field.renderPropertyName(true)» «field.getName.trim.renderAttributeName»;
				«ENDIF»
			'''
			case PropertyType.TIMESTAMP: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.Column(name = "«field.renderColumnName»")
				private java.util.Date «field.getName.trim.renderAttributeName»;
			'''
			case PropertyType.FILE: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.OneToOne
				@javax.persistence.JoinColumn(name = "«field.renderColumnName»")
				private info.scce.dime.util.DomainFile «field.getName.trim.renderAttributeName»;
			'''
			case PropertyType.BOOLEAN_LIST: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.ElementCollection
				@javax.persistence.CollectionTable(
					name="«field.renderCollectionTableName(implementer)»",
					joinColumns=@javax.persistence.JoinColumn(name="«implementer.renderTableName("id")»")
				)
				private java.util.List<Boolean> «field.getName.trim.renderAttributeName» = new java.util.ArrayList<>();
			'''
			case PropertyType.DOUBLE_LIST: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.ElementCollection
				@javax.persistence.CollectionTable(
					name="«field.renderCollectionTableName(implementer)»",
					joinColumns=@javax.persistence.JoinColumn(name="«implementer.renderTableName("id")»")
				)
				private java.util.List<Double> «field.getName.trim.renderAttributeName» = new java.util.ArrayList<>();
			'''
			case PropertyType.LONG_LIST: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.ElementCollection
				@javax.persistence.CollectionTable(
					name="«field.renderCollectionTableName(implementer)»",
					joinColumns=@javax.persistence.JoinColumn(name="«implementer.renderTableName("id")»")
				)
				private java.util.List<Long> «field.getName.trim.renderAttributeName» = new java.util.ArrayList<>();
			'''
			case PropertyType.STRING_LIST: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.ElementCollection
				@javax.persistence.CollectionTable(
					name="«field.renderCollectionTableName(implementer)»",
					joinColumns=@javax.persistence.JoinColumn(name="«implementer.renderTableName("id")»")
				)
				private java.util.List<String> «field.getName.trim.renderAttributeName» = new java.util.ArrayList<>();
			'''
			case PropertyType.OBJECT_LIST: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.PERSIST)
				@javax.persistence.JoinTable(name="«field.renderJoinTableName(implementer)»", joinColumns = { @javax.persistence.JoinColumn(name = "r1_«implementer.renderSimpleTableName»_id") }, inverseJoinColumns = { @javax.persistence.JoinColumn(name = "r2_«field.getTypeConstraint.renderSimpleTableName»_id") })
				«IF !field.getTypeConstraint.isEnumerable»
				@org.hibernate.annotations.ManyToAny(metaColumn=@javax.persistence.Column(name="«NameGenerator.generateIdentifierName("m_","Type"+"_"+field.cincoId,field.getName)»"))
				@org.hibernate.annotations.AnyMetaDef(
					idType="long", metaType="string",
					metaValues={
						«FOR type : field.getTypeConstraint.getTransitiveSubTypes.stream.filter[!isEnumerable].filter[!isDeleted].collect(Collectors.toList) SEPARATOR ', '»
						@org.hibernate.annotations.MetaValue(targetEntity=«type.renderFullCanonicalClassName("entity")».class, value="«type.cincoId»")
						«ENDFOR»
					}
				)
				private «field.renderPropertyName(false)» «field.getName.trim.renderAttributeName»  = new java.util.ArrayList<>();
				«ELSE»
				@javax.persistence.ManyToMany
				private «field.renderPropertyName(true)» «field.getName.trim.renderAttributeName»  = new java.util.ArrayList<>();
				«ENDIF»
			'''
			case PropertyType.TIMESTAMP_LIST: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.ElementCollection
				@javax.persistence.CollectionTable(
					name="«field.renderCollectionTableName(implementer)»",
					joinColumns=@javax.persistence.JoinColumn(name="«implementer.renderTableName("id")»")
				)
				private java.util.List<java.util.Date> «field.getName.trim.renderAttributeName» = new java.util.ArrayList<>();
			'''
			case PropertyType.FILE_LIST: '''
				@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
				@javax.persistence.OneToMany
				@javax.persistence.JoinTable(name="«field.renderFileJoinTableName(implementer)»", joinColumns = { @javax.persistence.JoinColumn(name = "«implementer.renderSimpleTableName»_«implementer.id»_id") }, inverseJoinColumns = { @javax.persistence.JoinColumn(name = "domainfile_id") })
				private java.util.List<info.scce.dime.util.DomainFile> «field.getName.trim.renderAttributeName» = new java.util.ArrayList<>();
			'''
			default: ''''''
		}
	}

	def cleanAndCapitalize(String str) {
		var String[] tab = str.split("\\W+");
		var clean = "";
		for (String t: tab) {
			clean += Character.toUpperCase(t.charAt(0)) + t.substring(1);
		}
		return clean;
	}

	def renderDywaAnnotations(DBField field) '''
		@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
		@de.ls5.dywa.annotations.OriginalName(name = "«field.getName()»")
		@de.ls5.dywa.annotations.ShortDescription(description = "«field.getShortDescription()»")
		@de.ls5.dywa.annotations.LongDescription(description = "«field.getLongDescription()»")
	'''

	def renderGetterAndSetter(DBField field, DBType currentType) '''
		«var methSfx = field.renderMethodSuffix»
		«var nameNoSfx = field.renderMethodPropertyName(false)»
		«var attrName = field.getName.renderAttributeName»
		«var wrappedAttr =attrName+"Wrapper"»
		«IF field.isList && field.isBidirectional» @javax.persistence.Transient private «nameNoSfx» «wrappedAttr»;«ENDIF»
		@java.lang.Override
		public «nameNoSfx» get«methSfx»() {
			«IF field.getPropertyType.equals(PropertyType.FILE)»
				return this.«attrName» != null ? new info.scce.dime.util.FileReference(this.«attrName») : null;
			«ELSEIF field.getPropertyType.equals(PropertyType.FILE_LIST)»
				if (this.«attrName» != null) {
					return this.«attrName».stream().map(o -> new info.scce.dime.util.FileReference(o)).collect(java.util.stream.Collectors.toList());
				}
				return null;
			«ELSEIF field.getPropertyType.equals(PropertyType.OBJECT) && field.getTypeConstraint.isEnumerable»
			if (this.«attrName» != null) {
				return «field.getTypeConstraint.renderCanonicalClassName("entity")».valueOf(this.«attrName».getDywaName());
			}
			return null;
			«ELSEIF field.getPropertyType.equals(PropertyType.OBJECT_LIST) && field.getTypeConstraint.isEnumerable»
			if (this.«attrName» != null) {
				return this.«attrName».stream().map(o -> «field.getTypeConstraint.renderCanonicalClassName("entity")».valueOf(o.getDywaName())).collect(java.util.stream.Collectors.toList());
			}
			return null;
			«ELSEIF field.getPropertyType.equals(PropertyType.OBJECT_LIST)»
				«IF field.isList && field.isBidirectional»
				if(«wrappedAttr» == null){
					«wrappedAttr» = new «field.getTypeConstraint.renderFullPackageName("entity")».«field.getTypeConstraint.renderClassName».CustomListImpl(this.«attrName»,
					new «field.getTypeConstraint.renderFullPackageName("entity")».«field.getTypeConstraint.renderClassName».BiDirectionalHelper<>(
						«IF field.bidirectionalReference.isList»
							«field.getTypeConstraint.renderFullPackageName("entity")».«field.getTypeConstraint.renderClassName»::get«field.bidirectionalReference.renderMethodSuffix»
						«ELSE»
							«field.getTypeConstraint.renderFullPackageName("entity")».«field.getTypeConstraint.renderClassName»::set«field.bidirectionalReference.renderMethodSuffix»
						«ENDIF»
						,
						this.casted())
					);
				}

				return «wrappedAttr»;
				«ELSE»
				return this.«attrName»;
				«ENDIF»
			«ELSE»
				return this.«attrName»;
            «ENDIF»
		}

		@java.lang.Override
		public void set«methSfx»(«nameNoSfx» object) {
			«IF field.getPropertyType.equals(PropertyType.FILE)»
				this.«attrName» = object != null ? object.getDelegate() : null;
			«ELSEIF field.getPropertyType.equals(PropertyType.FILE_LIST)»
				this.«attrName» = object != null ? object.stream().map(o -> o.getDelegate()).collect(java.util.stream.Collectors.toList()) : null;
			«ELSEIF field.getPropertyType.equals(PropertyType.OBJECT) && field.getTypeConstraint.isEnumerable»
			this.«field.getName.renderAttributeName» = object != null ? object.getEntityAs(«field.getTypeConstraint.renderFullClassName».class) : null;
			«ELSEIF field.getPropertyType.equals(PropertyType.OBJECT_LIST) && field.getTypeConstraint.isEnumerable»
			this.«field.getName.renderAttributeName» = object != null ? object.stream().map(o -> o.getEntityAs(«field.getTypeConstraint.renderFullPackageName("entity")».«field.getTypeConstraint.renderFullClassName».class)).collect(java.util.stream.Collectors.toList()) : null;
			«ELSEIF field.isBidirectional»
			if (!this.bidirectionalDirtyFlag) {
				this.bidirectionalDirtyFlag = true;
				final «nameNoSfx» current = this.get«methSfx»();
				«IF field.isList»
					if(current != null){
						// redundant updates can be skipped
						if (object != null && current.equals(object)) {
							this.bidirectionalDirtyFlag = false;
						return;
					}
					current.clear();
					if(object != null){
						current.addAll(object);
					}
				}
				«ELSEIF field.bidirectionalReference.isList»
					final «currentType.renderClassName» _instance =
					«IF currentType.getTransitiveSubTypes.filter[!isDeleted].filter[!equals(currentType)].size > 0»
						inheritance_ ? inheritor_  :
					«ENDIF»
					«IF currentType.isEnumerable»
						«currentType.renderClassName».valueOf(getDywaName());
					«ELSE»
						this;
					«ENDIF»
					if(current != null){
						// redundant updates can be skipped
						if (object != null && current.equals(object)) {
							this.bidirectionalDirtyFlag = false;
							return;
						}

						current.get«field.bidirectionalReference.renderMethodSuffix»().remove(_instance);
					}
					if (object != null) {
						object.get«field.bidirectionalReference.renderMethodSuffix»().add(_instance);
					}
				«ELSE»
					final «currentType.renderClassName» _instance =
					«IF currentType.getTransitiveSubTypes.filter[!isDeleted].filter[!equals(currentType)].size > 0»
						inheritance_ ? inheritor_  :
					«ENDIF»
					«IF currentType.isEnumerable»
						«currentType.renderClassName».valueOf(getDywaName());
					«ELSE»
						this;
					«ENDIF»
					if(current != null){
						current.set«field.bidirectionalReference.renderMethodSuffix»(null);
					}
					if(object!=null){
						object.set«field.bidirectionalReference.renderMethodSuffix»(_instance);
					}
				«ENDIF»
				«IF !field.isList»
					this.«field.getName.renderAttributeName» = object;
				«ENDIF»
				this.bidirectionalDirtyFlag = false;
			}
			«ELSE»
				this.«field.getName.renderAttributeName» = object;
			«ENDIF»
		}

		«IF field.getTypeConstraint !== null && field.getTypeConstraint.isEnumerable»
		protected void set«methSfx»_(«field.renderPropertyName(true)» object) {
			this.«field.getName.renderAttributeName» = object;
		}

		protected «field.renderPropertyName(true)» get«methSfx»_() {
			return this.«field.getName.renderAttributeName»;
		}

		«ENDIF»
	'''

	def renderInheritedGetterAndSetter(DBField field, DBType currentType) '''
		«var nearest = inheritance.get(currentType).stream.filter[t | t.getFullActiveFields.contains(field)].findAny.get»
		«var attrName = nearest.renderClassName»
		«var attrType = nearest.renderFullCanonicalClassName("entity")»
		«var methSfx = field.renderMethodSuffix»
		«var nameSfx = field.renderMethodPropertyName(true)»
		«var nameNoSfx = field.renderMethodPropertyName(false)»
		@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
		«IF field.getTypeConstraint !== null && field.getTypeConstraint.isEnumerable»
		protected «nameSfx» get«methSfx»_() {
			return this.«IF field.type.isAbstractType»abstractInherited«ELSE»inherited«ENDIF»«attrName»_.get«methSfx»_();
		}

		«ENDIF»
		@java.lang.Override
		public «nameNoSfx» get«methSfx»() {
			return this.«IF field.type.isAbstractType»abstractInherited«ELSE»inherited«ENDIF»«attrName»_.get«methSfx»();
		}

		@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
		@info.scce.dime.util.Delegation(attributeName = "«IF field.type.isAbstractType»abstractInherited«ELSE»inherited«ENDIF»«attrName»_", attributeClass = «attrType».class)
		«IF field.getTypeConstraint !== null && field.getTypeConstraint.isEnumerable»
		protected void set«methSfx»_(«nameSfx» object) {
			this.«IF field.type.isAbstractType»abstractInherited«ELSE»inherited«ENDIF»«attrName»_.set«methSfx»_(object);
		}

		«ENDIF»
		@java.lang.Override
		public void set«methSfx»(«nameNoSfx» object) {
			this.«IF field.type.isAbstractType»abstractInherited«ELSE»inherited«ENDIF»«attrName»_.set«methSfx»(object);
		}
	'''

	def renderAdditionalGetterAndSetter(DBField field, DBType typeToInheritFrom) '''
		«var attrName = typeToInheritFrom.renderClassName»
		«var methSfx = field.renderMethodSuffix»
		«var attrType = typeToInheritFrom.renderFullCanonicalClassName("entity")»
		«var nameSfx = field.renderMethodPropertyName(true)»
		«var nameNoSfx = field.renderMethodPropertyName(false)»
		@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
		«IF field.getTypeConstraint !== null && field.getTypeConstraint.isEnumerable»
		protected «nameSfx» get«methSfx»_() {
			if (this.inherited«attrName»_ != null) {
				return this.inherited«attrName»_.get«methSfx»_();
			}
			return null;
		}

		«ENDIF»
		@java.lang.Override
		public «nameNoSfx» get«methSfx»() {
			if (this.inherited«attrName»_ != null) {
				return this.inherited«attrName»_.get«methSfx»();
			}
			return null;
		}

		@de.ls5.dywa.annotations.IdRef(id = «field.getId»L)
		@info.scce.dime.util.Delegation(attributeName = "inherited«attrName»_", attributeClass = «attrType».class)
		«IF field.getTypeConstraint !== null && field.getTypeConstraint.isEnumerable»
		protected void set«methSfx»_(«nameSfx» object) {
			if (this.inherited«attrName»_ == null) {
				this.inherited«attrName»_ = new «attrType»(true);
			}
			this.inherited«attrName»_.set«methSfx»_(object);
		}

		«ENDIF»
		@java.lang.Override
		public void set«methSfx»(«nameNoSfx» object) {
			if (this.inherited«attrName»_ == null) {
				this.inherited«attrName»_ = new «attrType»(true);
			}
			this.inherited«attrName»_.set«methSfx»(object);
		}
	'''

	def renderGetterAndSetterSignature(DBField field) '''
		«var methSfx = field.renderMethodSuffix»
		«var nameNoSfx = field.renderMethodPropertyName(false)»
		«field.renderDywaAnnotations»
		«nameNoSfx» get«methSfx»();

		«field.renderDywaAnnotations»
		void set«methSfx»(«nameNoSfx» object);
	'''
}
