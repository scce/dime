/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.scheme

import de.ls5.dywa.entities.object.DBField
import de.ls5.dywa.entities.object.DBPackage
import de.ls5.dywa.entities.object.DBType
import de.ls5.dywa.entities.property.PropertyType
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.generator.process.BackendProcessGeneratorUtil
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.Process
import java.util.Collections
import java.util.LinkedList
import java.util.List
import java.util.Map

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class RenderExtensions extends BackendProcessGeneratorUtil {

	val String packageName;

	new(String packageName) {
		this.packageName = packageName
	}

	// type renderer
	def static renderTypeName(DBType type)
		'''«type.name.escapeJava»'''

	def static renderTypePackageSuffix(DBType type)
		'''«type.collectPackageHistory.reverseView.join(".", ".", "", [name.escapeJava])»'''

	def renderFQTypeName(DBType type)
		'''«packageName».entity«type.renderTypePackageSuffix».«type.renderTypeName»'''

	// field renderer
	def renderInnerFieldTypeName(DBField field) {
		val pt = field.propertyType
		val isFile = pt == PropertyType.FILE || pt == PropertyType.FILE_LIST
		val packageBuilder = new StringBuilder()

		packageBuilder.append(packageName)
		packageBuilder.append(if (isFile) ".util" else ".entity")
		packageBuilder.append(field.typeConstraint?.renderTypePackageSuffix?:"")

		'''«field.propertyType.renderInnerPropertyTypeName(
				packageBuilder.toString,
				field.typeConstraint?.name)»'''
	}

	def renderFieldTypeName(DBField field) {
		val pt = field.propertyType
		
		'''«pt.wrapInListIfNecessary(field.renderInnerFieldTypeName)»'''
	}

	def renderPropertyName(DBField field) '''
		«IF field.isList && field.isComplex»
			«field.rootOverriddenField.name.escapeJava»_«field.typeConstraint.name.escapeJava»
		«ELSE»
			«field.rootOverriddenField.name.escapeJava»
		«ENDIF»
	'''

	def renderGetter(DBField field)
		'''get«field.renderPropertyName»'''

	def renderSetter(DBField field)
		'''set«field.renderPropertyName»'''

	def renderExtensionAttributeDeclaration(DBType type, Map<String, List<ExtensionAttribute>> eas)
	'''
		«FOR ea : eas.getOrDefault(type.renderFQTypeName.toString, Collections.emptyList())»
			«ea.renderExtensionAttributeDeclaration»
		«ENDFOR»
	'''

	private def renderExtensionAttributeDeclaration(ExtensionAttribute ea)
	'''
		@de.ls5.dywa.annotations.OriginalName(name = "«ea.name.escapeJava»")
		public «ea.eaTypeName» get«ea.name.escapeJava»();

		@de.ls5.dywa.annotations.OriginalName(name = "«ea.name.escapeJava»")
		public void set«ea.name.escapeJava»(final «ea.eaTypeName» param);
	'''

	def renderExtensionAttributeImplementation(DBType type, Map<String, List<ExtensionAttribute>> eas, boolean skipImpl) {
		val transientEas = type.transitiveSuperTypes.map[ eas.getOrDefault(it.renderFQTypeName.toString, Collections.emptyList()) ].flatten
		'''
		«FOR ea : transientEas»
			«ea.renderExtensionAttributeImplementation(skipImpl)»
		«ENDFOR»
		'''
	}

	private def renderExtensionAttributeImplementation(ExtensionAttribute ea, boolean skipImpl) {
		val p = (ea.process as Process)
		'''
			«IF !skipImpl»
				// Cache value, because we rely on a single instance across multiple calls
				@javax.persistence.Transient
				private «ea.eaTypeName» dywa_ea_«ea.name.escapeJava»;
				@javax.persistence.Transient
				private boolean dywa_ea_«ea.name.escapeJava»_fetched;
			«ENDIF»
			@java.lang.Override
			public «ea.eaTypeName» get«ea.name.escapeJava»() {
				«IF skipImpl»
					throw new java.lang.UnsupportedOperationException("Cannot search after extension attributes");
				«ELSE»
					if (!dywa_ea_«ea.name.escapeJava»_fetched) {
						this.dywa_ea_«ea.name.escapeJava»_fetched = true;

					«IF p.requiresDependencyInjection»
						final «p.typeName» process = info.scce.dime.util.CDIUtil.getManagedInstance(«p.typeName».class);
					«ELSE»
						final «p.typeName» process = new «p.typeName»(javax.enterprise.inject.spi.CDI.current().getBeanManager());
					«ENDIF»
						«p.typeName».«p.rootElement.resultTypeName» result = process.execute(false, this);
						this.dywa_ea_«ea.name.escapeJava» = result.get«p.endSIBs.get(0).branchName.toFirstUpper.escapeJava»Return().get«ea.extensionAttributePort.name.toFirstUpper.escapeJava»();
					}
					return dywa_ea_«ea.name.escapeJava»;
				«ENDIF»
			};

			@java.lang.Override
			public void set«ea.name.escapeJava»(final «ea.eaTypeName» param) {
				«IF skipImpl»
				throw new java.lang.UnsupportedOperationException("Cannot search after extension attributes");
				«ELSE»
					this.dywa_ea_«ea.name.escapeJava»_fetched = true;
					this.dywa_ea_«ea.name.escapeJava» = param;
				«ENDIF»
			}
		'''
	}

	// internal utils
	private static def renderInnerPropertyTypeName(PropertyType pt, String packageName, String className) {
		switch pt {
			case PropertyType.BOOLEAN,
			case PropertyType.BOOLEAN_LIST: '''java.lang.Boolean'''
			case PropertyType.DOUBLE,
			case PropertyType.DOUBLE_LIST: '''java.lang.Double'''
			case PropertyType.LONG,
			case PropertyType.LONG_LIST: '''java.lang.Long'''
			case PropertyType.OBJECT,
			case PropertyType.OBJECT_LIST: '''«packageName.pkgEscape».«className.escapeJava»'''
			case PropertyType.STRING,
			case PropertyType.STRING_LIST: '''java.lang.String'''
			case PropertyType.TIMESTAMP,
			case PropertyType.TIMESTAMP_LIST: '''java.util.Date'''
			case PropertyType.FILE,
			case PropertyType.FILE_LIST: '''info.scce.dime.util.FileReference'''
			default: '''«packageName.pkgEscape».«className.escapeJava»'''
		}.toString
	}

	private static def wrapInListIfNecessary(PropertyType pt, CharSequence content) {
		val template = if(pt.isList) "java.util.List<%s>" else "%s"
		String.format(template, content)
	}

	private static def collectPackageHistory(DBType type) {
		val result = new LinkedList<DBPackage>()
		collectPackageHistoryRecursive(type.dyWAPackage, result)
		result
	}

	private static def void collectPackageHistoryRecursive(DBPackage ^package, List<DBPackage> hierarchy) {
		if (^package !== null) {
			hierarchy.add(^package);
			collectPackageHistoryRecursive(^package.parentPackage, hierarchy)
		}
	}
	
	private def eaTypeName(ExtensionAttribute ea) {
		val port = ea.extensionAttributePort
		val typeName = if (port instanceof InputStatic) {
				port.typeName
			} else if (port instanceof InputPort) {
				port.typeName
			} else {
				throw new IllegalArgumentException("Unknown Input type " + port.getClass())
			}

		'''«IF port.isList»java.util.«ENDIF»«typeName»'''
	}
}
