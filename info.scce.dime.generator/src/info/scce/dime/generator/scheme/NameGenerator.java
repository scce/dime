/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.scheme;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

/**
 * Helper class for specifing consistent, database friendly identifiers.
 *
 * @author <a href="mailto:hendrik.grewe@tu-dortmund.de">Hendrik Grewe</a>
 */
public final class NameGenerator {

	static int TOTAL_NAME_LENGTH = 63;

	public static final String generateIdentifierName(String prefix, String suffix, String typeName) {
		final StringBuilder sb = new StringBuilder(prefix);
		typeName = Arrays.stream(typeName.split("_")).filter(s -> s != null && !s.isEmpty())
				.map(s -> s.substring(0, 1).toUpperCase() + s.substring(1)).collect(Collectors.joining("_"));
		final List<String> tokens = Arrays.asList(
				typeName.replaceAll("\\W+", "*").split("(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|(?<=_)|(?<=\\*)"));

		Collections.reverse(tokens);
		if (!suffix.isEmpty()) {
			suffix = "_" + suffix;
		}

		int equalSplit = (TOTAL_NAME_LENGTH - sb.length() - suffix.length()) / tokens.size();

		for (ListIterator<String> iterator = tokens.listIterator(); iterator.hasNext();) {
			String token = iterator.next().replaceAll("\\*", "");
			final double weight = token.length() * 1. / typeName.length();
			int wightedTokenLength = (int) Math.ceil((TOTAL_NAME_LENGTH - sb.length() - suffix.length()) * weight);
			int tokenLength = Math.max(wightedTokenLength, equalSplit);
			equalSplit = (TOTAL_NAME_LENGTH - sb.length() - suffix.length())
					/ (tokens.size() + 1 - iterator.nextIndex());
			if (token.endsWith("_")) {
				String trimmedToken = token.substring(0, Math.min(token.length(), tokenLength));
				if (!trimmedToken.equals(token)) {
					token = trimmedToken.substring(0, trimmedToken.length() - 1) + "_";
				}
			}
			token = token.substring(0, Math.min(token.length(), tokenLength));
			iterator.set(token);
			sb.append(token);
		}
		Collections.reverse(tokens);
		sb.delete(prefix.length(), sb.length());
		sb.append(tokens.stream().collect(Collectors.joining()));
		sb.append(suffix);

		String result = sb.toString().replaceAll("[^\\dA-Za-z0-9]", "_");
		if (result.getBytes().length > TOTAL_NAME_LENGTH) {
			throw new IllegalStateException("identifier names are not allowed to exceed 64 characters! " + sb);
		}

		return result;
	}

	public static final String prepareIdentifierName(String oldTypeName, String cincoId, String typeName) {
		String prefix;
		switch (oldTypeName) {
		case "dtype":
			prefix = "t";
			break;
		case "denum":
			prefix = "e";
			break;
		case "attr":
			prefix = "a";
			break;
		case "dcol":
			prefix = "c";
			break;
		case "drel":
			prefix = "r";
			break;
		case "meta":
			prefix = "m";
			break;
		case "inherited":
			prefix = "i";
			break;
		case "abstractinherited":
			prefix = "ai";
			break;

		default:
			throw new IllegalStateException("unhandled type name:" + oldTypeName);
		}
		return generateIdentifierName(prefix + "_", cincoId, typeName);
	}
}
