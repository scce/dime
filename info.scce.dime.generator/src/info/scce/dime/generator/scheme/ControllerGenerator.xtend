/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.scheme

import de.ls5.dywa.entities.object.DBField
import de.ls5.dywa.entities.object.DBType
import de.ls5.dywa.entities.property.PropertyType
import java.util.Comparator
import java.util.Map
import java.util.Set
import java.util.stream.Collectors

class ControllerGenerator {
	val String packageName

	val extension ModelExtensions modelExtensions

	new(String packageName) {
		this(packageName,new ModelExtensions(packageName, null));
	}

	new(String packageName,ModelExtensions modelExtensions) {
    		this.packageName = packageName;
    		this.modelExtensions = modelExtensions
    	}

	def String generateInterface(DBType type) '''
		/* generated by «this.getClass().getName()» */
		package «type.renderFullPackageName("controller")»;

		import «type.renderCanonicalClassName("entity")»;

		public interface «type.renderClassName»Controller {

			«type.renderClassName» read(java.lang.Long id);

			java.util.List<«type.renderClassName»> findByProperties(«type.renderClassName» searchObject);

			«type.renderClassName» findFirstByProperties(«type.renderClassName» searchObject);

			«IF !type.abstractType»
				java.util.Set<«type.renderClassName»> fetch();

				java.util.Set<«type.renderClassName»> fetchByName(java.lang.String name);
			«ENDIF»

			«IF !type.enumerable»
				«IF !type.abstractType»
					«type.renderClassName» create(java.lang.String name);
					«type.renderClassName» createTransient(java.lang.String name);
				«ENDIF»

				«type.renderClassName» createSearchObject(java.lang.String name);

				java.util.Set<«type.renderClassName»> fetchWithSubtypes();

				void delete(«type.renderClassName» entity);

				void deleteWithIncomingReferences(«type.renderClassName» entity);
			«ENDIF»
		}
	'''

	def String generateController(DBType type, Map<DBType, Set<DBField>> referenceMap) '''
		/* generated by «this.getClass().getName()» */
		package «type.renderFullPackageName("controller")»;

		import «type.renderCanonicalClassName("entity")»;
		import «type.renderFullCanonicalClassName("entity")»;
		«IF !type.enumerable»import «type.renderCanonicalClassName("entity")»Search;«ENDIF»

		import java.util.Arrays;
		import java.util.stream.Collectors;

		@javax.enterprise.context.RequestScoped
		public class «type.renderClassName»ControllerImpl implements «type.renderClassName»Controller {
			private static final org.slf4j.Logger LOGGER =  org.slf4j.LoggerFactory.getLogger(«type.renderClassName»Controller.class);

			@javax.persistence.PersistenceContext
			private javax.persistence.EntityManager entityManager;

			@javax.inject.Inject
			private info.scce.dime.util.DomainFileController domainFileController;

		    «FOR subType : type.getSubTypes.filter[!deleted].filter[!abstractType]»
				@javax.inject.Inject // non ref map
				private «subType.renderFullPackageName("controller")».«subType.renderClassName»Controller «subType.getName.renderAttributeName»Controller;

		    «ENDFOR»
		    «IF referenceMap !== null»
			    «FOR refType : referenceMap.keySet»
					«IF (!type.getSubTypes.contains(refType) && !refType.isDeleted ) || refType.isEnumerable»
						@javax.inject.Inject //referenceMap
						private «refType.renderFullPackageName("controller")».«refType.renderClassName»Controller «refType.getName.renderAttributeName»Controller;

			    	«ENDIF»
			    «ENDFOR»
		    «ENDIF»

		    @Override
			public «type.renderClassName» read(final java.lang.Long id) {

				«IF type.enumerable»
				«type.renderFullClassName» result = entityManager.find(«type.renderFullClassName».class, id);
				if (result != null) {
					return «type.renderClassName».valueOf(result.getDywaName());
				}
				return «type.renderClassName».forId(id);
				«ELSE»
				 «type.renderClassName» result = «IF type.isAbstractType»null«ELSE»entityManager.find(«type.renderFullClassName».class, id)«ENDIF»;
				 	// delegate read until entity is found;
				«FOR t:type.getSubTypes.filter[!deleted].filter[!abstractType]»
				      result = result==null?«t.getName.renderAttributeName»Controller.read(id):result;
				«ENDFOR»
				return result;
				«ENDIF»
			}

		    @Override
			public java.util.List<«type.renderClassName»> findByProperties(«type.renderClassName» searchObject) {
				«IF type.enumerable»
				throw new java.lang.UnsupportedOperationException("Cannot perform this operation on an enumerable object");
				«ELSE»
				if (searchObject instanceof «type.renderClassName»Search) {
					«type.renderClassName»Search casted = («type.renderClassName»Search) searchObject;

					java.util.List<«type.renderClassName»> list = buildSimpleQuery(casted.queryAttributes(), casted.queryListAttributes(), true).getResultList().stream().map(«type.renderClassName»::casted).collect(Collectors.toList());

					return list;
				} else {
					throw new java.lang.IllegalArgumentException("Search object required.");
				}
				«ENDIF»
			}

			@Override
			public «type.renderClassName» findFirstByProperties(«type.renderClassName» searchObject) {
				«IF type.enumerable»
				throw new java.lang.UnsupportedOperationException("Cannot perform this operation on an enumerable object");
				«ELSE»
				if (searchObject instanceof «type.renderClassName»Search) {
					«type.renderClassName»Search casted = («type.renderClassName»Search) searchObject;
					java.util.List<«type.renderClassName»> results = new java.util.ArrayList<«type.renderClassName»>();
					«IF !type.isAbstractType»results.addAll(buildSimpleQuery(casted.queryAttributes(), casted.queryListAttributes(),false).setMaxResults(1).getResultList())«ENDIF»;
				«FOR t:type.getSubTypes.filter[!deleted].filter[!abstractType]»
					if(results.isEmpty()){
						results.add(«t.getName.renderAttributeName»Controller.findFirstByProperties( new «t.renderFullPackageName("entity")».«t.getName.renderAttributeName.toFirstUpper»Search(casted)));
					}
				«ENDFOR»
					return results.isEmpty() ? null : results.get(0);
				} else throw new java.lang.IllegalArgumentException("Search object required.");
				«ENDIF»
			}

			«IF !type.abstractType»
			@Override
			public java.util.Set<«type.renderClassName»> fetch() {
				«IF type.enumerable»
				return java.util.stream.Stream.of(«type.renderClassName».values()).collect(java.util.stream.Collectors.toSet());
				«ELSE»
				return new java.util.HashSet<«type.renderClassName»>(buildSimpleQuery(null, null,false).getResultList());
				«ENDIF»
			}

			@Override
			public java.util.Set<«type.renderClassName»> fetchByName(final java.lang.String name) {
				«IF type.enumerable»
				return java.util.stream.Stream.of(«type.renderClassName».values()).filter(e -> java.util.Objects.equals(e.getDywaName(), name)).collect(java.util.stream.Collectors.toSet());
				«ELSE»
				java.util.Map<String, Object> map = new java.util.HashMap<>();
				map.put("name_", name);
				java.util.HashSet<«type.renderClassName»> result = new java.util.HashSet<>(buildSimpleQuery(map, null,false).getResultList());
					«FOR subType : type.getSubTypes.filter[!deleted].filter[!abstractType].filter[!enumerable]»
						result.addAll(this.«subType.getName.renderAttributeName»Controller.fetchByName(name));
					«ENDFOR»
				return result;
				«ENDIF»
			}

			«ENDIF»
			«IF !type.enumerable»
				«IF !type.abstractType»
				@Override
				public «type.renderClassName» create(java.lang.String name) {
					«type.renderFullClassName» entity = new «type.renderFullClassName»();
					entity.setDywaName(name);
					entityManager.persist(entity);
					return entity;
				}

				@Override
				public «type.renderClassName» createTransient(java.lang.String name) {
					«type.renderFullClassName» entity = new «type.renderFullClassName»();
					entity.setDywaName(name);
					return entity;
				}

				«ENDIF»
			@Override
			public «type.renderClassName» createSearchObject(java.lang.String name) {
				return new «type.renderClassName»Search(name);
			}

			@Override
			public java.util.Set<«type.renderClassName»> fetchWithSubtypes() {
				java.util.Set<«type.renderClassName»> list = buildSimpleQuery(null,null, true).getResultList().stream().map(«type.renderClassName»::casted).collect(Collectors.toSet());

				return list;
			}

			@Override
			public void delete(«type.renderClassName» entity) {

				«IF !type.abstractType»
				if (entity instanceof «type.renderFullClassName») {
					«type.renderFullClassName» impl = («type.renderFullClassName») entity;
					«FOR field : type.getFullActiveFields.filter[propertyType == PropertyType.FILE]»
						{
							info.scce.dime.util.FileReference ref = entity.get«field.renderMethodSuffix»();
							if (ref != null) {
								entity.set«field.renderMethodSuffix»(null);
								domainFileController.deleteFile(ref);
							}
						}
					«ENDFOR»
					«FOR field : type.getFullActiveFields.filter[propertyType == PropertyType.FILE_LIST]»
						{
							// copy to prevent potential concurrent modification exceptions
							java.util.List<info.scce.dime.util.FileReference> refs = new java.util.ArrayList<>(entity.get«field.renderMethodSuffix»());
							for (info.scce.dime.util.FileReference ref : refs) {
								refs.remove(ref);
								domainFileController.deleteFile(ref);
							}
						}
					«ENDFOR»
					entityManager.remove(impl);
				}
				«ENDIF»
				// delegate delete
				«FOR t:type.getSubTypes.filter[!deleted].filter[!abstractType] SEPARATOR " else "»
				«var className=t.renderFullCanonicalClassName("entity")»

				if(entity instanceof «IF t.isAbstractType»«className=className.toString.replace("Impl","")»«ELSE»«className»« ENDIF») {
				       «t.getName.renderAttributeName»Controller.delete((«className») entity);
				}
				«ENDFOR»
			}

			@Override
			public void deleteWithIncomingReferences(«type.renderClassName» entityToDelete) {

				«FOR t:type.getSubTypes.filter[!deleted].filter[!abstractType] SEPARATOR " else "»
				«var className=t.renderFullCanonicalClassName("entity")»
				// delegate delete if entity type is «t.renderClassName»
				if(entityToDelete instanceof «className») {
				      «t.getName.renderAttributeName»Controller.deleteWithIncomingReferences((«className») entityToDelete);
				      return;
				}
				«ENDFOR»
				«IF referenceMap !== null && !referenceMap.isEmpty»
					«FOR entry : referenceMap.entrySet.stream.sorted(Comparator.comparing[getKey.getName]).collect(Collectors.toList)»

					// Delete references from type «entry.getKey.getName»
						«IF entry.getKey.isEnumerable»
						// Not supported yet: Create search objects for enums?
						«ELSE»
						«var searchObjectName = '''search«entry.getKey.renderClassName»'''»
						«entry.getKey.renderCanonicalClassName("entity")» «searchObjectName»;
						«FOR field : entry.getValue»
						«searchObjectName» = new «entry.getKey.renderCanonicalClassName("entity")»Search();
							«IF PropertyType.OBJECT.equals(field.getPropertyType)»
							«searchObjectName».set«field.renderMethodSuffix»(entityToDelete);
							«ELSEIF PropertyType.OBJECT_LIST.equals(field.getPropertyType)»
							«searchObjectName».set«field.renderMethodSuffix»(java.util.Arrays.asList(entityToDelete));
							«ENDIF»
						for («entry.getKey.renderCanonicalClassName("entity")» queryResult : this.«entry.getKey.getName.renderAttributeName»Controller.findByProperties(«searchObjectName»)) {
							«IF PropertyType.OBJECT.equals(field.getPropertyType)»
							queryResult.set«field.renderMethodSuffix»(null);
							«ELSEIF PropertyType.OBJECT_LIST.equals(field.getPropertyType)»
							queryResult.get«field.renderMethodSuffix»().remove(entityToDelete);
							«ENDIF»
						}
						«ENDFOR»
						«ENDIF»
					«ENDFOR»
				«ENDIF»
				delete(entityToDelete);
			}

			private javax.persistence.TypedQuery<«type.renderFullClassName»> buildSimpleQuery(java.util.Map<String, Object> attributeMap, java.util.Map<String, java.util.List> listAttributeMap, boolean withInherited) {
				java.lang.StringBuilder queryStr = new java.lang.StringBuilder("SELECT e FROM «type.renderFullClassName» e WHERE (true="+withInherited+" OR e.inheritance_ = false)");
				if (attributeMap != null) {
					for (java.util.Map.Entry<String, Object> entry : attributeMap.entrySet()) {
						queryStr.append(" AND e." + entry.getKey() + " = :" + entry.getKey().replaceAll("\\W", ""));
					}
				}
				if (listAttributeMap != null) {
					for (java.util.Map.Entry<String, java.util.List> entry : listAttributeMap.entrySet()) {
						if (entry.getValue() == null || entry.getValue().isEmpty()) {
							queryStr.append(" AND e." + entry.getKey() + " IS EMPTY");
						} else {
							String prefix = entry.getKey().replaceAll("\\W", "");
							for (int i = 0, s = entry.getValue().size(); i < s; i++) {
								queryStr.append(" AND :" + prefix + i + " MEMBER OF e." + entry.getKey());
							}
						}
					}
				}
				queryStr.append(" ORDER BY id_ DESC");
				javax.persistence.TypedQuery<«type.renderFullClassName»> query = entityManager.createQuery(queryStr.toString(), «type.renderFullClassName».class);
				if (attributeMap != null) {
					for (java.util.Map.Entry<String, Object> entry : attributeMap.entrySet()) {
						query.setParameter(entry.getKey().replaceAll("\\W", ""), entry.getValue());
					}
				}
				if (listAttributeMap != null) {
					for (java.util.Map.Entry<String, java.util.List> entry : listAttributeMap.entrySet()) {
						if (entry.getValue() != null && !entry.getValue().isEmpty()) {
							String prefix = entry.getKey().replaceAll("\\W", "");
							for (int i = 0, s = entry.getValue().size(); i < s; i++) {
								query.setParameter(prefix + i, entry.getValue().get(i));
							}
						}
					}
				}
				query.setHint(org.hibernate.jpa.QueryHints.HINT_CACHEABLE,true);
				return query;
			}
		«ENDIF»
		}
	'''
}
