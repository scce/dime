/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.scheme

import de.ls5.dywa.entities.object.DBType
import java.util.List

class UtilsGenerator {
	val String packageName
	val extension ModelExtensions modelExtensions;
	val extension RenderExtensions renderExtensions;

	new(String packageName) {
		this(packageName, new ModelExtensions(packageName, null));
	}

	new(String packageName, ModelExtensions modelExtensions) {
		this.packageName = packageName;
		this.modelExtensions = modelExtensions
		this.renderExtensions = new RenderExtensions(packageName)
	}

	def String generateTypeInfo(List<DBType> types) '''
		package «packageName».util;
		
		public class TypeInfo {
			private static java.util.Map<Long, java.lang.Class<?>> idToClassMap = new java.util.HashMap<>();
			private static java.util.Map<Long, java.util.Map<Long, String>> typeFieldNamesMap = new java.util.HashMap<>();
			private static java.util.Map<Long, String> typeNamesMap = new java.util.HashMap<>();
			private static java.util.Set<Long> enumTypes = new java.util.HashSet<>();
			private static java.util.Set<Long> allTypes = new java.util.HashSet<>();
			
			static {
				«FOR type : types»
					«IF !type.isDeleted»
						/* DBType «type.cincoId» */
						typeNamesMap.put(«type.getId»L, "«type.getName»");
						idToClassMap.put(«type.getId()»L, «type.renderFullCanonicalClassName("entity")».class);
						«IF type.isEnumerable»enumTypes.add(«type.getId()»L);«ENDIF»
						allTypes.add(«type.getId»L);
						java.util.Map<Long, String> map«type.getId» = new java.util.HashMap<Long, String>();
						«FOR field : type.getFields»
							map«type.getId».put(«field.getId»L, "«field.getName»");
						«ENDFOR»
						typeFieldNamesMap.put(«type.getId»L, map«type.getId»);
					«ENDIF»
				«ENDFOR»
			}
			
			public static String getGeneratedClassName(Long typeId) {
				java.lang.Class<?> clazz =  idToClassMap.get(typeId);
				return clazz != null ? clazz.getName() : null;
			}
			
			public static java.lang.Class<?> getGeneratedClass(Long typeId) {
				return idToClassMap.get(typeId);
			}
			
			public static java.util.Collection<Class<?>> getAllGeneratedClasses() {
				return idToClassMap.values();
			}
			
			public static java.util.List<String> getAllGeneratedClassNames() {
				return idToClassMap.values().stream().map(Class::getName).collect(java.util.stream.Collectors.toList());
			}
			
			public static boolean isEnumerable(Long id) {
				return enumTypes.contains(id);
			}
			
			public static boolean typeInUse(Long typeId) {
				return allTypes.contains(typeId);
			}
			
			public static java.util.Map<Long, String> getTypeFieldNames(Long typeId) {
				return typeFieldNamesMap.get(typeId);
			}
			
			public static String getTypeName(Long id) {
				return typeNamesMap.get(id);
			}
		}
	'''

	def String generateDomainFileController() '''
		package «packageName».util;
		
		import java.io.InputStream;
		import java.io.File;
		
		import javax.inject.Inject;
		import javax.inject.Named;
		import javax.enterprise.context.RequestScoped;
		
		import info.scce.dime.util.DomainFile;
		import info.scce.dime.util.DomainFileController;
		import info.scce.dime.util.FileReference;
		import info.scce.dime.util.StorageManager;
		
		@Named
		@RequestScoped
		public class DomainFileControllerImpl implements DomainFileController {
		
			@javax.persistence.PersistenceContext
			   private javax.persistence.EntityManager entityManager;
		
			@Inject
			private StorageManager storageManager;
			
			@Override
			public FileReference getFileReference(final long id) {
				final DomainFile file = entityManager.find(DomainFile.class, id);
				if (file == null) {
					return null;
				}
		
				return new FileReference(file);
			}
		
			@Override
			public InputStream loadFile(final FileReference identifier) {
				if (identifier == null) {
					return null;
				}
				File fsFile = getFileForDomainFile(identifier.getDelegate());
				return storageManager.getFile(fsFile);
			}
			
			@Override
			public FileReference storeFile(final String fileName, final InputStream dataStream) {
				final InputStream dataInput = org.apache.tika.io.TikaInputStream.get(dataStream);
				
				final DomainFile result = new DomainFile(fileName, storageManager.getContentType(dataInput));
				entityManager.persist(result);
		
				final File file = getFileForDomainFile(result);
				try {
					storageManager.createFile(file);
				}
				catch (org.xadisk.filesystem.exceptions.FileAlreadyExistsException e) {
					throw new IllegalStateException("file " + file.toString() + " existed, overwriting...");
				}
		
				org.xadisk.additional.XAFileOutputStreamWrapper fsFileOutput = new org.xadisk.additional.XAFileOutputStreamWrapper(storageManager.createXAFileOutputStream(file));
		
				try {
					storageManager.copyStreams(dataInput, fsFileOutput);
				} finally {
					org.apache.tika.io.IOUtils.closeQuietly(fsFileOutput);
					org.apache.tika.io.IOUtils.closeQuietly(dataInput);
				}
				return new FileReference(result);
			}
		
			@Override
			public void deleteFile(final FileReference identifier) {
				File fsFile = getFileForDomainFile(identifier.getDelegate());
				storageManager.deleteFile(fsFile);
				entityManager.remove(identifier.getDelegate());
			}
			
			private File getFileForDomainFile(final DomainFile domainFile) {
				return new File(StorageManager.getNativeDomainStorageRoot(), Long.toString(domainFile.getId()));
			}
		}
	'''

	def String generatePersistenceXML(List<String> generatedEntities) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<persistence version="2.0"
			xmlns="http://java.sun.com/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="
		      http://java.sun.com/xml/ns/persistence
		      http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd">
			<persistence-unit name="pu">
				<jta-data-source>java:jboss/datasources/ExampleDS</jta-data-source>
		
				<class>info.scce.dime.util.DomainFile</class>
				<class>«packageName».util.EnumMapping</class>
				<!-- generated entities -->
		        «FOR clazz : generatedEntities»
		        <class>«clazz»</class>
		        «ENDFOR»
		
				<shared-cache-mode>ALL</shared-cache-mode>
				<properties>
					<property name="hibernate.hbm2ddl.auto" value="update" />
		
					<property name="hibernate.jdbc.batch_size" value="64" />
					<property name="hibernate.default_batch_fetch_size" value="64" />
		
					<property name="hibernate.generate_statistics" value="false" />
					<property name="hibernate.cache.use_second_level_cache" value="true" />
					<property name="hibernate.cache.use_query_cache" value="true" />
		
					<property name="hibernate.show_sql" value="false" />
					<property name="hibernate.format_sql" value="false" />
					<property name="hibernate.use_sql_comments" value="false" />
					<property name="hibernate.archive.autodetection" value="class, hbm" />
		
		
					<property name="eclipselink.ddl-generation" value="create-tables"/>
					<property name="eclipselink.ddl-generation.output-mode" value="database" />
		
					<property name="eclipselink.cache.coordination.protocol" value="jms"/>
					<property name="eclipselink.cache.coordination.jms.topic" value="java:/jms/topic/DyWACacheTopic"/>
					<property name="eclipselink.cache.coordination.jms.factory" value="java:/ConnectionFactory"/>
		
					<property name="eclipselink.target-server" value="JBoss"/>
					<property name="eclipselink.deploy-on-startup" value="True" />
		
					<property name="eclipselink.weaving" value="static"/>
					<property name="eclipselink.weaving.internal" value="false"/>
					<property name="eclipselink.id-validation" value="NEGATIVE"/>
		
					<property name="eclipselink.logging.parameters" value="true"/>
				</properties>
			</persistence-unit>
		</persistence>
	'''

	def generateAALControllerImpl(Iterable<DBType> nonAbstractTypes) '''
		package «packageName».util;
		
		import javax.inject.Inject;
		import javax.enterprise.context.RequestScoped;
		import javax.persistence.EntityManager;
		import javax.persistence.FlushModeType;
		import javax.persistence.PersistenceContext;
		
		import info.scce.dime.util.AALController;
		
		@RequestScoped
		public class AALControllerImpl implements AALController {
		
			@PersistenceContext
			private EntityManager entityManager;
		
			«FOR t : nonAbstractTypes»
			@Inject
			private «packageName».controller«RenderExtensions.renderTypePackageSuffix(t)».«RenderExtensions.renderTypeName(t)»Controller «RenderExtensions.renderTypeName(t)»Controller;
			«ENDFOR»
		
			@Override
			public void reset() {
		
			final FlushModeType oldFlushMode = this.entityManager.getFlushMode();
			this.entityManager.flush();
			this.entityManager.setFlushMode(FlushModeType.COMMIT);
		
			«FOR t : nonAbstractTypes»
				for (final «t.renderFQTypeName» o : «RenderExtensions.renderTypeName(t)»Controller.fetch()) {
				«IF t.isEnumerable»
					o.setDywaName(o.toString());
					«FOR f : t.getFullActiveFields»
						o.set«f.renderPropertyName»(null);
					«ENDFOR»
				«ELSE»
					«RenderExtensions.renderTypeName(t)»Controller.deleteWithIncomingReferences(o);
				«ENDIF»
				}
			«ENDFOR»
		
				this.entityManager.setFlushMode(oldFlushMode);
			}
		}
	'''

	def generateEnumMappingEntity() '''
		package «packageName».util;
		
		import javax.persistence.Entity;
		import javax.persistence.Id;
		
		@Entity
		public class EnumMapping {
		
		    @Id
		    private long enumId;
		
		    private long objectId;
		
		    public EnumMapping() {}
		
		    public EnumMapping(long enumId, long objectId) {
		    this.enumId = enumId;
		    this.objectId = objectId;
		    }
		
		    public long getEnumId() {
		    return enumId;
		    }
		
		    public void setEnumId(long enumId) {
		    this.enumId = enumId;
		    }
		
		    public long getObjectId() {
		    return objectId;
		    }
		
		    public void setObjectId(long objectId) {
		    this.objectId = objectId;
		    }
		}
	'''
}
