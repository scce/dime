/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.scheme;

import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import de.ls5.dywa.entities.object.DBField;
import de.ls5.dywa.entities.object.DBObject;
import de.ls5.dywa.entities.object.DBType;
import de.ls5.dywa.entities.property.PropertyType;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.helper.JavaIdentifierUtils;
import info.scce.dime.generator.rest.DyWAAbstractGenerator;

/**
 * @author <a href="mailto:hendrik.grewe@tu-dortmund.de">Hendrik Grewe</a>
 */
public class NativeDomainGenerator {

	private final String packageName;

	private final Path targetDirectory;

	private NativeDomainGenerator(final String packageName, final Path targetDirectory) {
		this.packageName = JavaIdentifierUtils.pkgEscape(packageName);
		this.targetDirectory = targetDirectory;
	}

	public static void generateApi(final Map<DBType, Set<DBObject>> dbTypesWithObjects,
			Map<String, List<ExtensionAttribute>> eaAttributes, final Map<Long, String> dywaIdToCincoId,
			final String packageName, Path targetDirectory) {
		new NativeDomainGenerator(packageName, targetDirectory).generate(dbTypesWithObjects, eaAttributes,
				dywaIdToCincoId, true, false);
	}

	public static void generateImpl(final Map<DBType, Set<DBObject>> dbTypesWithObjects,
			Map<String, List<ExtensionAttribute>> eaAttributes, final Map<Long, String> dywaIdToCincoId,
			final String packageName, Path targetDirectory) {
		new NativeDomainGenerator(packageName, targetDirectory).generate(dbTypesWithObjects, eaAttributes,
				dywaIdToCincoId, false, true);
	}

	private void generate(Map<DBType, Set<DBObject>> typeToObjectMap,
			Map<String, List<ExtensionAttribute>> eaAttributes, final Map<Long, String> dywaIdToCincoId,
			boolean generateApi, boolean generateImpl) {

		final List<DBType> allTypes = Lists.newArrayList(typeToObjectMap.keySet());
		// Generate inheritance map
		Map<DBType, List<DBType>> inheritanceMap = getInheritanceMap(allTypes);
		System.out.println("Inheritance map built");

		// Instantiate generators
		final ModelExtensions modelExtensions = new ModelExtensions(packageName, inheritanceMap, dywaIdToCincoId);
		final ModelGenerator modelGenerator = new ModelGenerator(packageName, inheritanceMap, eaAttributes,
				modelExtensions);
		final EnumGenerator enumGenerator = new EnumGenerator(packageName, inheritanceMap, modelExtensions);
		final UtilsGenerator utilsGenerator = new UtilsGenerator(packageName, modelExtensions);
		final ControllerGenerator controllerGenerator = new ControllerGenerator(packageName, modelExtensions);

		List<String> generatedEntities = Lists.newArrayListWithCapacity(allTypes.size());
		// Generate objects for each type

		for (DBType type : allTypes) {
			if (type.isDeleted()) {
				System.out.println("Type '" + type.getName() + "' marked for deletion. Ignoring.");
				continue;
			}
			System.out.println("Checking type: " + type.getName());
			String className = modelExtensions.renderClassName(type).toString();
			String packageSuffix = modelExtensions.renderPackageSuffix(type);
			if (!packageSuffix.isEmpty()) {
				packageSuffix = "." + packageSuffix;
			}
			Set<DBField> additionalFields = Collections.emptySet();// GenerationUtility.getAbstractFields(type);
			System.out.println("Additional fields computed: Total " + additionalFields.size());
			Set<DBType> additionalTypes = getAdditionalDelegates(type);
			System.out.println("Additional types computed: Total " + additionalTypes.size());
			if (type.isEnumerable()) {
				System.out.println("Handling type as enumerable.");
				// Fetch available objects
				List<DBObject> dbObjects = typeToObjectMap.get(type).stream()
						.filter(e -> e.getName() != null && !e.getName().isEmpty()).collect(Collectors.toList());
				// Generate classes
				if (generateApi) {
					String interfaceAsString = enumGenerator.generateInterface(type);
					this.writeToFile(interfaceAsString, packageName + ".entity" + packageSuffix,
							className + "Interface.java");
					String enumClass = enumGenerator.generateEnum(type, dbObjects);
					this.writeToFile(enumClass, packageName + ".entity" + packageSuffix, className + ".java");
				}
				if (generateImpl) {
					String entityClass = enumGenerator.generateEntityClass(type, additionalFields, additionalTypes);
					this.writeToFile(entityClass, packageName + ".entity" + packageSuffix, className + "Entity.java");
					generatedEntities.add(packageName + ".entity" + packageSuffix + "." + className + "Entity");
				}

			} else {
				System.out.println("Handling type as regular class.");
				if (generateApi) {
					String interfaceAsString = modelGenerator.generateInterface(type);
					this.writeToFile(interfaceAsString, packageName + ".entity" + packageSuffix, className + ".java");
				}
				if (generateImpl) {
					String searchObjectAsString = modelGenerator.generateSearchObject(type, additionalFields,
							additionalTypes);
					this.writeToFile(searchObjectAsString, packageName + ".entity" + packageSuffix,
							className + "Search.java");
					String modelAsString = modelGenerator.generateModel(type, additionalFields, additionalTypes);
					this.writeToFile(modelAsString, packageName + ".entity" + packageSuffix,
							modelExtensions.renderFullClassName(type).toString() + ".java");
					generatedEntities.add(packageName + ".entity" + packageSuffix + "."
							+ modelExtensions.renderFullClassName(type).toString());
				}
			}
			Map<DBType, Set<DBField>> referencingTypes = null;
			referencingTypes = new HashMap<>();
			System.out.println("Fetching types referencing " + type.getName() + "...");
			// Find fields referencing type
			Set<DBField> fields = allTypes.stream().flatMap(t -> t.getFields().stream()).filter(f -> !f.isDeleted())
					.filter(f -> f.getPropertyType().equals(PropertyType.OBJECT)
							|| f.getPropertyType().equals(PropertyType.OBJECT_LIST))
					.filter(f -> f.getTypeConstraint().getTransitiveSubTypes().contains(type))
					.collect(Collectors.toSet());
			// Find types having those fields in their active fields
			for (DBField field : fields) {
				for (DBType typeToCheck : allTypes) {
					if (typeToCheck.getFullActiveFields().contains(field)) { // Ignore abstracts because they can't have
																				// direct reference to object
						Set<DBField> fieldsOfTypeToCheck;
						if (referencingTypes.containsKey(typeToCheck)) {
							fieldsOfTypeToCheck = referencingTypes.get(typeToCheck);
						} else {
							fieldsOfTypeToCheck = new HashSet<>();
							referencingTypes.put(typeToCheck, fieldsOfTypeToCheck);
						}
						fieldsOfTypeToCheck.add(field); // Match!
					}
				}
			}
			System.out.println("Done. Type is referenced by: "
					+ referencingTypes.keySet().stream().map(DBType::getName).collect(Collectors.joining(", ")));
			if (generateApi) {
				String controllerInterfaceAsString = controllerGenerator.generateInterface(type).toString();
				this.writeToFile(controllerInterfaceAsString, packageName + ".controller" + packageSuffix,
						className + "Controller.java");
			}
			if (generateImpl) {
				String controllerAsString = controllerGenerator.generateController(type, referencingTypes).toString();
				this.writeToFile(controllerAsString, packageName + ".controller" + packageSuffix,
						className + "ControllerImpl.java");
			}
			System.out.println("Domain classes for type '" + type.getName() + "' generated.");
		}

		// Generate utility classes
		if (generateApi) {
			generatedEntities.add("info.scce.dime.util.DomainFile");
			this.writeToFile(utilsGenerator.generateDomainFileController(), packageName + ".util", "DomainFileControllerImpl.java");
			System.out.println("DomainFileControllerImpl generated.");
		}
		if (generateImpl) {
			this.writeToFile(utilsGenerator.generatePersistenceXML(generatedEntities), "resources.META-INF",
					"persistence.xml");
			System.out.println("PersistenceXML generated.");
			this.writeToFile(utilsGenerator.generateTypeInfo(allTypes), packageName + ".util", "TypeInfo.java");
			System.out.println("typeinfo generated.");
			this.writeToFile(
					utilsGenerator.generateAALControllerImpl(
							allTypes.stream().filter(not(DBType::isAbstractType)).collect(Collectors.toSet())),
					packageName + ".util", "AALControllerImpl.java");
			System.out.println("AALController generated.");
			this.writeToFile(utilsGenerator.generateEnumMappingEntity(), packageName + ".util", "EnumMapping.java");
			System.out.println("EnumMapping generated.");
		}
	}

	public static <T> Predicate<T> not(Predicate<T> t) {
		return t.negate();
	}

	public void writeToFile(CharSequence content, String fullPackageName, String fileName) {
		DyWAAbstractGenerator.generate(content.toString(), fullPackageName + '.', fileName, targetDirectory, 0);
	}

	public static Map<DBType, List<DBType>> getInheritanceMap(List<DBType> all) {
		final Map<DBType, List<DBType>> map = new HashMap<>();
		while (map.size() < all.size()) {
			for (DBType t : all) {
				List<DBType> typesToInherit = new LinkedList<>(t.getSuperTypes());

				// filter unrequired types
				Iterator<DBType> it = typesToInherit.iterator();
				check: while (it.hasNext()) {
					DBType removeCandidate = it.next();
					for (DBType otherCandidate : typesToInherit) {
						if (!removeCandidate.equals(otherCandidate)
								&& otherCandidate.getTransitiveSuperTypes().contains(removeCandidate)) { // can be
																											// removed
																											// because
																											// indirectly
																											// inherited
							it.remove();
							continue check;
						}
					}
				}

				map.put(t, typesToInherit);
			}
		}
		return map;
	}

	public static Set<DBType> getAdditionalDelegates(DBType type) {
		Set<DBType> fields = new HashSet<>();
		for (DBType spt : type.getSuperTypes()) {
			if (!spt.isDeleted() && spt.isAbstractType()) {
				Map<Boolean, List<DBType>> partition = spt.getSuperTypes().stream()
						.collect(Collectors.partitioningBy(DBType::isAbstractType));
				fields.addAll(partition.get(false)); // add all non abstract types as additional delegates
				for (DBType abstractType : partition.get(true)) {
					fields.addAll(getAdditionalDelegates(abstractType));
				}
			}
		}
		return fields;
	}
}
