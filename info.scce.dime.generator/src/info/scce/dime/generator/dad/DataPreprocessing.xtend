/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.UserType
import info.scce.dime.data.factory.DataFactory
import graphmodel.internal.InternalModelElementContainer
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.PrimitiveAttribute

class DataPreprocessing extends CincoRuntimeBaseClass {
	
	public val static SWITCHED_TO_ATTR = "dywaSwitchedTo"
	public val static DISPLAY_NAME_ATTR = "dywaDisplayName"

	val registry = ReferenceRegistry.instance
	
	val generatedAttributes = <Attribute> newArrayList
	
	def execute() {
		registry.lookup(Data)
			.flatMap[types]
			.drop(ReferencedType)
			.drop(UserType)
			.filter[!userAttributes.isEmpty]
			.forEach[ type |
				println("[DataPreprocessing] Generate dywaSwitchedTo attribute for " + type.name + " | workbench running: " + org.eclipse.ui.PlatformUI.isWorkbenchRunning)
				val attr =
					if (org.eclipse.ui.PlatformUI.isWorkbenchRunning) {
						type.newComplexAttribute(new String(type.id.bytes.reverse), 0, 0) => [
							name = SWITCHED_TO_ATTR
							dataType = type
							generatedAttributes.add(it)
						]
					}
					/*
					 * workaround that uses the basic DataFactory in headless mode
					 */
					else new DataFactory().createComplexAttribute(
						new String(type.id.bytes.reverse),
						null, // internal model element
						type.internalElement_ as InternalModelElementContainer,
						false // run hooks
					) => [
						type.internalContainerElement.modelElements.add(it.internalElement_)
					]
				attr => [
					it.internalElement_.eSetDeliver(false)
					name = SWITCHED_TO_ATTR
					dataType = type
					it.internalElement_.eSetDeliver(true)
					generatedAttributes.add(it)
				]
			]
		
		registry.lookup(Data)
			.flatMap[types]
			.drop(ReferencedType)
			.filter(EnumType)
			.forEach[ type |
				println("[DataPreprocessing] Generate dywaDisplayName attribute for " + type.name + " | workbench running: " + org.eclipse.ui.PlatformUI.isWorkbenchRunning)
				val attr =
					if (org.eclipse.ui.PlatformUI.isWorkbenchRunning) {
						type.newPrimitiveAttribute(new String(type.id.bytes.reverse), 0, 0) => [
							name = DISPLAY_NAME_ATTR
							dataType = PrimitiveType.TEXT
							generatedAttributes.add(it)
						]
					}
					/*
					 * workaround that uses the basic DataFactory in headless mode
					 */
					else new DataFactory().createPrimitiveAttribute(
						new String(type.id.bytes.reverse),
						null, // internal model element
						type.internalElement_ as InternalModelElementContainer,
						false // run hooks
					) => [
						type.internalContainerElement.modelElements.add(it.internalElement_)
					]
				attr => [
					it.internalElement_.eSetDeliver(false)
					name = DISPLAY_NAME_ATTR
					dataType = PrimitiveType.TEXT
					it.internalElement_.eSetDeliver(true)
					generatedAttributes.add(it)
				]
			]
	}
	
	def cleanup() {
		generatedAttributes.forEach[it.delete]
	}
	
}
