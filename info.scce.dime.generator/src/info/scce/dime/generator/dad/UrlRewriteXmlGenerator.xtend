/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.generator.dad

import de.jabc.cinco.meta.plugin.dsl.ProjectDescriptionLanguage
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.dad.dad.DAD
import java.nio.file.Path

/**
 * Generator for the urlrewrite.xml file
 */
class UrlRewriteXmlGenerator {
	
	protected extension ProjectDescriptionLanguage = new ProjectDescriptionLanguage
	protected extension DimeIOExtension = new DimeIOExtension
	
	def generate(DAD dad, Path outlet) {
		outlet
			.resolve("dywa-app")
			.resolve("app-presentation")
			.resolve("src")
			.resolve("main")
			.resolve("webapp")
			.resolve("WEB-INF")
			.createDirectories()
			.resolve("urlrewrite.xml")
			.writeString(dad.template)
	}
	
	def template(DAD dad) '''
		<?xml version="1.0" encoding="utf-8"?>
		<!DOCTYPE urlrewrite PUBLIC "-//tuckey.org//DTD UrlRewrite 4.0//EN" "http://www.tuckey.org/res/dtds/urlrewrite4.0.dtd">
		<urlrewrite>
			«IF !dad.graphQLComponents.empty»
				<rule>
					<from>/graphql/</from>
					<to last="true">-</to>
				</rule>
		    «ENDIF»
			<rule>
				<from>/asset/</from>
				<to last="true">-</to>
			</rule>
			<rule>
				<from>/css/</from>
				<to last="true">-</to>
			</rule>
			<rule>
				<from>/favicon/</from>
				<to last="true">-</to>
			</rule>
			<rule>
				<from>/fonts/</from>
				<to last="true">-</to>
			</rule>
			<rule>
				<from>/img/</from>
				<to last="true">-</to>
			</rule>
			<rule>
				<from>/js/</from>
				<to last="true">-</to>
			</rule>
			<rule>
				<from>/main.dart.js</from>
				<to last="true">-</to>
			</rule>
			<rule>
				<from>/rest/</from>
				<to last="true">-</to>
			</rule>
			<rule match-type="wildcard">
				<!-- prevent infinite loops -->
				<condition type="request-uri" operator="notequal">/app/index.html</condition>
				<condition type="request-uri" operator="notequal">/app/login.jsp</condition>
				<condition type="request-uri" operator="notequal">/index.html</condition>
				<condition type="request-uri" operator="notequal">/login.jsp</condition>
				<from>/**</from>
				<to>/index.html</to>
			</rule>
		</urlrewrite>
	'''
}