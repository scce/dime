/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.replacement.generic

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.process.process.GenericSIB

import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.*

class GenericReplacement extends CincoRuntimeBaseClass {
	
	val registry = ReferenceRegistry.instance
	val replacements = <GenericSIBReplacement> newArrayList 
	
	def execute() {
		val genericSIBs = registry.lookup(GenericSIB).toSet
		for (genericSIB : genericSIBs) {
			if (genericSIB.referencedObject.isTransformable) {
				if (genericSIB.referencedObject.isGUI) {
					new GenericGUISIBReplacement => [
						replacements.add(it)
						replace(genericSIB)
					]
				} else {
					new GenericProcessSIBReplacement => [
						replacements.add(it)
						replace(genericSIB)
					]
				}
			}
		}
	}
	
	def cleanup() {
		replacements.forEach[it.cleanup]
	}
}
