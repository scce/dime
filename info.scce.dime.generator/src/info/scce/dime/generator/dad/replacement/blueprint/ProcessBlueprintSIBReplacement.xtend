/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.replacement.blueprint

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessBlueprintSIB
import info.scce.dime.process.process.StartSIB

import static extension info.scce.dime.process.helper.PortUtils.addInput

class ProcessBlueprintSIBReplacement extends BlueprintSIBReplacement<ProcessBlueprintSIB> {
	
	val registry = ReferenceRegistry.instance
	
	override replace(ProcessBlueprintSIB blueSib) {
		log("Replacing ProcessBlueprintSIB " + blueSib.displayName + " in " + blueSib.eResource.URI)
		val dummyGUI
			= switch blueSib.processType {
				case BASIC: createDummyGUI(blueSib)
				default: null
			}
		val replProcess = createReplacementProcess(blueSib, dummyGUI)
		blueSib.replaceWith(replProcess)
	}
	
	def createDummyGUI(ProcessBlueprintSIB blueSib) {
		val outlet = blueSib.outlet.resolve("gui")
		val fileName = blueSib.id + "_ReplacementGUIDummy"
		
		val gui = createGUI(outlet, fileName)
		generatedModelFiles.add(gui.eResource.URI)
		debug(" > Dummy GUI: " + gui?.title + " (" + gui?.id + ")")
		
		gui.addDummyGUIContent(blueSib)
		
		registry.register(gui)
		gui.save
		return gui
	}
	
	def createReplacementProcess(ProcessBlueprintSIB blueSib, GUI dummyGUI) {
		val outlet = blueSib.outlet.resolve("basic")
		val fileName = blueSib.id + "_ReplacementProcessDummy"
		
		val process = createBasicProcess(outlet, fileName)
		generatedModelFiles.add(process.eResource.URI)
		debug(" > Replacement Process: " + process?.modelName + " (" + process?.id + ")")
		
		val startSIB = process.findThe(StartSIB)
		
		for (input : blueSib.inputs) {
			startSIB.addOutputPort(input) => [
				debug("     > StartSIB port: " + name)
			]
		}
		
		val branches = blueSib.branchBlueprintSuccessors
		if (branches.isEmpty) {
			process.find(EndSIB).toSet.forEach[delete]
			if (dummyGUI !== null) {
				val guiSib = process.newGUISIB(dummyGUI, 0, 0)
				startSIB.successor = guiSib
			}
		} else {
			val defaultBranch = blueSib.defaultBranch
			debug("   > Default branch: " + defaultBranch)
			val firstBranch = defaultBranch ?: branches.head
			debug("   > First branch: " + firstBranch)
			
			val endSib = process.findThe(EndSIB) => [
				debug("   > EndSIB: " + it)
				branchName = firstBranch.name
				debug("   > EndSIB " + branchName)
				for (port : firstBranch.outputs) {
					debug("     > Port " + port.name)
					it.addInput(port)
				}
			]
			
			startSIB.newControlFlow(endSib)
			
			val otherBranches
				= if (defaultBranch === null) branches.tail
				  else branches.filter[it !== defaultBranch]
			
			for (branch : otherBranches) {
				process.newEndSIB(0,0) => [
					branchName = branch.name
					debug("   > EndSIB " + branchName)
					for (port : branch.outputs) {
						debug("     > Port " + port.name)
						it.addInput(port)
					}
				]
			}
		}
		
		registry.register(process)
		process.save
		return process
	}
	
	def replaceWith(ProcessBlueprintSIB blueSib, Process process) {
		blueSib.replaceWith(
			blueSib.rootElement.newProcessSIB(process, blueSib.x, blueSib.y, blueSib.width, blueSib.height) => [
				label = blueSib.label
				name = blueSib.name
			]
		)
	}
	
}
