/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.replacement.generic

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.GenericSIB

import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.*

class GenericGUISIBReplacement extends GenericSIBReplacement {
	
	val registry = ReferenceRegistry.instance
	
	override replace(GenericSIB sib) {
		val GUI gui = sib.referencedObject.transformedGUI
		generatedModelFiles.add(gui.eResource.URI)
		registry.register(gui)
		gui.save
		sib.replaceWith(gui)
	}
	
	def void replaceWith(GenericSIB sib, GUI gui) {
		
		sib.replaceWith(
			sib.rootElement.newGUISIB(gui, sib.x, sib.y, sib.width, sib.height) => [
				label = sib.label
				name = sib.name
				majorPage = sib.guiOptions.majorPage
				majorBranch = sib.guiOptions.majorBranch
				cacheGUI = sib.guiOptions.cacheGUI
				defaultContent = sib.guiOptions.defaultContent
			]
		)
	}
	
}
