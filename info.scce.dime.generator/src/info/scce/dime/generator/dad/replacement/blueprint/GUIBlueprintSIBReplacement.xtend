/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.replacement.blueprint

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.gui.factory.GUIFactory
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.DataContext
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.HeadlineSize
import info.scce.dime.gui.gui.Row
import info.scce.dime.gui.gui.Template
import info.scce.dime.process.actions.CreateInputDataContext
import info.scce.dime.process.process.BranchBlueprint
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.StartSIB
import org.eclipse.xtend.lib.annotations.Accessors

import static extension info.scce.dime.process.helper.PortUtils.addInput

class GUIBlueprintSIBReplacement extends BlueprintSIBReplacement<GUIBlueprintSIB> {
	
	val registry = ReferenceRegistry.instance
	
	@Accessors Boolean simpleDialog = false
	
	override replace(GUIBlueprintSIB blueSib) {
		log("Replacing GUIBlueprintSIB " + blueSib.displayName + " in " + blueSib.eResource.URI)
		val branches = blueSib.branchBlueprintSuccessors
		val replProcess
			= if (branches.isEmpty) null
			  else createDummyProcess(blueSib)
		val replGUI = createReplacementGUI(blueSib, replProcess)
		blueSib.replaceWith(replGUI)
	}
	
	def createDummyProcess(GUIBlueprintSIB blueSib) {
		val branches = blueSib.branchBlueprintSuccessors.drop[
			if (simpleDialog) find(Output).isEmpty else false
		]
		if (branches.nullOrEmpty) return null;
		val defaultBranch
			= if (simpleDialog) null
			  else blueSib.defaultBranch
		createDummyProcess(blueSib, branches, defaultBranch)
	}
	
	def createDummyProcess(GUIBlueprintSIB blueSib, Iterable<BranchBlueprint> branches, BranchBlueprint defaultBranch) {
		val outlet = blueSib.outlet.resolve("basic")
		val fileName = blueSib.id + "_ReplacementProcessDummy_" + System.currentTimeMillis
		val process = createBasicProcess(outlet, fileName)
		debug("> Replacement Process: " + process?.modelName + " (" + process?.id + ")")
		generatedModelFiles.add(process.eResource.URI)
		
		debug("  > Default branch: " + defaultBranch)
		val firstBranch = defaultBranch ?: branches.head
		debug("  > First branch: " + firstBranch)
		
		val endSib = process.findThe(EndSIB) => [
		debug("  > EndSIB: " + it)
			
			branchName = firstBranch.name
			debug("  > EndSIB " + branchName)
			if (!firstBranch.outputs.isEmpty) {
				for (port : firstBranch.outputs) {
					debug("    > Port " + port.name)
					it.addInput(port)
				}
				debug("    > Create input data context")
				new CreateInputDataContext().execute(it)
			}
		]
		
		process.findThe(StartSIB).newControlFlow(endSib)
		
		val otherBranches
			= if (defaultBranch === null) branches.tail
			  else branches.filter[it !== defaultBranch]
		
		for (branch : otherBranches) {
			debug("  > Branch: " + firstBranch)
			process.newEndSIB(0,0) => [
				branchName = branch.name
				debug("  > EndSIB " + branchName)
				if (!branch.outputs.isEmpty) {
					for (port : branch.outputs) {
						debug("    > Port " + port.name)
						it.addInput(port)
					}
					debug("    > Create input data context")
					new CreateInputDataContext().execute(it)
				}
			]
		}
		
		registry.register(process)
		process.save
		return process
	}
	
	def createReplacementGUI(GUIBlueprintSIB blueSib, Process replProcess) {
		val outlet = blueSib.outlet.resolve("gui")
		val fileName = blueSib.id + "_ReplacementGUIDummy"
		
		val gui = createGUI(outlet, fileName)
		generatedModelFiles.add(gui.eResource.URI)
		debug("> Replacement GUI: " + gui?.title + " (" + gui?.id + ")")
		
		val dataContext = gui.findThe(DataContext)
		debug("  > Data Context " + dataContext)
		
		for (input : blueSib.inputs) {
			dataContext.addVariable(input) => [
				isInput = true
				debug("    > Input variable: " + name)
			]
		}
		
		if (simpleDialog) gui.addSimpleDialogContent(blueSib)
		else gui.addDummyGUIContent(blueSib)
		
		if (replProcess !== null) {
			val template = gui.findThe(Template) ?: gui.newTemplate(0,0)
			val row = template.newRow(0, template.height-1) => [
				generalStyle?.setRawContent("display:none;")
			]
			val col = row.findThe(Col) ?: row.newCol(0,0)
			col.newProcessSIB(replProcess, 0, col.height-1) => [
				label = replProcess.modelName
				debug("  > InteractionSIB: " + label)
				modal = GUIFactory.eINSTANCE.createGUISIBModal => [
					buttonLabel = blueSib.defaultBranch?.name ?: "proceed"
					debug("    > Modal button label: " + buttonLabel)
				]
			]
		}
		
		registry.register(gui)
		gui.save
		return gui
	}
	
	def addSimpleDialogContent(GUI gui, GUIBlueprintSIB blueSib) {
		val template = gui.findThe(Template) ?: gui.newTemplate(0,0)
		debug("  > Template: " + template)
		var row = template.findThe(Row) ?: template.newRow(0,0)
		debug("  > Row: " + row)
		var col = row.findThe(Col) ?: row.newCol(0,0)
		debug("  > Col: " + col)
		col.newHeadline(0, col.height-1) => [
			content?.head?.setRawContent(blueSib.label ?: "")
			size = HeadlineSize.SMALL
		]
		col.newText(0, col.height-1) => [
			content?.head?.setRawContent(blueSib.documentation ?: "")
		]
		val branches = blueSib.branchBlueprintSuccessors.filter[find(Output).isEmpty].toList
		if (!branches.isEmpty) {
			row = template.newRow(0, template.height-1) => [
				generalStyle?.setRawContent("margin-top: 15px;");
			]
			col = row.findThe(Col) ?: row.newCol(0,0)
			val buttons = col.newButtonGroup(0,0) => [ buttons.forEach[delete] ]
			debug("  > ButtonGroup: " + buttons)
			branches.forEach[ branch |
				buttons.newButton(buttons.width-1, buttons.height-1) => [
					label = branch.name
					displayLabel = branch.name
					debug("    > Button: " + label)
				]
			]
		}
	}
	
	def replaceWith(GUIBlueprintSIB blueSib, GUI gui) {
		blueSib.replaceWith(
			blueSib.rootElement.newGUISIB(gui, blueSib.x, blueSib.y, blueSib.width, blueSib.height) => [
				label = blueSib.label
				name = blueSib.name
				majorPage = blueSib.majorPage
				defaultContent = blueSib.defaultContent
			]
		)
	}
	
}
