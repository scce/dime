/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.replacement.blueprint

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.dad.dad.BlueprintGUIReplacement
import info.scce.dime.dad.dad.BlueprintingConfig
import info.scce.dime.process.process.BlueprintSIB
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.ProcessBlueprintSIB

class BlueprintReplacement extends CincoRuntimeBaseClass {
	
	val registry = ReferenceRegistry.instance
	
	val BlueprintingConfig config
	val replacements = <BlueprintSIBReplacement<?>> newArrayList 
	
	new(BlueprintingConfig config) {
		this.config = config
	}
	
	def execute() {
		val blueSIBs = registry.lookup(BlueprintSIB).toSet
		for (blueSIB : blueSIBs) switch blueSIB {
			
			GUIBlueprintSIB: new GUIBlueprintSIBReplacement => [
				replacements.add(it)
				simpleDialog = switch config?.getGuiReplacement {
					BlueprintGUIReplacement case SIMPLE_DIALOG: true
					default: false
				}
				replace(blueSIB)
			]
			
			ProcessBlueprintSIB: new ProcessBlueprintSIBReplacement => [
				replacements.add(it)
				replace(blueSIB)
			]
		}
	}
	
	def cleanup() {
		replacements.forEach[it.cleanup]
	}
	
}
