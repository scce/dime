/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.core.utils.job.CompoundJob
import de.jabc.cinco.meta.core.utils.job.JobFactory
import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator2
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.StartupProcessPointer
import info.scce.dime.data.data.Data
import info.scce.dime.generator.dad.replacement.blueprint.BlueprintReplacement
import info.scce.dime.generator.dad.replacement.generic.GenericReplacement
import info.scce.dime.generator.data.DywaDataGenerator
import info.scce.dime.generator.gui.dart.base.DartGUIGenerator
import info.scce.dime.generator.migration.EnumMigrator
import info.scce.dime.generator.process.BackendProcessGenerator
import info.scce.dime.generator.process.BackendProcessGeneratorHelper
import info.scce.dime.generator.process.StartupProcessGenerator
import info.scce.dime.generator.rest.FileControllerGenerator
import info.scce.dime.generator.rest.TOGenerator
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.profile.actions.ApplyProfile
import java.nio.file.Path
import java.util.ArrayList
import java.util.List
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.core.runtime.OperationCanceledException
import org.eclipse.core.runtime.jobs.Job
import org.eclipse.xtend.lib.annotations.Accessors

import static info.scce.dime.gui.helper.GUIExtensionProvider.disableRefresh
import static info.scce.dime.gui.helper.GUIExtensionProvider.enableRefresh
import static info.scce.dime.process.process.ProcessType.NATIVE_FRONTEND_SIB_LIBRARY
import static org.eclipse.core.resources.ResourcesPlugin.FAMILY_AUTO_BUILD
import static org.eclipse.core.resources.ResourcesPlugin.FAMILY_MANUAL_BUILD

import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.generateContent
import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.isTransformable

class Generator implements IGenerator2<DAD> {
	
	extension DimeIOExtension = new DimeIOExtension()
	
	var Path outlet
	var IProgressMonitor monitor = new NullProgressMonitor()
	var DAD model
	var GenerationContext genctx
	var BackendProcessGeneratorHelper backendProcessGeneratorHelper
	var List<Data> dataModels
	var List<ApplyProfile> applyProfileActions
	var BlueprintReplacement blueprintReplacement
	var GenericReplacement genericReplacement
	var DataPreprocessing dataPreprocessing
	var long debugTime
	
	@Accessors
	var boolean headless = false

//	var List<CheckProcess<?, ?>> checkresults
//	var boolean proceedWithInvalidModels = true
	
	override collectTasks(DAD model, Path outlet, CompoundJob job) {
		
		this.model = model
		this.outlet = outlet
		disableRefresh()
		this.genctx = new GenerationContext(model)
		
		job
			.consume(5)
				.task("Waiting for builds to finish")           [ waitForBuilds() ]
				.task("Creating basic backend template")        [ copyStaticResources() ]
				.task("Copying additional resources")           [ copyAdditionalResources() ]
				.task("Initializing Generator")                 [ initialize() ]
				.task("Applying profiles")                      [ applyProfiles() ]
				.task("Replacing blueprints")                   [ replaceBlueprints() ]
				.task("Replacing generic SIBs")                 [ replaceGenericSIBs() ]
				.task("Pre-processing data models")             [ preprocessDataModels() ]
				.task("Collecting models")                      [ collectModels() ]
				.task("Generating backend files")               [ generateBackendFiles() ]
			
//			.consume(20, "Validating models")
//				.task("Collecting checks")                      [ collectChecks(checks) ]
//				.taskForEach(
//					[ checks.entrySet.stream ],
//					[ value.run() ],                            // Runnable
//					[ key ]                                     // Task name
//				)
//				.task("Evaluating validation results")          [ processValidationResults() ]
//				.cancelIf[!proceedWithInvalidModels]
			
			// Sequential, as these generators produce inputs for subsequent ones
			.consume(50, "Generating")
				.task("Generating data models")                 [ generateDataModels() ]
			
			// Sequential, as some of these generators output nothing when used concurrently
			.consume(30, "Generating")
				// Generate GUI before process + REST backend because it pre-computes the necessary selectives
				.task("Generating GUI")                         [ generateGUI() ]
				.task("Generating process class for backend processes") [ generateBackendProcess() ]
				.task("Generating startup process")             [ generateStartupProcess() ]
				.task("Generating external SIBs")               [ generateGenericSIBModels() ]
				.task("Generating authenticator")               [ generateAuthenticator() ]
				.task("Generating default REST TOs")            [ generateDefaultRestTOs() ]
				.task("Generating file token cache")            [ generateFileTokenCache() ]
				.task("Generating migrators")                   [ generateMigrators() ]
				.task("Generating GraphQL schema")              [ generateGraphQLSchema() ]
				.task("Generating operations folder structure") [ generateOperations() ]
				.task("Enabling extension cache refresh")       [ enableRefresh() ]
			
			.onDone [ cleanup() ]
			
	}

	def package void waitForBuilds() {
		val jobManager = Job.jobManager
		var family = FAMILY_AUTO_BUILD
		var jobs = jobManager.find(family)
		if (jobs.empty) {
			family = FAMILY_MANUAL_BUILD
			jobs = jobManager.find(family)
		}
		while (jobs.length > 0) {
			try {
				// Wait for manual build to finish if running
				println("[INFO] Waiting for running builds to finish.")
				jobManager.join(family, monitor)
				family = FAMILY_AUTO_BUILD
				jobs = jobManager.find(family)
				if (jobs.empty) {
					family = FAMILY_MANUAL_BUILD
					jobs = jobManager.find(family)
				}
			}
			catch (OperationCanceledException | InterruptedException e) {
				e.printStackTrace()
			}
		}
		println("[INFO] No running builds, asserting ReferenceRegistry is initialized.")
		ReferenceRegistry.instance.lookup(DAD)
		println("[INFO] ReferenceRegistry is initialized, proceed.")
	}

	override void generate(DAD model, Path outlet, IProgressMonitor monitor) {
		val job = JobFactory.job("DAD Generator", monitor, false)
		collectTasks(model, outlet, job)
		job
			.onFinishedShowMessage("Code generation successful.")
			.schedule()
	}

	def package void initialize() {
		this.debugTime = System.currentTimeMillis
		this.genctx = new GenerationContext(model)
		this.backendProcessGeneratorHelper = new BackendProcessGeneratorHelper(genctx)
		this.applyProfileActions = new ArrayList()
	}

	def package void collectModels() {
		// Trigger model collection, ignore return value
		genctx.usedModels
	}

	def package void applyProfiles() {
		model
			.profileContainers
			.flatMap[profileSIBs]
			.filter[active]
			.map[referencedProfile]
			.filterNull()
			.forEach[ profile |
				val applyProfile = new ApplyProfile()
				applyProfileActions.add(applyProfile)
				applyProfile.execute(profile)
			]
	}

	def package void replaceBlueprints() {
		blueprintReplacement = new BlueprintReplacement(model.blueprintingConfig)
		blueprintReplacement.execute()
	}

	def package void replaceGenericSIBs() {
		genericReplacement = new GenericReplacement()
		genericReplacement.execute()
	}

	def package void preprocessDataModels() {
		dataPreprocessing = new DataPreprocessing()
		dataPreprocessing.execute()
	}

	def package void cleanup() {
		if (!headless) {
			blueprintReplacement.cleanup()
			genericReplacement.cleanup()
			dataPreprocessing.cleanup()
			ReferenceRegistry.instance.clearRegistry()
			println('''[INFO] Generation finished in «System.currentTimeMillis - debugTime» ms.''')
		}
	}

//	def private void processValidationResults() {
//		proceedWithInvalidModels = true
//		checkresults
//			.filter[ hasErrors /* || hasWarnings */ ]
//			.forEach[ println ]
//		if (checkresults.exists[hasErrors]) {
//			val display = Display.getDefault()
//			if (display !== null) {
//				display.syncExec [
//					proceedWithInvalidModels = MessageDialog.openConfirm(
//						null,
//						"Invalid models!",
//						"Some models used in code-generation are not valid! Check the Project-Validation-View for more information. Continue anyway?"
//					)
//				]
//			}
//			else {
//				proceedWithInvalidModels = false
//			}
//		}
//	}

	// Generate data models
	def private void generateDataModels() {
		dataModels = model.dataComponents.map[ component | component.model ]
		new DywaDataGenerator()
			.generate(dataModels, outlet)
	}

	def private void generateGenericSIBModels() {
		for (referencedObject: genctx.genericSIBReferences) {
			if (!referencedObject.transformable) {
				referencedObject.generateContent()
			}
		}
	}

	def private void generateGraphQLSchema() {
		val graphQLModel = model.graphQLComponents.map[ component | component.model ].head
		if (graphQLModel !== null) {
			val graphQLGenerator = new info.scce.dime.generator.graphql.Generator(genctx)
			graphQLGenerator.generate(graphQLModel, outlet, monitor)
		}
	}

	// Generate process class for all backend processes
	def private void generateBackendProcess() {
		val backendProcessGenerator = new BackendProcessGenerator(backendProcessGeneratorHelper)
		backendProcessGenerator.projectPath = model.projectPath
		backendProcessGenerator.userType = model.systemUsers.head?.systemUser
		println("Generating ContextTransformer")
		backendProcessGenerator.generateContextTransformer()
		for (process: genctx.usedProcesses.reject[processType == NATIVE_FRONTEND_SIB_LIBRARY]) {
			println('''Generating Process: «process.modelName»''')
			backendProcessGenerator.generate(process, outlet, monitor)
		}
	}

	def private void generateBackendFiles() {
		new ShiroIniGenerator()
			.generate(model, outlet)
		new UrlRewriteXmlGenerator()
			.generate(model, outlet)
	}

	def private void copyStaticResources() {
		// outlet points to 'target/'
		new StaticResourcesHandler(outlet) => [
			copyStaticJavaResources()
			copyStaticDartResources()
			copyStaticDockerResources()
		]
	}

	def private void copyFolder(String source, String target) {
		val sourceFolder = outlet.resolve(source)
		if (sourceFolder.exists) {
			val targetFolder = outlet.resolve(target)
			println('''[INFO] Target folder «targetFolder» (exists: «targetFolder.exists»)''')
			targetFolder.createDirectories()
			sourceFolder.copyRecursively(targetFolder)
		}
		else {
			println('''[INFO] Skip copying static resources (source folder «sourceFolder» does not exist)''')
		}
	}

	// Copy backend additional resources
	def private void copyAdditionalResources() {
		copyFolder("../dependency", "dywa-app/app-addon")
		copyFolder("../asset", "dywa-app/app-presentation/src/main/webapp/asset")
		copyFolder("../initFiles", "dywa-app/app-presentation/src/main/resources")
	}

	// Generate startup process
	def private void generateStartupProcess() {
		val startupProcess = model.getEdges(StartupProcessPointer).head?.targetElement?.model
		if (startupProcess !== null) {
			new StartupProcessGenerator(model)
				.generate(startupProcess, outlet, monitor)
		}
	}

	def private void generateAuthenticator() {
		new AuthenticationGenerator()
			.generate(model, outlet.resolve("dywa-app/app-business/target/generated-sources"))
	}

	def private void generateGUI() {
		new DartGUIGenerator(genctx, backendProcessGeneratorHelper)
			.generate(model, outlet, monitor) // Philip does the real stuff here
	}

	def private void generateOperations() {
		new OperationsGenerator(model)
			.generate()
	}

	def private void generateDefaultRestTOs() {
		new TOGenerator()
			.generate(dataModels, outlet)
	}

	def private void generateFileTokenCache() {
		new FileControllerGenerator()
			.generate(outlet)
	}

	def private void generateMigrators() {
		new EnumMigrator()
			.generate(dataModels, outlet)
	}

//	def private void collectChecks(Map<String, Runnable> checks) {
//		checkresults = newArrayList()
//		val dadFE = new DADExecution()
//		val dataFE = new DataExecution()
//		val processFE = new ProcessExecution()
//		val guiFE = new GUIExecution()
//		checks.put('''DAD model «model.id»''', [
//			val adapter = dadFE.initApiAdapterFromResource(model.eResource, model.file)
//			val checkProcess = dadFE.executeCheckPhase(adapter)
//			checkresults.add(checkProcess)
//		])
//		for (component: model.dataComponents) {
//			checks.put('''Data model «component.id»''', [
//				val adapter = dataFE.initApiAdapterFromResource(component.model.eResource, component.model.file)
//				val checkProcess = dataFE.executeCheckPhase(adapter)
//				checkresults.add(checkProcess)
//			])
//		}
//		for (process: genctx.usedProcesses) {
//			checks.put('''Process model «process.id»''', [
//				val adapter = processFE.initApiAdapterFromResource(process.eResource, process.file)
//				val checkProcess = processFE.executeCheckPhase(adapter)
//				checkresults.add(checkProcess)
//			])
//		}
//		for (gui: genctx.usedGUIs) {
//			checks.put('''GUI model «gui.id»''', [
//				val adapter = guiFE.initApiAdapterFromResource(gui.eResource, gui.file)
//				val checkProcess = guiFE.executeCheckPhase(adapter)
//				checkresults.add(checkProcess)
//			])
//		}
//	}

//	/**
//	 * Get file for model (eObject)
//	 */
//	def private File getFile(org.eclipse.emf.ecore.EObject model) {
//		val uri = org.eclipse.emf.ecore.util.EcoreUtil.getURI(model)
//		val resolvedUri = org.eclipse.emf.common.CommonPlugin.resolve(uri)
//		val path = new org.eclipse.core.runtime.Path(resolvedUri.toFileString())
//		val iFile = org.eclipse.core.resources.ResourcesPlugin.workspace.root.getFile(path)
//		val file = iFile.fullPath.toFile()
//		if (file === null) {
//			throw new RuntimeException('''Could not find file for «model»''')
//		}
//		if (!file.exists) {
//			throw new RuntimeException('''File does not exist for «model»''')
//		}
//		return file
//	}
	
}
