/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations.infrastructure

import de.jabc.cinco.meta.plugin.template.FileTemplate
import info.scce.dime.dad.dad.NonProductionServer
import info.scce.dime.dad.dad.ProductionServer
import info.scce.dime.dad.dad.Server

class InventoryTemplate extends FileTemplate {
	
	Server server
	
	new (Server server) {
		this.server = server
	}
	
	override getTargetFileName() {
		'''«server.serverName».yml'''
	}
	
	override template() {
		switch server {
			ProductionServer : server.productionTemplate
			NonProductionServer : server.nonProductionTemplate
		}			
	}
	
	def productionTemplate(Server server) '''
		---
		all:
		  hosts:
		    «server.hostName»:
		      deployment_tier: «server.deploymentTier»
	'''
	
	def nonProductionTemplate(Server server) '''
		---
		all:
		  children:
		    non_production:
		      hosts:
		        «server.hostName»:
		          deployment_tier: «server.deploymentTier»
	'''
}
