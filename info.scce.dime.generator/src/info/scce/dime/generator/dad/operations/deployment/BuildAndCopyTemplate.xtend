/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations.deployment

import de.jabc.cinco.meta.plugin.template.FileTemplate
import info.scce.dime.dad.dad.Servers

class BuildAndCopyTemplate extends FileTemplate {
	
	Servers servers

	new (Servers servers) {
		this.servers = servers
	}
	
	override getTargetFileName() {
		"build-and-copy.sh"
	}
	
	override template() '''
		#!/bin/bash
		
		DEPLOY_USER="«servers.deployUser»"
		APP_DIRECTORY="/home/${DEPLOY_USER}/app"
		RSYNC_CMD="/usr/bin/rsync --recursive --update --progress --delete"
		MAVEN_REPOSITORY="${APP_DIRECTORY}/maven/repository"
		MAVEN_REPOSITORY_DIME_DIRECTORY="${MAVEN_REPOSITORY}/info/scce/dime/"
		set -e
		
		function copy() {
			${RSYNC_CMD} ../../«project.name»/target/{webapp,dywa-app} --exclude "dywa-app/app-dywa-bridge/*/target" --exclude "dywa-app/app-dywa-bridge/*wildfly.path*" ${DEPLOY_USER}@$1:${APP_DIRECTORY}/src/
			${RSYNC_CMD} ../../maintenance-page ${DEPLOY_USER}@$1:${APP_DIRECTORY}/src/
			${RSYNC_CMD} ~/.m2/repository/info/scce/dime/{app-parent,app-business,app-addon,app-addon-parent} ${DEPLOY_USER}@$1:${MAVEN_REPOSITORY_DIME_DIRECTORY}
			«IF !(servers.nativeLibraryGroupId.nullOrEmpty || servers.nativeLibraryArtifactId.nullOrEmpty)»
				MAVEN_REPOSITORY_NATIVE_DIRECTORY="${MAVEN_REPOSITORY}/«servers.nativeLibraryGroupId.dotToSlash»/"
				/usr/bin/ssh ${DEPLOY_USER}@$1 /bin/mkdir -p ${MAVEN_REPOSITORY_NATIVE_DIRECTORY}
				${RSYNC_CMD} ~/.m2/repository/«servers.nativeLibraryGroupId.dotToSlash»/«servers.nativeLibraryArtifactId» ${DEPLOY_USER}@$1:${MAVEN_REPOSITORY_NATIVE_DIRECTORY}
			«ENDIF»
		}
		
		function repairshiro() {
			sed -i "/logout.redirectUrl = .*/c\logout.redirectUrl = https://$1" ../../«project.name»/target/dywa-app/app-presentation/src/main/webapp/WEB-INF/shiro.ini
		}
		
		function build() {
			cd ../../«project.name»/target/dywa-app
			mvn install -U -Ddime.native=true
			cd -
		}
		
		case "$1" in
		«FOR server : servers.servers »
			"--«server.serverName»")
				repairshiro «server.domainName» 
				build
				copy «server.hostName»
				;;
		«ENDFOR»
		  *)
			echo "Please specify the server: «serversOptions»"
			exit 0
			;;
		esac
	'''
	
	def serversOptions() '''
		«FOR server : servers.servers BEFORE '[' SEPARATOR ', ' AFTER ']' »\"--«server.serverName»\"«ENDFOR»
	'''
	
	def dotToSlash(String s) {
		s.replaceAll("\\.", "/")	
	}

}
