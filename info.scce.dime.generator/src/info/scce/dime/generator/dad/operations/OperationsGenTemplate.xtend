/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations

import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.deployment.BuildAndCopyTemplate
import info.scce.dime.generator.dad.operations.deployment.DimeVersionTemplate
import info.scce.dime.generator.dad.operations.deployment.GenerateShTemplate
import info.scce.dime.generator.dad.operations.deployment.ReadmeMdTemplate
import info.scce.dime.generator.dad.operations.infrastructure.AllVarsYmlTemplate
import info.scce.dime.generator.dad.operations.infrastructure.AnsibleReadmeMdTemplate
import info.scce.dime.generator.dad.operations.infrastructure.HostVarsTemplate
import info.scce.dime.generator.dad.operations.infrastructure.InfrastructureReadmeMdTemplate
import info.scce.dime.generator.dad.operations.infrastructure.InventoryTemplate
import info.scce.dime.generator.dad.operations.infrastructure.MakefileTemplate
import info.scce.dime.generator.dad.operations.infrastructure.NonProductionVarsYmlTemplate

class OperationsGenTemplate extends ProjectTemplate {
	
	DAD dad
	Servers servers
	
	new (DAD dad) {
		this.dad = dad
		this.servers = dad.serverss.head
	}
	
	override protected _projectName() {
		dad.project.name
	}
	
	override _projectDescription() {

		val project = dad.project
		project(project.name) [
			deleteIfExistent = false
			folder ("operations-gen") [
				folder ("deployment") [
					file (new BuildAndCopyTemplate(servers))
					file (new DimeVersionTemplate(servers))
					file (new GenerateShTemplate(servers))
					file (new ReadmeMdTemplate(servers))
				]
				folder ("infrastructure") [
					folder ("ansible") [
						folder ("group_vars") [
						 	folder ("all") [
						 		file (new AllVarsYmlTemplate(servers))
						 		if (!servers.allVault.nullOrEmpty)
									copyFile ("vault.yml", servers.allVault, PROJECT)
						 	]	
						 	folder ("non_production") [
						 		file (new NonProductionVarsYmlTemplate())
						 		if (!servers.nonProductionVault.nullOrEmpty)
									copyFile ("vault.yml", servers.nonProductionVault, PROJECT)								
						 	]
						]
						folder ("host_vars") [
							forEachOf(servers.servers) [ server |
								folder (server.hostName) [
									file(new HostVarsTemplate(server))
									if (!server.vault.nullOrEmpty)
										copyFile ("vault.yml", server.vault, PROJECT)
								]
							]
						]
						folder ("inventories") [
							forEachOf(servers.servers) [ server |
								println('''inventory for «server.serverName»''')
								file(new InventoryTemplate(server))	
							]	
						]
						/* 
						 * ansible.cfg
						 * install.yml
						 * requirements.txt
						 */
						filesFromBundle("info.scce.dime.generator" -> "static-resources/dad/operations/infrastructure/ansible")	
						file (new MakefileTemplate(servers))
						file (new AnsibleReadmeMdTemplate())
					]
					file (new InfrastructureReadmeMdTemplate(servers))

					
				]		
			]	
			
		]
		
		/* 
		
		Example internal DSL usage from primeviewer meta plug-in
		
		project [
			folder ("src") [
				pkg [
					file (ActivatorTmpl)
				]
				forEachOf(primeNodes) [n |
					pkg (subPackage(n.primeTypePackagePrefix)) [
						files = #[
							new ContentProviderTmpl(n),
							new LabelProviderTmpl(n),
							new ProviderHelperTmpl(n)
						]
					]
				]
			]
			file (PluginXmlTmpl)
			
			activator = '''«basePackage».Activator'''
			lazyActivation = true
			requiredBundles = #[
				model.projectSymbolicName,
				"org.eclipse.ui",
				"org.eclipse.core.runtime",
				"org.eclipse.core.resources",
				"org.eclipse.ui.navigator",
				"org.eclipse.emf.common",
				"org.eclipse.emf.ecore"
			]
			binIncludes = #[
				"plugin.xml"
			]
		]
		
		*/
	}
	
}
