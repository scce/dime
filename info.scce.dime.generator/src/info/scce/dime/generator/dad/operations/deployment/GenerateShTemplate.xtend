/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations.deployment

import de.jabc.cinco.meta.plugin.template.FileTemplate
import info.scce.dime.dad.dad.Server
import info.scce.dime.dad.dad.Servers

class GenerateShTemplate extends FileTemplate {
	
	Servers servers

	new (Servers servers) {
		this.servers = servers
	}
	
	override getTargetFileName() {
		"generate.sh"
	}
	
	override template() '''
		#!/bin/bash
		
		set -e
		
		
		case "$1" in
		«FOR server : servers.servers »
			"--«server.serverName»")
				LATEST_DIME_ALLOWED=«server.latestDimeAllowed»
				;;
		«ENDFOR»
		  *)
		    echo "Please specify the server: «serversOptions»"
		    exit 0
		    ;;
		esac
		
		SCRIPTFILE=`readlink -f $0`
		SCRIPTPATH=`dirname $SCRIPTFILE`
		
		if [ $SCRIPTPATH != `pwd` ]; then
			echo "Script must be directly run from within its directory using ./build.sh"
			# TODO: we might think about cd'ing into the correct directory here?!
		   exit 1
		fi
		
		# at this point we know we are in operations-gen/deployment/
		
		// git clean removed for now, as operations-gen is in git but unversioned
		//echo "[build.sh] Completely resetting git repository (sorry if you had uncommitted changes)"
		//git clean -xdf ../.. && git checkout ../.. && git pull
		
		DIME_VERSION=`head -n 1 dime.version | tr -d '\n'`
		
		if [ $DIME_VERSION == "latest" ]; then
			DIME_URL="https://ls5download.cs.tu-dortmund.de/dime/daily/dime2-latest-linux.zip"
		else
			DIME_URL="https://ls5download.cs.tu-dortmund.de/dime/${DIME_VERSION}-linux.gtk.x86_64.zip"
		fi
		
		# TODO: if "latest" but not LATEST_DIME_ALLOWED: ask for confirmation
		
		echo "[build.sh] Creating dime directory..."
		mkdir dime
		cd dime
		
		# we are now in operations-gen/deployment/dime
		
		echo "[build.sh] Downloading and unzipping DIME installer..."
		wget $DIME_URL
		unzip *zip
		
		DIMEFOLDER=`unzip -qql *zip | head -n 1 | tr -s ' ' | cut -d' ' -f5`
		cd $DIMEFOLDER
		
		# we are now in operations-gen/deployment/dime/DIME-1.20xxxxxx/
		
		echo "[build.sh] Building App headlessly..."
		./dime-headless.sh ../../../../../«project.name»
		
	'''

	def serversOptions() '''
		«FOR server : servers.servers BEFORE '[' SEPARATOR ', ' AFTER ']' »\"--«server.serverName»\"«ENDFOR»
	'''
	
	def latestDimeAllowed(Server server) {
		switch server.deploymentTier {
			case "production": {
				"false"
			}
			default: {
				"true"
			}
			
		}	
	}

}

