/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations.infrastructure

import de.jabc.cinco.meta.plugin.template.FileTemplate

class AnsibleReadmeMdTemplate extends FileTemplate {
	
	override getTargetFileName() {
		"README.md"
	}
	
	override template() '''
		# Ansible
		
		## Preparation of the control machine
		
		### Change directory
		
		```sh
		cd operations/infrastructure/ansible
		```
		### Installation of Ansible
		Have a look at http://docs.ansible.com/ansible/latest/intro_installation.html#installing-the-control-machine, but we recommend to use pip.
		
		```sh
		pip install -r requirements.txt
		```
		
		## Installation of deploy target
		
		```sh
		make {testing-install,integration-install,staging-install,demo-install,production-install}
		```
		
		## How to edit a vault
		
		```sh
		ansible-vault edit group_vars/all/vault.yml
		```	
	'''
	
}
