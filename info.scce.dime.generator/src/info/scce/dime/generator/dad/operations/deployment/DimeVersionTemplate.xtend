/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations.deployment

import de.jabc.cinco.meta.plugin.template.FileTemplate
import info.scce.dime.dad.dad.Servers

class DimeVersionTemplate extends FileTemplate {
	
	Servers servers
	
	new (Servers servers) {
		this.servers = servers
	}
	
	override getTargetFileName() {
		"dime.version"
	}
	
	override template() '''
		«IF servers.dimeVersion.nullOrEmpty»latest«ELSE»«servers.dimeVersion»«ENDIF»
		#### everything but the first line in this file is ignored ####
		
		First line defines the version of DIME this app state depends
		on. Valid values are "latest" or a path snippet relative to the dime folder from
		ls5download (https://ls5download.cs.tu-dortmund.de/dime), e.g.
		"daily/2019-09-06/DIME-1.201909060855"
	'''
}
