/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations.infrastructure

import de.jabc.cinco.meta.plugin.template.FileTemplate
import info.scce.dime.dad.dad.Servers

class MakefileTemplate extends FileTemplate {
	
	Servers servers
	
	new (Servers servers) {
		this.servers = servers
	}
	
	override getTargetFileName() {
		"Makefile"
	}
	
	override template() '''
		ANSIBLE=ansible-playbook --ask-become-pass --diff --vault-id @prompt --inventory inventories/
		«FOR it : servers.servers»

			«serverName»-install:
				$(ANSIBLE)«serverName».yml install.yml
			
			«serverName»-check-install:
				$(ANSIBLE)«serverName».yml --check install.yml
		«ENDFOR»
	'''
}
