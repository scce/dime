/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad.operations.infrastructure

import de.jabc.cinco.meta.plugin.template.FileTemplate
import info.scce.dime.dad.dad.Servers

class AllVarsYmlTemplate extends FileTemplate {
	
	Servers servers
	
	new (Servers servers) {
		this.servers = servers
	}
	
	override getTargetFileName() {
		"vars.yml"
	}
	
	override template() '''
	ansible_python_interpreter: '/usr/bin/python3'
	ansible_user: «servers.ansibleUser»
	deploy_user: «servers.deployUser»
	certbot_email: «servers.certbotEmail»
	maven_edu_password: «servers.mavenEduPassword»
	maven_edu_username: «servers.mavenEduUsername»
	dywa_http_auth_password: «servers.dywaHttpAuthPassword»
	dywa_http_auth_username: «servers.dywaHttpAuthUsername»
	dywa_database_user: sa
	dywa_database_password: sa
	client_max_body_size: 104857600
	«FOR property : servers.wildflyProperty BEFORE "wildfly_system_properties:\n"»
		«"  "»- name: "«property.name»"
		«"  "»  value: "«property.value»"
	«ENDFOR»
	'''
	
	/*
	
	  - name: "info.scce.dime.app.equinocs.server.url"
	    value: "https://{{ domain }}/"
	  - name: "info.scce.dime.app.equinocs.server.tier"
	    value: "{{ deployment_tier }}"
	  - name: "info.scce.dime.app.encrypt"
	    value: "{{ 'true' if deployment_tier == 'production' else 'false' }}"
	  - name: "info.scce.dime.app.equinocs.mail.server"
	    value: "{{ mail_server }}"
	  - name: "info.scce.dime.app.equinocs.mail.port"
	    value: "{{ mail_port }}"
	    * 
	    * 
	    */
	
}
