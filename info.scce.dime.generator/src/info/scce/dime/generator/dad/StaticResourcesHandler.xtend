/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.dad

import info.scce.dime.generator.util.DimeIOExtension
import java.net.URI
import java.nio.file.Path
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.Platform
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

@FinalFieldsConstructor
class StaticResourcesHandler {
	
	extension DimeIOExtension = new DimeIOExtension
	
	static val STATIC_RESOURCES_BUNDLE = Platform.getBundle("info.scce.dime.generator")
	
	static val STATIC_DART_RESOURCES_FOLDER = "static-dart-resources"
	static val STATIC_DOCKER_RESOURCES_FOLDER = "static-docker-resources"
	static val STATIC_JAVA_RESOURCES_FOLDER = "static-java-resources"
	
	val Path targetFolder
	
	def copyStaticDartResources() {
		val webappFolder = targetFolder.resolve("webapp")
		if (webappFolder.exists) {
			println("[INFO] Skip copying static Dart resources (target folder already exists)")
		}
		else {
			println('''[INFO] Copy static Dart resources to «targetFolder» (exists: «targetFolder.exists»)''')
			copyBundlePathContent(STATIC_DART_RESOURCES_FOLDER, targetFolder)
		}
	}
	
	def copyStaticDockerResources() {
		println('''[INFO] Copy static Docker resources to «targetFolder» (exists: «targetFolder.exists»)''')
		copyBundlePathContent(STATIC_DOCKER_RESOURCES_FOLDER, targetFolder)
	}
	
	def copyStaticJavaResources() {
		println('''[INFO] Copy static Java resources to «targetFolder» (exists: «targetFolder.exists»)''')
		copyBundlePathContent(STATIC_JAVA_RESOURCES_FOLDER, targetFolder)
	}
	
	def copyBundlePathContent(String path, Path targetFolder) {
		try {
			println('''[INFO] Copy bundle entries in «path»''')
			val pathEntries = STATIC_RESOURCES_BUNDLE.getEntryPaths(path)
			while (pathEntries.hasMoreElements) {
				val pathEntry = pathEntries.nextElement
				val fileURL = FileLocator.toFileURL(STATIC_RESOURCES_BUNDLE.getEntry(pathEntry))
				println('''[INFO]  > fileURL: «fileURL»''')
				val entryName = pathEntry.substring(path.length + 1)
				println('''[INFO]  > path entry: «entryName»''')
				val resolvedURI = new URI(fileURL.protocol, fileURL.path, null)
				println('''[INFO]  > resolvedURI: «resolvedURI»''')
			    val sourceFile = resolvedURI.toNIOPath
				println('''[INFO]  > source «IF sourceFile.isDirectory»directory«ELSE»file«ENDIF»: «sourceFile»''')
				println('''[INFO]  > targetFolder: «targetFolder»''')
				val targetEntry = targetFolder.resolve(entryName)
				println('''[INFO]  > targetEntry: «targetEntry»''')
				sourceFile.copyRecursively(targetEntry)
			}
		}
		catch (Exception e) {
			e.printStackTrace()
			throw new RuntimeException(e)
		}
	}
}
