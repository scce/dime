/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.util

import de.jabc.cinco.meta.runtime.xapi.NIOExtension
import java.io.IOException
import java.nio.file.CopyOption
import java.nio.file.OpenOption
import java.nio.file.Path
import org.eclipse.emf.common.util.URI

class DimeIOExtension extends NIOExtension {
	
	override copy(Path source, Path target, CopyOption... options) throws IOException {
		if (options.nullOrEmpty) {
			super.copy(source, target, REPLACE_EXISTING)
		}
		else {
			super.copy(source, target, options)
		}
	}
	
	def copyRecursively(URI source, Path target, CopyOption... options) throws IOException {
		this.copyRecursively(source.toNIOPath, target, options)
	}
	
	override copyRecursively(Path source, Path target, CopyOption... options) throws IOException {
		if (options.nullOrEmpty) {
			super.copyRecursively(source, target, REPLACE_EXISTING, MERGE_DIRECTORIES)
		}
		else {
			super.copyRecursively(source, target, options)
		}
	}
	
	override writeString(Path path, CharSequence csq, OpenOption... options) throws IOException {
		if (csq?.toString.nullOrEmpty) {
			super.createFile(path)
		}
		else {
			super.writeString(path, csq, options)
		}
	}
	
}