/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.process

import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.dad.dad.DAD
import info.scce.dime.data.data.UserType
import info.scce.dime.process.process.GuardContainer
import java.nio.file.Path
import java.util.List
import org.eclipse.core.runtime.IProgressMonitor

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.escapeJava

class TODOListGenerator {
	
	extension DimeIOExtension  = new DimeIOExtension
	extension TODOListGeneratorHelper = new TODOListGeneratorHelper

	def void generate(DAD dad, UserType subject, List<GuardContainer> guardContainers, Path outlet, IProgressMonitor monitor) {
		
		monitor.beginTask("Generating TODO List", 100)
		monitor.subTask("Generating TODO List Class")
		
		val projectPath = dad.projectPath
		
		val contents = dad.generate(subject, guardContainers)
		if (contents !== null) {
			try {
				projectPath
					.resolve("target", "dywa-app", "app-business", "target", "generated-sources", "info", "scce", "dime", "process")
					.createDirectories()
					.resolve('''TODOList«dad.appName.escapeJava.toFirstUpper».java''')
					.writeString(contents)
			}
			catch (Exception e) {
				throw new RuntimeException(e)
			}
		}
		
		val dartContents = guardContainers.generateDartClass()
		if (dartContents !== null) {
			try {
				projectPath
					.resolve("target", "dywa-app", "app-presentation", "target", "generated-sources", "app", "lib", "models")
					.createDirectories()
					.resolve("Todos.dart")
					.writeString(contents)
			}
			catch (Exception e) {
				throw new RuntimeException(e)
			}
		}
		
		monitor.worked(90)
		monitor.subTask("Writing Java Files")
		
	}
	
}
