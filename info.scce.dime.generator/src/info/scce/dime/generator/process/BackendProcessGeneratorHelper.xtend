/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.process

import graphmodel.Container
import graphmodel.Node
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserType
import info.scce.dime.generator.dad.DataPreprocessing
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.Write
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.process.AbstractContainsSIB
import info.scce.dime.process.process.AbstractIterateSIB
import info.scce.dime.process.process.AtomicSIB
import info.scce.dime.process.process.ComplexAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexListAttribute
import info.scce.dime.process.process.ComplexListAttributeName
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.ContainsJavaNativeSIB
import info.scce.dime.process.process.ContainsPrimitiveSIB
import info.scce.dime.process.process.ContainsSIB
import info.scce.dime.process.process.CreateSIB
import info.scce.dime.process.process.CreateUserSIB
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DeleteSIB
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.EnumSwitchSIB
import info.scce.dime.process.process.EventConnector
import info.scce.dime.process.process.EventListener
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.GenericSIB
import info.scce.dime.process.process.GetOriginalUserSIB
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IsOfTypeSIB
import info.scce.dime.process.process.IterateJavaNativeSIB
import info.scce.dime.process.process.IteratePrimitiveSIB
import info.scce.dime.process.process.IterateSIB
import info.scce.dime.process.process.LinkProcessSIB
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.PrimitiveVariable
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessBlueprintSIB
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.PutComplexToContextSIB
import info.scce.dime.process.process.PutPrimitiveToContextSIB
import info.scce.dime.process.process.PutToContextSIB
import info.scce.dime.process.process.RemoveFromListSIB
import info.scce.dime.process.process.RetrieveCurrentUserSIB
import info.scce.dime.process.process.RetrieveEnumLiteralSIB
import info.scce.dime.process.process.RetrieveOfTypeSIB
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.SearchSIB
import info.scce.dime.process.process.SetAttributeValueSIB
import info.scce.dime.process.process.SwitchToOriginalUserSIB
import info.scce.dime.process.process.SwitchToUserSIB
import info.scce.dime.process.process.TransientCreateSIB
import info.scce.dime.process.process.UnsetAttributeValueSIB
import info.scce.dime.process.process.Variable
import java.util.List
import java.util.Map
import java.util.Set

import static info.scce.dime.process.process.ProcessType.ASYNCHRONOUS

import static extension info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator.*
import static extension info.scce.dime.generator.rest.DyWAAbstractGenerator.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.*

class BackendProcessGeneratorHelper extends BackendProcessGeneratorUtil {
	
	extension GUIExtension guiextension
	
	val GenerationContext genctx
	val Map<GUISIB, Set<GUISIB>> majorToMinorGUIs = newHashMap
	
	new (GenerationContext genctx) {
		this.genctx = genctx
		this.guiextension = genctx.guiExtension
	}

	dispatch private def Iterable<GUISIB> findMinor(SIB it, Set<SIB> alreadyVisited) {
		if (alreadyVisited.contains(it)) {
			#[]
		}
		else {
			branchSuccessors
				.map[succSIB]
				.filter(SIB)
				.map [ sib |
					alreadyVisited.add(sib)
					sib.findMinor(alreadyVisited)
				]
				.flatten
		}
	}
	
	dispatch private def Iterable<GUISIB> findMinor(GUISIB sib, Set<SIB> alreadyVisited) {
		if (sib.majorPage) #[] else #[sib]
	}
	
	override caseProcess(Process it) {
		for (major: find(GUISIB).filter[isMajorPage && defaultContent !== null]) {
			val minor = major
				.branchSuccessors
				.map[succSIB]
				.filter(SIB)
				.map[findMinor(newHashSet)]
				.flatten
				.toSet
			majorToMinorGUIs.put(major, minor)
		}
		val processContextEmpty = processContextEmpty
		
		return '''
			package «process.pkg»;
			
			import java.util.HashMap;
			import java.util.List;
			import java.util.ArrayList;
			import java.util.LinkedList;
			import java.util.Arrays;
			import java.util.Collection;
			import java.util.Collections;
			import java.util.Map;
			import java.util.Set;
			import java.util.stream.Stream;
			import java.util.stream.Collectors;
			import java.util.Date;
			import java.util.UUID;
			import java.util.Optional;
			
			import javax.enterprise.inject.spi.BeanManager;
			import javax.inject.Inject;
			
			import org.apache.shiro.SecurityUtils;
			import org.apache.shiro.subject.Subject;
			
			import info.scce.dime.exception.GUIEncounteredSignal;
			import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
			import info.scce.dime.process.CallFrame;
			import info.scce.dime.process.DIMEProcess;
			import info.scce.dime.process.DIMEProcessContext;
			import info.scce.dime.process.JSONContext;
			import info.scce.dime.process.ProcessCallFrame;
			import info.scce.dime.util.CDIUtil;
			
			import com.fasterxml.jackson.annotation.JsonAutoDetect;
			
			«IF requiresDependencyInjection»@info.scce.dime.process.RequiresDI«ENDIF»
			public final class «simpleTypeName» implements DIMEProcess {
				
				«IF !processContextEmpty»
					// helper context for JSON serialization.
					@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
					public static class JSONContext extends info.scce.dime.process.JSONContext {
						«IF !majorToMinorGUIs.empty»
							private String lastMajorSIBId;
							private GUIInfo majorGUIState;
						«ENDIF»
						
						// begin context variables
						«FOR variable: variables.filter[isUsedBeforeAndAfterGUI]»
							private «variable.restTypeName» «variable.name.escapeJava»«IF variable.isIsList» = new ArrayList<>()«ENDIF»;
						«ENDFOR»
						// end context variables
						
						// begin direct dataflow variables
						«FOR ddfSource: directDataFlows.filter[passesGUISIB].uniqueSources»
							private «ddfSource.restTypeName» «ddfSource.varName»«IF ddfSource.isIsList» = new ArrayList<>()«ENDIF»;
						«ENDFOR»
						// end direct dataflow variables
						
						// begin index variables of iterate sibs
						«FOR iter: iterateSIBsInUse»
							private int «iter.iterVarName» = 0;
						«ENDFOR»
						// endindex variables of iterate sibs
						
						public static JSONContext toJSON(BeanManager bm, Context ctx) {
							final JSONContext result = new JSONContext();
							final info.scce.dime.rest.ObjectCache objectCache = new info.scce.dime.rest.ObjectCache();
							«IF !majorToMinorGUIs.isEmpty»
								result.lastMajorSIBId = ctx.lastMajorSIBId;
								result.majorGUIState = ctx.majorGUIState;
							«ENDIF»
							
							«FOR it: variables.filter[isUsedBeforeAndAfterGUI]»
								«toJsonTransformation("result." + name.escapeJava, "ctx." + name.escapeJava, if (it instanceof ComplexVariable) dataType, it instanceof ComplexVariable,(it instanceof PrimitiveVariable)&&(it as PrimitiveVariable).dataType==PrimitiveType.FILE, isList)»
							«ENDFOR»
							«FOR it: directDataFlows.filter[passesGUISIB].uniqueSources»
								«toJsonTransformation("result." + varName, "ctx." + varName, if (it instanceof ComplexOutputPort) dataType, it instanceof ComplexOutputPort,(it instanceof info.scce.dime.process.process.PrimitiveOutputPort)&&(it as info.scce.dime.process.process.PrimitiveOutputPort).dataType==PrimitiveType.FILE, isList)»
							«ENDFOR»
							«FOR iter: iterateSIBsInUse»
								result.«iter.iterVarName» = ctx.«iter.iterVarName»;
							«ENDFOR»
							
							return result;
						}
						
						@Override
						public Context toContext(BeanManager bm, ProcessCallFrame callStack) {
							final Context result = new Context();
							result.beanManager = bm;
							result.callStack = callStack;
							
							«IF !majorToMinorGUIs.isEmpty»
								result.lastMajorSIBId = this.lastMajorSIBId;
								result.majorGUIState = this.majorGUIState;
							«ENDIF»
							
							«IF !variables.filter[isUsedBeforeAndAfterGUI].filter[requiresContextTransformer].isEmpty ||
								!directDataFlows.filter[passesGUISIB].uniqueSources.filter[requiresContextTransformer].isEmpty»
								info.scce.dime.process.ContextTransformer contextTransformer = info.scce.dime.process.ContextTransformer.getInstance(bm);
							«ENDIF»
							
							«FOR it: variables.filter[isUsedBeforeAndAfterGUI]»
								«toContextTransformation(name.escapeJava, if (it instanceof ComplexVariable) dataType, it instanceof ComplexVariable,(it instanceof PrimitiveVariable)&&(it as PrimitiveVariable).dataType==PrimitiveType.FILE, isList)»
							«ENDFOR»
							«FOR it: directDataFlows.filter[passesGUISIB].uniqueSources»
								«toContextTransformation(varName, if (it instanceof ComplexOutputPort) dataType, it instanceof ComplexOutputPort,(it instanceof info.scce.dime.process.process.PrimitiveOutputPort)&&(it as info.scce.dime.process.process.PrimitiveOutputPort).dataType==PrimitiveType.FILE, isList)»
							«ENDFOR»
							«FOR iter: iterateSIBsInUse»
								result.«iter.iterVarName» = this.«iter.iterVarName»;
							«ENDFOR»
							
							return result;
						}
					}
				«ENDIF»
				
				// attributes shaping the context variables.
				public static class Context implements DIMEProcessContext {
					
					// bean manager
					private BeanManager beanManager;
					
					// current eventID
					private String eventId;
					
					// last MajorSIBId
					private String lastMajorSIBId;
					
					// current MajorGUI
					private GUIInfo majorGUIState = null;
					
					// stack variables
					private ProcessCallFrame callStack = new ProcessCallFrame();
					
					«IF !variables.empty»
						// context variables.
						«FOR variable: variables»
							private «variable.typeName» «variable.name.escapeJava»«IF variable.isIsList» = new ArrayList<>()«ELSEIF (variable instanceof PrimitiveVariable)» = «(variable as PrimitiveVariable).dataType.trgtLangDeclaration»«ENDIF»;
						«ENDFOR»
					«ENDIF»
					«IF !directDataFlows.empty»
						// direct dataflow variables.
						«FOR ddfSource: directDataFlows.uniqueSources»
							private «ddfSource.typeName» «ddfSource.varName»«IF ddfSource.isIsList» = new ArrayList<>()«ELSEIF (ddfSource instanceof PrimitiveVariable)» = «(ddfSource as PrimitiveVariable).dataType.trgtLangDeclaration»«ENDIF»;
						«ENDFOR»
					«ENDIF»
					«IF !iterateSIBsInUse.empty»
						// index variables of iterate sibs.
						«FOR iter: iterateSIBsInUse»
							private int «iter.iterVarName» = 0;
						«ENDFOR»
					«ENDIF»
					«IF !processPlaceholderSIBs.empty»
						// process placeholders.
						«FOR it: processPlaceholderSIBs»
							private «typeName» «varName»;
						«ENDFOR»
					«ENDIF»
					
					public info.scce.dime.process.JSONContext toJSON() {
						«IF processContextEmpty»
							// this process has no context variables that need to be preserved
							return null;
						«ELSE»
							return JSONContext.toJSON(beanManager, this);
						«ENDIF»
					}
				}
				
				private enum SIB_ID {
				«FOR sib: dataFlowTargets»
					«sib.id.escapeJava»,
				«ENDFOR»
				«FOR event : eventListeners»
					«event.id.escapeJava»,
				«ENDFOR»
					;
				}
				
				private final BeanManager beanManager;
				«FOR c : controllerTypesToInject»
					private final «c.controllerTypeName» «c.controllerSimpleName»;
				«ENDFOR»
				
				@Inject
				public «simpleTypeName»(final BeanManager beanManager«controllerTypesToInject.join(', ', ', ', '')['''«controllerTypeName» «controllerSimpleName»''']») {
					this.beanManager = beanManager;
					«FOR c : controllerTypesToInject»
						this.«c.controllerSimpleName» = «c.controllerSimpleName»;
					«ENDFOR»
				}
				
				private Context createContext(«processAllInputs.join(', ') [
					switch it {
						OutputPort: '''«typeName» «name.escapeJava»'''
						ProcessPlaceholderSIB: '''«typeName» «label.escapeJava»'''
						default: throw new IllegalArgumentException('''No case for "«class»".''')
					}
				]») {
					final Context ctx = new Context();
					ctx.beanManager = this.beanManager;
					
					// store inputs
					«FOR inputDataFlows: processInputsInUse.outgoingDataFlows.sortTopologically»
						«inputDataFlows.buildSetter((inputDataFlows.sourceElement as OutputPort).name.escapeJava)»
					«ENDFOR»
					«FOR it: processPlaceholderSIBs»
						ctx.«varName» = «label.escapeJava»;
					«ENDFOR»
					
					return ctx;
				}
				
				public «resultReturnTypeName» execute(boolean isAuthenticationRequired«
				processAllInputs.join(",",', ',"") [
					switch it {
						OutputPort: '''«typeName» «name.escapeJava»'''
						ProcessPlaceholderSIB: '''«typeName» «label.escapeJava»'''
						default: throw new IllegalArgumentException('''No case for "«class»".''')
					}
				]») {
					final Context ctx = createContext(«processAllInputs.join(', ') [
						switch it {
							OutputPort: '''«name.escapeJava»'''
							ProcessPlaceholderSIB: '''«label.escapeJava»'''
							default: throw new IllegalArgumentException('''No case for "«class»".''')
						}
					]»);
					ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);
					
					return executeInternal(ctx, SIB_ID.«startSIB.controlFlowSucc.id.escapeJava»);
				}
				
				«FOR entryPoint:entryPointProcessSIBs»
					public «resultReturnTypeName» executeEntry«entryPoint.id.escapeJava»(«
						entryPoint.processEntryPointAllInputs.join(', ') [
							switch it {
								InputPort: '''«typeName» «name.escapeJava»'''
								default: throw new IllegalArgumentException('''No case for "«class»".''')
							}
						]
					») {
						final Context ctx = new Context();
						ctx.beanManager = this.beanManager;
						
						// store inputs
						«FOR inputDataFlows: entryPoint.inputPorts.filter[!incoming.empty].map[incoming].flatten.filter(DataFlow).sortTopologically»
							«inputDataFlows.buildSetter((inputDataFlows.targetElement as InputPort).name.escapeJava)»
						«ENDFOR»
						
						return executeInternal(ctx, SIB_ID.«entryPoint.id.escapeJava»);
					}
				«ENDFOR»
				
				«IF !dataFlowTargets.filter[isOrContainsGUISIB].isEmpty»
					public «resultReturnTypeName» execute(ProcessCallFrame callStack«
						processAllInputs.join(' ,' , ', ', '') [
							switch it {
								OutputPort: '''«typeName» «name.escapeJava»'''
								ProcessPlaceholderSIB: '''«typeName» «label.escapeJava»'''
								default: throw new IllegalArgumentException('''No case for "«class»".''')
							}
						]
					») {
						final Context ctx = createContext(«processAllInputs.join(', ') [
							switch it {
								OutputPort: '''«name.escapeJava»'''
								ProcessPlaceholderSIB: '''«label.escapeJava»'''
								default: throw new IllegalArgumentException('''No case for "«class»".''')
							}
						]»);
						ctx.callStack = callStack;
						
						return executeInternal(ctx, SIB_ID.«startSIB.controlFlowSucc.id.escapeJava»);
					}
				«ENDIF»
				
				@Override
				public «resultReturnTypeName» continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
					«IF processContextEmpty»
						assert context == null;
						final Context ctx = new Context();
						ctx.beanManager = this.beanManager;
						ctx.callStack = callStack;
					«ELSE»
						final Context ctx = ((JSONContext) context).toContext(this.beanManager, callStack);
					«ENDIF»
					
					switch (sibId) {
						«FOR sib : dataFlowTargets.filter[it.isOrContainsGUISIB]»
							case "«sib.id.escapeString»": return executeInternal(ctx, continue«sib.id.escapeJava»(ctx, slgResult));
						«ENDFOR»
						default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
					}
				}
				
				private «resultReturnTypeName» executeInternal(final Context ctx, final SIB_ID id) {
					SIB_ID curr = id;
					while (true) {
						switch (curr) {
							«FOR sib: dataFlowTargets»
								case «sib.id.escapeJava»: {
									«IF sib instanceof EndSIB»
										return execute«sib.id.escapeJava»(ctx);
									«ELSE»
										curr = execute«sib.id.escapeJava»(ctx);
										break;
									«ENDIF»
								}
							«ENDFOR»
							«FOR event : eventListeners»
								case «event.id.escapeJava»: {
									curr = execute«event.id.escapeJava»(ctx);
									break;
								}
							«ENDFOR»
							default: {
								throw new IllegalStateException("unhandled SIB container " + curr);
							}
						}
					}
				}
				
				/**
				 * The return type for this process. It stores the corresponding branch name 
				 * as well as the corresponding result for the branch.
				 */
				«val hasWrapper = genctx.GUIProcesses.contains(it)»
				«val wrapperType = '''de.ls5.dywa.generated.rest.process.interactable.«simpleTypeName»Output'''»
				public static class «resultTypeName» implements info.scce.dime.process.DIMEProcessResult<«IF hasWrapper»«wrapperType»«ELSE»Void«ENDIF»> {
					private String branchName;
					private String branchId;
					«FOR it: endSIBs»
						private «typeName» «branchName.escapeJava»;
						
						public «process.resultTypeName»(«typeName» «branchName.escapeJava») {
							this.branchName = "«branchName.escapeString»";
							this.branchId = "«id.escapeJava»";
							this.«branchName.escapeJava» = «branchName.escapeJava»;
						}
					«ENDFOR»
					
					public String getBranchName() {
						return branchName;
					}
					
					public String getBranchId() {
						return branchId;
					}
					
					«FOR it: endSIBs»
						public «typeName» get«typeName»() {
							return «branchName.escapeJava»;
						}
					«ENDFOR»
					
					«IF hasWrapper»
						private «wrapperType» wrapper;
						
						@Override
						public «wrapperType» toJSON(info.scce.dime.rest.ObjectCache objectCache) {
							if (wrapper == null) {
								this.wrapper = new «wrapperType»(this, objectCache);
							}
							
							return wrapper;
						}
					«ENDIF»
				}
				
				// model branches.
				«FOR it: endSIBs»
					/**
					 * Interface definition for return type of branch <code>«branchName»</code>.
					 */
					public interface «typeName» {
						«FOR it: inputPorts»
							public «typeName» get«name.toFirstUpper.escapeJava»();
						«ENDFOR»
						«FOR it: inputStatics»
							public «typeName» get«name.toFirstUpper.escapeJava»();
						«ENDFOR»
					}
					
					/**
					 * Return type of branch <code>«branchName»</code> accessing the 
					 * corresponding values in the process context, instead of storing
					 * the values locally.
					 */
					static class «implTypeName» implements «typeName» {
						
						private final Context ctx;
						
						«implTypeName»(Context ctx) {
							this.ctx = ctx;
						}
						
						«FOR it: inputPorts»
							public «typeName» get«name.toFirstUpper.escapeJava»() {
								return «literalOrGetter»;
							}
						«ENDFOR»
						«FOR it: inputStatics»
							public «typeName» get«name.toFirstUpper.escapeJava»() {
								return «literalOrGetter»;
							}
						«ENDFOR»
					}
					
				«ENDFOR»
				
				«FOR subject : userTypes»
					private <T> T getConcreteUser(final «subject.typeName» subject, final Class<T> userType) {
						for (final «subject.concreteUserType.typeName» concreteUser: subject.get«subject.nameOfUserAssocAccessor»()) {
							if (userType.isAssignableFrom(concreteUser.getClass())) {
								return (T)concreteUser;
							}
						}
						throw new IllegalStateException("Did not find concrete user of type '" + userType.getSimpleName() + "'");
					}
				«ENDFOR»
				
				// sibs
				«FOR sib: dataFlowTargets»
					«doSwitch(sib)»
				«ENDFOR»
				
				«FOR event : eventListeners»
					«doSwitch(event)»
				«ENDFOR»
			}
		'''
	}

	override caseEndSIB(EndSIB it) '''
		// container for graph i/o '«branchName»'.
		public «rootElement.resultReturnTypeName» execute«id.escapeJava»(final Context ctx) {
			return new «rootElement.resultReturnTypeName»(new «implTypeName»(ctx));
		}
	'''
	
	override caseProcessSIB(ProcessSIB processSIB) {
		processSIB.caseBackendProcessSIB
	}
	
	override caseProcessPlaceholderSIB(ProcessPlaceholderSIB it) '''
		// container for placeholder '«label»' and basic sib process '«proMod.modelName»'.
		public SIB_ID execute«id.escapeJava»(final Context ctx) {
			final «proMod.externalResultReturnTypeName» result = ctx.«varName».apply(«processInputs.join(', ') [ literalOrGetter ] »);
			
			switch (result.getBranchName()) {
				«FOR branch : branches»
					case "«branch.name.escapeString»": {
						«FOR outgoingDataFlow: branch.outputsInUse.outgoingDataFlows.sortTopologically»
							«outgoingDataFlow.buildSetter('''result.get«branch.name.toFirstUpper.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper»()''')»
						«ENDFOR»
						«IF proMod.processType != ASYNCHRONOUS»
							«renderSuccessorCall(branch.name)»
						«ELSE»
							«renderSuccessorCall("success")»
						«ENDIF»
					}
				«ENDFOR»
				default: throw new IllegalStateException("SIB '«label»' has no successor defined for branch '" + result.getBranchName() + '\'');
			}
		}
	'''
	
	def getInteractionResume(GuardContainer guardContainer) {
		val subProcess = guardContainer.subProcess
		val maxCountOfUsageInput = guardContainer.guardedProcessSIB.inputs.findFirst[name == "maxUsageCount"]
		return '''
			«FOR branch: guardContainer.branches»
			// Resume execution at the output branch of an interaction SIB for interaction process `«subProcess.modelName»`.
			public «process.resultTypeName» resume«guardContainer.id.escapeJava»At«branch.name.toFirstUpper.escapeJava»(«guardContainer.interactionTypeName.processContextsTypeName» interaction«IF branch.outputPorts.size > 0», «ENDIF»«branch.outputPorts.sortBy[name].join(', ') ['''«typeName» «name.escapeJava»''']») {
				
				ctx.beanManager = this.beanManager;
				
				// retrieve the stored data context object.
				«process.contextTypeName.processContextsTypeName» context = interaction.get«PROCESS_CONTEXT_ASSOCIATION.escapeJava»();
				«val variables = process.variables»
				«IF variables.size > 0»
					// read in the context values to the data context of the process.
					«FOR variable: variables»
						ctx.«variable.name.escapeJava» = context.get«variable.name.escapeJava»();
					«ENDFOR»
				«ENDIF»
				«val directDataFlows = process.directDataFlows»
				«IF directDataFlows.size > 0»
					// read in the direct data flow variables.
					«FOR ddfSource: directDataFlows.uniqueSources»
						ctx.«ddfSource.varName» = context.get«ddfSource.varName»();
					«ENDFOR»
				«ENDIF»
				«val iterateSIBsInUse = process.iterateSIBsInUse»
				«IF iterateSIBsInUse.size > 0»
					// read in position variables of iterate sibs.
					«FOR iter: iterateSIBsInUse»
						ctx.«iter.iterVarName» = context.get«iter.iterVarName»();
					«ENDFOR»
				«ENDIF»
				
				// store inputs
				«FOR inputDataFlow: branch.outputsInUse.outgoingDataFlows.sortTopologically»
					«inputDataFlow.buildSetter((inputDataFlow.sourceElement as OutputPort).name.escapeJava)»
				«ENDFOR»
				
				final long currUsageCount = interaction.getusageCount()+1;
				interaction.setusageCount(currUsageCount);
				if («maxCountOfUsageInput.literalOrGetter» != 0l && currUsageCount >= «maxCountOfUsageInput.literalOrGetter») {
					«guardContainer.callDeleteInteraction("this", "interaction")»
				}
				
				// resume the process
				final String branch = "«branch.name»";
				final «process.simpleTypeName».AbstractSIBContainer currSIB = localIdToContainer.get("«guardContainer.id»");
				final String succId = localAdjacencyMap.get(currSIB.getId()).get(branch);
				if (succId == null) throw new IllegalStateException("Interaction SIB '" + currSIB.getDisplayName() + "' returned the branch '" + branch + "'" + ", which has no successor.");
				return executeInternal(localIdToContainer.get(succId));
			}
			«ENDFOR»
			
			«guardContainer.deleteInteraction»
			«guardContainer.checkInteraction»
		'''
	}
	
	// TODO: Broken! Unsupported?
	def longRunningGuardContainer(GuardContainer guardContainer) {
		val subProcess = guardContainer.subProcess
		val guardedProcessSIB = guardContainer.guardedProcessSIB
		val realInputs = guardedProcessSIB.inputs.filter[i | !i.name.equals("maxUsageCount")]
		return '''
			«guardContainer.interactionResume»
			
			// container for interaction SIB '«guardedProcessSIB.label» and interaction process '«subProcess.modelName»'.
			private static class SIB«guardContainer.id.escapeJava» extends AbstractSIBContainer {
				
				public String execute(final Context ctx) {
					«guardContainer.lookupInteractionController("interactionController")»
					
					// create interaction data object.
					«guardContainer.interactionTypeName.processContextsTypeName» interaction = interactionController.create«guardContainer.interactionTypeName.escapeJava»("«guardContainer.interactionTypeName»");
					interaction.setinteractId("«subProcess.id»");
					interaction.setguardContainerId("«guardContainer.id.escapeJava»");
					interaction.setusageCount(0l);
					
					«lookupContextController("contextController")»
					// create process context data object.
					«process.contextTypeName.processContextsTypeName» context = contextController.create«process.contextTypeName.escapeJava»("«process.contextTypeName»");
					«val variables = process.variables»
					«IF variables.size > 0»
						// store context variables to context data.
						«FOR variable: variables»
							context.set«variable.name.escapeJava»(ctx.«variable.name.escapeJava»);
						«ENDFOR»
					«ENDIF»
					«val directDataFlows = process.directDataFlows»
					«IF directDataFlows.size > 0»
						// direct dataflow variables.
						«FOR ddfSource: directDataFlows.uniqueSources»
							context.set«ddfSource.varName»(ctx.«ddfSource.varName»);
						«ENDFOR»
					«ENDIF»
					«val iterateSIBsInUse = process.iterateSIBsInUse»
					«IF iterateSIBsInUse.size > 0»
						// position variables of iterate sibs.
						«FOR iter: iterateSIBsInUse»
							context.set«iter.iterVarName»(ctx.«iter.iterVarName»);
						«ENDFOR»
					«ENDIF»
					// create association between interaction data and process context data object.
					interaction.set«PROCESS_CONTEXT_ASSOCIATION.escapeJava»(context);
					
					«subProcess.lookupInteractionInputsController("interInputsController")»
					// create interaction inputs data object.
					«subProcess.interactionInputsTypeNameJava.processContextsTypeName» interInputs = interInputsController.create«subProcess.interactionInputsTypeNameJava.escapeJava»("«subProcess.interactionInputsTypeNameJava»");
					// store inputs to interaction data.
					«FOR input: realInputs»
						interInputs.set«input.name.escapeJava»(«input.literalOrGetter»);
					«ENDFOR»
					// create association between interaction data and process context data object.
					interaction.set«INTERACTION_INPUTS_ASSOCIATION.escapeJava»(interInputs);
					
					// handle guard inputs
					«FOR guardProcessSIB: guardContainer.guardProcessSIBs»
						{
							«val guardProcess = guardProcessSIB.securityProcess»
							«guardProcess.lookupSecurityInputsController("securityInputsController")»
							
							«guardProcess.securityInputsTypeName.processContextsTypeName» secInputs = securityInputsController.create«guardProcess.securityInputsTypeName.escapeJava»("«guardProcess.securityInputsTypeName»");
							// store inputs to security data.
							«FOR input: guardProcessSIB.inputs»
								secInputs.set«input.name.escapeJava»(«input.literalOrGetter»);
							«ENDFOR»
							// create association between interaction data and security inputs data object.
							interaction.set«guardProcess.securityInputsTypeName.toFirstLower.escapeJava»(secInputs);
						}
					«ENDFOR»
					
					return "suspended";
				}
			}
		'''
	}
	
	def basicGuardContainer(GuardContainer guardContainer) {
		val it = guardContainer.guardedProcessSIB
		return '''
			// guard container for guarded process '«label»' and sub process '«proMod.modelName»'.
			public SIB_ID execute«guardContainer.id.escapeJava»(final Context ctx) {
				//check authentication
				final Subject shiroSubj = SecurityUtils.getSubject();
				if (!shiroSubj.isAuthenticated()) {
					final CallFrame currentFrame = new CallFrame("«process.pkg».«process.simpleTypeName»:«guardContainer.id»", ctx);
					ctx.callStack.getCallFrames().add(currentFrame);
					GUIEncounteredSignal sig = new GUIEncounteredSignal(ctx.callStack, "«guardContainer.id»");
					sig.setStatus(401);
					throw sig;
				}
				
				«proMod.buildBackendProcessBeanLookup»
				
				«IF !guardContainer.guardProcessSIBs.empty»
				//execute guards
					«FOR g:guardContainer.guardProcessSIBs»
					«{
						val userOutput = g.securityProcess.startSIB.outputPorts.findFirst[name.equals("currentUser")]
						val ConcreteType subject = (userOutput as ComplexOutputPort).dataType as ConcreteType
						val UserType userSubject = subject.userTypePredecessors.get(0)
						'''
							{
								«g.securityProcess.buildBackendProcessBeanLookup("guard")»
								«userSubject.lookupDomainController("subjectController")»
								
								final «userSubject.typeName» guardSubject = subjectController.read((Long)shiroSubj.getPrincipal());
								final «userOutput.typeName» loggedInUser = guardSubject.get«userSubject.nameOfUserAssocAccessor»().get(0);
								final «userOutput.typeName» concreteUser = loggedInUser.get«DataPreprocessing.SWITCHED_TO_ATTR»() != null ? loggedInUser.get«DataPreprocessing.SWITCHED_TO_ATTR»() : loggedInUser;
								
								final «g.securityProcess.externalResultReturnTypeName» result = instanceguard.execute(ctx.callStack.isAuthenticationRequired(),concreteUser«g.processInputs.join(', ', ', ', '') [ literalOrGetter ] »);
								if(!result.getBranchName().toLowerCase().contains("granted")) {
									«guardContainer.renderSuccessorCall("denied")»
								}
							}
						'''
					}»
					
					«ENDFOR»
				«ENDIF»
				
				«IF isOrContainsGUISIB»
					final CallFrame currentFrame = new CallFrame("«process.pkg».«process.simpleTypeName»:«guardContainer.id»", ctx);
					ctx.callStack.getCallFrames().add(currentFrame);
					ctx.callStack.setAuthenticationRequired(true);
					
					final «proMod.externalResultReturnTypeName» result = instance.execute(ctx.callStack «processInputs.join(', ', ', ', '') [ literalOrGetter ] »);
					
					return continue«guardContainer.id.escapeJava»(ctx, result);
				«ELSE»
					final «proMod.externalResultReturnTypeName» result = instance.execute(ctx.callStack.isAuthenticationRequired() «processInputs.join(', ', ', ', '') [ literalOrGetter ] »);
					
					switch (result.getBranchName()) {
					«FOR branch: guardContainer.branches»
						case "«branch.name.escapeString»": {
							«IF branch.outputPorts.size > 0»
								«FOR outgoingDataFlow: branch.outputsInUse.outgoingDataFlows.sortTopologically»
									«outgoingDataFlow.buildSetter('''result.get«branch.name.toFirstUpper.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper.escapeJava»()''')»
								«ENDFOR»
							«ENDIF»
							«guardContainer.renderSuccessorCall(branch.name)»
						}
					«ENDFOR»
						default: throw new IllegalStateException("SIB '«label»' has no successor defined for branch '" + result.getBranchName() + '\'');
					}
				«ENDIF»
			}
			
			public SIB_ID continue«guardContainer.id.escapeJava»(Context ctx, Object slgResult) {
				ctx.callStack.getCallFrames().remove(ctx.callStack.getCallFrames().size() - 1);
				
				if(slgResult == null) {
					//re-execute the guard container
					return execute«guardContainer.id.escapeJava»(ctx);
				}
				
				final «proMod.externalResultReturnTypeName» result = («proMod.externalResultReturnTypeName») slgResult;
				
				switch (result.getBranchName()) {
					«FOR branch: guardContainer.branches»
						case "«branch.name.escapeString»": {
							«IF branch.outputPorts.size > 0»
								«FOR outgoingDataFlow: branch.outputsInUse.outgoingDataFlows.sortTopologically»
									«outgoingDataFlow.buildSetter('''result.get«branch.name.toFirstUpper.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper.escapeJava»()''')»
								«ENDFOR»
							«ENDIF»
							«guardContainer.renderSuccessorCall(branch.name)»
						}
					«ENDFOR»
					default: throw new IllegalStateException("SIB '«label»' has no successor defined for branch '" + result.getBranchName() + '\'');
				}
			}
		'''
	}
	
	override caseGuardContainer(GuardContainer guardContainer) {
		if(guardContainer.rootElement.processType == ProcessType.LONG_RUNNING) {
			guardContainer.longRunningGuardContainer
		}
		else {
			guardContainer.basicGuardContainer
		}
	}
	
	override caseRetrieveCurrentUserSIB(RetrieveCurrentUserSIB userSIB) {
		val subject = userSIB.currentUser
		val successBranch = userSIB.getBranchByName("success")
		val userOutput = successBranch.outputsInUse?.head
		return '''
			// container for retrieve user sib '«userSIB.label»'.
			public SIB_ID execute«userSIB.id.escapeJava»(final Context ctx) {
				final Subject shiroSubj = SecurityUtils.getSubject();
				
				«userSIB.renderAuthenticationCheck»
				
				«IF userOutput != null»
					«subject.lookupDomainController("subjectController")»
					
					final «subject.typeName» subject = subjectController.read((Long)shiroSubj.getPrincipal());
					final «userOutput.typeName» concreteUser = getConcreteUser(subject, «userOutput.typeName».class);
					«userOutput.buildSetterForSingleOutputPort('''concreteUser.get«DataPreprocessing.SWITCHED_TO_ATTR»() != null ? concreteUser.get«DataPreprocessing.SWITCHED_TO_ATTR»() : concreteUser''')»
				«ENDIF»
				«userSIB.renderSuccessorCall("success")»
			}
		'''
	}
	
	override caseSwitchToUserSIB(SwitchToUserSIB sib) {
		val subject = sib.currentUser
		val successBranch = sib.getBranchByName("success");
		val userOutput = successBranch.outputsInUse?.head
		val outputTypeName = successBranch.outputPorts.head.singularTypeName
		return '''
			// container for switch-to-user SIB '«sib.label»'.
			public SIB_ID execute«sib.id.escapeJava»(final Context ctx) {
				final Subject shiroSubj = SecurityUtils.getSubject();
				
				«sib.renderAuthenticationCheck»
				
				«subject.lookupDomainController("subjectController")»
				
				final «subject.typeName» subject = subjectController.read((Long)shiroSubj.getPrincipal());
				final «outputTypeName» originalUser = getConcreteUser(subject, «outputTypeName».class);
				final «outputTypeName» targetUser = getConcreteUser(«sib.inputs.get(0).literalOrGetter», «outputTypeName».class);
				
				originalUser.set«DataPreprocessing.SWITCHED_TO_ATTR»(targetUser);
				
				«IF userOutput != null»
					«userOutput.buildSetterForSingleOutputPort("targetUser")»
				«ENDIF»
				«sib.renderSuccessorCall("success")»
			}
		'''
	}
	
	override caseSwitchToOriginalUserSIB(SwitchToOriginalUserSIB sib) {
		val subject = sib.currentUser
		val successBranch = sib.getBranchByName("success");
		val userOutput = successBranch.outputsInUse?.head
		val outputTypeName = successBranch.outputPorts.head.singularTypeName
		return '''
			// container for switch-to-user SIB '«sib.label»'.
			public SIB_ID execute«sib.id.escapeJava»(final Context ctx) {
				final Subject shiroSubj = SecurityUtils.getSubject();
				
				«sib.renderAuthenticationCheck»
				
				«subject.lookupDomainController("subjectController")»
				
				final «subject.typeName» subject = subjectController.read((Long)shiroSubj.getPrincipal());
				final «outputTypeName» originalUser = getConcreteUser(subject, «outputTypeName».class);
				
				originalUser.set«DataPreprocessing.SWITCHED_TO_ATTR»(null);
				
				«IF userOutput != null»
					«userOutput.buildSetterForSingleOutputPort("originalUser")»
				«ENDIF»
				«sib.renderSuccessorCall("success")»
			}
		'''
	}
	
	override caseGetOriginalUserSIB(GetOriginalUserSIB userSIB) {
		val subject = userSIB.currentUser
		val successBranch = userSIB.getBranchByName("success");
		val successOutput = successBranch.outputsInUse?.head
		val outputTypeName = successBranch.outputPorts.head.singularTypeName
		val notSwitchedOutput = userSIB.getBranchByName("not switched").outputsInUse?.head
		return '''
			// container for retrieve user sib '«userSIB.label»'.
			public SIB_ID execute«userSIB.id.escapeJava»(final Context ctx) {
				final Subject shiroSubj = SecurityUtils.getSubject();
				
				«userSIB.renderAuthenticationCheck»
				
				«subject.lookupDomainController("subjectController")»
				
				final «subject.typeName» subject = subjectController.read((Long)shiroSubj.getPrincipal());
				final «outputTypeName» concreteUser = getConcreteUser(subject, «outputTypeName».class);
				
				if (concreteUser.get«DataPreprocessing.SWITCHED_TO_ATTR»() == null) {
					«IF notSwitchedOutput != null»
						«notSwitchedOutput.buildSetterForSingleOutputPort("concreteUser")»
					«ENDIF»
					«userSIB.renderSuccessorCall("not switched")»
				} else {
					«IF successOutput != null»
						«successOutput.buildSetterForSingleOutputPort("concreteUser")»
					«ENDIF»
					«userSIB.renderSuccessorCall("success")»
				}
			}
		'''
	}

	def renderAuthenticationCheck(SIB sib) '''
		if (!shiroSubj.isAuthenticated()) {
			«IF sib.getBranchByName("not authenticated") === null»
				GUIEncounteredSignal sig = new GUIEncounteredSignal(ctx.callStack, "«sib.id»");
				sig.setStatus(401);
				throw sig;
			«ELSE»
				«sib.renderSuccessorCall("not authenticated")»
			«ENDIF»
		}
	'''
	
	def caseBackendProcessSIB(ProcessSIB processSIB) {
		if (processSIB?.proMod === null) {
			println("[DEBUG] caseBackendProcessSIB " + processSIB)
			println("[DEBUG]   proMod: " + processSIB?.proMod)
		}
		val it = processSIB
		return '''
			// container for graph abstraction '«label»' and sub process '«proMod.modelName»'.
			public SIB_ID execute«id.escapeJava»(final Context ctx) {
				«proMod.buildBackendProcessBeanLookup»
				
				«IF isOrContainsGUISIB»
					final CallFrame currentFrame = new CallFrame("«process.pkg».«process.simpleTypeName»:«id»", ctx, ctx.lastMajorSIBId);
					ctx.callStack.getCallFrames().add(currentFrame);
					
					final «proMod.externalResultReturnTypeName» result = instance.execute(ctx.callStack «processSIB.processInputs.join(', ', ', ', '') [ literalOrGetter ] »);
					
					return continue«id.escapeJava»(ctx, result);
				«ELSE»
					final «proMod.externalResultReturnTypeName» result = instance.execute(false«processSIB.processInputs.join(', ', ', ', '') [ literalOrGetter ] »);
					
					switch(result.getBranchName()) {
					«FOR branch: processSIB.branches»
						case "«branch.name.escapeString»": {
							«IF branch.outputPorts.size > 0»
								«FOR outgoingDataFlow: branch.outputsInUse.outgoingDataFlows.sortTopologically»
									«outgoingDataFlow.buildSetter('''result.get«branch.name.toFirstUpper.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper.escapeJava»()''')»
								«ENDFOR»
							«ENDIF»
							«renderSuccessorCall(branch.name)»
						}
					«ENDFOR»
						default: throw new IllegalStateException("SIB '«label»' has no successor defined for branch '" + result.getBranchName() + '\'');
					}
				«ENDIF»
			}
			
			«IF isOrContainsGUISIB»
				public SIB_ID continue«id.escapeJava»(Context ctx, Object slgResult) {
					final «proMod.externalResultReturnTypeName» result = («proMod.externalResultReturnTypeName») slgResult;
					// clean up inner execution
					ctx.callStack.getCallFrames().remove(ctx.callStack.getCallFrames().size() - 1);
					
					switch(result.getBranchName()) {
					«FOR branch: processSIB.branches»
						case "«branch.name.escapeString»": {
							«IF branch.outputPorts.size > 0»
								«FOR outgoingDataFlow: branch.outputsInUse.outgoingDataFlows.sortTopologically»
									«outgoingDataFlow.buildSetter('''result.get«branch.name.toFirstUpper.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper.escapeJava»()''')»
								«ENDFOR»
							«ENDIF»
							«renderSuccessorCall(branch.name)»
						}
					«ENDFOR»
						default: throw new IllegalStateException("SIB '«label»' has no successor defined for branch '" + result.getBranchName() + '\'');
					}
				}
			«ENDIF»
		'''
	}
	
	override caseCreateUserSIB(CreateUserSIB createSIB) {
		buildCreateSIBInternal(
			createSIB, 
			createSIB.createdType, 
			"create User SIB",
			"create"
		)
	}
	
	override caseCreateSIB(CreateSIB createSIB) {
		buildCreateSIBInternal(
			createSIB, 
			createSIB.createdType, 
			"create SIB",
			"create"
		)
	}
	
	override caseGenericSIB(GenericSIB genericSIB) {
		val referencedObject = genericSIB.referencedObject
		if (!referencedObject.isTransformable) {
			val inputPorts = genericSIB.inputs
			val nameToIndex = (0 ..< inputPorts.size).toMap[inputPorts.get(it).name]
			val resultTypeName = referencedObject.resultTypeName			
			val methodCall = '''«referencedObject.methodCallSignatur»(«genericSIB.inputs.sortBy[nameToIndex.get(name)].join(', ') [ literalOrGetter ]»)'''
			return '''
				// container for generic SIB '«genericSIB.label»'.
				public SIB_ID execute«genericSIB.id.escapeJava»(final Context ctx) {
					
					final «resultTypeName» result = «methodCall»;
					
					switch(result.getBranchName()) {
						«FOR branch: genericSIB.branches»
							case "«branch.name.escapeString»": {
								«IF branch.outputPorts.size > 0»
									«FOR outgoingDataFlow: branch.outputsInUse.outgoingDataFlows.sortTopologically»
										«outgoingDataFlow.buildSetter('''result.get«branch.name.toFirstUpper.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper.escapeJava»()''')»
									«ENDFOR»
								«ENDIF»
								«genericSIB.renderSuccessorCall(branch.name)»
							}
						«ENDFOR»
						default: throw new IllegalStateException("SIB '«genericSIB.label»' has no successor defined for branch '" + result.getBranchName() + '\'');
					}
				}
			'''
		}
	}
	
	override caseTransientCreateSIB(TransientCreateSIB createSIB) {
		buildCreateSIBInternal(
			createSIB, 
			createSIB.createdType, 
			"transient create SIB",
			"createTransient"
		)
	}
	
	private def buildCreateSIBInternal(SIB it, Type type, String sibTypeName, String createMethodName) {
		val resultPort = getBranchByName("success")?.outputPorts?.head
		return '''
			// container for «sibTypeName» '«label»'.
			public SIB_ID execute«id.escapeJava»(final Context ctx) {
				«type.lookupDomainController("domController")»
				
				final «type.typeName» createdObj = domController.«createMethodName»(«getInputByName("internalName").literalOrGetter»);
				
				«FOR it: inputs.filter[!name.equals("internalName")]»
					createdObj.set«type.getNameOfAccessor(name)»(«literalOrGetter»);
				«ENDFOR»
				
				«IF !resultPort?.outgoingDataFlows.nullOrEmpty»
					«resultPort.buildSetterForSingleOutputPort("createdObj")»
				«ENDIF»
				
				«renderSuccessorCall("success")»
			}
		'''
	}
	
	override caseDeleteSIB(DeleteSIB deleteSIB) '''
		// container for delete SIB '«deleteSIB.label»'.
		public SIB_ID execute«deleteSIB.id.escapeJava»(final Context ctx) {
			«val deleteMethod=if(deleteSIB.force)"deleteWithIncomingReferences"else"delete"»
			«FOR input: deleteSIB.complexInputsInUse»
				«val type = input.dataType»
				{
					«type.lookupDomainController("domController")»
					«switch source: input.dataEdge.sourceElement {
						ComplexVariable case source.isIsList,
						ComplexAttribute case source.isIsList: '''
							if(!«source.buildGetterNoValueSemantics(input)».isEmpty()){
								«source.buildGetterNoValueSemantics(input)».stream().forEach(instanceToDelete -> domController.«deleteMethod»(instanceToDelete));
							}	
							«source.buildGetterNoValueSemantics(input)».clear();
						'''
						ComplexListAttribute case source.attributeName == ComplexListAttributeName.FIRST: '''
							final «type.typeName» instanceToDelete = «source.parent.getter».get().remove(0);
							if (instanceToDelete != null) {
								domController.«deleteMethod»(instanceToDelete);
							}
						'''
						ComplexListAttribute case source.attributeName == ComplexListAttributeName.LAST: '''
							final «type.typeName» instanceToDelete = «source.parent.getter».get().remove(«source.parent.getter».get().size()-1);
							if (instanceToDelete != null) {
								domController.«deleteMethod»(instanceToDelete);
							}
						'''
						ComplexVariable case source.unfoldListParent != null && source.unfoldedListAttributeName == ComplexListAttributeName.FIRST: '''
							final «type.typeName» instanceToDelete = «source.unfoldListParent.getter».get().remove(0);
							if (instanceToDelete != null) {
								domController.«deleteMethod»(instanceToDelete);
							}
						'''
						ComplexVariable case source.unfoldListParent != null && source.unfoldedListAttributeName == ComplexListAttributeName.LAST: '''
							final «type.typeName» instanceToDelete = «source.unfoldListParent.getter».get().remove(«source.unfoldListParent.getter».get().size()-1);
							if (instanceToDelete != null) {
								domController.«deleteMethod»(instanceToDelete);
							}
						'''
						default: '''
							final «type.typeName» instanceToDelete = «input.literalOrGetter»;
							if (instanceToDelete != null) {
							
								// Clear attribute values in order to release all bidirectional associations from this object.
								«FOR attribute: type.complexAttributes»
									«IF attribute.isList»
										instanceToDelete.get«attribute.nameOfAccessor»().clear();
									«ELSE»
										instanceToDelete.set«attribute.nameOfAccessor»(null);
									«ENDIF»
								«ENDFOR»
								domController.«deleteMethod»(instanceToDelete);
							}
						'''
					}»
				}
			«ENDFOR»
			«deleteSIB.renderSuccessorCall("deleted")»
		}
	'''
	
	override caseRemoveFromListSIB(RemoveFromListSIB removeFromListSIB) {
		val list = removeFromListSIB.getInputByName("list")
		val element = removeFromListSIB.getInputByName("element")
		return '''
			// container for remove from list SIB '«removeFromListSIB.label»'.
			public SIB_ID execute«removeFromListSIB.id.escapeJava»(final Context ctx) {
				if («list.literalOrGetterNoValueSemantics».remove(«element.literalOrGetter»)) {
					«removeFromListSIB.renderSuccessorCall("removed")»
				}
				«removeFromListSIB.renderSuccessorCall("not found")»
			}
		'''
	}
	
	// TODO allow EL expressions in input static text? If yes, MCAM-Checks have to be implemented for this!!!
	override casePutPrimitiveToContextSIB(PutPrimitiveToContextSIB putPrimSIB) {
		val input = putPrimSIB.inputs.head
		val type = switch input {
			PrimitiveInputPort: input.dataType
			InputStatic: input.dataType
		}
		val resultPort = putPrimSIB.getBranchByName("success")?.outputPorts?.head
		return '''
			// container for SIB '«putPrimSIB.label»' putting primitive value of type '«type»' to context.
			public SIB_ID execute«putPrimSIB.id.escapeJava»(final Context ctx) {
				«IF !resultPort?.outgoingDataFlows.nullOrEmpty»
					«resultPort.buildSetterForSingleOutputPort('''«input.literalOrGetter»''')»
				«ELSE»
					// result is not used.
				«ENDIF»
				«putPrimSIB.renderSuccessorCall("success")»
			}
		'''
	}
	
	override casePutToContextSIB(PutToContextSIB putCompSIB) {
		val inputs = putCompSIB.inputs
		val outgoingDataFlows = putCompSIB.getBranchByName("success")?.outputsInUse?.outgoingDataFlows?.sortTopologically?: #[]
		return '''
			// container for SIB '«putCompSIB.label»' putting some values to context.
			public SIB_ID execute«putCompSIB.id.escapeJava»(final Context ctx) {
				«FOR outgoingDataFlow: outgoingDataFlows»
					«val resultPort = outgoingDataFlow.sourceElement as OutputPort»
					«val inputPort = inputs.findFirst[it.name == resultPort.name]»
					// put '«inputPort.name»'.
					«outgoingDataFlow.buildSetter('''«inputPort.literalOrGetter»''')»
				«ENDFOR»
				
				«putCompSIB.renderSuccessorCall("success")»
			}
		'''
	}
	
	override casePutComplexToContextSIB(PutComplexToContextSIB putCompSIB) {
		val type = putCompSIB.putType
		val inputs = putCompSIB.inputs
		val outputs = putCompSIB.getBranchByName("success")?.outputs
		val outgoingDataFlows = putCompSIB.getBranchByName("success")?.outputsInUse?.outgoingDataFlows?.sortTopologically?: #[]
		return '''
			// container for SIB '«putCompSIB.label»' putting complex value of type '«type.name»' to context.
			public SIB_ID execute«putCompSIB.id.escapeJava»(final Context ctx) {
				«FOR outgoingDataFlow: outgoingDataFlows»
					«val outputPort = outgoingDataFlow.sourceElement as OutputPort»
					«val inputPort = inputs.findFirst[it.name == outputPort.name]?: inputs.get(outputs.indexOf(outputPort))»
					// put '«inputPort.name»'.
					«outgoingDataFlow.buildSetter('''«inputPort.literalOrGetter»''')»
				«ENDFOR»
				«putCompSIB.renderSuccessorCall("success")»
			}
		'''
	}
	
	override caseSetAttributeValueSIB(SetAttributeValueSIB sib) {
		val attrName = sib.attribute.name.toLowerCase
		val typeInput = sib.getInputByName((sib.attribute.container as Type).name.toLowerCase)
		val valInput = sib.getInputByName(attrName)
		return '''
			// container for SIB '«sib.label»' setting a new attribute value.
			public SIB_ID execute«sib.id.escapeJava»(final Context ctx) {
				«typeInput.literalOrGetter».set«sib.attribute.nameOfAccessor»(«valInput.literalOrGetter»);
				«sib.renderSuccessorCall("success")»
			}
		'''
	}
	
	override caseUnsetAttributeValueSIB(UnsetAttributeValueSIB sib) {
		val typeInput = sib.getInputByName((sib.attribute.container as Type).name.toLowerCase)
		return '''
			// container for SIB '«sib.label»' unsetting an attribute value.
			public SIB_ID execute«sib.id.escapeJava»(final Context ctx) {
				«typeInput.literalOrGetter».set«sib.attribute.nameOfAccessor»(«sib.attribute.nullDeclaration»);
				«sib.renderSuccessorCall("success")»
			}
		'''
	}
	
	override caseRetrieveOfTypeSIB(RetrieveOfTypeSIB it) {
		val type = retrievedType
		val resultPort = getBranchByName("success")?.outputPorts?.head
		val resultList = "result"
		return '''
			// container for retrieve all of type (matching the given constraints) SIB '«label»'.
			public SIB_ID execute«id.escapeJava»(final Context ctx) {
				«IF type instanceof EnumType»
					final List<«type.typeName»> result = Arrays.asList(«type.typeName».values());
				«ELSE»
					«type.lookupDomainController("domController")»
					
					// search for all objects of type matching the given contraints.
					final «type.typeName» searchObject = domController.createSearchObject("");
					«FOR it: inputs»
						searchObject.set«type.getNameOfAccessor(name)»(«literalOrGetter»);
					«ENDFOR»
					«IF resultPort.isList»
						final List<«type.typeName»> result = domController.findByProperties(searchObject);
					«ELSE»
						final «type.typeName» result = domController.findFirstByProperties(searchObject);
					«ENDIF»
				«ENDIF»
				«IF !resultPort?.outgoingDataFlows.nullOrEmpty»
					«resultPort.buildSetterForSingleOutputPort(resultList)»
				«ENDIF»
				«IF resultPort.isList»
					if (result.isEmpty()) {
				«ELSE»
					if (result == null) {
				«ENDIF»
					«renderSuccessorCall("none found")»
				}
				else {
					«renderSuccessorCall("success")»
				}
			}
		'''
	}
	
	override caseRetrieveEnumLiteralSIB(RetrieveEnumLiteralSIB enumLitSIB) {
		val enumLiteral = enumLitSIB.retrievedLiteral
		val type = enumLiteral.container as EnumType
		val resultPort = enumLitSIB.getBranchByName(enumLiteral.name)?.outputPorts.head
		return '''
			// container for retrieve enum literal SIB '«enumLitSIB.label»'.
			public SIB_ID execute«enumLitSIB.id.escapeJava»(final Context ctx) {
				«IF !resultPort?.outgoingDataFlows.nullOrEmpty»
					«resultPort.buildSetterForSingleOutputPort('''«type.typeName».«enumLiteral.name.escapeJava»''')»
				«ENDIF»
				«enumLitSIB.renderSuccessorCall(enumLiteral.name)»
			}
		'''
	}
	
	override caseEnumSwitchSIB(EnumSwitchSIB enumSwitchSIB) {
		// TODO currently we do not support enums as static inputs. Need to handle this as soon as it is available.
		val input = enumSwitchSIB.inputPorts.head
		return '''
			// container for switch enum SIB '«enumSwitchSIB.label»'.
			public SIB_ID execute«enumSwitchSIB.id.escapeJava»(final Context ctx) {
				«IF input.dataEdge != null»
				if(«input.literalOrGetter» == null) {
					«IF enumSwitchSIB.branchSuccessors.exists[name=="else"]»
						«enumSwitchSIB.renderSuccessorCall("else")»
					«ELSE»
						throw new IllegalStateException("Modeling mistake! No enum input set.");
					«ENDIF»	
				}
				
				switch(«input.literalOrGetter».toString()){
					«FOR branch : enumSwitchSIB.branchSuccessors.filter[name != "else"]»
						case "«branch.name.escapeJava»": «enumSwitchSIB.renderSuccessorCall(branch.name)»
					«ENDFOR»
					«IF enumSwitchSIB.branchSuccessors.exists[name=="else"]»
						default : «enumSwitchSIB.renderSuccessorCall("else")»
					«ENDIF»	
				}
				«ENDIF»
				«IF !enumSwitchSIB.branchSuccessors.exists[name=="else"]»
						throw new IllegalStateException("Modeling mistake! No enum input set.");
				«ENDIF»	
			}
		'''
	}
	
	override caseProcessBlueprintSIB(ProcessBlueprintSIB blueSIB) {
		val firstBranch = blueSIB.defaultBranch?.name ?: (blueSIB.branches.head?.name ?: "success")
		return '''
			// container for blueprint SIB '«blueSIB.label»'.
			public SIB_ID execute«blueSIB.id.escapeJava»(final Context ctx) {
				«blueSIB.renderSuccessorCall(firstBranch)»
			}
		'''
	}
	
	override caseIsOfTypeSIB(IsOfTypeSIB it) {
		val input = complexInputPorts.head
		val resultPort = getBranchByName("yes")?.outputPorts.head
		return '''
			// container for is of type SIB '«label»'.
			public SIB_ID execute«id.escapeJava»(final Context ctx) {
				«IF !input.isList»
					final «input.typeName» instance = «input.literalOrGetter»;
					if(instance instanceof «checkedType.typeName») {
						«IF !resultPort?.outgoingDataFlows.nullOrEmpty»
							«resultPort.buildSetterForSingleOutputPort('''(«checkedType.typeName»)instance''')»
						«ENDIF»
						«renderSuccessorCall("yes")»
					}
					«renderSuccessorCall("no")»
				«ELSE»
					List<«checkedType.typeName»> result = «input.literalOrGetter».stream()
						.filter((e) -> e instanceof «checkedType.typeName»)
						.map((e) -> («checkedType.typeName»)e).collect(Collectors.toList());
					
					if(result.size() == «input.literalOrGetter».size()) {
						«IF !resultPort?.outgoingDataFlows.nullOrEmpty»
							«resultPort.buildSetterForSingleOutputPortNoValueSemantics('''result''')»
						«ENDIF»
						«renderSuccessorCall("yes")»
					}
					else {
						«renderSuccessorCall("no")»
					}
				«ENDIF»
			}
		'''
	}
	
	override caseContainsSIB(ContainsSIB containsSIB) {
		buildContainsSIB(containsSIB, 
			containsSIB.getInputByName("list"), 
			containsSIB.getInputByName("element"), 
			"complex"
		)
	}
	
	override caseContainsJavaNativeSIB(ContainsJavaNativeSIB containsSIB) {
		buildContainsSIB(containsSIB,
			containsSIB.getInputByName("element"),
			containsSIB.getInputByName("element"),
			"java native"
		)
	}
	
	override caseContainsPrimitiveSIB(ContainsPrimitiveSIB containsSIB) {
		buildContainsSIB(containsSIB,
			containsSIB.getInputByName("element"),
			containsSIB.getInputByName("element"),
			"primitive"
		)
	}
	
	private def buildContainsSIB(AbstractContainsSIB it, Input listInput, Input elementInput, String variant) '''
		// container for contains «variant» SIB '«label»'.
		public SIB_ID execute«id.escapeJava»(final Context ctx) {
			«IF listInput !== null»
				if («listInput.literalOrGetter».contains(«elementInput.literalOrGetter»)) {
					«renderSuccessorCall("yes")»
				}
				else {
					«renderSuccessorCall("no")»
				}
			«ELSE»
				// no iterable set.
				«renderSuccessorCall("no")»
			«ENDIF»
			
		}
	'''
	
	override caseIterateSIB(IterateSIB iterateSIB) {
		val input = iterateSIB.complexInputsInUse.head
		val resultPort = iterateSIB.getBranchByName("next").outputsInUse.head
		iterateSIB.buildIterateSIB(input, resultPort, "complex")
	}
	
	override caseIteratePrimitiveSIB(IteratePrimitiveSIB iterateSIB) {
		val input = iterateSIB.primitiveInputsInUse.head
		val resultPort = iterateSIB.getBranchByName("next").outputsInUse.head
		iterateSIB.buildIterateSIB(input, resultPort, "primitive")
	}
	
	override caseIterateJavaNativeSIB(IterateJavaNativeSIB iterateSIB) {
		val input = iterateSIB.javaNativeInputsInUse.head
		val resultPort = iterateSIB.getBranchByName("next").outputsInUse.head
		iterateSIB.buildIterateSIB(input, resultPort, "java native")
	}
	
	private def buildIterateSIB(AbstractIterateSIB iterateSIB, InputPort input, OutputPort resultPort, String variant) '''
		// container for iterate «variant» SIB '«iterateSIB.label»'.
		public SIB_ID execute«iterateSIB.id.escapeJava»(final Context ctx) {
			«IF input !== null»
				final int pos = ctx.«iterateSIB.iterVarName»++;
				«input.typeName» list = «input.literalOrGetter»;
				if(list != null && pos < list.size()) {
					«IF resultPort !== null»«resultPort.buildSetterForSingleOutputPort('''«input.literalOrGetter».get(pos)''')»«ELSE»«input.literalOrGetter».get(pos);«ENDIF»
					«iterateSIB.renderSuccessorCall("next")»
				}
				else {
					ctx.«iterateSIB.iterVarName» = 0;
					«iterateSIB.renderSuccessorCall("exit")»
				}
			«ELSE»
				// no iterable set.
				«iterateSIB.renderSuccessorCall("exit")»
			«ENDIF»
			
		}
	'''
	
	override caseAtomicSIB(AtomicSIB atomicSIB) {
		val sibComp = (atomicSIB.sib as info.scce.dime.siblibrary.SIB)
		val inputPorts = sibComp.inputPorts
		val isBoolSIB = sibComp.branches.exists[name == "true"] && sibComp.branches.flatMap[outputPorts].isEmpty
		val successBranch = sibComp.branches.findFirst[name == "success"]
		val isSuccessSIB = successBranch !== null && successBranch.outputPorts.size <= 1
		val nameToIndex = (0 ..< inputPorts.size).toMap[ index | inputPorts.get(index).name ]
		val methodCall = '''«sibComp.executorClass».«sibComp.executorMethod»(«atomicSIB.inputs.sortBy[nameToIndex.get(name)].join(", ") [literalOrGetter]»)'''
		return '''
			// container for atomic SIB '«atomicSIB.label»'.
			public SIB_ID execute«atomicSIB.id.escapeJava»(final Context ctx) {
				try {
					«IF isBoolSIB»
						if («methodCall») {
							«atomicSIB.renderSuccessorCall("true")»
						}
						else {
							«atomicSIB.renderSuccessorCall("false")»
						}
					«ELSEIF isSuccessSIB»
						«val resultPort = atomicSIB.getBranchByName("success")?.outputPorts?.head»
						«IF resultPort?.outgoingDataFlows.nullOrEmpty»
							«methodCall»;
						«ELSE»
							final «resultPort.typeName» result = «methodCall»;
							«resultPort.buildSetterForSingleOutputPort("result")»
						«ENDIF»
						«atomicSIB.renderSuccessorCall("success")»
					«ELSE»
						final java.util.Map<String,?> result = «methodCall»;
						«FOR branch : atomicSIB.branches»
							if (result.containsKey("«branch.name»") || result.keySet().stream().filter(k -> k.startsWith("«branch.name».")).findAny().isPresent()) {
								«IF branch.outputPorts.size == 1»
									«val outputPort = branch.outputPorts.head»
									«IF !outputPort?.outgoingDataFlows.nullOrEmpty»
										final «outputPort.typeName» value = («outputPort.typeName») result.get("«branch.name»");
										«outputPort.buildSetterForSingleOutputPort("value")»
									«ENDIF»
								«ELSEIF branch.outputPorts.size > 1»
									«FOR i : 0 ..< branch.outputPorts.size»
										«val outputPort = branch.outputPorts.get(i)»
										«IF !outputPort?.outgoingDataFlows.nullOrEmpty»
											final «outputPort.typeName» value«i» = («outputPort.typeName») result.get("«branch.name».«outputPort.name»");
											«outputPort.buildSetterForSingleOutputPort('''value«i»''')»
										«ENDIF»
									«ENDFOR»
								«ENDIF»
								«atomicSIB.renderSuccessorCall(branch.name)»
							}
						«ENDFOR»
						throw new IllegalStateException("SIB '«atomicSIB.label»' has no branch defined");
					«ENDIF»
				}
				catch (Exception e) {
					e.printStackTrace();
					«atomicSIB.renderSuccessorCall("failure")»
				}
			}
		'''
	}
	
	override caseSearchSIB(SearchSIB searchSIB) {
		// TODO
	}
	
	// Example: InputsForGUISIB_abcdef1234567890
	private def renderInputClassName(Node node) {
		'''InputsFor«node.eClass.name»«node.id.escapeJava»'''
	}
	
	private def renderInputClass(SIB sib) '''
		// Input class -- generated by «class.name»#renderInputClass(SIB)
		//   for SIB «sib.label» «sib.id»
		private static class «sib.renderInputClassName» {
			«renderInputClassBody(sib.inputs)»
		}
	'''
	
	private def renderInputClass(LinkProcessSIB sib) '''
		// Input class -- generated by «class.name»#renderInputClass(SIB)
		//   for Link Process SIB «sib.label» «sib.id»
		private static class «sib.renderInputClassName» {
			«renderLinkInputClassBody(sib.inputs)»
		}
	'''
	
	private def renderInputClass(EventListener event) '''
		«val sib = event.findSourceOf(EventConnector) as SIB»
		// Input class -- generated by «class.name»#renderInputClass
		//   for Event «event.name» «event.id»
		//     of SIB «sib?.label» «sib?.id»
		private static class «event.renderInputClassName» {
			«renderInputClassBody(event.inputs)»
		}
	'''
	
	private def renderInputClassBody(List<Input> inputs) '''
		«FOR ip : inputs»
			«IF ip instanceof ComplexInputPort»
				public «(typeViewForInput(ip) as ComplexTypeView).generateBasicTOName(DyWAExtension.dywaPkg)» «ip.inputFieldName»«IF ip.isList» = new ArrayList<>()«ENDIF»;
			«ELSE»
				public «IF ip.isList»List<«ENDIF»«ip.primitiveType.getRestLiteral»«IF ip.isList»>«ENDIF» «ip.inputFieldName»«IF ip.isList» = new ArrayList<>()«ENDIF»;
			«ENDIF»
		«ENDFOR»
	'''
	
	private def renderLinkInputClassBody(List<Input> inputs) '''
		«FOR ip : inputs»
			«IF ip instanceof ComplexInputPort»
				public «IF ip.isList»List<«ENDIF»Long«IF ip.isList»>«ENDIF» «ip.inputFieldName»«IF ip.isList» = new ArrayList<>()«ENDIF»;
			«ELSE»
				public «IF ip.isList»List<«ENDIF»«ip.primitiveType.getRestLiteral»«IF ip.isList»>«ENDIF» «ip.inputFieldName»«IF ip.isList» = new ArrayList<>()«ENDIF»;
			«ENDIF»
		«ENDFOR»
	'''
	
	private def handleInputPorts(List<Input> it, CharSequence typeName, CharSequence variableName, boolean parseToPrimitive) {
		handleInputPorts(typeName, variableName, parseToPrimitive, true)
	}

	private def handleInputPorts(List<Input> it, CharSequence inputTypeName, CharSequence variableName, boolean parseToPrimitive, boolean createObjectCache)'''
		«IF !parseToPrimitive»
			«IF createObjectCache»final info.scce.dime.rest.ObjectCache objectCache = info.scce.dime.util.CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.rest.ObjectCache.class);«ENDIF»
			«FOR it : filter(ComplexInputPort).filter[!incoming.empty].map[dataType].toSet»
			final «RESTControllerName» «variableName»«RESTControllerSimpleName» = info.scce.dime.util.CDIUtil.getManagedInstance(ctx.beanManager, «RESTControllerName».class);
			«ENDFOR»
		«ENDIF»
		
		final «inputTypeName» «variableName» = new «inputTypeName»();
		
		«FOR ip : it»
			«IF parseToPrimitive»
				«variableName».«ip.inputKeyName» = «ip.literalOrGetter»«ip.toPrimitive»;
			«ELSEIF ip instanceof ComplexInputPort»
				«IF !ip.incoming.empty»
					«val view = typeViewForInput(ip) as ComplexTypeView»
					«val rep = genctx.selectiveCache.getRepresentative(view)»
					{
						«IF ip.isList»
							//«ip.name»
							«view.generateBasicTOName(DyWAExtension.dywaPkg)» result = new java.util.ArrayList<>(«ip.literalOrGetter».size());
							for («view.generateInnerDywaTOName(DyWAExtension.dywaPkg)» obj : «ip.literalOrGetter») {
						«ELSE»
							«view.generateBasicTOName(DyWAExtension.dywaPkg)» result = null;
							«view.generateInnerDywaTOName(DyWAExtension.dywaPkg)» obj = «ip.literalOrGetter»;
						«ENDIF»
						
						«view.generateInnerBasicTOName(DyWAExtension.dywaPkg)» restTO;
						
						if (obj != null) {
							if (obj.getDywaId() > info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
								// read_«DyWASelectiveDartGenerator.getSelectiveNameJava(view)»
								restTO = «variableName»«ip.dataType.RESTControllerSimpleName».read_«DyWASelectiveDartGenerator.getSelectiveNameJava(rep)»(obj.getDywaId());
							}
							else {
								restTO = objectCache.getRestTo(obj);
								if (restTO == null) {
									restTO = «view.type.RESTTOName».fromDywaEntity(obj, objectCache);
								}
								// «DyWASelectiveDartGenerator.getSelectiveNameJava(view)»
								«DyWAExtension.restTypePkg».«rep.selectiveNameJava».copy(obj, restTO, objectCache);
							}
							«IF ip.isList»
								result.add(restTO);
							«ELSE»
								result = restTO;
							«ENDIF»
						}
						«IF ip.isList»
							}
						«ENDIF»
						«variableName».«ip.inputFieldName» = result;
					}
				«ENDIF»
			«ELSE»
				«val isFile = ip instanceof PrimitiveInputPort && (ip as PrimitiveInputPort).dataType == PrimitiveType.FILE»
				«IF isFile»
					«IF ip.isList»
						«variableName».«ip.inputFieldName» = «ip.literalOrGetter».stream().map(«DyWAExtension.restUtilPkg».FileReference::new).collect(java.util.stream.Collectors.toList());
					«ELSE»
						«variableName».«ip.inputFieldName» = new «DyWAExtension.restUtilPkg».FileReference(«ip.literalOrGetter»);
					«ENDIF»
				«ELSE»
					«variableName».«ip.inputFieldName» = «ip.literalOrGetter»;
				«ENDIF»
			«ENDIF»
		«ENDFOR»
	'''
	
	def typeViewForInput(ComplexInputPort port) {
		val gui = port.guiSIB.gui
		val guiCV = genctx.getCompoundView(gui)
		if (port.container instanceof GUISIB) {
			val tv = guiCV.compounds.findFirst[it.name.equals(port.name)]
			if (tv === null) {
				System.err.println("[ERROR] TypeView not found for InputPort '" + port.name + "' of GUISIB '" + (port.container as GUISIB).name + "' in " + port.rootElement.modelName)
			}
			return tv
		}
		if (port.container instanceof EventListener) {
			val eventName = (port.container as EventListener).name
			val event = gui.listenerContexts.map[events].flatten.findFirst[it.name.equals(eventName)]
			val outputPort = event.outputPorts.findFirst[it.name.equals(port.name)]
			if(outputPort.outgoing.empty) {
				// create base Type view
				if (outputPort instanceof info.scce.dime.gui.gui.ComplexOutputPort) {
					val tv =  new BaseComplexTypeView(outputPort.dataType)
					tv.data = outputPort
					tv.name = outputPort.name
					tv.GModel = gui
					tv.list = outputPort.isList
					return tv
				}
				if (outputPort instanceof PrimitiveOutputPort) {
					val tv =  new PrimitiveTypeView()
					tv.primitiveType = outputPort.dataType.toData
					tv.data = outputPort
					tv.name = outputPort.name
					tv.GModel = gui
					tv.list = outputPort.isList
					return tv
				}
				throw new IllegalStateException("Eventlistener port type view not created")
			}
			else {
				// port is connected to variable
				val data = outputPort.getOutgoing(Write).head.targetElement
				val tv = genctx.getCompoundView(gui).pairs.get(data)
				if (tv === null) {
					throw new IllegalStateException("Eventlistener port type view not found")
				}
				return tv
			}
		}
		
	}
	
	def GUISIB getGuiSIB(ComplexInputPort port) {
		switch (con: port.container) {
			GUISIB:        con
			EventListener: con.getIncoming(EventConnector).head.sourceElement
			default:       throw new IllegalStateException("EventListener source not found")
		}
	}
	
	def getToPrimitive(Input input) {
		switch (it: input) {
			ComplexInputPort case isList: ".stream().map((n)->n.getDywaId()).collect(java.util.stream.Collectors.toList())"
			ComplexInputPort:             ".getDywaId()"
			default:                      ""
		}
	}
	
	override caseGUISIB(GUISIB guiSIB) {
		val it = guiSIB
		val complexInputs = inputs.filter(ComplexInputPort).exists[!incoming.empty]
		return '''
			«renderInputClass(guiSIB)»
			
			«FOR event : guiSIB.getSuccessors(EventListener)»
				«renderInputClass(event)»
			«ENDFOR»
			
			// container for GUI SIB '«label»'.
			public SIB_ID execute«id.escapeJava»(final Context ctx) {
				
				«inputs.handleInputPorts(renderInputClassName(guiSIB), "inputs", false, complexInputs)»
				
				«IF defaultContent !== null»
					ctx.lastMajorSIBId="«process.pkg».«process.simpleTypeName»:«id»:«gui.id»";
				«ELSE»
					final CallFrame currentFrame = new CallFrame("«process.pkg».«process.simpleTypeName»:«id»:«gui.id»", ctx, ctx.lastMajorSIBId);
					
					ctx.callStack.getCallFrames().add(currentFrame);
				«ENDIF»
				
				GUIEncounteredSignal signal = new GUIEncounteredSignal(ctx.callStack, "«id»", inputs);
				
				«FOR event: it.eventListenerSuccessors SEPARATOR 'else '»
					if("«event.id»".equals(ctx.eventId)){
						ctx.eventId = null;
						«event.inputs.handleInputPorts(renderInputClassName(event), "eventInputs", false, !complexInputs)»
						signal =  new GUIEncounteredSignal(ctx.callStack, "«id»", inputs, "«event.name»", eventInputs); 
					}
				«ENDFOR»
				«IF defaultContent !== null»
					// put MajorGUI state to context
					ctx.majorGUIState = signal.getGuiResult().getMajor_sib();
					«renderSuccessorCall(defaultContent.majorBranch.name)»
				«ELSEIF !isMajor»
					«FOR pred : majorToMinorGUIs.filter[p,predecessors|predecessors.contains(it)].keySet SEPARATOR 'else '»
						«IF !pred.inputs.empty»
						if(ctx.lastMajorSIBId != null &&"«pred.id»".equals(ctx.lastMajorSIBId.split(":")[1])){
							«pred.inputs.handleInputPorts(renderInputClassName(pred), "majorInputs_"+pred.id.escapeJava, false, !complexInputs)»
							//update major input with current inputs
							ctx.majorGUIState.updateInputs(majorInputs_«pred.id.escapeJava»);
						}
						«ENDIF»
					«ENDFOR»
					
					//include magic from MajorGUI (from context)
					signal = new GUIEncounteredSignal(ctx.majorGUIState, signal);
					throw signal;
				«ELSE»
					throw signal;
				«ENDIF»
			}
			
			«val processBranches = guiSIB.gui.getProcessBranches»
			«val guiBranches = gui.getGUIBranches(false).filter[hasGUIOrigin]»
			public SIB_ID continue«id.escapeJava»(Context ctx, Object guiResult) {
				if(ctx.callStack.isAuthenticationRequired()) {
					final Subject shiroSubj = SecurityUtils.getSubject();
					if (!shiroSubj.isAuthenticated()) {
						GUIEncounteredSignal sig = new GUIEncounteredSignal(ctx.callStack, "«id»");
						sig.setStatus(401);
						throw sig;
					}
				}
				
				ctx.callStack.getCallFrames().remove(ctx.callStack.getCallFrames().size()-1);
				
				«val connectedBranches = guiSIB.branches.filter[!outgoing.empty]»
				«IF connectedBranches.isEmpty»
					// unspecified branch, show same GUI again
					return execute«id.escapeJava»(ctx);
				«ELSE»
					final «gui.externalResultTypeName» result = («gui.externalResultTypeName») guiResult;
					«FOR branch: connectedBranches SEPARATOR " else "»
						«val foundProcessBranches = processBranches.filter[name.equals(branch.name)].toMap([element])»
						«val foundGUIBranches = guiBranches.filter[name.equals(branch.name)].toMap([element])»
						if ("«branch.name.escapeJava»".equals(result.getBranchName())) {
							«IF branch.outputPorts.size > 0»
								«FOR processBranch:foundProcessBranches.values»
									«val branchName = '''process_«processBranch.element.id»«branch.name»'''»
									if(result.get«branchName.escapeJava»Return() != null) {
									«FOR outgoingDataFlow: branch.outputsInUse.filter[isPresentIn(processBranch)].outgoingDataFlows.sortTopologically»
										«outgoingDataFlow.buildSetter('''result.get«branchName.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper.escapeJava»()''')»
									«ENDFOR»
									}
								«ENDFOR»
								«FOR guiBranch : foundGUIBranches.values»
									«val branchName = '''gui_«guiBranch.element.id»«branch.name»'''»
									if(result.get«branchName.escapeJava»Return() != null) {
									«FOR outgoingDataFlow: branch.outputsInUse.filter[isPresentIn(guiBranch)].outgoingDataFlows.sortTopologically»
										«outgoingDataFlow.buildSetter('''«outgoingDataFlow.castIfNeccessary(guiBranch)»result.get«branchName.escapeJava»Return().get«(outgoingDataFlow.sourceElement as OutputPort).name.toFirstUpper.escapeJava»()''')»
									«ENDFOR»
									}
								«ENDFOR»
							«ENDIF»
							«renderSuccessorCall(branch.name)»
						}
					«ENDFOR»
					else {
						// unspecified branch, show same GUI again
						return execute«id.escapeJava»(ctx);
					}
				«ENDIF»
			}
		'''
	}
	
	def castIfNeccessary(DataFlow flow, GUIBranch branch) {
		switch (it: flow.sourceElement) {
			ComplexOutputPort case isList: '''(List<«dataType.dyWATypeName»>)'''
			ComplexOutputPort:             '''(«dataType.dyWATypeName»)'''
			default:                       ""
		}
	}
	
	def isPresentIn(OutputPort port, GUIBranch branch) {
		branch.ports.exists[name == port.name]
	}
	
	override caseLinkProcessSIB(LinkProcessSIB it) '''
		«renderInputClass»

		// container for Link SIB '«label»'.
		public SIB_ID execute«id.escapeJava»(final Context ctx) {

			«inputs.handleInputPorts(renderInputClassName, "inputs",true)»

			throw new GUIEncounteredSignal(new ProcessCallFrame(), "«id»", inputs);
		}
	'''
		
	override caseEventListener(EventListener it) '''
		// container for EventListener SIB '«name»'.
		public SIB_ID execute«id.escapeJava»(final Context ctx) {
			ctx.eventId = "«id»";
			«renderSuccessorCall("default")»
		}
	'''
	
	def <T extends Container> toJsonTransformation(CharSequence toName, CharSequence fromName, Type t, boolean isComplex, boolean isFile, boolean isList) '''
		«IF isComplex»
			«IF isList»
				«toName» = new java.util.ArrayList<>(«fromName».size());
				
				for («t.dyWATypeName» o : «fromName».stream().filter((n)->n!=null).collect(java.util.stream.Collectors.toList())) {
					final «t.RESTTOName» trans = «t.RESTTOName».fromDywaEntity(o, objectCache);
					«t.RESTTOName»Selective.copy(o, trans, objectCache);
					«toName».add(trans);
				}
			«ELSE»
				if(«fromName» != null) {
					«toName» = «t.RESTTOName».fromDywaEntity(«fromName», objectCache);
					«t.RESTTOName»Selective.copy(«fromName», «toName», objectCache);
				}
			«ENDIF»
		«ELSE»
			«IF isFile»
				«IF isList»
					«toName» = «fromName».stream().filter((n)->n!=null).map((n)->new de.ls5.dywa.generated.rest.util.FileReference(n)).collect(java.util.stream.Collectors.toList());
				«ELSE»
					if(«fromName» != null) {
						«toName» = new de.ls5.dywa.generated.rest.util.FileReference(«fromName»);
					}
				«ENDIF»
			«ELSE»
			«toName» = «fromName»;
			«ENDIF»
		«ENDIF»
	'''

	def <T extends Container> toContextTransformation(CharSequence name, Type t, boolean isComplex, boolean isFile, boolean isList) '''
		«IF isComplex»
			«IF isList»
				result.«name» = this.«name».stream().filter(o -> o != null).map(o -> contextTransformer.transform(o)).collect(Collectors.toList());
			«ELSE»
				if (this.«name» != null) {
					result.«name» = contextTransformer.transform(this.«name»);
				}
			«ENDIF»
		«ELSEIF isFile»
			«IF isList»
				result.«name» = this.«name».stream().filter((n)->n!=null).map((n)->contextTransformer.transformFile(n.getDywaId())).collect(java.util.stream.Collectors.toList());
			«ELSE»
				if (this.«name» != null) {
					result.«name» = contextTransformer.transformFile(this.«name».getDywaId());
				}
			«ENDIF»
		«ELSE»
			result.«name» = this.«name»;
		«ENDIF»
	'''
	
	def <T extends Container> generateJSONToContextTransformer() '''
		// generated by «class.name»#generateJSONToContextTransformer
		package info.scce.dime.process;
		
		import java.util.List;
		import java.util.stream.Collectors;
		import javax.inject.Inject;
		
		/**
		 * Utility class to explicitly declare the required controllers that need to be injected.
		 * This allows to resolve all injection points once at deploy time instead dynamically on each invocation, thus improving performance.
		 */
		public final class ContextTransformer {
		
		private static ContextTransformer instance;
		
		public static ContextTransformer getInstance(javax.enterprise.inject.spi.BeanManager bm) {
			if (instance == null) {
				instance = info.scce.dime.util.CDIUtil.getManagedInstance(bm, ContextTransformer.class);
			}
			return instance;
		}
		
		«val types = genctx.usedDatas.flatMap[types].map[originalType].toSet»
		
		«FOR t : types»
			@Inject «t.controllerTypeName» «t.getControllerSimpleName»;
			@Inject «t.getRESTControllerName» «t.getRESTControllerSimpleName»;
		«ENDFOR»
		
		@Inject info.scce.dime.util.DomainFileController domainFileController;
		
		public info.scce.dime.util.FileReference transformFile(long dywaId) {
			return domainFileController.getFileReference(dywaId);
		}
		
		«FOR t : types»
			public «t.dyWATypeName» transform(«t.RESTTOName» o) {
				final «t.dyWATypeName» obj;
				«t.renderTOLookup("o", "obj")»
				return obj;
			}
		«ENDFOR»
		}
	'''
	
	private def renderTOLookup(Type type, CharSequence source, CharSequence target) '''
		if («source».getDywaId() > 0) {
			«target» = «type.controllerSimpleName».read(«source».getDywaId());
		}
		«IF type instanceof EnumType»
			else {
				throw new java.lang.IllegalArgumentException("Transient enum types are not allowed");
			}
		«ELSE»
			«FOR t : type.knownSubTypes.filter[!isAbstract].sortTopologically
				BEFORE " else "
				SEPARATOR " else "
				AFTER ''' else { throw new java.lang.IllegalArgumentException("Unknown type"); } '''»
				if («source» instanceof «t.RESTTOName») {
					«target» = «t.getRESTControllerSimpleName».copyToTransient((«t.RESTTOName») «source»);
				}
			«ENDFOR»
		«ENDIF»
	'''
	
	static def getInputFieldName(Input ip) {
		getInputKeyName(ip)
	}
	
	static def getInputKeyName(Input ip) {
		'''«ip.name.escapeJava»'''
	}
	
	def processContextEmpty(Process it) {
		majorToMinorGUIs.isEmpty &&
			variables.filter[isUsedBeforeAndAfterGUI].isEmpty &&
			directDataFlows.filter[passesGUISIB].uniqueSources.isEmpty &&
			iterateSIBsInUse.isEmpty
	}
	
	def renderSuccessorCall(SIB it, String branchName) {
		val branch = branches.findFirst[name == branchName]
		return '''
			// branch '«branchName»'
			«IF branch?.eventListenerSuccessors?.head !== null»
				return SIB_ID.«branch.eventListenerSuccessors.head.id.escapeJava»;
			«ELSEIF branch?.succSIB !== null»
				return SIB_ID.«branch.succSIB.id.escapeJava»;
			«ELSE»
				throw new IllegalStateException("SIB '«label.escapeString»' has no successor defined for branch '«branchName»'");
			«ENDIF»
		'''
	}

	def renderSuccessorCall(EventListener it, String branchName) '''
		«IF incoming?.head !== null»
			return execute«GUISIBPredecessors.head.id.escapeJava»(ctx);
		«ELSE»
			throw new IllegalStateException("EventListener '«id.escapeString»' has no successor defined");
		«ENDIF»
	'''
	
	def requiresContextTransformer(Variable v) {
		switch (v) {
			ComplexVariable:   true
			PrimitiveVariable: v.dataType == PrimitiveType.FILE
			default:           false
		}
	}
	
	def requiresContextTransformer(OutputPort o) {
		switch (o) {
			ComplexOutputPort:   true
			PrimitiveOutputPort: o.dataType == PrimitiveType.FILE
			default:             false
		}
	}
	
}
