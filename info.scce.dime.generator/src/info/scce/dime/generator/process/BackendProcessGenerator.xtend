/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.process

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator2
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.UserType
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.process.process.Process
import java.nio.file.Path
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.xtend.lib.annotations.Accessors

import static info.scce.dime.generator.process.BackendProcessGeneratorUtil.PROCESS_PKG

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.pkgEscape

class BackendProcessGenerator implements IGenerator2<Process> {
	
	/** 
	 * In default case, {@link #generate(Process, Path, IProgressMonitor)} will generate into a static path
	 * within the Project the passed Process belongs to. But there are cases where a special target project
	 * might be desired, e.g. when generating processes from multiple projects into one target application. In
	 * this case, this {@code setProjectPath} method can be used to override the default behavior.
	 * @param projectPath
	 */
	@Accessors(PUBLIC_SETTER)
	var Path projectPath
	
	/** 
	 * An additional field required for generation, if certain security features are required.
	 * However we still want to implement {@link #generate(Process, Path, IProgressMonitor)}
	 * @param userType
	 */
	@Accessors(PUBLIC_SETTER)
	var UserType userType

	static extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	static extension DimeIOExtension = new DimeIOExtension
	
	extension BackendProcessGeneratorHelper helper
	protected extension RESTExtension = new RESTExtension
	
	new(BackendProcessGeneratorHelper helper) {
		this.helper = helper
	}

	override void generate(Process model, Path outlet, IProgressMonitor monitor) {
		
		val folder = projectPath.createOutputDirectories(model)
		
		// TODO: Create this filename at some central place
		val file = folder.resolve('''«model.simpleTypeName».java''')
		
		// Override the target file if it is older than the model file
		if (!file.exists || file.lastModifiedTime < model.toNIOPath.lastModifiedTime) {
			val content = model.generate
			file.writeString(content)
		}
		
	}

	def void generateContextTransformer() {
		projectPath
			// TODO: Get this folder information from some central place
			.resolve("target", "dywa-app", "app-business", "target", "generated-sources")
			.resolve(PROCESS_PKG.split('''\.'''))
			.createDirectories()
			.resolve("ContextTransformer.java")
			.writeString(generateJSONToContextTransformer)
	}
	
	def static Path createOutputDirectories(Path projectPath, Process model) {
		projectPath
			// TODO get this folder information from some central place
			.resolve("target", "dywa-app", "app-business", "target", "generated-sources")
			.resolve(PROCESS_PKG.split('''\.'''))
			.resolve(model.localPkg.pkgEscape.split('''\.'''))
			.createDirectories()
	}
}
