/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.process

import graphmodel.Node
import info.scce.dime.data.data.Type
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.AbstractIterateSIB
import info.scce.dime.process.process.Attribute
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.ComplexAttribute
import info.scce.dime.process.process.ComplexAttributeConnector
import info.scce.dime.process.process.ComplexExtensionAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexListAttribute
import info.scce.dime.process.process.ComplexListAttributeConnector
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.ControlFlow
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DataFlowSource
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.ExtensionAttribute
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.JavaNativeVariable
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveAttribute
import info.scce.dime.process.process.PrimitiveExtensionAttribute
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveListAttribute
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.PrimitiveVariable
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessInputStatic
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.StartSIB
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.process.process.Variable
import info.scce.dime.siblibrary.JavaType

import static info.scce.dime.generator.util.DyWAExtension.*
import static info.scce.dime.process.process.ComplexListAttributeName.FIRST
import static info.scce.dime.process.process.ComplexListAttributeName.LAST
import static info.scce.dime.process.process.ProcessType.ASYNCHRONOUS

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class BackendProcessGeneratorUtil extends GeneralProcessGeneratorHelper {
	
	protected extension RESTExtension = new RESTExtension
	protected extension DyWAExtension = new DyWAExtension
	
	// TODO: Should be defined in model or dad or somewhere?
	val public static PROCESS_PKG = "info.scce.dime.process"
	val public static GUI_PKG = "info.scce.dime.gui"
	
	val public static PROCESS_CONTEXT_ASSOCIATION = "processContext"
	val public static INTERACTION_INPUTS_ASSOCIATION = "interactionInputs"
	
	var long valueVariableIndex = 0
	
	def getPkg(Process it) {
		'''«PROCESS_PKG.pkgEscape».«localPkg.pkgEscape»'''
	} 
	
	def getPkg(Type it) {
		'''«dywaPkg.pkgEscape».entity.«localPkgWithFilename.pkgEscape»'''
	}
	
	def getPkg(GUI it) {
		'''«GUI_PKG.pkgEscape».«localPkgWithFilename.pkgEscape»'''
	}
	
	override caseIntegerInputStatic(IntegerInputStatic it) {
		'''«value»l'''
	}
	
	override caseBooleanInputStatic(BooleanInputStatic it) {
		'''«value»'''
	}
	
	override caseRealInputStatic(RealInputStatic it) {
		'''«value»d'''
	}
	
	override caseTextInputStatic(TextInputStatic it) {
		'''"«value.escapeString»"'''
	}
	
	override caseProcessInputStatic(ProcessInputStatic input) {
		
		val baseProcess = (input.container as ProcessSIB)
			.proMod
			.processPlaceholderSIBs
			.findFirst[label == input.name]
			?.proMod
			
		val baseInputNames = baseProcess
			.processAllInputs
			.map[ in |
				switch (in) {
					OutputPort:            in.name
					ProcessPlaceholderSIB: in.label
				}
			]
		
		val replaceProcess = input.value
		val replaceInputNames = replaceProcess
			.processAllInputs
			.map[ in |
				switch (in) {
					OutputPort:            in.name
					ProcessPlaceholderSIB: in.label
				}
			]
		
		return '''
			(«baseInputNames.join(", ")») -> {
				// lookup
				«replaceProcess.buildBackendProcessBeanLookup(input.name)»
				
				// execute
				final «replaceProcess.externalResultReturnTypeName» result«input.name» = instance«input.name».execute(«replaceInputNames.join(", ")»);
				
				// evaluate branch
				switch(result«input.name».getBranchName()) {
					«FOR endSIB: replaceProcess.endSIBs»
						case "«endSIB.branchName.escapeJava»":
							// create new base process result
							return new «baseProcess.externalResultReturnTypeName»(
								«val baseEndSIB = baseProcess.endSIBs.findFirst[branchName == endSIB.branchName]»
								new «baseEndSIB.externalStoredTypeName»(
									«baseEndSIB.inputs.map['''result«name».get«endSIB.branchName.toFirstUpper.escapeJava»Return().get«name.toFirstUpper.escapeJava»'''].join(", ")»
								)
							);
					«ENDFOR»
					default:
						throw new IllegalStateException("returned unknown branch " + result«input.name».getBranchName());
				}
			}
		'''
		
	}
	
	def buildBackendProcessBeanLookup(Process process) {
		process.buildBackendProcessBeanLookup("")
	}
	
	def buildBackendProcessBeanLookup(Process process, String name) {
		if (process.requiresDependencyInjection) {
			'''final «process.typeName» instance«name» = CDIUtil.getManagedInstance(ctx.beanManager, «process.typeName».class);'''
		}
		else {
			'''final «process.typeName» instance«name» = new «process.typeName»(ctx.beanManager);'''
		}
	}
	
	override caseTimestampInputStatic(TimestampInputStatic input) {
		'''new Date(«input.value»000l)'''
	}
	
	def getInputByName(SIB sib, String name) {
		sib.inputs.findFirst[ input | input.name == name ]
	}

	def getExternalResultTypeName(Process it) {
		'''«typeName».«resultTypeName»'''
	}
	
	def getExternalResultReturnTypeName(Process it) {
		buildInternalResultReturnTypeName(externalResultTypeName)
	} 
	
	def getResultReturnTypeName(Process it) {
		buildInternalResultReturnTypeName(resultTypeName)
	}
	
	private def static buildInternalResultReturnTypeName(Process process, CharSequence resultTypeName) {
		if (process.processType == ASYNCHRONOUS) {
			'''Future<«resultTypeName»>'''
		}
		else {
			resultTypeName
		}
	}
	
	def getExternalResultTypeName(GUI it) {
		'''«pkg».«resultTypeName»'''
	}

	def getExternalResultReturnTypeName(GUI it, String branch) {
		'''«externalResultTypeName».«branch.escapeJava»Return'''
	}

	def getTypeName(Process it) {
		'''«pkg».«simpleTypeName»'''
	}
	
	def getTypeName(GUI it) {
		'''«pkg».«simpleTypeName»'''
	}

	def getControllerTypeName(Type it) {
		'''«controllerPkg».«name.escapeJava»Controller'''
	}

	def getInteractionControllerTypeName(GuardContainer it) {
		'''«controllerPkg».«interactionTypeName.escapeJava»Controller'''
	}
	
	def getInteractionInputsControllerTypeName(Process subProcess) {
		'''«subProcess.controllerPkg».«subProcess.interactionInputsTypeNameJava.escapeJava»Controller'''
	}
	
	def getSecurityInputsControllerTypeName(Process subProcess) {
		'''«subProcess.controllerPkg».«subProcess.securityInputsTypeName.escapeJava»Controller'''
	}
	
	def getContextControllerTypeName(Process process) {
		'''«process.controllerPkg».«process.contextTypeName.escapeJava»Controller'''
	}
	
	def lookupSecurityInputsController(Process guardProcess, String name) {
		guardProcess.securityInputsControllerTypeName.lookupController(name)
	}
	
	def lookupInteractionInputsController(Process subProcess, String name) {
		subProcess.interactionInputsControllerTypeName.lookupController(name)
	}
	
	def lookupContextController(String name) {
		process.contextControllerTypeName.lookupController(name)
	}
	
	def lookupInteractionController(GuardContainer guardContainer, String name) {
		guardContainer.interactionControllerTypeName.lookupController(name)
	}
	
	def lookupDomainController(Type type, String name) '''
		final «type.controllerTypeName» «name» = this.«type.controllerSimpleName»;
	'''
	
	def lookupController(CharSequence typeName, String name) '''
		final «typeName» «name» = this.«typeName»;
	'''
	
	def callDeleteInteraction(GuardContainer guardContainer, String processVarName, String interactionVarName) '''
		«processVarName».deleteInteraction«guardContainer.id.escapeJava»(«interactionVarName»);
	'''
	
	def deleteInteraction(GuardContainer guardContainer) '''
		public void deleteInteraction«guardContainer.id.escapeJava»(«guardContainer.interactionTypeName.processContextsTypeName» interaction) {
			ctx.beanManager = this.beanManager;
			«val subProcess = guardContainer.subProcess»
			// delete interaction
			«"contextController".lookupContextController»
			contextController.delete«process.contextTypeName.escapeJava»(interaction.get«PROCESS_CONTEXT_ASSOCIATION.escapeJava»());
			
			«subProcess.lookupInteractionInputsController("interInputsController")»
			interInputsController.delete«subProcess.interactionInputsTypeNameJava.escapeJava»(interaction.get«INTERACTION_INPUTS_ASSOCIATION.escapeJava»());
			
			«FOR guardProcessSIB: guardContainer.guardProcessSIBs»
				{
					«val guardProcess = guardProcessSIB.securityProcess»
					«guardProcess.lookupSecurityInputsController("securityInputsController")»
					securityInputsController.delete«guardProcess.securityInputsTypeName.escapeJava»(interaction.get«guardProcess.securityInputsTypeName.toFirstLower.escapeJava»());
				}
			«ENDFOR»
			«guardContainer.lookupInteractionController("interactionController")»
			interactionController.delete«guardContainer.interactionTypeName.escapeJava»(interaction);
		}
	'''
	
	def checkInteraction(GuardContainer guardContainer) '''
		// Security checks for executing the «guardContainer.subProcess.modelName» interaction
		public boolean checkInteraction«guardContainer.id.escapeJava»(final «guardContainer.interactionTypeName.processContextsTypeName» interaction, «guardContainer.subProcess.interactionInputsTypeNameJava.processContextsTypeName» parametersToCheck) {
			
			final «guardContainer.subProcess.interactionInputsTypeNameJava.processContextsTypeName» interactionInputs = interaction.getinteractionInputs();
			
			«FOR input : guardContainer.subProcess.processInputs»
				// if both objects are null, we consider them equal. Otherwise compare:
				if (interactionInputs.get«input.name.escapeJava»() != null || parametersToCheck.get«input.name.escapeJava»() != null) {

					// if one object is null and the other is not, then not equal
					if (interactionInputs.get«input.name.escapeJava»() == null || parametersToCheck.get«input.name.escapeJava»() == null) {
						return false;
					}
					//compare
					if (!interactionInputs.get«input.name.escapeJava»().equals(parametersToCheck.get«input.name.escapeJava»())) {
						return false;
					}
				}
			«ENDFOR»
			
			«FOR processGuard: guardContainer.guardProcessSIBs»
				{
					«val guardProcess = processGuard.securityProcess»
					final «guardProcess.securityInputsTypeName.processContextsTypeName» inputs = interaction.get«guardProcess.securityInputsTypeName.toFirstLower.escapeJava»();
					final «guardProcess.typeName».«guardProcess.resultTypeName» result = «guardProcess.simpleTypeName.toFirstLower».execute(«processGuard.securityProcess.processInputs.sortBy[name].join(', ') [ '''«IF name.equals("currentUser")»getConcreteUser(«guardProcess.concreteUserType.typeName».class)«ELSE»inputs.get«name.escapeJava»()«ENDIF»''' ]»);
					if ("permanently denied".equals(result.getBranchName())) {
						this.deleteInteraction«guardContainer.id.escapeJava»(interaction);
						return false;
					}
					if (!"granted".equals(result.getBranchName())) {
						return false;
					}
				}
			«ENDFOR»
			
			return true;
		}
	'''
	
	def String getTypeName(ProcessPlaceholderSIB it) {
		val typeArguments = proMod
			.processAllInputs
			.map [ con |
				switch con {
					OutputPort:            con.typeName
					ProcessPlaceholderSIB: con.typeName
					default:               throw new IllegalArgumentException('''No case for «class».''')
				}
			]
			+
			#[proMod.externalResultReturnTypeName]
		return '''org.jooq.lambda.function.Function«inputs.size»<«typeArguments.join(", ")»>'''
	}
	
	override getTypeName(OutputPort outputPort) {
		if (outputPort.isList) {
			'''List<«outputPort.singularTypeName»>'''
		}
		else {
			outputPort.singularTypeName
		}
	}
		
	def getSingularTypeName(OutputPort outputPort) {
		switch (outputPort) {
			PrimitiveOutputPort:  outputPort.trgtLangLiteral
			ComplexOutputPort:    outputPort.dataType.typeName
			JavaNativeOutputPort: (outputPort.dataType as JavaType).fqn
		}
	}
	
	def getRouterTypeName(OutputPort outputPort) {
		if (outputPort.isList) {
			'''List<«outputPort.singularRestTypeName»>'''
		}
		else {
			outputPort.singularRestTypeName
		}
	}
	
	def getRouterTypeName(InputPort inputPort) {
		if (inputPort.isList) {
			'''List<«inputPort.singularRestTypeName»>'''
		}
		else {
			inputPort.singularRestTypeName
		}
	}
	
	def getSingularRouterTypeName(OutputPort outputPort) {
		switch (outputPort) {
			PrimitiveOutputPort:  outputPort.restTypeName
			ComplexOutputPort:    "java.lang.Long"
			JavaNativeOutputPort: (outputPort.dataType as JavaType).fqn
		}
	}
		
	def getSingularRouterTypeName(InputPort inputPort) {
		switch (inputPort) {
			PrimitiveInputPort: inputPort.restTypeName
			ComplexInputPort:   "java.lang.Long"
		}
	}
	
	def getRestTypeName(InputPort inputPort) {
		if (inputPort.isList) {
			'''List<«inputPort.singularRestTypeName»>'''
		}
		else {
			inputPort.singularRestTypeName
		}
	}
	
	
	def getRestTypeName(OutputPort outputPort) {
		if (outputPort.isList) {
			'''List<«outputPort.singularRestTypeName»>'''
		}
		else {
			outputPort.singularRestTypeName
		}
	}
		
	def getSingularRestTypeName(OutputPort outputPort) {
		switch (outputPort) {
			PrimitiveOutputPort:  outputPort.dataType.restLangLiteral
			ComplexOutputPort:    outputPort.dataType.restTypeName
			JavaNativeOutputPort: (outputPort.dataType as JavaType).fqn
		}
	}
	
	def getSingularRestTypeName(InputPort inputPort) {
		switch (inputPort) {
			PrimitiveInputPort:  inputPort.dataType.restLangLiteral
			ComplexInputPort:    inputPort.dataType.restTypeName
			JavaNativeInputPort: (inputPort.dataType as JavaType).fqn
		}
	}
	
	def getSimpleTypeName(OutputPort outputPort) {
		switch (outputPort) {
			PrimitiveOutputPort: outputPort.trgtLangLiteral
			ComplexOutputPort:   outputPort.dataType.name.escapeJava
			JavaNativeVariable:  (outputPort.dataType as JavaType).fqn.split("\\.").last
		}
	}
	
	override getTypeName(InputPort inputPort) {
		val type = switch (inputPort) {
			PrimitiveInputPort:  inputPort.trgtLangLiteral
			ComplexInputPort:    inputPort.dataType.typeName
			JavaNativeInputPort: (inputPort.dataType as JavaType).fqn
		}
		if (inputPort.isList) {
			return '''List<«type»>'''
		}
		else {
			return type
		}
	}
			
	override getTypeName(InputStatic input) {
		input.dataType.trgtLangLiteral
	}
	
	def getSimpleTypeName(InputPort inputPort) {
		switch (inputPort) {
			PrimitiveInputPort:  inputPort.trgtLangLiteral
			ComplexInputPort:    inputPort.dataType.name.escapeJava
			JavaNativeInputPort: (inputPort.dataType as JavaType).fqn.split("\\.").last
		}
	}
		
	def buildGetterNoValueSemantics(InputPort inputPort) {
		inputPort.buildGetterInternal[ node | node.buildGetterNoValueSemantics(inputPort) ]
	}
		
	override buildGetter(InputPort inputPort) {
		inputPort.buildGetterInternal[ node | node.buildGetter(inputPort) ]
	}
	
	private def buildGetterInternal(InputPort inputPort, (Node) => CharSequence buildGetter) {
		val incoming = inputPort.dataEdge
		if (incoming !== null) {
			switch (source: incoming.sourceElement) {
				PrimitiveVariable:
					'''ctx.«source.name.escapeJava»'''
				JavaNativeVariable:
					'''ctx.«source.name.escapeJava»'''
				ComplexVariable: 
					buildGetter.apply(source)
				PrimitiveAttribute case source.isList: 
					'''new Array«inputPort.typeName»(«source.getter».orElse(new LinkedList<>()))'''
				PrimitiveAttribute:
					'''«source.getter».orElse(«(inputPort as PrimitiveInputPort).dataType.trgtLangDeclaration»)'''
				PrimitiveExtensionAttribute case source.isList: 
					'''new Array«inputPort.typeName»(«source.getter».orElse(new LinkedList<>()))'''
				PrimitiveExtensionAttribute:
					'''«source.getter».orElse(«(inputPort as PrimitiveInputPort).dataType.trgtLangDeclaration»)'''
				ComplexAttribute, ComplexExtensionAttribute:
					buildGetter.apply(source)
				PrimitiveListAttribute:
					'''Long.valueOf(«source.parent.getter».map(List::size).orElse(0))'''
				ComplexListAttribute case source.attributeName == FIRST:
					'''«source.parent.getter».map(List::stream).flatMap(Stream::findFirst).orElse(null)'''
				ComplexListAttribute case source.attributeName == LAST:
					'''«source.parent.getter».map(List::stream).flatMap(stream -> stream.reduce((previous, current) -> current)).orElse(null)'''
				ComplexListAttribute:
					throw new IllegalArgumentException('''No case for ComplexListAttributeName «source.attributeName».''')
				OutputPort:
					'''ctx.«source.varName»'''
			}
		}
	}
	
	dispatch def buildGetterNoValueSemantics(ComplexVariable source, InputPort inputPort) {
		if (source.complexVariablePredecessor === null) {
			'''ctx.«source.name.escapeJava»'''
		}
		else if (source.isList) {
			'''«source.getter».orElse(new LinkedList<>())'''
		}
		else {
			'''«source.getter».orElse(null)'''
		} 
	}
	
	dispatch def buildGetterNoValueSemantics(ComplexAttribute source, InputPort inputPort) {
		if (source.isList) {
			'''«source.getter».orElse(new LinkedList<>())'''
		}
		else {
			'''«source.getter».orElse(null)'''
		}
	}
	
	dispatch def buildGetterNoValueSemantics(ComplexExtensionAttribute source, InputPort inputPort) {
		if (source.isList) {
			'''«source.getter».orElse(new LinkedList<>())'''
		}
		else {
			'''«source.getter».orElse(null)'''
		}
	}
	
	dispatch def buildGetter(ComplexVariable source, InputPort inputPort) {
		if (source.isList) {
			'''new Array«inputPort.typeName»(«source.buildGetterNoValueSemantics(inputPort)»)'''
		}
		else {
			source.buildGetterNoValueSemantics(inputPort)
		}
	}
	
	dispatch def buildGetter(ComplexAttribute source, InputPort inputPort) {
		if (source.isList) {
			'''new Array«inputPort.typeName»(«source.buildGetterNoValueSemantics(inputPort)»)'''
		}
		else {
			source.buildGetterNoValueSemantics(inputPort)
		}
	}
	
	dispatch def buildGetter(ComplexExtensionAttribute source, InputPort inputPort) {
		if (source.isList) {
			'''new Array«inputPort.typeName»(«source.buildGetterNoValueSemantics(inputPort)»)'''
		}
		else {
			source.buildGetterNoValueSemantics(inputPort)
		}
	}
	
	def buildSetterForSingleOutputPortNoValueSemantics(OutputPort outputPort, String value) '''
		«FOR outgoing : outputPort.outgoingDataFlows.sortTopologically»
			«outgoing.buildSetterNoValueSemantics(value)»
		«ENDFOR»
	'''
	
	override buildSetterForSingleOutputPort(OutputPort outputPort, String value) '''
		«FOR outgoing : outputPort.outgoingDataFlows.sortTopologically»
			«outgoing.buildSetter(value)»
		«ENDFOR»
	'''
	
	override buildSetter(DataFlow outgoing, String value) {
		buildSetterInternal(outgoing, value, true)
	}
	
	def buildSetterNoValueSemantics(DataFlow outgoing, String value) {
		buildSetterInternal(outgoing, value, false)
	}
	
	def String buildSetterInternal(DataFlow flow, String valueStr, boolean valueSemantics) {
		
		val source = flow.sourceElement
		val target = flow.targetElement
		
		val port = if (source instanceof OutputPort) {
			source
		}
		else if (target instanceof InputPort) {
			target
		}
		
		val sourceIsList = switch (source) {
			PrimitiveVariable:  source.isList
			ComplexVariable:    source.isList
			JavaNativeVariable: source.isList
			Attribute:          source.isList
			Input:              source.isList
			Output:             source.isList
			default:            false
		}
		
		val sourceTypeName = switch (source) {
			PrimitiveVariable:  source.typeName
			ComplexVariable:    source.typeName
			JavaNativeVariable: source.typeName
			Attribute:          source.typeName
			InputPort:          source.typeName
			OutputPort:         source.typeName
			default:            null
		}

		
		val targetIsList = switch (target) {
			PrimitiveVariable:  target.isList
			ComplexVariable:    target.isList
			JavaNativeVariable: target.isList
			Attribute:          target.isList
			Input:              target.isList
			Output:             target.isList
			default:            false
		}
		
		val value = if (targetIsList) {
			'''value«valueVariableIndex++»'''
		}
		else {
			valueStr
		}
		
		val setStatement = switch (target) {
			
			PrimitiveVariable:
				target.buildSetterToVariable(sourceIsList, value)
				
			ComplexVariable:
				if (target.complexVariablePredecessor === null && target.isList && sourceIsList && valueSemantics) {
					target.buildSetterToVariable(sourceIsList, '''new Array«target.typeName»(«value»)''')
				}
				else if (target.complexVariablePredecessor === null) {
					target.buildSetterToVariable(sourceIsList, value)
				}
				else if (target.isList && !sourceIsList) {
					'''«target.setter».add(«value»)'''
				}
				else if (target.isList && sourceIsList && valueSemantics) {
					'''«target.listSourceSetter»(new Array«target.typeName»(«value»))'''
				}
				else if (target.isList && sourceIsList) {
					'''«target.listSourceSetter»(«value»)'''
				} 
				else {
					'''«target.setter»(«value»)'''
				}
				
			JavaNativeVariable:
				target.buildSetterToVariable(sourceIsList, value)
				
			ComplexAttribute: 
				if (target.isList && !sourceIsList) {
					'''«target.setter».add(«value»)'''
				}
				else if (target.isList && sourceIsList && valueSemantics) {
					'''«target.listSourceSetter»(new Array«target.typeName»(«value»))'''
				}
				else {
					'''«target.listSourceSetter»(«value»)'''
				}
				
			PrimitiveAttribute: 
				if (target.isList && !sourceIsList) {
					'''«target.setter».add(«value»)'''
				}
				else if (target.isList && sourceIsList && valueSemantics) {
					'''«target.listSourceSetter»(new Array«target.typeName»(«value»))'''
				}
				else {
					'''«target.listSourceSetter»(«value»)'''
				}
				
			ComplexListAttribute: 
				if (target.attributeName == FIRST) {
					'''«target.parent.getter».get().set(0, «value»)'''
				}
				else if (target.attributeName == LAST) {
					'''«target.parent.getter».get().set(«target.parent.getter».get().size() - 1, «value»)'''
				}
				
			InputPort: // TODO Switch over outgoing DDF and then use one variable per input
				if (target.isList && !sourceIsList) {
					'''ctx.«(port as OutputPort).varName».add(«value»)'''
				}
				else if (target.isList && sourceIsList && valueSemantics) {
					'''ctx.«(port as OutputPort).varName» = new Array«(port as OutputPort).typeName»(«value»)'''
				}
				else {
					'''ctx.«(port as OutputPort).varName» = «value»'''
				}
				
			Input:
				if (target.isList && !sourceIsList) {
					'''ctx.«(port as OutputPort).varName».add(«value»)'''
				}
				else {
					'''ctx.«(port as OutputPort).varName» = «value»'''
				}
				
			OutputPort: // TODO Switch over outgoing DDF and then use one variable per input
				if (target.isList && !sourceIsList) {
					'''ctx.«(port as InputPort).varName».add(«value»)'''
				}
				else if (target.isList && sourceIsList && valueSemantics) {
					'''ctx.«(port as InputPort).varName» = new Array«(port as InputPort).typeName»(«value»)'''
				}
				else {
					'''ctx.«(port as InputPort).varName» = «value»'''
				}
				
			Output:
				if (target.isList && !sourceIsList) {
					'''ctx.«(port as InputPort).varName».add(«value»)'''
				}
				else {
					'''ctx.«(port as InputPort).varName» = «value»'''
				}
				
		}
		
		if (targetIsList) {
			return '''
				«sourceTypeName» «value» = «valueStr»;
				if («value» != null) { // Prevent null in lists
					«setStatement»;
				}
			'''
		}
		else {
			return '''
				«setStatement»;
			'''
		}
		
	}
		
	override buildSetterToVariable(Variable target, OutputPort outputPort, String value) {
		if (target.isList && !outputPort.isList) {
			'''ctx.«target.name.escapeJava».add(«value»)'''
		}
		else {
			'''ctx.«target.name.escapeJava» = «value»'''
		}
	}
	
	def buildSetterToVariable(Variable target, boolean isList, String value) {
		if (target.isList && !isList) {
			'''ctx.«target.name.escapeJava».add(«value»)'''
		}
		else {
			'''ctx.«target.name.escapeJava» = «value»'''
		}
	}
	
	def getListSourceSetter(PrimitiveAttribute attr) {
		'''«attr.parent.getter».get().set«attr.nameOfAccessor»'''
	}
	
	def getSetter(PrimitiveAttribute attr) {
		if (attr.isList) {
			'''«attr.parent.getter».map(«attr.parent.type.typeName»::get«attr.nameOfAccessor»).get()'''
		}
		else {
			'''«attr.parent.getter».get().set«attr.nameOfAccessor»'''
		}
	}
	
	def getListSourceSetter(ComplexAttribute attr) {
		'''«attr.parent.getter».get().set«attr.nameOfAccessor»'''
	}
	
	def getSetter(ComplexAttribute attr) {
		if (attr.isList) {
			'''«attr.parent.getter».map(«attr.parent.type.typeName»::get«attr.nameOfAccessor»).get()'''
		}
		else {
			'''«attr.parent.getter».get().set«attr.nameOfAccessor»'''
		}
	}
	
	def getListSourceSetter(ComplexVariable cv) {
		'''«cv.complexVariablePredecessor.getter».get().set«cv.complexVariablePredecessor.type.getAttributeByName(cv.attributeName).nameOfAccessor»'''
	}
	
	def getSetter(ComplexVariable cv) {
		if (cv.complexVariablePredecessor === null) {
			'''Optional.ofNullable(ctx.«cv.name.escapeJava»)'''
		}
		else {
			'''«cv.complexVariablePredecessor.getter».«cv.innerSetter»'''
		}
	}
	
	def getGetter(PrimitiveAttribute attr) {
		'''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.nameOfAccessor»)'''
	}
	
	def getGetter(PrimitiveExtensionAttribute attr) {
		'''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.attribute.nameOfAccessor»)'''
	}
	
	def getGetter(ComplexAttribute attr) {
		'''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.nameOfAccessor»)'''
	}
	
	def getGetter(ComplexExtensionAttribute attr) {
		'''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.attribute.nameOfAccessor»)'''
	}
	
	def CharSequence getGetter(ComplexVariable cv) {
		if (cv.complexVariablePredecessor === null) {
			'''Optional.ofNullable(ctx.«cv.name.escapeJava»)'''
		}
		else {
			'''«cv.complexVariablePredecessor.getter».«cv.innerGetter»'''
		}
	}
	
	def getLiteralOrGetterNoValueSemantics(Input input) {
		input.buildLiteralOrGetter[buildGetterNoValueSemantics]
	}
	
	override getLiteralOrGetter(Input input) {
		input.buildLiteralOrGetter[buildGetter]
	}
	
	private def buildLiteralOrGetter(Input input, (InputPort) => CharSequence buildGetter) {
		switch (it: input) {
			InputStatic:                      doSwitch(it)
			InputPort case dataEdge !== null: buildGetter.apply(it)
			InputPort case isList:            "new LinkedList<>()"
			ComplexInputPort:                 "null"
			JavaNativeInputPort:              "null"
			PrimitiveInputPort:               nullDeclaration
		}
	}
	
	def isIsList(Attribute attr) {
		switch (it: attr) {
			ComplexAttribute:   isList
			ExtensionAttribute: isList
			PrimitiveAttribute: isList
		}
	}
	
	def isList(ComplexAttribute attr) {
		attr.attribute.isList
	}
	
	def isList(ExtensionAttribute attr) {
		attr.attribute.isList
	}
	
	def isList(PrimitiveAttribute attr) {
		attr.attribute.isList
	}
	
	def isList(Input input) {
		switch(it: input) {
			InputPort: isList
			default: false
		}
	}
	
	def isList(Output output) {
		switch (output) {
			OutputPort: output.isList
			default:    false
		}
	}
	
	def getNameOfAccessor(PrimitiveAttribute attr) {
		attr.attribute.nameOfAccessor
	}
	
	def getNameOfAccessor(ComplexAttribute attr) {
		attr.attribute.nameOfAccessor
	}
	
	def getNameOfAccessor(Type type, String name) {
		type.getAttributeByName(name).nameOfAccessor
	}
	
	def getParent(Attribute attr) {
		attr.container as ComplexVariable
	}
	
	def getAttributeName(ComplexVariable cv) {
		cv.getIncoming(ComplexAttributeConnector).head.attributeName
	}
	
	def getAttributeByName(Type type, String name) {
		type.inheritedAttributes.findFirst[ attr | attr.name == name ]
	}
	
	def getType(ComplexVariable cv) {
		cv.dataType
	}
	
	def getUnfoldedListAttributeName(ComplexVariable cv) {
		cv.getIncoming(ComplexListAttributeConnector).head?.attributeName
	}
	
	def getUnfoldListParent(ComplexVariable cv) {
		cv.getIncoming(ComplexListAttributeConnector).head?.sourceElement	
	}
	
	def getInnerGetter(ComplexVariable cv) {
		val parentCV = cv.complexVariablePredecessor
		if (parentCV.isList) {
			switch cv.getUnfoldedListAttributeName {
				case FIRST: '''map(List::stream).flatMap(Stream::findFirst)'''
				case LAST: '''map(List::stream).flatMap(stream -> stream.reduce((previous, current) -> current))'''
			}
		}
		else {
			'''map(«parentCV.type.typeName»::get«parentCV.type.getAttributeByName(cv.attributeName).nameOfAccessor»)'''
		}
	}
	
	def getInnerSetter(ComplexVariable cv) {
		val parentCV = cv.complexVariablePredecessor
		if (cv.isList) {
			'''map(«parentCV.type.typeName»::get«parentCV.type.getAttributeByName(cv.attributeName).nameOfAccessor»).get()'''
		}
		else {
			'''get().set«parentCV.type.getAttributeByName(cv.attributeName).nameOfAccessor»'''
		}
	}
	
	def getTypeNameOfDataSource(InputPort inputPort) {
		switch (it: inputPort.dataEdge?.sourceElement) {
			Variable:   typeName
			OutputPort: typeName
		}
	}
	
	def getTypeName(Attribute attr) {
		val typeName = switch (it: attr) {
			ComplexAttribute:       attribute.dataType.typeName
			ComplexListAttribute:   listType.typeName
			PrimitiveAttribute:     attribute.dataType.trgtLangLiteral
//			PrimitiveListAttribute: (listType as info.scce.dime.data.data.PrimitiveType).trgtLangLiteral
			default:                "?"
		}
		if (attr.isList) {
			'''List<«typeName»>'''
		}
		else {
			typeName
		}
	}
	
	override getTypeName(Variable variable) {
		if (variable.isList) {
			'''List<«variable.singularTypeName»>'''
		}
		else {
			variable.singularTypeName
		}
	}
	
	def getSingularTypeName(Variable variable) {
		switch (it: variable) {
			ComplexVariable:    dataType.typeName
			PrimitiveVariable:  dataType.trgtLangLiteral
			JavaNativeVariable: (dataType as JavaType).fqn
		}
	}
	
	def getRestTypeName(Variable variable) {
		if (variable.isList) {
			'''List<«variable.singularRestTypeName»>'''
		}
		else {
			variable.singularRestTypeName
		}
	}
	
	def getSingularRestTypeName(Variable variable) {
		switch (it: variable) {
			ComplexVariable:    dataType.restTypeName
			PrimitiveVariable:  dataType.getRestLangLiteral
			JavaNativeVariable: (dataType as JavaType).fqn
		}
	}
	
	def getRestTypeName(Type it) {
		'''«dywaPkg.pkgEscape».rest.types.«name.escapeJava»'''
	}
	
	override getTypeName(Type it) {
		'''«originalType.pkg».«name.escapeJava»'''
	}
	
	def getControlFlowSucc(DataFlowSource source) {
		source.getOutgoing(ControlFlow).head?.targetElement
	}
	
	def getIdentifier(DataFlowTarget sib) {
		switch (it: sib) {
			SIB:    label
			EndSIB: branchName
		}
	}
	
	def getTypeName(EndSIB it) {
		'''«branchName.toFirstUpper.escapeJava»Return'''
	}
	
	def getImplTypeName(EndSIB it) {
		'''«typeName»Impl'''
	}
	
	def getExternalStoredTypeName(EndSIB it) {
		'''«rootElement.typeName».«implTypeName»'''
	}
	
	def getSuccSIB(AbstractBranch branch) {
		branch.dataFlowTargetSuccessors.head
	}
	
	override getVarName(OutputPort ddf) {
		val parentName = switch (it: ddf.container) {
			StartSIB:       "start"
			AbstractBranch: '''«SIBPredecessors.head.label»«name.toFirstUpper»'''
		}
		return '''«parentName»«ddf.name.toFirstUpper»«ddf.id»'''.escapeJava
	}
	
	def getVarName(InputPort ddf) {
		val parentName = switch (it: ddf.container) {
			SIB: '''«label»«name.toFirstUpper»'''
		}
		return '''«parentName»«ddf.name.toFirstUpper»«ddf.id»'''.escapeJava
	}
	
	def getVarName(ProcessPlaceholderSIB it) {
		'''placeholder«label.toFirstUpper»«id»'''.escapeJava
	}
	
	override getIterVarName(AbstractIterateSIB iter) {
		'''counter«iter.label.toFirstUpper»«iter.id»'''.escapeJava
	}
	
	override getTrgtLangLiteral(PrimitiveOutputPort output) {
		if (output.isIsList && output.dataType != PrimitiveType.FILE) {
			output.dataType.trgtLangLiteral
		}
		else {
			output.dataType.trgtLangLiteral
		}
	}
	
	override getTrgtLangLiteral(PrimitiveInputPort input) {
//		if (input.isList) {
//			input.dataType.trgtLangLiteral.toFirstUpper
//		}
//		else {
			input.dataType.trgtLangLiteral
//		}
	}
	
	def getTrgtLangLiteral(info.scce.dime.data.data.PrimitiveType pType) {
		switch (pType) {
			case BOOLEAN:   "java.lang.Boolean"
			case INTEGER:   "java.lang.Long"
			case REAL:      "java.lang.Double"
			case TEXT:      "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE:      "info.scce.dime.util.FileReference"
		}
	}
	
	def getRestLangLiteral(PrimitiveType pType) {
		switch (pType) {
			case BOOLEAN:   "java.lang.Boolean"
			case INTEGER:   "java.lang.Long"
			case REAL:      "java.lang.Double"
			case TEXT:      "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE:      '''«dywaPkg».rest.util.FileReference'''
		}
	}
	
	override getTrgtLangLiteral(PrimitiveType pType) {
		switch (pType) {
			case BOOLEAN:   "java.lang.Boolean"
			case INTEGER:   "java.lang.Long"
			case REAL:      "java.lang.Double"
			case TEXT:      "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE:      "info.scce.dime.util.FileReference"
		}
	}
	
	override getTrgtLangDeclaration(PrimitiveType pType) {
		switch (pType) {
			case BOOLEAN:   "false"
			case INTEGER:   "0l"
			case REAL:      "0.0d"
			case TEXT:      "new java.lang.String()"
			case TIMESTAMP: "new java.util.Date()"
			case FILE:      "null"
		}
	}
	
	override getNullDeclaration(PrimitiveInputPort input) {
		if (input.isList) {
			"null"
		}
		else {
			input.dataType.nullDeclaration
		}
	}
	
	def getNullDeclaration(info.scce.dime.data.data.Attribute attr) {
		if (attr.isList) {
			"new java.util.LinkedList<>()"
		}
		else {
			switch (attr.type) {
				case info.scce.dime.data.data.PrimitiveType.BOOLEAN: "false"
				case info.scce.dime.data.data.PrimitiveType.INTEGER: "0l"
				case info.scce.dime.data.data.PrimitiveType.REAL:    "0.0d"
				default: "null"
			}
		}
	}
	
	override getNullDeclaration(PrimitiveType pType) {
		switch (pType) {
			case BOOLEAN:   "false"
			case INTEGER:   "0l"
			case REAL:      "0.0d"
			case TEXT:      "null"
			case TIMESTAMP: "null"
			case FILE:      "null"
		}
	}
	
	def getGuardedProcessSIB(GuardContainer guardContainer) {
		guardContainer.guardedProcessSIBs.head
	}
	
	static def getSubProcess(GuardContainer guardContainer) {
		guardContainer.guardedProcessSIBs.head?.proMod
	}
	
	private static def String getInteractionInputsTypeNameInternal(Process interaction) {
		'''InteractionInputsFor«interaction.modelName»'''
	}
	
	static def String getInteractionInputsTypeNameJava(Process interaction) {
		'''«interaction.interactionInputsTypeNameInternal.escapeJava»'''
	}
	
	static def String getInteractionInputsTypeNameDart(Process interaction) {
		'''«interaction.interactionInputsTypeNameInternal.escapeDart»'''
	}
	
	static def String getSecurityInputsTypeName(Process security) {
		'''SecurityInputsFor«security.modelName.escapeJava»'''
	}
	
	static def String getInteractionTypeName(GuardContainer guardContainer) {
		'''Interaction«guardContainer.subProcess.modelName.escapeJava»In«(guardContainer.container as Process).modelName.escapeJava»«guardContainer.id.escapeJava»'''
	}

	static def String getContextTypeName(Process longrunning) {
		'''«longrunning.modelName.escapeJava»Context'''
	}
	
}
