/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.process

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator2
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.dad.dad.DAD
import info.scce.dime.process.process.Process
import java.nio.file.Path
import org.eclipse.core.runtime.IProgressMonitor

import static extension info.scce.dime.generator.process.BackendProcessGenerator.createOutputDirectories
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.escapeJava

class StartupProcessGenerator implements IGenerator2<Process> {
	
	extension DimeIOExtension = new DimeIOExtension
	
	val DAD dad

	new (DAD dad) {
		this.dad = dad
	}

	override void generate(Process model, Path outlet, IProgressMonitor monitor) {
		monitor.beginTask("Generating Startup Process Model", 100)
		monitor.subTask("Generating Startup Bean Class")
		val contents = new StartupProcessGeneratorHelper().generate(model)
		if (contents !== null) {
			try {
				dad
					.projectPath
					.createOutputDirectories(model)
					.resolve('''Startup«model.modelName.escapeJava.toFirstUpper».java''')
					.writeString(contents)
			}
			catch (Exception e) {
				throw new RuntimeException(e)
			}
		}
		monitor.worked(90)
		monitor.subTask("Writing Java Files")
	}
	
}
