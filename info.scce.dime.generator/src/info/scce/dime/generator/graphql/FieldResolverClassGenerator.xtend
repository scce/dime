/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.generator.graphql

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.graphql.schema.graphql.GraphQL
import info.scce.dime.graphql.schema.graphql.Resolver
import info.scce.dime.graphql.schema.graphql.ResolverAttribute
import info.scce.dime.process.process.Process

import static info.scce.dime.generator.util.JavaIdentifierUtils.*

/*
 * For each data type referenced in the "Resolvers" column in the GraphQL model, this
 * generator generates a <DataType>FieldResolver.java file that contains the process 
 * calls to resolve fields of an entity of type <DataType>.
 */
class FieldResolverClassGenerator extends CincoRuntimeBaseClass {
	
	protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	protected extension GraphQLExtension = new GraphQLExtension
	protected extension DimeIOExtension = new DimeIOExtension

	val GraphQL model
	
	new (GraphQL model) {
		this.model = model
	}	
	
	def generate() {
		val resolvers = model.resolverss.head.resolvers
		if (resolvers.nullOrEmpty) {
			return
		}
		val targetFolder = model
			.projectPath
			.resolve("target", "dywa-app", "app-business", "src", "main", "java", "info", "scce", "dime", "graphql", "resolvers", "fields")
			.createDirectories()
		for (resolver: resolvers) {
			val filename = '''«resolver.referencedType.name.toFirstUpper»FieldResolver.java'''
			val contents = template(resolver)
			targetFolder.resolve(filename).writeString(contents)
		}
	}
	
	private def template(Resolver resolver) '''
		package info.scce.dime.graphql.resolvers.fields;
		
		import graphql.schema.GraphQLObjectType;
		import graphql.schema.TypeResolver;
		import graphql.TypeResolutionEnvironment;
		import javax.inject.Inject;
		import java.util.List;
				
		@javax.transaction.Transactional
		@javax.enterprise.context.RequestScoped
		public class «resolver.referencedType.name.toFirstUpper»FieldResolver {
			
			// inject linked processes that are linked to field resolvers
			«FOR process : resolver.resolverAttributes.flatMap[it.resolverProcesss].map[it.resolverProcess]»
				@Inject
				private «process.fullyQualifiedType» «process.id»_process;
			«ENDFOR»
		
		    «FOR attribute: resolver.resolverAttributes»
		    	«{
		    		val entityPackage = '''de.ls5.dywa.generated.entity.«pkgEscape(resolver.referencedType.localPkgWithFilename)»'''
	    			val entityName = '''«resolver.referencedType.name.toFirstUpper»'''
	    			var referencedAttribute = attribute.referencedAttribute
	    			
	    			var returnType = ""
	    			var methodName = "resolve"
	    			if (referencedAttribute instanceof PrimitiveAttribute) {
	    				returnType = '''«referencedAttribute.dataType.javaType»'''
	    				methodName = '''«methodName»«referencedAttribute.name.toFirstUpper»'''
	    			} else if (referencedAttribute instanceof ComplexAttribute) {
	    				returnType = '''«entityPackage».«referencedAttribute.dataType.name»'''
	    				methodName = '''«methodName»«referencedAttribute.dataType.name.toFirstUpper»'''
	    			}
	    			
	    			// wrap return type in a List if necessary
	    			if (referencedAttribute.isIsList) returnType = '''List<«returnType»>'''
	    			
	    			'''
	    				public «returnType» «methodName»(«entityPackage».«entityName» entity) {
	    					// execute the process linked to the resolved field
	    					«attribute.fullyQualifiedProcessResultType» result = «attribute.referencedProcess.id»_process.execute(true, entity);
	    					    				
	    					if(result.getBranchName().equals("success")) {
	    						return result.getSuccessReturn().get«attribute.referencedProcess.endSIBs.get(0).inputPorts.get(0).name.toFirstUpper»();
	    					}
	    						
	    					throw new RuntimeException("Unhandled branch in field resolver process.");
	    				}
	    			'''
		    	}»
			«ENDFOR»
		}
	'''

	private def getReferencedProcess(ResolverAttribute attribute) {
		attribute.resolverProcesss.get(0).resolverProcess
	}
	
	private def getFullyQualifiedType(Process process) {
		return '''info.scce.dime.process.dime__HYPHEN_MINUS__models.graphql.«pkgEscape(process.modelName.toFirstUpper + process.id)»'''
	}
				
	private def getFullyQualifiedProcessResultType(ResolverAttribute attribute) {
		return '''«attribute.referencedProcess.fullyQualifiedType».«pkgEscape(attribute.referencedProcess.modelName.toFirstUpper + attribute.referencedProcess.id)»Result'''
	}
}