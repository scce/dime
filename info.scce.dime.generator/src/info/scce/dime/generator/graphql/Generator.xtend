/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.graphql

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator2
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.process.BackendProcessGenerator
import info.scce.dime.generator.process.BackendProcessGeneratorHelper
import info.scce.dime.graphql.schema.graphql.ForbiddenType
import info.scce.dime.graphql.schema.graphql.ForbiddenUserType
import info.scce.dime.graphql.schema.graphql.GraphQL
import info.scce.dime.graphql.schema.graphql.Type
import info.scce.dime.process.process.Process
import java.nio.file.Path
import java.util.List
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.xtend.lib.annotations.Data

/** 
 * This generator is the entry point for all GraphQL related stuff.
 */
class Generator implements IGenerator2<GraphQL> {
	
	extension DimeIOExtension = new DimeIOExtension
	
	var IProgressMonitor monitor = new NullProgressMonitor
	var Path outlet
	var GenerationContext genctx
	var BackendProcessGenerator backendProcessGenerator

	new () {
		// Intentionally left blank
	}

	new (GenerationContext genctx) {
		this.genctx = genctx
		var backendProcessGeneratorHelper = new BackendProcessGeneratorHelper(genctx)
		backendProcessGenerator = new BackendProcessGenerator(backendProcessGeneratorHelper)
		backendProcessGenerator.projectPath = this.genctx.dad.projectPath
		backendProcessGenerator.userType = this.genctx.dad.systemUsers.head?.systemUser
		backendProcessGenerator.generateContextTransformer()
	}

	override void generate(GraphQL model, Path outlet, IProgressMonitor monitor) {
		
		this.outlet = outlet
		this.monitor = monitor
		
		println("Starting generator for GraphQL API")
		
		val graphqlComponent = genctx.dad.graphQLComponents.head
		if (graphqlComponent === null) {
			return
		}
		
		println("Generating GraphQL API")
		
		// Collect all data types from all referenced data models
		val processedDataModels = newHashSet
		val types = graphqlComponent
			.outgoingGraphQLDataPointers
			.map[targetElement.internalDataComponent]
			.flatMap [ comp |
				processedDataModels.add(comp.id)
				return comp.model.types
			]
			.toList
		
		// Collect all data types from data models referenced by ReferencedTypes
		graphqlComponent
			.outgoingGraphQLDataPointers
			.flatMap[targetElement.internalDataComponent.model.referencedTypes]
			.map[referencedType.rootElement]
			.reject[processedDataModels.contains(id)]
			.flatMap[ data | data.types ]
			.filter[ type |
				types.forall[name != type.name]
			]
			.forEach[ type | types.add(type) ]
		
		// Find all attributes that should not be accessible via the GraphQL API
		val forbiddenTypes = model.forbiddenTypess.head
		val forbiddenList = (forbiddenTypes.forbiddenTypes + forbiddenTypes.forbiddenUserTypes)
			.map[ type | new ForbiddenTypeObject(type) ]
			.toList
		val apiQuerySibs = model.queriess.head.APISIBs
		val apiMutationSibs = model.mutationss.head.APISIBs
		
		new SchemaGenerator(model, types, forbiddenList, apiQuerySibs, apiMutationSibs).generate()
		new TypeResolverClassGenerator(types).generate()
		new FieldResolverClassGenerator(model).generate()
		new WrapperClassGenerator(model, types).generate()
		new ServletGenerator(model, types).generate()
		
		// Generate process files used in APISIBs that are not already part of used processes
		for (apiSib: apiQuerySibs + apiMutationSibs) {
			val process = apiSib.model.processSIBs.head.proMod
			generateBackendProcess(process)
		}
		
		// Generate process files used in field resolvers
		model
			.resolverss
			.head
			.resolvers
			.flatMap[resolverAttributes]
			.map[resolverProcesss.head.resolverProcess]
			.forEach[ process | generateBackendProcess(process) ]
	}

	def private void generateBackendProcess(Process referencedProcess) {
		for (process: referencedProcess.allSubProcesses) {
			if (genctx.usedProcesses.forall[id != process.id]) {
				println('''Generating process: «process.modelName»''')
				backendProcessGenerator.generate(process, outlet, monitor)
			}
		}
	}
	
	def private getAllSubProcesses(Process process) {
		return #[process] + process.getAllSubProcessesRecursive(newArrayList)
	}
	
	def private List<Process> getAllSubProcessesRecursive(Process process, List<Process> foundProcesses) {
		val processes = newArrayList
		val subProcesses = process
			.processSIBs
			.map[internalProcessSIB.proMod]
			.reject[ p | foundProcesses.contains(p) ]
			.toList
		foundProcesses.addAll(subProcesses)
		processes.addAll(subProcesses)
		for (p: subProcesses) {
			processes.addAll(p.getAllSubProcessesRecursive(foundProcesses))
		}
		return processes
	}

	@Data
	static class ForbiddenTypeObject {
		
		val String typeID
		val List<String> attributeIDs
		val boolean fullyRemoved
		
		new (Type type) {
			
			this.typeID = switch (type) {
				ForbiddenType:     type.referencedType.id
				ForbiddenUserType: type.referencedType.id
			}
			
			this.attributeIDs = newArrayList(
				type
					.forbiddenComplexAttributes
					.filter[isForbidden]
					.map [attribute.id]
				+
				type
					.forbiddenPrimitiveAttributes
					.filter[isForbidden]
					.map [attribute.id]
			)
			
			this.fullyRemoved = attributeIDs.size == type.forbiddenComplexAttributes.size + type.forbiddenPrimitiveAttributes.size
			
		}

		override boolean equals(Object o) {
			switch (o) {
				case this === o:     true
				ForbiddenTypeObject: o.typeID == this.typeID
				String:              o == this.typeID
				default:             false
			}
		}
		
	}
}
