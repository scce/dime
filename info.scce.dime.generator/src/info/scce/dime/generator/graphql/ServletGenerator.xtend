/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.generator.graphql

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.graphql.api.api.PrimitiveType
import info.scce.dime.graphql.schema.graphql.GraphQL
import java.util.List
import java.util.Set

/**
 * Generates the java servlet that serves the endpoint for the GraphQL API.
 */
class ServletGenerator extends CincoRuntimeBaseClass {
	
	protected extension DataExtension = DataExtension.instance
	protected extension DimeIOExtension = new DimeIOExtension
			
	val GraphQL model
	val List<Type> types
	
	val Set<Type> complexQueryTypes = newHashSet
	val Set<Type> complexMutationTypes = newHashSet
	val Set<PrimitiveType> primitiveQueryTypes = newHashSet
	val Set<PrimitiveType> primitiveMutationTypes = newHashSet
	
	new (GraphQL model, List<Type> types) {
		this.model = model
		this.types = types
	}

	def generate() {
		
		returnedClasses()
							
		val gqlFolder = model
			.projectPath
			.resolve("target", "dywa-app", "app-business", "src", "main", "java", "info", "scce", "dime", "graphql")					
			.createDirectories()
					
		gqlFolder
			.resolve("Endpoint.java")
			.writeString(getGenerateEndpoint())
			
		gqlFolder
			.resolve("DimeErrorHandler.java")
			.writeString(getGenerateDimeErrorHandler())
			
		gqlFolder
			.resolve("DimeException.java")
			.writeString(getGenerateDimeException())
		
		new QueryAndMutationGenerator(model, gqlFolder).generate()
		
	}

	def returnedClasses() {
		
		// Queries
		var apiSIBs = model.queriess.head.APISIBs

		for (apisib: apiSIBs) {
			val endSIB = apisib.model.endSIBs.head
			if (!endSIB.complexAPIoutputs.nullOrEmpty) {
				complexQueryTypes.add(endSIB.complexAPIoutputs.head?.dataType)
			}
			if (!endSIB.primitiveAPIoutputs.nullOrEmpty) {
				primitiveQueryTypes.add(endSIB.primitiveAPIoutputs.head.dataType)
			}
		}

		// Mutations
		apiSIBs = model.mutationss.head.APISIBs
		for (apisib: apiSIBs) {
			val endSIB = apisib.model.endSIBs.head
			if (!endSIB.complexAPIoutputs.nullOrEmpty) {
				complexMutationTypes.add(endSIB.complexAPIoutputs.head?.dataType)
			}
			if (!endSIB.primitiveAPIoutputs.nullOrEmpty) {
				primitiveMutationTypes.add(endSIB.primitiveAPIoutputs.head.dataType)
			}
		}
		
	}

	def getGenerateEndpoint() '''
		package info.scce.dime.graphql;
		
		import graphql.kickstart.execution.GraphQLObjectMapper;
		import graphql.kickstart.servlet.GraphQLConfiguration;
		import graphql.kickstart.servlet.GraphQLHttpServlet;
		import graphql.schema.DataFetcher;
		import graphql.schema.GraphQLSchema;
		import graphql.schema.StaticDataFetcher;
		import graphql.schema.idl.RuntimeWiring;
		import graphql.schema.idl.SchemaGenerator;
		import graphql.schema.idl.SchemaParser;
		import graphql.schema.idl.TypeDefinitionRegistry;
		import graphql.schema.idl.TypeRuntimeWiring;
		import info.scce.dime.graphql.resolvers.*;
		import info.scce.dime.util.CDIUtil;
		
		import org.apache.commons.io.IOUtils;
		import org.slf4j.Logger;
		import org.slf4j.LoggerFactory;
		
		import javax.servlet.annotation.WebServlet;
		import javax.transaction.Transactional;
		
		import java.io.IOException;
		import java.io.InputStream;
		import java.nio.charset.StandardCharsets;
		import java.util.ArrayList;
		import java.util.List;
		import java.util.Map;
		import java.util.Set;
		
		import static graphql.schema.idl.RuntimeWiring.newRuntimeWiring;
		
		@WebServlet("/graphql/*")
		@Transactional
		public class Endpoint extends GraphQLHttpServlet {
			
			private static final Logger LOGGER = LoggerFactory.getLogger(Endpoint.class);
			
			@Override
			protected GraphQLConfiguration getConfiguration() {
				GraphQLObjectMapper dimeObjectMapper = GraphQLObjectMapper
					.newBuilder()
					.withGraphQLErrorHandler(new DimeErrorHandler())
					.build();
				
				return GraphQLConfiguration
					.with(createSchema())
					.with(dimeObjectMapper)
					.build();
				
			}
			
			private GraphQLSchema createSchema() {
				String schema = null;
				try {
					InputStream schemaStream = getClass().getClassLoader().getResourceAsStream("«SchemaGenerator.SCHEMA_FILE_NAME»");
					if(schemaStream==null){
						throw new NullPointerException("«SchemaGenerator.SCHEMA_FILE_NAME» not found");
					} else {
						schema = IOUtils.toString(schemaStream, StandardCharsets.UTF_8);
					}
				} catch (IOException | NullPointerException e) {
					LOGGER.error("Could not open «SchemaGenerator.SCHEMA_FILE_NAME»");
					return null;
				}		
				
				SchemaParser schemaParser = new SchemaParser();
				TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);
				
				RuntimeWiring.Builder runtimeWiringBuilder = newRuntimeWiring();
				
				«FOR type: types.filter[it instanceof AbstractType]»
				runtimeWiringBuilder.type(TypeRuntimeWiring.newTypeWiring("«type.name»")
					.typeResolver(new «type.name»TypeResolver())
					.build());
				«ENDFOR»
				
				«FOR type: types.filter[it instanceof ConcreteType]»
					«IF !type.incomingInheritances.nullOrEmpty»
						runtimeWiringBuilder.type(TypeRuntimeWiring.newTypeWiring("«type.name»Union")
							.typeResolver(new «type.name»UnionTypeResolver())
							.build());
					«ENDIF»
				«ENDFOR»
				
				List<Class<? extends AbstractQuery>> queryClasses = new ArrayList<Class<? extends AbstractQuery>>();
				List<Class<? extends AbstractQuery>> mutationClasses = new ArrayList<Class<? extends AbstractQuery>>();
				
				«FOR complexType : complexQueryTypes»
					queryClasses.add(«(complexType as Type).name»Queries.class);
				«ENDFOR»
				
				«FOR primitiveType : primitiveQueryTypes»
					queryClasses.add(«(primitiveType as PrimitiveType).getName»Queries.class);
				«ENDFOR»
				
				«FOR complexType : complexMutationTypes»
					mutationClasses.add(«(complexType as Type).name»Mutations.class);
				«ENDFOR»
				
				«FOR primitiveType : primitiveMutationTypes»
					mutationClasses.add(«(primitiveType as PrimitiveType).getName»Mutations.class);
				«ENDFOR»
				
				for(Class<? extends AbstractQuery> clazz : queryClasses){
					for(Map.Entry<String, DataFetcher> entry : CDIUtil.getManagedInstance(clazz).getDataFetchers().entrySet()){
						runtimeWiringBuilder.type("Query", builder -> builder.dataFetcher(entry.getKey(), entry.getValue()));
					}
				}
				
				for(Class<? extends AbstractQuery> clazz : mutationClasses){
					for(Map.Entry<String, DataFetcher> entry : CDIUtil.getManagedInstance(clazz).getDataFetchers().entrySet()){
						runtimeWiringBuilder.type("Mutation", builder -> builder.dataFetcher(entry.getKey(), entry.getValue()));
					}
				}
				
				RuntimeWiring runtimeWiring = runtimeWiringBuilder.build();
				
				SchemaGenerator schemaGenerator = new SchemaGenerator();
				return schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);
			}
		}
	'''

	// Generate Error Classes
	def getGenerateDimeErrorHandler() '''
		package info.scce.dime.graphql;
		
		import graphql.ExceptionWhileDataFetching;
		import graphql.GraphQLError;
		import graphql.kickstart.execution.error.DefaultGraphQLErrorHandler;
		import graphql.kickstart.execution.error.GenericGraphQLError;
		
		import java.util.List;
		import java.util.stream.Collectors;
		
		public class DimeErrorHandler extends DefaultGraphQLErrorHandler {
			
			@Override
			public List<GraphQLError> processErrors(List<GraphQLError> errors) {
				// Only get errors which are intended for the client
				final List<GraphQLError> clientErrors = filterGraphQLErrors(errors);
				
				// Handle internal server errors
				if (clientErrors.size() < errors.size()) {
					// Some errors were filtered out to hide implementation - put a generic error in place.
					clientErrors.add(new GenericGraphQLError("Internal Server Error(s) while executing query"));
					
					// Log internal errors
					errors.stream()
							.filter(error -> !isClientError(error))
							.forEach(this::logError);
				}
				
				// Handle Dime Exceptions:
				// Remove ExceptionWhileDataFetching wrapper around the actual Dime Exception
				return clientErrors.stream()
						.map(this::prettifyDimeExceptions)
						.collect(Collectors.toList());
			}
			
			@Override
			protected boolean isClientError(GraphQLError error) {
				// Do not replace boolean operators with bitwise operators for obvious reasons
				return !(error instanceof ExceptionWhileDataFetching)
						|| ((ExceptionWhileDataFetching) error).getException() instanceof GraphQLError
						|| ((ExceptionWhileDataFetching) error).getException() instanceof DimeException;
			}
			
			private GraphQLError prettifyDimeExceptions(GraphQLError error) {
				if (error instanceof ExceptionWhileDataFetching) {
					Throwable innerException = (Exception) ((ExceptionWhileDataFetching) error).getException();
					if(innerException instanceof DimeException){
						return new GenericGraphQLError(innerException.getMessage());
					}
				}
				return error;
			}
		}
	'''
	
	def getGenerateDimeException() '''
		package info.scce.dime.graphql;
		
		import graphql.ErrorClassification;
		import graphql.GraphQLError;
		import graphql.GraphQLException;
		import graphql.language.SourceLocation;
		
		import java.util.List;
		
		public class DimeException extends RuntimeException {
			protected DimeException(String message){
				super(message);
			}
		}
	'''
}
