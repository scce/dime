/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */

package info.scce.dime.generator.graphql

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.generator.util.DyWAExtension
import java.util.List

/**
 * Generates TypeResolver classes for abstract data types.
 */
class TypeResolverClassGenerator extends CincoRuntimeBaseClass {
	
	protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	protected extension DyWAExtension = new DyWAExtension
	protected extension DataExtension = DataExtension.instance
	protected extension DimeIOExtension = new DimeIOExtension

	val List<Type> types
	
	new (List<Type> types) {
		this.types = types
	}	
	
	def generate() {
		
		if (types.nullOrEmpty) {
			return
		}
		
		val targetFolder = types
			.head
			.projectPath
			.resolve("target", "dywa-app", "app-business", "src", "main", "java", "info", "scce", "dime", "graphql", "resolvers")
			.createDirectories()
		
		for (type: types.filter(AbstractType)) {
			targetFolder
				.resolve('''«type.name.toFirstUpper»TypeResolver.java''')
				.writeString(template(type, 'TypeResolver'))
		}
			
		for (type: types.filter(ConcreteType).reject(AbstractType).reject[incomingInheritances.nullOrEmpty]) {
			targetFolder
				.resolve('''«type.name.toFirstUpper»UnionTypeResolver.java''')
				.writeString(template(type, 'UnionTypeResolver'))
		}
			
	}
	
	private def template(Type type, String classSuffix) '''
		package info.scce.dime.graphql.resolvers;
		
		import graphql.schema.GraphQLObjectType;
		import graphql.schema.TypeResolver;
		import graphql.TypeResolutionEnvironment;
		
		public class «type.name»«classSuffix» implements TypeResolver {
			
		    @Override
		    public GraphQLObjectType getType(TypeResolutionEnvironment env) {
		        Object javaObject = env.getObject();
				«FOR concreteType: type.getKnownSubTypes.filter[!(it instanceof AbstractType)].filter[it !== type].reverse»
					if (javaObject instanceof info.scce.dime.graphql.wrapper.«concreteType.name.toFirstUpper») {
						return env.getSchema().getObjectType("«concreteType.name»");
					}
				«ENDFOR»
				«IF !(type instanceof AbstractType)»
					if (javaObject instanceof info.scce.dime.graphql.wrapper.«type.name.toFirstUpper») {
						return env.getSchema().getObjectType("«type.name»");
					}
				«ENDIF»
				return null;
			}
		}
	'''
	
}
