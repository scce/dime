/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.graphql;

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserType
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.graphql.schema.graphql.GraphQL
import java.nio.file.Path
import java.util.Arrays
import java.util.List

import static info.scce.dime.data.data.PrimitiveType.*

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.pkgEscape

/**
 *  Generates files for marshalling database objects to serializable objects compatible with the GraphQL API.
 *  Such a wrapper file is generated for each type referenced in the data models.
 */
class WrapperClassGenerator extends CincoRuntimeBaseClass{
	
	protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	protected extension DyWAExtension = new DyWAExtension
	protected extension GraphQLExtension = new GraphQLExtension
	protected extension DataExtension = DataExtension.instance
	protected extension DimeIOExtension = new DimeIOExtension
	
	static val ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
		
	val List<Type> types
	val GraphQL model
	
	new (GraphQL model, List<Type> types) {
		this.model = model
		this.types = types
	}
	
	def generate() {
		val wrapperFolder = model
			.projectPath
			.resolve("target", "dywa-app", "app-business", "src", "main", "java", "info", "scce", "dime", "graphql", "wrapper")
			.createDirectories()
		for (type: types) {
			generateInterfaceClassForType(wrapperFolder, type)
			generateWrapperClassForType(wrapperFolder, type)
		}
	}
	
	
	def void generateInterfaceClassForType(Path folder, Type type) {
		switch (type) {
			ReferencedType: {
				generateInterfaceClassForType(folder, type.referencedType)
			}
			AbstractType,
			UserType,
			ConcreteType: {
				val contents = '''
					package info.scce.dime.graphql.wrapper;
					
					import java.util.List;
					import java.util.Date;
					import de.ls5.dywa.generated.rest.util.FileReference;
					
					public interface I«type.name.toFirstUpper» «implementsTemplate(type)» {
						public Long getDywaId();
						public String getDywaType();
						«FOR attribute: type.inheritedAttributeSeq»
							public «getGetterString(attribute, true)»;
						«ENDFOR»
					}
				'''
				folder
					.resolve('''I«type.name.toFirstUpper».java''')
					.writeString(contents)
			}
		}
	}
	
	def void generateWrapperClassForType(Path folder, Type type) {
		switch (type) {
			ReferencedType: {
				generateWrapperClassForType(folder, type.referencedType)
			}
			AbstractType: {
				val wrapperClass = type.generateAbstractWrapperClass
				folder
					.resolve('''«type.name.toFirstUpper».java''')
					.writeString(wrapperClass)
			}
			UserType,
			ConcreteType: {
				val wrapperClass = type.generateWrapperClass
				folder
					.resolve('''«type.name.toFirstUpper».java''')
					.writeString(wrapperClass)
			}
		}
	}
	
	def implementsTemplate(Type type) {
		if (type.outgoingInheritances.empty) {
			return ""
		}
		else {
			val interfaceNames = type.outgoingInheritances
				.filter[targetElement instanceof AbstractType || targetElement instanceof ConcreteType]
				.map['''I«targetElement.name»''']
			if (interfaceNames.empty) {
				return ""
			}
			else {
				return '''extends «interfaceNames.join(", ")»'''
			}
		}
	}
	
	def getConcreteTypes(Type type) {
		return type.knownSubTypes.reject(AbstractType).toList.reverse
	}
	
	def generateAbstractWrapperClass(Type type) '''
		package info.scce.dime.graphql.wrapper;
		
		public interface «type.name.toFirstUpper» «implementsTemplate(type)» {}
	'''
	
	def resolverExistsForType(Type type) {
		return model
			.resolverss
			.head
			.resolvers
			.filter[referencedType.name == type.name]
			.head !== null
	}
	
	def resolverExistsForAttribute(Type type, Attribute attribute) {
		val matchingAttribute = model
			.resolverss
			.head
			.resolvers
			.filter[referencedType.name == type.name]
			.head
			
		if (matchingAttribute == null) {
			return false
		}
			
		return matchingAttribute
			.resolverAttributes
			.map[referencedAttribute]
			.exists[
				(name == attribute.name) &&
				(isList == attribute.isList) &&
				(
					(it instanceof PrimitiveAttribute && attribute instanceof PrimitiveAttribute) ||
					(it instanceof ComplexAttribute   && attribute instanceof ComplexAttribute  )
				)
			]
	}
	
	def javaType(Attribute attribute) {
		var type = ""
		switch (attribute) {
			PrimitiveAttribute: {
				type = attribute.dataType.getName.toFirstUpper.primitiveToSupportedType
			}
			ComplexAttribute: {
				type = '''de.ls5.dywa.generated.entity.«attribute.dataType.localPkgWithFilename.pkgEscape».«attribute.dataType.name»'''
			}
		}
		if (attribute.isList) {
			type = '''List<«type»>'''
		}
		return type
	}
	
	def resolverTemplate(Type type, Attribute attribute) {
		if (resolverExistsForAttribute(type, attribute)) {
			val resolverClass = '''«type.name.toFirstUpper»FieldResolver'''
			return '''
				BeanManager bm = CDI.current().getBeanManager();
				Set<Bean<?>> beans = bm.getBeans(«resolverClass».class);
				Bean<«resolverClass»> bean = (Bean<«resolverClass»>) bm.resolve(beans);
				CreationalContext<«resolverClass»> cctx = bm.createCreationalContext(bean);
				«resolverClass» resolver = («resolverClass») bm.getReference(bean, «resolverClass».class, cctx);
				«IF attribute instanceof PrimitiveAttribute»
					«javaType(attribute)» resolvedField = resolver.resolve«attribute.name.toFirstUpper»(this.entity);
				«ELSEIF attribute instanceof ComplexAttribute»
					«javaType(attribute)» resolvedField = resolver.resolve«attribute.dataType.name.toFirstUpper»(this.entity);
				«ENDIF»
			'''
		}
		else {
			return ""
		}
	}
	
	def resolverReturnValueTemplate(Type type, Attribute attribute, String alterntive) {
		if (resolverExistsForAttribute(type, attribute)) {
			return "resolvedField"
		}
		else {
			return alterntive
		}
	}
	
	def hasSubTypes(Type type) {
		return (type instanceof AbstractType) || (type instanceof ConcreteType && !type.incomingInheritances.nullOrEmpty)
	}
	
	def generateWrapperClass(Type type) {
		
		return '''
			package info.scce.dime.graphql.wrapper;
			
			import javax.enterprise.context.spi.CreationalContext;
			import javax.enterprise.inject.spi.Bean;
			import javax.enterprise.inject.spi.BeanManager;
			import javax.enterprise.inject.spi.CDI;
			import java.util.List;
			import java.util.Set;
			import java.util.Map;
			import java.util.Date;
			import java.util.TimeZone;
			import java.text.SimpleDateFormat;
			import java.text.DateFormat;
			import java.text.ParseException;
			import java.util.stream.Collectors;
			import de.ls5.dywa.generated.rest.util.FileReference;
			import de.ls5.dywa.generated.controller.«type.localPkgWithFilename.pkgEscape».«type.name.toFirstUpper»Controller;
			«FOR ea: type.inheritedAttributeSeq.filter[it instanceof ExtensionAttribute].map[it as ExtensionAttribute]»
				import info.scce.dime.process.dime__HYPHEN_MINUS__models.data.extension.«ea.process.modelName»«ea.process.id.escapedEnumLiteral»;
			«ENDFOR»
			«IF resolverExistsForType(type)»
				import info.scce.dime.graphql.resolvers.fields.«type.name.toFirstUpper»FieldResolver;
			«ENDIF»
			
			public class «type.name.toFirstUpper» implements I«type.name.toFirstUpper» {
				
				private de.ls5.dywa.generated.entity.«type.localPkgWithFilename.pkgEscape».«type.name.toFirstUpper» entity;
				
				public «type.name.toFirstUpper»(de.ls5.dywa.generated.entity.«type.localPkgWithFilename.pkgEscape».«type.name.toFirstUpper» entity) {
					this.entity = entity;
				}
				
				@Override
				public Long getDywaId() {
					return this.entity.getDywaId();
				}
				
				@Override
				public String getDywaType() {
					return "«type.localPkgWithFilename».«type.name.toFirstUpper»";
				}
				
				«FOR attribute: type.inheritedAttributeSeq»
					@Override
					public «getGetterString(attribute, true)» {
						«{
							if (attribute instanceof PrimitiveAttribute) {
								if (attribute.dataType.equals(FILE)) {
									'''
										«resolverTemplate(type, attribute)»
										if (this.entity.get«attribute.name.escapedAttributeName»() != null) {
											return new FileReference(«resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name.escapedAttributeName»()''')»);
										} else {
											return null;
										}
									'''
								} else if (attribute.dataType.equals(TIMESTAMP)) {
									'''
									«resolverTemplate(type, attribute)»
									
									«IF resolverExistsForAttribute(type, attribute)»
										if (resolvedField == null) return null;
									«ELSE»
										if (this.entity.get«attribute.name.escapedAttributeName()»() == null) return null;
									«ENDIF»
									
									DateFormat df = new SimpleDateFormat("«ISO_DATE_FORMAT»");
									df.setTimeZone(TimeZone.getTimeZone("UTC"));
									return df.format(«resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name.escapedAttributeName()»()''')»);
									'''
								} else {
									'''
									«resolverTemplate(type, attribute)»
									return «resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name.escapedAttributeName()»()''')»;
									'''
								}
							} else if (attribute instanceof ComplexAttribute) {
								if (attribute.dataType instanceof EnumType) {
									if (attribute.isIsList) {
										'''
											«resolverTemplate(type, attribute)»
											return «resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name»«attribute.methodSuffix»()''')».stream()
												.map(e -> e.getDywaName())
												.collect(Collectors.toList());
										'''
									} else {
										'''
											if (this.entity.get«attribute.name»«attribute.methodSuffix»() != null) {
												«resolverTemplate(type, attribute)»
												return «resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name»«attribute.methodSuffix»()''')».getDywaName();
											} else {
												return null;
											}
										'''
									}
								} else {
									if (attribute.isList) {
										val concreteAttributeDataType = if (attribute.dataType instanceof ReferencedType) {
											(attribute.dataType as ReferencedType).referencedType
										} else {
											attribute.dataType
										}
										
										if (concreteAttributeDataType.hasSubTypes) {
											'''
												«resolverTemplate(type, attribute)»
												return «resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name»«attribute.methodSuffix»()''')».stream()
													.map(e -> {
														«FOR concreteType: getConcreteTypes(concreteAttributeDataType)»
														if (e instanceof de.ls5.dywa.generated.entity.«concreteType.localPkgWithFilename.pkgEscape».«concreteType.name.toFirstUpper») {
															return new «concreteType.name.toFirstUpper»((de.ls5.dywa.generated.entity.«concreteType.localPkgWithFilename.pkgEscape».«concreteType.name.toFirstUpper») e);
														}
														«ENDFOR»
														return null;
													})
													.collect(Collectors.toList());
											'''
										} else {
											'''
												«resolverTemplate(type, attribute)»
												return «resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name»«attribute.methodSuffix»()''')».stream()
													.map(«concreteAttributeDataType.name.toFirstLower» -> new «concreteAttributeDataType.name.toFirstUpper»(«attribute.dataType.name.toFirstLower»))
													.collect(Collectors.toList());
											'''
										}
									} else {
										val concreteAttributeDataType = if (attribute.dataType instanceof ReferencedType) {
											(attribute.dataType as ReferencedType).referencedType
										} else {
											attribute.dataType
										}
										
										if (concreteAttributeDataType.hasSubTypes) {
											'''
											«resolverTemplate(type, attribute)»
											«FOR concreteType: getConcreteTypes(concreteAttributeDataType)»
											if (this.entity.get«attribute.name»«attribute.methodSuffix»() != null && this.entity.get«attribute.name»«attribute.methodSuffix»() instanceof de.ls5.dywa.generated.entity.«concreteType.localPkgWithFilename.pkgEscape».«concreteType.name.toFirstUpper») {
												return new «concreteType.name.toFirstUpper»((de.ls5.dywa.generated.entity.«concreteType.localPkgWithFilename.pkgEscape».«concreteType.name.toFirstUpper») this.entity.get«attribute.name»«attribute.methodSuffix»());
											}
											«ENDFOR»
											return null;
											'''
										} else {
											'''
												if (this.entity.get«attribute.name»«attribute.methodSuffix»() != null) {
													«resolverTemplate(type, attribute)»
													return new «concreteAttributeDataType.name.toFirstUpper»(«resolverReturnValueTemplate(type, attribute, '''this.entity.get«attribute.name»«attribute.methodSuffix»()''')»);
												} else {
													return null;
												}
											'''
										}
									}
								}
							} else if (attribute instanceof ExtensionAttribute) {
								'''
									«IF attribute.process.requiresDependencyInjection»
									final «getExtensionProcessClass(attribute)» process = info.scce.dime.util.CDIUtil.getManagedInstance(«getExtensionProcessClass(attribute)».class);
									«ELSE»
									final «getExtensionProcessClass(attribute)» process = new «getExtensionProcessClass(attribute)»(javax.enterprise.inject.spi.CDI.current().getBeanManager());
									«ENDIF»
									final «getExtensionProcessClass(attribute)».«getExtensionProcessClass(attribute)»Result result = process.execute(false, this.entity);
									«IF attribute.dataType.isPrimitiveType»
									return result.getSuccessReturn().get«getExtensionProcessReturn(attribute)»;
									«ELSE»
										«IF attribute.dataType.isEnumType»
											«IF attribute.isIsList»
											return result.getSuccessReturn().get«getExtensionProcessReturn(attribute)».stream()
												.map(e -> e.getDywaName())
												.collect(Collectors.toList());
											«ELSE»
											if (result.getSuccessReturn().get«getExtensionProcessReturn(attribute)» != null) {
												return result.getSuccessReturn().get«getExtensionProcessReturn(attribute)».getDywaName();
											} else {
												return null;
											}
											«ENDIF»
										«ELSE»
											«IF attribute.isIsList»
											return result.getSuccessReturn().get«getExtensionProcessReturn(attribute)».stream()
												.map(e -> {
													«switchAbstractTypeTemplate(attribute)»
												})
												.collect(Collectors.toList());
											«ELSE»
											«switchAbstractTypeTemplate(attribute)»
											«ENDIF»
										«ENDIF»
									«ENDIF»
								'''
							}
						}»
					}
				«ENDFOR»
				
				public static de.ls5.dywa.generated.entity.«type.localPkgWithFilename.pkgEscape».«type.name.toFirstUpper» toEntity(Map<String, Object> map) {
					if (map == null) {
						return null;
					}
					
					BeanManager bm = CDI.current().getBeanManager();
					Set<Bean<?>> beans = bm.getBeans(«getControllerType(type)».class);
					Bean<«getControllerType(type)»> bean = (Bean<«getControllerType(type)»>) bm.resolve(beans);
					CreationalContext<«getControllerType(type)»> cctx = bm.createCreationalContext(bean);
					«getControllerType(type)» ctrl = («getControllerType(type)») bm.getReference(bean, «getControllerType(type)».class, cctx);
					
					de.ls5.dywa.generated.entity.«type.localPkgWithFilename.pkgEscape».«type.name.toFirstUpper» entity = ctrl.createTransient("«type.name.toFirstUpper»");
					
					«FOR attr: type.inheritedAttributeSeq»
						«IF attr instanceof PrimitiveAttribute»
						if (map.containsKey("«attr.name»") && map.get("«attr.name»") != null) {
							«IF attr.isIsList»
								«IF attr.dataType.isFile»
									List<FileReference> list = entity.get«attr.name»();
									((List<Map<String, Object>>) map.get("«attr.name»")).stream()
										.forEach(fr -> {
											«createFileReferenceTemplate()»
											list.add(fileReference);
										});
								«ELSEIF attr.dataType.isDate»
									DateFormat df = new SimpleDateFormat("«ISO_DATE_FORMAT»");
									List<Date> list = entity.get«attr.name»();
									((List<String>) map.get("«attr.name»")).stream()
										.forEach(date -> {
											try {
												list.add(df.parse(date));
											} catch (ParseException ignored) {}
										});
								«ELSE»
									List<«primitiveToSupportedType(attr.dataType)»> list = entity.get«attr.name»();
									«IF attr.dataType === INTEGER»
									((List<Integer>) map.get("«attr.name»")).stream()
										.forEach(value -> { list.add(Long.valueOf(value)); });
									«ELSE»
									((List<«primitiveToSupportedType(attr.dataType)»>) map.get("«attr.name»")).stream()
										.forEach(value -> { list.add((«primitiveToSupportedType(attr.dataType)») value); });
									«ENDIF»
								«ENDIF»
							«ELSE»
								«IF attr.dataType.isFile»
									FileReference fr = (FileReference) map.get("«attr.name»");
									«createFileReferenceTemplate()»
									entity.set«attr.name.escapedAttributeName»(fileReference);
								«ELSEIF attr.dataType.isDate»
									try {
										DateFormat df = new SimpleDateFormat("«ISO_DATE_FORMAT»");
										entity.set«attr.name.escapedAttributeName»(df.parse((String) map.get("«attr.name»")));
									} catch (ParseException ignored) {}
								«ELSE»
									«IF attr.dataType === INTEGER»
									entity.set«attr.name.escapedAttributeName»(Long.valueOf((Integer) map.get("«attr.name»")));
									«ELSE»
									entity.set«attr.name.escapedAttributeName»((«primitiveToSupportedType(attr.dataType)») map.get("«attr.name»"));
									«ENDIF»
								«ENDIF»
							«ENDIF»
						}
						«ELSEIF attr instanceof ComplexAttribute»
						if (map.containsKey("«attr.name»_«attr.dataType.name»") && map.get("«attr.name»_«attr.dataType.name»") != null) {
							«IF attr.dataType instanceof EnumType»
								«IF attr.isIsList»
									List<«attr.dataType.fullyQualifiedName»> list = entity.get«attr.name»_«attr.dataType.name»();
									((List<String>) map.get("«attr.name»_«attr.dataType.name»")).forEach(e -> {
										«FOR literal: attr.dataType.enumLiterals»
											if ("«literal.name.escapedEnumLiteral»".equals(e)) {
												list.add(«attr.dataType.fullyQualifiedName».«literal.name.escapedEnumLiteral»);
											}
										«ENDFOR»
									});
								«ELSE»
									String literal = (String) map.get("«attr.name»_«attr.dataType.name»");
									«FOR literal: attr.dataType.enumLiterals»
									if ("«literal.name.escapedEnumLiteral»".equals(literal)) {
										entity.set«attr.name.escapedAttributeName»(«attr.dataType.fullyQualifiedName».«literal.name.escapedEnumLiteral»);
									}
									«ENDFOR»
								«ENDIF»
							«ELSE»
								«{
									val concreteAttributeDataType = if (attr.dataType instanceof ReferencedType) {
										(attr.dataType as ReferencedType).referencedType
									} else {
										attr.dataType
									}
									
									'''
									«IF attr.isIsList»
										List<«attr.dataType.fullyQualifiedName»> list = entity.get«attr.name»_«attr.dataType.name»();
										((List<Map<String, Object>>) map.get("«attr.name»_«attr.dataType.name»"))
											.forEach(e -> {
												«IF concreteAttributeDataType instanceof AbstractType»
													«FOR concreteType: getConcreteTypes(concreteAttributeDataType as AbstractType)»
													if (((String) e.get("dywaType")).endsWith("«concreteType.name»")) {
														list.add(«concreteType.name».toEntity(e));
													}
													«ENDFOR»
												«ELSE»
												list.add(«attr.dataType.name».toEntity(e));
												«ENDIF»
											});
									«ELSE»
										«IF concreteAttributeDataType instanceof AbstractType»
											Map<String, Object> o = (Map<String, Object>) map.get("«attr.name»_«attr.dataType.name»");
											«FOR concreteType: getConcreteTypes(concreteAttributeDataType as AbstractType)»
												if (((String) o.get("dywaType")).endsWith("«concreteType.name»")) {
													entity.set«attr.name.escapedAttributeName»(«concreteType.name».toEntity(o));
												}
											«ENDFOR»
										«ELSE»
										entity.set«attr.name.escapedAttributeName»(«concreteAttributeDataType.name».toEntity((Map<String, Object>) map.get("«attr.name»_«attr.dataType.name»")));
										«ENDIF»
									«ENDIF»
									'''
								}»
							«ENDIF»
						}
						«ENDIF»
					«ENDFOR»
					
					return entity;
				}
			}
		'''
	}
	
	def fullyQualifiedName(Type type) {
		var package = "de.ls5.dywa.generated.entity."
		if (type instanceof ReferencedType) {
			package += type.referencedType.localPkgWithFilename.pkgEscape
		}
		else {
			package += type.localPkgWithFilename.pkgEscape
		}
		return '''«package».«type.name»'''
	}
	
	def switchAbstractTypeTemplate(ExtensionAttribute attribute) {
		val typeFromString = findTypeByName(attribute.dataType)
		if (typeFromString instanceof AbstractType) {
			return '''
				«FOR concreteType: getConcreteTypes(typeFromString as AbstractType)»
					if (result.getSuccessReturn().get«getExtensionProcessReturn(attribute)» instanceof de.ls5.dywa.generated.entity.«concreteType.localPkgWithFilename.pkgEscape».«concreteType.name.toFirstUpper») {
						return new «concreteType.name.toFirstUpper»((de.ls5.dywa.generated.entity.«concreteType.localPkgWithFilename.pkgEscape».«concreteType.name.toFirstUpper») result.getSuccessReturn().get«getExtensionProcessReturn(attribute)»);
					}
				«ENDFOR»
				return null;
			'''
		}
		else {
			return '''return new «attribute.dataType»(e);'''
		}
	}
	
	def getExtensionProcessReturn(ExtensionAttribute attr) {
		'''«attr.process.endSIBs.filter[branchName == "success"].head.inputPorts.head.name.toFirstUpper»()'''
	}
	
	def getExtensionProcessClass(ExtensionAttribute attr) {
		'''«attr.process.modelName»«attr.process.id.escapedEnumLiteral»'''
	}
	
	def createFileReferenceTemplate() '''
		info.scce.dime.util.DomainFile df = new info.scce.dime.util.DomainFile();
		df.setId(fr.getDywaId());
		df.setContentType(fr.getContentType());
		df.setFileName(fr.getFileName());
		df.setCreatedAt(fr.getCreatedAt());
		info.scce.dime.util.FileReference fileReference = new info.scce.dime.util.FileReference(df);
	'''
		
	def getControllerType(Type type) {
		'''«type.name.toFirstUpper»Controller'''
	}
	
	def getMethodSuffix(ComplexAttribute attribute) {
		if (attribute.isList) {
			return '''_«attribute.dataType.name.toFirstUpper»'''
		}
		else {
		 	return ""
		}
	}
	
	def findTypeByName(String name) {
		types.findFirst[ type | type.name == name ]
	}
	
	def typeWithInterface(Type type, boolean isInterface) {
		if (isInterface) {
			return '''I«type.name.toFirstUpper»'''
		}
		else {
			return type.name.toFirstUpper
		}
	}
			
	def getGetterString(Attribute attribute, boolean isInterface) {
		switch (attribute) {
			ComplexAttribute: {
				val attrName = attribute.name.toFirstUpper
				var typeString = if (attribute.dataType instanceof EnumType) "String" else typeWithInterface(attribute.dataType, isInterface)
				if (attribute.isList) {
					typeString = '''List<«typeString»>'''
				}
				return ''' «typeString» get«attrName»_«attribute.dataType.name.toFirstUpper»()'''
			}
			PrimitiveAttribute: {
				var typeString = attribute.dataType.primitiveToSupportedType
				if (attribute.isList) {
					typeString = '''List<«typeString»>'''
				}
				return ''' «typeString» get«attribute.name.toFirstUpper»()'''
			}
			ExtensionAttribute: {
				val attrName = attribute.name.toFirstUpper
				if (attribute.dataType.isPrimitiveType) {
					var typeString = primitiveToSupportedType(attribute.dataType)
					if (attribute.isList) {
						typeString = '''List<«typeString»>'''
					}
					return ''' «typeString» get«attrName»()'''
				}
				else {
					var typeString = if (attribute.dataType.isEnumType) "String" else '''I«attribute.dataType.toFirstUpper»'''
					if (attribute.isList) {
						typeString = '''List<«typeString»>'''
					}
					return ''' «typeString» get«attrName»_«attribute.dataType.toFirstUpper»()'''
				}
			}
			default: {
				throw new RuntimeException('''Unknown data type: «attribute.class.name»''')
			}
		}
	}
	
	def isReservedJavaKeyword(String keyword) {
		Arrays.asList("abstract", "class").contains(keyword)
	}
	
	def escapedAttributeName(String name) {
		if (name.isReservedJavaKeyword) '''_«name»''' else name
	}
	
	def escapedEnumLiteral(String name) {
		name.replaceAll(" ", "_").replaceAll('-', '__HYPHEN_MINUS__')
	}
		
	def getAttributeQualifier(Attribute attribute){
		switch(attribute) {
			ComplexAttribute:   '''«attribute.dataType.name» «attribute.name»'''
			PrimitiveAttribute: '''«attribute.dataType.literal.primitiveToSupportedType» «attribute.name»'''
			default:            throw new RuntimeException('''Unknown data type: «attribute.class.name»''')
		}
	}
	
	def isFile(PrimitiveType type) {
		type.equals(FILE)
	}
	
	def isDate(PrimitiveType type) {
		type.equals(TIMESTAMP)
	}
	
	def isPrimitiveType(String type) {
		#["Boolean", "Integer", "Real", "Text", "File"].contains(type)
	}
	
	def isComplexType(String type) {
		types
			.filter[
				it instanceof ConcreteType ||
				it instanceof ReferencedType ||
				it instanceof UserType
			]
			.map[name]
			.contains(type)
	}
	
	def isEnumType(String type) {
		types
			.filter[it instanceof EnumType]
			.map[name]
			.contains(type)
	}
	
	def primitiveToSupportedType(PrimitiveType type) {
		switch (type) {
			case BOOLEAN: "Boolean"
			case FILE:    "FileReference"
			case INTEGER: "Long"
			case REAL:    "Double"
			case TEXT:    "String"
			default:      "String"
		}
	}
	
	def primitiveToSupportedTypeGraphQl(PrimitiveType type) {
		switch (type) {
			case BOOLEAN: "Boolean"
			case FILE:    "FileReference"
			case INTEGER: "Integer"
			case REAL:    "Double"
			case TEXT:    "String"
			default:      "String"
		}
	}
	
	def primitiveToSupportedType(String datatype){
		switch (datatype){
			case "File":    "FileReference"
			case "Text":    "String"
			case "Real":    "Double"
			case "Boolean": "Boolean"
			case "Integer": "Long"
			default:        "String"
		}
	}
	
}
