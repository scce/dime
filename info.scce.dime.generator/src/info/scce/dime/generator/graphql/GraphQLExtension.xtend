/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.generator.graphql

import info.scce.dime.data.data.PrimitiveType

class GraphQLExtension {
	
	dispatch def getJavaType(info.scce.dime.data.data.PrimitiveType type) {
		return switch (type) {
			case PrimitiveType.TEXT:      "String"
			case PrimitiveType.INTEGER:   "Long"
			case PrimitiveType.REAL:      "Float"
			case PrimitiveType.BOOLEAN:   "Boolean"
			case PrimitiveType.TIMESTAMP: "String"
			case PrimitiveType.FILE:      "FileReference"
			default:                      "String"
		}
	}
	
	dispatch def getJavaType(info.scce.dime.graphql.api.api.PrimitiveType type) {
		return switch (type) {
			case info.scce.dime.graphql.api.api.PrimitiveType.TEXT:      "String"
			case info.scce.dime.graphql.api.api.PrimitiveType.INTEGER:   "Long"
			case info.scce.dime.graphql.api.api.PrimitiveType.REAL:      "Float"
			case info.scce.dime.graphql.api.api.PrimitiveType.BOOLEAN:   "Boolean"
			case info.scce.dime.graphql.api.api.PrimitiveType.TIMESTAMP: "String"
			case info.scce.dime.graphql.api.api.PrimitiveType.FILE:      "FileReference"
			default:                                                     "String"
		}
	}
		
	def primitiveToSupportedType(String datatype) {
		return switch (datatype){
			case "File":    "FileReference"
			case "Text":    "String"
			case "Real":    "Float"
			case "Boolean": "Boolean"
			case "Integer": "Long"
			default:        "String"
		}
	}
	
}