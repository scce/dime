/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.graphql

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.graphql.api.api.APIinputPort
import info.scce.dime.graphql.api.api.Branch
import info.scce.dime.graphql.api.api.ComplexAPIinput
import info.scce.dime.graphql.api.api.ErrorSIB
import info.scce.dime.graphql.api.api.OutputPort
import info.scce.dime.graphql.api.api.PrimitiveAPIinput
import info.scce.dime.graphql.api.api.PrimitiveType
import info.scce.dime.graphql.schema.graphql.APISIB
import info.scce.dime.graphql.schema.graphql.GraphQL
import info.scce.dime.process.process.Process
import java.nio.file.Path
import java.util.List
import java.util.Map

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.pkgEscape

/**
 * Generates resolver classes for queries and mutations based on APISIBs referenced
 * in the graphql schema file.
 */
class QueryAndMutationGenerator extends CincoRuntimeBaseClass {
	
	protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	protected extension GraphQLExtension = new GraphQLExtension
	protected extension DataExtension = DataExtension.instance
	protected extension DimeIOExtension = new DimeIOExtension
	
	val GraphQL model
	val Path gqlFolder
	
	var Map<Type, List<APISIB>> complexTypeMap = newHashMap
	var Map<PrimitiveType, List<APISIB>> primitiveTypeMap = newHashMap
	
	new (GraphQL model, Path gqlFolder) {
		this.model = model
		this.gqlFolder = gqlFolder
	}
	
	def void generate() {
		gqlFolder
			.createDirectories()
			.resolve("AbstractQuery.java")
			.writeString(abstractQueryClass)
		generateClassesForQueries()
		println("Generate Mutation classes")
		generateClassesForMutations()
	}

	def void getTypeMaps(List<APISIB> apiSIBs) {
		complexTypeMap = newHashMap
		primitiveTypeMap = newHashMap
		for (apisib: apiSIBs) {
			val endSIB = apisib.model.endSIBs.head
			val cIPorts = endSIB.complexAPIoutputs
			if (!cIPorts.nullOrEmpty) {
				val dataType = cIPorts?.head?.dataType as Type
				if (complexTypeMap.get(dataType).nullOrEmpty) {
					complexTypeMap.put(dataType, newArrayList(apisib))
				}
				else {
					complexTypeMap.get(dataType).add(apisib)
				}
			}
			val pIPorts = endSIB.primitiveAPIoutputs
			if (!pIPorts.nullOrEmpty) {
				val dataType = pIPorts?.head?.getDataType()
				if (primitiveTypeMap.get(dataType).nullOrEmpty) {
					primitiveTypeMap.put(dataType, newArrayList(apisib))
				}
				else {
					primitiveTypeMap.get(dataType).add(apisib)
				}
			}
		}
	}

	def void generateClassesForQueries() {
		val queryContainer = model.queriess.head
		val apiSIBs = queryContainer.APISIBs
		getTypeMaps(apiSIBs)
		for (complexType: complexTypeMap.keySet) {
			generateForType(complexType)
		}
		for (primitiveType: primitiveTypeMap.keySet) {
			generateForType(primitiveType)
		}
	}

	def void generateClassesForMutations() {
		val queryContainer = model.mutationss.head
		val apiSIBs = queryContainer.APISIBs
		getTypeMaps(apiSIBs)
		for (complexType: complexTypeMap.keySet) {
			generateForMutation(complexType)
		}
		for (primitiveType: primitiveTypeMap.keySet) {
			generateForMutation(primitiveType)
		}
	}

	def void generateForMutation(PrimitiveType primitiveType) {
		val name = '''«primitiveType.getName»Mutations'''
		val contents = primitiveType.generateClass(name)
		gqlFolder
			.resolve('''«name».java''')
			.writeString(contents)
	}

	def void generateForMutation(Type complexType) {
		val name = '''«complexType.name»Mutations'''
		val contents = complexType.generateClass(name)
		gqlFolder
			.resolve('''«name».java''')
			.writeString(contents)
	}

	def void generateForType(PrimitiveType primitiveType) {
		val name = '''«primitiveType.getName»Queries'''
		val contents = primitiveType.generateClass(name)
		gqlFolder
			.resolve('''«name».java''')
			.writeString(contents)
	}

	def void generateForType(Type complexType) {
		val name = '''«complexType.name»Queries'''
		val contents = complexType.generateClass(name)
		gqlFolder
			.resolve('''«name».java''')
			.writeString(contents)
	}

	def generateClass(Type complexType, String name) '''
		package info.scce.dime.graphql;
		
		import graphql.schema.DataFetcher;
		import graphql.schema.DataFetchingEnvironment;
		
		import javax.inject.Inject;
		import javax.persistence.criteria.CriteriaBuilder;
		import java.util.List;
		import java.util.Map;
		import java.util.function.Function;
		import graphql.GraphQLException;
		import java.util.stream.Collectors;
		
		«FOR subType: complexType.knownSubTypes»
			import de.ls5.dywa.generated.entity.«subType.localPkgWithFilename.pkgEscape».«subType.name»;
		«ENDFOR»
		import de.ls5.dywa.generated.rest.types.*;
		
		@javax.transaction.Transactional
		@javax.enterprise.context.RequestScoped
		public class «name» extends AbstractQuery {
			
			«FOR apisib: complexTypeMap.get(complexType)»
				@Inject
				private info.scce.dime.process.«apisib.processPackage» «apisib.model.modelName+"_process"»;
			«ENDFOR»
			
			public «name»(){
				super();
				createDataFetchers();
			}
			
			@Override
			protected void createDataFetchers() {
				«FOR apisib: complexTypeMap.get(complexType)»
					addDataFetcher("«apisib.model.modelName»", new DataFetcher<«apisib.getTypeString(true)»>() {
						@Override
						public «apisib.getTypeString(true)» get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
							// DataFetcher for «apisib.model.modelName»
							
							«IF apisib.authenticated» if(!org.apache.shiro.SecurityUtils.getSubject().isAuthenticated()) {
								throw new DimeException("Please login");
							}
							«ENDIF»
														
							info.scce.dime.process.«apisib.processPackage».«(apisib.model.processSIBs.head.proMod as Process).modelName.toFirstUpper»«(apisib.model.processSIBs.head.proMod as Process).id.replace("-", "__HYPHEN_MINUS__")»Result result =  «apisib.model.modelName+"_process"».execute(
								«apisib.authenticated»
								«apisib.argumentsForProcess»
							);
							
							String returnBranchName = result.getBranchName();
							
							if (returnBranchName.equals("«apisib.apiReturnBranchName»")) {
								«IF apisib.model.endSIBs.head.complexAPIoutputs.head.isList»
									return result.get«apisib.apiReturnBranchName.toFirstUpper»Return().«apisib.returnMethod».stream()
										.map(r -> {
											«IF complexType.hasSubTypes»
												«FOR subType: complexType.knownSubTypes.reverse»
												if (r instanceof «subType.name.toFirstUpper») {
													return new info.scce.dime.graphql.wrapper.«complexType.name»((«subType.name.toFirstUpper») r);
												}
												«ENDFOR»
											«ELSE»
											return new info.scce.dime.graphql.wrapper.«complexType.name»(r);
											«ENDIF»
										})
										.collect(Collectors.toList());
								«ELSE»
									«getTypeString(apisib)» returnValue = result.get«apisib.apiReturnBranchName.toFirstUpper»Return().«apisib.returnMethod»;
									«IF complexType.hasSubTypes»
										«FOR subType: complexType.knownSubTypes.reverse»
										if (returnValue instanceof «subType.name») {
											return new info.scce.dime.graphql.wrapper.«subType.name»((«subType.name») returnValue);
										}
										«ENDFOR»
									«ELSE»
									return new info.scce.dime.graphql.wrapper.«complexType.name»(returnValue);
									«ENDIF»
								«ENDIF»
							}
							
							«FOR errorSIB: apisib.model.errorSIBs»
								if (returnBranchName.equals("«errorSIB.errorBranchName»")) {
									throw new DimeException("[«errorSIB.errorCode»] «errorSIB.errorMessage»");
								}
							«ENDFOR»
							
							throw new RuntimeException("Unhandled branch in APISIB");
						}
					});
				«ENDFOR»
			}
		}
	'''
	
	def hasSubTypes(Type type) {
		(type instanceof AbstractType) ||
		(type instanceof ConcreteType && !type.incomingInheritances.nullOrEmpty)
	}
	
	def getTypeString(APISIB apiSIB, boolean isInterface) {
		val complexApiSIBPort = apiSIB.model.endSIBs.head.complexAPIoutputs.head
		val primitiveApiSIBPort = apiSIB.model.endSIBs.head.primitiveAPIoutputs.head
		if (primitiveApiSIBPort !== null) {
			val primitiveType = primitiveApiSIBPort.dataType as PrimitiveType
			if (primitiveApiSIBPort.isList) {
				return '''List <«primitiveType.javaType»>'''
			}
			else {
				return primitiveType.javaType
			}
		}
		if (complexApiSIBPort !== null) {
			val complexType = complexApiSIBPort.dataType as Type
			val name = (if (isInterface) 'I' else '') + complexType.name
			if (complexApiSIBPort.isList) {
				return '''List <info.scce.dime.graphql.wrapper.«name»>'''
			}
			else {
				return '''info.scce.dime.graphql.wrapper.«name»'''
			}
		}
		throw new RuntimeException("APIs must have an output port")
	}
	
	def getTypeString(APISIB apiSIB) {
		val complexApiSIBPort = apiSIB.model.endSIBs.head.complexAPIoutputs.head
		if (complexApiSIBPort !== null) {
			return (complexApiSIBPort.dataType as Type).name
		}
		return ''
	}
		
	def generateClass(PrimitiveType primitiveType, String name) '''
		package info.scce.dime.graphql;
		
		import graphql.schema.DataFetcher;
		import graphql.schema.DataFetchingEnvironment;
		import graphql.GraphQLException;
		import javax.inject.Inject;
		
		@javax.transaction.Transactional
		@javax.enterprise.context.RequestScoped
		public class «name» extends AbstractQuery {
			
			«FOR apisib: primitiveTypeMap.get(primitiveType)»
				@Inject
				private info.scce.dime.process.«getProcessPackage(apisib)» «apisib.model.modelName+"_process"»;
			«ENDFOR»
			
			public «name»() {
				super();
				createDataFetchers();
			}
			
			@Override
			protected void createDataFetchers() {
				«FOR apisib: primitiveTypeMap.get(primitiveType)»
				addDataFetcher("«apisib.model.modelName»", new DataFetcher<«getTypeString(apisib, false)»>() {
						@Override
						public «getTypeString(apisib, false)» get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
							// DataFetcher for «apisib.model.modelName»
							
							«IF apisib.authenticated»
								if (!org.apache.shiro.SecurityUtils.getSubject().isAuthenticated()) {
									throw new DimeException("Please login");
								}
							«ENDIF»
							
							info.scce.dime.process.«apisib.processPackage».«(apisib.model.processSIBs.head.proMod as Process).modelName.toFirstUpper»«(apisib.model.processSIBs.head.proMod as Process).id.replace("-", "__HYPHEN_MINUS__")»Result result =  «apisib.model.modelName+"_process"».execute(
								«apisib.authenticated»
								«apisib.argumentsForProcess»
							);
							
							String returnBranchName = result.getBranchName();
							
							if (returnBranchName.equals("«apisib.apiReturnBranchName»")) {
								return result.get«apisib.apiReturnBranchName.toFirstUpper»Return().«apisib.returnMethod»;
							}
							
							«FOR errorSIB: apisib.model.errorSIBs»
								if (returnBranchName.equals("«errorSIB.errorBranchName»")) {
									throw new DimeException("[«errorSIB.errorCode»] «errorSIB.errorMessage»");
								}
							«ENDFOR»
							
							throw new RuntimeException("Unhandled branch in APISIB");
						}
					});
				«ENDFOR»
			}
		}
	'''
	
	def getApiReturnBranchName(APISIB apisib) {
		val branch = apisib.model.endSIBs.head.incomingControlFlows.head.sourceElement as Branch
		return branch.name
	}
	
	def getErrorBranchName(ErrorSIB errorSIB) {
		val branch = errorSIB.incomingControlFlows.head.sourceElement as Branch
		return branch.name
	}
	
	def getArgumentsForProcess(APISIB apisib) {
		val processModel = apisib.model.processSIBs.head
		val inputsOfProcessSIB = processModel.inputPorts.sortBy[name]
		return '''
			«FOR inputPort: inputsOfProcessSIB BEFORE "," SEPARATOR ","»
				«IF !inputPort.incomingDataFlows.isNullOrEmpty»
					«val outputPortOfStartSib = inputPort.incomingDataFlows.head.sourceElement as APIinputPort»
					«IF outputPortOfStartSib instanceof PrimitiveAPIinput»
						dataFetchingEnvironment.getArgument("«outputPortOfStartSib.name»")
					«ELSEIF outputPortOfStartSib instanceof ComplexAPIinput»
						info.scce.dime.graphql.wrapper.«outputPortOfStartSib.dataType.name».toEntity(dataFetchingEnvironment.getArgument("«outputPortOfStartSib.name»"))
					«ENDIF»
				«ELSE»
					null
				«ENDIF»
			«ENDFOR»
		'''
	}

	def getReturnMethod(APISIB apisib) {
		val endSIB = apisib.model.endSIBs.head
		for (outputPort : endSIB.APIoutputPorts) {
			if (!outputPort.incomingDataFlows.isNullOrEmpty) {
				val branchName = (outputPort.incomingDataFlows.head.sourceElement as OutputPort).name
				return '''get«branchName.replace(" ", "_").toFirstUpper»()'''
			}
		}
	}

	// Sets the name of the package in APISIB
	def getProcessPackage(APISIB apisib) {
		val process = apisib
			.model
			.processSIBs
			.head
			.proMod
		val packageName = process
			.projectPath
			.relativize(process.toNIOPath.parent)
			.map[toString.toLowerCase]
			.join(".")
		return '''«packageName».«process.modelName.toFirstUpper»«process.id»'''
			.toString
			.replace("-", "__HYPHEN_MINUS__")
	}

	def getAbstractQueryClass() '''
		package info.scce.dime.graphql;
		
		import graphql.schema.DataFetcher;
		
		import java.util.Collection;
		import java.util.HashMap;
		import java.util.Map;
		import java.util.Set;
		import java.util.function.Function;
		
		public abstract class AbstractQuery {
			private Map<String, DataFetcher> dataFetchers;
			
			AbstractQuery() {
				this.dataFetchers = new HashMap<String, DataFetcher>();
			}
			
			abstract void createDataFetchers();
			
			protected void addDataFetcher(String name, DataFetcher datafetcher) {
				this.dataFetchers.put(name, datafetcher);
			}
			
			public Map<String, DataFetcher> getDataFetchers() {
				return dataFetchers;
			}
		}
	'''
}
