/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.graphql

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserType
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.graphql.Generator.ForbiddenTypeObject
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.graphql.api.api.APIinputPort
import info.scce.dime.graphql.api.api.ComplexAPIinput
import info.scce.dime.graphql.api.api.EndSIB
import info.scce.dime.graphql.api.api.PrimitiveAPIinput
import info.scce.dime.graphql.schema.graphql.APISIB
import info.scce.dime.graphql.schema.graphql.GraphQL
import java.util.List

class SchemaGenerator extends CincoRuntimeBaseClass {
	
	public static String SCHEMA_FILE_NAME = "schema.graphql"
	
	protected extension DataExtension = DataExtension.instance
	protected extension DimeIOExtension = new DimeIOExtension
	
	val GraphQL model
	val List<Type> types
	val List<ForbiddenTypeObject> forbiddenList
	val List<APISIB> queryApiSibs
	val List<APISIB> mutationApiSibs
	
	new (GraphQL model, List<Type> types, List<ForbiddenTypeObject> forbiddenList, List<APISIB> queryApiSibs, List<APISIB> mutationApiSibs) {
		this.model = model
		this.forbiddenList = forbiddenList
		this.queryApiSibs = queryApiSibs
		this.mutationApiSibs = mutationApiSibs
		
		// Filter duplicate types from referenced types
		this.types = newArrayList
		for (type: types) {
			val concreteType = if (type instanceof ReferencedType) type.referencedType else type
			if (this.types.forall[ t | t.name != concreteType.name ]) {
				this.types.add(concreteType)
			}
		}
	}
	
	def generate() {
		model
			.projectPath
			.resolve("target","dywa-app","app-business","src","main","resources")
			.createDirectories()
			.resolve(SCHEMA_FILE_NAME)
			.writeString(template())
	}
		
	private def template() '''			
		type FileReference {
			dywaId: Int!
			fileName: String!
			contentType: String!
			createdAt: String!
		}
		
		«FOR type:types.filter[it instanceof EnumType]»
			enum «type.name» {
				«FOR literal:type.enumLiterals»
					«literal.name.formatEnumLiteral»
				«ENDFOR»
			}
			
		«ENDFOR»
		
		«FOR type: types.filter[it instanceof AbstractType]»
			interface «type.name» {
				dywaId: Int
				dywaType: String
				«attributesTemplate(type, forbiddenList)»
			}
			
		«ENDFOR»
		
		«FOR type: types.filter[isInput(model, it, forbiddenList)]»
			input «type.name» {
				«attributesTemplate(type, forbiddenList)»
			}
			
		«ENDFOR»
		
		«FOR type: types.filter[!isInput(model, it, forbiddenList)].filter[it instanceof ConcreteType || it instanceof ReferencedType || it instanceof UserType]»
			type «type.name» «implementsTemplate(type)» {
				dywaId: Int
				dywaType: String
				«attributesTemplate(type, forbiddenList)»
			}
			
		«ENDFOR»
		
		«FOR type: types.filter[it instanceof ConcreteType]»
			«IF !type.incomingInheritances.nullOrEmpty»
				union «type.name.toFirstUpper»Union = «FOR subType: type.knownSubTypes SEPARATOR ' | '»«subType.name»«ENDFOR»
				
			«ENDIF»	
		«ENDFOR»
		
		type Query {
			«FOR queryApiSib: queryApiSibs»
				«buildQueryString(queryApiSib.model.modelName, queryApiSib.model.startSIBs.get(0).APIinputPorts, queryApiSib.model.endSIBs.get(0))»
			«ENDFOR»
		}
		
		«IF !mutationApiSibs.empty»
		type Mutation {
			«FOR mutationApiSib: mutationApiSibs»
				«buildQueryString(mutationApiSib.model.modelName, mutationApiSib.model.startSIBs.get(0).APIinputPorts, mutationApiSib.model.endSIBs.get(0))»
			«ENDFOR»
		}
		«ENDIF»
	'''
	
	private def isInput(GraphQL model, Type type, List<ForbiddenTypeObject> forbiddenList) {		
		val schemaTypes = newArrayList
		model.forbiddenTypess.map[it.types].forEach[schemaTypes.addAll(it)]		
		val schemaType = schemaTypes.findFirst[it.name == type.name]
		return schemaType !== null ? schemaType.input : false
	}
	
	private def implementsTemplate(Type type) {
		if (type.outgoingInheritances.empty) {
			return ''
		} else {
			val interfaceNames = type.outgoingInheritances
				.filter[it.targetElement instanceof AbstractType]
				.map[it.targetElement.name]
			if (interfaceNames.empty) {
				return ''
			} else {
				return '''implements «FOR n: interfaceNames SEPARATOR ' & '»«n»«ENDFOR»'''
			}
		}
	}
	
	private def String buildQueryString(String modelName, List<APIinputPort> outports, EndSIB end) '''
		«modelName» «IF !outports.empty»(
			«FOR output : outports SEPARATOR ', '»
				«IF output instanceof PrimitiveAPIinput»
					«output.name»: «primitiveToSupportedType(output.dataType.toString(), false)»«IF output.required»!«ENDIF»
				«ELSEIF output instanceof ComplexAPIinput»
					«output.name»: «output.dataType.name»«IF output.required»!«ENDIF»
				«ENDIF»
			«ENDFOR»
		)«ENDIF»: «IF end.primitiveAPIoutputs.size() > 0»
				«convertIsList(end.primitiveAPIoutputs.get(0).dataType.toString(), end.primitiveAPIoutputs.get(0).isIsList, false)»«IF end.primitiveAPIoutputs.get(0).isNotNull»!«ENDIF»
			«ELSE»
				«{
					var dataType = end.complexAPIoutputs.get(0).dataType
					var name = dataType.name + (if (dataType.incomingInheritances.nullOrEmpty) '' else 'Union')
					'''«convertIsList(name, end.complexAPIoutputs.get(0).isIsList, true)»«IF end.complexAPIoutputs.get(0).isNotNull»!«ENDIF»'''
				}»
			«ENDIF»
	'''
	
	private def String primitiveToSupportedType(String datatype, boolean isComplexType) {
		if (isComplexType) {
			return datatype
		}
		return switch (datatype) {
			case "File":      "FileReference"
			case "Text":      "String"
			case "Boolean":   "Boolean"
			case "Real":      "Float"
			case "Integer":   "Int"
			case "Timestamp": "String"
			default:          "String"
		}
	}
	
	private def formatEnumLiteral(String literal) {
		return literal.replaceAll(" ", "_").replaceAll("-", "__HYPHEN_MINUS__")
	}
	
	private def attributesTemplate(Type type, List<ForbiddenTypeObject> forbiddenList) {				
		return '''
			«FOR complexAttribute: type.inheritedAttributes.filter[it instanceof ComplexAttribute].map[it as ComplexAttribute]»
				«IF !attributeHidden(type.id, complexAttribute.id, forbiddenList)»
					«IF complexAttribute.dataType instanceof EnumType»
						«complexAttribute.name»_«complexAttribute.dataType.name.toFirstUpper»: «complexAttribute.dataType.name.toFirstUpper»
					«ELSE»
						«{
							var Type concreteType;
							if (complexAttribute.dataType instanceof ReferencedType) {
								concreteType = (complexAttribute.dataType as ReferencedType).referencedType
							} else {
								concreteType = complexAttribute.dataType
							}
							
							if (concreteType instanceof ConcreteType && !concreteType.incomingInheritances.nullOrEmpty) {
								'''
								«complexAttribute.name»_«concreteType.name.toFirstUpper»: «convertIsList('''«concreteType.name»Union''', complexAttribute.isIsList, true)»
								'''	
							} else {
								'''
								«complexAttribute.name»_«concreteType.name.toFirstUpper»: «convertIsList(concreteType.name, complexAttribute.isIsList, true)»
								'''	
							}
						}»
					«ENDIF»
				«ENDIF»
			«ENDFOR»
			«FOR primitiveAttribute: type.inheritedAttributes.filter[it instanceof PrimitiveAttribute].map[it as PrimitiveAttribute]»
				«IF !attributeHidden(type.id, primitiveAttribute.id, forbiddenList)»
					«primitiveAttribute.name»: «convertIsList(primitiveAttribute.dataType.literal, primitiveAttribute.isIsList, false)»
				«ENDIF»
			«ENDFOR»
		'''
	}

	private def String convertIsList(String datatype, boolean isList, boolean isComplexType){
		if (isList) {
			return '''[«primitiveToSupportedType(datatype, isComplexType)»]'''
		} else{
			return primitiveToSupportedType(datatype, isComplexType)
		}
	}
		
	private def boolean attributeHidden(String typeID, String attributeID, List<ForbiddenTypeObject> forbiddenList){
		var index = checkIndex(typeID, forbiddenList)
		return if (index == -1) false else forbiddenList.get(index).attributeIDs.contains(attributeID)
	}
	
	private def int checkIndex(String typeID, List<ForbiddenTypeObject> forbiddenList) {
		for (var i = 0; i < forbiddenList.size; i++) {
			if (forbiddenList.get(i).typeID == typeID) {
				return i
			}
		}
		return -1
	}
}
