/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.data

import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.BidirectionalAttribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumLiteral
import info.scce.dime.data.data.Inheritance
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserAttribute
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.util.DyWAExtension
import java.util.HashSet

import static extension info.scce.dime.generator.gui.rest.model.TypeViewUtils.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.ReferencedBidirectionalAttribute
import info.scce.dime.data.data.ReferencedComplexAttribute
import info.scce.dime.data.data.ReferencedExtensionAttribute

/**
 * The dart data generator is used to generate the dart class files
 * for every type of all data models referenced in the current DAD model
 */
class DartDataGenerator {
	extension DataExtension = DataExtension.instance
	extension DyWAExtension = new DyWAExtension
	
	
	/**
	 * Returns the escaped name of the given data model
	 */
	static def getName(Data data){
		return data.modelName.escapeDart;
	}
	
	def referencedDataModels(Data d) {
		(d.referencedTypes.map[originalType] + d.referencedEnumTypes.map[originalType]).map[rootElement].toSet
	}
	
	/**
	 * Generates dart classes for each type
	 * present in the given data model
	 */
	def create(Data data)
	{
		var p = '''de.ls5.dywa.generated.rest.types.''';
	return '''
	import 'dart:core' as core;
	import 'dart:convert';
	import '../models/FileReference.dart';
	import '../models/Selectives.dart';
	import '../commons/Exceptions.dart';
	«FOR importedmodel:data.referencedDataModels»
	import '«importedmodel.name».dart' as «importedmodel.name»;
	«ENDFOR»
	
	class «data.name.escapeDart.toFirstUpper»CastUtil {
		
		const «data.name.escapeDart.toFirstUpper»CastUtil();
		«FOR type : data.types.filter[!isReferencedType] SEPARATOR "\n"»
		«val typeName = '''«IF type.isReferencedType»«type.referencedDataModel.name».«ENDIF»«type.name.escapeDart.toFirstUpper»'''»
		static «typeName» castTo«type.name.escapeDart.toFirstUpper»(core.Object o) => o as «typeName»;
		static DIMEList<«typeName»> castToList«type.name.escapeDart.toFirstUpper»(DIMEList<core.Object> os) => new DIMEList.from(os.cast<«typeName»>());
		static DIMEList<«typeName»> newList«type.name.escapeDart.toFirstUpper»() => new DIMEList<«typeName»>();
		«ENDFOR»
	}
	«FOR type : data.types.filter[!isReferencedType] SEPARATOR "\n"»
		«createConcreteType(type,p)»
	«ENDFOR»
	'''
	
	}
	
	def typeName(Type t) {
		if(t.isReferencedType){
			return '''«t.referencedDataModel.name».«t.name.escapeDart»'''
		}
		t.name.escapeDart
	}
	
	/**
	 * Generates a Dart class for the given type
	 * which has a corresponding Java class implementation
	 * on the FQN given by p
	 */
	def createConcreteType(Type ct,String p)
	{
		val g = ct.rootElement
		val sortedAttributes = new HashSet;
		for(attribute:ct.inheritedAttributes.filter[n|!(n instanceof EnumLiteral)]){
			val field = new DartDataField
			field.attribute = attribute
			field.attrname = attribute.attrName.escapeDart
			field.restname = attribute.name.escapeString
			sortedAttributes+=field
		}
	'''
	«IF ct.isAbstract»abstract «ENDIF»class «ct.name.escapeDart.toFirstUpper» extends BaseModel«IF ct.hasSuperTypes» implements «ct.superTypes.map[typeName].toList.join(" ,")»«ENDIF»
	{
		«FOR attribute:sortedAttributes.sortBy[restname]»
			core.bool _«attribute.attrname»HasBeenSet = false;
			core.bool «attribute.attrname»HasBeenChanged() => _«attribute.attrname»HasBeenSet«IF attribute.attribute.isComplex && !attribute.attribute.isIsList»||(_«attribute.attrname»==null?false:_«attribute.attrname».hasChanged()) «ENDIF»;
			«attribute.attribute.getType(true,g)» _«attribute.attrname»;
		«ENDFOR»
		
		static «IF !ct.isAbstract»«ct.name.escapeDart.toFirstUpper»«ELSE»ParameterBaseModel«ENDIF» fromId(core.int id) {
			«IF ct.isAbstract»
			var m = new ParameterBaseModel();
			m.dywaRuntimeType = "«ct.RESTTOImplName»";
			«ELSE»
			var m = new «ct.name.escapeDart.toFirstUpper»();
			«ENDIF»
			m.dywa_id = id;
			return m;
		}
		
		«IF !ct.isAbstract»
		
		// reflection methods to read and wirte dart properties by string name
		
		core.dynamic $$getProperty(core.String prop) {
		  var propMap = {
		  «FOR attribute:sortedAttributes.sortBy[restname] SEPARATOR ', '»
		    '«attribute.attrname»': this.«attribute.attrname»
		  «ENDFOR»
		  };
		
		  if (!propMap.containsKey(prop)) throw NoSuchPropertyException(prop);
		  return propMap[prop];
		}
		
		void $$setProperty(core.String prop, core.dynamic value) {
		  switch (prop) {
		  «FOR attribute:sortedAttributes.sortBy[restname]»
		    case '«attribute.attrname»': this.«attribute.attrname» = value; return;
		  «ENDFOR»
		    default: throw NoSuchPropertyException(prop);
		  }
		}
		
		core.bool $$hasProperty(core.String prop) {
		  try {
		    $$getProperty(prop);
		    return true;
		  } on NoSuchPropertyException catch (e) {
		    return false;
		  }
		}
		
		«ct.name.escapeDart.toFirstUpper»({core.Map<core.String,core.dynamic> cache, jsog}) {
			if (cache == null) {
				cache = new core.Map();
			}

			// default constructor
			if (jsog == null) {
				
				this.dywa_id = -1;
				this.dywa_version = 0;
				this.dywa_name = null;
				
				// properties
				«FOR attribute:sortedAttributes.sortBy[restname]»
					«IF attribute.attribute.isList»
						this._«attribute.attrname» = new DIMEList.monitored(«attribute.attrname»HasBeenSetted);
«««						this._orig_«attribute.attrName.escapeDart» = new List();
					«ELSEIF attribute.attribute.isPrimitive»
						this._«attribute.attrname» = «attribute.attribute.init»;
					«ENDIF»
				«ENDFOR»
			}
			// from jsog
			else {
				core.String jsogId = jsog['@id'];
				cache[jsogId] = this;
				
				this.dywa_id = jsog['dywaId'];
				this.dywa_version = jsog['dywaVersion'];
				this.dywa_name = jsog['dywaName'];
				
				// properties
				«FOR attribute:sortedAttributes.sortBy[restname]»
					«IF attribute.attribute.isList»
						this._«attribute.attrname» = new DIMEList.monitored(«attribute.attrname»HasBeenSetted);
					«ENDIF»
					«attribute.deserialize(g)»
					
					«IF attribute.attribute.hasSuperAttribute»
					«attribute.deserialize(g)»
					«ENDIF»
					
					«IF attribute.attribute.isPrimitive && !attribute.attribute.isList»
					else{
						this._«attribute.attrname» = «attribute.attribute.init»;
					}
					«ENDIF»
					_«attribute.attrname»HasBeenSet = false;
				«ENDFOR»
			}
		}
		«ENDIF»
		
		«FOR attribute:sortedAttributes.sortBy[restname]»
			void «attribute.attrname»HasBeenSetted() { _«attribute.attrname»HasBeenSet=true; }
		«ENDFOR»
		
		core.bool hasChanged() {
			return «IF sortedAttributes.empty»false
			«ELSE»«FOR attribute:sortedAttributes.sortBy[restname] SEPARATOR " || "»
				_«attribute.attrname»HasBeenSet
			«ENDFOR»«ENDIF»;
		}
		
		«IF ct instanceof EnumType»
		core.String toString() {
			switch(this.dywa_name) {
				«FOR literal:ct.enumLiterals»
				case '«literal.name.escapeJava»':
					«IF literal.displayName === null || literal.displayName == ''»
						return "«literal.name»";
					«ELSE»
						return "«literal.displayName»";
					«ENDIF»
				«ENDFOR»
			}
	    	return this.dywa_name;
	  	}
		«ENDIF»
		
		core.Map<core.String,core.dynamic> toJSOG(core.Map<core.Object,core.dynamic> objects) {
			if (objects == null) {
				objects = new core.Map();
			}
			core.int jsogId;
			core.Map<core.String,core.dynamic> jsonObj = new core.Map();
			if(objects.containsKey(this)) {
				jsogId = objects[this]['id'];
				jsonObj = objects[this]['value'];
				«serialize(ct,false)»
				return { 
					'@ref': jsogId.toString()
				};
			}
			else {
				jsogId = objects.length;
				var pair = {
					'id': jsogId,
					'value': jsonObj
				};
				objects[this] = pair;
			}
			jsonObj['@id'] = jsogId.toString();
			jsonObj['dywaRuntimeType'] = "«ct.RESTTOImplName»";

			jsonObj['dywaId'] = this.dywa_id;
			jsonObj['dywaVersion'] = this.dywa_version;
			if(this.dywa_name != null) {
				jsonObj['dywaName'] = this.dywa_name;
			}
			«serialize(ct,true)»
			return jsonObj;
		}
		«IF !ct.isAbstract»
		static fromJSON(core.String json) {
			return fromJSOG(cache:new core.Map<core.String,core.dynamic>(),jsog:jsonDecode(json));
		}
		«ENDIF»
			
		static «ct.name.escapeDart.toFirstUpper» fromJSOG({core.Map<core.String,core.dynamic> cache, jsog}) {
			if(jsog.containsKey('@ref')) {
				assert (cache.containsKey(jsog['@ref']));
				return cache[jsog['@ref']];
			}
			«FOR type: ct.knownSubTypes.filter[!isAbstract]»
			if(jsog['dywaRuntimeType'] == '«type.RESTTOImplName»') {
				return new «type.name.escapeDart.toFirstUpper»(cache:cache,jsog:jsog);
			}
			«ENDFOR»
			«IF ct.isAbstract»
			throw new core.Exception("«ct.name» is abstract and cannot be initiated by ${jsog['dywaRuntimeType']}");
			«ELSE»
			return new «ct.name.escapeDart.toFirstUpper»(cache:cache,jsog:jsog);
			«ENDIF»
		}
		
		«FOR attribute:ct.inheritedAttributes.filter[n|!(n instanceof EnumLiteral)]»
		«IF attribute.hasSuperAttribute && !attribute.superAttr.equals(attribute.name)»
		void set «attribute.superAttr.escapeDart» («attribute.getType(true,g)» value)
		{
			this._«attribute.superAttr.escapeDart»HasBeenSet = true;
			this._«attribute.superAttr.escapeDart» = value;
		}
				
		«attribute.getType(true,g)» get «attribute.superAttr.escapeDart»
		{
			return this._«attribute.superAttr.escapeDart»;
		}
		«ENDIF»
		void set «attribute.name.escapeDart» («attribute.getType(true,g)» value)
		{
			«IF attribute.hasSuperAttribute && !attribute.superAttr.equals(attribute.name)»
			this.«attribute.superAttr.escapeDart» = value;
			«ELSE»
			this._«attribute.name.escapeDart»HasBeenSet = true;
			this._«attribute.name.escapeDart» = value;
			«ENDIF»
		}
		
		«attribute.getType(true,g)» get «attribute.name.escapeDart»
		{
			«IF attribute.hasSuperAttribute && !attribute.superAttr.equals(attribute.name)»
			return this.«attribute.superAttr.escapeDart»;
			«ELSE»
			return this._«attribute.name.escapeDart»;
			«ENDIF»
		}
		
			«attribute.getType(true,g)» initOnDemand«attribute.name.escapeDart»()
			{
				«IF (attribute.isIsList || attribute.isComplex)»
					if(this._«attribute.name.escapeDart» == null) {
					«IF attribute.isIsList»
					this._«attribute.name.escapeDart» = new DIMEList();
					«ELSE»
						«IF (attribute.isComplex) && !attribute.complexDataType.abstract»
							this.«attribute.name.escapeDart» = new «attribute.getType(true,g)»();
						«ENDIF»
					«ENDIF»
					}
				«ENDIF»
				return this.«attribute.name.escapeDart»;
			}
			
			void setValue«attribute.name.escapeDart»(«attribute.getType(true,g)» value)
			{
				this.«attribute.name.escapeDart» = value;
			}
			
			void «attribute.name.escapeDart»setValue(«attribute.getType(true,g)» value)
			{
				this.«attribute.name.escapeDart» = value;
			}
			
			«IF attribute.isIsList»
				void «attribute.name.escapeDart»add(«attribute.getType(false,g)» value)
				{
					this.«attribute.name.escapeDart».add(value);
					this._«attribute.name.escapeDart»HasBeenSet = true;
				}

			«ENDIF»
		
		«ENDFOR»
	}
	'''
	}
	
	def deserialize(DartDataField attribute,Data g)
	'''
	if (jsog.containsKey("«attribute.restname»")) {
		«IF attribute.attribute.isList»
			«IF attribute.attribute.isPrimitive»
			for (core.dynamic jsogObj in jsog["«attribute.restname»"]) {
				if(jsogObj is core.List) {
					jsogObj = jsogObj[1];
				}
				«attribute.attribute.renderCopyFromJSOG(g)»
				this._«attribute.attrname».add(value«attribute.attribute.id.escapeDart»);
			}
			«ELSE»
			for (core.Map<core.String,core.dynamic> iter in jsog["«attribute.restname»"]) {
				core.Map<core.String,core.dynamic> jsogObj = iter;
				«attribute.attribute.renderCopyFromJSOG(g)»
				this._«attribute.attrname».add(value«attribute.attribute.id.escapeDart»);
			}
			«ENDIF»
		«ELSE»
			«IF attribute.attribute.isPrimitive»
				«IF attribute.attribute.primitiveDataType == PrimitiveType.FILE»
					core.Map<core.String,core.dynamic> jsogObj = jsog["«attribute.restname»"];
				«ELSEIF attribute.attribute.primitiveDataType == PrimitiveType.TIMESTAMP»
					«attribute.attribute.getType(true,g)» jsogObj = null;
					if(jsog["«attribute.restname»"]!=null){
						jsogObj = DateConverter.fromJSON(jsog["«attribute.restname»"]);
					}
				«ELSE»
					«attribute.attribute.getType(true,g)» jsogObj = jsog["«attribute.restname»"];
				«ENDIF»
			«ELSE»
			core.Map<core.String,core.dynamic> jsogObj = jsog["«attribute.restname»"];
			«ENDIF»
			if(jsogObj != null) {
				«attribute.attribute.renderCopyFromJSOG(g)»
				this._«attribute.attrname» = value«attribute.attribute.id.escapeDart»;
			}
			«IF attribute.attribute.isPrimitive && !attribute.attribute.isList»
			else {
				this._«attribute.attrname» = «attribute.attribute.init»;
			}
			«ENDIF»
		«ENDIF»
	}
	'''
	
	def String attrName(Attribute attribute){
		if(attribute.hasSuperAttribute)return attribute.superAttr;
		return attribute.name;
	}
	
	def String getSuperAttr(Attribute attribute){
		if(attribute instanceof PrimitiveAttribute){
			return attribute.superAttr.name;
		}
		if(attribute instanceof ComplexAttribute){
			return attribute.superAttr.name;
		}
		return "";
	}
	
	def boolean getHasSuperAttribute(Attribute attribute){
		if(attribute instanceof PrimitiveAttribute){
			return attribute.superAttr !== null;
		}
		if(attribute instanceof ComplexAttribute){
			return attribute.superAttr !== null;
		}
		return false;
	}
	
	def boolean getHasSuperTypes(Type type){
		return !type.getOutgoing(Inheritance).empty
	}
	
	/**
	 * Checks, if a given attribute is of the primitive type Timestamp
	 * which can be used to represent any time or date value
	 */
	def boolean getIsTimeStamp(Attribute attribute){
		if(attribute.isPrimitive){
			return attribute.primitiveDataType==PrimitiveType.TIMESTAMP;
		}
		return false;
	}
	
	/**
	 * Checks, if the given attribute is either complex or a
	 * primitive FILE attribute which leads to a specific
	 * handling since the file values are objects as well
	 */
	def isEncoding(Attribute attribute) {
		if(attribute.isComplex)return true;
		if(attribute.isPrimitive){
			return attribute.primitiveDataType==PrimitiveType.FILE
		}
		return false;
	}
	
	/**
	 * Generates the JSON serialization for the given type.
	 * Every attribute is serialized to a JSON entry.
	 * Complex attributes are serialized inductive.
	 * To detect circles, a cache is provided and checked on every inductive step
	 */
	def serialize(Type ct,boolean onlyExtend)
	'''
	«FOR attribute:ct.inheritedAttributes.filter[n|!(n instanceof EnumLiteral)]»
		if(( «attribute.attrName.escapeDart»HasBeenChanged() && !jsonObj.containsKey('«attribute.name.escapeString»'))«IF !attribute.isIsList» && !objects.containsKey(this._«attribute.attrName.escapeDart»)«ENDIF»«IF onlyExtend» || this.dywa_id == 0«ENDIF») {
		«IF attribute.isList»
			if(this._«attribute.attrName.escapeDart».isEmpty){
				jsonObj["«attribute.name.escapeString»"] = [];
			}
			else{
				jsonObj["«attribute.name.escapeString»"] = this._«attribute.attrName.escapeDart»«IF isEncoding(attribute)»«IF attribute.isComplex».where((n) => objects.containsKey(n))«ENDIF».map((n)=>n.toJSOG(objects)).toList()«ENDIF»;
			}
	 	«ELSE»
		 	«IF isEncoding(attribute)»
		 		if(this._«attribute.attrName.escapeDart» != null) {
		 			«IF attribute.isComplex && ct.isTypeOf(attribute.complexDataType)»
		 				if (this._«attribute.attrName.escapeDart» == this) {
		 					jsonObj["«attribute.name.escapeString»"] = { '@ref': jsogId.toString() };
		 				} else {
		 					jsonObj["«attribute.name.escapeString»"] = this._«attribute.attrName.escapeDart».toJSOG(objects);
		 				}
		 			«ELSE»
		 				jsonObj["«attribute.name.escapeString»"] = this._«attribute.attrName.escapeDart».toJSOG(objects);
		 			«ENDIF»
		 		}
		 		else {
		 			jsonObj["«attribute.name.escapeString»"] = null;
		 		}
		 	«ELSE»
		 		«IF attribute.isTimeStamp»
		 		if(this.«attribute.name.escapeDart» != null){
		 			jsonObj["«attribute.name.escapeString»"] = DateConverter.toJSON(this._«attribute.attrName.escapeDart»);
		 		}
		 		«ELSE»
		 		jsonObj["«attribute.name.escapeString»"] = this._«attribute.attrName.escapeDart»;
		 		«ENDIF»
		 	«ENDIF»
		«ENDIF»
		}
	«ENDFOR»
	'''
	
	/**
	 * Returns the initial, default value for the given primitive attribute
	 * depended on the primitive data type
	 */
	def String getInit(Attribute attribute){
		if(attribute.isIsList) return "[]";
		switch(attribute.primitiveDataType)
			{
				case BOOLEAN: return "false"
				case TEXT: return "null"
				case FILE: return "null"
				case INTEGER: return "0"
				case REAL: return "0.0"
				case TIMESTAMP: return "null"
			}
	}
	
	/**
	 * Returns the declaration Dart data type for the given attribute.
	 * The consider list option decides, if the attribute list status is
	 * considered
	 */
	def String getType(Attribute attribute,boolean considerList,Data d){
		var result = "";
		var prefix = ""
		if(!attribute.rootElement.equals(d)) {
			prefix = attribute.rootElement.modelName.escapeDart+".";
		}
		if(attribute.isIsList && considerList) result += "DIMEList<";
		if(attribute instanceof UserAttribute){
			result += prefix+(attribute as UserAttribute).dataType.typeName.toFirstUpper;
		}
		else if(attribute instanceof ReferencedBidirectionalAttribute){
			val t = (attribute as ReferencedBidirectionalAttribute)
			result += t.dataType.rootElement.modelName.escapeDart + "." + t.dataType.typeName.toFirstUpper;
			
		}
		else if(attribute instanceof BidirectionalAttribute){
			result += prefix+(attribute as BidirectionalAttribute).dataType.typeName.toFirstUpper;
		}
		else if(attribute instanceof ReferencedComplexAttribute){
			val t = (attribute as ReferencedComplexAttribute)
			result += t.dataType.rootElement.modelName.escapeDart + "." + t.dataType.typeName.toFirstUpper;
			
		}
		else if(attribute instanceof ComplexAttribute){
			result += prefix+(attribute as ComplexAttribute).dataType.typeName.toFirstUpper;
		}
		else if(attribute instanceof ReferencedExtensionAttribute){
			val ea = (attribute as ReferencedExtensionAttribute)
			val complexEA = ea.complexExtensionAttributeType
			if(complexEA !== null) {
				result += complexEA.rootElement.modelName.escapeDart + "." + complexEA.typeName.toFirstUpper;
			} else {
				result += ea.primitiveExtensionAttributeType.primitiveTypeName;
			}
			
		}
		else if(attribute instanceof ExtensionAttribute){
			val ea = (attribute as ExtensionAttribute)
			val complexEA = ea.complexExtensionAttributeType
			if(complexEA !== null) {
				result += prefix+complexEA.typeName.toFirstUpper;
			} else {
				result += ea.primitiveExtensionAttributeType.primitiveTypeName;
			}
			
		}
		else if(attribute instanceof PrimitiveAttribute){
			result += getPrimitiveTypeName(attribute.dataType)
		}
		if(attribute.isIsList && considerList) result += ">";
		return result;
	}
	
	private def getPrimitiveTypeName(PrimitiveType dataType) {
		return switch(dataType)
			{
				case BOOLEAN:"core.bool"
				case TEXT:"core.String"
				case FILE:"FileReference"
				case INTEGER:"core.int"
				case REAL:"core.double"
				case TIMESTAMP:"core.DateTime"
			}
	}
	
	/**
	 * Generates the deserialization for a given attribute
	 * which parses a JSON object to the attributes of the Dart class.
	 * To resolve references between complex attributes, a cache is provided
	 */
	def renderCopyFromJSOG(Attribute attribute,Data g) '''
		«attribute.getType(false,g)» value«attribute.id.escapeDart»;
		
		«IF attribute.isPrimitive»
			if(jsogObj!=null){
			«switch(attribute.primitiveDataType) {
				case TIMESTAMP: '''value«attribute.id.escapeDart» = DateConverter.fromJSON(jsogObj.toString());'''
				case FILE: '''value«attribute.id.escapeDart» = new FileReference(jsog: jsogObj);'''
				case BOOLEAN: '''value«attribute.id.escapeDart» = jsogObj.toString().toLowerCase()=='true'?true:false;'''
				case INTEGER: '''value«attribute.id.escapeDart» = core.int.parse(jsogObj.toString());'''
				case REAL: '''value«attribute.id.escapeDart» = core.double.parse(jsogObj.toString());'''
				default: '''value«attribute.id.escapeDart» = jsogObj.toString();'''
			}»
			}
		«ELSE»
			core.String jsogId;
			
			if (jsogObj.containsKey('@ref')) {
				jsogId = jsogObj['@ref'];
			}
			else {
			 	jsogId = jsogObj['@id'];
			}
			if (cache.containsKey(jsogId)) {
				value«attribute.id.escapeDart» = cache[jsogId];
			}
			else {
				«attribute.complexDataType.newInstanceDeclaration('''value«attribute.id.escapeDart»''', "cache", "jsogObj",g)»
			}
		«ENDIF»
	'''
	
	def prefix(Type type) {
		return DyWASelectiveDartGenerator.prefix(type)
	}
	
	def newInstanceDeclaration(Type dataType, String varName, String cacheName, String jsogName, Data g) '''
		«val prefixing = dataType.isReferencedType || !dataType.rootElement.equals(g)»
		«val type = dataType.originalType»
		if («jsogName» != null) {
			«FOR subType: type.knownSubTypes.sortTopologically.filter[!isAbstract] SEPARATOR ''' else '''»
				if («jsogName»['dywaRuntimeType'] == "«subType.RESTTOImplName»") {
					«varName» = new «IF prefixing»«subType.prefix».«ENDIF»«subType.name.escapeDart.toFirstUpper»(cache: «cacheName», jsog: «jsogName»);
				}
			«ENDFOR»
			«IF !type.isAbstract»
				else {
					«varName» = new «IF prefixing»«type.prefix».«type.name.escapeDart.toFirstUpper»«ELSE»«type.typeName.toFirstUpper»«ENDIF»(cache: «cacheName»,jsog: «jsogName»);
				}
			«ENDIF»
		}
		«IF !type.isAbstract»
			else {
				«varName» = new «IF prefixing»«type.prefix».«type.name.escapeDart.toFirstUpper»«ELSE»«type.typeName.toFirstUpper»«ENDIF»(cache: «cacheName»,jsog: «jsogName»);
			}
		«ENDIF»
	'''
}
