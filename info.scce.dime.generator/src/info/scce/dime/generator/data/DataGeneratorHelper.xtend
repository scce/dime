/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.data

import info.scce.dime.api.DIMEDataSwitch
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumLiteral
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserType
import info.scce.dime.data.helper.DataExtension

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

public class DataGeneratorHelper extends DIMEDataSwitch<CharSequence> {
	
	override caseAbstractType(AbstractType it) '''
		data.createAbstractType("«name»", "«id»", "«localPkgWithFilename»");
	'''
	
	override caseEnumType(EnumType it) '''
		data.createEnumType("«name»", "«id»", "«localPkgWithFilename»", «enumLiterals.join(", ") [ '''"«name.escapeJava»"''' ]»);
	'''
	
	override caseConcreteType(ConcreteType it) '''
		data.createConcreteType("«name»", "«id»", "«localPkgWithFilename»");
	'''
	
	override caseUserType(UserType it) '''
		data.createConcreteType("«name»", "«id»", "«localPkgWithFilename»");
	'''
	
	def doSwitch(Attribute attr, Type type) {
		switch attr {
			PrimitiveAttribute: attr.caseAttribute(type)
			ComplexAttribute: attr.caseAttribute(type)
		}
	}
	
	def caseAttribute(PrimitiveAttribute attr, Type type) '''
		data.addPrimitiveField("«type.id»", "«attr.id»", "«attr.name.escapeString»", «attr.dyWAType», true);
	'''
	
	def caseAttribute(ComplexAttribute attr, Type type) '''
		data.addComplexField("«type.id»", "«attr.id»", "«attr.name.escapeString»", «attr.dyWAType», "«attr.originalType.id»", true);
	'''
	
	def overridingComplexAttribute(ComplexAttribute attr, Type type) '''
		data.overrideField("«type.id»", "«attr.id»", "«attr.dataType.id»", "«attr.superAttr.originalAttribute.id»", "«attr.name.escapeString»");
	'''
	
	def overridingPrimitiveAttribute(PrimitiveAttribute attr, Type type) '''
		data.overrideField("«type.id»", "«attr.id»", null, "«attr.superAttr.originalAttribute.id»", "«attr.name»");
	'''
	
	static def getFilterOriginalTypes(Data model) {
		model.types.filter[t | !(
			t instanceof ReferencedType
			|| t instanceof ReferencedUserType
			|| t instanceof ReferencedEnumType
		)]
	}
	
	def getDyWAType(Attribute attr) '''PropertyType.«attr.getDyWATypeLiteral»'''
	def getDyWATypeLiteral(Attribute attr) '''«attr.innerDyWAType»«IF attr.isList»_LIST«ENDIF»'''
	
	def getNonOverridingAttributes(Type type) {
		type.originalAttributes.filter[a | !(a instanceof ComplexAttribute) || (a as ComplexAttribute).superAttr == null ]
			.filter[a | !(a instanceof PrimitiveAttribute) || (a as PrimitiveAttribute).superAttr == null ]
	}
	
	def getOverridingComplexAttributes(Type type) {
		type.originalAttributes.filter(ComplexAttribute).filter[superAttr != null]
	}
	
	def getOverridingPrimitiveAttributes(Type type) {
		type.originalAttributes.filter(PrimitiveAttribute).filter[superAttr != null]
	}
	
	def getOriginalAttributes(Type type) {
		type.attributes.map[originalAttribute]
	}
	
	private def innerDyWAType(Attribute attr) {
		switch attr {
			PrimitiveAttribute: attr.dataType.dyWALiteral
			ComplexAttribute: "OBJECT"
			EnumLiteral: "OBJECT"
		}
	}
	
	def getDyWALiteral(PrimitiveType pType){
		switch pType {
			case BOOLEAN: "BOOLEAN"
			case INTEGER: "LONG"
			case REAL: "DOUBLE"
			case TEXT: "STRING"
			case TIMESTAMP: "TIMESTAMP"
			case FILE: "FILE"
		}
	}
}
