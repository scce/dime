/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.migration

import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumType
import info.scce.dime.generator.rest.DyWAAbstractGenerator
import info.scce.dime.generator.rest.TOGenerator
import info.scce.dime.generator.util.DyWAExtension
import java.nio.file.Path
import java.util.List

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class EnumMigrator {
	
	static protected extension DyWAExtension = new DyWAExtension
	
	val packageName = "de.ls5.dywa.generated.migration"
	
	def generate(List<Data> models, Path outlet) {
		val enums = TOGenerator.extractTypes(models).filter(EnumType)
		generateMigrator(outlet, enums)
	}
	
	private def generateMigrator(Path outlet, Iterable<EnumType> enumTypes) {
		val targetDir = outlet.resolve("dywa-app/app-persistence-impl/target/generated-sources/")
		DyWAAbstractGenerator.generate(renderMigrator(enumTypes).toString, '''«packageName».''', "EnumMigrator.java", targetDir, 0)
	}
		
	private def renderMigrator(Iterable<EnumType> enumTypes) '''
		package «packageName»;
		
		import java.util.List;
		
		import javax.annotation.PostConstruct;
		import javax.ejb.Singleton;
		import javax.ejb.Startup;
		import javax.persistence.EntityManager;
		import javax.persistence.PersistenceContext;
		import javax.persistence.TypedQuery;
		
		import de.ls5.dywa.generated.util.EnumMapping;
		
		@Singleton
		@Startup
		public class EnumMigrator {
			
			@PersistenceContext
			private EntityManager em;
			
			@PostConstruct
			public void migrate() {
				
				for (EnumMapping mapping : em.createQuery("SELECT mapping FROM de.ls5.dywa.generated.util.EnumMapping mapping", EnumMapping.class).getResultList()) {
					em.remove(mapping);
				}
				em.flush();
				
				«FOR type : enumTypes»
					setup«type.dyWATypeSimpleName»(«type.enumLiterals.join(", ")['''«type.getDyWATypeName».«it.name.escapeJava»''']»);
				«ENDFOR»
			}
			
			«FOR type : enumTypes»
				private void setup«type.dyWATypeSimpleName»(«type.getDyWATypeName»... values) {
					
					for («type.getDyWATypeName» value : values) {
						final TypedQuery<«type.getDyWATypeName»Entity> query = em.createQuery(
								"SELECT o FROM «type.getDyWATypeName»Entity o WHERE o.name_ = :name", «type.getDyWATypeName»Entity.class);
						query.setParameter("name", value.name());
						
						final List<«type.getDyWATypeName»Entity> res = query.getResultList();
						final «type.getDyWATypeName»Entity entity;
						
						switch (res.size()) {
							case 0:
								entity = new «type.getDyWATypeName»Entity();
								entity.setDywaName(value.name());
								em.persist(entity);
								break;
							case 1:
								entity = res.get(0);
								break;
							default:
								throw new IllegalStateException("There must not exist multiple enum-entities with the same name");
						}
						
						em.persist(new EnumMapping(value.getDywaEnumId(), entity.getId_()));
					}
				}
			«ENDFOR»
		}
	'''
}
