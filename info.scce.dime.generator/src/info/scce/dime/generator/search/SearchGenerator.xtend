/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.search

import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.search.search.Search
import java.nio.file.Path
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IProgressMonitor

class SearchGenerator {
	
	extension DimeIOExtension = new DimeIOExtension
	extension SearchControllerHelper = new SearchControllerHelper
	
	val Path projectPath
	
	var String contentsImpl = ""
	var String contentsInterface = ""

	new (IProject project) {
		super()
		this.projectPath = project.toNIOPath
		this.contentsImpl += generateImplHead()
		this.contentsInterface += generateInterfaceHead()
	}

	def void generateForModel(Search model, Path outlet, IProgressMonitor monitor) {
		println('''Generating Search: «model.modelName»''')
		monitor.subTask('''Generating Search: «model.modelName»''')
		contentsImpl += model.generateMethod()
		contentsInterface += model.generateMethodDefinition()
	}

	def void finishFiles() {
		contentsImpl += generateImplFooter()
		contentsInterface += generateInterfaceFooter()
		writeFile("SearchControllerImpl.java", packageName, contentsImpl)
		writeFile("SearchController.java", packageName, contentsInterface)
	}

	def private void writeFile(CharSequence fileName, CharSequence packageName, CharSequence contents) {
		if (contents !== null) {
			try {
				projectPath
					.resolve("dywa-app", "app-persistence", "target", "generated-sources")
					.resolve(packageName.toString.split('''\.'''))
					.createDirectories()
					.resolve(fileName)
					.writeString(contents)
			}
			catch (Exception e) {
				throw new RuntimeException(e)
			}
		}
	}
	
}
