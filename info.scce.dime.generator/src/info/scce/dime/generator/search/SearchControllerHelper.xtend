/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.search;

import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Type
import info.scce.dime.search.search.AndOperator
import info.scce.dime.search.search.BooleanCompareOperator
import info.scce.dime.search.search.CompareOperator
import info.scce.dime.search.search.ComplexOutputParameter
import info.scce.dime.search.search.DataEdge
import info.scce.dime.search.search.InputParameter
import info.scce.dime.search.search.IntegerCompareOperator
import info.scce.dime.search.search.OrOperator
import info.scce.dime.search.search.PrimitiveInputParameter
import info.scce.dime.search.search.PrimitiveType
import info.scce.dime.search.search.RealCompareOperator
import info.scce.dime.search.search.Search
import info.scce.dime.search.search.SearchConditional
import info.scce.dime.search.search.SearchNode
import info.scce.dime.search.search.TextCompareOperator
import info.scce.dime.search.search.TimestampCompareOperator
import info.scce.dime.search.search.Variable
import info.scce.dime.search.search.util.SearchSwitch

class SearchControllerHelper extends SearchSwitch<CharSequence> {

	val public String packageName          = "de.ls5.dywa.generated.search.controller"
	val public String packageEntity        = "de.ls5.dywa.generated.entity"
	val public String packageEntityWrapper = "de.ls5.dywa.generated.search.entity"
	
	def generateImplHead() '''
		package «this.packageName»;
		
		import java.util.List;
		import java.util.Map;
		import java.util.HashMap;
		import javax.enterprise.context.RequestScoped;
		import javax.inject.Inject;
		import javax.inject.Named;
		import de.ls5.dywa.generated.search.api.*;
		
		@Named
		@RequestScoped
		public class SearchControllerImpl implements SearchController {
		
			@Inject
			private SearchBuilder searchBuilder;
	'''

	def generateImplFooter() '''
		}
	'''

	def generateInterfaceHead() '''
		package «this.packageName»;
		
		import java.util.List;
		
		public interface SearchController {
	'''

	def generateInterfaceFooter() '''
		}
	'''

	def generateMethodDefinition(Search search) '''
		
		// Generated Search for «search.modelName»
		public List<«search.outputClass»> executeSearch«search.methodName»(«search.inputParameters»);
	'''

	def generateMethod(Search search) '''
		
		// Generated Search for «search.modelName»
		public List<«search.outputClass»> executeSearch«search.methodName»(«search.inputParameters») {
			
			DyWAQuery<«search.outputClass»> _query_ = this.searchBuilder.createQuery(«search.outputClass».class);
			_query_.distinct(true);
			From<«search.outputClass»> _root_ = _query_.from(«search.outputClass».class);
			PathConstraintBuilder<«search.outputClass»> _pathConstraintBuilder_ = _root_.newPathConstraintBuilder();
			
			// build constraint
			«IF search.searchNode !== null»
				_pathConstraintBuilder_.addConstraints(«search.searchNode.doSwitch»);
			«ENDIF»
			
			PathConstraint _pathConstraint_ = _pathConstraintBuilder_.buildPathConstraint();
			_query_.where(_pathConstraint_);
			
«««			// create parameterMap
«««			Map<String, Object> _parameterMap_ = new HashMap<>();
«««			«FOR ip: search.searchInterfaces.head.inputParameters.filter(PrimitiveInputParameter)»
«««				_parameterMap_.put("«ip.name»", «ip.name»);
«««			«ENDFOR»
«««			
			return this.searchBuilder.executeQuery(_query_);
		}
		
	'''
	
	def getOutputClass(Search search) {
		search
			.searchInterfaces
			.head
			.outputParameters
			.head
			.doSwitch
	}
	
	def getInputParameters(Search search) {
		search
			.searchInterfaces
			.head
			.inputParameters
			.filter(PrimitiveInputParameter)
			.map['''«dataType.primitiveType» «name»''']
			.join(", ")
	}
	
	def getSearchNode(Search search) {
		search
			.searchInterfaces
			.head
			.getPredecessors(SearchNode)
			.head
	}

	def getMethodName(Search search) {
		search.id.replace('-', '')
	}
	
	def getPrimitiveType(PrimitiveType pType) {
		switch (pType) {
			case BOOLEAN:   "Boolean"
			case INTEGER:   "Long"
			case REAL:      "Double"
			case TEXT:      "String"
			case TIMESTAMP: "java.util.Date"
			default:        "UNDEF"
		}
	}

	def getOperatorMethodName(String operator) {
		switch (operator) {
			case "less_than":        "lt"
			case "less_or_equal":    "leq"
			case "greater_than":     "gt"
			case "greater_or_equal": "geq"
			case "equal":            "equal"
			case "not_equal":        "not equal"
			case "like":             "like"
			case "before":           "before"
			case "after":            "after"
			default:                 "UNDEF"
		}
	}

	override caseComplexOutputParameter(ComplexOutputParameter cop) {
		'''«packageEntity».«cop.dataType.name»'''
	}

	override caseAndOperator(AndOperator op) {
		'''_pathConstraintBuilder_.and(«op.getPredecessors(SearchNode).map[doSwitch].join(", ")»)'''
	}

	override caseOrOperator(OrOperator op) {
		'''_pathConstraintBuilder_.or(«op.getPredecessors(SearchNode).map[doSwitch].join(", ")»)'''
	}
	
	override caseSearchConditional(SearchConditional sc) {
		'''_pathConstraintBuilder_.«sc.compareOperators.head.compareOperator»(«sc.compareAttribute», «sc.compareVariable»)'''
	}
	
	def getCompareAttribute(SearchConditional sc) {
		'''«packageEntityWrapper».«sc.attribute.entityWrapper»_.«sc.attribute.name»'''
	}
	
	def getCompareVariable(SearchConditional sc) {
		switch (varNode: sc.compareOperators.head.getIncoming(DataEdge).head.sourceElement) {
			InputParameter: varNode.name
			Variable:       varNode.inputParameterPredecessors.head.name
			default:        "UNDEF"
		}
	}

	def getEntityWrapper(Attribute attr) {
		switch (con: attr.container) {
			Type:    con.name
			default: "UNDEF"
		}
	}

	def getCompareOperator(CompareOperator co) {
		switch (co) {
			IntegerCompareOperator:   co.operator.getName().operatorMethodName
			RealCompareOperator:      co.operator.getName().operatorMethodName
			TextCompareOperator:      co.operator.getName().operatorMethodName
			BooleanCompareOperator:   co.operator.getName().operatorMethodName
			TimestampCompareOperator: co.operator.getName().operatorMethodName
			default:                  "UNDEF"
		}
	}
	
}
