/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import info.scce.dime.data.data.Type
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.CompoundView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.process.BackendProcessGeneratorUtil
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.Process
import java.nio.file.Path
import java.util.Collection
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set

import static extension info.scce.dime.generator.rest.DyWAAbstractGenerator.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class SelectiveLongRunningProcessGenerator extends BackendProcessGeneratorUtil {
		
	def generate(List<Process> allLRPs, Process interactionProcess, Map<String, CompoundView> resumePoints, Path outlet) {
		
	    val guardContainers = allLRPs.map[guardContainers].flatten.filter[x | x.subProcess.id.equals(interactionProcess.id)].toSet
		
		val packageName = "de.ls5.dywa.generated"
		val targetDir = outlet.resolve("app-business/target/generated-sources")
		
		DyWAAbstractGenerator.generate(
			generateResumeController(allLRPs, interactionProcess, resumePoints, guardContainers, packageName).toString(),
			packageName + ".rest.process.longrunning.",
			BackendProcessGeneratorUtil.getSimpleTypeName(interactionProcess) + "Resume.java",
			targetDir,
			0
		)
			
		generateResumeInputs(interactionProcess, resumePoints, packageName, targetDir)
	}
	
	private def generateResumeController(List<Process> allModels, Process interactionProcess, Map<String, CompoundView> resumePoints, Collection<GuardContainer> guardContainers, String packageName) '''
		package «packageName».rest.process.longrunning;
		
		@javax.transaction.Transactional
		@javax.ws.rs.Path("/resume")
		public class «interactionProcess.simpleTypeName»Resume {
			
			@javax.inject.Inject
			private «"InteractionController".processContextsControllerName» interactionController;
			
			@javax.inject.Inject
			private info.scce.dime.util.DomainFileController domainFileController;
			
			«FOR i : allModels»
				@javax.inject.Inject
				private «i.typeName» process«i.simpleTypeName»;
			«ENDFOR»

			«FOR typeView : resumePoints.values.map[compounds].flatten.importTypeForResumeProcess»
				@javax.inject.Inject
				private «packageName».rest.controller.«DyWASelectiveDartGenerator.dataModelType(typeView.data).name.escapeJava»REST «typeView.generateTOName»REST;
				@javax.inject.Inject
				private «DyWASelectiveDartGenerator.dataModelType(typeView.data).controllerTypeName» «typeView.generateTOName»Controller;
			«ENDFOR»
		
			«FOR resumePoint : resumePoints.entrySet»
			«val branch = resumePoint.key»
			«val inputs = resumePoint.value.compounds»
			
			/**
			 * Resume.
			 */
			@javax.ws.rs.POST
			@javax.ws.rs.Path("«interactionProcess.modelName.escapeJava»/«branch.escapeJava»/{id}")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			public javax.ws.rs.core.Response resume«interactionProcess.id.escapeJava»At«branch.toFirstUpper.escapeJava»(
				@javax.ws.rs.PathParam("id") final long interactionId«IF inputs.size > 0», final «packageName».rest.process.longrunning.Resume«interactionProcess.modelName.escapeJava»«branch.toFirstUpper.escapeJava»Input ctx«ENDIF»
				) {
					final «"Interaction".processContextsTypeName» interaction = interactionController.readInteraction(interactionId);
					
					«inputs.copyParametersIntoLocalVariables(packageName)»
					
					«FOR guardContainer : guardContainers»
						if (interaction instanceof «guardContainer.interactionTypeName.processContextsTypeName») {
							«val longRunning = guardContainer.container as Process»
							final «guardContainer.interactionTypeName.processContextsTypeName» realInteraction = («guardContainer.interactionTypeName.processContextsTypeName») interaction;
							
							// TODO check for real interaction inputs
							if (this.process«longRunning.simpleTypeName».checkInteraction«guardContainer.id.escapeJava»(realInteraction, realInteraction.getinteractionInputs())) {
								this.process«longRunning.simpleTypeName».resume«guardContainer.id.escapeJava»At«branch.toFirstUpper.escapeJava»(realInteraction «inputs.sortBy[name].join(", ", ", ", "")['''«it.name.escapeJava»''']»);
								return javax.ws.rs.core.Response.ok().build();
							}
							
							return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
						}
					«ENDFOR»
					
					return javax.ws.rs.core.Response.ok().build();
			}
			«ENDFOR»
		}
	'''
	
	def generateResumeInputs(Process interactionProcess, Map<String, CompoundView> resumePoints, String packageName, Path targetDir) {
	
		for (rp : resumePoints.entrySet) {
			
			val processName = interactionProcess.modelName.escapeJava;
			val branch = rp.key.toFirstUpper.escapeJava
			val inputs = rp.value.compounds
			
			if (!inputs.empty) {
					
				val code = '''
					package «packageName».rest.process.longrunning;
					
					import java.util.*;
					
					public class Resume«processName»«branch»Input {
						
						«FOR i : inputs»
							private «i.generateBasicTOName(packageName)» «i.name.escapeJava»;
								
							@com.fasterxml.jackson.annotation.JsonProperty("«i.name.escapeString»")
							public «i.generateBasicTOName(packageName)» get«i.name.escapeJava»() {
								return this.«i.name.escapeJava»;
							}

							@com.fasterxml.jackson.annotation.JsonProperty("«i.name.escapeString»")
							public void set«i.name.escapeJava»(final «i.generateBasicTOName(packageName)» «i.name.escapeJava») {
								this.«i.name.escapeJava» = «i.name.escapeJava»;
							}
						«ENDFOR»
					}
				'''.toString
				 
				DyWAAbstractGenerator.generate(code,
					packageName + ".rest.process.longrunning.",
					"Resume" + processName + branch + "Input.java",
					targetDir,0);
			}
		}
	}
	
	private def importTypeForResumeProcess(Iterable<TypeView> tvs) {
		
		val Set<Type> cache = new HashSet<Type>();
		val Set<TypeView> result = new HashSet<TypeView>();
		
		for (tv : tvs.filter(ComplexTypeView)) {
			if (!cache.contains(tv.type)) {
				cache.add(tv.type)
				result.add(tv);
			}
		}
		
		result
	}
}
