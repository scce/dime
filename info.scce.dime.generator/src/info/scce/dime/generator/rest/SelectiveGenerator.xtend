/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.FieldView
import java.nio.file.Path

import static info.scce.dime.generator.util.DyWAExtension.*

import static extension info.scce.dime.generator.gui.data.SelectiveExtension.*
import static extension info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class SelectiveGenerator extends DyWAAbstractGenerator {
	
	val GenerationContext genctx
	
	new(GenerationContext genctx) {
		this.genctx = genctx
	}
	
	def generate(Path outlet) {
		val targetDir = outlet.resolve("app-business/target/generated-sources")

		val cviews = genctx.collectComplexTypeViews;
		val uniqueCTVs = cviews.map[it | genctx.selectiveCache.getRepresentative(it)].toSet

		uniqueCTVs.forEach[generate(it, targetDir)];
	}
	
	def generate(Type type, Path outlet) {
		val targetDir = outlet.resolve("app-business/target/generated-sources")

		type.buildBlankTypeView.generateFullSelective(targetDir)
	}

	private def void generate(ComplexTypeView tv, Path outlet) {
		DyWAAbstractGenerator.generate(
			generate(tv).toString,
			restTypePkg + ".",
			tv.selectiveNameJava.escapeJava + ".java",
			outlet,
			0
		);
	}

	private def generate(ComplexTypeView view) '''
		// generated by SelectiveGenerator#generate(ComplexTypeView view)
		package «restTypePkg»;

		public class «view.selectiveNameJava» {
		
			«view.renderCopyMethod»
		}
	'''

	private def void generateFullSelective(ComplexTypeView tv, Path outlet) {
		val fileName = tv.selectiveNameJava.escapeJava + ".java"
		DyWAAbstractGenerator.generate(
			generateFullSelective(tv).toString,
			restTypePkg + ".",
			fileName,
			outlet,
			0
		);
	}

	private def generateFullSelective(ComplexTypeView view) '''
		// generated by SelectiveGenerator#generateFullSelective(ComplexTypeView view)
		package «restTypePkg»;

		public class «view.selectiveNameJava» {

			public static void copy(
				final «view.generateInnerDywaTOName(dywaPkg)» from,
				final «view.generateInnerBasicTOName(dywaPkg)» to,
				final info.scce.dime.rest.ObjectCache objectCache) {

				««« .remove(view.type) does not filter our own type
				«FOR t : view.type.knownSubTypes.drop(1).sortTopologically SEPARATOR " else "»
				«val subView = t.buildNonRecursiveTypeView»
				if ((from instanceof «subView.generateInnerDywaTOName(dywaPkg)») &&
					(to instanceof «subView.generateInnerBasicTOName(dywaPkg)»)) {
					«subView.selectiveNameJava».copy((«subView.generateInnerDywaTOName(dywaPkg)») from, («subView.generateInnerBasicTOName(dywaPkg)») to, objectCache);
					return;
				}
				«ENDFOR»

				if (objectCache.containsSelective(to, "«view.selectiveNameJava»")) {
					return;
				}

				to.setDywaId(from.getDywaId());

				// put to cache, after id has been set
				objectCache.putSelective(to, "«view.selectiveNameJava»");

				// non-transient objects can be fetched completely from the DB
				if (from.getDywaId() > info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
					return;
				}

				to.setDywaVersion(from.getDywaVersion());
				to.setDywaName(from.getDywaName());

				«FOR dispField : view.displayedFields»
					«view.renderCopyAttribute(dispField)»
				«ENDFOR»
			}
		}
	'''
	
	private def renderCopyMethod(ComplexTypeView view) '''
		public static void copy(
			final «view.generateInnerDywaTOName(dywaPkg)» from,
			final «view.generateInnerBasicTOName(dywaPkg)» to,
			final info.scce.dime.rest.ObjectCache objectCache) {

			«val fullReplacement = view.compositionView !== null && !view.isCompositionToSubType»

			«IF fullReplacement»
				«view.compositionView.selectiveNameJava».copy(from, to, objectCache);
			«ELSE»
				«IF view.compositionView !== null»
					// this is a reflexive typeview, so check this case first
					if ( (from instanceof «view.compositionView.generateInnerDywaTOName(dywaPkg)»)
							&& (to instanceof «view.compositionView.generateInnerBasicTOName(dywaPkg)»)) {
						«view.compositionView.selectiveNameJava».copy(
							(«view.compositionView.generateInnerDywaTOName(dywaPkg)») from,
							(«view.compositionView.generateInnerBasicTOName(dywaPkg)») to,
							objectCache
						);
						return;
					}
				«ENDIF»

				if (objectCache.containsSelective(to, "«view.selectiveNameJava»")) {
					return;
				}

				to.setDywaId(from.getDywaId());
				to.setDywaVersion(from.getDywaVersion());
				to.setDywaName(from.getDywaName());

				// put to cache, after id has been set
				objectCache.putSelective(to, "«view.selectiveNameJava»");

				«FOR dispField : view.displayedFields»
					«view.renderCopyAttribute(dispField)»
				«ENDFOR»
			«ENDIF»
		}
	'''
	
	
	private def renderCopyAttribute(ComplexTypeView typeView, FieldView fieldView) {
		var CharSequence listGetter = fieldView.field.nameOfAccessor;
		var CharSequence delegateTypeName = generateDywaTOName(fieldView, dywaPkg);
		var CharSequence delegateInnerTypeName = generateInnerDywaTOName(fieldView, dywaPkg);
		 
		'''
			«val needRuntimeCheck = typeView.needRuntimeCheck(fieldView)»
			«IF needRuntimeCheck»
				«val runtimeFromType = typeView.renderDeclaringDywaType(fieldView)»
				«val runtimeToType = typeView.renderDeclaringRestType(fieldView)»
				if (from instanceof «runtimeFromType» && to instanceof «runtimeToType») { // TODO: maybe use dispatch methods
					final «runtimeFromType» effectiveFrom = («runtimeFromType») from;
					final «runtimeToType» effectiveTo = («runtimeToType») to;
			«ELSE»
				{
					final «generateInnerDywaTOName(typeView, dywaPkg)» effectiveFrom = from;
					final «generateInnerBasicTOName(typeView, dywaPkg)» effectiveTo = to;
			«ENDIF»
			«IF fieldView.field.isPrimitive»
				«IF fieldView.field.originalAttribute.primitiveDataType == PrimitiveType.FILE»
					«IF fieldView.isList»
						final «delegateTypeName» source = effectiveFrom.get«fieldView.field.nameOfAccessor»();
						final «generateBasicTOName(fieldView, dywaPkg)» target = new java.util.ArrayList<>(source.size());

						for (final «delegateInnerTypeName» s : source) {
							target.add(new «restPkg».util.FileReference(s));
						}

						effectiveTo.set«fieldView.field.nameOfAccessor»(target);
					«ELSE»
						if (effectiveFrom.get«fieldView.field.nameOfAccessor»() != null) {
							to.set«fieldView.field.nameOfAccessor»(
								new «restPkg».util.FileReference(effectiveFrom.get«fieldView.field.nameOfAccessor»())
							);
						}
					«ENDIF»
					
				«ELSE»
					«IF fieldView.field.isList»
						effectiveTo.set«fieldView.field.nameOfAccessor»(new java.util.ArrayList<>(effectiveFrom.get«fieldView.field.nameOfAccessor»()));
					«ELSE»
						effectiveTo.set«fieldView.field.nameOfAccessor»(effectiveFrom.get«fieldView.field.nameOfAccessor»());
					«ENDIF»
				«ENDIF»
			«ELSE»
				«val tv = (fieldView as ComplexFieldView).view as ComplexTypeView»
				«val rep = genctx.selectiveCache.getRepresentative(tv)»
				«IF fieldView.isList»
					final «delegateTypeName» source = effectiveFrom.get«listGetter»();
					final «generateBasicTOName(fieldView, dywaPkg)» target = new java.util.ArrayList<>(source.size());

					for (final «delegateInnerTypeName» s : source) {
						// original selective: «tv.selectiveNameJava»
						«rep.renderCacheLookup("s", "cached")»

						target.add(cached);
					}

					effectiveTo.set«fieldView.field.nameOfAccessor»(target);
				«ELSE»
					final «delegateTypeName» source = effectiveFrom.get«fieldView.field.nameOfAccessor»();
					if (source != null) {
						// original selective: «tv.selectiveNameJava»
						«rep.renderCacheLookup("source", "cached")»

						effectiveTo.set«fieldView.field.nameOfAccessor»(cached);
					}
				«ENDIF»
			«ENDIF»
			}
		'''
	}
}
