/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.process.BackendProcessGeneratorUtil
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.File
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GuardSIB
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import java.nio.file.Path
import java.util.Collections
import java.util.HashMap
import java.util.LinkedList
import java.util.List
import java.util.Map

import static info.scce.dime.generator.util.DyWAExtension.dywaPkg

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import static extension info.scce.dime.gui.helper.GUIExtension.lastModified2

class DownloadTokenGenerator extends BackendProcessGeneratorUtil {
	
	val packageName = "de.ls5.dywa.generated.rest.file"
	val packageNameForFiles = packageName + '.'
	
	def generate(GUI gui, GUICompoundView gcv, Path rootDir, GenerationContext genctx) {
		val targetDir = rootDir.resolve("app-business/target/generated-sources")
		val Map<MovableContainer, Pair<GuardSIB, List<GuardSIB>>> containerToFetchToGuardsMap = gui.fetchAllReferencedGuardProcess(genctx);
		
		containerToFetchToGuardsMap.entrySet.forEach[generateInputWrapper(gui,targetDir,genctx)]
				
		val String content = generateRestService(gui, containerToFetchToGuardsMap, packageName).toString();
		val String fileName = "TokenGeneratorFor" + gui.id.escapeJava + ".java";
		
		DyWAAbstractGenerator.generate(content, packageNameForFiles, fileName, targetDir, gui.lastModified2);
	}
	
	private def generateInputWrapper(Map.Entry<MovableContainer,Pair<GuardSIB, List<GuardSIB>>> containerToFetchToGuardsEntry, GUI gui, Path targetDir, GenerationContext genctx) {
		
		val container = containerToFetchToGuardsEntry.key
		val fetchProcess = containerToFetchToGuardsEntry.value.key.process as Process
		
		generateSecurityInputs(containerToFetchToGuardsEntry, gui, targetDir)
		generateFetchInputs(fetchProcess, targetDir, genctx)
		
		val wrapperName = container.computeClassNamePrefix(gui) + "InputWrapper"
		val secInputsName = container.computeClassNamePrefix(gui) + "SecurityInputs"
		val fetchInputsName = fetchProcess.simpleTypeName + "FetchInputs"
		
		val String content = DyWAAbstractGenerator.generateInputWrapperClassForSecuredProcess(packageName, wrapperName, fetchInputsName, "context", secInputsName, null).toString();
		val String fileName = wrapperName + ".java";
		
		DyWAAbstractGenerator.generate(content, packageNameForFiles, fileName, targetDir, 0);
	}
	
	private def generateSecurityInputs(Map.Entry<MovableContainer,Pair<GuardSIB, List<GuardSIB>>> containerToFetchToGuardsEntry, GUI gui, Path targetDir) {
		val container = containerToFetchToGuardsEntry.key
		
		val String content = guardsInputTemplate(containerToFetchToGuardsEntry, gui).toString();
		val String fileName = container.computeClassNamePrefix(gui) + "SecurityInputs.java";
		
		DyWAAbstractGenerator.generate(content, packageNameForFiles, fileName, targetDir,0);
	}
	
	private def generateFetchInputs(Process process, Path targetDir, GenerationContext genctx) {
		val String content = fetchInputTemplate(process).toString();
		val String fileName = process.simpleTypeName + "FetchInputs.java";
		
		DyWAAbstractGenerator.generate(content, packageNameForFiles, fileName, targetDir, process.lastModified2);
	}
	
	
	
	private def fetchInputTemplate(Process process) '''
		package «packageName»;
			
		public class «process.simpleTypeName»FetchInputs {
			«FOR input : process.processInputs»
				private «input.restTOName» «input.name.escapeJava»;
					
				@com.fasterxml.jackson.annotation.JsonProperty("«input.name.escapeString»")
				public «input.restTOName» get«input.name.escapeJava»() {
					return this.«input.name.escapeJava»;
				}

				@com.fasterxml.jackson.annotation.JsonProperty("«input.name.escapeString»")
				public void set«input.name.escapeJava»(final «input.restTOName» «input.name.escapeJava») {
					this.«input.name.escapeJava» = «input.name.escapeJava»;
				}
			«ENDFOR»			
		}
	'''
	
	private def guardsInputTemplate(Map.Entry<MovableContainer,Pair<GuardSIB, List<GuardSIB>>> containerToFetchToGuardsEntry, GUI gui) {
	
		val container = containerToFetchToGuardsEntry.key
		
		'''
		package «packageName»;
			
		public class «container.computeClassNamePrefix(gui)»SecurityInputs {
			«FOR guard : containerToFetchToGuardsEntry.value.value»
				//guard
			«ENDFOR»			
		}
		'''
	}
	
	private def generateRestService(GUI gui, Map<MovableContainer, Pair<GuardSIB, List<GuardSIB>>> containerToFetchToGuardsMap, String packageName) {
		
		val involvedProcesses = containerToFetchToGuardsMap.values.map[value].flatten.map[getProcess as Process].toSet 
		involvedProcesses.addAll(containerToFetchToGuardsMap.values.map[key].map[getProcess as Process].toSet)
		
		'''
		package «packageName»;

		import java.util.Date;
		import java.util.List;
		
		import javax.validation.constraints.NotNull;
		import javax.ws.rs.QueryParam;
		
		@javax.transaction.Transactional
		@javax.ws.rs.Path("/files/«gui.id.escapeJava»")
		public class TokenGeneratorFor«gui.id.escapeJava» {

			
			«FOR inputType : involvedProcesses.map[processInputs].flatten.filter(ComplexOutputPort).map[dataType.originalType].toSet»
				@javax.inject.Inject
				private «inputType.controllerTypeName» «inputType.name.escapeJava»Controller;
			«ENDFOR»
			
			«FOR guard : involvedProcesses»
				@javax.inject.Inject
				private «guard.typeName» «guard.simpleTypeName»;
			«ENDFOR»
			
			@javax.inject.Inject
			private info.scce.dime.util.DomainFileController domainFileController;
			
			«FOR entry : containerToFetchToGuardsMap.entrySet»
				«generateTokenGenerator(gui, entry.key, entry.value)»
			«ENDFOR»
		}
		'''
	}
		
	private def getInputStaticType(InputStatic input) {
		switch(input) {
			IntegerInputStatic: '''Long'''
			BooleanInputStatic: '''Boolean'''
			TextInputStatic: '''String'''
			TimestampInputStatic: '''Date'''
			RealInputStatic: '''Double'''
		}	
	}
		
	private def getInputStaticValue(InputStatic input) {
		switch(input) {
			IntegerInputStatic: '''«input.value»l'''
			BooleanInputStatic: '''«input.value»'''
			TextInputStatic: '''"«input.value»"'''
			TimestampInputStatic: '''new Date(«input.value»)'''
			RealInputStatic: '''«input.value»d'''
		}	
	}
		
	private def generateStaticVars(GUI gui, MovableContainer container, Pair<GuardSIB, List<GuardSIB>> fetchToGuards, CharSequence content) {
		val guardSib = fetchToGuards.key
		val guard = guardSib.getProcess as Process
							
		'''
		«FOR input : guardSib.inputStatics»
		final «getInputStaticType(input)» «input.name.escapeJava» = «getInputStaticValue(input)»;
		«ENDFOR»
		«FOR input : guardSib.primitiveInputPorts.filter[dataType==info.scce.dime.gui.gui.PrimitiveType.FILE]»
		final «IF input.isIsList»List<«ENDIF»info.scce.dime.util.FileReference«IF input.isIsList»>«ENDIF» «input.name.escapeJava» =
			«IF input.isIsList»
				file_«input.name.escapeJava».stream().map((n)=>this.domainFileController.getFileReference(n)).collect(java.util.stream.Collectors.toList())
			«ELSE»
				this.domainFileController.getFileReference(file_«input.name.escapeJava»)
			«ENDIF»;
		«ENDFOR»
		
		final «guard.typeName».«guard.simpleTypeName»Result result = this.«guard.simpleTypeName».execute(false«guard.processInputs.map(n|{
			if(n instanceof ComplexOutputPort) {
				if(n.isIsList) {
					return '''«Collections.emptyList()»'''
				}
				return '''null'''
			}
			n.name.escapeJava
		}).join(',',',','',[it])»);
		
		if ("result".equals(result.getBranchName())) {
			final info.scce.dime.util.FileReference reference = result.getResultReturn().getFile();
			
			«content»
		} else {
			return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
		}
		'''
	}
	
	private def generateQueryParams(GUI gui, MovableContainer container, Pair<GuardSIB, List<GuardSIB>> fetchToGuards) {
		val guardSib = fetchToGuards.key
		val guard = guardSib.getProcess as Process
		'''
		«FOR input : guard.processInputs.filter(PrimitiveOutputPort).filter[n|guardSib.inputPorts.exists[name.equals(n.name)]] SEPARATOR ','»
			  @NotNull @QueryParam("«input.name.escapeDart»") «IF input.dataType==PrimitiveType.FILE»«IF input.isIsList»List<«ENDIF»Long«IF input.isIsList»>«ENDIF» file_«input.name.escapeJava»«ELSE»«input.typeName» «input.name.escapeJava»«ENDIF»
		«ENDFOR»
		'''
	}
		
	private def generateTokenGenerator(GUI gui, MovableContainer container, Pair<GuardSIB, List<GuardSIB>> fetchToGuards) {
		'''
			@javax.ws.rs.GET
			@javax.ws.rs.Path("read/«container.id.escapeJava»/public")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
			public javax.ws.rs.core.Response readFor«container.id.escapeJava»(«generateQueryParams(gui, container, fetchToGuards)») {
			
				«generateStaticVars(gui, container, fetchToGuards, '''
					final java.io.InputStream stream = this.domainFileController.loadFile(reference);
					
					final byte[] fileResult;
					
					try {
						fileResult = com.google.common.io.ByteStreams.toByteArray(stream);
					} catch (java.io.IOException e) {
						throw new javax.ws.rs.WebApplicationException(e);
					}
					«IF fetchToGuards.key.isCachable»
					java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
					String lastModified = sdf.format(reference.getCreatedAt());
					«ENDIF»
					
					return javax.ws.rs.core.Response
							.ok(fileResult, reference.getContentType())
							«IF fetchToGuards.key.isCachable»
							.header("Last-Modified", lastModified)
							«ENDIF»
							.header("Content-Disposition", "attachment; filename=" + reference.getFileName())
							.build();
				''')»
			}
			
			@javax.ws.rs.GET
			@javax.ws.rs.Path("get/«container.id.escapeJava»/public")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			public javax.ws.rs.core.Response getFor«container.id.escapeJava»(«generateQueryParams(gui, container, fetchToGuards)») {
			
				«generateStaticVars(gui, container, fetchToGuards, '''									
					final de.ls5.dywa.generated.rest.util.FileReference ref =
						new de.ls5.dywa.generated.rest.util.FileReference(
							reference
						);
							
					return javax.ws.rs.core.Response.ok(ref).build();
				''')»
			}
		'''
	}
	
	private static def fetchAllReferencedGuardProcess(GUI gui, GenerationContext genctx) {
		val List<MovableContainer> containers = new LinkedList<MovableContainer>();
		containers.addAll(genctx.guiExtension.find(gui,File))
		containers.addAll(genctx.guiExtension.find(gui,Image))
		
		val Map<MovableContainer, Pair<GuardSIB, List<GuardSIB>>> result = new HashMap<MovableContainer, Pair<GuardSIB, List<GuardSIB>>>();
		for (MovableContainer container : containers) {
			val guardSIBs = container.allContainers.filter(GuardSIB);
			if (guardSIBs !== null && !guardSIBs.empty) {
				// for now we assume only a single fetch process
				val fetchProcess = guardSIBs.get(0)
				val Pair<GuardSIB, List<GuardSIB>> fetchToGuards = 
					fetchProcess as GuardSIB -> Collections.emptyList()
					
				result.put(container, fetchToGuards)	
			}
		}
		
		return result;
	}
	
	private def getRestTOName(OutputPort output) {
		val template = 
			if (output.isIsList) {
				"java.util.List<%s>"
			}
			else {
				"%s"
			}
		String.format(template, output.innerRestTOName)		
	}
	
	private def getInnerRestTOName(OutputPort output) {
		if (output instanceof PrimitiveOutputPort) {
			(output as PrimitiveOutputPort).dataType.javaLiteral
		}
		else if (output instanceof ComplexOutputPort) {
			dywaPkg + ".rest.types." + output.simpleTypeName
		}
		else {
			throw new IllegalStateException()
		}	
	}
	
	private def getJavaLiteral(PrimitiveType pType){
		switch(pType){
			case BOOLEAN: "java.lang.Boolean"
			case INTEGER: "java.lang.Long"
			case REAL: "java.lang.Double"
			case TEXT: "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE: dywaPkg + ".rest.util.FileReference"
		}
	}
	
	private def computeClassNamePrefix(MovableContainer container, GUI gui)
		'''«gui.title.escapeJava»«container.id.escapeJava»'''
}
