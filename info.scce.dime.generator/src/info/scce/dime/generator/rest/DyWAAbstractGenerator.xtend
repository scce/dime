/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.FieldView
import info.scce.dime.generator.gui.rest.model.PortRepresentant
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.process.BackendProcessGeneratorUtil
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardProcessSIB
import java.nio.file.Path
import java.util.List
import java.util.Map
import java.util.Set

import static info.scce.dime.generator.util.DyWAExtension.*

import static extension info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator.getSelectiveNameJava
import static extension info.scce.dime.generator.process.BackendProcessGeneratorUtil.*
import static extension info.scce.dime.generator.process.GeneralProcessGeneratorHelper.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

abstract class DyWAAbstractGenerator {
	
	static protected extension DyWAExtension = new DyWAExtension
	static protected extension DataExtension = DataExtension.instance
	static protected extension ProcessExtension = new ProcessExtension
	static protected extension DimeIOExtension = new DimeIOExtension
	
	static def generate(CharSequence content, CharSequence packageName, CharSequence fileName, Path targetDir, long lastModified) {
		val typePath = packageName.toString.split('''\.''')
		val path = targetDir.resolve(typePath).resolve(fileName)
		if (path.exists && lastModified > 0 && path.lastModifiedTime.toMillis >= lastModified) {
			return
		}
		path.parent.createDirectories()
		path.writeString(content)
	}
	
	static def Set<ComplexTypeView> fetchTypeViewsDeeply(Iterable<ComplexTypeView> tvs) {
		val cache = newHashSet
		collectRecursive(tvs, cache)
		return cache
	}
	
	private static def void collectRecursive(Iterable<ComplexTypeView> tvs, Set<ComplexTypeView> cache) {
		for (tv: tvs) {
//			if (!cache.contains(tv)) {
				cache.add(tv)
				tv
					.displayedFields
					.filter(ComplexFieldView)
					.map[view]
					.filter(ComplexTypeView)
					.collectRecursive(cache)
//			}
		}
	}
	
	// only reasonable for complex views, so any ClassCastException indicates a semantic error
	static def generateTOName(TypeView typeView) {
		(typeView as BaseComplexTypeView).typeName.escapeJava
	}

	// only reasonable for complex views, so any ClassCastException indicates a semantic error
	static def generateTOName(FieldView fieldView) {
		((fieldView as ComplexFieldView).view as BaseComplexTypeView).typeName.escapeJava
	}
	
	static def generateRestTOName(TypeView typeView, CharSequence packageName) {
		switch (typeView) {
			PrimitiveTypeView: generateRestTOName(typeView, packageName)
			default:           generateRestTOName(typeView as BaseComplexTypeView, packageName)
		}
	}
	
	static def generateRestTOName(PrimitiveTypeView typeView, CharSequence packageName) {
		evaluateList(typeView, generateInnerRestTOName(typeView, packageName))
	}
	
	static def generateRestTOName(BaseComplexTypeView typeView, CharSequence packageName) {
		evaluateList(typeView, generateInnerRestTOName(typeView, packageName))
	}
	
	static def generateRestTOName(FieldView fieldView, CharSequence packageName, boolean considerList) {
		switch (fieldView) {
			PrimitiveFieldView: generateRestTOName(fieldView, packageName, considerList)
			default:            generateRestTOName(fieldView as ComplexFieldView, packageName)
		}
	}
	
	static def CharSequence generateRestTOName(FieldView fieldView, CharSequence packageName) {
		fieldView.generateRestTOName(packageName, true)
	}
	
	static def generateRestTOName(PrimitiveFieldView fieldView, CharSequence packageName) {
		fieldView.generateRestTOName(packageName, true)
	}
	
	static def generateRestTOName(ComplexFieldView fieldView, CharSequence packageName) {
		generateRestTOName(fieldView.view as ComplexTypeView, packageName)
	}
	
	static def generateInnerRestTOName(TypeView typeView, CharSequence packageName) {
		switch (typeView) {
			PrimitiveTypeView: generateInnerRestTOName(typeView, packageName)
			default:           generateInnerRestTOName(typeView as BaseComplexTypeView, packageName)
		}
	}
	
	static def generateInnerRestTOName(PrimitiveTypeView typeView, CharSequence packageName) {
		typeView.primitiveType.restLiteral
	}
	
	static def generateInnerRestTOName(BaseComplexTypeView typeView, CharSequence packageName) {
		'''«packageName».rest.types.«typeView.type.RESTTOSimpleName»'''
	}
	
	static def generateInnerRestTOName(FieldView fieldView, CharSequence packageName) {
		switch (fieldView) {
			PrimitiveFieldView: generateInnerRestTOName(fieldView, packageName)
			default:            generateInnerRestTOName(fieldView as ComplexFieldView, packageName)
		}
	}
	
	static def generateRestTOName(PrimitiveFieldView fieldView, CharSequence packageName, boolean considerList) {
		if (considerList) {
			evaluateList(fieldView, generateInnerRestTOName(fieldView, packageName))
		}
		else {
			generateInnerRestTOName(fieldView, packageName)
		}
	}
	
	static def generateInnerRestTOName(PrimitiveFieldView fieldView, CharSequence packageName) {
		fieldView.primitiveType.restLiteral
	}
	
	static def generateInnerRestTOName(ComplexFieldView fieldView, CharSequence packageName) {
		generateInnerRestTOName(fieldView.view as ComplexTypeView, packageName)
	}
	
	static def generateDywaTOName(TypeView typeView, CharSequence packageName) {
		switch (typeView) {
			PrimitiveTypeView:   generateDywaTOName(typeView, packageName)
			BaseComplexTypeView: generateDywaTOName(typeView, packageName)
			default:             generateDywaTOName(typeView as ComplexTypeView, packageName)
		}
	}
	
	static def generateInnerDywaTOName(TypeView typeView, CharSequence packageName) {
		switch (typeView) {
			PrimitiveTypeView:   generateInnerDywaTOName(typeView, packageName)
			BaseComplexTypeView: generateInnerDywaTOName(typeView, packageName)
			default:             generateInnerDywaTOName(typeView as ComplexTypeView, packageName)
		}
	}
	
	static def generateDywaTOName(PrimitiveTypeView typeView, CharSequence packageName) {
		evaluateList(typeView, generateInnerDywaTOName(typeView, packageName))
	}
	
	static def generateInnerDywaTOName(PrimitiveTypeView typeView, CharSequence packageName) {
		typeView.primitiveType.getDyWaLiteral()
	}
	
	static def generateDywaTOName(BaseComplexTypeView typeView, CharSequence packageName) {
		evaluateList(typeView,  generateInnerDywaTOName(typeView, packageName))
	}
	
	static def generateInnerDywaTOName(ComplexTypeView typeView, CharSequence packageName) {
		DyWASelectiveDartGenerator.dataModelType(typeView.data).dyWATypeName
	}
	
	static def generateInnerDywaTOName(Type t, CharSequence packageName) {
		DyWASelectiveDartGenerator.dataModelType(t).dyWATypeName
	}
	
	static def generateInnerDywaTOName(BaseComplexTypeView typeView, CharSequence packageName) {
		DyWASelectiveDartGenerator.dataModelType(typeView.type).dyWATypeName
	}
	
	static def generateDywaTOName(FieldView fieldView, CharSequence packageName) {
		switch (fieldView) {
			PrimitiveFieldView: generateDywaTOName(fieldView, packageName)
			default:            generateDywaTOName(fieldView as ComplexFieldView, packageName)
		}
	}
	
	static def generateInnerDywaTOName(FieldView fieldView, CharSequence packageName) {
		switch (fieldView) {
			PrimitiveFieldView: generateInnerDywaTOName(fieldView, packageName)
			default:            generateInnerDywaTOName(fieldView as ComplexFieldView, packageName)
		}
	}
	
	static def generateDywaTOName(PrimitiveFieldView fieldView, CharSequence packageName) {
		evaluateList(fieldView, generateInnerDywaTOName(fieldView, packageName))
	}
	
	static def generateInnerDywaTOName(PrimitiveFieldView fieldView, CharSequence packageName) {
		fieldView.primitiveType.dyWaLiteral
	}
	
	static def generateDywaTOName(ComplexFieldView fieldView, CharSequence packageName) {
		evaluateList(fieldView, generateInnerDywaTOName(fieldView, packageName))
	}
	
	static def generateInnerDywaTOName(ComplexFieldView fieldView, CharSequence packageName) {
		generateInnerDywaTOName(fieldView.view as ComplexTypeView, packageName)
	}
	
	static def generateBasicTOName(TypeView typeView, CharSequence packageName) {
		switch (typeView) {
			PrimitiveTypeView: generateBasicTOName(typeView, packageName)
			default:           generateBasicTOName(typeView as BaseComplexTypeView, packageName)
		}
	}
	
	static def generateInnerBasicTOName(TypeView typeView, CharSequence packageName) {
		switch (typeView) {
			PrimitiveTypeView: generateInnerBasicTOName(typeView, packageName)
			default:           generateInnerBasicTOName(typeView as BaseComplexTypeView, packageName)
		}
	}
	
	static def generateBasicTOName(PrimitiveTypeView typeView, CharSequence packageName) {
		evaluateList(typeView, generateInnerBasicTOName(typeView, packageName))
	}
	
	static def generateInnerBasicTOName(PrimitiveTypeView typeView, CharSequence packageName) {
		typeView.primitiveType.restLiteral
	}
	
	static def generateBasicTOName(BaseComplexTypeView typeView, CharSequence packageName) {
		evaluateList(typeView, generateInnerBasicTOName(typeView, packageName))
	}
	
	static def generateInnerBasicTOName(BaseComplexTypeView typeView, CharSequence packageName) {
		'''«packageName».rest.types.«typeView.type.name.escapeJava»'''
	}
	
	static def generateBasicTOName(FieldView fieldView, CharSequence packageName) {
		switch (fieldView) {
			PrimitiveFieldView: generateBasicTOName(fieldView, packageName)
			default:            generateBasicTOName(fieldView as ComplexFieldView, packageName)
		}
	}
	
	static def generateInnerBasicTOName(FieldView fieldView, CharSequence packageName) {
		switch (fieldView) {
			PrimitiveFieldView: generateInnerBasicTOName(fieldView, packageName)
			default:            generateInnerBasicTOName(fieldView as ComplexFieldView, packageName)
		}
	}
	
	static def generateBasicTOName(PrimitiveFieldView fieldView, CharSequence packageName) {
		evaluateList(fieldView, generateInnerBasicTOName(fieldView, packageName))
	}
	
	static def generateInnerBasicTOName(PrimitiveFieldView fieldView, CharSequence packageName) {
		fieldView.primitiveType.restLiteral
	}
	
	static def generateBasicTOName(ComplexFieldView fieldView, CharSequence packageName) {
		evaluateList(fieldView, generateInnerBasicTOName(fieldView, packageName))
	}
	
	static def generateInnerBasicTOName(ComplexFieldView fieldView, CharSequence packageName) {
		'''«packageName».rest.types.«fieldView.field.complexDataType.name.escapeJava»'''
	}
		
//	protected def generateTOName(TypeView typeView, CharSequence packageName) {
//		'''«packageName».rest.types.«typeView.name.escape»_«typeView.id.escape»'''
//	}
//	
//	protected def generateTOName(FieldView fieldView, CharSequence packageName) '''
//		«IF fieldView.field.realAttribute instanceof ComplexAttribute»
//			«packageName».rest.types.«generateTOName((fieldView as ComplexFieldView))»
//		«ELSE»
//			«getRESTLiteral((fieldView.field.realAttribute as PrimitiveAttribute).dataType, packageName)»
//		«ENDIF»
//	'''
//	
//	protected def originalType(FieldView fieldView, CharSequence packageName) '''
//		«IF fieldView.field.realAttribute instanceof ComplexAttribute»
//			«packageName».entity.«originalType(fieldView)»
//		«ELSE»
//			«getJavaLiteral((fieldView.field.realAttribute as PrimitiveAttribute).dataType, packageName)»
//		«ENDIF»
//	'''
	
	static def originalType(FieldView fieldView) {
		(fieldView.field.originalAttribute as ComplexAttribute).dataType.name.escapeJava
	}
	
//	protected def attributeType(FieldView fieldView, CharSequence packageName) '''
//		«IF fieldView.field.isList»
//			java.util.List<
//		«ENDIF»
//		«IF fieldView.field instanceof PrimitiveAttribute»
//			«getRESTLiteral((fieldView.field.realAttribute as PrimitiveAttribute).dataType, packageName)»
//		«ELSE»	
//			«generateTOName(fieldView, packageName)»
//		«ENDIF»
//		«IF fieldView.field.isList»
//			>
//		«ENDIF»
//	'''
//
//	protected def attributeType(CompoundFieldView fieldView, CharSequence packageName) '''
//		«IF fieldView.isList»
//			java.util.List<
//		«ENDIF»
//		«IF fieldView instanceof PrimitveCompoundFieldView»
//			«getJavaLiteral(fieldView.pt, packageName)»
//		«ELSE»	
//			«generateTOName((fieldView as ComplexCompoundFieldView).typeView, packageName)»
//		«ENDIF»
//		«IF fieldView.isList»
//			>
//		«ENDIF»
//	'''
//
//	protected def originalAttributeType(CompoundFieldView fieldView, CharSequence packageName) '''
//		«IF fieldView.isList»
//			java.util.List<
//		«ENDIF»
//		«IF fieldView instanceof PrimitveCompoundFieldView»
//			«getJavaLiteral(fieldView.pt, packageName)»
//		«ELSE»	
//			«packageName».entity.«generateRestTOName((fieldView as ComplexCompoundFieldView).typeView)»
//		«ENDIF»
//		«IF fieldView.isList»
//			>
//		«ENDIF»
//	'''

	static def evaluateList(TypeView typeView, CharSequence content) {
		if (typeView.isList) {
			'''java.util.List<«content»>'''
		}
		else {
			content
		}
	}

	static def evaluateList(FieldView typeView, CharSequence content) {
		if (typeView.isList) {
			'''java.util.List<«content»>'''
		}
		else {
			content
		}
	}
	
	static def getJavaLiteral(PrimitiveType pType) {
		if (pType === null) {
			throw new IllegalStateException
		}
		switch (pType) {
			case BOOLEAN:   "java.lang.Boolean"
			case INTEGER:   "java.lang.Long"
			case REAL:      "java.lang.Double"
			case TEXT:      "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE:      "FileReference"
		}
	}
	
	static def getDyWaLiteral(PrimitiveType pType){
		switch (pType) {
			case FILE: "info.scce.dime.util.FileReference"
			default:   pType.javaLiteral
		}
		
	}
	
	static def getRestLiteral(PrimitiveType pType) {
		switch (pType) {
			case FILE: "de.ls5.dywa.generated.rest.util.FileReference"
			default:   pType.javaLiteral
		}
	}
	
	static def copyParametersIntoLocalVariables(Iterable<TypeView> typeViews, CharSequence packageName) {
		val typeViewNameMap = typeViews.groupBy[name]
		return copyParametersIntoLocalVariables(typeViewNameMap, "ctx", packageName, false, true)
	}
	
	static def copyParametersIntoLocalVariables(Map<String,List<TypeView>> typeViews, CharSequence variableName, CharSequence packageName) {
		return copyParametersIntoLocalVariables(typeViews, variableName, packageName, false, false)
	}
	
	static def copyParametersIntoLocalVariables(Map<String,List<TypeView>> typeViews, CharSequence variableName, CharSequence packageName, boolean serverSideSource) {
		return copyParametersIntoLocalVariables(typeViews, variableName, packageName, serverSideSource, false)
	}
	
	static def getCommonSuperTypeView(Iterable<? extends TypeView> typeViews) {
		if (typeViews.length == 1) {
			return typeViews.head
		}
		val complexTypeViews = typeViews.filter(ComplexTypeView).toList
		if (complexTypeViews.size > 1) {
			val candidate = new ComplexTypeView(complexTypeViews.head)
				// select union type
				for (otherTypeView: complexTypeViews) {
					if (otherTypeView.type != candidate.type) {
						val otherSuperTypes = otherTypeView.type.superTypes.toList
						val sharedSuperTypes = candidate.type.superTypes.filter[otherSuperTypes.contains(it)].toList
						if (!sharedSuperTypes.isEmpty) {
							candidate.type = sharedSuperTypes.sortBy[it.superTypes.length].last
						}
					}
				}
			return candidate
		}
		return null
	}

	static def copyParametersIntoLocalVariables(Map<String, List<TypeView>> typeViews, CharSequence variableName, CharSequence packageName, boolean serverSideSource, boolean considerSubTypes) '''
		«val selectiveCache = GenerationContext.instance.selectiveCache»
		«FOR typeViewCompound: typeViews.entrySet»
			«val name = typeViewCompound.key»
			«val superCTV = typeViewCompound.value.commonSuperTypeView»
			«val firstCompond = typeViewCompound.value.head»
			«val dywaTOName = superCTV.generateDywaTOName(packageName)»
			final «dywaTOName» «name.escapeJava»;
			«IF superCTV instanceof BaseComplexTypeView»
				«val complexTV = superCTV as BaseComplexTypeView»
				«val isEnum = complexTV.type instanceof EnumType»
				«IF complexTV.isList»
					«name.escapeJava» = new java.util.ArrayList<>();
					
					if («variableName».get«name.toFirstUpper.escapeJava»() != null) {
						for (final «superCTV.generateInnerBasicTOName(packageName)» o: «variableName».get«name.toFirstUpper.escapeJava»()) {
						«IF !isEnum»
							// create new object
							if (o.getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
								«IF serverSideSource»
									throw new java.lang.IllegalArgumentException("The server never returns objects with id '-1'");
								«ELSE»
									«FOR subComplexTV: typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + o.getClass()); }'''»
										if (o instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
											«IF subComplexTV.type.isAbstract && !considerSubTypes»
												throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
											«ELSE»
												«subComplexTV.preprocessObject(packageName, '''o''', name)»
												final long id;
												«IF considerSubTypes»
													«FOR st : subComplexTV.type.knownSubTypes.sortTopologically SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + o.getClass()); }'''»
														if(o instanceof «st.RESTTOName») {
															«IF st.isAbstract»
																throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
															«ELSE»
																id = «st.RESTControllerSimpleName».create(dywaName);
															«ENDIF»
														}
													«ENDFOR»
												«ELSE»
													id = «subComplexTV.type.RESTControllerSimpleName».create(dywaName);
												«ENDIF»
												o.setDywaId(id);
												
												//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
												«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)o);
												«name.escapeJava».add(«subComplexTV.type.controllerSimpleName».read(o.getDywaId()));
											«ENDIF»
										}
									«ENDFOR»
								«ENDIF»
							}
							// transient object
							else if (o.getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
								«FOR subComplexTV: typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + o.getClass()); }'''»
									if (o instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
										«IF subComplexTV.type.isAbstract && !considerSubTypes»
											throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
										«ELSE»
											«subComplexTV.preprocessObject(packageName, "o", name)»
											
											«subComplexTV.createTransientObject(packageName, "o")»
											
											//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
											«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)o, transientObject);
											«name.escapeJava».add(transientObject);
										«ENDIF»
									}
								«ENDFOR»
							}
							// regular object
						«ELSE»
							// for enum types ignore non-persisted objects
							if (o.getDywaId() < 0) {
								«name.escapeJava».add(null);
							}
						«ENDIF»
							else {
								«FOR subComplexTV: typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else "»
									if (o instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
										«IF !serverSideSource»
											//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
											«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)o);
										«ENDIF»
										«name.escapeJava».add(«subComplexTV.type.controllerSimpleName».read(o.getDywaId()));
									}
								«ENDFOR»
							}
						}
					}
				«ELSE»
					if («variableName».get«name.toFirstUpper.escapeJava»() != null) {
					«IF !isEnum»
						// create new object
						if («variableName».get«name.toFirstUpper.escapeJava»().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
							«IF serverSideSource»
								throw new java.lang.IllegalArgumentException("The server never returns objects with id '-1'");
							«ELSE»
								«FOR subComplexTV: typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else " AFTER  ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «variableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
									if («variableName».get«name.toFirstUpper.escapeJava»() instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
										«IF subComplexTV.type.isAbstract && !considerSubTypes»
											throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
										«ELSE»
											«subComplexTV.preprocessObject(packageName, '''«variableName».get«name.toFirstUpper.escapeJava»()''', name)»
											final long id;
											«IF considerSubTypes»
												«FOR st: subComplexTV.type.knownSubTypes.sortTopologically SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «variableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
													if («variableName».get«name.toFirstUpper.escapeJava»() instanceof «st.RESTTOName») {
														«IF st.isAbstract»
															throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
														«ELSE»
															id = «st.RESTControllerSimpleName».create(dywaName);
														«ENDIF»
													}
												«ENDFOR»
											«ELSE»
												id = «subComplexTV.type.RESTControllerSimpleName».create(dywaName);
											«ENDIF»
											«variableName».get«name.toFirstUpper.escapeJava»().setDywaId(id);
											//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
											«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)«variableName».get«name.toFirstUpper.escapeJava»());
											«name.escapeJava» = («dywaTOName») «subComplexTV.type.controllerSimpleName».read(«variableName».get«name.toFirstUpper.escapeJava»().getDywaId());
										«ENDIF»
									}
								«ENDFOR»
							«ENDIF»
						}
						// transient object
						else if («variableName».get«name.toFirstUpper.escapeJava»().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
							«FOR subComplexTV: typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «variableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
								if («variableName».get«name.toFirstUpper.escapeJava»() instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
									«IF subComplexTV.type.isAbstract && !considerSubTypes»
										throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
									«ELSE»
										«subComplexTV.preprocessObject(packageName, '''«variableName».get«name.toFirstUpper.escapeJava»()''', name)»
										«subComplexTV.createTransientObject(packageName, '''«variableName».get«name.toFirstUpper.escapeJava»()''')»
										
										//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
										«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)«variableName».get«name.toFirstUpper.escapeJava»(), transientObject);
										«name.escapeJava» = («dywaTOName») transientObject;
									«ENDIF»
								}
							«ENDFOR»
						}
						// regular object
					«ELSE»
						// for enum types ignore non-persisted objects
						if («variableName».get«name.toFirstUpper.escapeJava»().getDywaId() < 0) {
							«name.escapeJava» = null;
						}
					«ENDIF»
						else {
							«FOR tvCompound: typeViewCompound.value SEPARATOR "else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «variableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
								«val subComplexTV = tvCompound as BaseComplexTypeView»
								if («variableName».get«name.toFirstUpper.escapeJava»() instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
									«IF !serverSideSource»
										//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
										«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)«variableName».get«name.toFirstUpper.escapeJava»());
									«ENDIF»
									«name.escapeJava» = («dywaTOName») «subComplexTV.type.controllerSimpleName».read(«variableName».get«name.toFirstUpper.escapeJava»().getDywaId());
								}
							«ENDFOR»
						}
					}
					else {
						«name.escapeJava» = null;
					}
				«ENDIF»
			«ELSEIF firstCompond instanceof PrimitiveTypeView»
				«val primitiveTV = firstCompond as PrimitiveTypeView»
				«IF primitiveTV.primitiveType == PrimitiveType.FILE»
					«IF primitiveTV.isList»
						«name.escapeJava» = new java.util.LinkedList<>();
						
						if («variableName».get«name.toFirstUpper.escapeJava»() != null) {
							for (final «primitiveTV.generateInnerBasicTOName(packageName)» o: «variableName».get«name.toFirstUpper.escapeJava»()) {
								«name.escapeJava».add(domainFileController.getFileReference(o.getDywaId()));
							}
						}
					«ELSE»
						if («variableName».get«name.toFirstUpper.escapeJava»() != null) {
							«name.escapeJava» = domainFileController.getFileReference(«variableName».get«name.toFirstUpper.escapeJava»().getDywaId());
						}
						else {
							«name.escapeJava» = null;
						}
					«ENDIF»
				«ELSE»
				 	«name.escapeJava» = «variableName».get«name.toFirstUpper.escapeJava»();
				«ENDIF»
			«ENDIF»
		«ENDFOR»
	'''
	
	private static def preprocessObject(BaseComplexTypeView tv, CharSequence packageName, CharSequence attr, CharSequence name) '''
		final java.lang.String dywaName;
		if («attr».getDywaName() == null || «attr».getDywaName().isEmpty()) {
			dywaName = "«name»";
		} else {
			dywaName = «attr».getDywaName();
		}
	'''

	private static def createTransientObject(BaseComplexTypeView tv, CharSequence packageName, CharSequence name) '''
		«val subTypes = tv.type.knownSubTypes.sortTopologically»
		«IF subTypes.isEmpty»
			final «tv.generateInnerDywaTOName(packageName)» transientObject = «tv.type.controllerSimpleName».createTransient(dywaName);
		«ELSE»
			final «tv.generateInnerDywaTOName(packageName)» transientObject;
			«FOR t: subTypes SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «name».getClass()); }'''»
				if («name» instanceof «t.RESTTOName») {
					«IF t.isAbstract»
						throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
					«ELSE»
						transientObject = «t.controllerSimpleName».createTransient(dywaName);
					«ENDIF»
				}
			«ENDFOR»
		«ENDIF»
	'''

	static def generateCheckForInteraction(GuardContainer guardContainer) '''
		private boolean checkInteractionFor«guardContainer.id.escapeJava»(final long interactionId) {
			
			final «"Interaction".processContextsTypeName» interaction = interactionController.read(interactionId);
			
			if (interaction instanceof «guardContainer.interactionTypeName.processContextsTypeName») {
				final «guardContainer.interactionTypeName.processContextsTypeName» currInteraction = («guardContainer.interactionTypeName.processContextsTypeName») interaction;
				
				// TODO: Use correct interaction inputs
				if (!«guardContainer.rootElement.id.escapeJava».checkInteraction«guardContainer.id.escapeJava»(currInteraction, currInteraction.getinteractionInputs())) {
					return false;
				}
				
				return true;
			}
			else {
				return false;
			}
		}
	'''
	
	static def generateCheckForGuardProcess(Map<GuardProcessSIB, Map<PortRepresentant, TypeView>> guardProcessMap, CharSequence methodIdentifier, CharSequence securityInputClass) '''
		private boolean checkGuardContainersFor«methodIdentifier»(«securityInputClass» secCtx) {
			
			«val extension BackendProcessGeneratorUtil = new BackendProcessGeneratorUtil()»
			«FOR guardProcessEntry: guardProcessMap.entrySet»
				«val guardProcessSIB        = guardProcessEntry.key»
				«val guardProcess           = guardProcessSIB.securityProcess»
				«val guardProcessInputMap   = guardProcessEntry.value»
				«val guardProcessInputNames = guardProcess.startSIB.outputs.map[name].sort»
				«val nameToInputMap         = guardProcessEntry.value.keySet.toMap[name]»
				«val userTypeName           = guardProcess.concreteUserType.dyWATypeName»
				{
					// inputs for «guardProcess.simpleTypeName»
					final «securityInputClass».«guardProcessSIB.label.toFirstUpper.escapeJava»«guardProcessSIB.id.escapeJava» secInputs = secCtx.get«guardProcessSIB.label.escapeJava»«guardProcessSIB.id.escapeJava»();
					
					«userTypeName» currentUser = this.currentUserController.getCurrentUser(«userTypeName».class);
					«guardProcess.typeName».«guardProcess.simpleTypeName»Result result = this.«guardProcess.simpleTypeName».execute(
						«FOR inputName: guardProcessInputNames SEPARATOR ", "»
							«IF inputName == "currentUser"»
								currentUser
							«ELSE»
								«val input = nameToInputMap.get(inputName)»
								«generateSecurityProcessInputLookup(input, guardProcessInputMap.get(input))»
							«ENDIF»
						«ENDFOR»
					);
					
					if (!"granted".equals(result.getBranchName())) {
						return false;
					}
				}
			«ENDFOR»
			
			return true;
		}
	'''
	
	static def generateSecurityProcessInputLookup(PortRepresentant input, TypeView typeView) '''
		«IF typeView === null /* null if static */»
			«input.resolveStaticValue»
		«ELSE»
			«val isComplex = (typeView instanceof ComplexTypeView)»
			«val isFile = if (typeView instanceof PrimitiveTypeView) (typeView.primitiveType == PrimitiveType.FILE) else false»
			«val lookupRequired = isComplex || isFile»
			
			«IF lookupRequired»
				«IF typeView.isList»
					secInputs.get«typeView.name.escapeJava»()
						.stream()
						.map(«typeView.generateInnerBasicTOName(DyWAExtension.dywaPkg)»::getDywaId)
						.map(«
							IF isFile
								»domainFileController::getFileReference«
							ELSE
								»«(typeView as ComplexTypeView).type.name.escapeJava»Controller::read«
							ENDIF
						»)
						.collect(java.util.stream.Collectors.toList())
				«ELSE»
					«IF isFile
						»domainFileController.getFileReference«
					ELSE
						»«(typeView as ComplexTypeView).type.name.escapeJava»Controller.read«
					ENDIF»
					(secInputs.get«typeView.name.escapeJava»().getDywaId())
				«ENDIF»
			«ELSE»
				secInputs.get«typeView.name.escapeJava»()
			«ENDIF»
		«ENDIF»
	'''
	
	static def generateInteractableInputs(Map.Entry<PortRepresentant, TypeView> it) {
		if (value === null) { // null if static
			key.resolveStaticValue
		}
		else {
			value.name.escapeJava
		}
	}
	
	static def resolveStaticValue(PortRepresentant input) {
		switch (it: input) {
			case isStaticBoolean:   staticBooleanValue
			case isStaticInt:       '''«staticIntValue»l'''
			case isStaticReal:      '''«staticRealValue»d'''
			case isStaticText:      '''"«staticTextValue»"'''
			case isStaticTimestamp: '''new java.util.Date(«staticTimestampValue»l)'''
			default:                "null"
		}
	}
	
	static def generateSecurityInputClass(Map<GuardProcessSIB, Map<PortRepresentant, TypeView>> inputMap, CharSequence securityInputPackage, CharSequence securityInputClass, CharSequence packageName) '''
		package «securityInputPackage»;
		
		public class «securityInputClass» {
			
			«FOR entry: inputMap.entrySet» 
				«val gcs = entry.key»
				«val inputs = entry.value»
				
				private «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» «gcs.label.escapeJava»«gcs.id.escapeJava»;
					
				@com.fasterxml.jackson.annotation.JsonProperty("«gcs.label.escapeString»«gcs.id.escapeString»")
				public «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» get«gcs.label.escapeJava»«gcs.id.escapeJava»() {
					return this.«gcs.label.escapeJava»«gcs.id.escapeJava»;
				}

				@com.fasterxml.jackson.annotation.JsonProperty("«gcs.label.escapeString»«gcs.id.escapeString»")
				public void set«gcs.label.escapeJava»«gcs.id.escapeJava»(final «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» «gcs.label.escapeJava»«gcs.id.escapeJava») {
					this.«gcs.label.escapeJava»«gcs.id.escapeJava» = «gcs.label.escapeJava»«gcs.id.escapeJava»;
				}
				
				static class «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» {
					«FOR tv: inputs.values.filterNull.toList /* null if static */»
						private «generateBasicTOName(tv, packageName)» «tv.name.escapeJava»;
							
						@com.fasterxml.jackson.annotation.JsonProperty("«tv.name.escapeString»")
						public «generateBasicTOName(tv, packageName)» get«tv.name.escapeJava»() {
							return this.«tv.name.escapeJava»;
						}
						
						@com.fasterxml.jackson.annotation.JsonProperty("«tv.name.escapeString»")
						public void set«tv.name.escapeJava»(final «generateBasicTOName(tv, packageName)» «tv.name.escapeJava») {
							this.«tv.name.escapeJava» = «tv.name.escapeJava»;
						}
					«ENDFOR»
				}
			«ENDFOR»
		}
	'''
	
	static def generateInputWrapperClassForSecuredProcess(CharSequence packageName, CharSequence wrapperName, CharSequence inputClass, CharSequence inputName, CharSequence securityClassForInteractable, CharSequence securityClassForInteraction) '''
		package «packageName»;
		
		public class «wrapperName» {
			
			«IF inputClass !== null»
				private «packageName».«inputClass» «inputName»;
			«ENDIF»
			«IF securityClassForInteractable !== null»
				private «packageName».«securityClassForInteractable» securityInputsForInteractable;
			«ENDIF»
			«IF securityClassForInteraction !== null»
				private «packageName».«securityClassForInteraction» securityInputsForInteraction;
			«ENDIF»
			
			«IF inputClass !== null»
				@com.fasterxml.jackson.annotation.JsonProperty("«inputName»")
				public «packageName».«inputClass» get«inputName.toString.toFirstUpper»() {
					return this.«inputName»;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("«inputName»")
				public void set«inputName.toString.toFirstUpper»(final «packageName».«inputClass» «inputName») {
					this.«inputName» = «inputName»;
				}
			«ENDIF»

			«IF securityClassForInteractable !== null»
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteractable")
				public «packageName».«securityClassForInteractable» getSecurityInputsForInteractable() {
					return this.securityInputsForInteractable;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteractable")
				public void setSecurityInputsForInteractable(final «packageName».«securityClassForInteractable» securityInputsForInteractable) {
					this.securityInputsForInteractable = securityInputsForInteractable;
				}
			«ENDIF»

			«IF securityClassForInteraction !== null»
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteraction")
				public «packageName».«securityClassForInteraction» getSecurityInputsForInteraction() {
					return this.securityInputsForInteraction;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteraction")
				public void setSecurityInputsForInteraction(final «packageName».«securityClassForInteraction» securityInputsForInteraction) {
					this.securityInputsForInteraction = securityInputsForInteraction;
				}
			«ENDIF»
		}
	'''
	
	protected static def renderDeclaringRestType(ComplexTypeView typeView, FieldView fieldView) {
		'''«restTypePkg».«typeView.getDeclaringType(fieldView).name.escapeJava»'''
	}
	
	protected static def renderDeclaringDywaType(ComplexTypeView typeView, FieldView fieldView) {
		typeView.getDeclaringType(fieldView).dyWATypeName
	}
	
	// FIXME move to GUIExtension
	protected static def getDeclaringType(ComplexTypeView typeView, FieldView fieldView) {
		switch (data: fieldView.data) {
			PrimitiveAttribute:                        data.container.originalType
			ComplexAttribute:                          data.container.originalType
			ExtensionAttribute:                        data.container.originalType
			info.scce.dime.gui.gui.PrimitiveAttribute: data.attribute.container.originalType
			info.scce.dime.gui.gui.ComplexAttribute:   data.attribute.container.originalType
			info.scce.dime.gui.gui.ExtensionAttribute: data.attribute.container.originalType
			ComplexVariable:                           getDeclaringType(data)
			default:                                   typeView.type.originalType
		}
	}
	
	// FIXME move to GUIExtension
	private static def getDeclaringType(ComplexVariable data) {
		if (data.complexVariablePredecessors.isEmpty) {
			throw new IllegalStateException("Top level variable declaring type not calculatable: " + data)
		}
		val dataDataType = data.dataType.originalType
		val effectiveType = data.complexVariablePredecessors.head.dataType
		val superTypes = #[effectiveType] + effectiveType.superTypes
		val type = superTypes.findFirst [
			attributes.map[originalAttribute].exists [
				isComplex &&
				name == data.name &&
				complexDataType.originalType == dataDataType
			]
		]
		if (type === null) {
			throw new IllegalStateException("Declaring type not calculatable: " + data)
		}
		return type.originalType
	}
	
	// FIXME move to GUIExtension
	protected static def needRuntimeCheck(ComplexTypeView typeView, FieldView fieldView) {
		typeView.type.originalType != typeView.getDeclaringType(fieldView)
	}
	
	static def getPossibleConcreteSubTypes(ComplexTypeView typeView) {
		typeView
			.displayedFields
			.filter [ fieldView | typeView.needRuntimeCheck(fieldView) ]
			.map [ fieldView | typeView.getDeclaringType(fieldView) ]
			.reject(AbstractType)
			.toSet
	}
	
	static def renderCacheLookup(ComplexTypeView view, CharSequence source, CharSequence target) '''
		«view.generateInnerBasicTOName(DyWAExtension.dywaPkg)» «target» = objectCache.getRestTo(«source»);
		
		if («target» == null) {
			«target» = «view.type.RESTTOName».fromDywaEntity(«source», objectCache);
		}
		
		if (!objectCache.containsSelective(«target», "«view.selectiveNameJava»")) {
			«restTypePkg».«view.selectiveNameJava».copy(«source», «target», objectCache);
		}
	'''
	
	static def typeRESTControllerInjection(Type t) '''
		@javax.inject.Inject
		private «t.RESTControllerName» «t.RESTControllerSimpleName»;
		@javax.inject.Inject
		private «t.controllerTypeName» «t.controllerSimpleName»;
	'''
	
}
