/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.process.BackendProcessGeneratorUtil
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.process.EndSIB
import java.nio.file.Path
import java.util.Collections
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Map.Entry
import java.util.Set

import static extension info.scce.dime.generator.gui.data.SelectiveExtension.*
import static extension info.scce.dime.generator.rest.DyWAAbstractGenerator.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import static extension info.scce.dime.gui.helper.GUIExtension.lastModified2

class GUIProgressGenerator extends BackendProcessGeneratorUtil {

	extension GUIExtension = new GUIExtension
	// use new instance because the superSelectives may clash with existing ones
	var SelectiveCache selectiveCache
	
	val packageName = "info.scce.dime.rest.gui"
	val modelPackageName = packageName + ".model"
	
	def generate(GenerationContext genctx, Path outlet) {
		_gUIExtension = genctx.guiExtension
		selectiveCache = GenerationContext.instance.selectiveCache
		val targetDir = outlet.resolve("app-business/target/generated-sources")
	
		val branchCache = new HashMap

		val guiBranchRepresentatives = new HashMap
		val Map<Map<String, TypeView>, GUIBranch> guiBranchMapping = new HashMap

		val endSIBRepresentatives = new HashMap
		val endSIBMapping = new HashMap
		// since TypeView's equality ignore their isList property, store this information separately
		val isListMapping = new HashMap

		// pre-fetch selective information
		for (gui : genctx.usedGUIs) {
			val branches = gui.getGUIBranches(true)
			branchCache.put(gui, branches)
			if (!branches.isEmpty) {
				val Set<Type> types = new HashSet
				val Set<BaseComplexTypeView> complexTypeViews = new HashSet

				val guiBranchTypeViews = genctx.computeBranchTypeViews(gui, types, complexTypeViews)
				for (entry : guiBranchTypeViews.entrySet) {
					val guiBranch = entry.key
					val representative = new HashMap
					val listMapping = new HashMap
					for (typeView : entry.value.entrySet) {
						val superTypeView = typeView.value.commonSuperTypeView
						val rep = if (superTypeView instanceof BaseComplexTypeView) {
							selectiveCache.getRepresentative(superTypeView)
						} else {
							superTypeView
						}

						representative.put(typeView.key, rep)
						listMapping.put(typeView.key, superTypeView.isList)
					}
					if (!guiBranchMapping.containsKey(representative)) {
						guiBranchMapping.put(representative, guiBranch);
						isListMapping.put(guiBranch, listMapping);
					}
					guiBranchRepresentatives.put(guiBranch, guiBranchMapping.get(representative))
				}

				val processBranchTypeViews = gui.computeProcessTypeViews(types)
				for (entry : processBranchTypeViews.entrySet) {
					val endSIB = entry.key
					val representative = endSIB.rootElement
					endSIBMapping.putIfAbsent(representative, endSIB);
					endSIBRepresentatives.put(endSIB, endSIBMapping.get(representative))
				}
			}
		}

		/*
		 * CODE GENERATION
		 * Generate code for the input TO interfaces
		 * Generate code for the /continue Endpoints for all branches of a GUI
		 * differentiating between branches based on a Button (GUI) and an EndSIB (embedded Process)
		 */
		for (entry: guiBranchMapping.entrySet) {
			val branch = entry.value
			val listMapping = isListMapping.get(branch)
			DyWAAbstractGenerator.generate(
				generateGUIResumeInput(branch.buildInputClassName + "Branch", entry.key, null, [s | listMapping.get(s)]).toString(),
				modelPackageName + '.',
				buildInputClassName(branch) + "Branch.java",
				targetDir,
				0
			)
		}

		for (entry : endSIBMapping.entrySet) {
			val endSIB = entry.value
			DyWAAbstractGenerator.generate(
				generateGUIResumeInput(endSIB.buildInputClassName + "EndSIB", Collections.emptyMap, endSIB, [s|false]).toString(),
				modelPackageName + '.',
				buildInputClassName(endSIB) + "EndSIB.java",
				targetDir,
				0
			)
		}

		for (gui : genctx.usedGUIs) {
			val branches = branchCache.get(gui)
			if (!branches.isEmpty) {
				val Set<Type> types = new HashSet
				val Set<BaseComplexTypeView> complexTypeViews = new HashSet

				val guiBranchTypeViews = genctx.computeBranchTypeViews(gui, types, complexTypeViews)
				val processBranchTypeViews = gui.computeProcessTypeViews(types)
				
				DyWAAbstractGenerator.generate(
					generateGUIResumer(gui,types,complexTypeViews,guiBranchTypeViews,processBranchTypeViews,guiBranchRepresentatives,endSIBRepresentatives).toString(),
					packageName + '.',
					gui.buildHandlerClassName + ".java",
					targetDir,
					gui.lastModified2
				)
				
				val map = new HashMap
				map.putAll(guiBranchTypeViews.entrySet.toMap(['''gui_«it.key.element.id»«it.key.name»'''],[it.value]))
				map.putAll(processBranchTypeViews.entrySet.toMap(['''process_«it.key.id»«it.key.branchName»'''],[it.value]))
				
				DyWAAbstractGenerator.generate(
					generateGUIResult(gui,map).toString(),
					gui.pkg + '.',
					gui.getResultTypeName + ".java",
					targetDir,
					gui.lastModified2
				)
			}
		}	
	}
	
	def generateGUIResumer(GUI gui,
		Set<Type> types,
		Set<BaseComplexTypeView> complexTypeViews,
		Map<GUIBranch,Map<String,List<TypeView>>> guiBranchTypeViews,
		Map<EndSIB,Map<String,List<TypeView>>> processBranchTypeViews,
		Map<GUIBranch, GUIBranch> guiRepresentatives,
		Map<EndSIB, EndSIB> endSIBRepresentatives
	) '''
		// generated by «class.name»#generateGUIResumer
		package «packageName»;
		
		@javax.transaction.Transactional(dontRollbackOn = info.scce.dime.exception.GUIEncounteredSignal.class)
		@javax.ws.rs.Path("/continue/«gui.id.escapeJava»")
		public class «gui.buildHandlerClassName» extends info.scce.dime.process.GUIResumer {
			
			@javax.inject.Inject
			private info.scce.dime.gui.ProcessResumer processResumer;
			
			// controller for fetching dywa file entities
			@javax.inject.Inject
			private info.scce.dime.util.DomainFileController domainFileController;
			
			«(complexTypeViews.map[type] + types).map[knownSubTypes].flatten.toSet.map[typeRESTControllerInjection].join»
		
			«FOR branch : guiBranchTypeViews.entrySet»
				«val branchName = branch.key.name»
				«val branchIdName = '''«branch.key.element.id»«branch.key.name»'''»
				«val outputs = branch.value»
				@javax.ws.rs.POST
				@javax.ws.rs.Path("«branchName.escapeJava»/branch/public")
				@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				public javax.ws.rs.core.Response continue«branchName.escapeJava»Branch(«buildFQInputClassName(guiRepresentatives.get(branch.key))»Branch output) {
					
					checkAuthentication(output.getCallStack(),"«gui.id»/«branchName.escapeJava»");
					
					«IF !outputs.empty»
						«DyWAAbstractGenerator.copyParametersIntoLocalVariables(outputs,"output","de.ls5.dywa.generated")»
						final «gui.getExternalResultReturnTypeName('''gui_«branchIdName»''')» guiReturn = new «gui.getExternalResultReturnTypeName('''gui_«branchIdName»''')»();
						«FOR output : outputs.entrySet»
								guiReturn.set«output.key.toFirstUpper.escapeJava»(«output.key.escapeJava»);
						«ENDFOR»
					«ENDIF»
					
					final «gui.getExternalResultTypeName» guiResult = 
					«IF outputs.empty»
						new «gui.getExternalResultTypeName»("«branchName.escapeJava»");
					«ELSE»
						new «gui.getExternalResultTypeName»("«branchName.escapeJava»", guiReturn);
					«ENDIF»
					if(!"«gui.id»".equals(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getPointer().split(":")[2])){
								output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).setPointer(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getMajorGUI());
					}
					if(!output.getCallStack().getCallFrames().isEmpty()) {
					
						if(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size() - 1).getPointer() == null) {
							return javax.ws.rs.core.Response.status(422).build();
						}
					}
					final Object result = this.processResumer.resumeFromGUI(output.getCallStack(), guiResult);
					return javax.ws.rs.core.Response.ok(result).build();
				}
			«ENDFOR»
			
			«FOR branch : processBranchTypeViews.entrySet»	
				«val outputs = branch.value»
				«val branchName = branch.key.branchName»
				«val branchId = branch.key.id»
				@javax.ws.rs.POST
				@javax.ws.rs.Path("«branchId.escapeJava»/«branchName.escapeJava»/endsib/public")
				@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				public javax.ws.rs.core.Response continue«branchId.escapeJava»«branchName.escapeJava»EndSIB(«buildFQInputClassName(endSIBRepresentatives.get(branch.key))»EndSIB output) {
					
					checkAuthentication(output.getCallStack(),"«gui.id»/«branchName.escapeJava»");
					
					«IF !outputs.empty»
						
						final «gui.getExternalResultReturnTypeName('''process_«branch.key.id»«branch.key.branchName»''')» guiReturn = new «gui.getExternalResultReturnTypeName('''process_«branch.key.id»«branch.key.branchName»''')»();
						«DyWAAbstractGenerator.copyParametersIntoLocalVariables(outputs, "output.getOutputs().get"+branchName.escapeJava+"()", "de.ls5.dywa.generated", true)»
						«FOR output : outputs.entrySet»
								guiReturn.set«output.key.toFirstUpper.escapeJava»(«output.key.escapeJava»);
						«ENDFOR»
					«ENDIF»
					
					final «gui.getExternalResultTypeName» guiResult = 
					«IF outputs.empty»
						new «gui.getExternalResultTypeName»("«branchName.escapeJava»");
					«ELSE»
						new «gui.getExternalResultTypeName»("«branchName.escapeJava»", guiReturn);
					«ENDIF»
					if(!"«gui.id»".equals(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getPointer().split(":")[2])){
						output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).setPointer(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getMajorGUI());
					}
					if(!output.getCallStack().getCallFrames().isEmpty()) {
					
						if(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size() - 1).getPointer() == null) {
							return javax.ws.rs.core.Response.status(422).build();
						}
					}
					final Object result = this.processResumer.resumeFromGUI(output.getCallStack(), guiResult);
					return javax.ws.rs.core.Response.ok(result).build();
				}
			«ENDFOR»
		}
	'''
	
	def getToRestType(Entry<String, TypeView> entry) {
		if(entry.value.isPrimitive) {
			if((entry.value as PrimitiveTypeView).primitiveType == PrimitiveType.FILE) {
				if(entry.value.isList) {
						return '''«entry.key.escapeJava».stream().map((n)=>new de.ls5.dywa.generated.rest.util.FileReference(n)).collect(java.util.stream.Collectors.toList())'''
				}
				return '''new de.ls5.dywa.generated.rest.util.FileReference(«entry.key.escapeJava»)'''
			}	
		}
		entry.key.escapeJava
	}
	
	def generateGUIResumeInput(CharSequence className, Map<String,TypeView> inputs, EndSIB endsib, (String) => Boolean isList) '''
	
			// generated by «class.name»#generateGUIResumeInput
			
			package «modelPackageName»;
			
			public class «className»{
				

				info.scce.dime.process.ProcessCallFrame callStack;
				
				@com.fasterxml.jackson.annotation.JsonProperty("dywaData")
				@com.codingrodent.jackson.crypto.Encrypt
				@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = info.scce.dime.rest.ContextIndependentSerializer.class)
				@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = info.scce.dime.rest.ContextIndependentDeserializer.class)
				public info.scce.dime.process.ProcessCallFrame getCallStack() {
					return callStack;
				}
				

				@com.fasterxml.jackson.annotation.JsonProperty("dywaData")
				public void setCallStack(info.scce.dime.process.ProcessCallFrame callStack) {
					this.callStack = callStack;
				}
				«IF endsib!=null»
				«val outputName = '''«endsib.rootElement.renderOutputForInteractable(null)»'''»
				«val outputClass = '''de.ls5.dywa.generated.rest.process.interactable.«outputName».«outputName»Wrapper'''»
				private «outputClass» outputs;
				@com.fasterxml.jackson.annotation.JsonProperty("outputs")
				@com.codingrodent.jackson.crypto.Encrypt
				@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = info.scce.dime.rest.ContextIndependentSerializer.class)
				@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = info.scce.dime.rest.ContextIndependentDeserializer.class)
				public «outputClass» getOutputs() {
					return outputs;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("outputs")
				public void setOutputs(«outputClass» outputs) {
					this.outputs = outputs;
				}
				«ELSE»
					«FOR input : inputs.entrySet»
						«val key = input.key»
						«getterSetter(input.value.generateInnerRestTOName(DyWAExtension.dywaPkg), key, isList.apply(key))»
					«ENDFOR»
				«ENDIF»
			}
	'''
	
	
	def generateGUIResult(GUI gui,Map<String,Map<String,List<TypeView>>> branches)
		'''
			// Generator: «class.name»#generateGUIResult(GUI gui,Map<String,Map<String,TypeView>> branches)
			
			package «gui.pkg»;
			
			public class «gui.getResultTypeName» {
				
				private String branchName;
				
				public String getBranchName() {
					return this.branchName;
				}
				
				
				public «gui.getResultTypeName»(String branchName) {
					this.branchName = branchName;
				}
				
				«FOR e : branches.entrySet»
					«val branchName = e.key»
					«val ports = e.value.entrySet.toMap([key],[DyWAAbstractGenerator.getCommonSuperTypeView(value)])»
					«val className = '''«branchName.escapeJava»Return'''»
					«val variableName = '''«branchName.escapeJava»Return'''»
					
					public «gui.getResultTypeName»(String branchName, «className» «variableName») {
						this.branchName = branchName;
						this.«variableName» = «variableName»;
					}
					
					private «className» «variableName»;
					
					public «className» get«className»() {
						return «variableName»;
					}
					
					public static class «className» {
						«FOR port : ports.entrySet»
							«IF port.value instanceof PrimitiveTypeView»
								«(port.value as PrimitiveTypeView).primitiveType.getDyWaLiteral().getterSetter(port.key, port.value.isList)»
							«ELSEIF port.value instanceof BaseComplexTypeView»
								«(port.value as BaseComplexTypeView).generateInnerDywaTOName(packageName).getterSetter(port.key.escapeJava.toFirstUpper,port.value.isList)»
							«ENDIF»
						«ENDFOR»
					}
				«ENDFOR»
			}
		'''
	
	def getterSetter(CharSequence noListType, String name, boolean isList) {
		val type =
			if (!isList) noListType
			else '''java.util.List<«noListType»>'''
		val varName = name.escapeJava
		val methodName = name.toFirstUpper.escapeJava
		'''
			private «type» «varName»;
			
			@com.fasterxml.jackson.annotation.JsonProperty("«name»")
			public «type» get«methodName»() {
				return «varName»;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("«name»")
			public void set«methodName»(«type» «varName») {
				this.«varName» = «varName»;
			}
		'''
	}
	
	
	def buildHandlerClassName(GUI gui) '''«gui.title.escapeJava»GUIResumer'''
	def buildInputClassName(GUIBranch guiBranch) '''«guiBranch.element.id.escapeJava»'''
	def buildFQInputClassName(GUIBranch guiBranch) '''«modelPackageName».«buildInputClassName(guiBranch)»'''
	def buildInputClassName(EndSIB endSIB) '''«endSIB.id.escapeJava»'''
	def buildFQInputClassName(EndSIB endSIB) '''«modelPackageName».«buildInputClassName(endSIB)»'''
	
}
