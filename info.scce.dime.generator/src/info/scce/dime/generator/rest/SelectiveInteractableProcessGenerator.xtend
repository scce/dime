/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import graphmodel.Node
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.process.BackendProcessGeneratorHelper
import info.scce.dime.generator.process.BackendProcessGeneratorUtil
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EntryPointProcessSIB
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import java.nio.file.Path
import java.util.HashSet
import java.util.Map

import static info.scce.dime.generator.util.DyWAExtension.*

import static extension info.scce.dime.generator.rest.DyWAAbstractGenerator.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import static extension info.scce.dime.gui.helper.GUIExtension.lastModified2
import info.scce.dime.generator.gui.dart.functionality.FrontEndProcessExtension

class SelectiveInteractableProcessGenerator extends BackendProcessGeneratorUtil {
	
	val completePackage = restPkg + ".process.interactable"
	
	val extension BackendProcessGeneratorHelper
	extension FrontEndProcessExtension = new FrontEndProcessExtension
	
	new(BackendProcessGeneratorHelper helper) {
		this._backendProcessGeneratorHelper = helper
	}

//	def generate(Process process, GUICompoundView cv, Map<String, Map<String, Collection<TypeView>>> ouputs, IPath outlet, Node sib) {
//	def generate(Process process, GUICompoundView cv, IPath outlet) {
	def generate(Process process, Path outlet, GenerationContext genctx, boolean isInDAD) {
		
		val targetDir = outlet.resolve("app-business/target/generated-sources")
		val processModified = process.lastModified2
		// rest ctrl
		{
			val String content = generateController(process, process.processInputs, genctx, isInDAD).toString();
			val String fileName = process.simpleTypeName + "Controller.java";
			
			DyWAAbstractGenerator.generate(content, completePackage + '.', fileName, targetDir,processModified);
		}
		
		// process inputs
		{
			val String content = generateInputs(process).toString();
			val String fileName = process.renderInputForInteractable(null) + ".java";
			
			DyWAAbstractGenerator.generate(content, completePackage + '.', fileName, targetDir,processModified);
		}
		
		// process entry point inputs using real objects
		process.entryPointProcessSIBs.forEach[ep|{
			val String content = generateInputs(process, ep).toString();
			val String fileName = process.renderInputForEntryPoint(ep) + ".java";
			
			DyWAAbstractGenerator.generate(content, completePackage + '.', fileName, targetDir,processModified);
		}]
		
		// process outputs
		{
			val String content = generateOutputs(process).toString();
			val String fileName = process.renderOutputForInteractable(null) + ".java";
			
			DyWAAbstractGenerator.generate(content, completePackage + '.', fileName, targetDir,processModified);
		}
	}
	
	def void generate(Process process, Map<InputPort, TypeView> inputs, Path outlet, ProcessSIB sib, GenerationContext genctx) {
		
		val targetDir = outlet.resolve("app-business/target/generated-sources")
		val fixedTypeViews = inputs.entrySet.map[e | 
			val copy = switch it : e.value {
				PrimitiveTypeView: 
					new PrimitiveTypeView(it)
				ComplexTypeView:
					new ComplexTypeView(it)
				default:
					throw new IllegalArgumentException("Unknown type " + e.value)
			}
			copy.setList = e.key.isList
			copy.name = e.key.name
			copy
		]
		
		val processModified = lastModified2(#[process,sib.rootElement])
		// rest ctrl
		{
			
			val String content = generateController(process, fixedTypeViews, sib, genctx).toString();
			val String fileName = process.simpleTypeName + sib.id.escapeJava + "Controller.java";
			
			DyWAAbstractGenerator.generate(content, completePackage + '.', fileName, targetDir,processModified);
		}
		
		// process inputs using real objects
		{
			val String content = generateInputs(process, fixedTypeViews, sib).toString();
			val String fileName = process.renderInputForInteractable(sib) + ".java";
			
			DyWAAbstractGenerator.generate(content, completePackage + '.', fileName, targetDir,processModified);
		}
		
		
		// process outputs no typeview information is needed, since processresult is needed 
		{
			val String content = generateOutputs(process).toString();
			val String fileName = process.renderOutputForInteractable(null) + ".java";
			
			DyWAAbstractGenerator.generate(content, completePackage + '.', fileName, targetDir,processModified);
		}
	}
	
	private def generateController(Process process, Iterable<TypeView> inputs, ProcessSIB sib, GenerationContext genctx){
		val guardContainers = process.findGuardContainersSibs(genctx, new HashSet)
		 '''
		// generated by «class.name»#generateController
		 
		package «completePackage»;
		
		@javax.transaction.Transactional(dontRollbackOn = info.scce.dime.exception.GUIEncounteredSignal.class)
		@javax.ws.rs.Path("/start/«process.simpleTypeName»/«sib.id.escapeJava»")
		public class «process.simpleTypeName»«sib.id.escapeJava»Controller {
		
			«IF !guardContainers.empty»
			@javax.inject.Inject
			private info.scce.dime.gui.ProcessResumer processResumer;
			«ENDIF»
		
			@javax.inject.Inject
			private info.scce.dime.rest.ObjectCache objectCache;
			@javax.inject.Inject
			private «process.typeName» process;

			«FOR t : inputs.filter(ComplexTypeView).map[type.originalType.knownSubTypes].flatten.toSet»
				«t.typeRESTControllerInjection»
			«ENDFOR»
			
			// controller for fetching dywa entities
			@javax.inject.Inject
			private info.scce.dime.util.DomainFileController domainFileController;
			
			// default controllers
			@javax.inject.Inject
			private «restPkg».user.CurrentUserController currentUserController;
			
			@javax.ws.rs.POST
			@javax.ws.rs.Path("public")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			public javax.ws.rs.core.Response execute(«process.renderInputForInteractable(sib)» ctx) {
				boolean isAuthenticationRequired = false;
				if(ctx.getCallStack()!=null) {
					isAuthenticationRequired = ctx.getCallStack().isAuthenticationRequired();
				}
				if(isAuthenticationRequired) {
						if (!org.apache.shiro.SecurityUtils.getSubject().isAuthenticated()) {
							info.scce.dime.exception.GUIEncounteredSignal sig = new info.scce.dime.exception.GUIEncounteredSignal(ctx.getCallStack(), "«sib.id.escapeJava»");
							sig.setStatus(401);
							throw sig;
						}
				}
				«inputs.copyParametersIntoLocalVariables(dywaPkg)»
				«process.renderOutputForInteractable(null)» result = new «process.renderOutputForInteractable(null)»(this.process.execute(isAuthenticationRequired«(sib.inputPorts + sib.inputStatics).sortBy[name].join(',',', ',"") [inputToValue]»), objectCache);
				return javax.ws.rs.core.Response.ok(result).build();
			}
			
			«FOR guardContainer:guardContainers»
			@javax.ws.rs.POST
			@javax.ws.rs.Path("«guardContainer.id.escapeJava»/public")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			public javax.ws.rs.core.Response continueAfterLogin«guardContainer.id.escapeJava»(info.scce.dime.rest.RESTContext ctx) {
				final Object result = this.processResumer.resumeFromGUI(ctx.getCallStack(),null);
				return javax.ws.rs.core.Response.ok(result).build();
			}
			«ENDFOR»
		}
	'''
	}
	

	private def generateController(Process process, Iterable<OutputPort> outputs, GenerationContext genctx, boolean isInDAD) {
		val guardContainers = process.findGuardContainersSibs(genctx, new HashSet)
		val types = outputs.filter(ComplexOutputPort).map[dataType].toList
		types += process.entryPointProcessSIBs.map[inputPorts].flatten.filter(ComplexInputPort).map[dataType]
		'''
			package «completePackage»;
			
			import java.util.List;
			
			@javax.transaction.Transactional(dontRollbackOn = info.scce.dime.exception.GUIEncounteredSignal.class)
			@javax.ws.rs.Path("/start/«process.simpleTypeName»")
			public class «process.simpleTypeName»Controller {
			
				«IF !guardContainers.empty»
				@javax.inject.Inject
				private info.scce.dime.gui.ProcessResumer processResumer;
				«ENDIF»
			
				@javax.inject.Inject
				private info.scce.dime.rest.ObjectCache objectCache;
				@javax.inject.Inject
				private «process.typeName» process;
				«FOR ops : types.map[originalType.knownSubTypes].flatten.toSet»
					«ops.typeRESTControllerInjection()»
				«ENDFOR»
				
				// controller for fetching dywa entities
				@javax.inject.Inject
				private info.scce.dime.util.DomainFileController domainFileController;
				
					«IF isInDAD»
					@javax.ws.rs.POST
					@javax.ws.rs.Path("public")
					@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
					@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
					public javax.ws.rs.core.Response execute(«process.renderInputForInteractable(null)» ctx) {
						«FOR op : outputs»
							final «op.typeName» «op.name.escapeJava» = «
							switch it : op {
								PrimitiveOutputPort: '''«'''ctx.get«name.escapeJava»()'''.parsePrimitive(dataType,isIsList)»;'''
								ComplexOutputPort: '''«'''ctx.get«name.escapeJava»()'''.parseComplex('''«complexType.name.escapeJava»''',isIsList)»;'''
								default: throw new IllegalArgumentException('''No case for "«class»".''')
							}»
						«ENDFOR»
							«process.renderOutputForInteractable(null)» result = new «process.renderOutputForInteractable(null)»(this.process.execute(false«outputs.sortBy[name].join(',',', ','') [name.escapeJava]»),objectCache);
							return javax.ws.rs.core.Response.ok(result).build();
					}
					
					«FOR entry:process.entryPointProcessSIBs»
					@javax.ws.rs.POST
					@javax.ws.rs.Path("«entry.id.escapeJava»/public")
					@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
					@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
					public javax.ws.rs.core.Response execute(«process.renderInputForEntryPoint(entry)» ctx) {
						«FOR ip : entry.inputPorts»
							final «ip.typeName» «ip.name.escapeJava» = «
							switch it : ip {
								PrimitiveInputPort: '''«'''ctx.get«name.escapeJava»()'''.parsePrimitive(dataType,isIsList)»;'''
								ComplexInputPort: '''«'''ctx.get«name.escapeJava»()'''.parseComplex('''«complexType.name.escapeJava»''',isIsList)»;'''
								default: throw new IllegalArgumentException('''No case for "«class»".''')
							}»
						«ENDFOR»
						«FOR ip : entry.inputStatics»
						final «ip.typeName» «ip.name.escapeJava» = «ip.value»;
						«ENDFOR»
							«process.renderOutputForInteractable(null)» result = new «process.renderOutputForInteractable(null)»(this.process.executeEntry«entry.id.escapeJava»(«entry.inputs.sortBy[name].join(', ') [name.escapeJava]»), objectCache);
							return javax.ws.rs.core.Response.ok(result).build();
					}
					«ENDFOR»
				«ENDIF»
				
				«FOR guardContainer:guardContainers»
				@javax.ws.rs.POST
				@javax.ws.rs.Path("«guardContainer.id.escapeJava»/public")
				@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				public javax.ws.rs.core.Response continueAfterLogin«guardContainer.id.escapeJava»(info.scce.dime.rest.RESTContext ctx) {
					final Object result = this.processResumer.resumeFromGUI(ctx.getCallStack(),null);
					return javax.ws.rs.core.Response.ok(result).build();
				}
				«ENDFOR»
			}
		'''
	
	}
	
	def parsePrimitive(CharSequence getter,PrimitiveType pt,boolean isList) {
		if(pt==PrimitiveType.FILE) {
			if(isList) {
				return '''«getter».stream().map((n)->domainFileController.getFileReference(n.getDywaId())).collect(java.util.stream.Collectors.toList())'''
			}
			return '''«getter»==null?null:domainFileController.getFileReference(«getter».getDywaId())'''
		}
		return '''«getter»'''
	}
	
	def parseComplex(CharSequence getter,CharSequence type,boolean isList) {
		if(isList) {
			return '''«getter».stream().map((n)->«type»Controller.read(n.getDywaId())).collect(java.util.stream.Collectors.toList())'''
		}
		return '''«getter»==null?null:«type»Controller.read(«getter».getDywaId())'''
	}
	
	
	def getValue(InputStatic static1) {
		switch(static1) {
			info.scce.dime.process.process.RealInputStatic: '''«static1.value»d'''
			info.scce.dime.process.process.IntegerInputStatic: '''«static1.value»l'''
			info.scce.dime.process.process.TimestampInputStatic: '''new DateTime("«static1.value»")'''
			info.scce.dime.process.process.TextInputStatic: '''"«static1.value»"'''
			info.scce.dime.process.process.BooleanInputStatic: '''«static1.value»'''
			
		}
	}
	
	private def generateInputs(Process process, Iterable<TypeView> inputs, ProcessSIB sib) '''
	
		// generated by «class.name»#generateInputs
	
		package «completePackage»;
		
		public class «process.renderInputForInteractable(sib)» {
				info.scce.dime.process.ProcessCallFrame callStack;
				
				@com.fasterxml.jackson.annotation.JsonProperty("dywaData")
				@com.codingrodent.jackson.crypto.Encrypt
				@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = info.scce.dime.rest.ContextIndependentSerializer.class)
				@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = info.scce.dime.rest.ContextIndependentDeserializer.class)
				public info.scce.dime.process.ProcessCallFrame getCallStack() {
					return callStack;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("dywaData")
				public void setCallStack(info.scce.dime.process.ProcessCallFrame callStack) {
					this.callStack = callStack;
				}
				
			«FOR tv : inputs»
				«tv.generateBasicTOName(dywaPkg).getterSetter(tv.name,'''«tv.name.escapeJava»«tv.id»''')»
			«ENDFOR»
		}
	'''
	
	def getterSetter(CharSequence type,String name,String attributeName)'''
	private «type» «attributeName»;
		
	@com.fasterxml.jackson.annotation.JsonProperty("«name.escapeString»")
	public «type» get«name.toFirstUpper.escapeJava»() {
		return this.«attributeName»;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("«name.escapeString»")
	public void set«name.toFirstUpper.escapeJava»(final «type» «attributeName») {
		this.«attributeName» = «attributeName»;
	}
	'''
	
	private def generateInputs(Process it) '''
		package «completePackage»;
		
		import java.util.List;
		
		public class «renderInputForInteractable(null)» {

		«FOR it : processInputs»
			private «routerTypeName» «name.escapeJava»;
				
			@com.fasterxml.jackson.annotation.JsonProperty("«name.escapeString»")
			public «routerTypeName» get«name.escapeJava»() {
				return this.«name.escapeJava»;
			}

			@com.fasterxml.jackson.annotation.JsonProperty("«name.escapeString»")
			public void set«name.escapeJava»(final «routerTypeName» «name.escapeJava») {
				this.«name.escapeJava» = «name.escapeJava»;
			}
		«ENDFOR»
		}
	'''
	

	private def generateInputs(Process it, EntryPointProcessSIB sib) '''
		package «completePackage»;
		
		import java.util.List;
		
		public class «renderInputForEntryPoint(sib)» {
				
		«FOR it : sib.inputPorts»
			private «routerTypeName» «name.escapeJava»;
				
			@com.fasterxml.jackson.annotation.JsonProperty("«name.escapeString»")
			public «routerTypeName» get«name.escapeJava»() {
				return this.«name.escapeJava»;
			}

			@com.fasterxml.jackson.annotation.JsonProperty("«name.escapeString»")
			public void set«name.escapeJava»(final «routerTypeName» «name.escapeJava») {
				this.«name.escapeJava» = «name.escapeJava»;
			}
		«ENDFOR»
		}
	'''
	
	private def generateOutputs(Process process) '''
		// generated by «class.name»#generateOutputs(Process)

		package «completePackage»;

		import java.util.List;
		
		public class «process.renderOutputForInteractable(null)»{
			private String branchName;
			private String branchId;
			private «process.renderOutputForInteractable(null)»Wrapper result;
				
			@com.fasterxml.jackson.annotation.JsonProperty("branchName")
			public java.lang.String getBranchName() {
				return this.branchName;
			}
			
			public void setBranchName(String branchname) {
				this.branchName = branchname;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("branchId")
			public java.lang.String getBranchId() {
				return this.branchId;
			}
			
			public void setBranchId(String branchId) {
				this.branchId = branchId;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("outputs")
			@com.codingrodent.jackson.crypto.Encrypt
			@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = info.scce.dime.rest.ContextIndependentSerializer.class)
			@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = info.scce.dime.rest.ContextIndependentDeserializer.class)
			public «process.renderOutputForInteractable(null)»Wrapper getResult() {
				return this.result;
			}
			
			public void setResult(«process.renderOutputForInteractable(null)»Wrapper result) {
				this.result = result;
			}
			public «process.renderOutputForInteractable(null)»(){}
			public «process.renderOutputForInteractable(null)»(«process.getExternalResultTypeName» result, info.scce.dime.rest.ObjectCache objectCache){
				this.branchName = result.getBranchName();
				this.branchId = result.getBranchId();
				this.result = new «process.renderOutputForInteractable(null)»Wrapper(result, objectCache);
			}
		
		public static class «process.renderOutputForInteractable(null)»Wrapper {
			
			private String branchName;
				
			@com.fasterxml.jackson.annotation.JsonProperty("branchName")
			public java.lang.String getBranchName() {
				return this.branchName;
			}
			
			public void setBranchName(String branchname) {
				this.branchName = branchname;
			}
			public «process.renderOutputForInteractable(null)»Wrapper(){}
			public «process.renderOutputForInteractable(null)»Wrapper(«process.getExternalResultTypeName» result, info.scce.dime.rest.ObjectCache objectCache){
				this.branchName = result.getBranchName();
				«FOR endSIB: process.endSIBs»
				this.«endSIB.branchName.toFirstLower.escapeJava» = result.get«endSIB.typeName»() == null ? null : new «endSIB.typeName»Wrapper(result.get«endSIB.typeName»(), objectCache);
				«ENDFOR»
			}
			«FOR endSIB: process.endSIBs»
				private «endSIB.typeName»Wrapper «endSIB.branchName.toFirstLower.escapeJava»;
				
				@com.fasterxml.jackson.annotation.JsonProperty("«endSIB.branchName.escapeString»")
				public «endSIB.typeName»Wrapper get«endSIB.branchName.escapeJava»() {
					return this.«endSIB.branchName.toFirstLower.escapeJava»;
				}
				
				public void set«endSIB.branchName.escapeJava»(«endSIB.typeName»Wrapper «endSIB.branchName.toFirstLower.escapeJava» ) {
					this.«endSIB.branchName.toFirstLower.escapeJava» = «endSIB.branchName.toFirstLower.escapeJava» ;
				}
				
				
				public static class «endSIB.typeName»Wrapper {

				«FOR it: endSIB.inputPorts»
					public «getRestTypeName» «name.toFirstLower.escapeJava»;
					public «getRestTypeName» get«name.toFirstUpper.escapeJava»() { return «name.toFirstLower.escapeJava»; }
				«ENDFOR»
				«FOR it: endSIB.inputStatics»
					public «typeName» «name.toFirstLower.escapeJava»;
					
					public «typeName» get«name.toFirstUpper.escapeJava»() { return «name.toFirstLower.escapeJava»; }
				«ENDFOR»
				
					public «endSIB.typeName»Wrapper(){}
					public «endSIB.typeName»Wrapper(«process.typeName».«endSIB.typeName» result, info.scce.dime.rest.ObjectCache objectCache){
					«FOR it: endSIB.inputPorts»
						«toJsonTransformation(name.toFirstLower.escapeJava, "result.get" + name.toFirstUpper.escapeJava + "()", if (it instanceof ComplexInputPort) dataType, it instanceof ComplexInputPort,(it instanceof PrimitiveInputPort)&&(it as PrimitiveInputPort).dataType==PrimitiveType.FILE, isList)»
					«ENDFOR»
					«FOR it: endSIB.inputStatics»
						«name.toFirstLower.escapeJava» = result.get«name.toFirstUpper.escapeJava»();
					«ENDFOR»
				}
				
			}
			«ENDFOR»
		}
		}
	'''
	
	private def renderInputForInteractable(Process p, ProcessSIB sib)
		'''«p.renderInputClassPrefix()»«sib?.id?.escapeJava?:""»Input'''
		
	private def renderInputForEntryPoint(Process p, EntryPointProcessSIB sib)
	'''«p.renderInputClassPrefix()»«sib?.id?.escapeJava?:""»Input'''

	private def inputToValue(Node input) {
		switch it : input {
			InputPort: '''«name.escapeJava»'''
			BooleanInputStatic: '''«value»'''
			IntegerInputStatic: '''«value»l'''
			RealInputStatic: '''«value»d'''
			TextInputStatic: '''"«value»"'''
			TimestampInputStatic : '''new java.util.Date(«value»000l)'''
			default: throw new IllegalArgumentException('''No case for «class».''')
		}
	}
}
