/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import java.nio.file.Path

import static extension info.scce.dime.generator.process.BackendProcessGeneratorUtil.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import static extension info.scce.dime.gui.helper.GUIExtension.lastModified2

class InteractionContextGenerator {
	
	extension DyWAExtension = new DyWAExtension
	extension ProcessExtension = new ProcessExtension
	
	static val packageName = "de.ls5.dywa.generated"

	def generate(Process interaction, Path outlet) {
		
		val targetDir = outlet.resolve("app-business/target/generated-sources")
		
		val String content = generate(interaction).toString()
		val String packageName = '''«packageName».rest.types.'''
		val String fileName = '''InteractionWrapper«interaction.modelName.escapeJava».java'''
		
		DyWAAbstractGenerator.generate(content, packageName, fileName, targetDir, interaction.lastModified2)
	}

	private def generate(Process interaction) '''
		package «packageName».rest.types;
		
		public class InteractionWrapper«interaction.modelName.escapeJava» { 
		
			final static com.github.mustachejava.DefaultMustacheFactory mustacheFactory = new com.github.mustachejava.DefaultMustacheFactory();
			final static com.github.mustachejava.Mustache descriptionMustache = mustacheFactory.compile(new java.io.StringReader("«interaction.description.escapeString»"), null);
			final static com.github.mustachejava.Mustache displayNameMustache = mustacheFactory.compile(new java.io.StringReader("«interaction.displayName.escapeString»"), null);
			final static com.github.mustachejava.Mustache imagePathMustache = mustacheFactory.compile(new java.io.StringReader("«interaction.imagePath.escapeString»"), null);
			final static com.github.mustachejava.Mustache overlayImagePathMustache = mustacheFactory.compile(new java.io.StringReader("«interaction.overlayImagePath.escapeString»"), null);

			private final static java.lang.String name = "InteractionWrapper«interaction.modelName.escapeJava»";

			private final long id;
			private final java.lang.String interactId;
			private final java.lang.String guardContainerId;
			private final java.lang.String displayName;
			private final java.lang.String description;
			private final java.lang.String imagePath;
			private final java.lang.String overlayImagePath;
			private final «interaction.interactionInputsTypeNameJava» interactionInputs;

			public InteractionWrapper«interaction.modelName.escapeJava»(«("Interaction" + interaction.modelName).processContextsTypeName» delegate) {
				this.id = delegate.getDywaId();
				this.interactId = delegate.getinteractId();
				this.guardContainerId = delegate.getguardContainerId();

				final java.io.StringWriter displayNameWriter = new java.io.StringWriter();
				displayNameMustache.execute(displayNameWriter, delegate.getinteractionInputs());
				displayName = displayNameWriter.toString();

				final java.io.StringWriter descriptionWriter = new java.io.StringWriter();
				descriptionMustache.execute(descriptionWriter, delegate.getinteractionInputs());
				description = descriptionWriter.toString();

				final java.io.StringWriter imagePathWriter = new java.io.StringWriter();
				imagePathMustache.execute(imagePathWriter, delegate.getinteractionInputs());
				imagePath = imagePathWriter.toString();

				final java.io.StringWriter overlayImagePathWriter = new java.io.StringWriter();
				overlayImagePathMustache.execute(overlayImagePathWriter, delegate.getinteractionInputs());
				overlayImagePath = overlayImagePathWriter.toString();

				this.interactionInputs = new «interaction.interactionInputsTypeNameJava»(delegate.getinteractionInputs());
			}

			@com.fasterxml.jackson.annotation.JsonProperty("name")
			public java.lang.String getName() {
				return this.name;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("id")
			public long getId() {
				return this.id;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("interactId")
			public java.lang.String getInteractId() {
				return this.interactId;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("guardContainerId")
			public java.lang.String getGuardContainerId() {
				return this.guardContainerId;
			}

			@com.fasterxml.jackson.annotation.JsonProperty("displayName")
			public java.lang.String getDisplayName() {
				return this.displayName;
			}

			@com.fasterxml.jackson.annotation.JsonProperty("description")
			public java.lang.String getDescription() {
				return this.description;
			}

			@com.fasterxml.jackson.annotation.JsonProperty("imagePath")
			public java.lang.String getImagePath() {
				return this.imagePath;
			}

			@com.fasterxml.jackson.annotation.JsonProperty("overlayImagePath")
			public java.lang.String getOverlayImagePath() {
				return this.overlayImagePath;
			}

			@com.fasterxml.jackson.annotation.JsonProperty("interactionInputs")
			public «interaction.interactionInputsTypeNameJava» getInteractionInputs() {
				return this.interactionInputs;
			}

			static class «interaction.interactionInputsTypeNameJava» {
				
				«FOR input : interaction.processInputs»
					private «input.renderType» «input.name.escapeJava»;
					
					@com.fasterxml.jackson.annotation.JsonProperty("«input.name.escapeString»")
					public «input.renderType» get«input.name.escapeJava»() {
						return «input.name.escapeJava»;
					}
				«ENDFOR»
				
				public «interaction.interactionInputsTypeNameJava»(«interaction.interactionInputsTypeNameJava.processContextsTypeName» delegate) {
					«FOR input : interaction.processInputs»
							«IF input instanceof PrimitiveOutputPort» 
							if (delegate.get«input.name.escapeJava»() == null) {
								this.«input.name.escapeJava» = null;
							}
							else {
								this.«input.name.escapeJava» =
								«IF input.isIsList»
									«IF input.dataType == PrimitiveType.FILE»
										delegate.get«input.name.escapeJava»().stream().map(«packageName».rest.util.FileReference::new).collect(java.util.stream.Collectors.toList());
									«ELSE»
										new java.util.ArrayList<>(delegate.get«input.name.escapeJava»());
									«ENDIF»
								«ELSE»
									«IF input.dataType == PrimitiveType.FILE»
										new «packageName».rest.util.FileReference(delegate.get«input.name.escapeJava»());
									«ELSE»
										delegate.get«input.name.escapeJava»();
									«ENDIF»
								«ENDIF»
							«ELSE»
								«IF input.isIsList»
								if (delegate.get«input.name.escapeJava»_«(input as ComplexOutputPort).dataType.name.escapeJava.toFirstUpper»() == null) {
									this.«input.name.escapeJava» = null;
								}
								else {
									this.«input.name.escapeJava» =
									delegate.get«input.name.escapeJava»_«(input as ComplexOutputPort).dataType.name.escapeJava.toFirstUpper»().stream().map(info.scce.dime.util.Identifiable::getDywaId).collect(java.util.stream.Collectors.toList());
								«ELSE»
									if (delegate.get«input.name.escapeJava»() == null) {
										this.«input.name.escapeJava» = null;
									}
									else {
										this.«input.name.escapeJava» =
									delegate.get«input.name.escapeJava»().getDywaId();
								«ENDIF»
							«ENDIF»
						}
					«ENDFOR»
				}
			}
		}
	'''
	
	private def static renderType(OutputPort output) '''
		«IF output instanceof PrimitiveOutputPort» 
«««			TODO: use global utility method?
			«IF output.isIsList»
				java.util.List<
			«ENDIF»
			«switch(output.dataType){
				case BOOLEAN: "java.lang.Boolean"
				case INTEGER: "java.lang.Long"
				case REAL: "java.lang.Double"
				case TEXT: "java.lang.String"
				case TIMESTAMP: "java.util.Date"
				case FILE: packageName + ".rest.util.FileReference"
			}»
			«IF output.isIsList»
				>
			«ENDIF»
		«ELSE»
			«IF output.isIsList»
				java.util.List<java.lang.Long>
			«ELSE»
				java.lang.Long
			«ENDIF»
		«ENDIF»
	'''
}
