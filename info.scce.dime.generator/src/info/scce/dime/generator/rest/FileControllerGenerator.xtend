/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import java.nio.file.Path

class FileControllerGenerator {
	
	val packageName = "de.ls5.dywa.generated"
	
	def generate(Path rootDir) {
		val String restServiceContent = renderRESTService(packageName).toString();
		val String utilPackage = packageName + ".rest.file.";
		val targetDir = rootDir.resolve("dywa-app/app-business/target/generated-sources")
		DyWAAbstractGenerator.generate(restServiceContent, utilPackage, "FileReferenceREST.java", targetDir, 1)
	}
		
	private def renderRESTService(String packageName) '''
		package «packageName».rest.util;
		
		@javax.transaction.Transactional
		@javax.ws.rs.Path("/files")
		public class FileReferenceREST {
			
			@javax.inject.Inject
			private info.scce.dime.util.DomainFileController DomainFileController;
				
		
			@javax.ws.rs.POST
			@javax.ws.rs.Path("create")
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA)
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			public javax.ws.rs.core.Response create(final org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput input) throws java.io.IOException {
				

				final java.util.List<org.jboss.resteasy.plugins.providers.multipart.InputPart> inputParts = input.getFormDataMap().get("file");
		
				if (inputParts == null || inputParts.isEmpty()) {
					throw new javax.ws.rs.WebApplicationException("invalid request");
				}
		
				final org.jboss.resteasy.plugins.providers.multipart.InputPart inputPart = inputParts.get(0);
				final javax.ws.rs.core.MultivaluedMap<java.lang.String, java.lang.String> header = inputPart.getHeaders();
		
				java.lang.String fileName = "unknown";
				final java.lang.String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
		
		
				for (java.lang.String filename : contentDisposition) {
					if ((filename.trim().startsWith("filename"))) {
						final java.lang.String[] name = filename.split("=");
						fileName = name[1].trim().replaceAll("\"", "");
						break;
					}
				}
		
				final info.scce.dime.util.FileReference reference =
						this.DomainFileController.storeFile(fileName, inputPart.getBody(java.io.InputStream.class, null));
		
				return javax.ws.rs.core.Response.ok(new «packageName».rest.util.FileReference(reference)).build();
			}
		}
	'''
}
