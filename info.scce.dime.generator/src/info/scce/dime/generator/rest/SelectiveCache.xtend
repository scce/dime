/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.rest

import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Type
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import java.util.Collections
import java.util.HashMap
import java.util.HashSet
import java.util.Map
import java.util.Set
import org.eclipse.xtend.lib.annotations.Data

import static extension info.scce.dime.generator.gui.data.SelectiveExtension.*

class SelectiveCache {

	val Map<BaseComplexTypeView, Signature> signatureMap = new HashMap
	val Map<Signature, BaseComplexTypeView> representativeMap = new HashMap
	val Set<ComplexTypeView> blankCache = new HashSet

	/**
	 * Blank type views may be recursive so handle them separately.
	 */
	def registerBlankTypeView(Type t) {
		blankCache.add(t.buildBlankTypeView)
	}

	def registerTypeView(BaseComplexTypeView tv) {
		val sig = signatureMap.get(tv)

		if (sig === null) {
			val signature = computeSignature(tv);
			signatureMap.put(tv, signature)
			representativeMap.putIfAbsent(signature, tv)
		}
	}

	def <T extends BaseComplexTypeView> T getRepresentative(T tv) {
		if (blankCache.contains(tv)) {
			return tv
		}
		
		val sig = signatureMap.get(tv);

		if (sig === null) {
			throw new IllegalStateException("TypeView queried before inserted")
		}

		return representativeMap.get(sig) as T
	}
	
	def void printStats() {
		println("[####] " + signatureMap.size + " original typeviews vs. " + representativeMap.size + " unique typeviews")
	}

	private dispatch def Signature computeSignature(ComplexTypeView ctv) {

		val primtiveFields = new HashSet
		val complexFields = new HashSet
		val complexFieldsSignatures = new HashSet

		for (it : ctv.displayedFields) {

			switch it {
				PrimitiveFieldView: {
					primtiveFields.add(it.field)
				}
				ComplexFieldView: {
					val view = it.view as ComplexTypeView
					complexFields.add(it.field)
					val sig = signatureMap.getOrDefault(view, computeSignature(view))
					complexFieldsSignatures.add(sig)
				}
				default:
					throw new IllegalArgumentException("Unknown FieldType " + it)
			}
		}

		return new Signature(ctv.type, primtiveFields, complexFields, complexFieldsSignatures)
	}
	
	private dispatch def Signature computeSignature(BaseComplexTypeView bctv) {
		// base types have no displayed fields
		return new Signature(bctv.type, Collections.emptySet, Collections.emptySet, Collections.emptySet)
	}

	@Data
	private static class Signature {
		val Type type
		val Set<Attribute> primtiveFields
		val Set<Attribute> complexFields
		val Set<Signature> complexFieldsSignatures
	}

}
