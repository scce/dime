/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.livevariable

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator2
import info.scce.dime.generator.util.DimeIOExtension
import info.scce.dime.dad.dad.DAD
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.gui.gui.GUI
import java.io.IOException
import java.nio.file.Path
import java.util.Map
import java.util.Set
import org.eclipse.core.runtime.IProgressMonitor

class LiveVariableGenerator implements IGenerator2<DAD> {
	
	extension DimeIOExtension = new DimeIOExtension
	
	val public static PKG = "info.scce.dime"
	
	var static Map<String, Integer> portMap = newHashMap
	var static Integer nextPort = 50000
	
	var Set<GUICompoundView> guiCompundViews

	new (Set<GUICompoundView> guiCompundViews) {
		this.guiCompundViews = guiCompundViews
	}

	override generate(DAD dad, Path path, IProgressMonitor progressMonitor) {
		
		println("[INFO] Generating live variable components.")
		
		var variableManager = new LiveVariableManager(guiCompundViews)
		for (container: variableManager.liveVariableContainers) {
			println('''[INFO] «container»''')
		}
		
		// Disable generation since LiveVariable feature is not working with native schema 				
//		val eventHandlerTemplate = new EventHandlerTemplate(guiCompundViews)
//		val eventHandlerRegistryBeanTemplate = new EventHandlerRegistryBeanTemplate
//		
//		// Generate EventHandler
//		writeToFile(
//			'''«EventHandlerTemplate.className».java''',
//			path, 
//			eventHandlerTemplate.generate(pkg, variableManager)
//		)
//		
//		// Generate RegistryBean
//		writeToFile(
//			'''«EventHandlerRegistryBeanTemplate.className».java''',
//			path,
//			eventHandlerRegistryBeanTemplate.generate(pkg)
//		)
		
		// Generate WebSocket Class for each GUI.
		for (gui: variableManager.GUIs) {
			writeToFile(
				'''«gui.websocketType».java''',
				path,
				LiveVariableWebsocketTemplate.generate(PKG, gui.websocketType, gui, variableManager)
			)
		}
		
		println("[INFO] Live variable components generated.")
		
	}

	def private void writeToFile(String fileName, Path rootPath, CharSequence contents) {
		if (contents !== null) {
			try {
				rootPath
					.resolve("app-business", "target", "generated-sources")
					.resolve(PKG.split("\\."))
					.createDirectories()
					.resolve(fileName)
					.writeString(contents)
			}
			catch (IOException e) {
				throw new RuntimeException(e)
			}
		}
	}

	def static Integer getPort(GUI gui) {
		if (!portMap.containsKey(gui.title)) {
			portMap.put(gui.title, nextPort++)
		}
		return portMap.get(gui.title)
	}

	def static CharSequence getWebsocketType(GUI gui) {
		'''«gui.title»WebSocket'''
	}
	
}
