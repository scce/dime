/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.livevariable

class EventHandlerRegistryBeanTemplate {
	
	public static def getClassName() '''LiveVariableRegistryBean'''
	
	public def generate(String pkg) '''
		package «pkg»;
		
		import de.ls5.dywa.adapter.events.EventHandler;
		import de.ls5.dywa.api.event.EventDispatcher;
		
		import javax.annotation.PostConstruct;
		import javax.annotation.PreDestroy;
		import javax.ejb.Singleton;
		import javax.ejb.Startup;
		import javax.inject.Inject;
		import java.io.IOException;
		import java.io.InputStream;
		import java.util.Properties;
		
		@Singleton
		@Startup
		public class «className» {
		
			private final static String ARTIFACT_NAME;
		
			static {
				final Properties config = new Properties();
				final InputStream inputStream = «className».class.getResourceAsStream("/META-INF/config.properties");
		
				try {
					config.load(inputStream);
				}
				catch (IOException ioe) {
					throw new RuntimeException(ioe);
				}
		
				ARTIFACT_NAME = config.getProperty("finalName");
			}
		
			private static final String EVENTLISTENER_JNDI = "java:global/" +
					ARTIFACT_NAME +
					'/' +
					«EventHandlerTemplate.className».class.getSimpleName() +
					'!' +
					EventHandler.class.getName();
		
			@Inject
			private EventDispatcher eventDispatcher;
		
			@PostConstruct
			public void registerListener() {
				try {
					eventDispatcher.registerEventListener(EVENTLISTENER_JNDI);
				} catch (Exception e) {
					this.unregisterListener();
					throw new RuntimeException(e);
				}
			}
		
			@PreDestroy
			public void unregisterListener() {
				try {
					eventDispatcher.unregisterEventListener(EVENTLISTENER_JNDI);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	'''
}
