/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.livevariable

import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

/** 
 * Container for live variables, including the variable itself, the live
 * candidate and the path from the root variable, ending at the candidate.
 */
@Accessors(PUBLIC_GETTER)
class LiveVariableContainer {
	
	/**
	 * Variable, which is marked as live.
	 */
	var ComplexVariable liveVariable
	
	/**
	 * Parent of the highest list in the tree upwards the live variable.
	 * If there is no list upwards, the candidate is the live variable himself.
	 */
	var ComplexVariable candidateVariable
	
	/**
	 * Path from the candidate variable to the live variable.
	 * Empty, if the live variable is the candidate.
	 */
	var List<ComplexVariable> pathFromCandidateToLive

	new (ComplexVariable liveVariable, ComplexVariable candidateVariable, List<ComplexVariable> pathFromCandidateToLive) {
		this.liveVariable = liveVariable
		this.candidateVariable = candidateVariable
		this.pathFromCandidateToLive = pathFromCandidateToLive
	}

	/** 
	 * Checks the live variable is the candidate.
	 * @return	True, if the live variable is the candidate.
	 */
	def boolean isCandidate() {
		pathFromCandidateToLive.nullOrEmpty
	}

	/** 
	 * @return	True, if this container has a candidate.
	 */
	def boolean hasCandidate() {
		candidateVariable !== null
	}

	/** 
	 * @return	GUI of the live variable.
	 */
	def GUI getGUI() {
		liveVariable.rootElement
	}

	override equals(Object other) {
		switch (other) {
			case null:             false
			LiveVariableContainer: other.liveVariable.equals(this)
			default:               false
		}
	}

	override toString() '''
		«class.name» {
			GUI               = «GUI.title»
			Live              = «liveVariable.name»
			Candidate         = «candidateVariable.name»
			Candidate -> Live = [«pathFromCandidateToLive.map[name].join(" -> ")»]
		}
	'''
	
}
