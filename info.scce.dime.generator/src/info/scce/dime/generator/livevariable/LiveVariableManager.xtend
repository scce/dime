/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.livevariable

import info.scce.dime.data.data.Type
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.PrimitiveAttribute
import java.util.LinkedList
import java.util.List
import java.util.Set
import org.eclipse.xtend.lib.annotations.Accessors

/** 
 * Manages live variables:
 * <ul>
 *     <li>Extracts them from GUI Compound Views.</li>
 *     <li>Provides methods to collect all candidate variables, types, live
 *         variables and root variables, GUIs with live variables.</li>
 * </ul>
 */
@Accessors(PUBLIC_GETTER)
class LiveVariableManager {
	
	/**
	 * All Containers for live variables (including the live variable,
	 * her candidate and the path from the candidate to the live variable).
	 */
	var Set<LiveVariableContainer> liveVariableContainers

	new (GUICompoundView guiCompoundView) {
		liveVariableContainers = createLiveContainers(guiCompoundView)
	}

	new (Set<GUICompoundView> guiCompoundViews) {
		liveVariableContainers = createLiveContainers(guiCompoundViews)
	}

	/** 
	 * @return	All GUIs, which have live variables.
	 */
	def Set<GUI> getGUIs() {
		val guis = newHashMap
		for (n: liveVariableContainers.map[liveVariable.rootElement]) {
			guis.putIfAbsent(n.id, n)
		}
		return guis.values.toSet
	}

	/** 
	 * @return	Types of all candidate variables.
	 */
	def Set<Type> getCandidateVariableTypes() {
		val types = newHashMap
		for (n: liveVariableContainers.map[candidateVariable.dataType]) {
			types.putIfAbsent(n.id, n)
		}
		return types.values.toSet
	}

	/** 
	 * @return	Types of all live variables.
	 */
	def Set<Type> getLiveVariableTypes() {
		val types = newHashMap
		for (n: liveVariableContainers.map[liveVariable.dataType]) {
			types.putIfAbsent(n.id, n)
		}
		return types.values.toSet
	}

	/** 
	 * @return	Types of live and candidate variables.
	 */
	def Set<Type> getAllTypes() {
		(liveVariableTypes + candidateVariableTypes).toSet
	}

	/** 
	 * @return	True, if there is at least one live variable.
	 */
	def boolean hasLiveVariables() {
		!liveVariableContainers.empty
	}

	/** 
	 * Collects all containers which includes a live variable
	 * with the given type.
	 * @param type
	 * @return
	 */
	def Set<LiveVariableContainer> getLiveVariableContainersByLiveVariableType(Type type) {
		liveVariableContainers.filter[liveVariable.dataType == type].toSet
	}

	/** 
	 * @return	All candidate variables.
	 */
	def Set<ComplexVariable> getCandidateVariables() {
		val variables = newHashMap
		for (n: liveVariableContainers.map[candidateVariable]) {
			variables.putIfAbsent(n.id, n)
		}
		return variables.values.toSet
	}

	/** 
	 * @param	GUI.
	 * @return	All candidate variables for the given GUI.
	 */
	def Set<ComplexVariable> getCandidateVariables(GUI gui) {
		val variables = newHashMap
		for (n: liveVariableContainers.filter[liveVariable.rootElement.title == gui.title].map[candidateVariable]) {
			variables.putIfAbsent(n.id, n)
		}
		return variables.values.toSet
	}

	/** 
	 * Creates a path, starting at the root variable of the given candidate
	 * and ends at the parent of the given candidate variable.
	 * @param candidateVariable
	 * @return
	 */
	def LinkedList<ComplexVariable> getPathFromRootToCandidate(ComplexVariable candidateVariable) {
		val path = newLinkedList
		var current = candidateVariable
		while (current !== null) {
			if (!current.isListAttribute) {
				path.offerFirst(current)
			}
			current = current.parent
		}
		return path
	}

	/** 
	 * Checks the variable as a root variable.
	 * @param candidateVariable
	 * @return	True, of the variable is a root variable.
	 */
	def boolean isRoot(ComplexVariable candidateVariable) {
		!candidateVariable.hasParent
	}

	/** 
	 * Finds the root of a given variable.
	 * @param candidateVariable
	 * @return
	 */
	def ComplexVariable getRoot(ComplexVariable candidateVariable) {
		var current = candidateVariable
		while (current.hasParent) {
			current = current.parent
		}
		return current
	}

	/** 
	 * Creates a set of successors of a variable, including the successors 
	 * of the successors, and so on.
	 * @param variable
	 * @return			Set of successors (and their successors, and...) of the given variable.
	 */
	def Set<ComplexVariable> getComplexSuccessors(ComplexVariable variable) {
		val successors = variable.complexVariableSuccessors.toSet
		var added = 0
		do {
			val nextSuccessors = successors
				.flatMap[complexVariableSuccessors]
				.filter[ s | !successors.contains(s) ]
				.toSet
			added = nextSuccessors.size
			successors += nextSuccessors
		}
		while (added > 0)
		return successors.filter[!isListAttribute].toSet
	}

	/** 
	 * Creates the live containers from the given GUI compound views by the following steps:
	 * <ol>
	 *     <li>Find all complex variables which are marked as "live", excluding list attributes.</li>
	 *     <li>Find all complex lists, which have at least one attribute that is marked as "live".</li>
	 *     <li>Find all complex attributes, which are marked as "live"</li>
	 *     <li>Find all complex variables, which have at least one primitive attribute that is marked as "live".</li>
	 *     <li>Find all successors (and their successors, and...) of all previously found complex variables.</li>
	 * </ol>
	 * For each complex variable, that accomplish these steps:
	 * <ol>
	 *     <li>Find the candidate variable (parent of the highest list upwards the live variable).</li>
	 *     <li>Find the path from the candidate to the live variable.</li>
	 * </ol>
	 * @param guiCompoundViews
	 * @return
	 */
	def private Set<LiveVariableContainer> createLiveContainers(Set<GUICompoundView> gcvs) {
		val guiCompoundViewsMap = newHashMap
		for (n: gcvs) {
			guiCompoundViewsMap.putIfAbsent(n.gui.id, n)
		}
		val guiCompoundViews = guiCompoundViewsMap.values.toSet
		
		val liveVariables = (
			guiCompoundViews.findComplexLiveVariables +
			guiCompoundViews.findLiveListAttributes +
			guiCompoundViews.findComplexVariableFromComplexAttribute +
			guiCompoundViews.findComplexVariablesWithLivePrimitiveAttribute
		)
		.toSet
			
		// Add all successors of all live variables.
		liveVariables += liveVariables.flatMap[complexSuccessors].toSet
		
		return liveVariables
			.filterNull
			.map[ v | new LiveVariableContainer(v, v.findCandidate, v.createPathFromCandidateToLive) ]
			.filter[hasCandidate]
			.toSet
	}

	/** 
	 * Finds all complex variables with isLive = true in
	 * the given GUI compound views.
	 * @param guiCompoundViews
	 * @return
	 */
	def private Set<ComplexVariable> findComplexLiveVariables(Set<GUICompoundView> guiCompoundViews) {
		guiCompoundViews
			.flatMap[pairs.elements]
			.filter(ComplexVariable)
			.filter[isLive && !isListAttribute]
			.toSet
	}

	/** 
	 * Finds all live list attributes (first, current, last)
	 * and maps them to their list.
	 * @param guiCompoundViews
	 * @return
	 */
	def private Set<ComplexVariable> findLiveListAttributes(Set<GUICompoundView> guiCompoundViews) {
		guiCompoundViews
			.flatMap[pairs.elements]
			.filter(ComplexVariable)
			.filter[isLive && isListAttribute]
			.map[parent]
			.toSet
	}

	/** 
	 * Finds all complex variables having at least 
	 * one primitive attribute
	 * which is marked as live.
	 * @param guiCompoundViews
	 * @return
	 */
	def private Set<ComplexVariable> findComplexVariablesWithLivePrimitiveAttribute(Set<GUICompoundView> guiCompoundViews) {
		guiCompoundViews
			.flatMap[pairs.elements]
			.filter(ComplexVariable)
			.filter[attributes !== null]
			.filter[
				attributes.exists[ a | a instanceof PrimitiveAttribute && a.isLive ]
			]
			.toSet
	}

	/** 
	 * Finds all complex attributes, that are live
	 * and transforms them to their complex variable.
	 * @param guiCompoundViews
	 * @return
	 */
	def private Set<ComplexVariable> findComplexVariableFromComplexAttribute(Set<GUICompoundView> guiCompoundViews) {
		guiCompoundViews
			.flatMap[pairs.elements]
			.filter(ComplexVariable)
			.flatMap[attributes]
			.filter(ComplexAttribute)
			.filter[isLive]
			.map[container]
			.filter(ComplexVariable)
			.map[ v | if (v.isListAttribute) v.parent else v ]
			.toSet
	}

	/** 
	 * Find the live candidate of a given complex variable.
	 * The live candidate is the highest complex variable
	 * in the variable tree, upwards the given variable,
	 * that is not a list and has no list upwards.
	 * @param variable
	 * @return
	 */
	def private ComplexVariable findCandidate(ComplexVariable variable) {
		var ComplexVariable highestList = null
		var current = variable
		while (current !== null) {
			if (current.isList) {
				highestList = current
			}
			current = current.parent
		}
		// No list upwards this variable -> Can use it directly.
		if (highestList === null) {
			return variable
		}
		else if (!highestList.hasParent) {
			println('''[WARN] «variable.name» is a list without parent -> no live updates possible''')
			return null
		}
		else {
			return highestList.parent
		}
	}

	/** 
	 * Creates the path, starting at the candidate variable
	 * and ending at the live variable.
	 * @param live variable
	 * @return
	 */
	def private List<ComplexVariable> createPathFromCandidateToLive(ComplexVariable liveVariable) {
		val path = newLinkedList
		if (liveVariable === null) {
			return path
		}
		val candidate = liveVariable.findCandidate
		var current = liveVariable
		while (current !== null && current != candidate) {
			if (!current.isListAttribute) {
				path.offerFirst(current)
			}
			current = current.parent
		}
		return path
	}

	/** 
	 * @param variable
	 * @return	Parent complex variable of the given.
	 */
	def private ComplexVariable getParent(ComplexVariable variable) {
		if (variable.hasParent) {
			return variable.complexVariablePredecessors.head
		}
		else {
			return null
		}
	}

	/** 
	 * Checks the parent of the given variable.
	 * @param variable
	 * @return	True, if the variable has a parent.
	 */
	def private boolean hasParent(ComplexVariable variable) {
		val cvp = variable?.complexVariablePredecessors
		return cvp !== null && cvp.size == 1
	}

	/** 
	 * Checks the variable is list attribute.
	 * @param variable
	 * @return	True, if the variable is a list attribute.
	 */
	def private boolean isListAttribute(ComplexVariable variable) {
		variable.hasParent && variable.parent.isList
	}

	def private Set<LiveVariableContainer> createLiveContainers(GUICompoundView guiCompoundView) {
		createLiveContainers(#{guiCompoundView})
	}
	
}
