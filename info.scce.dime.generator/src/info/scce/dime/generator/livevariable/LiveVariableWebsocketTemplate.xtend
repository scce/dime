/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.generator.livevariable

import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI

/**
 * Template generating live variable WebSocket-Classes at
 * the server part.
 */
class LiveVariableWebsocketTemplate {
	
	val public static DELETED_CLOSE_CODE = 4000
	
	def static generate(CharSequence pkg, CharSequence className, GUI gui, LiveVariableManager liveVariableManager) '''
		package «pkg»;
		
		import java.io.IOException;
		import java.net.InetSocketAddress;;
		import java.util.HashMap;
		import java.util.HashSet;
		import java.util.Map;
		import java.util.Set;
		import java.util.stream.Collectors;
		import java.util.regex.Matcher;
		import java.util.regex.Pattern;
		
		import org.java_websocket.WebSocket;
		import org.java_websocket.framing.CloseFrame;
		import org.java_websocket.handshake.ClientHandshake;
		import org.java_websocket.server.WebSocketServer;
		
		import javax.annotation.PostConstruct;
		import javax.annotation.PreDestroy;
		import javax.ejb.Startup;
		import javax.ejb.Singleton;
		
		import org.slf4j.Logger;
		import org.slf4j.LoggerFactory;
		
		@Startup
		@Singleton
		public class «className» extends WebSocketServer {
			
			/**
			 * Logger for this class
			 */
			private Logger LOGGER = LoggerFactory.getLogger(«className».class);
			
			private static final Pattern VARIABLE_PATTERN = Pattern.compile("([_a-zA-Z0-9]+)/(\\d+)");
			
			/**
			 * Maps from a variable name to the map (needed for onOpen).
			 */
			private final Map<String, Map<Long, Set<WebSocket>>> variableMap = new HashMap<>();
			
			«FOR candidate: liveVariableManager.getCandidateVariables(gui)»
			/**
			 * Set of Clients and their «candidate.dataType.name» id for «candidate.name»
			 */
			private final Map<Long, Set<WebSocket>> «candidate.clientsMapName» = new HashMap<>();
			
			«ENDFOR»
			
			public «className»() {
				super(new InetSocketAddress("localhost", «LiveVariableGenerator.getPort(gui)»));
			}
			
			@PostConstruct
			public void init() {
				«FOR candidate: liveVariableManager.getCandidateVariables(gui)»
					variableMap.put("«candidate.id»", this.«candidate.clientsMapName»);
				«ENDFOR»
				this.start();
			}
			
			@PreDestroy
			public void shutdown() {
				«FOR candidate: liveVariableManager.getCandidateVariables(gui)»
					variableMap.remove("«candidate.id»");
				«ENDFOR»
				try {
					this.stop();
				}
				catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			/**
			 * Called every time, when a client connected to this endpoint.
			 * Adds the new connection to analogue set of connections.
			 */
			@Override
			public void onOpen(WebSocket client, ClientHandshake handshake) {
				
				Matcher matcher = VARIABLE_PATTERN.matcher(handshake.getResourceDescriptor());
				
				// Return, if the variable id could not extract from descriptor 
				// or the extracted variable id is unknown.
				if(!matcher.find()) {
					client.close(CloseFrame.NORMAL);
					return;
				}
				
				String variableId = matcher.group(1);
				Long dywaId = Long.parseLong(matcher.group(2));
				
				if(!variableMap.containsKey(variableId)) {
					client.close(CloseFrame.NORMAL);
					return;
				}
				
				Map<Long, Set<WebSocket>> dywaIdMap = this.variableMap.get(variableId);
				if(!dywaIdMap.containsKey(dywaId)) {
					dywaIdMap.put(dywaId, new HashSet<WebSocket>());
				}
				//remove closed websockets
				new java.util.LinkedList<>(dywaIdMap.get(dywaId))
								.stream()
								.filter(n->n.getReadyState()!= WebSocket.READYSTATE.OPEN)
								.forEach(n->dywaIdMap.values().forEach(e->e.remove(n)));
				
				dywaIdMap.get(dywaId).add(client);
				LOGGER.info("«className»: Client connected for variable >{}< and dywaId >{}<.", variableId, dywaId);
			}
			
			/**
			 * Called every time, when a client disconnected.
			 * Removes the connection from all sets.
			 */
			@Override
			public void onClose(WebSocket client, int code, String reason, boolean isRemote) 
			{
				LOGGER.info("«className»: Client disconnected.");
				removeWebsocket(client);
			}
			
			private void removeWebsocket(WebSocket client) {
				«FOR candidate : liveVariableManager.getCandidateVariables(gui)»
					this.«candidate.clientsMapName».values().forEach(n->n.remove(client));
				«ENDFOR»
			}
			
			@Override
			public void onError(WebSocket connection, Exception exception) {
				
			}
		
			@Override
			public void onMessage(WebSocket connection, String message) {
				
			}
			
			«FOR candidate : liveVariableManager.getCandidateVariables(gui)»
				/**
				 * Checks, if there is a WebSocket connection for an instance of
				 * «candidate.name» with the given id. 
				 * 
				 * @param dywaId	ID of «candidate.name».
				 * @return			True, if there is a client listening for the given id.
				 */
				public boolean «candidate.functionNameHasProspectiveCustomer»(long dywaId) {
					return this.«candidate.clientsMapName».containsKey(dywaId) &&
					       this.«candidate.clientsMapName».get(dywaId).size() > 0;
				}
				
				/**
				 * Sends the given serialized instance of «candidate.name» to all
				 * listening WebSocket connections for «candidate.name» with the
				 * given id. 
				 * 
				 * @param dywaId	ID of «candidate.name».
				 * @param object	Serialized «candidate.name».
				 */
				public void «candidate.functionNameSendToPerspectiveCustomer»(long dywaId, String object) {
					if(object != null && !object.isEmpty() && this.«candidate.functionNameHasProspectiveCustomer»(dywaId)) {
						new java.util.LinkedList<>(this.«candidate.clientsMapName»
							.get(dywaId))
							.forEach((w) -> {
								try {
									if(w.getReadyState()!=WebSocket.READYSTATE.OPEN) {
										removeWebsocket(w);
									} else {
										w.send(object);
									}
								} catch(Exception e) {
									onClose(w, 0, e.getMessage(), true); 
								}
							});
					}
				}
				
				/**
				 * Closes connections of all WebSockets, that are
				 * listening on «candidate.name» with the given id,
				 * because of the deletion.
				 * 
				 * @param dywaId	ID of «candidate.name».
				 */
				public void «candidate.functionNameCloseAfterDeletion»(long dywaId) {
					if(this.«candidate.functionNameHasProspectiveCustomer»(dywaId)) {
						this.«candidate.clientsMapName»
							.get(dywaId)
							.forEach((w) -> {
								try {
									LOGGER.info("Closing WebSocket with code «DELETED_CLOSE_CODE» after deleting «candidate.name»{}.", dywaId);
									w.close(«DELETED_CLOSE_CODE»);
								} catch(Exception e) {
									onClose(w, 0, e.getMessage(), true); 
								}
							});
						this.«candidate.clientsMapName».remove(dywaId);
					}
				}
				
				/**
				 * @return	List of ids, which have listeners for «candidate.name».
				 */
				public Set<Long> «candidate.functionNameGetDywaIds»() {
					return this.«candidate.clientsMapName».keySet();
				}
				
			«ENDFOR »
		}
	'''
	
	def private static clientsMapName(ComplexVariable variable) {
		'''clients«variable.name.toFirstUpper»'''
	}
	
	def static functionNameHasProspectiveCustomer(ComplexVariable variable) {
		'''hasProspectiveCustomer«variable.name.toFirstUpper»'''
	}
	
	def static functionNameSendToPerspectiveCustomer(ComplexVariable variable) {
		'''sendToPerspectiveCustomer«variable.name.toFirstUpper»'''
	}
	
	def static functionNameCloseAfterDeletion(ComplexVariable variable) {
		'''closeAfterDeletion«variable.name.toFirstUpper»'''
	}
	
	def static functionNameGetDywaIds(ComplexVariable variable) {
		'''getDywaIds_«variable.name»'''
	}
	
}
