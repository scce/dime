package info.scce.dime.util;

import javax.persistence.EntityManager;

@javax.enterprise.context.ApplicationScoped
public class EntityManagerProvider {
    @javax.persistence.PersistenceContext
    private EntityManager entityManager;

    @javax.enterprise.inject.Produces
    public EntityManager getEntityManager() {
        return this.entityManager;
    }
}
