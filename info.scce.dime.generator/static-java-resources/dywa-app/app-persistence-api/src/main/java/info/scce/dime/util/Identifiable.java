package info.scce.dime.util;

public interface Identifiable {

    long getDywaId();

    long getDywaVersion();

    void setDywaVersion(final long version);

    String getDywaName();

    void setDywaName(final String name);
}
