package info.scce.dime.util;

import java.io.InputStream;

public interface DomainFileController {

    FileReference getFileReference(final long id);

    InputStream loadFile(final FileReference identifier);

    FileReference storeFile(final String fileName, final java.io.InputStream data);

    void deleteFile(final FileReference identifier);
}
