package info.scce.dime.util;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DomainFileTest {

    @Test
    public void testToStringWithoutValues() {
        //given
        DomainFile domainFile = new DomainFile();
        //then
        assertEquals(
                "DomainFile{id_=0, fileName='null', contentType='null'}",
                //when
                domainFile.toString()
        );
    }

    @Test
    public void testToStringWithValues() {
        //given
        DomainFile domainFile = new DomainFile("fileName", "contentType");
        domainFile.setId(1L);
        //then
        assertEquals(
                "DomainFile{id_=1, fileName='fileName', contentType='contentType'}",
                //when
                domainFile.toString()
        );
    }

    @Test
    public void testGetterAndSetter() {
        //given
        DomainFile domainFile = new DomainFile();
        assertEquals(0, domainFile.getDywaId());
        assertEquals(0L, (long) domainFile.getId());
        assertNull(domainFile.getContentType());
        assertNull(domainFile.getCreatedAt());
        assertNull(domainFile.getFileName());
        String fileName = "fileName";
        String contentType = "contentType";
        Date date = new Date();
        //when
        long id = 1L;
        domainFile.setId(id);
        domainFile.setFileName(fileName);
        domainFile.setContentType(contentType);
        domainFile.setCreatedAt(date);
        //then
        // todo Why does getDywaId() and getId() return the same value?
        assertEquals(domainFile.getDywaId(), (long) domainFile.getId());
        assertEquals(1L, domainFile.getDywaId());
        assertEquals(id, (long) domainFile.getId());
        assertEquals(contentType, domainFile.getContentType());
        assertEquals(date, domainFile.getCreatedAt());
        assertEquals(fileName, domainFile.getFileName());
    }
}