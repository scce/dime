package info.scce.dime.util;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FileReferenceTest {

    @Test
    public void passNull() {
        try {
            new FileReference(null);
            fail("Expected IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals("Not allowed to wrap null, use null directly", e.getMessage());
        }
    }

    @Test
    public void passDomainFile() {
        Date expectedCreatedAt = new Date();
        String expectedContentType = "contentType";
        String expectedFileName = "fileName";
        long expectedDywaId = 1L;
        DomainFile expectedDomainFile = new DomainFile(expectedFileName, expectedContentType);
        expectedDomainFile.setCreatedAt(expectedCreatedAt);
        expectedDomainFile.setId(expectedDywaId);
        FileReference fileReference = new FileReference(expectedDomainFile);
        //when
        Date actualCreatedAt = fileReference.getCreatedAt();
        DomainFile actualDomainFile = fileReference.getDelegate();
        String actualContentType = fileReference.getContentType();
        String actualFileName = fileReference.getFileName();
        long actualDywaId = fileReference.getDywaId();
        //then
        assertEquals(expectedContentType, actualContentType);
        assertEquals(expectedCreatedAt, actualCreatedAt);
        assertEquals(expectedDomainFile, actualDomainFile);
        assertEquals(expectedDywaId, actualDywaId);
        assertEquals(expectedFileName, actualFileName);


    }
}