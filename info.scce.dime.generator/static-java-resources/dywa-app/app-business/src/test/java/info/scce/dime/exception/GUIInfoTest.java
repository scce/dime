package info.scce.dime.exception;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class GUIInfoTest {

    @Test
    public void testConstructor() {
        //given
        String expectedId = "1";
        String expectedInputs = "inputs";
        GUIEncounteredSignal.GUIEvent expectedGUIEvent = new GUIEncounteredSignal.GUIEvent("Name", expectedInputs);
        GUIEncounteredSignal.GUIInfo guiInfo = new GUIEncounteredSignal.GUIInfo(expectedId, expectedInputs, expectedGUIEvent);
        //when
        String actualId = guiInfo.getId();
        Object actualInputs = guiInfo.getInputs();
        GUIEncounteredSignal.GUIEvent actualGUIEvent = guiInfo.getEvent();
        //then
        assertEquals(expectedId, actualId);
        assertEquals(expectedInputs, actualInputs);
        assertEquals(expectedGUIEvent, actualGUIEvent);
    }

    @Test
    public void testUpdateInputs() {
        //given
        String expectedId = "1";
        String expectedInputs = "inputs";
        GUIEncounteredSignal.GUIEvent expectedGUIEvent = new GUIEncounteredSignal.GUIEvent("Name", expectedInputs);
        GUIEncounteredSignal.GUIInfo guiInfo = new GUIEncounteredSignal.GUIInfo(expectedId, null, expectedGUIEvent);
        assertNull(guiInfo.getInputs());
        //when
        guiInfo.updateInputs(expectedInputs);
        //then
        assertEquals(expectedInputs, guiInfo.getInputs());
    }
}