package info.scce.dime.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class JacksonProviderTest {

    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        String property = "info.scce.dime.app.encrypt.disable";
        System.setProperty(property, "true");
        objectMapper = JacksonProvider.getPreconfiguredMapper();
        System.clearProperty(property);
    }

    @Test
    public void testAlphabeticallyOrderUnchanged() throws JsonProcessingException {
        //given
        Map<String, String> map = new HashMap<>();
        map.put("a", "a");
        map.put("b", "b");
        //then
        assertEquals(
                "{\"a\":\"a\",\"b\":\"b\"}",
                //when
                objectMapper.writeValueAsString(map)
        );
    }

    @Test
    public void testAlphabeticallyOrderChanged() throws JsonProcessingException {
        //given
        Map<String, String> map = new HashMap<>();
        map.put("b", "b");
        map.put("a", "a");
        //then
        assertEquals(
                "{\"a\":\"a\",\"b\":\"b\"}",
                //when
                objectMapper.writeValueAsString(map)
        );
    }

    @Test
    public void testDoNotFailOnEmptyBeans() throws JsonProcessingException {
        //then
        assertEquals(
                "{}",
                //when
                objectMapper.writeValueAsString(
                        //given
                        new Object()
                )
        );
    }

    @Test
    public void testDoNotWriteDatesAsTimestamps() throws JsonProcessingException {
        //given
        Map<String, Date> map = new HashMap<>();
        map.put("date", new Date(1));
        //then
        assertEquals(
                "{\"date\":\"1970-01-01T00:00:00.001+00:00\"}",
                //when
                objectMapper.writeValueAsString(map)
        );
    }
}