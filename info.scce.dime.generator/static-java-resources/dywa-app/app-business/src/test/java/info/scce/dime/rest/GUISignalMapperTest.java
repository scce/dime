package info.scce.dime.rest;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.process.ProcessCallFrame;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class GUISignalMapperTest {

    @Test
    public void toResponse() {
        //given
        GUISignalMapper guiSignalMapper = new GUISignalMapper();
        GUIEncounteredSignal guiEncounteredSignal = new GUIEncounteredSignal(new ProcessCallFrame(), "1");
        //when
        Response response = guiSignalMapper.toResponse(guiEncounteredSignal);
        //then
        assertEquals(200, response.getStatus());
        assertEquals(guiEncounteredSignal.getGuiResult(), response.getEntity());
    }

}