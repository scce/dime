package info.scce.dime.auth;

import info.scce.dime.config.fake.FakeSystemProperty;
import junit.framework.TestCase;

public class JWTCookieTest extends TestCase {

    public void testLoggedIn() {
        //then
        assertEquals(
                //given
                JWTCookie
                        .loggedIn(
                                "token",
                                new FakeSystemProperty("production")
                        )
                        //when
                        .toString(),
                "jwt=token;Version=1;Comment=\"Cookie which holds a valid JWT; the user is logged in.\";Path=/;Max-Age=28800;Secure;HttpOnly"
        );
    }

    public void testLoggedInLocalDevelopment() {
        //then
        assertEquals(
                //given
                JWTCookie
                        .loggedIn(
                                "token",
                                new FakeSystemProperty("local")
                        )
                        //when
                        .toString(),
                "jwt=token;Version=1;Comment=\"Cookie which holds a valid JWT; the user is logged in.\";Path=/;Max-Age=28800;Secure;HttpOnly;SameSite=None"
        );
    }

    public void testLoggedOut() {
        //then
        assertEquals(
                //given
                JWTCookie
                        .loggedOut(
                                new FakeSystemProperty("production")
                        )
                        //when
                        .toString()
                ,
                "jwt=;Version=1;Comment=\"Cookie which holds an empty string; the user is logged out.\";Path=/;Max-Age=0;Secure;HttpOnly"
        );
    }

    public void testLoggedOutLocalDevelopment() {
        //then
        assertEquals(
                //given
                JWTCookie
                        .loggedOut(
                                new FakeSystemProperty("local")
                        )
                        //when
                        .toString()
                ,
                "jwt=;Version=1;Comment=\"Cookie which holds an empty string; the user is logged out.\";Path=/;Max-Age=0;Secure;HttpOnly;SameSite=None"
        );
    }
}