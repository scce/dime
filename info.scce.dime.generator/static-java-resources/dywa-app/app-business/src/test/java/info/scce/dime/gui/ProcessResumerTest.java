package info.scce.dime.gui;

import info.scce.dime.process.ProcessCallFrame;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProcessResumerTest {

    @Test
    public void resumeFromGUIReturnSlgResult() {
        //given
        ProcessResumer processResumer = new ProcessResumer();
        String slgResult = "slgResult";
        //then
        assertEquals(
                slgResult,
                //when
                processResumer.resumeFromGUI(new ProcessCallFrame(), slgResult)
        );
    }

}