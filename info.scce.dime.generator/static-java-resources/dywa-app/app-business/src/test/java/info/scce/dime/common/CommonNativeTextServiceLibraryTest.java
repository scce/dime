package info.scce.dime.common;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CommonNativeTextServiceLibraryTest {


    @Test
    public void isNullFalse() {
        //false
        assertFalse(
                //when
                CommonNativeTextServiceLibrary.isNull(
                        //given
                        ""
                )
        );
    }

    @Test
    public void isNull() {
        //then
        assertTrue(
                //when
                CommonNativeTextServiceLibrary.isNull(
                        //given
                        null
                )
        );
    }

    @Test
    public void length() {
        //then
        assertEquals(
                1,
                //when
                CommonNativeTextServiceLibrary.length(
                        //given
                        "a"
                )
        );
    }

    @Test
    public void toLowercase() {
        //then
        assertEquals(
                "lowercase",
                //when
                CommonNativeTextServiceLibrary.toLowercase(
                        //given
                        "LowerCase"
                )
        );
    }

    @Test
    public void textContainsTrue() {
        //then
        assertTrue(
                //when
                CommonNativeTextServiceLibrary.contains(
                        //given
                        "textContains",
                        "Contains"
                )
        );
    }

    @Test
    public void textContainsFalse() {
        //then
        assertFalse(
                //when
                CommonNativeTextServiceLibrary.contains(
                        //given
                        "textContains",
                        "contains"
                )
        );
    }
    
    @Test
    public void textEqualsTrue() {
        //then
        assertTrue(
                //when
                CommonNativeTextServiceLibrary.equals(
                        //given
                        "a",
                        "a"
                )
        );
    }

    @Test
    public void textEqualsFalse() {
        //then
        assertFalse(
                //when
                CommonNativeTextServiceLibrary.equals(
                        //given
                        "a",
                        "b"
                )
        );
    }

    @Test
    public void textAppend() {
        //then
        assertEquals(
                "a b",
                //when
                CommonNativeTextServiceLibrary.append(
                        //given
                        "a",
                        "b"
                )
        );
    }

    @Test
    public void textJoin() {
        //then
        assertEquals(
                "expected",
                //when
                CommonNativeTextServiceLibrary.join(
                        //given
                        "ex",
                        "ted",
                        "pec"
                )
        );
    }


    @Test
    public void shortenTextALengthGreaterThanB() {
        //then
        assertEquals(
                "t",
                //when
                CommonNativeTextServiceLibrary.shorten(
                        //given
                        "trim",
                        1
                )
        );
    }

    @Test
    public void shortenTextALengthLesserOrEqualsThanB() {
        //then
        assertEquals(
                "trim",
                //when
                CommonNativeTextServiceLibrary.shorten(
                        //given
                        "trim",
                       5
                )
        );
    }


    @Test
    public void textSplitDash() {
        //then
        assertEquals(
                Arrays.asList("text", "split"),
                //when
                CommonNativeTextServiceLibrary.split(
                        //given
                        "text-split",
                        "-"
                )
        );
    }

    @Test
    public void textSplitEmptyString() {
        //then
        assertEquals(
                Arrays.asList("t", "e", "x", "t", "-", "s", "p", "l", "i", "t"),
                //when
                CommonNativeTextServiceLibrary.split(
                        //given
                        "text-split",
                        ""
                )
        );
    }
}