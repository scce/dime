package info.scce.dime.process;

import info.scce.dime.exception.GUIEncounteredSignal;
import org.junit.Test;

import static org.junit.Assert.fail;

public class GUIResumerTest {

    @Test
    public void isAuthenticatedRequiredFalse() {
        try {
            new GUIResumer()
                    //when
                    .checkAuthentication(
                            //given
                            new ProcessCallFrame(),
                            "id"
                    );
        } catch (GUIEncounteredSignal e) {
            fail("GUIEncounteredSignal not excepted to be thrown");
        }
        //then
    }
}