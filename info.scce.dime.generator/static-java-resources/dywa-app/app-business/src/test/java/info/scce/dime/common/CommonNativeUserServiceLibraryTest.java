package info.scce.dime.common;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommonNativeUserServiceLibraryTest {


    @Test
    public void createSaltedSHA512HashEmptyString() {
        //then
        assertEquals(
                "",
                //when
                CommonNativeUserServiceLibrary.createSaltedSHA512Hash(
                        //given
                        ""
                )
        );
    }

    @Test
    public void createSaltedSHA512HashNull() {
        //then
        assertEquals(
                "",
                //when
                CommonNativeUserServiceLibrary.createSaltedSHA512Hash(
                        //given
                        null
                )
        );
    }

    @Test
    public void createSaltedSHA512HashString() {
        //when
        String hash = CommonNativeUserServiceLibrary.createSaltedSHA512Hash(
                //given
                "param"
        );
        //then
        assertTrue(hash.contains(":"));
        assertEquals(113, hash.length());
    }


}