package info.scce.dime.rest;

import org.junit.Test;

import javax.persistence.OptimisticLockException;

import static org.junit.Assert.assertEquals;

public class RESTOLEMapperTest {

    @Test
    public void toResponse() {
        //given
        RESTOLEMapper restoleMapper = new RESTOLEMapper();
        OptimisticLockException optimisticLockException = new OptimisticLockException();
        //then
        assertEquals(
                409,
                //when
                restoleMapper
                        .toResponse(optimisticLockException)
                        .getStatus()
        );
    }
}