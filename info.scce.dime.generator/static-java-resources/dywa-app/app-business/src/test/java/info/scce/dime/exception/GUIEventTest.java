package info.scce.dime.exception;

import org.junit.Test;

import static org.junit.Assert.*;

public class GUIEventTest {

    @Test
    public void test() {
        //given
        String expectedName = "Name";
        String expectedInputs = "inputs";
        GUIEncounteredSignal.GUIEvent guiEvent = new GUIEncounteredSignal.GUIEvent(expectedName, expectedInputs);
        //when
        String actualName = guiEvent.getName();
        Object actualInputs = guiEvent.getInputs();
        //then
        assertEquals(expectedName, actualName);
        assertEquals(expectedInputs, actualInputs);
    }
}