package info.scce.dime.common;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CommonNativeServiceLibraryTest {


    @Test
    public void integerAdd() {
        //then
        assertEquals(
                3,
                //when
                CommonNativeServiceLibrary.integerAdd(
                        //given
                        1,
                        2
                )
        );
    }

    @Test
    public void realAdd() {
        //then
        assertEquals(
                3.0,
                //when
                CommonNativeServiceLibrary.realAdd(
                        //given
                        1.0,
                        2.0
                ),
                0
        );
    }

    @Test
    public void realDiv() {
        //then
        assertEquals(
                3.0,
                //when
                CommonNativeServiceLibrary.realDiv(
                        //given
                        3.0,
                        1.0
                ),
                0
        );
    }

    @Test
    public void intToReal() {
        //then
        assertEquals(
                1.0,
                //when
                CommonNativeServiceLibrary.intToReal(
                        //given
                        1
                ),
                0
        );
    }


    @Test
    public void integerEqualsTrue() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.integerEquals(
                        //given
                        5,
                        6
                )
        );
    }

    @Test
    public void integerEqualsFalse() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.integerEquals(
                        //given
                        5,
                        6
                )
        );
    }

    @Test
    public void integerGreaterIsGreater() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.integerGreater(
                        //given
                        1,
                        0
                )
        );
    }

    @Test
    public void integerGreaterIsEquals() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.integerGreater(
                        //given
                        0,
                        0
                )
        );
    }

    @Test
    public void integerGreaterIsLesser() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.integerGreater(
                        //given
                        0,
                        1
                )
        );
    }

    @Test
    public void intToString() {
        //then
        assertEquals(
                "1",
                //when
                CommonNativeServiceLibrary.intToString(
                        //given
                        1
                )
        );
    }

    @Test
    public void textEqualsTrue() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.textEquals(
                        //given
                        "a",
                        "a"
                )
        );
    }

    @Test
    public void textEqualsFalse() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.textEquals(
                        //given
                        "a",
                        "b"
                )
        );
    }

    @Test
    public void textAppend() {
        //then
        assertEquals(
                "a b",
                //when
                CommonNativeServiceLibrary.textAppend(
                        //given
                        "a",
                        "b"
                )
        );
    }

    @Test
    public void textJoin() {
        //then
        assertEquals(
                "expected",
                //when
                CommonNativeServiceLibrary.textJoin(
                        //given
                        "ex",
                        "ted",
                        "pec"
                )
        );
    }

    @Test
    public void shortenTextLessThan80Chars() {
        //then
        assertEquals(
                "trim",
                //when
                CommonNativeServiceLibrary.shortenText(
                        //given
                        " trim ",
                        1
                )
        );
    }

    @Test
    public void shortenTextMoreThan80Chars() {
        //then
        assertEquals(
                "o",
                //when
                CommonNativeServiceLibrary.shortenText(
                        //given
                        "oi4Iewohbeighaosheijie0oabaethaeTh3geing5queechoo6paefiez6oosh8fioPoodee2phoo5ai6",
                        1
                )
        );
    }

    @Test
    public void booleanSwitchNull() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.booleanSwitch(
                        //given
                        null
                )
        );
    }

    @Test
    public void booleanSwitchFalse() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.booleanSwitch(
                        //given
                        false
                )
        );
    }

    @Test
    public void booleanSwitchTrue() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.booleanSwitch(
                        //given
                        true
                )
        );
    }

    @Test
    public void objectEqualsNullNull() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.objectEquals(
                        //given
                        null,
                        null
                )
        );
    }


    @Test
    public void objectEqualsAA() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.objectEquals(
                        //given
                        "A",
                        "A"
                )
        );
    }

    @Test
    public void objectEqualsEmptyStringNull() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.objectEquals(
                        //given
                        "",
                        null
                )
        );
    }

    @Test
    public void objectEqualsNullEmptyString() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.objectEquals(
                        //given
                        null,
                        ""
                )
        );
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.objectEquals(
                        //given
                        "a",
                        "b"
                )
        );
    }

    @Test
    public void objectEqualsNullAB() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.objectEquals(
                        //given
                        "A",
                        "B"
                )
        );
    }

    @Test
    public void objectExistsFalse() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.objectExists(
                        //given
                        null
                )
        );
    }

    @Test
    public void objectTrue() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.objectExists(
                        //given
                        1
                )
        );
    }

    @Test
    public void textSplitDash() {
        //then
        assertEquals(
                Arrays.asList("text", "split"),
                //when
                CommonNativeServiceLibrary.textSplit(
                        //given
                        "text-split",
                        "-"
                )
        );
    }

    @Test
    public void textSplitEmptyString() {
        //then
        assertEquals(
                Arrays.asList("t", "e", "x", "t", "-", "s", "p", "l", "i", "t"),
                //when
                CommonNativeServiceLibrary.textSplit(
                        //given
                        "text-split",
                        ""
                )
        );
    }

    @Test
    public void convertToTextNull() {
        //then
        assertEquals(
                "null",
                //when
                CommonNativeServiceLibrary.convertToText(
                        //given
                        null
                )
        );
    }

    @Test
    public void convertToText1() {
        //then
        assertEquals(
                "1",
                //when
                CommonNativeServiceLibrary.convertToText(
                        //given
                        1
                )
        );
    }

    @Test
    public void createSaltedSHA512HashEmptyString() {
        //then
        assertEquals(
                "",
                //when
                CommonNativeServiceLibrary.createSaltedSHA512Hash(
                        //given
                        ""
                )
        );
    }

    @Test
    public void createSaltedSHA512HashNull() {
        //then
        assertEquals(
                "",
                //when
                CommonNativeServiceLibrary.createSaltedSHA512Hash(
                        //given
                        null
                )
        );
    }

    @Test
    public void createSaltedSHA512HashString() {
        //when
        String hash = CommonNativeServiceLibrary.createSaltedSHA512Hash(
                //given
                "param"
        );
        //then
        assertTrue(hash.contains(":"));
        assertEquals(113, hash.length());
    }

    @Test
    public void textIsNullFalse() {
        //false
        assertFalse(
                //when
                CommonNativeServiceLibrary.textIsNull(
                        //given
                        ""
                )
        );
    }

    @Test
    public void textIsNullTrue() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.textIsNull(
                        //given
                        null
                )
        );
    }

    @Test
    public void textLength() {
        //then
        assertEquals(
                1,
                //when
                CommonNativeServiceLibrary.textLength(
                        //given
                        "a"
                )
        );
    }

    @Test
    public void textToLowercase() {
        //then
        assertEquals(
                "lowercase",
                //when
                CommonNativeServiceLibrary.textToLowercase(
                        //given
                        "LowerCase"
                )
        );
    }

    @Test
    public void textContainsTrue() {
        //then
        assertTrue(
                //when
                CommonNativeServiceLibrary.textContains(
                        //given
                        "textContains",
                        "Contains"
                )
        );
    }

    @Test
    public void textContainsFalse() {
        //then
        assertFalse(
                //when
                CommonNativeServiceLibrary.textContains(
                        //given
                        "textContains",
                        "contains"
                )
        );
    }

}