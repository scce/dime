package info.scce.dime.common;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CommonNativeBoolServiceLibraryTest {


    @Test
    public void booleanSwitchFalse() {
        //then
        assertFalse(
                //when
                CommonNativeBoolServiceLibrary.booleanSwitch(
                        //given
                        false
                )
        );
    }

    @Test
    public void booleanSwitchTrue() {
        //then
        assertTrue(
                //when
                CommonNativeBoolServiceLibrary.booleanSwitch(
                        //given
                        true
                )
        );
    }

}