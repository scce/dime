package info.scce.dime.process;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ProcessCallFrameTest {

    @Test
    public void testIsAuthenticationRequired() {
        //given
        ProcessCallFrame processCallFrame = new ProcessCallFrame();
        //then
        assertFalse(
                //when
                processCallFrame.isAuthenticationRequired()
        );
        //given
        processCallFrame.setAuthenticationRequired(true);
        //then
        assertTrue(
                //when
                processCallFrame.isAuthenticationRequired()
        );
    }

    @Test
    public void testCallFrames() {
        //given
        ProcessCallFrame processCallFrame = new ProcessCallFrame();
        //then
        assertEquals(
                Collections.emptyList(),
                //when
                processCallFrame.getCallFrames()
        );
        //given
        List<CallFrame> callFrames = Collections.emptyList();
        processCallFrame = new ProcessCallFrame(callFrames);
        //then
        assertEquals(
                callFrames,
                //when
                processCallFrame.getCallFrames()
        );
    }
}