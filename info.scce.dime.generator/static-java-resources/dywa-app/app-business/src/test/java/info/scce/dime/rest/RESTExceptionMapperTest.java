package info.scce.dime.rest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RESTExceptionMapperTest {

    @Test
    public void toResponse() {
        //given
        RESTExceptionMapper restExceptionMapper = new RESTExceptionMapper();
        Exception exception = new Exception();
        //then
        assertEquals(
                500,
                //when
                restExceptionMapper
                        .toResponse(exception)
                        .getStatus()
        );
    }
}