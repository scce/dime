package info.scce.dime.rest;

import org.jboss.resteasy.spi.DefaultOptionsMethodException;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class RESTOptionsExceptionMapperTest {

    @Test
    public void toResponse() {
        //given
        RESTOptionsExceptionMapper restOptionsExceptionMapper = new RESTOptionsExceptionMapper();
        DefaultOptionsMethodException defaultOptionsMethodException = new DefaultOptionsMethodException(
                "test",
                Response.ok().build()
        );
        //then
        assertEquals(
                200,
                //when
                restOptionsExceptionMapper
                        .toResponse(defaultOptionsMethodException)
                        .getStatus()
        );
    }

}