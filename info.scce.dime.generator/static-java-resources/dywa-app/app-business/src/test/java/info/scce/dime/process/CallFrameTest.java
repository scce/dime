package info.scce.dime.process;

import org.junit.Test;

import javax.enterprise.inject.spi.BeanManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

class FakeJsonContext extends JSONContext {
    @Override
    public DIMEProcessContext toContext(BeanManager bm, ProcessCallFrame callStack) {
        return null;
    }
}

class FakeDIMEProcessContext implements DIMEProcessContext {

    private final JSONContext jsonContext;

    FakeDIMEProcessContext(JSONContext jsonContext) {
        this.jsonContext = jsonContext;
    }

    @Override
    public JSONContext toJSON() {
        return jsonContext;
    }
}

public class CallFrameTest {

    @Test
    public void firstConstructor() {
        //given
        CallFrame callFrame = new CallFrame();
        //when
        JSONContext context = callFrame.getContext();
        String majorGUI = callFrame.getMajorGUI();
        String pointer = callFrame.getPointer();
        //then
        assertNull(context);
        assertNull(majorGUI);
        assertNull(pointer);
    }

    @Test
    public void secondConstructor() {
        //given
        FakeJsonContext expectedJsonContext = new FakeJsonContext();
        String expectedPointer = "pointer";
        CallFrame callFrame = new CallFrame(expectedPointer, new FakeDIMEProcessContext(expectedJsonContext));
        //when
        JSONContext actualJsonContext = callFrame.getContext();
        String actualMajorGUI = callFrame.getMajorGUI();
        String actualPointer = callFrame.getPointer();
        //then
        assertEquals(expectedJsonContext, actualJsonContext);
        assertNull(actualMajorGUI);
        assertEquals(expectedPointer, actualPointer);
    }

    @Test
    public void thirdConstructor() {
        //given
        FakeJsonContext expectedJsonContext = new FakeJsonContext();
        String expectedPointer = "pointer";
        String expectedMajorGUI = "majorGUI";
        CallFrame callFrame = new CallFrame(expectedPointer, new FakeDIMEProcessContext(expectedJsonContext), expectedMajorGUI);
        //when
        JSONContext actualJsonContext = callFrame.getContext();
        String actualMajorGUI = callFrame.getMajorGUI();
        String actualPointer = callFrame.getPointer();
        //then
        assertEquals(expectedJsonContext, actualJsonContext);
        assertEquals(expectedMajorGUI, actualMajorGUI);
        assertEquals(expectedPointer, actualPointer);
    }

    @Test
    public void emptyConstructorAndSetters() {
        //given
        FakeJsonContext expectedJsonContext = new FakeJsonContext();
        String expectedPointer = "pointer";
        String expectedMajorGUI = "majorGUI";
        CallFrame callFrame = new CallFrame();
        //when
        callFrame.setContext(expectedJsonContext);
        callFrame.setPointer(expectedPointer);
        callFrame.setMajorGUI(expectedMajorGUI);
        JSONContext actualJsonContext = callFrame.getContext();
        String actualMajorGUI = callFrame.getMajorGUI();
        String actualPointer = callFrame.getPointer();
        //then
        assertEquals(expectedJsonContext, actualJsonContext);
        assertEquals(expectedMajorGUI, actualMajorGUI);
        assertEquals(expectedPointer, actualPointer);
    }
}
