package info.scce.dime.common;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommonNativeIntegerServiceLibraryTest {

    @Test
    public void integerAdd() {
        //then
        assertEquals(
                3,
                //when
                CommonNativeIntegerServiceLibrary.add(
                        //given
                        1,
                        2
                )
        );
    }


    @Test
    public void divide() {
        //then
        assertEquals(
                3.0,
                //when
                CommonNativeIntegerServiceLibrary.divide(
                        //given
                        3,
                        1
                ),
                0
        );
    }

    @Test
    public void intToReal() {
        //then
        assertEquals(
                1.0,
                //when
                CommonNativeIntegerServiceLibrary.toReal(
                        //given
                        1
                ),
                0
        );
    }


    @Test
    public void integerEqualsTrue() {
        //then
        assertTrue(
                //when
                CommonNativeIntegerServiceLibrary.equals(
                        //given
                        5,
                        5
                )
        );
    }

    @Test
    public void integerEqualsFalse() {
        //then
        assertFalse(
                //when
                CommonNativeIntegerServiceLibrary.equals(
                        //given
                        5,
                        6
                )
        );
    }

    @Test
    public void integerGreaterIsGreater() {
        //then
        assertTrue(
                //when
                CommonNativeIntegerServiceLibrary.greater(
                        //given
                        1,
                        0
                )
        );
    }

    @Test
    public void integerGreaterIsEquals() {
        //then
        assertFalse(
                //when
                CommonNativeIntegerServiceLibrary.greater(
                        //given
                        0,
                        0
                )
        );
    }

    @Test
    public void integerGreaterIsLesser() {
        //then
        assertFalse(
                //when
                CommonNativeIntegerServiceLibrary.greater(
                        //given
                        0,
                        1
                )
        );
    }

    @Test
    public void intToString() {
        //then
        assertEquals(
                "1",
                //when
                CommonNativeIntegerServiceLibrary.toString(
                        //given
                        1
                )
        );
    }

}