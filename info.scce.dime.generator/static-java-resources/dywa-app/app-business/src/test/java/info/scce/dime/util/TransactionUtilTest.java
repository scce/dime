package info.scce.dime.util;

import org.junit.Test;

import javax.transaction.*;

import static org.junit.Assert.*;

class FakeUserTransaction implements UserTransaction {

    private final int status;
    private boolean beginCalled = false;
    private boolean commitCalled = false;
    private boolean rollbackCalled = false;

    public FakeUserTransaction(int status) {
        this.status = status;
    }

    @Override
    public void begin() {
        beginCalled = true;
    }

    @Override
    public void commit() throws SecurityException, IllegalStateException {
        commitCalled = true;
    }

    @Override
    public void rollback() throws IllegalStateException, SecurityException {
        rollbackCalled = true;
    }

    @Override
    public void setRollbackOnly() throws IllegalStateException {

    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setTransactionTimeout(int i) {

    }

    public boolean isBeginCalled() {
        return beginCalled;
    }

    public boolean isCommitCalled() {
        return commitCalled;
    }

    public boolean isRollbackCalled() {
        return rollbackCalled;
    }
}

public class TransactionUtilTest {

    @Test
    public void beginTxSuccessStatusNoTransaction() throws SystemException, NotSupportedException {
        //given
        FakeUserTransaction transaction = new FakeUserTransaction(Status.STATUS_NO_TRANSACTION);
        //when
        TransactionUtil.beginTX(transaction);
        //then
        assertTrue(transaction.isBeginCalled());
        assertFalse(transaction.isCommitCalled());
        assertFalse(transaction.isRollbackCalled());
    }

    @Test
    public void beginTxSuccessStatusRolledback() throws SystemException, NotSupportedException {
        //given
        FakeUserTransaction transaction  = new FakeUserTransaction(Status.STATUS_ROLLEDBACK);
        //when
        TransactionUtil.beginTX(transaction);
        //then
        assertTrue(transaction.isBeginCalled());
        assertFalse(transaction.isCommitCalled());
        assertFalse(transaction.isRollbackCalled());
    }

    @Test
    public void beginTxFailed() throws SystemException, NotSupportedException {
        //given
        FakeUserTransaction transaction = new FakeUserTransaction(Status.STATUS_COMMITTED);
        try {
            //when
            TransactionUtil.beginTX(transaction);
            fail("Expected IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            //then
            assertEquals("Transaction is neither in state '6' nor '0', but: 3", e.getMessage());
            assertFalse(transaction.isBeginCalled());
            assertFalse(transaction.isCommitCalled());
            assertFalse(transaction.isRollbackCalled());
        }
    }

    @Test
    public void commitTxSuccess() throws HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException {
        //given
        FakeUserTransaction transaction = new FakeUserTransaction(Status.STATUS_ACTIVE);
        //when
        TransactionUtil.commitTX(transaction);
        //then
        assertTrue(transaction.isCommitCalled());
        assertFalse(transaction.isBeginCalled());
        assertFalse(transaction.isRollbackCalled());
    }

    @Test
    public void commitTxNullParameter() throws HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException {
        //when
        TransactionUtil.commitTX(null);
    }

    @Test
    public void commitTxRollback() throws HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException {
        //given
        FakeUserTransaction transaction = new FakeUserTransaction(Status.STATUS_MARKED_ROLLBACK);
        //when
        TransactionUtil.commitTX(transaction);
        //then
        assertFalse(transaction.isCommitCalled());
        assertFalse(transaction.isBeginCalled());
        assertTrue(transaction.isRollbackCalled());
    }

    @Test
    public void rollbackTxNullParameter() throws SystemException {
        //when
        TransactionUtil.rollbackTX(null);
    }
}