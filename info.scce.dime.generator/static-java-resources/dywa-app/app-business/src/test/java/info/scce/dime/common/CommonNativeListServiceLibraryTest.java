package info.scce.dime.common;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CommonNativeListServiceLibraryTest {

    @Test
    public void swapSameIndices() {
        //given
        List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
        int index = 1;
        //when
        List<String> result = CommonNativeListServiceLibrary.swap(list, index, index);
        //then
        assertEquals(
                "one",
                result.get(0)
        );
        assertEquals(
                "two",
                result.get(1)
        );
    }

    @Test
    public void swapIndexNegative() {
        //given
        List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
        int firstIndex = -1;
        int secondIndex = 1;
        //when
        List<String> result = CommonNativeListServiceLibrary.swap(list, firstIndex, secondIndex);
        //then
        assertEquals(
                "one",
                result.get(0)
        );
        assertEquals(
                "two",
                result.get(1)
        );
    }

    @Test
    public void swapIndexGreaterThanListSize() {
        //given
        List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
        int firstIndex = 1;
        int secondIndex = 3;
        //when
        List<String> result = CommonNativeListServiceLibrary.swap(list, firstIndex, secondIndex);
        //then
        assertEquals(
                "one",
                result.get(0)
        );
        assertEquals(
                "two",
                result.get(1)
        );
    }

    @Test
    public void swap() {
        //given
        List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
        int firstIndex = 0;
        int secondIndex = 1;
        //when
        List<String> result = CommonNativeListServiceLibrary.swap(list, firstIndex, secondIndex);
        //then
        assertEquals(
                "two",
                result.get(0)
        );
        assertEquals(
                "one",
                result.get(1)
        );
    }

    @Test
    public void forwards() {
        //given
        String two = "two";
        List<String> list = new ArrayList<>(Arrays.asList("one", two));
        //when
        List<String> result = CommonNativeListServiceLibrary.forwards(list, two);
        //then
        assertEquals(
                two,
                result.get(0)
        );
        assertEquals(
                "one",
                result.get(1)
        );
    }

    @Test
    public void backwards() {
        //given
        String one = "one";
        List<String> list = new ArrayList<>(Arrays.asList(one, "two"));
        //when
        List<String> result = CommonNativeListServiceLibrary.backwards(list, one);
        //then
        assertEquals(
                "two",
                result.get(0)
        );
        assertEquals(
                one,
                result.get(1)
        );
    }

    @Test
    public void indexOf() {
        //given
        String one = "one";
        String two = "two";
        List<String> list = new ArrayList<>(Arrays.asList(one, two));
        //then
        assertEquals(
                0,
                //when
                CommonNativeListServiceLibrary.indexOf(list, one, 0)
        );
        //then
        assertEquals(
                1,
                //when
                CommonNativeListServiceLibrary.indexOf(list, one, 1)
        );
        assertEquals(
                1,
                //when
                CommonNativeListServiceLibrary.indexOf(list, two, 0)
        );
        //then
        assertEquals(
                2,
                //when
                CommonNativeListServiceLibrary.indexOf(list, two, 1)
        );
    }

    @Test
    public void addAsFirst() {
        //given
        String zero = "zero";
        String one = "one";
        String two = "two";
        List<String> list = new ArrayList<>(Arrays.asList(one, two));
        //when
        List<String> result = CommonNativeListServiceLibrary.addAsFirst(list, zero);
        //then
        assertEquals(
                zero,
                result.get(0)
        );
        assertEquals(
                one,
                result.get(1)
        );
        assertEquals(
                two,
                result.get(2)
        );
    }

    @Test
    public void addAsLast() {
        //given
        String one = "one";
        String two = "two";
        String three = "three";
        List<String> list = new ArrayList<>(Arrays.asList(one, two));
        //when
        List<String> result = CommonNativeListServiceLibrary.addAsLast(list, three);
        //then
        assertEquals(
                one,
                result.get(0)
        );
        assertEquals(
                two,
                result.get(1)
        );
        assertEquals(
                three,
                result.get(2)
        );
    }

    @Test
    public void isEmptyFalse() {
        //given
        List<String> list = Collections.singletonList("one");
        //then
        assertFalse(
                //when
                CommonNativeListServiceLibrary.isEmpty(list)
        );
    }

    @Test
    public void isEmptyTrue() {
        //given
        List<String> list = Collections.emptyList();
        //then
        assertTrue(
                //when
                CommonNativeListServiceLibrary.isEmpty(list)
        );
    }

    @Test
    public void isInListFalse() {
        //given
        List<String> list = Collections.singletonList("one");
        //then
        assertFalse(
                //when
                CommonNativeListServiceLibrary.isInList(list, 2)
        );
    }

    @Test
    public void isInListTrue() {
        //given
        List<String> list = Collections.singletonList("one");
        //then
        assertTrue(
                //when
                CommonNativeListServiceLibrary.isInList(list, 0)
        );
    }

    @Test
    public void getNext() {
        //given
        String one = "one";
        String two = "two";
        List<String> list = Arrays.asList(one, two);
        //then
        assertEquals(
                two,
                //when
                CommonNativeListServiceLibrary.getNext(list, one)
        );
    }

    @Test
    public void getPrevious() {
        //given
        String one = "one";
        String two = "two";
        List<String> list = Arrays.asList(one, two);
        //then
        assertEquals(
                one,
                //when
                CommonNativeListServiceLibrary.getPrevious(list, two)
        );
    }

    @Test
    public void remove() {
        //given
        String one = "one";
        String two = "two";
        List<String> list = new ArrayList<>(Arrays.asList(one, two));
        //when
        List<String> result = CommonNativeListServiceLibrary.remove(list, two);
        //then
        assertEquals(
                one,
                result.get(0)
        );
        assertEquals(1, result.size());
    }

    @Test
    public void clear() {
        //given
        String one = "one";
        String two = "two";
        List<String> list = new ArrayList<>(Arrays.asList(one, two));
        //when
        List<String> result = CommonNativeListServiceLibrary.clear(list);
        //then
        assertEquals(0, result.size());
    }

    @Test
    public void concat() {
        //given
        String one = "one";
        String two = "two";
        List<String> first = Collections.singletonList(one);
        List<String> second = Collections.singletonList(two);
        //when
        List<String> result = CommonNativeListServiceLibrary.concat(first, second);
        //then
        assertEquals(
                one,
                result.get(0)
        );
        assertEquals(
                two,
                result.get(1)
        );
    }

    @Test
    public void removeByIndex() {
        //given
        List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
        int index = 0;
        //when
        List<String> result = CommonNativeListServiceLibrary.removeByIndex(list, index);
        //then
        assertEquals(
                "two",
                result.get(0)
        );
        assertEquals(
                1,
                result.size()
        );
    }

    @Test
    public void addAll() {
        //given
        String one = "one";
        String two = "two";
        List<String> first = new ArrayList<>(Collections.singletonList(one));
        List<String> second = Collections.singletonList(two);
        //when
        List<String> result = CommonNativeListServiceLibrary.addAll(first, second);
        //then
        assertEquals(
                one,
                result.get(0)
        );
        assertEquals(
                two,
                result.get(1)
        );
    }

    @Test
    public void reverse() {
        //given
        List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
        //when
        List<String> result = CommonNativeListServiceLibrary.reverse(list);
        //then
        assertEquals(
                "one",
                result.get(1)
        );
        assertEquals(
                "two",
                result.get(0)
        );
    }

    @Test
    public void insertAtRandomPosition() {
        //given
        String two = "two";
        String one = "one";
        List<String> list = new ArrayList<>(Collections.singletonList(one));
        //when
        List<String> result = CommonNativeListServiceLibrary.insertAtRandomPosition(list, two);
        //then
        assertTrue(
                result.contains(one)
        );
        assertTrue(
                result.contains(two)
        );
    }

    @Test
    public void firstElements() {
        //given
        String one = "one";
        String two = "two";
        int tol = 1;
        List<String> list = new ArrayList<>(Arrays.asList(one, two));
        //when
        List<String> result = CommonNativeListServiceLibrary.firstElements(list, tol);
        //then
        assertEquals(
                tol,
                result.size()
        );
        assertEquals(
                "one",
                result.get(0)
        );
    }

    @Test
    public void firstElementsNegativeIndex() {
        //given
        String one = "one";
        String two = "two";
        int tol = -1;
        List<String> list = new ArrayList<>(Arrays.asList(one, two));
        //when
        List<String> result = CommonNativeListServiceLibrary.firstElements(list, tol);
        //then
        assertTrue(result.isEmpty());
    }

    @Test
    public void sublist() {
        //given
        String one = "one";
        String two = "two";
        String three = "three";
        List<String> list = new ArrayList<>(Arrays.asList(one, two, three));
        //when
        List<String> result = CommonNativeListServiceLibrary.sublist(list, 0, 1);
        //then
        assertEquals(
                1,
                result.size()
        );
        assertEquals(
                "one",
                result.get(0)
        );
    }

    @Test
    public void sublistEmptyList() {
        //given
        String one = "one";
        List<String> list = Collections.singletonList(one);
        //when
        List<String> result = CommonNativeListServiceLibrary.sublist(list, -1, 1);
        //then
        assertTrue(
                result.isEmpty()
        );
    }
}