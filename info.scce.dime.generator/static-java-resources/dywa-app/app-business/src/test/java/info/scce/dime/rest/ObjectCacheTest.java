package info.scce.dime.rest;

import info.scce.dime.util.Identifiable;
import org.junit.Test;

import static org.junit.Assert.*;

class FakeIdentifiable implements Identifiable {

    @Override
    public long getDywaId() {
        return 0;
    }

    @Override
    public long getDywaVersion() {
        return 0;
    }

    @Override
    public void setDywaVersion(long version) {

    }

    @Override
    public String getDywaName() {
        return null;
    }

    @Override
    public void setDywaName(String name) {

    }
}

public class ObjectCacheTest {

    @Test
    public void testRestTo() {
        ObjectCache objectCache = new ObjectCache();
        FakeIdentifiable identifiable = new FakeIdentifiable();
        RESTBaseImpl restTo = new RESTBaseImpl();

        //absent
        assertFalse(objectCache.containsRestTo(identifiable));
        assertNull(objectCache.getRestTo(identifiable));

        //put
        objectCache.putRestTo(identifiable, restTo);

        //present
        assertTrue(objectCache.containsRestTo(identifiable));
        assertEquals(restTo, objectCache.getRestTo(identifiable));
    }

    @Test
    public void testTransient() {
        ObjectCache objectCache = new ObjectCache();
        RESTBaseImpl restTo = new RESTBaseImpl();
        FakeIdentifiable identifiable = new FakeIdentifiable();

        //absent
        assertFalse(objectCache.containsTransient(restTo));
        assertNull(objectCache.getTransient(restTo));

        //put
        objectCache.putTransient(restTo, identifiable);

        //present
        assertTrue(objectCache.containsTransient(restTo));
        assertEquals(identifiable, objectCache.getTransient(restTo));
    }

    @Test
    public void testSelective() {
        ObjectCache objectCache = new ObjectCache();
        RESTBaseImpl restTo = new RESTBaseImpl();
        FakeIdentifiable identifiable = new FakeIdentifiable();

        //absent
        assertFalse(objectCache.containsSelective(restTo, "selective"));
        assertFalse(objectCache.containsSelective(identifiable, "selective"));

        //put
        objectCache.putSelective(restTo, "selective");
        objectCache.putSelective(identifiable, "selective");

        //present
        assertTrue(objectCache.containsSelective(restTo, "selective"));
        assertTrue(objectCache.containsSelective(identifiable, "selective"));
    }
}