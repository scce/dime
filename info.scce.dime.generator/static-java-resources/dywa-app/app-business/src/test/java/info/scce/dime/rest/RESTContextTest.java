package info.scce.dime.rest;

import info.scce.dime.process.ProcessCallFrame;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RESTContextTest {

    @Test
    public void getCallStack() {
        //given
        RESTContext restContext = new RESTContext();
        ProcessCallFrame processCallFrame = new ProcessCallFrame();
        restContext.setCallStack(processCallFrame);
        //then
        assertEquals(
                processCallFrame,
                //when
                restContext.getCallStack()
        );
    }
}