package info.scce.dime.rest;

import info.scce.dime.util.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

public class RESTBaseImplTest {

    @Test
    public void setDywaVersion() {
        //given
        RESTBaseType restBase = new RESTBaseImpl();
        assertEquals(0, restBase.getDywaVersion());
        assertFalse(restBase.isDywaVersionSet());
        //when
        restBase.setDywaVersion(1);
        //then
        assertEquals(1, restBase.getDywaVersion());
        assertTrue(restBase.isDywaVersionSet());
    }

    @Test
    public void setDywaName() {
        //given
        RESTBaseType restBase = new RESTBaseImpl();
        assertNull(restBase.getDywaName());
        assertFalse(restBase.isDywaNameSet());
        //when
        restBase.setDywaName("DywaName");
        //then
        assertEquals("DywaName",restBase.getDywaName());
        assertTrue(restBase.isDywaNameSet());
    }

    @Test
    public void testEqualsSameObject() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        //then
        assertEquals(a, a);
    }

    @Test
    public void testEqualsDifferentObjects() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        RESTBaseType b = new RESTBaseImpl();
        //then
        assertNotEquals(a, b);
    }

    @Test
    public void testEqualsDifferentClasses() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        //then
        assertNotEquals(a, "");
    }

    @Test
    public void testEqualsSameDywaIds() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        RESTBaseType b = new RESTBaseImpl();
        a.setDywaId(1);
        b.setDywaId(1);
        //then
        assertEquals(a, b);
    }

    @Test
    public void testEqualsDifferentDywaIds() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        RESTBaseType b = new RESTBaseImpl();
        a.setDywaId(1);
        b.setDywaId(2);
        //then
        assertNotEquals(a, b);
    }

    @Test
    public void testEqualsDywaIdTransient() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        RESTBaseType b = new RESTBaseImpl();
        a.setDywaId(Constants.DYWA_ID_TRANSIENT);
        b.setDywaId(1);
        //then
        assertNotEquals(a, b);
        //given
        a.setDywaId(1);
        b.setDywaId(Constants.DYWA_ID_TRANSIENT);
        //then
        assertNotEquals(a, b);
    }

    @Test
    public void testHashCodeEmptyDywaId() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        //then
        assertEquals(
                0,
                //when
                a.hashCode()
        );
    }

    @Test
    public void testHashCodeSetDywaId() {
        //given
        RESTBaseType a = new RESTBaseImpl();
        a.setDywaId(1);
        //then
        assertEquals(
                1,
                //when
                a.hashCode()
        );
    }
}