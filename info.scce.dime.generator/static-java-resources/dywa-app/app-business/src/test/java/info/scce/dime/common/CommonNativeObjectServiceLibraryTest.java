package info.scce.dime.common;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommonNativeObjectServiceLibraryTest {


    @Test
    public void objectEqualsNullNull() {
        //then
        assertTrue(
                //when
                CommonNativeObjectServiceLibrary.objectEquals(
                        //given
                        null,
                        null
                )
        );
    }


    @Test
    public void objectEqualsAA() {
        //then
        assertTrue(
                //when
                CommonNativeObjectServiceLibrary.objectEquals(
                        //given
                        "A",
                        "A"
                )
        );
    }

    @Test
    public void objectEqualsEmptyStringNull() {
        //then
        assertFalse(
                //when
                CommonNativeObjectServiceLibrary.objectEquals(
                        //given
                        "",
                        null
                )
        );
    }

    @Test
    public void objectEqualsNullEmptyString() {
        //then
        assertFalse(
                //when
                CommonNativeObjectServiceLibrary.objectEquals(
                        //given
                        null,
                        ""
                )
        );
        //then
        assertFalse(
                //when
                CommonNativeObjectServiceLibrary.objectEquals(
                        //given
                        "a",
                        "b"
                )
        );
    }

    @Test
    public void objectEqualsNullAB() {
        //then
        assertFalse(
                //when
                CommonNativeObjectServiceLibrary.objectEquals(
                        //given
                        "A",
                        "B"
                )
        );
    }

    @Test
    public void isNullTrue() {
        //then
        assertTrue(
                //when
                CommonNativeObjectServiceLibrary.isNull(
                        //given
                        null
                )
        );
    }

    @Test
    public void isNullFalse() {
        //then
        assertFalse(
                //when
                CommonNativeObjectServiceLibrary.isNull(
                        //given
                        1
                )
        );
    }

    @Test
    public void convertToText1() {
        //then
        assertEquals(
                "1",
                //when
                CommonNativeObjectServiceLibrary.convertToText(
                        //given
                        1
                )
        );
    }
}