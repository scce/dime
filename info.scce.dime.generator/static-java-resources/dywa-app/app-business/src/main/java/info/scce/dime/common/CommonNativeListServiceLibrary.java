package info.scce.dime.common;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CommonNativeListServiceLibrary {

    public static <T> List<T> swap(List<T> list, long index1, long index2) {
        if (index1 == index2) {
            return list;
        }

        int min = Math.min((int) index1, (int) index2);
        if (min < 0) {
            return list;
        }

        int max = Math.max((int) index1, (int) index2);
        if (max >= list.size()) {
            return list;
        }

        list.add(max, list.set(min, list.remove(max)));
        return list;
    }

    public static <T> List<T> sublist(List<T> list, long from, long to) {
        int fromInt = java.lang.Math.toIntExact(from);
        int toInt = java.lang.Math.toIntExact(to);
        if (fromInt >= 0 && toInt < list.size() && fromInt < list.size()) {
            return list.subList(fromInt, toInt);
        }
        return Collections.emptyList();
    }

    public static <T> List<T> firstElements(List<T> list, long tol) {
        int to = java.lang.Math.toIntExact(tol);
        if (to >= 0) {
            return list.subList(0, Math.min(to, list.size()));
        }
        return Collections.emptyList();
    }

    public static <T> int indexOf(List<T> list, T object, int add) {
        return list.indexOf(object) + add;
    }

    public static <T> List<T> forwards(List<T> list, T object) {
        int index = list.indexOf(object);
        return swap(list, index, index - 1);
    }

    public static <T> List<T> backwards(List<T> list, T object) {
        int index = list.indexOf(object);
        return swap(list, index, index + 1);
    }

    public static <T> List<T> addAsFirst(List<T> list, T object) {
        list.add(0, object);
        return list;
    }

    public static <T> List<T> addAsLast(List<T> list, T object) {
        list.add(object);
        return list;
    }

    public static <T> boolean isEmpty(List<T> list) {
        return list.isEmpty();
    }

    public static <T> boolean isInList(List<T> list, int index) {
        return index >= 0 && index < list.size();
    }

    public static <T> T getNext(List<T> list, T object) {
        int index = list.indexOf(object) + 1;
        return isInList(list, index) ? list.get(index) : null;
    }

    public static <T> T getPrevious(List<T> list, T object) {
        int index = list.indexOf(object) - 1;
        return isInList(list, index) ? list.get(index) : null;
    }

    public static <T> List<T> remove(List<T> list, T object) {
        list.remove(object);
        return list;
    }

    public static <T> List<T> clear(List<T> list) {
        list.clear();
        return list;
    }

    public static <T, U extends T> List<T> concat(List<T> listA, List<U> listB) {
        List<T> listC = new ArrayList<>(listA.size() + listB.size());
        listC.addAll(listA);
        listC.addAll(listB);
        return listC;
    }

    public static <T> List<T> removeByIndex(List<T> list, long index) {
        int i = (int) index;
        if (i >= 0 && i < list.size()) {
            list.remove(i);
        }
        return list;
    }

    public static <T> List<T> addAll(List<T> list1, List<T> list2) {
        list1.addAll(list2);
        return list1;
    }

    public static <T> List<T> reverse(List<T> list1) {
        return Lists.reverse(list1);
    }

    public static <T> List<T> insertAtRandomPosition(List<T> list, T element) {
        list.add((int) (Math.random() * list.size()), element);
        return list;
    }

}
