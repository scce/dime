package info.scce.dime.auth;

import info.scce.dime.config.SystemPropertyInterface;

import javax.ws.rs.core.NewCookie;

public class JWTCookie extends NewCookie {

    private final SystemPropertyInterface serverTier;

    private JWTCookie(
            String value,
            String comment,
            int maxAge,
            SystemPropertyInterface serverTier
    ) {
        super(
                "jwt",
                value,
                "/",
                null,
                comment,
                maxAge,
                true,//this setting is important. see https://gitlab.com/scce/dime/-/issues/989
                true//this setting is important. see https://gitlab.com/scce/dime/-/issues/989
        );
        this.serverTier = serverTier;
    }

    public static JWTCookie loggedIn(
            String value,
            SystemPropertyInterface serverTier
    ) {
        return new JWTCookie(
                value,
                "Cookie which holds a valid JWT; the user is logged in.",
                28800,// 8 hours,
                serverTier
        );
    }

    public static JWTCookie loggedOut(
            SystemPropertyInterface serverTier
    ) {
        return new JWTCookie(
                "",
                "Cookie which holds an empty string; the user is logged out.",
                0,// tells the client to expire the token
                serverTier
        );
    }

    public String toString() {
        String value = super.toString();
        if (serverTier.value().equals("local")) {
            return value + ";SameSite=None";
        }
        return value;
    }
}
