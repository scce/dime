package info.scce.dime.auth;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

public class CSRFGuard {

    public final static String CSRF_TOKEN_NAME = "X-csrf-token";

    private final HttpServletRequest httpServletRequest;

    public static CSRFGuard create(HttpServletRequest httpServletRequest) {
        return new CSRFGuard(httpServletRequest);
    }

    private CSRFGuard(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    public boolean isNotProtected() {
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies == null) {
            return true;
        }
        Optional<Cookie> optionalCookie = Arrays
                .stream(cookies)
                .filter(candidate -> candidate.getName().equals(CSRFCookie.CSRF_COOKIE_NAME))
                .findFirst();
        Optional<String> optionalHeader = Optional
                .ofNullable(httpServletRequest.getHeader(CSRF_TOKEN_NAME));
        if (!optionalCookie.isPresent()) {
            return true;
        }
        if (!optionalHeader.isPresent()) {
            return true;
        }
        String cookie = optionalCookie.get().getValue();
        if (cookie.isEmpty()) {
            return true;
        }
        String header = optionalHeader.get();
        if (header.isEmpty()) {
            return true;
        }
        return !cookie.equals(header);
    }

}
