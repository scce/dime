package info.scce.dime.auth;

import javax.ws.rs.core.NewCookie;
import java.security.SecureRandom;

public class CSRFCookie extends NewCookie {

    public final static String CSRF_COOKIE_NAME = "csrf-token";

    private CSRFCookie(
            String value,
            String comment,
            int maxAge
    ) {
        super(
                CSRF_COOKIE_NAME,
                value,
                "/",
                null,
                comment,
                maxAge,
                true,//this setting is important. see https://gitlab.com/scce/dime/-/issues/989
                false//we require JavaScript to be able to access this token
        );
    }

    public static CSRFCookie create() {
        SecureRandom random = new SecureRandom();
        return new CSRFCookie(
                String.valueOf(random.nextInt()),
                "Cookie which holds a valid JWT; the user is logged in.", // todo
                28800// 8 hours
        );
    }

}