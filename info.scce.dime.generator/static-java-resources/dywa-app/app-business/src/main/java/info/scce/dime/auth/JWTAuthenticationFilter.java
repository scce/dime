package info.scce.dime.auth;

import com.nimbusds.jose.JWSObject;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HttpMethod;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

public class JWTAuthenticationFilter extends AuthenticatingFilter {

    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        HttpServletRequest httpServletRequest = WebUtils.toHttp(request);
        if(CSRFGuard.create(httpServletRequest).isNotProtected()){
            // todo should we throw an exception instead?
            return failed();
        }
        final Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies == null) {
            return failed();
        }
        final Optional<Cookie> cookie = Arrays
                .stream(cookies)
                .filter(candidate -> candidate.getName().equals("jwt"))
                .findFirst();
        if (!cookie.isPresent()) {
            return failed();
        }
        String value = cookie.get().getValue();
        if (value.isEmpty()) {
            return failed();
        }
        return buildShiroToken(value);
    }

    private static UsernamePasswordToken failed() {
        // return empty token, that will fail authentication
        return new UsernamePasswordToken();
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {

        // allow unauthenticated OPTION requests
        if (HttpMethod.OPTIONS.equalsIgnoreCase(WebUtils.toHttp(request).getMethod())) {
            return true;
        }

        // try to login
        executeLogin(request, response);
        return true;
    }

    private JWTShiroToken buildShiroToken(String token) {
        try {
            final JWSObject jwsObject = JWSObject.parse(token);
            final Map<String, Object> payload = jwsObject.getPayload().toJSONObject();

            final long id = Long.parseLong(payload.get("sub").toString());

            return new JWTShiroToken(id, token);
        } catch (ParseException ex) {
            throw new AuthenticationException(ex);
        }

    }
}
