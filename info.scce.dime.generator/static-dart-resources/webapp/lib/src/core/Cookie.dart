class Cookie {
  static Cookie get(String cookies, String cookieName) {
    if (cookies.isEmpty) {
      return Cookie(null);
    }
    var name = cookieName + '=';
    var decodedCookie = Uri.decodeComponent(cookies);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c[0] == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return Cookie(c.substring(name.length, c.length));
      }
    }
    return Cookie(null);
  }

  final String value;

  Cookie(this.value);

  bool present() {
    return value != null && value.isNotEmpty;
  }
}
