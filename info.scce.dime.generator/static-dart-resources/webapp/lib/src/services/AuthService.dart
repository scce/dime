import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:app/src/core/dime_process_service.dart';
import 'package:app/src/url/RedirectUrl.dart';

class AuthService {
  Future<bool> auth(String username, String password) async {
    try {
      await request(username, password);
      redirect();
      return true;
    } on ProgressEvent catch (e) {
      print('ProgressEvent: ${e}');
      return failure(e);
    }
  }

  Future<HttpRequest> request(String username, String password) {
    final url = '${DIMEProcessService.getBaseUrl()}/rest/auth/authenticate';
    final headers = {'Content-Type': 'application/json'};
    final data = jsonEncode({'username': username, 'password': password});
    return HttpRequest.request(url,
        method: 'POST',
        requestHeaders: headers,
        sendData: data,
        withCredentials: false);
  }

  bool failure(ProgressEvent e) {
    // 401 response is delivered as error
    var ct = e.currentTarget;
    if (ct is HttpRequest) {
      print('Request.status: ${ct.status}');
      if (ct.status == 401) {
        // Future completes with 'false'
        return false;
      }
    }
    // in any other case forward the exception
    throw (e);
  }

  Future<HttpRequest> logout() {
    final url = '${DIMEProcessService.getBaseUrl()}/rest/auth/logout';
    return HttpRequest.request(url, method: 'POST');
  }

  void redirect() {
    var redirectUrl = RedirectUrl(window.location.href);
    if (redirectUrl.isValid()) {
      window.location.href = redirectUrl.url();
    }
  }
}
