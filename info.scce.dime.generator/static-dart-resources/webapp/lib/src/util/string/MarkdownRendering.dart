import 'package:markdown/markdown.dart';
import 'package:sanitize_html/sanitize_html.dart';

String renderMarkdown(String str) {
  return sanitizeHtml(
      markdownToHtml(str, extensionSet: ExtensionSet.gitHubFlavored));
}
