class RedirectUrl {
  final String _href;
  String _url;
  bool _isValid;

  RedirectUrl(this._href);

  bool isValid() {
    _decode();
    return _isValid;
  }

  String url() {
    _decode();
    return _url;
  }

  void _decode() {
    if (_isValid != null) {
      return;
    }
    // Extract the 'redirect' query parameter
    final uri = Uri.parse(_href);
    final encodedURL = uri.queryParameters['redirect'];
    if (encodedURL == null) {
      _isValid = false;
      return;
    }
    try {
      // decode & validate URL
      final decodedUrl = Uri.decodeComponent(encodedURL);
      _isValid = Uri.parse(decodedUrl).isAbsolute;
      if (_isValid) {
        _url = decodedUrl;
      }
    } catch (e) {
      _isValid = false;
      print('Error decoding or redirecting to URL: $e');
    }
  }
}
