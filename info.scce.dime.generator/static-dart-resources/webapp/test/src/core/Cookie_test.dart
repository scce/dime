import 'package:app/src/core/Cookie.dart';
import 'package:test/test.dart';

void main() {
  group('Cookie', () {
    test('.get one cookie', () {
      var cookies = 'csrf=kiw8C';
      expect(Cookie.get(cookies, 'csrf').value, 'kiw8C');
    });
    test('.get two cookies', () {
      var cookies = 'csrf=kiw8C; food=tripe;';
      expect(Cookie.get(cookies, 'csrf').value, 'kiw8C');
      expect(Cookie.get(cookies, 'food').value, 'tripe');
    });
    test('.get three cookies', () {
      var cookies = 'csrf=kiw8C; food=tripe; test1=Hello';
      expect(Cookie.get(cookies, 'csrf').value, 'kiw8C');
      expect(Cookie.get(cookies, 'food').value, 'tripe');
      expect(Cookie.get(cookies, 'test1').value, 'Hello');
    });
    test('.get exception', () {
      var cookies = 'csrf=kiw8C; food=tripe; test1=Hello';
      var cookie = Cookie.get(cookies, 'test2');
      expect(cookie.value, null);
      expect(cookie.present(), false);
    });
  });
}
