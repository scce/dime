import 'package:app/src/url/RedirectUrl.dart';
import 'package:test/test.dart';

void main() {
  group('RedirectUrl', () {
    test('empty string', () {
      var redirectUrl = RedirectUrl('');
      expect(redirectUrl.isValid(), false);
      expect(redirectUrl.url(), null);
    });
    test('no redirect parameter', () {
      var redirectUrl = RedirectUrl(
          'https://www.w3schools.com/js/js_window_location.asp');
      expect(redirectUrl.isValid(), false);
      expect(redirectUrl.url(), null);
    });
    test('redirect parameter is empty', () {
      var redirectUrl = RedirectUrl(
          'https://www.w3schools.com/js/js_window_location.asp?redirect=');
      expect(redirectUrl.isValid(), false);
      expect(redirectUrl.url(), null);
    });
    test('invalid redirect url encoding', () {
      var redirectUrl = RedirectUrl(
          'https://www.w3schools.com/js/js_window_location.asp?redirect=https%3%2F%2Fdime.scce.info');
      expect(redirectUrl.isValid(), false);
      expect(redirectUrl.url(), null);
    });
    test('valid url', () {
      var redirectUrl = RedirectUrl(
          'https://www.w3schools.com/js/js_window_location.asp?redirect=https%3A%2F%2Fdime.scce.info');
      expect(redirectUrl.isValid(), true);
      expect(redirectUrl.url(), 'https://dime.scce.info');
    });
  });
}
