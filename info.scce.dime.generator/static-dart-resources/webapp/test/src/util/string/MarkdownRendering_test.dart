import 'package:app/src/util/string/MarkdownRendering.dart';
import 'package:test/test.dart';

void main() {
  group('MarkdownRendering', () {
    test('.renderMarkdown strong', () {
      expect(renderMarkdown('Some **strong** text'),
          '<p>Some <strong>strong</strong> text</p>\n');
    });
    test('.renderMarkdown underline', () {
      expect(renderMarkdown('Some _underlined_ text'),
          '<p>Some <em>underlined</em> text</p>\n');
    });
    test('.renderMarkdown safe link', () {
      expect(renderMarkdown('[Safe Link](http://example.com)'),
          '<p><a href="http://example.com">Safe Link</a></p>\n');
    });
    test('.renderMarkdown bad md link', () {
      expect(renderMarkdown('[Bad Link](javascript:alert("XSS"))'),
          '<p><a>Bad Link</a></p>\n');
    });
    test('.renderMarkdown bad href link', () {
      expect(renderMarkdown('<a href="javascript:alert(\'XSS\')">Bad Link</a>'),
          '<p><a>Bad Link</a></p>\n');
    });
    test('.renderMarkdown safe image', () {
      expect(renderMarkdown('![Safe Image](http://example.com/image.png)'),
          '<p><img src="http://example.com/image.png" alt="Safe Image" /></p>\n');
    });
    test('.renderMarkdown bad image', () {
      expect(renderMarkdown('<img src="x" onerror="alert(\'XSS\');">'),
          '<img src="x" />\n');
    });
    test('.renderMarkdown script', () {
      expect(renderMarkdown('Bad <script>alert(\'XSS\');</script> script'),
          '<p>Bad  script</p>\n');
    });
  });
}
