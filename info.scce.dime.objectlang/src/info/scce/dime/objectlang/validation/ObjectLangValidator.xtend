/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.objectlang.validation

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.objectlang.ObjectLangExtension
import info.scce.dime.objectlang.objectLang.AttributeDef
import info.scce.dime.objectlang.objectLang.AttributeValueDef
import info.scce.dime.objectlang.objectLang.AttributeValueReferenceDef
import info.scce.dime.objectlang.objectLang.BooleanAttributeValueDef
import info.scce.dime.objectlang.objectLang.BooleanListAttributeValueDef
import info.scce.dime.objectlang.objectLang.ComplexInputDef
import info.scce.dime.objectlang.objectLang.ComplexListAttributeValuesDef
import info.scce.dime.objectlang.objectLang.EmptyList
import info.scce.dime.objectlang.objectLang.Import
import info.scce.dime.objectlang.objectLang.InputDef
import info.scce.dime.objectlang.objectLang.IntegerAttributeValueDef
import info.scce.dime.objectlang.objectLang.IntegerListAttributeValueDef
import info.scce.dime.objectlang.objectLang.NamedObject
import info.scce.dime.objectlang.objectLang.ObjectCreation
import info.scce.dime.objectlang.objectLang.ObjectDef
import info.scce.dime.objectlang.objectLang.ObjectExtend
import info.scce.dime.objectlang.objectLang.ObjectLangPackage
import info.scce.dime.objectlang.objectLang.ObjectsModel
import info.scce.dime.objectlang.objectLang.PrimitiveListAttributeValueDef
import info.scce.dime.objectlang.objectLang.ProcessArgumentDef
import info.scce.dime.objectlang.objectLang.ProcessOutput
import info.scce.dime.objectlang.objectLang.RealAttributeValueDef
import info.scce.dime.objectlang.objectLang.RealListAttributeValueDef
import info.scce.dime.objectlang.objectLang.TextAttributeValueDef
import info.scce.dime.objectlang.objectLang.TextListAttributeValueDef
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import java.util.Collections
import org.eclipse.xtext.validation.Check

/**
 * Custom validation rules.
 * 
 * @author Steve Bosselmann
 */
class ObjectLangValidator extends AbstractObjectLangValidator {

	extension DataExtension = DataExtension.instance
	extension ResourceExtension = new ResourceExtension
	extension ObjectLangExtension = new ObjectLangExtension

	@Check
	def checkImport(Import it) {
		if (path.nullOrEmpty) {
			error("Specify an ID or a project relative file path")
			return
		}
		
		val acceptedClasses = #[ObjectsModel, Data, Process]
		val id = path
		
		if (it.objectsModel.generations.exists[it.modelId == id]) {
			error('''Cyclic reference''')
			return
		}
		
		val obj = ReferenceRegistry.instance.getEObject(id)
		if (obj !== null && !acceptedClasses.exists[isInstance(obj)]) {
			error('''Supported types of imported objects: «acceptedClasses.map[simpleName].join(', ')»''')
			return
		}
		
		if (!it.objectsModel.otherObjectsModels.exists[generations.exists[it.modelId == id]]
				&& !it.eResource.project.getFile(path)?.exists) {
			error('''«path» not found''')
		}
	}
	
	def error(Import imp, String msg) {
		error(msg, ObjectLangPackage.Literals.IMPORT__PATH, "Failed to load resource")
	}
	
	@Check
	def checkObjectSrc(NamedObject objSrc) {
		if (objSrc.objectsModel.isDuplicateObjectName(objSrc.getName)) {
			error("Duplicate name", ObjectLangPackage.Literals.NAMED_OBJECT__NAME)
		}
	}
	
	def isDuplicateObjectName(ObjectsModel model, String name) {
		if (!name.nullOrEmpty) {
			val names = (model.objects + model.inputs).map[it.name].toList
			val frequency = Collections.frequency(names, name)
			return frequency > 1
		}
	}
	
	@Check
	def checkObjectExtend(ObjectExtend objExt) {
		val objDef = objExt.findFirstParent(ObjectDef)
		if (objDef == objExt.objSrc) {
			error("You really seek to provoke a stack overflow, huh? An object cannot extend itself!", ObjectLangPackage.Literals.OBJECT_EXTEND__OBJ_SRC)
		}
	}
	
	@Check
	def checkBooleanAttributeValueDef(BooleanAttributeValueDef valueDef) {
		valueDef.assertValueType(valueDef.eContainer, #[PrimitiveType.BOOLEAN])
	}
	
	@Check
	def checkBooleanListAttributeValueDef(BooleanListAttributeValueDef valueDef) {
		valueDef.assertValueListType(PrimitiveType.BOOLEAN)
	}
	
	@Check
	def checkIntegerAttributeValueDef(IntegerAttributeValueDef valueDef) {
		valueDef.assertValueType(valueDef.eContainer, #[PrimitiveType.INTEGER, PrimitiveType.TIMESTAMP])
	}
	
	@Check
	def checkIntegerListAttributeValueDef(IntegerListAttributeValueDef valueDef) {
		valueDef.assertValueListType(PrimitiveType.INTEGER, PrimitiveType.TIMESTAMP)
	}
	
	@Check
	def checkRealAttributeValueDef(RealAttributeValueDef valueDef) {
		valueDef.assertValueType(valueDef.eContainer, #[PrimitiveType.REAL])
	}
	
	@Check
	def checkRealListAttributeValueDef(RealListAttributeValueDef valueDef) {
		valueDef.assertValueListType(PrimitiveType.REAL)
	}
	
	@Check
	def checkTextAttributeValueDef(TextAttributeValueDef valueDef) {
		val parent = valueDef.findFirstParent(AttributeDef, ProcessArgumentDef)
		switch parent {
			
			ProcessArgumentDef: {
				// TODO
//				switch param : parent.key {
//					PrimitiveOutputPort: param.primitiveType
//					PrimitiveInputDef: param.primitiveType
//				}
			}
			
			AttributeDef: {
				val attribute = parent.key
				if (attribute instanceof Attribute) {
					if (attribute.isEnumAttribute) {
						val literals  = attribute.enumType?.enumLiterals
						if (!literals.exists[it.name == valueDef.value]) {
							error("Enum literal does not exist", ObjectLangPackage.Literals.TEXT_ATTRIBUTE_VALUE_DEF__VALUE)
							return
						}
					} else {
						valueDef.assertValueType(parent, #[PrimitiveType.TEXT, PrimitiveType.FILE])
					}
					
					var PrimitiveType primitiveType = null
					if (attribute instanceof PrimitiveAttribute) {
						primitiveType = attribute?.primitiveType
					} else {
						val output = valueDef.findFirstParent(ProcessArgumentDef)?.key
						if (output instanceof PrimitiveOutputPort) {
							primitiveType = output.dataType.mapOnDataType
						}
					}
					switch primitiveType {
						case FILE: {
							val fileName = valueDef.value
							val file = valueDef.eResource.project.getFolder("initFiles").getFile(fileName)
							if (!file.exists) {
								error("File does not exist", ObjectLangPackage.Literals.TEXT_ATTRIBUTE_VALUE_DEF__VALUE)
							}
						}
					}
				}
			}
		}
	}
	
	@Check
	def checkTextListAttributeValueDef(TextListAttributeValueDef valueDef) {
		valueDef.assertValueListType(PrimitiveType.TEXT, PrimitiveType.FILE)
	}
	
	@Check
	def checkComplexAttributeValueDef(ObjectCreation valueDef) {
		val attribute = valueDef.findFirstParent(AttributeDef)?.key
		if (attribute instanceof ComplexAttribute) {
			checkAttributeValueType(valueDef, attribute.dataType)
		} else {
			val output = valueDef.findFirstParent(ProcessArgumentDef)?.key
			if (output instanceof ComplexOutputPort) {
				checkAttributeValueType(valueDef, output.dataType)
			}
		}
	}
	
	@Check
	def checkAttributeValueReferenceDef(AttributeValueReferenceDef valueDef) {
		val parent = valueDef.findFirstParent(AttributeDef, ProcessArgumentDef)
		switch parent {
			
			ProcessArgumentDef: {
				switch param : parent.key {
					ComplexOutputPort: checkAttributeValueType(valueDef, param.dataType)
					ComplexInputDef: checkAttributeValueType(valueDef, param.type)
				}
			}
			
			AttributeDef: {
				val attribute = parent.key
				if (attribute instanceof ComplexAttribute) {
					checkAttributeValueType(valueDef, attribute.dataType)
				}
			}
		}
	}
	
	def checkAttributeValueType(AttributeValueDef valueDef, Type type) {
		val feature = switch valueDef {
			ProcessOutput: ObjectLangPackage.Literals.PROCESS_OUTPUT__PROCESS_DEF
			ObjectCreation: ObjectLangPackage.Literals.OBJECT_DEF__CREATION
			AttributeValueReferenceDef: ObjectLangPackage.Literals.ATTRIBUTE_VALUE_REFERENCE_DEF__VALUE
		}
		val isAbstract = switch valueDef {
			ObjectCreation: valueDef.isAbstract
			AttributeValueReferenceDef: valueDef.value.isAbstract
		}
		if (isAbstract) {
			error("Attribute value cannot be abstract", feature)
			return
		}
		val valueType = switch valueDef {
			ObjectCreation: valueDef.type
			AttributeValueReferenceDef: valueDef.value.type
		}
		if (!valueType?.isTypeOf(type)) {
			error("Attribute value is " + valueType?.name + " but should be " + type.name, feature)
			return
		}
	}
	
	@Check
	def checkComplexListAttributeValuesDef(ComplexListAttributeValuesDef valueDef) {
		val attrDef = valueDef.findFirstParent(AttributeDef)
		if (attrDef instanceof AttributeDef) {
			if (attrDef.key instanceof ComplexAttribute
					&& (attrDef.key as ComplexAttribute).isList) {
						
				val attribute = attrDef.key as ComplexAttribute
				valueDef.valuesDef.findFirst[
					!checkTypeMatches(attribute.dataType)
				]
			}
		}
		val argDef = valueDef.findFirstParent(ProcessArgumentDef)
		if (argDef instanceof ProcessArgumentDef) {
			if (argDef.key instanceof ComplexOutputPort
					&& (argDef.key as ComplexOutputPort).isList) {
				val outputPort = argDef.key as ComplexOutputPort
				valueDef.valuesDef.findFirst[
					!checkTypeMatches(outputPort.dataType)
				]
			}
		}
	}
	
	dispatch def checkTypeMatches(ObjectCreation valueDef, Type dataType) {
		val type = valueDef.type
		if (!type?.isTypeOf(dataType)) {
			error(
				"Value is " + type?.name + " but should be " + dataType?.name,
				valueDef,
				ObjectLangPackage.Literals.OBJECT_DEF__CREATION)
			return false
		}
		return true
	}
	
	dispatch def checkTypeMatches(AttributeValueReferenceDef valueDef, Type dataType) {
		val type = valueDef.value?.type
		if (!type?.isTypeOf(dataType)) {
			error(
				"Value is " + type?.name + " but should be " + dataType?.name,
				valueDef,
				ObjectLangPackage.Literals.ATTRIBUTE_VALUE_REFERENCE_DEF__VALUE)
			return false
		}
		return true
	}
	
	dispatch def checkTypeMatches(ProcessOutput processOutput, Type dataType) {
		val type = processOutput?.type
		if (!type?.isTypeOf(dataType)) {
			error(
				"Value is " + type?.name + " but should be " + dataType?.name,
				processOutput,
				ObjectLangPackage.Literals.PROCESS_OUTPUT__PROCESS_DEF)
			return false
		}
		return true
	}
	
	@Check
	def checkAttributeDef(AttributeDef attrdef) {
		val attribute = attrdef.key as Attribute
		if (attribute === null) {
			error("Attribute not found", ObjectLangPackage.Literals.ATTRIBUTE_DEF__KEY)
			return
		}
		
		val objDef = attrdef.eContainer
		if (objDef instanceof ObjectDef) {
			val names = objDef.attributes.map[key].filter(Attribute).map[name].toList
			val frequency = Collections.frequency(names, attribute.name)
			if (frequency > 1)
				error("Duplicate attribute", ObjectLangPackage.Literals.ATTRIBUTE_DEF__KEY)
		}
		
		val valueDef = attrdef.valueDef
		if (valueDef === null) {
			return
		}
		
		if (attribute instanceof ComplexAttribute) {
			if (attribute.isEnumAttribute) {
				val match = #[
					TextAttributeValueDef,
					TextListAttributeValueDef,
					EmptyList
				].exists[isInstance(valueDef)]
				if (!match) {
					error("  Attribute has EnumType", ObjectLangPackage.Literals.ATTRIBUTE_DEF__KEY)
					return
				}
			} else {
				val match = #[
					ObjectCreation,
					AttributeValueReferenceDef,
					ComplexListAttributeValuesDef,
					EmptyList
				].exists[isInstance(valueDef)]
				if (!match) {
					error("  Attribute is complex", ObjectLangPackage.Literals.ATTRIBUTE_DEF__KEY)
					return
				}
			}
			if (attribute.isList
				&& !(valueDef instanceof ComplexListAttributeValuesDef)
				&& !((valueDef instanceof TextListAttributeValueDef) && attribute.isEnumAttribute)
				&& !(valueDef instanceof EmptyList)) {
				return
			}
			if (!attribute.isList
				&& (valueDef instanceof ComplexListAttributeValuesDef
					|| valueDef instanceof EmptyList)) {
				error("Attribute is not list", ObjectLangPackage.Literals.ATTRIBUTE_DEF__KEY)
				return
			}
		}
		
		if (attribute instanceof PrimitiveAttribute) {
			if (attribute.isIsList
				&& !(valueDef instanceof PrimitiveListAttributeValueDef)
				&& !(valueDef instanceof EmptyList)) {
				error("Attribute is list. Define a list value [ ... ]", ObjectLangPackage.Literals.ATTRIBUTE_DEF__KEY)
				return
			}
			if (!attribute.isIsList
				&& (valueDef instanceof PrimitiveListAttributeValueDef
					|| valueDef instanceof EmptyList)) {
				error("Attribute is not list", ObjectLangPackage.Literals.ATTRIBUTE_DEF__KEY)
				return
			}
		}
	}
	
	@Check
	def checkProcessArgumentDef(ProcessArgumentDef argDef) {
		if (argDef.key === null) {
			error("Input not found", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
			return
		}
		val input = argDef.key
		val outDef = argDef.findFirstParent(ProcessOutput)
		if (outDef instanceof ProcessOutput) {
			val names = outDef.arguments.map[key].filter(Output).map[name].toList
			val inputName = switch it : input {
				Output: it.name
				InputDef: it.name
			}
			val frequency = Collections.frequency(names, inputName)
			if (frequency > 1)
				error("Duplicate argument", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
		}
		
		val valueDef = argDef.valueDef
		if (valueDef === null) {
			return
		}
		
		if (input instanceof ComplexOutputPort) {
			if (input.isEnumPort) {
				val match = #[
					TextAttributeValueDef,
					TextListAttributeValueDef,
					EmptyList
				].exists[isInstance(valueDef)]
				if (!match) {
					error("  Input has EnumType", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
					return
				}
			} else {
				val match = #[
					ObjectCreation,
					AttributeValueReferenceDef,
					ComplexListAttributeValuesDef,
					EmptyList
				].exists[isInstance(valueDef)]
				if (!match) {
					error("Input is complex", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
					return
				}
			}
			if (input.isList
				&& !(valueDef instanceof ComplexListAttributeValuesDef)
				&& !((valueDef instanceof TextListAttributeValueDef) && input.isEnumPort)
				&& !(valueDef instanceof EmptyList)) {
				error("Input is list. Define a list value [ ... ]", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
				return
			}
			if (!input.isList
				&& (valueDef instanceof ComplexListAttributeValuesDef
					|| valueDef instanceof EmptyList)) {
				error("Input is not list", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
				return
			}
		}
		
		if (input instanceof PrimitiveOutputPort) {
			if (input.isList
				&& !(valueDef instanceof PrimitiveListAttributeValueDef)
				&& !(valueDef instanceof EmptyList)) {
				error("Input is list. Define a list value [ ... ]", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
				return
			}
			if (!input.isList
				&& (valueDef instanceof PrimitiveListAttributeValueDef
					|| valueDef instanceof EmptyList)) {
				error("Input is not list", ObjectLangPackage.Literals.PROCESS_ARGUMENT_DEF__KEY)
				return
			}
		}
	}
	
	def getPrimitiveType(Attribute attr) {
		switch it : attr {
			PrimitiveAttribute: dataType
		}
	}
	
	def getPrimitiveType(OutputPort port) {
		switch it : port {
			PrimitiveOutputPort: dataType
		}
	}
	
	def getPrimitiveType(InputDef reqDef) {
		switch it : reqDef {
			PrimitiveAttribute: dataType
		}
	}
	
	dispatch def assertValueType(AttributeValueDef valueDef, ProcessArgumentDef argDef, PrimitiveType... dataTypes) {
		val output = argDef.key
		if (output instanceof PrimitiveOutputPort) {
			val reqType = output.dataType.mapOnDataType
			if (dataTypes.contains(reqType)) {
				return // all fine
			}
			buildValueTypeError(valueDef, reqType)
		}
	}
	
	dispatch def assertValueType(AttributeValueDef valueDef, AttributeDef attrDef, PrimitiveType... dataTypes) {
		val attribute = attrDef.key as Attribute
		val reqType = switch it : attribute {
			case isEnumAttribute: PrimitiveType.TEXT
			default: primitiveType
		}
		if (reqType === null) {
			new RuntimeException().printStackTrace
		}
		if (dataTypes.contains(reqType)) {
			return // all fine
		}
		buildValueTypeError(valueDef, reqType)
	}
	
	def buildValueTypeError(AttributeValueDef valueDef, PrimitiveType reqType) {
		val msg = switch reqType {
			case BOOLEAN: "Attribute value should be Boolean (true / false)"
			case INTEGER: "Attribute value should be Integer"
			case REAL: "Attribute value should be Real (e.g. 1.0)"
			case TEXT: "Attribute value should be Text"
			case TIMESTAMP: "Attribute value should be Timestamp Integer (e.g. 123456789)"
			case FILE: "Attribute value should be file path relative to the initFiles"
		}
		val feature = switch valueDef {
			BooleanAttributeValueDef: ObjectLangPackage.Literals.BOOLEAN_ATTRIBUTE_VALUE_DEF__VALUE
			IntegerAttributeValueDef: ObjectLangPackage.Literals.INTEGER_ATTRIBUTE_VALUE_DEF__VALUE
			RealAttributeValueDef: ObjectLangPackage.Literals.REAL_ATTRIBUTE_VALUE_DEF__VALUE
			TextAttributeValueDef: ObjectLangPackage.Literals.TEXT_ATTRIBUTE_VALUE_DEF__VALUE
		}
		error(msg, feature)
	}
	
	def assertValueListType(AttributeValueDef valueDef, PrimitiveType... dataTypes) {
		val container = valueDef.eContainer
		if (container instanceof AttributeDef) {
			val attribute = container.key as Attribute
			if (!attribute.isList) {
				val feature = switch valueDef {
					BooleanListAttributeValueDef: ObjectLangPackage.Literals.BOOLEAN_ATTRIBUTE_VALUE_DEF__VALUE
					IntegerListAttributeValueDef: ObjectLangPackage.Literals.INTEGER_ATTRIBUTE_VALUE_DEF__VALUE
					RealListAttributeValueDef: ObjectLangPackage.Literals.REAL_ATTRIBUTE_VALUE_DEF__VALUE
					TextListAttributeValueDef: ObjectLangPackage.Literals.TEXT_ATTRIBUTE_VALUE_DEF__VALUE
				}
				error("Attribute is not list", feature)
				return
			}
		}
		if (container instanceof ProcessArgumentDef) {
			val outputPort = container.key as OutputPort
			if (!outputPort.isList) {
				val feature = switch valueDef {
					BooleanListAttributeValueDef: ObjectLangPackage.Literals.BOOLEAN_ATTRIBUTE_VALUE_DEF__VALUE
					IntegerListAttributeValueDef: ObjectLangPackage.Literals.INTEGER_ATTRIBUTE_VALUE_DEF__VALUE
					RealListAttributeValueDef: ObjectLangPackage.Literals.REAL_ATTRIBUTE_VALUE_DEF__VALUE
					TextListAttributeValueDef: ObjectLangPackage.Literals.TEXT_ATTRIBUTE_VALUE_DEF__VALUE
				}
				error("Argument is not list", feature)
				return
			}
		}
		assertValueType(valueDef, valueDef.eContainer, dataTypes)
	}
	
}
