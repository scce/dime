/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
grammar info.scce.dime.objectlang.ObjectLang with org.eclipse.xtext.common.Terminals

import "http://www.eclipse.org/emf/2002/Ecore" as ecore

generate objectLang "http://www.scce.info/dime/objectlang/ObjectLang"

ObjectsModel:
	'name' name=FQN
	(
		generations+=GenerateDef |
		imports+=Import |
		inputs+=InputDef |
		objects+=ObjectDef
	)*
;

GenerateDef:
	'generate' '{'
		( ('file' genFile=STRING) |
		  ('modelId' modelId=StringOrID) )+
	 '}'
;

Import:
	'import' path=StringOrID 'as' name=ID
;

NamedObject:
	InputDef | ObjectDef
;

InputDef:
	ComplexInputDef | PrimitiveInputDef
;

ComplexInputDef:
	'input' name=ID ':' typeDef=TypeDef
;

PrimitiveInputDef:
	'input' name=ID ':' typeDef=PrimitiveTypeDef
;

ObjectDef:
	output=OutputDef?  (name=ID '=')? creation=ObjectCreation
;

OutputDef: {OutputDef}
	'output' ('(' alias=ID ')')?
;

ObjectCreation:
	(ObjectInstantiation | ProcessOutput)
	('{'
		attributes+=AttributeDef*
	 '}')?
;

ObjectInstantiation:
	ObjectNew | ObjectExtend
;

ObjectNew:
	(abstract?='abstract' | 'new') typeDef=TypeDef
;

ObjectExtend:
	abstract?='abstract'? 'extend' objSrc=[ObjectDef]
;

ProcessOutput:
	'output' outputDef=ProcessOutputDef?
	'of' processDef=ProcessDef
	('('
		arguments+=ProcessArgumentDef*
	 ')')?
;

ProcessArgumentDef:
	key=[ecore::EObject|StringOrID] '=' valueDef=AttributeValueDef
;

ProcessDef:
	processImport=[Import]
;

ProcessOutputDef:
	output=[ecore::EObject|StringOrID]
;

TypeDef:
	dataImport=[Import] '.' type=[ecore::EObject|StringOrID]
;

AttributeDef:
	key=[ecore::EObject|StringOrID] '=' valueDef=AttributeValueDef
;

AttributeValueDef:
	PrimitiveAttributeValueDef
	| PrimitiveListAttributeValueDef
	| ComplexListAttributeValuesDef
	| AttributeValueReferenceDef
	| ObjectCreation
;
	
PrimitiveAttributeValueDef:
	BooleanAttributeValueDef
	| RealAttributeValueDef
	| IntegerAttributeValueDef
	| TextAttributeValueDef
;

PrimitiveListAttributeValueDef: 
	BooleanListAttributeValueDef
	| RealListAttributeValueDef
	| IntegerListAttributeValueDef
	| TextListAttributeValueDef
	| EmptyList {EmptyList}
;

BooleanAttributeValueDef:
	value=BOOLEAN
;

BooleanListAttributeValueDef: {BooleanListAttributeValueDef}
	'[' values+=BOOLEAN (',' values+=BOOLEAN)* ']'
;

RealAttributeValueDef:
	value=DoubleValue
;

RealListAttributeValueDef: {RealListAttributeValueDef}
	'[' values+=DoubleValue (',' values+=DoubleValue)* ']'
;

IntegerAttributeValueDef:
	value=LongValue
;

IntegerListAttributeValueDef: {IntegerListAttributeValueDef}
	'[' values+=LongValue (',' values+=LongValue)* ']'
;

TextAttributeValueDef:
	hashed?='hashed'? value=STRING
;

TextListAttributeValueDef: {TextListAttributeValueDef}
	hashed?='hashed'? '[' values+=STRING (',' values+=STRING)* ']'
;

AttributeValueReferenceDef:
	value=[NamedObject]
;

ComplexListAttributeValuesDef: {ComplexListAttributeValuesDef}
	 '[' valuesDef+=(ObjectCreation | AttributeValueReferenceDef)
	(',' valuesDef+=(ObjectCreation | AttributeValueReferenceDef))* ']'
;

EmptyList: 
	'[' ']'
;

StringOrID:
	STRING | ID
;

FQN:
	ID ('.' ID)*
;

DoubleValue returns ecore::EDouble:
	LongValue '.' INT
;

LongValue returns ecore::ELong: 
	SIGN? INT
;

enum PrimitiveTypeDef:
	NONE | Boolean="bool" | File="file" | Integer="integer" | Real="real" | Text="text" | Timestamp="timestamp"
;

terminal SIGN: '+' | '-' ;

terminal BOOLEAN returns ecore::EBoolean:
	'true' | 'false'
;
