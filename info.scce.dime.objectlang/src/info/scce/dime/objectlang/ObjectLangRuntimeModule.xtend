/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.objectlang

import info.scce.dime.objectlang.generator.IObjectLangGenerator
import info.scce.dime.objectlang.generator.ObjectLangGenerator
import info.scce.dime.objectlang.objectLang.ObjectCreation
import info.scce.dime.objectlang.objectLang.ProcessDef
import info.scce.dime.objectlang.objectLang.ProcessOutput
import info.scce.dime.objectlang.objectLang.TypeDef
import org.eclipse.emf.common.util.AbstractTreeIterator
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.linking.impl.Linker

/**
 * Registers components to be used at runtime / without the Equinox extension registry.
 * 
 * @author Steve Bosselmann
 */
class ObjectLangRuntimeModule extends AbstractObjectLangRuntimeModule {
	
	def Class<? extends IObjectLangGenerator> bindIObjectLangGenerator () {
	    return ObjectLangGenerator
	}
	
    override bindIValueConverterService() {
        ObjectLangValueConverterService
    }

	override bindILinker() {
		ObjectLangLinker
	}

	static class ObjectLangLinker extends Linker {
		
		override protected getAllLinkableContents(EObject model) {
			new AbstractTreeIterator<EObject>(model) {
				override protected getChildren(Object object) {
					// assert that TypeDefs are linked before other children
					switch it : object {
						ProcessOutput: (eContents.filter(ProcessDef) + eContents.filter[it instanceof ProcessDef == false]).iterator
						ObjectCreation: (eContents.filter(TypeDef) + eContents.filter[it instanceof TypeDef == false]).iterator
						EObject: eContents.iterator
					}
				}
			}
		}
		
		override isClearAllReferencesRequired(Resource resource) {
			false
		}
	}
}
