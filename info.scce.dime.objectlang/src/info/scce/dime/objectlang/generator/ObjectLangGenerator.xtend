/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.objectlang.generator

import de.jabc.cinco.meta.runtime.xapi.NIOExtension
import info.scce.dime.objectlang.objectLang.ObjectsModel
import java.nio.file.Path
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

import static extension info.scce.dime.objectlang.generator.ObjectLangProcessGenerator.generateProcess

/**
 * Generates an object creation process from an ObjectsModel on save.
 * 
 * @author Steve Bosselmann
 */
class ObjectLangGenerator extends AbstractGenerator implements IObjectLangGenerator {

	extension NIOExtension = new NIOExtension
	
	override void doGenerate(ResourceSet resSet, IFileSystemAccess2 fsa, IGeneratorContext context) {
		resSet.resources.forEach[doGenerate(fsa, context)]
	}
	
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		val model = resource.allContents.filter(ObjectsModel).head
		if (model === null) {
			return
		}
		
		val fullFileName = resource.toNIOPath.fileNameWithoutExtension
		if (model.generations.nullOrEmpty) {
			val fileName = "Create" + fullFileName
			val outlet = model.toNIOPath.parent.resolve("model-gen")
			model.generateProcess(outlet, fileName, null)
		}
		else {
			val fileNames = newHashSet
			for (gendef: model.generations) {
				var String fileName
				var Path outlet
				if (gendef.genFile.nullOrEmpty) {
					var baseFileName = fileName = "Create" + fullFileName
					var index = 0
					while (fileNames.contains(fileName)) {
						fileName = baseFileName + index
						index += 1
					}
					/*
					 * Workaround for dealing with imported projects, where
					 * the workspace path differs from the project path.
					 * https://gitlab.com/scce/dime/-/issues/1008
					 */
					val tmpOutlet = model.toNIOPath.parent.resolve("model-gen")
					/*
					 * tmpOutlet:         /Users/work/git/app/dime-models/test/model-gen
					 * workspaceRootPath: /Users/work/workspace/app
					 * model.projectPath: /Users/work/git/app
					 * projRelPath:       app/dime-models/test/model-gen
					 * outlet:            /Users/work/workspace/app/dime-models/test/model-gen
					 */
					val projRelPath = tmpOutlet.subpath(model.projectPath.length - 1, tmpOutlet.length)
					outlet = workspaceRootPath.resolve(projRelPath)
				}
				else {
					/*
					 * Workaround for dealing with imported projects, where
					 * the workspace path differs from the project path.
					 * https://gitlab.com/scce/dime/-/issues/1008
					 */
					val projectPath = model.projectPath
					val tmpOutlet = projectPath.resolve(gendef.genFile)
					val projRelPath = tmpOutlet.subpath(model.projectPath.length - 1, tmpOutlet.length)
					outlet = workspaceRootPath.resolve(projRelPath)
					fileName = tmpOutlet.fileName.toString
				}
				fileNames.add(fileName)
				model.generateProcess(outlet, fileName, gendef.modelId)
			}
		}
		
	}
	
}
