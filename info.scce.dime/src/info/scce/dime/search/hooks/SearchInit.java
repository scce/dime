/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.search.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.search.search.Search;

public class SearchInit extends DIMEPostCreateHook<Search> {

	@Override
	public void postCreate(Search searchModel) {
		try {

			String fileName = searchModel.eResource().getURI().lastSegment();
			String fileExtension = searchModel.eResource().getURI()
					.fileExtension();
			String modelName = fileName.replace("." + fileExtension, "");

			searchModel.newDataContext(50, 50);
			searchModel.newSearchInterface(350, 50);
			searchModel.setModelName(modelName);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
