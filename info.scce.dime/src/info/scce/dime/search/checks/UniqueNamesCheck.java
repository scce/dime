/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.search.checks;

import java.util.ArrayList;

import info.scce.dime.search.mcam.adapter.SearchAdapter;
import info.scce.dime.search.mcam.adapter.SearchId;
import info.scce.dime.search.search.InputParameter;
import info.scce.dime.search.search.Variable;
import info.scce.dime.checks.AbstractCheck;

public class UniqueNamesCheck extends AbstractCheck<SearchId, SearchAdapter> {

	@Override
	public void doExecute(SearchAdapter adapter) {
		ArrayList<String> varNames = new ArrayList<>();
		ArrayList<String> inputNames = new ArrayList<>();
		
		for (SearchId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Variable) {
				Variable var = (Variable) obj;
				if (varNames.contains(var.getName()))
					addError(id, var.getName() + " not unique");
				varNames.add(var.getName());
			}
			if (obj instanceof InputParameter) {
				InputParameter ip = (InputParameter) obj;
				if (inputNames.contains(ip.getName()))
					addError(id, ip.getName() + " not unique");
				inputNames.add(ip.getName());
			}
		}
	}
	
	@Override
	public void init() {}

}
