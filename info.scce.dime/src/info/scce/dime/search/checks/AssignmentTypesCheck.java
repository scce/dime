/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.search.checks;

import graphmodel.Node;
import info.scce.dime.search.mcam.adapter.SearchAdapter;
import info.scce.dime.search.mcam.adapter.SearchId;
import info.scce.dime.search.search.CompareOperator;
import info.scce.dime.search.search.DataEdge;
import info.scce.dime.search.search.PrimitiveInputParameter;
import info.scce.dime.search.search.PrimitiveType;
import info.scce.dime.search.search.PrimitiveVariable;
import info.scce.dime.checks.AbstractCheck;

public class AssignmentTypesCheck extends AbstractCheck<SearchId, SearchAdapter> {

	@Override
	public void doExecute(SearchAdapter adapter) {
		for (SearchId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof DataEdge) {
				DataEdge dEdge = (DataEdge) obj;

				PrimitiveType srcType = getPrimitiveType(dEdge
						.getSourceElement());
				PrimitiveType tgtType = getPrimitiveType(dEdge
						.getTargetElement());

				if (srcType != null && tgtType != null) {
					if (!srcType.equals(tgtType)) {
						addError(id, "type mismatch! " + srcType + " != "
								+ tgtType);
					}
				}
			}
		}
	}
	
	@Override
	public void init() {}

	private PrimitiveType getPrimitiveType(Node node) {
		if (node instanceof PrimitiveInputParameter)
			return ((PrimitiveInputParameter) node).getDataType();
		if (node instanceof PrimitiveVariable)
			return ((PrimitiveVariable) node).getDataType();
		if (node instanceof CompareOperator)
			return ((CompareOperator) node).getOperand();
		return null;
	}
}
