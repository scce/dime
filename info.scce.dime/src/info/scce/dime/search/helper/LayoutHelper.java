/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.search.helper;

import info.scce.dime.search.search.InputParameter;
import info.scce.dime.search.search.OutputParameter;
import info.scce.dime.search.search.Parameter;
import info.scce.dime.search.search.SearchInterface;

public class LayoutHelper {

	/*
	 * CONSTANTS FOR Search MODEL LAYOUT
	 */

	public static final int QI_FIRST_PORT_Y = 67;

	public static final int PORT_X = 5;
	public static final int PORT_SPACE = 18;
	
	public static final int CQ_Y = 25;

	/*
	 * Helper Functions
	 */
	public static int getSearchInterfaceHeight(int parameterAmount) {
		return (65 + parameterAmount * PORT_SPACE + (parameterAmount > 0 ? 7
				: 0));
	}

	public static void resize(SearchInterface cQI,
			int parameterAmount) {
		cQI.resize(cQI.getWidth(), getSearchInterfaceHeight(parameterAmount < 0 ? 0 : parameterAmount));
	}
	
	public static void layout(SearchInterface cQI, Parameter skipParameter) {
		int x = LayoutHelper.PORT_X;
		int y = LayoutHelper.QI_FIRST_PORT_Y;

		if (cQI.getOutputParameters().size() > 0) {
			OutputParameter cOP = cQI.getOutputParameters().get(0);
			if (cOP != skipParameter) {
				cOP.moveTo(cQI, x, y);
				cOP.resize(cQI.getWidth() - 2 * LayoutHelper.PORT_X,
						cOP.getHeight());
				y += LayoutHelper.PORT_SPACE;
			}
			
		}

		for (InputParameter cIP : cQI.getInputParameters()) {
			if (cIP == skipParameter)
				continue;
			
			cIP.moveTo(cQI, x, y);
			cIP.resize(cQI.getWidth() - 2 * LayoutHelper.PORT_X,
					cIP.getHeight());
			y += LayoutHelper.PORT_SPACE;
		}
	}

}
