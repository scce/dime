/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.checks;

import graphmodel.GraphModel;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter;
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId;

public class AbstractModelIntegrityCheck<E extends _CincoId, M extends _CincoAdapter<E, ? extends GraphModel>> extends AbstractCheck<E,M> {
	
	@Override
	public void doExecute(M adapter) {
		Resource res = null;
		try {
			res = adapter.getModel().eResource();
		} catch(Exception e) {
			fail(adapter, "failed to retrieve resource");
			return;
		}
		if (res == null) {
			fail(adapter, "failed to retrieve resource");
			return;
		}
		if (res instanceof XtextResource) {
			if (((XtextResource) res).getParseResult().hasSyntaxErrors()) {
				fail(adapter, "model has syntax errors");
				return;
			}
		}
	}
	
	private void fail(M adapter, String msg) {
		addError(adapter.getIdByString(adapter.getModel().getId()), msg);
		processResults();
	}
	
	@Override
	public void init() { /* nothing to do */ }


}
