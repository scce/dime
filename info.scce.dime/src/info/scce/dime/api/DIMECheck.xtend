/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.api

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import graphmodel.GraphModel
import info.scce.dime.checks.AbstractCheck
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension

abstract class DIMECheck<E extends _CincoId, M extends _CincoAdapter<E, ? extends GraphModel>> extends AbstractCheck<E, M> {
	
	protected extension GraphModelExtension = new GraphModelExtension
	protected extension DataExtension = DataExtension.instance
	
	
	/**
	 * Always reinitialize extensions to avoid caching problems. This method is final. If you want to add own
	 * behavior to be executed before every check, implement #beforeCheck 
	 */
	override final init() {
		_graphModelExtension = new GraphModelExtension	
		_dataExtension = DataExtension.instance
		beforeCheck()	
	}
	
	def void beforeCheck() {
		// does nothing by default
	}
	
	
}
