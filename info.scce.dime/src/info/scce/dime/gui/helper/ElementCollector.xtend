/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.helper

import graphmodel.Node

/**
 * Utility methods to collect model elements of a certain type
 * respecting their generation order as well as their vertical
 * and horizontal position.
 */
 // TODO Delete as soon as the calling classes have been translated to Xtend classes. It is obsolete, as all necessary extension methods moved to GUIExtension.
class ElementCollector {
	
	/**
	 * Sorts a list of nodes according to their horizontal position.
	 * 
	 * @param elements
	 * @return
	 */
	// TODO Delete because calling 'sortBy[x]' is shorter and more descriptive than calling 'getElementsH'
	static def <T extends Node> getElementsH(Iterable<T> elements) {
		elements.sortBy[x]
	}
	
	/**
	 * Sorts a list of nodes according to their vertical position.
	 * 
	 * @param elements
	 * @return
	 */
	 // TODO Delete because calling 'sortBy[y]' is shorter and more descriptive than calling 'getElementsV'
	static def <T extends Node> getElementsV(Iterable<T> elements) {
		elements.sortBy[y]
	}
	
}
