/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.helper

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.core.utils.registry.NonEmptyRegistry
import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import graphmodel.Container
import graphmodel.GraphModel
import graphmodel.IdentifiableElement
import graphmodel.ModelElement
import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.data.helper.JavaIdentifierUtils
import info.scce.dime.gUIPlugin.ComplexInputParameter
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonOptions
import info.scce.dime.gui.gui.ButtonToolbar
import info.scce.dime.gui.gui.ButtonToolbarType
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexListAttribute
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.IS
import info.scce.dime.gui.gui.Input
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.InputType
import info.scce.dime.gui.gui.Iteration
import info.scce.dime.gui.gui.LinkSIB
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.PrimitiveListAttribute
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.TableLoad
import info.scce.dime.gui.gui.Variable
import info.scce.dime.process.process.AtomicSIB
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.RetrieveCurrentUserSIB
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.siblibrary.SIB
import java.io.File
import java.util.Collection
import java.util.HashMap
import java.util.LinkedList
import java.util.List
import java.util.Map
import java.util.Set

import static extension info.scce.dime.gui.helper.GUIBranch.*
import static extension info.scce.dime.gui.helper.GUIBranchPort.*
import info.scce.dime.process.process.RemoveFromListSIB
import info.scce.dime.process.process.RetrieveOfTypeSIB

class GUIExtension extends DIMEGraphModelExtension {
	
	static extension WorkspaceExtension = new WorkspaceExtension
	
	protected extension DataExtension = DataExtension.instance
	protected extension ResourceExtension = new ResourceExtension
	protected extension FileExtension = new FileExtension
	
	boolean cacheValues = false
	
	// Caches
	val _modelElements = new NonEmptyRegistry<ModelElementContainer, Map<Class<? extends IdentifiableElement>, List<? extends IdentifiableElement>>> [ newHashMap ]
	val _firstParents = new NonEmptyRegistry<ModelElement, Map<Class<?>, ModelElement>> [ newHashMap ]
	Map<GUI,Set<GUI>> _subGUIs
	Map<GUI,List<GUI>> _superGUIs
	
	val _branchElements = <ModelElementContainer, Collection<GUIBranch>> newHashMap
	val _processBranchElements = <ModelElementContainer, Collection<GUIBranch>> newHashMap
	val _embeddedProcesses = <ModelElementContainer, Set<Process>> newHashMap
	val _surroundingLoops = <ModelElement, Node> newHashMap
	val _formInputs = <Container, List<Input>> newHashMap
	
	val (IdentifiableElement) => ModelElementContainer digIntoGUISIBs = [
		switch it { GUISIB: gui }
	]
	
	val (IdentifiableElement) => ModelElementContainer digIntoNonExtendingGUISIBs = [
		//switch it { GUISIB case !hasParent(ExtensionContext): gui }
		switch it { GUISIB case !hasParent(ExtensionContext): it }
	]
		
	static def lastModified2(GUIPlugin g) {
		new File(g.file.locationURI).lastModified
	}
	
	static def lastModified2(GraphModel g) {
		new File(g.file.locationURI).lastModified
	}
	
	def graphModelFileName(GraphModel g) {
		g.file.locationURI.toString
	}
	
	static def lastModified2(Iterable<GraphModel> gs) {
		gs.map[lastModified2].max
	}
	
	new() {
		this(/*cacheValues*/false)
	}
	
	new(boolean cacheValues) {
		this.cacheValues = true
	}
	
	def dataExtension() {
		_dataExtension
	}

	
	/**
	 * Maps a primitive type (i.e. the enum value) of native ui components to 
	 * primitive types of data models.
	 * 
	 * @param it the primitive type enum value of gui.
	 * @return the corresponding primitive type enum value of data. 
	 */
	def toData(info.scce.dime.gUIPlugin.PrimitiveType it) {
		switch it {
			case info.scce.dime.gUIPlugin.PrimitiveType.BOOLEAN: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			case info.scce.dime.gUIPlugin.PrimitiveType.INTEGER: info.scce.dime.data.data.PrimitiveType.INTEGER
			case info.scce.dime.gUIPlugin.PrimitiveType.REAL: info.scce.dime.data.data.PrimitiveType.REAL
			case info.scce.dime.gUIPlugin.PrimitiveType.TEXT: info.scce.dime.data.data.PrimitiveType.TEXT
			case info.scce.dime.gUIPlugin.PrimitiveType.TIMESTAMP: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}

	/**
	 * Maps a primitive type (i.e. the enum value) of native ui components to 
	 * primitive types of data models.
	 * 
	 * @param it the primitive type enum value of gui.
	 * @return the corresponding primitive type enum value of data. 
	 */
	def toGUI(info.scce.dime.data.data.PrimitiveType it) {
		switch it {
			case info.scce.dime.data.data.PrimitiveType.BOOLEAN: info.scce.dime.gui.gui.PrimitiveType.BOOLEAN
			case info.scce.dime.data.data.PrimitiveType.INTEGER: info.scce.dime.gui.gui.PrimitiveType.INTEGER
			case info.scce.dime.data.data.PrimitiveType.REAL: info.scce.dime.gui.gui.PrimitiveType.REAL
			case info.scce.dime.data.data.PrimitiveType.TEXT: info.scce.dime.gui.gui.PrimitiveType.TEXT
			case info.scce.dime.data.data.PrimitiveType.TIMESTAMP: info.scce.dime.gui.gui.PrimitiveType.TIMESTAMP
			case info.scce.dime.data.data.PrimitiveType.FILE: info.scce.dime.gui.gui.PrimitiveType.FILE
		}
	}

	
	/**
	 * Maps a primitive type (i.e. the enum value) of data models to 
	 * primitive types of process models.
	 * 
	 * @param it the primitive type enum value of data.
	 * @return the corresponding primitive type enum value of process. 
	 */
	def toProcess(info.scce.dime.data.data.PrimitiveType it) {
		switch it {
			case BOOLEAN: PrimitiveType.BOOLEAN
			case FILE: PrimitiveType.FILE
			case INTEGER: PrimitiveType.INTEGER
			case REAL: PrimitiveType.REAL
			case TEXT: PrimitiveType.TEXT
			case TIMESTAMP: PrimitiveType.TIMESTAMP
		}
	}

	def dispatch String defaultValue(info.scce.dime.gui.gui.PrimitiveType pt) {
		pt.toData.defaultValue
	}
	def dispatch String defaultValue(info.scce.dime.data.data.PrimitiveType pt) {
		switch pt {
			case BOOLEAN: return "false"
			case FILE: return "null"
			case INTEGER: return "0"
			case REAL: return "0.0"
			case TEXT: return "''"
			case TIMESTAMP: return "null"
		}
	}
	
	def primitiveType(info.scce.dime.data.data.PrimitiveType pt) {
		switch pt {
			case BOOLEAN: return "bool"
			case FILE: return "FileReference"
			case INTEGER: return "int"
			case REAL: return "double"
			case TEXT: return "String"
			case TIMESTAMP: return "DateTime"
		}
	}
	
	def boolean getIsDateTime(Field field) {
		return field.inputType == InputType.DATE ||
		field.inputType == InputType.DATETIME ||
		field.inputType == InputType.MONTH ||
		field.inputType == InputType.TIME ||
		field.inputType == InputType.WEEK
	}
	
	def boolean getIsFile(Field field) {
		return field.inputType == InputType.SIMPLE_FILE ||
		field.inputType == InputType.ADVANCED_FILE
	}
	
	// FIXME this method is GUI generator specific, move it there
	def defaultValue(info.scce.dime.data.data.PrimitiveType pt,boolean list) {
		if(list) "new DIMEList()" else pt.defaultValue
		
	}
	
	// FIXME this method is GUI generator specific, move it there
	def dispatch String defaultValue(PrimitiveVariable pv) {
		if(pv.isIsList) return '''new DIMEList()'''
		pv.dataType.toData.defaultValue
	}
	
	// FIXME this method is GUI generator specific, move it there
	def dispatch String defaultValue(info.scce.dime.process.process.PrimitiveInputPort pv) {
		if(pv.isIsList) return '''const []'''
		pv.dataType.toData.defaultValue
	}
	
	// FIXME this method is GUI generator specific, move it there
	def dispatch String defaultValue(info.scce.dime.process.process.ComplexInputPort pv) {
		if(pv.isIsList) {
			val data = '''«JavaIdentifierUtils.escapeDart(pv.dataType.rootElement.modelName)»'''
			return '''«data».«data»CastUtil.newList«JavaIdentifierUtils.escapeDart(pv.dataType.name).toFirstUpper»()'''
		}
		'''null'''
	}
	
	// FIXME this method is GUI generator specific, move it there
	def variableListStatus(CharSequence s, Variable v)'''«IF v.isIsList»DIMEList<«ENDIF»«s»«IF v.isIsList»>«ENDIF»'''

	// FIXME this method is GUI generator specific, move it there
	def variableType(PrimitiveVariable v) {
		v.dataType.toData.primitiveType.variableListStatus(v)
	}

	// FIXME this method is GUI generator specific, move it there
	def dispatch String defaultValue(ComplexVariable pt) {
		if(pt.isIsList) return '''const []'''
		return '''null'''
	}
	
	// FIXME this method is GUI generator specific, move it there
	def dispatch String defaultValue(info.scce.dime.gui.gui.InputStatic input) {
		input.staticValue
	}
	
	// FIXME this method is GUI generator specific, move it there
	def String staticValue(info.scce.dime.gui.gui.InputStatic input) {
		switch(input){
				info.scce.dime.gui.gui.TextInputStatic: { if (input?.value === null) { System.err.println("Value is null for input: " + input) }; return "'"+input.value.replaceAll("\\{\\{","'+").replaceAll("\\}\\}",".toString()+'")+"'" }
				info.scce.dime.gui.gui.RealInputStatic: return input.value.toString
				info.scce.dime.gui.gui.IntegerInputStatic: return input.value.toString
				info.scce.dime.gui.gui.BooleanInputStatic: return input.value.toString
				info.scce.dime.gui.gui.TimestampInputStatic: return '''staticDate«JavaIdentifierUtils.escapeDart(input.id)»'''.toString
			}
		return "''";
	}
	
	// FIXME this method is GUI generator specific, move it there
	def String staticValue(InputStatic input) {
		switch(input){
				TextInputStatic: return "'"+input.value+"'"
				RealInputStatic: return input.value.toString
				IntegerInputStatic: return input.value.toString
				BooleanInputStatic: return input.value.toString
				TimestampInputStatic: return '''getStaticDate(«input.value.toString»000)'''.toString
			}
		return "''";
	}
	
	// FIXME this method is GUI generator specific, move it there
	def dispatch String defaultValue(IO input)
	{
		if(input instanceof ComplexInputPort){
			
			if(input.isIsList) {
				val data = '''«JavaIdentifierUtils.escapeDart(input.dataType.rootElement.modelName)»'''
				return '''«data».«data»CastUtil.newList«JavaIdentifierUtils.escapeDart(input.dataType.name).toFirstUpper»()'''
			}
			return "null";
		}
		if(input instanceof info.scce.dime.gui.gui.InputStatic) {
			return input.staticValue
		}
		switch((input as PrimitiveInputPort).dataType){
			case BOOLEAN: return "false"
			case FILE: return "null"
			case INTEGER: return "0"
			case REAL: return "0.0"
			case TEXT: return "''"
			case TIMESTAMP: return "null"
		}
	}
	
	// FIXME this method is GUI generator specific, move it there
	def dispatch String defaultValue(InputStatic input)
	{
		input.staticValue
	}
	

	/**
	 * Checks whether the type of the source node (i.e., a {@link Variable} or {@link Attribute}) 
	 * matches (i.e., is a sub type) the type of the target element (i.e., an {@link InputPort}). If not,
	 * it checks whether all {@link IS}-edges pointing to any containing element on the path to the 
	 * root (starting from the target element itself) match the type of the target element. The latter check
	 * can be seen as a smart cast for {@link IS}-edges.
	 * 
	 * @param source a pair associating the source node to its type. The source node has type {@link Node}, 
	 * 		since this is the lowest common supert type of {@link Variable} or {@link Attribute}.
	 * @param target a pair associating the target element to its type.
	 * @return whether the type of the source node matches the type of the target element respecting 
	 * 		smart cast capabilities of {@link IS}-edges.
	 */
	def boolean isSubTypeWithSmartCast(Pair<Node, Type> source, Pair<InputPort, Type> target) {
		val sourceNode = source.key
		val sourceType = source.value

		val targetElement = target.key
		val targetType = target.value

		if (!sourceType.isTypeOf(targetType)) {
			val pathToRoot = targetElement.pathToRoot.toSet
			val matchingISEdges = sourceNode.getOutgoing(IS)// respect is-edges pointing at the guisib itself or a containing parent element, only. 
			.filter[isEdge|pathToRoot.contains(isEdge.targetElement)].toSet
			!(matchingISEdges.isEmpty() || !matchingISEdges// check whether all is-edges match the type constraint 
			.forall [ isEdge |
				val matchingType = isEdge.type
				if (!isEdge.isNegate()) {
					if(isEdge.isExactMatch()) matchingType == targetType else matchingType.isTypeOf(targetType)
				} else {
					if(isEdge.isExactMatch()) !(matchingType == targetType) else !matchingType.isTypeOf(targetType)
				}
			])
		} else
			true
	}
	

	/**
	 * Retrieves the type of an {@link IS}-edge.
	 * 
	 * @param the {@link IS}-edge.
	 * @return the type of the {@link IS}-edge.
	 */
	def Type getType(IS isEdge) {
		val it = isEdge.sourceElement
		if (it instanceof ComplexVariable) {
			dataType.rootElement.types.findFirst[id == isEdge.matchingType];
		} else if (it instanceof ComplexAttribute) {
			attribute.rootElement.types.findFirst[id == isEdge.matchingType]
		} else
			throw new IllegalStateException(
				"IS-edge not starting from a complex variable or " + "attribute, but '" + class.simpleName + "'.");
	}

	/**
	 * Converts the GUI plug in primitive types to GUI model primitive types
	 * @param pt
	 * @return
	 */
	def info.scce.dime.gui.gui.PrimitiveType toPrimitiveType(info.scce.dime.gUIPlugin.PrimitiveType pt) {
		return switch (pt) {
			case BOOLEAN: info.scce.dime.gui.gui.PrimitiveType.BOOLEAN
			case INTEGER: info.scce.dime.gui.gui.PrimitiveType.INTEGER
			case TEXT: info.scce.dime.gui.gui.PrimitiveType.TEXT
			case REAL: info.scce.dime.gui.gui.PrimitiveType.REAL
			case TIMESTAMP: info.scce.dime.gui.gui.PrimitiveType.TIMESTAMP
		}
	}

	/**
	 * Retrieves the data type that is referenced by the specified complex parameter
	 * of a GUI plugin.
	 * 
	 * @param p  The complex parameter that references the data type to be retrieved.
	 * @return  The Type that is referenced by the specified complex parameter, or
	 *   {@code null} if not existent or the data model does not exist or cannot be
	 *   accessed for whatever reason.
	 */
	def Type toComplexType(ComplexOutputPort p) {
		p.dataType
	}
	
	/**
	 * Checks if the data node is primitive
	 * 
	 * @param node  A node from a data context of a GUI or Process model
	 * @return if the given node is a primitive variable or attribute
	 */
	def boolean isPrimitve(Node node) {
		if(node instanceof PrimitiveAttribute)return true
		if(node instanceof info.scce.dime.process.process.PrimitiveAttribute)return true
		if(node instanceof PrimitiveVariable)return true
		if(node instanceof info.scce.dime.process.process.PrimitiveVariable)return true
		if(node instanceof info.scce.dime.process.process.PrimitiveOutputPort)return true
		if(node instanceof info.scce.dime.process.process.PrimitiveInputPort)return true
		if(node instanceof PrimitiveOutputPort)return true
		if(node instanceof PrimitiveInputPort)return true
		false
	}
	
	/**
	 * Returns the primitive data type or null
	 * 
	 * @param node  A node from a data context of a GUI or Process model
	 * @return data type or null
	 */
	def info.scce.dime.data.data.PrimitiveType toPrimitve(Node node) {
		switch it:node {
			PrimitiveAttribute: attribute.dataType
			PrimitiveListAttribute: getPrimitiveType.toData
			PrimitiveVariable: dataType.toData
			PrimitiveInputPort: dataType.toData
			PrimitiveOutputPort: dataType.toData
			
			info.scce.dime.process.process.PrimitiveAttribute: attribute.dataType
			info.scce.dime.process.process.PrimitiveVariable: dataType.toData
		}
	}

	/**
	 * Returns the complex data type or null
	 * 
	 * @param node  A node from a data context of a GUI or Process model
	 * @return data type or null
	 */
	def Type toComplex(Node it) {
		switch it {
			ComplexAttribute: attribute.dataType
			ComplexListAttribute: listType
			ComplexVariable: dataType
			ComplexInputPort: dataType
			ComplexOutputPort: dataType
			
			info.scce.dime.process.process.ComplexAttribute: attribute.dataType
			info.scce.dime.process.process.ComplexVariable: dataType
		}
	}
	
	/**
	 * Retrieves the data type that is referenced by the specified complex parameter
	 * of a GUI plugin.
	 * 
	 * @param p  The complex parameter that references the data type to be retrieved.
	 * @return  The Type that is referenced by the specified complex parameter, or
	 *   {@code null} if not existent or the data model does not exist or cannot be
	 *   accessed for whatever reason.
	 */
	def Type toComplexType(ComplexParameter p) {
		p.type as Type
	}
	

	/**
	 * Retrieves the data type that is referenced by the specified complex parameter
	 * of a GUI plugin.
	 * 
	 * @param p  The complex parameter that references the data type to be retrieved.
	 * @return  The Type that is referenced by the specified complex parameter, or
	 *   {@code null} if not existent or the data model does not exist or cannot be
	 *   accessed for whatever reason.
	 */
	def Type toComplexType(ComplexInputParameter p) {
		p.parameter.toComplexType
	}

	/**
	 * Determines whether the specified button has the specified type.
	 * 
	 * @param button
	 * @return {@code true} if this button is part of a {@code ButtonGroup} which
	 *   itself is part of a {@code ButtonToolbar} that has the role {@code type}.
	 */
	def boolean hasType(Button button, ButtonToolbarType type) {
		button.findFirstParent(ButtonToolbar)?.role == type
	}

	/**
	 * Collects all input variables present in the given GUI model
	 */
	def Iterable<Variable> inputVariables(GUI gui) {
		gui.topLevelVariables.filter[isIsInput]
	}
	
	/**
	 * Collects all input variables present in the given GUI model
	 */
	def Iterable<Variable> topLevelVariables(GUI gui) {
		gui.dataContexts.map[variables].flatten.filter [
			getIncoming(ComplexAttributeConnector).empty && getIncoming(ComplexListAttributeConnector).empty
		]
	}

	def List<GUIBranchPort> equallyNamedPorts(List<GUIBranchPort> entry) {
		val ports = new LinkedList
		val knownPorts = new LinkedList
		entry.forEach [n|
			{
				if (knownPorts.contains(n.name)) {
					ports.add(n)
				} else {
					knownPorts.add(n.name)
				}
				knownPorts.add(n)
			}
		]
		return ports
	}

	def List<GUIBranchPort> notEquallyNamedPorts(Iterable<GUIBranchPort> ports) {
		ports.groupBy[name].entrySet.map[value.get(0)].toList
	}
	
	def getGUIBranchesMerged(ModelElementContainer container) {
		getGUIBranches(container,true).groupBy[it.name].mapValues[
			// FIXME Find a better solution - the current one exists because the previous implementation implicitly preferred GUI over Process
			it.reduce[ x,y | if (x.hasGUIOrigin) x.mergedWith(y) else y.mergedWith(x) ]
		].values
	}

	/**
	 * Collects model elements that imply a GUI branch.
	 * These objects can be Buttons, GUIPlugin outputs or EndSIBs of embedded processes.
	 * Searches deeply, i.e. recursively digs down into GUIs that are embedded via GUISIBs.
	 */
	def List<GUIBranch> getGUIBranches(ModelElementContainer container,boolean merge) {
		if (cacheValues) {
			var branches = _branchElements.get(container)
			if (branches === null) {
				branches = calcGUIBranches(container, newHashSet, GUIBranch.ALL_BRANCH_ELEMENT_TYPES, merge)
				_branchElements.put(container, branches)
			}
			
//			val modelName = if (container instanceof GUI) container.title else container.eClass.name
//			val alternative = new GUIExtensionOld().branchElements(container)
//			for (ours : branches) {
//				val theirs = branches.findFirst[it.name == ours.name]
//				if (theirs === null) {
//					System.err.println(modelName + "| Additional Branch " + theirs.name)
//				}
//			}
//			for (theirs : alternative) {
//				val ours = branches.findFirst[it.name == theirs.name]
//				if (ours === null) {
//					System.err.println(modelName + "| Branch " + theirs.name + " is missing!")
//				} else {
//					for (ourPort : ours.ports) {
//						val theirPort = theirs.ports.findFirst[it.name == ourPort.name]
//						if (theirPort === null) {
//							System.err.println(modelName + "| Branch " + ours.name + " has additional port " + theirPort.name)
//						}
//					}
//					for (theirPort : theirs.ports) {
//						val ourPort = ours.ports.findFirst[it.name == theirPort.name]
//						if (ourPort === null) {
//							System.err.println(modelName + "| Branch " + ours.name + " is missing port " + theirPort.name)
//						} else {
//							if (ourPort.portSelectiveNode != theirPort.portSelectiveNode) {
//								System.err.println(modelName + "| Branch " + ours.name + " | Port " + ourPort.name + " -> " + ourPort)
//								System.err.println(modelName + "    Our Port Selective Node: " + ourPort.portSelectiveNode)
//								System.err.println(modelName + "    Their Port Selective Node: " + theirPort.portSelectiveNode)
//							}
//							if (ourPort.portNode != theirPort.portNode) {
//								System.err.println(modelName + "| Branch " + ours.name + " | Port " + ourPort.name + " -> " + ourPort)
//								System.err.println(modelName + "    Our Port Node: " + ourPort.portSelectiveNode)
//								System.err.println(modelName + "    Their Port Node: " + theirPort.portSelectiveNode)
//							}
//							if ((ourPort instanceof ComplexGUIBranchPort) != (theirPort.portNode.isComplex)) {
//								System.err.println(modelName + "| Branch " + ours.name + " | Port " + ourPort.name + " -> " + ourPort)
//								System.err.println(modelName + "    Our Port is complex: " + (ourPort instanceof ComplexGUIBranchPort))
//								System.err.println(modelName + "    Their Port is complex: " + theirPort.portNode.isComplex)
//							}
//							if ((ourPort instanceof ComplexGUIBranchPort) && theirPort.portNode.isComplex
//									&& (ourPort as ComplexGUIBranchPort).type != theirPort.portNode.complexType) {
//								System.err.println(modelName + "| Branch " + ours.name + " | Port " + ourPort.name + " -> " + ourPort)
//								System.err.println(modelName + "    Our Port complex type: " + ourPort.portSelectiveNode)
//								System.err.println(modelName + "    Their Port complex type: " + theirPort.portSelectiveNode)
//							}
//						}
//					}
//				}
//			}
			
			return branches.toList
		}
		else calcGUIBranches(container, newHashSet,GUIBranch.ALL_BRANCH_ELEMENT_TYPES,merge).toList
	}
	
	def List<GUIBranch> getProcessBranches(ModelElementContainer container) {
		if (cacheValues) {
			var branches = _processBranchElements.get(container)
			if (branches === null) {
				branches = calcGUIBranches(container, newHashSet,GUIBranch.PROCESS_BRANCH_ELEMENT_TYPES,false)
				_processBranchElements.put(container, branches)
			}
			
			return branches.toList
		}
		else calcGUIBranches(container, newHashSet,GUIBranch.PROCESS_BRANCH_ELEMENT_TYPES,false).toList
	}
	
	
	private def Collection<GUIBranch> calcGUIBranches(ModelElementContainer container, Set<ModelElementContainer> seen, List<? extends Class<? extends ModelElement >> branchElements, boolean merge) {
		if (!seen.add(container)) {
			return #[]
		}
		val res = container
			.findDeeply(branchElements, digIntoNonExtendingGUISIBs)
			.filter(ModelElement) // we need this line to satisfy the compiler
			.filter[switch it {
				Button: isBranchable
				GUISIB: !hasParent(ExtensionContext)
				default: true
			}].map[switch it {
					DispatchedGUISIB: gui.subGUIs+#[gui]
					default: #[it]
				}].flatten
			.flatMap[switch it {
				GUI: calcGUIBranches(seen,branchElements,merge)
				GUISIB: findSelectiveNodes(seen,branchElements,merge)
				GUIPlugin: pluginOutputs.map[toGUIBranch]
				ProcessSIB: processEndSIBs.map[toGUIBranch]
				ModelElement: #[it.toGUIBranch]
				default: #[]
			}]
		if(merge) {
			return res.merge // merge branches that have the same name
		}
		res.toList
			
	}
	
private def <C extends ModelElement> List<GUIBranch> dispatchBranches(GUI parentGUI, Iterable<GUI> subGUIs, Set<ModelElementContainer> seen, List<? extends Class<? extends ModelElement >> branchElements, boolean merge) {
		if (seen.contains(parentGUI)) {
			return #[]
		}
		val branchesByName = parentGUI.calcGUIBranches(seen, branchElements, merge).toMap[name]
		seen.add(parentGUI)
		
		subGUIs.filter[!seen.contains(it)]
			.flatMap[calcGUIBranches(seen, branchElements, merge)]
			.forEach[ subGUIBranch | // merge branch ports, use the more general type
				val knownBranch = branchesByName.get(subGUIBranch.name)
				branchesByName.put(
					subGUIBranch.name, knownBranch?.mergedWith(subGUIBranch) ?: subGUIBranch
				)
			]
		return branchesByName.values.toList
	}
	
	
	
	private def <C extends ModelElement> Collection<GUIBranch> findSelectiveNodes(GUISIB sib, Set<ModelElementContainer> seen, List<? extends Class<? extends ModelElement >> branchElements, boolean merge) {
		val guiSIBBranches = sib.gui.calcGUIBranches(seen,branchElements,merge)
		//find correlating branch elements
//		guiSIBBranches.forEach[ branchRep |
//			branchRep.ports.forEach[ portRep |
//				val n = portRep.portNode
//				if (n instanceof AddComplexToSubmission && (n as AddComplexToSubmission).sourceElement.isIsInput) {
//					val inputVar = (n as AddComplexToSubmission).sourceElement;
//					//TODO
//					//check if there is a dataflow from the variables in th GUI to the variable
//					sib.gui.dataContexts.map[complexVariables].flatten  
//					val inputPort = sib.inputPorts.findFirst[name.equals(inputVar.name)]
//					val connectedVar = inputPort.getIncoming(ComplexRead).get(0).sourceElement
//					portRep.setPortSelectiveNode(connectedVar)
//				} else if (n instanceof ComplexVariable && (n as ComplexVariable).isIsInput) {
//					val inputVar = (n as ComplexVariable);
//					val inputPort = sib.inputPorts.findFirst[name.equals(inputVar.name)]
//					if (inputPort !== null) {
//						val connectedVar = inputPort.getIncoming(ComplexRead).get(0).sourceElement
//						portRep.setPortSelectiveNode(connectedVar)
//					}
//				}
//			]
//		]
		if(sib.modal!==null) {
			val bs = guiSIBBranches.filter[!sib.modal.modalCloseButton.map[branchName].contains(it.name)].toList
			return bs;
		}
		return guiSIBBranches
	}
	
	def boolean modalCloseButton(GUISIB guisib, String branchName) {
		if(guisib.modal!==null) {
			return guisib.modal.modalCloseButton.map[branchName].contains(branchName)
		}
		false
	}

	def Iterable<EndSIB> getProcessEndSIBs(ProcessSIB ip) {
		(ip.proMod as Process).endSIBs
	}

	/**
	 * Collects all events present in the given GUI model listener contexts
	 */
	def Iterable<Event> events(GUI gui) {
		gui.listenerContexts.map[events].flatten
	}

	/**
	 * Collects all outputs of a given GUI plug in component.
	 * The outputs correspond to a branch of all GUI plug in SIBs
	 * @param p the GUI plug in
	 * @return List<Output> the list of outputs correspond to branches
	 */
	def pluginOutputs(GUIPlugin p) {
		return p.abstractBranchs.filter(Branch)
	}

	/**
	 * Retrieves all sub-GUIs of the specified GUI.
	 * 
	 * @param gui - The GUI for which to retrieve sub-GUIs.
	 * @return The list of sub-GUIs.
	 */
	def getSubGUIs(GUI gui) {
		subGUIsMap.get(gui)
	}
	
	private def getSubGUIsMap() {
		if (cacheValues) {
			_subGUIs ?: (_subGUIs = calcSubGUIsMap)
		}
		else calcSubGUIsMap
	}
	
	private def calcSubGUIsMap() {
		newHashMap => [
			ReferenceRegistry.instance.lookup(GUI).forEach[ gui |
				put(gui, gui.calcExtendingGUIs)
			]
		]
	}
	
	private def calcExtendingGUIs(GUI gui) {
		superGUIsMap.values
			.filter[contains(gui)]
			.flatMap[subList(0, indexOf(gui))]
			.toSet
	} 

	/**
	 * Collects all direct sub GUI models for which the given GUI model is the parent
	 * @param gui the parent GUI model
	 * @param allGUIs list of all GUI models which should be considered as sub GUI model
	 * @return Iterable<GUI> list of all direct sub GUI models
	 */
	def List<GUI> findAllSubGUIs(GUI gui, Map<Integer, List<GUI>> sortedSubGuis) {
		val key = sortedSubGuis.entrySet.findFirst[value.findFirst[n|n.id.equals(gui.id)] != null].key
		val list = new LinkedList
		var i = key - 1
		while (i >= 0) {
			list += sortedSubGuis.get(i)
			i--
		}
		return list
	}

	/**
	 * Collects all sub GUI models for which the given GUI model is the parent
	 * @param gui the parent GUI model
	 * @param allGUIs list of all GUI models which should be considered as sub GUI model
	 * @return Iterable<GUI> list of all direct sub GUI models
	 */
	def findAllSubGUIs(GUISIB guiSib, Map<Integer, List<GUI>> allGUIs) {
		guiSib.gui.findAllSubGUIs(allGUIs)
	}

	/**
	 * Calculates the data model type of the given complex input port
	 * which is specialized by the equally named input port of the given sub GUI model
	 * @param port the complex input port of a dispatched GUI SIB
	 * @param subGUI one of the specialized sub GUI models which extends the type of the given port
	 * @return Type the type of the port or a specialized sub Type
	 */
	def Type toSpecialType(ComplexInputPort port, GUI subGUI) {
		subGUI.inputVariables.filter(ComplexVariable).filter[name.equals(port.name)].get(0).dataType
	}
	
	def List<GUI> getSuperGUIs(GUI gui) {
		superGUIsMap.get(gui)
	}
	
	private def getSuperGUIsMap() {
		if (cacheValues) {
			_superGUIs ?: (_superGUIs = calcSuperGUIsMap)
		} else calcSuperGUIsMap
	}
	
	private def calcSuperGUIsMap() {
		newHashMap => [
			ReferenceRegistry.instance.lookup(GUI).forEach[ gui |
				put(gui, gui.calcExtensionHierarchy)
			]
		]
	}
	
	private def List<GUI> calcExtensionHierarchy(GUI gui) {
		(#[gui] + (gui.extendedGUI?.calcExtensionHierarchy ?: #[])).toList
	}
	
	def getExtendedGUI(GUI it) {
		extendedGUIs.head
	}
	
	def getExtendedGUIs(GUI it) {
		extensionContexts.flatMap[find(GUISIB)].map[gui]
	}

	def dispatchedOutputs(GUI gui) {
		dispatchBranches(gui, gui.subGUIs, newHashSet, ALL_BRANCH_ELEMENT_TYPES, true)
	}
	
	def dispatchedOutputs(GUI parentGUI, GUI dispatchedGUI) {
		dispatchBranches(parentGUI, #[dispatchedGUI], newHashSet, ALL_BRANCH_ELEMENT_TYPES, true)
	}

	def getSortedDispatchedGUIs(GUI parentGUI) {
		// TODO this is an expensive operation. should be memoizable
		getSortedDispatchedGUIs(parentGUI, parentGUI.subGUIs)
	}

	def getSortedDispatchedGUIs(GUI parentGUI, Collection<GUI> specializedGUI) {
		// phase 1: group by input score
		val spezAndSelf = new LinkedList
		spezAndSelf.addAll(specializedGUI)
		spezAndSelf.add(parentGUI)
		val Map<Integer, List<GUI>> scoredGUIList = spezAndSelf.groupBy[inputScore(parentGUI.inputVariables)]
		// phase 2: sort by input
		val sortedByInputScore = scoredGUIList.entrySet.sortBy[key].reverse

		val Map<Integer, List<GUI>> resultMap = new HashMap
		var i = 0;
		for (s : sortedByInputScore) {
			val sortedByHierachyScore = s.value.groupBy[hierachyScore].entrySet.sortBy[key].reverse
			for (h : sortedByHierachyScore) {
				if (!resultMap.containsKey(i)) {
					resultMap.put(i, new LinkedList)
				}
				resultMap.get(i).addAll(h.value)
				i++
			}
		}
		return resultMap
	}

	private def int inputScore(GUI g, Iterable<Variable> parentInputs) {
		val sum = g.inputVariables.filter(ComplexVariable).filter [v|
			(parentInputs.findFirst[name.equals(v.name)] != null)
		].map[singleScore].reduce[sum, score|score + sum]
		if (sum === null) {
			return 0
		}
		return sum
	}

	private def int hierachyScore(GUI gui) {
		gui.superGUIs.size
	}

	private def int getSingleScore(ComplexVariable v) {
		v.dataType.superTypes.size
	}
	
	
	/**
	 * Retrieves the top-level variable of an attribute in the data context.
	 */
	def Variable getTopLevelVariable(Attribute it) {
		rootVariable?.topLevelVariable
	}
	
	/**
	 * Retrieves the top-level variable of an attribute if it is represented
	 * as a node of type {@link Variable} that is connected to the actual
	 * variable via an {@link AttributeConnector}.
	 */
	def Variable getTopLevelVariable(Variable it) {
		rootVariable?.topLevelVariable ?: it
	}
	
	/**
	 * Retrieves the root variable of an attribute.
	 */
	def getRootVariable(Attribute attribute) {
		attribute.container as Variable
	}
	
	/**
	 * Retrieves the root variable of an attribute if it is represented
	 * as a node of type {@link Variable} that is connected to the actual
	 * variable via an {@link AttributeConnector}.
	 */
	def getRootVariable(Variable attribute) {
		{ attribute.findSourceOf(ComplexAttributeConnector)
			?: attribute.findSourceOf(ComplexListAttributeConnector)
		} as Variable
	}
	
	/**
	 * Retrieves the name of an attribute.
	 */
	def String getName(Attribute it) {
		switch it {
			PrimitiveAttribute: attribute.name
			PrimitiveListAttribute: attributeName.toString
			ComplexAttribute: attribute.name
			ComplexListAttribute: attributeName.toString
		}
	}
	
	/**
	 * Returns all top level variables of a given GUI model
	 * @param gui
	 * @return
	 */
	def findVariables(GUI gui) {
		gui.dataContexts.map[variables].flatten
			.filter[getIncoming(ComplexAttributeConnector).isEmpty]
			.filter[getIncoming(ComplexListAttributeConnector).isEmpty]
	}
	
	/** 
	 * Finds all embedded processes inside the specified container.
	 * Recurses into all sub-containers.
	 * 
	 * @param container - The container holding the elements to be searched through.
	 * @return A set of processes. Might be empty but is never null.
	 */
	def Set<Process> getEmbeddedProcesses(ModelElementContainer container) {
		if (cacheValues) {
			_embeddedProcesses.get(container)
			?: container.find(ProcessSIB).map[proMod as Process].toSet => [
				_embeddedProcesses.put(container, it)
			]
		} else container.find(ProcessSIB).map[proMod as Process].toSet
	}
	
	/** 
	 * Finds the model element that is used in an iteration, i.e. it has an incoming
	 * Iteration or TableLoad edge. This element is either the specified model element
	 * itself or the first parent (bottom-up) that fulfills the condition.
	 * 
	 * @param elm - The element for which to find the loop container.
	 * @return  The first matching parent, or {@code null} if none exists.
	 */
	def Node getSurroundingLoop(Node elm) {
		if (cacheValues) {
			_surroundingLoops.get(elm)
			?: calcSurroundingLoop(elm) => [
				_surroundingLoops.put(elm, it)
			]
		} else calcSurroundingLoop(elm)
	}
	
	def calcSurroundingLoop(Node elm) {
		if (!elm.findPredecessorsVia(Iteration, TableLoad).isEmpty)
			elm
		else elm.findFirstParent(Container) [
			!findPredecessorsVia(Iteration, TableLoad).isEmpty
		]
	}
	
	/** 
	 * Finds all input elements that are relevant for the specified form only,
	 * i.e. the search recurses into all sub-containers except Forms.
	 * 
	 * @param form - The Form container holding the elements to be searched through.
	 * @return  A list of Inputs. Might be empty but is never null.
	 */
	def List<Input> getFormInputs(Form form) {
		if (cacheValues) {
			_formInputs.get(form)
			?: calcFormInputs(form) => [
				_formInputs.put(form, it)
			]
		} else calcFormInputs(form)
	}
	
	private def calcFormInputs(Form form) {
		form.find(Input).filter[
			it.findFirstParent(Form) == form
		].toList
	}
	
	/** 
	 * Finds all elements of specific type inside the specified container.
	 * Recurses into all sub-containers.
	 * 
	 * @param container - The container holding the elements to be searched through.
	 * @param clazz - The class of the elements to be found.
	 * @return  A list of elements of the specified type. Might be empty but is never null.
	 */
	override <C extends IdentifiableElement> List<C> find(ModelElementContainer container, Class<C> clazz) {
		if (cacheValues) {
			_modelElements.get(container).get(clazz) as List<C>
			?: (super.find(container, clazz).toList => [
				_modelElements.get(container).put(clazz, it)
			])
		} else super.find(container, clazz).toList
	}
	
	/** 
	 * Finds the first parent (bottom-up) of a model element of a specific type.
	 * 
	 * @param elm - The element for which to find the parent elements.
	 * @param cls - The class of the parent elements to be found.
	 * @return  The first parent of the specified type, or {@code null} if none exists.
	 */
	override <E extends ModelElement> E findFirstParent(ModelElement elm, Class<E> cls) {
		if (cacheValues) {
			_firstParents.get(elm).get(cls) as E
			?: (super.findFirstParent(elm, cls) => [
				_firstParents.get(elm).put(cls, it)
			])
		} else super.findFirstParent(elm, cls)
	}
	
	/**
	 * Find all elements of specific type inside the specified container.
	 * <br>Recurses into all sub-containers.
	 * <br>Recurses into GUI sub-models that are integrated via GUISIBs.
	 * <br>The type of each element is matched to the specified type via {@code instanceof}.
	 * 
	 * <p>Convenient method for {@code findDeeply(container, cls, [switch it { GUISIB: it.gui }])}</p>
	 * 
	 * @param container  The container holding the elements to be searched through.
	 * @param cls  The class of the elements to be found.
	 * @return  A set of elements of the specified type. Might be empty but is never {@code null}.
	 */
	def <C extends ModelElement> findDeepInGUIs(ModelElementContainer it, Class<C> cls) {
		findDeeply(cls, digIntoGUISIBs)
	}
	
	/**
	 * Find all elements of any of the specified types inside the specified container.
	 * <br>Recurses into all sub-containers.
	 * <br>Recurses into GUI sub-models that are integrated via GUISIBs.
	 * <br>The type of each element is matched to the specified types via {@code instanceof}.
	 * 
	 * <p>Convenient method for {@code findDeeply(container, classes, [switch it { GUISIB: it.gui }])}</p>
	 * 
	 * @param container  The container holding the elements to be searched through.
	 * @param classes  A list of classes. The elements to be found must be instance of at least one of them.
	 * @return  A set of elements of any of the specified types. Might be empty but never {@code null}.
	 */
	def <C extends ModelElement> findDeepInGUIs(ModelElementContainer it, Class<C>[] classes) {
		findDeeply(classes, digIntoGUISIBs)
	}
	
	def String placeholderTransclusion(String name,String id)'''select="[dime«IF !id.nullOrEmpty»-«ENDIF»«id»-placeholder=«name»]"'''
	
	def String argumentTransclusion(String name,String id)'''dime«IF !id.nullOrEmpty»-«ENDIF»«id»-placeholder="«name»"'''
	
	/**
	 * Returns the static URL of the given button if one is present
	 */
	def getStaticURL(Button button){
		button?.options.toStaticURL
	}
	
	def toStaticURL(ButtonOptions options) {
		if(options == null ||options.staticURL.nullOrEmpty) {
			return null;
		}
		if(options.staticURL.equals("/logout")||options.staticURL.equals("logout")){
			return "{{getLogoutURL}}"
		}
		options?.staticURL
	}
	
	/**
	 * Returns the static URL of the given button if one is present
	 */
	def getStaticURL(LinkSIB button) {
		button?.options.toStaticURL	
	}
	
	def dispatch void getIgnorableBranches(info.scce.dime.process.process.SIB sib, Map<String, String> ignorableBranches) {}
	def dispatch void getIgnorableBranches(info.scce.dime.process.process.GUISIB sib, Map<String, String> ignorableBranches) {
		sib.gui.getGUIBranchesMerged.map[name].forEach[ignorableBranches.put(it,it)]
	}
	def dispatch void getIgnorableBranches(info.scce.dime.process.process.ProcessSIB sib, Map<String, String> ignorableBranches) {
		sib.proMod.endSIBs.map[branchName].forEach[ignorableBranches.put(it,it)]
	}
	def dispatch void getIgnorableBranches(AtomicSIB sib, Map<String, String> ignorableBranches) {
		(sib.getSib as SIB).branches.map[name].forEach[ignorableBranches.put(it,it)]
	}
	def dispatch void getIgnorableBranches(RetrieveCurrentUserSIB sib, Map<String, String> ignorableBranches) {
		ignorableBranches.put("not authenticated","not authenticated")
	}
	def dispatch void getIgnorableBranches(RemoveFromListSIB sib, Map<String, String> ignorableBranches) {
		ignorableBranches.put("not found","not found")
	}
	def dispatch void getIgnorableBranches(RetrieveOfTypeSIB sib, Map<String, String> ignorableBranches) {
		ignorableBranches.put("none found","none found")
	}
}
