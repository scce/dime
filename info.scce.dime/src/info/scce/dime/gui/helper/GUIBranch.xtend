/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.helper

import graphmodel.ModelElement
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gUIPlugin.Output
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.Process
import java.util.List
// TODO Find replacement for Data Annotation
import org.eclipse.xtend.lib.annotations.Data

@Data class GUIBranch {
	
	ModelElement element
	String name
	List<GUIBranchPort> ports
	
	extension DataExtension = DataExtension.instance
	
	public static val ALL_BRANCH_ELEMENT_TYPES = #[Button, GUIPlugin, ProcessSIB, DispatchedGUISIB, GUISIB]
	
	public static val PROCESS_BRANCH_ELEMENT_TYPES = #[ProcessSIB, GUISIB, DispatchedGUISIB]
	
	def mergedWith(GUIBranch other) {
		val portsByName = this.ports.toMap[it.name]
		// add unknown ports, merge known ones
		for (otherPort : other.ports) {
			val knownPort = portsByName.get(otherPort.name)
			if (knownPort === null) {
				portsByName.put(otherPort.name, otherPort)
			} else if (knownPort instanceof ComplexGUIBranchPort) {
				// port is known and port is complex => use the more general type
				if(!knownPort.type.equals((otherPort as ComplexGUIBranchPort).type)) {
				
					val superTypes = knownPort.type.superTypes.toList
					val equalSuperTypes = (otherPort as ComplexGUIBranchPort).type.superTypes.filter[superTypes.contains(it)].toList
					if(!equalSuperTypes.empty) {
						knownPort.type = equalSuperTypes.get(0)
					}
					//find best super type
					equalSuperTypes.forEach[est|{
						if(knownPort.type.superTypes.length < est.superTypes.length) {
							knownPort.type = est
						}
					}]
				}
			}
		}
		new GUIBranch(this.element, this.name, portsByName.values.toList)
	}
	
	def getOriginatingModelClass() {
		this.element.rootElement.class
	}
	
	def hasGUIOrigin() {
		this.element.rootElement instanceof GUI
	}
	
	def hasProcessOrigin() {
		this.element.rootElement instanceof Process
	}
	
	static def toGUIBranch(ModelElement elm) {
		new GUIBranch(elm, getBranchName(elm), GUIBranchPort.getPorts(elm))
	}
	
	static def String getBranchName(ModelElement elm) {
		switch it:elm {
			// GUI
			Button: it.label
			Branch: it.name
			// GUI Plugin
			Output: it.outputName
			// Process
			EndSIB: it.branchName
		}
	}
	
	
	
	static def merge(Iterable<GUIBranch> it) {
		groupBy[
			// FIXME Find a better solution - the current one exists because the previous implementation implicitly preferred GUI over Process
			#[it.name, it.originatingModelClass]
		].mapValues[
			reduce[ x,y | x.mergedWith(y) ]
		].values
	}
}
