/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.helper
// TODO Find replacement for Accessors Annotation
import org.eclipse.xtend.lib.annotations.Accessors
import info.scce.dime.data.data.Type
import org.eclipse.emf.ecore.EObject
import info.scce.dime.gui.gui.AddComplexToSubmission
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gUIPlugin.Function

@Accessors class ComplexGUIBranchPort extends GUIBranchPort {
	
	Type type
	
	new(EObject portNode) {
		super(portNode)
		this.type = portNode.complexType
	}
	
	def getTypeViewData() {
		switch selectiveNode : portSelectiveNode {
			// GUI
			AddComplexToSubmission: selectiveNode.sourceElement
			ComplexAttribute: selectiveNode.attribute
			ComplexOutputPort 
				case selectiveNode.container instanceof Branch
					&& ((selectiveNode.container as Branch).container instanceof GUIPlugin):
				{
				val guiPlugin = (selectiveNode.container as Branch).container as GUIPlugin
				val fun = guiPlugin.function as Function
				val param = fun.outputs.flatMap[parameters].findFirst[it.name == selectiveNode.name]
				if (param === null) {
					throw new IllegalStateException('''port could not be found on plugin «guiPlugin.label» in «guiPlugin.rootElement.title»''')
				}
				else param
			}
			
			default: selectiveNode
		}
	}
}
