/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.gui.gui.Alert
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.TableEntry
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Thumbnail

/** 
 * Context menu action, which updates a given GUI SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class UpdateDispatchedGuiSIB extends UpdateGuiSIB {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Update dispatched GuiSIB"
	}
	
	override GUISIB createSIB(GUISIB guiSIB) {
		val gui = guiSIB.gui
		val x = guiSIB.x
		val y = guiSIB.y
		switch it : guiSIB.container {
			Alert: newDispatchedGUISIB(gui, x, y)
			Argument: newDispatchedGUISIB(gui, x, y)
			Box: newDispatchedGUISIB(gui, x, y)
			Col: newDispatchedGUISIB(gui, x, y)
			ExtensionContext: newDispatchedGUISIB(gui, x, y)
			Form: newDispatchedGUISIB(gui, x, y)
			Jumbotron: newDispatchedGUISIB(gui, x, y)
			Panel: newDispatchedGUISIB(gui, x, y)
			Tab: newDispatchedGUISIB(gui, x, y)
			TableEntry: newDispatchedGUISIB(gui, x, y)
			Template: newDispatchedGUISIB(gui, x, y)
			Thumbnail: newDispatchedGUISIB(gui, x, y)
			default: {
				showDialog("Unknown container", "Update failed: No handler for the SIB's container")
				return null
			}
		}
	}
	
}
