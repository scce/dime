/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.ListenerContext

class EventNewPrimitiveInput extends DIMECustomAction<Event> {
	val OFFSET = 30
	override getName() {
		"New Primitive Input"
	}
	override execute(Event evt) {
		try {
			// layout and resize is then automatically done by OutputPortHook
			evt.newPrimitiveOutputPort(1, 1)
			layout(evt)
		} catch (Exception e) {
			e.printStackTrace()
		}

	}
	
	def layout(Event node) {
		if (node.getContainer() instanceof ListenerContext) {
			var listenerContext = node.getContainer() as ListenerContext
			var maxHeight = maxHeight(listenerContext);
			listenerContext.setHeight(maxHeight + OFFSET);
		}

	}

	def maxHeight(ListenerContext context) {
		var maxLowerBound = 50;
		for (node : context.nodes) {
			if (node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height
			}
		}
		return maxLowerBound
	}
}
