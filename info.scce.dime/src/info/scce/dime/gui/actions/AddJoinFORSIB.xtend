/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.hooks.CreateComponentHook

/** 
 * Adds an Join argument to the FORSIB.
 * @author zweihoff
 */
class AddJoinFORSIB extends DIMECustomAction<FORSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"add join"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(FORSIB node) throws ClassCastException {
		return !node.arguments.exists[blockName.equals("JOIN")]
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(FORSIB node) {
		var arg = node.newArgument(10,0)
		arg.blockName = "JOIN"
		//trigger layouter
		var CreateComponentHook cch = new CreateComponentHook()
		cch.postCreate(arg)
			
		
		
	}
}
