/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Thumbnail
import info.scce.dime.process.process.Process
import org.eclipse.emf.ecore.EObject

/** 
 * Context menu action, which updates a given Link Process SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class UpdateLinkProcessSIB extends DIMEGUICustomAction<LinkProcessSIB> {
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Update Link SIB"
	}

	/** 
	 * Checks if the reference to a Link Process SIB is present and valid
	 */
	override canExecute(LinkProcessSIB sib) {
//		FIXME: Workaround because getter for stealth imports are not generated
//		sib.interactionProcess != null
		ReferenceRegistry.instance.getEObject(sib.libraryComponentUID) != null
	}

	/** 
	 * Updates the given Link Process SIB.
	 * The update process creates a new Link Process SIB
	 * and uses the semantics of the post create hook to
	 * consider all changes.
	 * In a second step, the data flow edges are reconnected
	 * and the out dated Link Process SIB is removed, so that
	 * the new SIB is in the same position
	 */
	//TODO beautify after auto conversion to Xtend class
	//TODO create/use extension method for this stuff
	override void execute(LinkProcessSIB sib) {
		var EObject eObj = sib.proMod
		if (eObj === null || eObj instanceof Process === false) {
			showDialog("Reference is null", "Update failed. Reference is null!")
			return;
		}
		//collect static inputs
		
		// Create new Link SIB
		var LinkProcessSIB newSIB = null
		var EObject container = sib.getContainer()
		if (container instanceof Template) {
			var Template n = (container as Template)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Col) {
			var Col n = (container as Col)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Tab) {
			var Tab n = (container as Tab)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Argument) {
			var Argument n = (container as Argument)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Panel) {
			var Panel n = (container as Panel)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Jumbotron) {
			var Jumbotron n = (container as Jumbotron)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Box) {
			var Box n = (container as Box)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Form) {
			var Form n = (container as Form)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Thumbnail) {
			var Thumbnail n = (container as Thumbnail)
			newSIB = n.newLinkProcessSIB(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if(sib.generalStyle !=null) {
			newSIB.generalStyle = sib.generalStyle;
		}
		sib.rebuildPorts(newSIB);
		sib.delete()
	}
	
}
