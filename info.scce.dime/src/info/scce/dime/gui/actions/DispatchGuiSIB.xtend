/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.DispatchedGUISIB

/** 
 * Context menu action, which updates a given GUI SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class DispatchGuiSIB extends DIMEGUICustomAction<GUISIB> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Activate Dispatching"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(GUISIB guiSib) {
		!(guiSib instanceof DispatchedGUISIB) && !(guiSib.container instanceof ExtensionContext)
	}

	override void execute(GUISIB guiSib) {
		new UpdateDispatchedGuiSIB().updateSIB(guiSib)
	}
}
