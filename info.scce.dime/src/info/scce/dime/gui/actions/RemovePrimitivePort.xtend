/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.hooks.CreateComponentHook
import info.scce.dime.process.helper.LayoutConstants

/** 
 * Adds input ports to a given IF SIB
 * @author zweihoff
 */
class RemovePrimitivePort<T extends IO> extends DIMECustomAction<IO> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"Remove input port"
	}


	/** 
	 * Input ports can always be added
	 */
	override canExecute(IO node) throws ClassCastException {
		return node.container instanceof ControlSIB
	}

	/** 
	 * Creates a new primitive input port
	 */
	override execute(IO io) {
		val node = io.container as ControlSIB
		// move ports
		node.IOs.filter[!id.equals(io.id)].forEach[n|{
			if(n.y >= io.y){
				// has to be moved
				n.y = n.y -LayoutConstants.PORT_SPACE
			}
		}]
		//move arguments
		node.arguments.forEach[n|{
			n.y = n.y -LayoutConstants.PORT_SPACE
		}]
		//resize SIB
		node.resize(node.width,node.height-LayoutConstants.PORT_SPACE)
		//calculate next position
		io.delete
		//trigger layouter
		var CreateComponentHook cch = new CreateComponentHook()
		cch.postCreate(node)
	}
}
