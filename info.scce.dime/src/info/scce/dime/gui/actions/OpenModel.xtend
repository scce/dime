/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.GuardSIB
import info.scce.dime.gui.gui.SIB
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.dad.dad.URLProcess

/** 
 * The open model action opens the editor for the model referenced by a given embedded
 * GUI SIBs, Guard SIBs or GUI plug in SIB placed in a GUI model
 * @author zweihoff
 */
class OpenModel extends DIMECustomAction<SIB> {
	
	/** 
	 * Preserves that the action does not perform any changes to the GUI model,
	 * so that it has not to be flagged as dirty
	 */
	override hasDoneChanges() {
		false
	}

	/** 
	 * If a GUI SIB, Guard SIB or GUI plug in SIB is double clicked, the corresponding model
	 * is opened in a new editor tab, which is brought to front.
	 */
	override execute(SIB s) {
		switch s {
			GUISIB: s.gui
			GUIPlugin: s.function.eContainer.eContainer
//			FIXME: This is a workaround, since the prime attribute getter are not generated for
//			stealth imports...
			GuardSIB: s.process
			ProcessSIB: s.proMod
			LinkProcessSIB: (s.proMod as URLProcess).rootElement
//			GuardSIB: s.process
//			InteractionSIB: s.interactionProcess
		}?.openEditor
	}
}
