/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import graphmodel.Container
import graphmodel.Node
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.MoveLeft
import info.scce.dime.gui.gui.MoveSign
import info.scce.dime.gui.gui.MoveUp
import info.scce.dime.gui.hooks.CreateComponentHook
import info.scce.dime.gui.hooks.LayoutHelper

/** 
 * The MoveAction is triggered, when a moving sign on a movable component is double clicked.
 * The component which surrounds the clicked moving sign is moved in the direction, the sign signs.
 * @author zweihoff
 */
class MoveAction extends DIMECustomAction<MoveSign> {
	public static int MOVE_UP = 1
	public static int MOVE_DOWN = 2
	public static int MOVE_LEFT = 3
	public static int MOVE_RIGHT = 4

	/** 
	 * If a moving sign is double clicked the component is moved.
	 * Moving means, the clicked component switches its place with a neighbor on the signed place. 
	 */
	// TODO beautify after auto conversion to Xtend class
	override execute(MoveSign node) {
		var gui = node.rootElement
		try {
			var int direction = 0
			var Container clickedNode = (node.getContainer() as Container)
			var Container cContainer = (clickedNode.getContainer() as Container)
			var Node shufflePartner = null
			if (LayoutHelper.isHorizontalComponent(cContainer)) {
				// If Move Left is clicked
				if (node instanceof MoveLeft) {
					for (Node n : LayoutHelper.getAllNodes(cContainer)) {
						if (n.getX() < clickedNode.getX()) {
							if (shufflePartner === null) {
								shufflePartner = n
							} else {
								if (clickedNode.getX() - n.getX() < clickedNode.getX() - shufflePartner.getX()) {
									shufflePartner = n
								}
							}
							direction = MOVE_LEFT
						}
					}
				} else {
					for (Node n : LayoutHelper.getAllNodes(cContainer)) {
						if (n.getX() > clickedNode.getX()) {
							if (shufflePartner === null) {
								shufflePartner = n
							} else {
								if (n.getX() - clickedNode.getX() < shufflePartner.getX() - clickedNode.getX()) {
									shufflePartner = n
								}
							}
							direction = MOVE_RIGHT
						}
					}
				}
			} else {
				if (node instanceof MoveUp) {
					for (Node n : LayoutHelper.getAllNodes(cContainer)) {
						if (n.getY() < clickedNode.getY()) {
							if (shufflePartner === null) {
								shufflePartner = n
							} else {
								if (clickedNode.getY() - n.getY() < clickedNode.getY() - shufflePartner.getY()) {
									shufflePartner = n
								}
							}
							direction = MOVE_UP
						}
					}
				} else {
					for (Node n : LayoutHelper.getAllNodes(cContainer)) {
						if (n.getY() > clickedNode.getY()) {
							if (shufflePartner === null) {
								shufflePartner = n
							} else {
								if (n.getY() - clickedNode.getY() < shufflePartner.getY() - clickedNode.getY()) {
									shufflePartner = n
								}
							}
							direction = MOVE_DOWN
						}
					}
				}
			}
			// Shuffle positions
			if (shufflePartner !== null) {
				if (direction === MOVE_UP) {
					clickedNode.setY(shufflePartner.getY())
					shufflePartner.setY(clickedNode.getY() + clickedNode.getHeight() + CreateComponentHook.V_OFFSET)
				}
				if (direction === MOVE_DOWN) {
					shufflePartner.setY(clickedNode.getY())
					clickedNode.setY(shufflePartner.getY() + shufflePartner.getHeight() + CreateComponentHook.V_OFFSET)
				}
				if (direction === MOVE_LEFT) {
					clickedNode.setX(shufflePartner.getX())
					shufflePartner.setX(clickedNode.getX() + clickedNode.getWidth() + CreateComponentHook.H_OFFSET)
				}
				if (direction === MOVE_RIGHT) {
					shufflePartner.setX(clickedNode.getX())
					clickedNode.setX(shufflePartner.getX() + shufflePartner.getWidth() + CreateComponentHook.H_OFFSET)
				}
			}
		} catch (Exception e) {
			e.printStackTrace()
		}

	}
}
