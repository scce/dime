/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions;

import graphmodel.ModelElementContainer;
import info.scce.dime.api.DIMECustomAction;
import info.scce.dime.gui.gui.EventListener;
import info.scce.dime.gui.gui.IO;
import info.scce.dime.gui.gui.PrimitiveInputPort;
import info.scce.dime.gui.gui.PrimitiveType;
import info.scce.dime.gui.gui.SIB;

/**
 * Context menu action to convert a primitive input port of a GUI SIB or GUI plug in SIB
 * to a static input port of the same data type.
 * The context menu entry is available for primitive input ports.
 * @author zweihoff
 *
 */
//TODO convert to Xtend to use extension providers more elegantly
public class PrimitivePortToStatic extends DIMECustomAction<PrimitiveInputPort> {

	/**
	 * Returns the name of the context menu entry
	 */
	@Override
	public String getName() {
		return "Convert Input to Static Value";
	}
	
	/**
	 * The conversion is possible for all primitive data types despite the File type,
	 * because a file cannot be specified statically
	 */
	@Override
	public boolean canExecute(PrimitiveInputPort port) {
		return (!PrimitiveType.FILE.equals(port.getDataType()));
	}

	/**
	 * Creates a new primitive static input port with the corresponding type
	 * of the primitive port data type and deleted the primitive port afterwards.
	 */
	@Override
	public void execute(PrimitiveInputPort port) {
		try {
     		int x = port.getX();
     		int y = port.getY();
     		String name = port.getName();
     		PrimitiveType dataType = port.getDataType();
     		ModelElementContainer container = port.getContainer();
     		port.delete();
     		
     		//TODO create/use extension method for this stuff
     		
     		IO staticInput = null;
     		if(container instanceof SIB) {
     			switch (dataType) {
     			case BOOLEAN:
     				staticInput = ((SIB)container).newBooleanInputStatic(x,y);
     				break;
     			case INTEGER:
     				staticInput = ((SIB)container).newIntegerInputStatic(x,y);
     				break;
     			case REAL:
     				staticInput = ((SIB)container).newRealInputStatic(x,y);
     				break;
     			case TEXT:
     				staticInput = ((SIB)container).newTextInputStatic(x,y);
     				break;
     			case TIMESTAMP:
     				staticInput = ((SIB)container).newTimestampInputStatic(x,y);
     				break;
     			default:
     				throw new IllegalStateException("default case in exhaustive switch should not happen; please fix broken implementation");
     			}
     		}
     		if(container instanceof EventListener) {
     			switch (dataType) {
     			case BOOLEAN:
     				staticInput = ((EventListener)container).newBooleanInputStatic(x,y);
     				break;
     			case INTEGER:
     				staticInput = ((EventListener)container).newIntegerInputStatic(x,y);
     				break;
     			case REAL:
     				staticInput = ((EventListener)container).newRealInputStatic(x,y);
     				break;
     			case TEXT:
     				staticInput = ((EventListener)container).newTextInputStatic(x,y);
     				break;
     			case TIMESTAMP:
     				staticInput = ((EventListener)container).newTimestampInputStatic(x,y);
     				break;
     			default:
     				throw new IllegalStateException("default case in exhaustive switch should not happen; please fix broken implementation");
     			}
     		}
     		
     		if(staticInput != null){
     			staticInput.setName(name);
     			staticInput.highlight();  
     		}
     		

     	}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
