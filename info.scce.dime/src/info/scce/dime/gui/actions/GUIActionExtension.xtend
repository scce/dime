/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import graphmodel.Container
import info.scce.dime.gui.gui.BaseElement
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexRead
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.^FOR
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.^IF
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.IS
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.PrimitiveRead
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.Registration
import info.scce.dime.gui.gui.SIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import info.scce.dime.process.process.Process
import java.util.HashMap
import java.util.List
import java.util.Map
import de.jabc.cinco.meta.core.utils.messages.CincoMessageHandler

class GUIActionExtension {
	
	def rebuildPorts(BaseElement oldSIB,BaseElement newSIB)
	{
		//collect static inputs
		var staticPorts = oldSIB.IOs.filter[n|n instanceof InputStatic].map[n|n as InputStatic]
		if (newSIB !== null) {
			// If a new (up-to-date) GUISIB has been created
			if(newSIB instanceof SIB && oldSIB instanceof SIB){
				(newSIB as SIB).setLabel((oldSIB as SIB).getLabel())				
			}
			// Reconnect If and For edges
			for (^IF i : oldSIB.getIncoming(typeof(^IF))) {
				i.reconnectTarget(newSIB)
			}
			for (^FOR i : oldSIB.getIncoming(typeof(^FOR))) {
				i.reconnectTarget(newSIB)
			}
			for (IS i : oldSIB.getIncoming(typeof(IS))) {
				i.reconnectTarget(newSIB)
			}
			
			
			// Reconnet dataflow to inputs for event listeners
			if(newSIB instanceof GUISIB || newSIB instanceof GUIPlugin || newSIB instanceof DispatchedGUISIB) {
				
				reconnectPorts((oldSIB as SIB).eventInputs,(newSIB as SIB).eventInputs)
				reconnectRegistration((oldSIB as SIB).eventListeners,(newSIB as SIB).eventListeners)
				rebuildStaticInputs((oldSIB as SIB).staticEventInputs,(newSIB as SIB).eventInputs,newSIB)
			}
			//reconnect data flow to sib inputs
			reconnectPorts(oldSIB.sibInputs,newSIB.sibInputs)

			rebuildStaticInputs(staticPorts.toList,newSIB.IOs,newSIB)
		}
		else {
			System::out.println("Could not create new GUISIB in Update Action")
		}
		
	}
	
	def Map<String, List<InputStatic>> getStaticEventInputs(SIB csib)
	{
//		csib.eventInputs.mapValues[n|n.filter[e|e instanceof CInputStatic].map[modelElement as InputStatic].toList]
		csib.eventInputs.mapValues[n|n.filter[e|e instanceof InputStatic].map[it as InputStatic].toList]
	}
	
	def rebuildStaticInputs(List<InputStatic> staticPortMap,List<IO> cioMap,Container inputContainer){
		val oldMap = new HashMap
		oldMap.put("key",staticPortMap)
		val newMap = new HashMap
		newMap.put("key",cioMap)
		rebuildStaticInputs(oldMap,newMap,inputContainer)
	}
	
	def rebuildStaticInputs(Map<String,List<InputStatic>> staticPortMap,Map<String,List<IO>> cioMap,Container inputContainer){
		for(compoundStaticPort:staticPortMap.entrySet) {
			// get same compound
			var compoundCios = cioMap.get(compoundStaticPort.key);
			for(staticPort:compoundStaticPort.value) {
				//FIXME: Provide method to check if a IdentifiableElement is deleted
				val port = compoundCios.filter[eContainer!=null].findFirst[name.equals(staticPort.name)]
				if(port != null){
					val portName = port.name
					var ppts = new PrimitivePortToStatic()
					ppts.execute(port as PrimitiveInputPort)
					//find static port
					var newStaticPort = inputContainer.IOs.filter[n|n instanceof InputStatic].findFirst[name.equals(portName)]
					if(newStaticPort != null){
						switch newStaticPort {
							IntegerInputStatic case (staticPort instanceof IntegerInputStatic): newStaticPort.value = (staticPort as IntegerInputStatic).value
							RealInputStatic case (staticPort instanceof RealInputStatic): newStaticPort.value = (staticPort as RealInputStatic).value
							TextInputStatic case (staticPort instanceof TextInputStatic): newStaticPort.value = (staticPort as TextInputStatic).value
							BooleanInputStatic case (staticPort instanceof BooleanInputStatic): newStaticPort.value = (staticPort as BooleanInputStatic).value
							TimestampInputStatic case (staticPort instanceof TimestampInputStatic): newStaticPort.value = (staticPort as TimestampInputStatic).value
						}
					}
				}
			}
			
		}
	}
	
	def List<IO> getIOs(Container container){
		if(container instanceof SIB)return (container as SIB).IOs;
		if(container instanceof ControlSIB)return (container as ControlSIB).IOs;
		if(container instanceof EventListener)return (container as EventListener).IOs;
	}
	
	def reconnectRegistration(List<EventListener> oldEvt, List<EventListener> newEvt) {
		for(o:oldEvt.filter[!getIncoming(Registration).empty].filter[o|!newEvt.filter[name.equals(o.name)].empty]) {
			var e = newEvt.filter[name.equals(o.name)].get(0)
			for(reg:o.getIncoming(Registration)) {
				reg.reconnectTarget(e);
			}
		}
	}
	
	def List<EventListener> eventListeners(SIB csib) {
		csib.abstractBranchs.filter(EventListener).toList
	}
	
	def dispatch Map<String, List<IO>> getSibInputs(SIB csib) {
		return csib.IOs.groupBy[csib.label]
	}
	
	def dispatch Map<String, List<IO>> getSibInputs(SecuritySIB csib) {
		return csib.IOs.groupBy[(csib.proMod as Process).modelName]
	}
	
	def Map<String,List<IO>> eventInputs(SIB csib){
		return csib.eventListeners.map[IOs].flatten.groupBy[(container as EventListener).name]
	}
	
	def boolean showDialog(String title, String msg) {
		return CincoMessageHandler.showQuestion(msg,title,true);
	}
	
	def reconnectPorts(Map<String,List<IO>> oldCompound,Map<String,List<IO>> newCompound)
	{
		// Reconnet dataflow to inputs
		for(compound:newCompound.entrySet) {
			var newCIOs = compound.value
			if(oldCompound.containsKey(compound.key)) {
			var oldCIOs = oldCompound.get(compound.key)
				for (IO io : oldCIOs) {
				
				// Search for corresponding
				for (IO newIo : newCIOs) {
					
					if (io.getName().equals(newIo.getName())) {
						// Found matching port
						if (newIo instanceof ComplexInputPort && io instanceof ComplexInputPort) {
							// IF both are still complex
							if (!io.getIncoming(typeof(ComplexRead)).isEmpty()) {
								// Data read edge is present
								// and will be reconnected
								io.getIncoming(typeof(ComplexRead)).get(0).reconnectTarget(
									(newIo as ComplexInputPort))
							}
						}
						if (newIo instanceof PrimitiveInputPort && io instanceof PrimitiveInputPort) {
							// IF both are still complex
							if (!io.getIncoming(typeof(PrimitiveRead)).isEmpty()) {
								// Data read edge is present
								// and will be reconnected
								io.getIncoming(typeof(PrimitiveRead)).get(0).reconnectTarget(
									(newIo as PrimitiveInputPort))
							}
						}
					}
				}
			}
			}
		}
			
	}
}
