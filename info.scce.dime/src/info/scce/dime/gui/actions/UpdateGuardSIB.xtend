/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import graphmodel.ModelElementContainer
import info.scce.dime.gui.gui.File
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GuardSIB
import info.scce.dime.gui.gui.Image
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessType

/** 
 * Context menu action, which updates a given GUI SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class UpdateGuardSIB extends DIMEGUICustomAction<GuardSIB> {
	GUI parentGUI
//	CGuardSIB cGuardSIB

	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Update GuardSIB"
	}

	/** 
	 * Checks if the reference to a GUI SIB is present and valid
	 */
	override canExecute(GuardSIB guiSib) throws ClassCastException {
		// FIXME: Workaround because getProcess method not generated for stealth import
		var Process eObj = ReferenceRegistry.instance.getEObject(guiSib.libraryComponentUID) as Process
//		var Process eObj = (guiSib.getProcess() as Process)
		if(eObj === null || eObj instanceof Process === false) return false
		if(eObj.getProcessType() !== ProcessType::FILE_DOWNLOAD_SECURITY) return false
		return true
	}

	/** 
	 * Updates the given GUI SIB.
	 * The update process creates a new GUI SIB
	 * and uses the semantics of the post create hook to
	 * consider all changes.
	 * In a second step, the data flow edges are reconnected
	 * and the out dated GUI SIB is removed, so that
	 * the new SIB is in the same position
	 */
	//TODO beautify after auto conversion to Xtend class
	//TODO create/use extension method for this stuff
	override void execute(GuardSIB guiSib) {
		// FIXME: Workaround because getProcess method not generated for stealth import
		var Process eObj = ReferenceRegistry.instance.getEObject(guiSib.libraryComponentUID) as Process
//		var EObject eObj = guiSib.getProcess()
		if (eObj === null || eObj instanceof Process === false) {
			showDialog("Reference is null", "Update failed. Reference is null!")
			return;
		}
		// Create new GUISIB
		var GuardSIB newGuardSIB = null
		var ModelElementContainer container = guiSib.getContainer()
		if (container instanceof File) {
			var File n = (container as File)
			newGuardSIB = n.newGuardSIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Image) {
			var Image n = (container as Image)
			newGuardSIB = n.newGuardSIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		parentGUI =guiSib.getRootElement()
		if(guiSib.generalStyle !==null) {
			newGuardSIB.generalStyle = guiSib.generalStyle;
		}
		guiSib.rebuildPorts(newGuardSIB);
		guiSib.delete()
		
	}

	
}
