/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.gui.gui.Alert
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.TableEntry
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Thumbnail

/** 
 * Context menu action, which updates a given GUI SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class UpdateGuiSIB extends DIMEGUICustomAction<GUISIB> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Update GuiSIB"
	}

	/** 
	 * Checks if the reference to a GUI SIB is present and valid
	 */
	override canExecute(GUISIB guiSib) {
		guiSib.gui != null
	}

	/** 
	 * Updates the given GUI SIB.
	 * The update process creates a new GUI SIB
	 * and uses the semantics of the post create hook to
	 * consider all changes.
	 * In a second step, the data flow edges are reconnected
	 * and the out dated GUI SIB is removed, so that
	 * the new SIB is in the same position
	 */
	override void execute(GUISIB guiSib) {
		guiSib.updateSIB
	}
	
	def updateSIB(GUISIB guiSIB) {
		val gui = guiSIB.gui
		if (gui == null) {
			showDialog("Referenced model is null", "Update failed. Referenced model is null!")
			return
		}
		val newSIB = guiSIB.createSIB
		if (newSIB == null) return;
		newSIB.modal = guiSIB.modal
		newSIB.name = guiSIB.name
		if(guiSIB.generalStyle !=null) {
			newSIB.generalStyle = guiSIB.generalStyle;
		}
		
		guiSIB => [
			rebuildPorts(newSIB)
			arguments.forEach[arg|
				arg.baseElements.forEach[
					moveTo(newSIB.arguments.findFirst[blockName == arg.blockName], x, y)
				]
			]
			delete
		]
	}
	
	def GUISIB createSIB(GUISIB guiSIB) {
		val gui = guiSIB.gui
		val x = guiSIB.x
		val y = guiSIB.y
		switch it : guiSIB.container {
			Alert: newGUISIB(guiSIB.gui, x, y)
			Argument: newGUISIB(gui, x, y)
			Box: newGUISIB(gui, x, y)
			Col: newGUISIB(gui, x, y)
			ExtensionContext: newGUISIB(gui, x, y)
			Form: newGUISIB(gui, x, y)
			Jumbotron: newGUISIB(gui, x, y)
			Panel: newGUISIB(gui, x, y)
			Tab: newGUISIB(gui, x, y)
			TableEntry: newGUISIB(gui, x, y)
			Template: newGUISIB(gui, x, y)
			Thumbnail: newGUISIB(gui, x, y)
			default: {
				showDialog("Unknown container", "Update failed: No handler for the SIB's container")
				return null
			}
		}
	}
	
}
