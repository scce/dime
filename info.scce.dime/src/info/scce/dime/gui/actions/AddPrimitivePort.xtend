/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.hooks.CreateComponentHook
import info.scce.dime.process.helper.LayoutConstants

/** 
 * Adds input ports to a given IF SIB
 * @author zweihoff
 */
class AddPrimitivePort extends DIMECustomAction<ControlSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"Add primitive input"
	}


	/** 
	 * Input ports can always be added
	 */
	override canExecute(ControlSIB node) throws ClassCastException {
		return true
	}

	/** 
	 * Creates a new primitive input port
	 */
	override execute(ControlSIB node) {
		val cpip = node.createPrimitivePort
		cpip.name = '''«cpip.dataType.literal.toFirstLower»«node.IOs.size»'''
		
	}
	
	def createPrimitivePort(ControlSIB cControlSIB) {
		//calculate next position
		val posY = 50+(cControlSIB.IOs.size+1)*LayoutConstants.PORT_SPACE
		val cpip = cControlSIB.newPrimitiveInputPort(LayoutConstants.PORT_X,posY)
		
		//resize SIB
		cControlSIB.resize(cControlSIB.width,cControlSIB.height+LayoutConstants.PORT_SPACE)
		//move arguments
		cControlSIB.arguments.forEach[n|{
			n.y= n.y+LayoutConstants.PORT_SPACE
		}]
		//trigger layouter
		var CreateComponentHook cch = new CreateComponentHook()
		cch.postCreate(cControlSIB)
		return cpip
	}
}
