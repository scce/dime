/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.Template

/** 
 * Context menu action to remove a template from a GUI model.
 * The context menu entry is available for templates.
 * @author zweihoff
 */
class RemoveTemplate extends DIMECustomAction<Template> {
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Remove Template"
	}

	/** 
	 * At least one template is required for a GUI model,
	 * so that all templates but one can be removed
	 */
	override canExecute(Template port) {
		!port.rootElement?.templates.nullOrEmpty
	}

	/** 
	 * Removes the given template from its GUI model
	 */
	override execute(Template template) {
		template.delete
	}
}
