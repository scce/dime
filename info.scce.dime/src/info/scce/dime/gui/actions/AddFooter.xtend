/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Template

/** 
 * Context menu action which can only be used on the GUI graphmodel.
 * Adds a footer template to the model, if no footer is already present
 * @author zweihoff
 */
class AddFooter extends DIMECustomAction<GUI> {
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"Add Footer"
	}

	//TODO beautify after auto conversion to Xtend class

	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(GUI gui) throws ClassCastException {
		return !gui.getAllNodes().stream().filter([n|n instanceof Template]).map([n|((n as Template))]).filter([n |
			n.getBlockName().equals("footer")
		]).findAny().isPresent()
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(GUI gui) {
		var Template ct = gui.newTemplate(100, 200)
		ct.setBlockName("footer")
	}
}
