/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.hooks.CreateComponentHook

/** 
 *Removes the Else argument of an IF SIB.
 * @author zweihoff
 */
class RemoveJoinFORSIB extends DIMECustomAction<FORSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"remove join"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(FORSIB node) throws ClassCastException {
		return node.arguments.exists[blockName.equals("JOIN")]
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(FORSIB node) {
		val argElse = node.arguments.findFirst[blockName.equals("JOIN")]
		val otherArgs = node.arguments.filter[!blockName.equals("JOIN")]
		if(argElse != null) {
			for(oA:otherArgs){
				// reposition arg
				if(oA.y >= argElse.y){
					//switch
					oA.y = argElse.y
				}
				
			}
			//resize sib
			val h = argElse.height
			node.resize(node.width,node.height-h)
			argElse.delete
			var CreateComponentHook cch = new CreateComponentHook()
			cch.postCreate(node)
		}
		
		
	}
}
