/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.gui.gui.Alert
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.TableEntry
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Thumbnail
import info.scce.dime.process.process.Process
import org.eclipse.emf.ecore.EObject

/** 
 * Context menu action, which updates a given Process SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class UpdateProcessSIB extends DIMEGUICustomAction<ProcessSIB> {
	
	

	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Update Process SIB"
	}

	/** 
	 * Checks if the reference to a Process SIB is present and valid
	 */
	override canExecute(ProcessSIB sib) {
//		FIXME: Workaround because getter for stealth imports are not generated
//		sib.interactionProcess != null
		ReferenceRegistry.instance.getEObject(sib.libraryComponentUID) != null
	}

	/** 
	 * Updates the given Process SIB.
	 * The update process creates a new Process SIB
	 * and uses the semantics of the post create hook to
	 * consider all changes.
	 * In a second step, the data flow edges are reconnected
	 * and the out dated Interaction SIB is removed, so that
	 * the new SIB is in the same position
	 */
	override void execute(ProcessSIB sib) {
		var EObject eObj = sib.proMod
		if (eObj === null || eObj instanceof Process === false) {
			showDialog("Reference is null", "Update failed. Reference is null!")
			return;
		}
		
		var ProcessSIB newSIB = sib.createSIB
		if (newSIB == null) return;
		
		if(sib.generalStyle !=null) {
			newSIB.generalStyle = sib.generalStyle;
		}
		if(sib.modal !=null) {
			newSIB.modal = sib.modal;
		}
		sib.rebuildPorts(newSIB);
		sib.delete()
	}
	
	def ProcessSIB createSIB(ProcessSIB sib) {
		val process = sib.proMod
		val x = sib.x
		val y = sib.y
		switch it : sib.container {
			Alert: newProcessSIB(process, x, y)
			Argument: newProcessSIB(process, x, y)
			Box: newProcessSIB(process, x, y)
			Col: newProcessSIB(process, x, y)
			Form: newProcessSIB(process, x, y)
			Jumbotron: newProcessSIB(process, x, y)
			Panel: newProcessSIB(process, x, y)
			Tab: newProcessSIB(process, x, y)
			TableEntry: newProcessSIB(process, x, y)
			Template: newProcessSIB(process, x, y)
			Thumbnail: newProcessSIB(process, x, y)
			default: {
				showDialog("Unknown container", "Update failed: No handler for the SIB's container")
				return null
			}
		}
	}
	
}
