/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.actions

import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Thumbnail
import org.eclipse.emf.ecore.EObject

/** 
 * Context menu action, which updates a given GUI plug in SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
 //TODO beautify after auto conversion to Xtend class
class UpdateGuiPlugin extends DIMEGUICustomAction<GUIPlugin> {
	GUI parentGUI
	Function refGui
	GUIPlugin sib
	EObject eObj

	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Update GuiPlugin"
	}

	/** 
	 * Checks if the reference to a GUI plug in is present and valid
	 */
	override boolean canExecute(GUIPlugin guiSib) throws ClassCastException {
		var EObject eObj = guiSib.getFunction()
		if(eObj === null || eObj instanceof Function === false) return false
		return true
	}

	/** 
	 * Updates the given GUI plug in SIB.
	 */
	override void execute(GUIPlugin guiSib) {
		eObj = guiSib.getFunction()
		if (eObj === null || eObj instanceof Function === false) {
			showDialog("Reference is null", "GUI Plugin Update failed. Reference is null!")
			return;
		}
		refGui = eObj as Function
		this.sib = guiSib
		this.parentGUI = sib.getRootElement()
//		this.guiSIB = sib
		updateSib()
	}

	/** 
	 * The update process creates a new GUI plug in SIB
	 * and uses the semantics of the post create hook to
	 * consider all changes.
	 * In a second step, the data flow edges are reconnected
	 * and the out dated GUI plug in SIB is removed, so that
	 * the new SIB is in the same position
	 */
	 //TODO create/use extension method for this stuff
	def private void updateSib() {
		sib.setLabel(refGui.getFunctionName())
		var GUIPlugin newGUIPlugin = null
		var EObject container = sib.getContainer()
		if (container instanceof Template) {
			var Template n = (container as Template)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Col) {
			var Col n = (container as Col)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Tab) {
			var Tab n = (container as Tab)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Argument) {
			var Argument n = (container as Argument)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Panel) {
			var Panel n = (container as Panel)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Jumbotron) {
			var Jumbotron n = (container as Jumbotron)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Box) {
			var Box n = (container as Box)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Form) {
			var Form n = (container as Form)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		if (container instanceof Thumbnail) {
			var Thumbnail n = (container as Thumbnail)
			newGUIPlugin = n.newGUIPlugin(eObj, sib.getX() + 1, sib.getY() + 1)
		}
		parentGUI = sib.getRootElement()
		if(sib.generalStyle !=null) {
			newGUIPlugin.generalStyle = sib.generalStyle;
		}
		sib.rebuildPorts(newGUIPlugin)
		
		if ((!sib.getArguments().isEmpty()) && (!newGUIPlugin.getArguments().isEmpty())) {
			val fg = newGUIPlugin
			sib.getArguments().forEach[e|e.getBaseElements().forEach([n|n.moveTo(fg.getArguments().findFirst[blockName.equals(e.blockName)],n.x,n.y)])]
		}
		
		sib.delete()
		
	}

}
