/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.aps;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;
import info.scce.dime.gui.gui.Alert;
import info.scce.dime.gui.gui.Badge;
import info.scce.dime.gui.gui.Bar;
import info.scce.dime.gui.gui.Button;
import info.scce.dime.gui.gui.Checkbox;
import info.scce.dime.gui.gui.Coloring;
import info.scce.dime.gui.gui.Combobox;
import info.scce.dime.gui.gui.Descriptionentry;
import info.scce.dime.gui.gui.Dropdown;
import info.scce.dime.gui.gui.Field;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.gui.Headline;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.gui.Listentry;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.PageUp;
import info.scce.dime.gui.gui.Panel;
import info.scce.dime.gui.gui.ProgressBar;
import info.scce.dime.gui.gui.Radio;
import info.scce.dime.gui.gui.Select;
import info.scce.dime.gui.gui.Text;
import info.scce.dime.gui.gui.TextStyling;
import style.Appearance;
import style.BooleanEnum;
import style.Color;
import style.Font;
import style.StyleFactory;

/**
 * The component coloring appearance provider is used to change the color of the
 * model elements in the same way the styling of this component is changed
 * 
 * @author zweihoff
 *
 */
public class ComponentColoring implements StyleAppearanceProvider<MovableContainer> {

	/**
	 * Converts a given coloring constant to RGB color
	 * 
	 * @param c
	 * @return
	 */
	private Color getColor(Coloring c) {
		Color cl = StyleFactory.eINSTANCE.createColor();
		switch (c) {
		case BLUE: {
			cl.setR(51);
			cl.setG(122);
			cl.setB(183);
			break;
		}
		case DEFAULT: {
			cl.setR(204);
			cl.setG(204);
			cl.setB(204);
			break;
		}
		case GREEN: {
			cl.setR(92);
			cl.setG(184);
			cl.setB(92);
			break;
		}
		case LIGHTBLUE: {
			cl.setR(91);
			cl.setG(192);
			cl.setB(222);
			break;
		}
		case RED: {
			cl.setR(217);
			cl.setG(83);
			cl.setB(79);
			break;
		}
		case YELLOW: {
			cl.setR(240);
			cl.setG(173);
			cl.setB(78);
			break;
		}
		}
		return cl;
	}

	/**
	 * Returns a grey style color
	 * 
	 * @return
	 */
	private Color grey() {
		Color cl = StyleFactory.eINSTANCE.createColor();
		cl.setR(214);
		cl.setG(215);
		cl.setB(212);
		return cl;
	}

	/**
	 * Returns a black style color
	 * 
	 * @return
	 */
	private Color black() {
		Color cl = StyleFactory.eINSTANCE.createColor();
		cl.setB(0);
		cl.setG(0);
		cl.setR(0);
		return cl;
	}

	/**
	 * Returns a white style color
	 * 
	 * @return
	 */
	private Color white() {
		Color cl = StyleFactory.eINSTANCE.createColor();
		cl.setB(255);
		cl.setG(255);
		cl.setR(255);
		return cl;
	}

	/**
	 * Returns the style color for a given navigation bar component depended on its
	 * inverted status.
	 * 
	 * @return
	 */
	private Color getNavColor(Bar bar) {
		Color cl = StyleFactory.eINSTANCE.createColor();
		if (!bar.isInverted()) {
			cl.setR(248);
			cl.setG(248);
			cl.setB(248);
		} else {
			cl.setR(34);
			cl.setG(34);
			cl.setB(34);
		}
		return cl;
	}

	private Font getFont(TextStyling styling) {
		Font font = StyleFactory.eINSTANCE.createFont();
		if (styling.isBold()) {
			font.setIsBold(true);
		} else {
			font.setIsBold(false);
		}
		if (styling.isItalic()) {
			font.setIsItalic(true);
		} else {
			font.setIsItalic(false);
		}
		return font;
	}

	/**
	 * Returns the modified appearance for a given movable container. The type of
	 * the movable container is checked and if it can be styled by a color constant,
	 * the appearance is extended by the selected color
	 */
	@Override
	public Appearance getAppearance(MovableContainer mc, String s) {
		Appearance app = StyleFactory.eINSTANCE.createAppearance();
		app.setFilled(BooleanEnum.TRUE);
		app.setLineInVisible(false);
		app.setLineWidth(2);
		app.setTransparency(0);
		if (mc instanceof Button) {
			Button e = (Button) mc;
			if (s.equals("buttonColor")) {
				Coloring c = Coloring.DEFAULT;
				if (e.getStyling() != null) {
					c = e.getStyling().getColor();
				}
				if (c == Coloring.DEFAULT) {
					app.setBackground(white());
				} else {
					app.setBackground(getColor(c));
				}

				app.setForeground(getColor(c));
			}
			if (s.equals("buttonText")) {
				app.setFilled(BooleanEnum.FALSE);
				Coloring c = Coloring.DEFAULT;
				if (e.getStyling() != null) {
					c = e.getStyling().getColor();
				}
				if (c == Coloring.DEFAULT) {
					app.setForeground(black());
					app.setBackground(white());
				} else {
					app.setForeground(white());
					app.setBackground(getColor(c));
				}
			}
			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			return app;

		}
		if (mc instanceof Panel) {
			Panel e = (Panel) mc;
			Coloring c = Coloring.DEFAULT;
			if (e.getColoring() != null) {
				c = e.getColoring().getColor();
			}
			if (s.startsWith("panelBorder")) {
				app.setBackground(white());
				app.setForeground(getColor(c));
				return app;
			}
			if (s.startsWith("panelColor")) {
				app.setBackground(getColor(c));
				app.setForeground(getColor(c));
				return app;
			}
			if (s.startsWith("panelText")) {
				app.setBackground(getColor(c));
				app.setFilled(BooleanEnum.FALSE);
				if (c == Coloring.DEFAULT) {
					app.setForeground(black());
				} else {
					app.setForeground(white());
				}
				return app;
			}
			if (s.startsWith("pre2")) {
				if(e.getFooter() != null) {
					if (e.getFooter().getIcon() != null) {
						String literal = e.getFooter().getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			} else if (s.startsWith("pre")) {
				if(e.getHeading() != null) {
					if (e.getHeading().getIcon() != null) {
						String literal = e.getHeading().getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("post2")) {
				if(e.getFooter() != null) {
					if (e.getFooter().getIcon() != null) {
						String literal = e.getFooter().getIcon().getPostIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			} else if (s.startsWith("post")) {
				if(e.getHeading() != null) {
					if (e.getHeading().getIcon() != null) {
						String literal = e.getHeading().getIcon().getPostIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			app.setBackground(white());
			app.setForeground(grey());
			return app;
		}
		if (mc instanceof Bar) {
			Bar e = (Bar) mc;
			if (s.startsWith("barColor")) {
				app.setBackground(getNavColor(e));
				app.setForeground(getNavColor(e));
			}
			if (s.startsWith("barText")) {
				app.setFilled(BooleanEnum.FALSE);
				app.setBackground(getNavColor(e));
				if (!e.isInverted()) {
					app.setForeground(black());
				} else {
					app.setForeground(white());
				}
			}
			return app;
		}
		if (mc instanceof Text) {
			Text e = (Text) mc;
			if (s.startsWith("textText")) {
				app.setFilled(BooleanEnum.FALSE);
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getStyling() != null) {
						if (e.getContent().get(0).getStyling().getColor().getLiteral() == "Default") {
							app.setForeground(black());
						} else {
							app.setForeground(getColor(e.getContent().get(0).getStyling().getColor()));
						}
						Font font = getFont(e.getContent().get(0).getStyling());
						font.setFontName("Helvetica");
						font.setSize(10);
						app.setFont(font);
					}
				} else {
					app.setForeground(black());
				}
				app.setBackground(white());
				return app;
			}
			if (s.startsWith("pre")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("post")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPostIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			app.setForeground(white());
			app.setBackground(white());
			return app;

		}
		if (mc instanceof Alert) {
			Alert e = (Alert) mc;
			Coloring c = Coloring.DEFAULT;
			if (e.getColoring() != null) {
				c = e.getColoring().getColor();
			}
			if (s.startsWith("alertColor")) {
				app.setBackground(getColor(c));
				app.setForeground(getColor(c));
			}
			if (s.startsWith("alertText")) {
				app.setFilled(BooleanEnum.FALSE);
				app.setBackground(getColor(c));
				if (c == Coloring.DEFAULT) {
					app.setForeground(black());
				} else {
					app.setForeground(white());
				}
			}
			return app;
		}
		if (mc instanceof ProgressBar) {
			ProgressBar e = (ProgressBar) mc;
			Coloring c = Coloring.DEFAULT;
			if (e.getStyling() != null) {
				c = e.getStyling().getColor();
			}
			if (s.startsWith("progressBarColor")) {
				app.setBackground(getColor(c));
				app.setForeground(getColor(c));
			}
			if (s.startsWith("progressBarText")) {
				app.setBackground(getColor(c));
				app.setFilled(BooleanEnum.FALSE);
				if (c == Coloring.DEFAULT) {
					app.setForeground(black());
				} else {
					app.setForeground(white());
				}
			}
			return app;
		}
		if (mc instanceof Field) {
			Field e = (Field) mc;
			Coloring c = Coloring.DEFAULT;
			if (e.getStyling() != null) {
				c = e.getStyling().getColor();
			}
			if (s.startsWith("fieldColor")) {
				app.setBackground(white());
				app.setForeground(getColor(c));
				return app;
			}
			if (s.startsWith("fieldText")) {
				app.setFilled(BooleanEnum.FALSE);
				if (c == Coloring.DEFAULT) {
					app.setBackground(white());
					app.setForeground(black());
					return app;
				} else {
					app.setBackground(white());
					app.setForeground(getColor(c));
					return app;
				}
			}
			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			app.setBackground(white());
			app.setBackground(white());
			return app;
		}
		if (mc instanceof Select) {
			Select e = (Select) mc;
			Coloring c = Coloring.DEFAULT;
			if (e.getStyling() != null) {
				c = e.getStyling().getColor();
			}
			if (s.startsWith("inner")) {
				app.setBackground(black());
				app.setForeground(black());
				return app;
			}
			if (s.startsWith("radioCircle")) {
				app.setBackground(white());
				app.setForeground(grey());
				return app;
			}
			if (s.startsWith("radioColor")) {
				app.setBackground(getColor(c));
				app.setForeground(getColor(c));
				return app;
			}
			if (s.startsWith("radioText")) {
				app.setBackground(white());
				app.setFilled(BooleanEnum.FALSE);
				if (c == Coloring.DEFAULT) {
					app.setForeground(black());
				} else {
					app.setForeground(getColor(c));
				}
				return app;
			}
			app.setBackground(white());
			app.setForeground(white());
		}
		if (mc instanceof Radio) {
			Radio e = (Radio) mc;
			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
		}
		if (mc instanceof Checkbox) {
			Checkbox e = (Checkbox) mc;
			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
		}
		if (mc instanceof Radio) {
			Radio e = (Radio) mc;
			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
		}
		if (mc instanceof Combobox) {
			Combobox e = (Combobox) mc;
			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
		}
		if (mc instanceof Dropdown) {
			// visualize drop up/down
			Dropdown e = (Dropdown) mc;
			if (s.startsWith("arrow")) {
				if (!e.isDropUp()) {
					app.setImagePath("/icons/gui/ArrowDown.png");
				} else {
					app.setImagePath("/icons/gui/ArrowUp.png");
				}
			}

			// change fore/backgroudn
			Coloring c = Coloring.DEFAULT;
			if (e.getStyling() != null) {
				c = e.getStyling().getColor();
			}
			if (s.startsWith("dropDownRectangleInnerUpper")) {
				app.setBackground(getColor(c));
			}
			if (s.startsWith("dropDownRectangle")) {
				app.setBackground(getColor(c));
			}
			if (s.startsWith("dropdownText")) {
				if (c == Coloring.DEFAULT) {
					app.setForeground(black());
				} else {
					app.setForeground(white());
				}
			}

			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			return app;
		}
		if (mc instanceof Headline) {
			Headline e = (Headline) mc;
			if (s.startsWith("headlineText")) {
				app.setFilled(BooleanEnum.FALSE);
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getStyling() != null) {
						if (e.getContent().get(0).getStyling().getColor().getLiteral() == "Default") {
							app.setForeground(black());
						} else {
							app.setForeground(getColor(e.getContent().get(0).getStyling().getColor()));
						}
						Font font = getFont(e.getContent().get(0).getStyling());
						font.setFontName("Helvetica");
						font.setSize(14);
						app.setFont(font);
					}

				} else {
					app.setForeground(black());
				}
				app.setBackground(white());
				return app;
			}
			if (s.startsWith("headlineLine")) {
				app.setFilled(BooleanEnum.FALSE);
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getStyling() != null)
						app.setForeground(getColor(e.getContent().get(0).getStyling().getColor()));
				} else {
					app.setForeground(black());
				}
				app.setBackground(white());
				return app;
			}
			if (s.startsWith("pre")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("post")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPostIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			app.setForeground(white());
			app.setBackground(white());
			return app;
		}
		if (mc instanceof Listentry) {
			Listentry e = (Listentry) mc;
			if (s.startsWith("list")) {
				app.setFilled(BooleanEnum.FALSE);
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getStyling() != null) {
						if (e.getContent().get(0).getStyling().getColor().getLiteral() == "Default") {
							app.setForeground(black());
						} else {
							app.setForeground(getColor(e.getContent().get(0).getStyling().getColor()));
						}
						Font font = getFont(e.getContent().get(0).getStyling());
						font.setFontName("Helvetica");
						font.setSize(10);
						app.setFont(font);
					}

				} else {
					app.setForeground(black());
				}
				app.setBackground(white());
				return app;
			}
			if (s.startsWith("pre")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("post")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPostIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			app.setForeground(white());
			app.setBackground(white());
			return app;
		}
		if (mc instanceof Descriptionentry) {
			Descriptionentry e = (Descriptionentry) mc;
			if (s.startsWith("description")) {
				app.setFilled(BooleanEnum.FALSE);
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getStyling() != null) {
						if (e.getDescription().get(0).getStyling().getColor().getLiteral() == "Default") {
							app.setForeground(black());
						} else {
							app.setForeground(getColor(e.getDescription().get(0).getStyling().getColor()));
						}
						Font font = getFont(e.getDescription().get(0).getStyling());
						font.setFontName("Helvetica");
						font.setSize(12);
						app.setFont(font);
					}

				} else {
					app.setForeground(black());
				}
				app.setBackground(white());
				return app;
			}
			if (s.startsWith("entry")) {
				app.setFilled(BooleanEnum.FALSE);
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getStyling() != null) {
						if (e.getContent().get(0).getStyling().getColor().getLiteral() == "Default") {
							app.setForeground(black());
						} else {
							app.setForeground(getColor(e.getContent().get(0).getStyling().getColor()));
						}
						Font font = getFont(e.getContent().get(0).getStyling());
						font.setFontName("Helvetica");
						font.setSize(10);
						app.setFont(font);
					}
				} else {
					app.setForeground(black());
				}
				app.setBackground(white());
				return app;
			}
			if (s.startsWith("pre1")) {
				if (!e.getContent().isEmpty()) {
					if (e.getDescription().get(0).getIcon() != null) {
						String literal = e.getDescription().get(0).getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("post1")) {
				if (!e.getContent().isEmpty()) {
					if (e.getDescription().get(0).getIcon() != null) {
						String literal = e.getDescription().get(0).getIcon().getPostIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("pre2")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("post2")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPostIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			app.setForeground(white());
			app.setBackground(white());
			return app;
		}
		if (mc instanceof File) {
			File e = (File) mc;
			if (s.startsWith("downloadText")) {
				if(e.getStyling() != null) {
					if (e.getStyling().getColor().getLiteral() == "Default") {
						app.setForeground(black());
					} else {
						app.setForeground(white());
					}
				}
				return app;
			}
			if (s.startsWith("downloadRectangle")) {
				app.setFilled(BooleanEnum.FALSE);
				if(e.getStyling() != null) {
					app.setForeground(getColor(e.getStyling().getColor()));
					app.setBackground(getColor(e.getStyling().getColor()));
				}
				return app;
			}
			if (s.startsWith("pre")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPreIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			if (s.startsWith("post")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			app.setBackground(white());
			return app;
		}
		if (mc instanceof Badge) {
			Badge e = (Badge) mc;
			if (s.startsWith("badgeText")) {
				app.setFilled(BooleanEnum.FALSE);
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getStyling() != null) {
						if (e.getContent().get(0).getStyling().getColor().getLiteral() == "Default") {
							app.setForeground(black());
						} else {
							app.setForeground(getColor(e.getContent().get(0).getStyling().getColor()));
						}
						Font font = getFont(e.getContent().get(0).getStyling());
						font.setFontName("Helvetica");
						font.setSize(12);
						app.setFont(font);
					}
				} else {
					app.setForeground(black());
				}
				app.setBackground(white());
				return app;
			}
			if (s.startsWith("badgeOutline")) {
				app.setFilled(BooleanEnum.FALSE);
				app.setForeground(getColor(e.getColor()));
				app.setBackground(getColor(e.getColor()));
				return app;
			}

			if (s.startsWith("pre")) {
				if (!e.getContent().isEmpty()) {
					if (e.getContent().get(0).getIcon() != null) {
						String literal = e.getContent().get(0).getIcon().getPreIcon().getLiteral();
						app.setImagePath("/icons/gui/" + literal + ".png");
						return app;
					}
				}
			}
			if (s.startsWith("post")) {
				if (!e.getContent().isEmpty()) {
				if (e.getContent().get(0).getIcon()  != null) {
					String literal = e.getContent().get(0).getIcon().getPostIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
				}
			}
			app.setBackground(white());
			return app;
		}

		// for image visualize border
		if (mc instanceof Image) {
			Image e = (Image) mc;
			if (s.startsWith("image")) {
				if (e.getStyling() != null) {
					String literal = e.getStyling().getBorder().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
			}
			return app;
		}
		if (mc instanceof PageUp) {
			PageUp e = (PageUp) mc;
			if (s.startsWith("image")) {
				if (e.getIcon() != null) {
					String literal = e.getIcon().getLiteral();
					app.setImagePath("/icons/gui/" + literal + ".png");
					return app;
				}
				return app;
			}
			return app;
		}
		return app;
	}

}
