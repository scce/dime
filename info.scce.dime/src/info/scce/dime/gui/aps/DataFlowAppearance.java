/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.aps;

import java.util.Optional;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;
import graphmodel.ModelElement;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexExtensionAttribute;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.gui.PrimitiveExtensionAttribute;
import info.scce.dime.gui.gui.Read;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.gui.Write;
import style.Appearance;
import style.BooleanEnum;
import style.Color;
import style.LineStyle;
import style.StyleFactory;

/**
 * The data flow appearance provider is used to change the color of a variable
 * placed in the data context, if the variable is defined as an input variable.
 * The color of the variable is changed from green to input port yellow
 * 
 * @author zweihoff
 *
 */
public class DataFlowAppearance implements StyleAppearanceProvider<ModelElement> {

	/**
	 * Checks if the given model element is a variable and whether it is an input
	 * variable and changes its color.
	 */
	@Override
	public Appearance getAppearance(ModelElement e, String element) {

		Appearance appearance = StyleFactory.eINSTANCE.createAppearance();
		Color cl = StyleFactory.eINSTANCE.createColor();
		cl.setR(182);
		cl.setG(211);
		cl.setB(207);
		appearance.setBackground(cl);
		appearance.setFilled(BooleanEnum.TRUE);
		appearance.setTransparency(0.0);

		if (e instanceof Variable) {
			if (((Variable) e).isIsInput()) {
				Appearance input = StyleFactory.eINSTANCE.createAppearance();
				input.setFilled(BooleanEnum.TRUE);
				input.setLineInVisible(false);
				input.setLineWidth(1);
				input.setTransparency(0);
				Color cl1 = StyleFactory.eINSTANCE.createColor();
				cl1.setR(255);
				cl1.setG(158);
				cl1.setB(0);
				input.setBackground(cl1);
				return input;
			}
		}

		if (e instanceof ComplexExtensionAttribute || e instanceof PrimitiveExtensionAttribute) {
			Appearance input = StyleFactory.eINSTANCE.createAppearance();
			input.setFilled(BooleanEnum.TRUE);
			input.setLineInVisible(false);
			input.setLineWidth(1);
			input.setTransparency(0);
			Color cl1 = StyleFactory.eINSTANCE.createColor();
			cl1.setR(202);
			cl1.setG(202);
			cl1.setB(202);
			input.setBackground(cl1);
			return input;
		}

		if (e instanceof ComplexAttribute || e instanceof PrimitiveAttribute) {
			Appearance input = StyleFactory.eINSTANCE.createAppearance();
			input.setFilled(BooleanEnum.TRUE);
			input.setLineInVisible(false);
			input.setLineWidth(1);
			input.setTransparency(0);
			Color cl1 = StyleFactory.eINSTANCE.createColor();
			cl1.setR(210);
			cl1.setG(239);
			cl1.setB(235);
			input.setBackground(cl1);
			return input;
		}

		if (e instanceof ComplexVariable) {
			if (!((Variable) e).isIsInput()) {
				// check if the variable is complex attribute
				ComplexVariable cv = (ComplexVariable) e;
				if (!cv.getIncomingComplexAttributeConnectors().isEmpty()) {
					// check if the variable corresponds to an extension attribute
					ComplexVariable parent = cv.getComplexVariablePredecessors().get(0);
					String attr = cv.getIncomingComplexAttributeConnectors().get(0).getAssociationName();
					Optional<Attribute> attrOpt = parent.getDataType().getAttributes().stream()
							.filter(n -> n.getName().equals(attr)).findAny();
					if (attrOpt.isPresent() && attrOpt.get() instanceof ExtensionAttribute) {
						// complex variable corresponds to an extension attribute
						Appearance input = StyleFactory.eINSTANCE.createAppearance();
						input.setFilled(BooleanEnum.TRUE);
						input.setLineInVisible(false);
						input.setLineWidth(1);
						input.setTransparency(0);
						Color cl1 = StyleFactory.eINSTANCE.createColor();
						cl1.setR(202);
						cl1.setG(202);
						cl1.setB(202);
						input.setBackground(cl1);
						return input;
					}
					// variable corresponds to an attribute
					Appearance input = StyleFactory.eINSTANCE.createAppearance();
					input.setFilled(BooleanEnum.TRUE);
					input.setLineInVisible(false);
					input.setLineWidth(1);
					input.setTransparency(0);
					Color cl1 = StyleFactory.eINSTANCE.createColor();
					cl1.setR(210);
					cl1.setG(239);
					cl1.setB(235);
					input.setBackground(cl1);
					return input;
				}

			}
		}

		if (e instanceof Read) {
			Appearance app = StyleFactory.eINSTANCE.createAppearance();
			app.setLineStyle(LineStyle.SOLID);
			app.setLineWidth(1);
			app.setForeground(getColor("GREY"));
			
			return app;
		} 
		if (e instanceof Write) {
			Appearance app = StyleFactory.eINSTANCE.createAppearance();
			app.setLineStyle(LineStyle.SOLID);
			app.setLineWidth(1);
			app.setForeground(getColor("ORANGE"));
			return app;
		}
		
		return appearance;
	}

	private Color getColor(String color) {
		Color cl = StyleFactory.eINSTANCE.createColor();
		switch (color) {
		case "GREY": {
			cl.setR(150);
			cl.setG(150);
			cl.setB(150);
			break;
		}
		case "ORANGE": {
			cl.setR(170);
			cl.setG(110);
			cl.setB(0);
			break;
		}
		}
		return cl;
	}

}
