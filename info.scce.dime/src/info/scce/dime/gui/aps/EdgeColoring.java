/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.aps;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;
import graphmodel.Edge;
import info.scce.dime.gui.gui.AddComplexToSubmission;
import info.scce.dime.gui.gui.AddPrimitiveToSubmission;
import info.scce.dime.gui.gui.AddToSubmission;
import info.scce.dime.gui.gui.ChoiceData;
import info.scce.dime.gui.gui.ComplexChoiceData;
import info.scce.dime.gui.gui.ComplexDataTarget;
import info.scce.dime.gui.gui.DataBinding;
import info.scce.dime.gui.gui.DataTarget;
import info.scce.dime.gui.gui.Display;
import info.scce.dime.gui.gui.FOR;
import info.scce.dime.gui.gui.IF;
import info.scce.dime.gui.gui.IS;
import info.scce.dime.gui.gui.PrimitivDataTarget;
import info.scce.dime.gui.gui.PrimitiveChoiceData;
import info.scce.dime.gui.gui.PrimitiveFOR;
import info.scce.dime.gui.gui.Registration;
import style.Appearance;
import style.Color;
import style.LineStyle;
import style.StyleFactory;

public class EdgeColoring implements StyleAppearanceProvider<Edge> {

	/**
	 * Converts a given coloring constant to RGB color
	 * 
	 * @param c
	 * @return
	 */
	private Color getColor(String color) {
		Color cl = StyleFactory.eINSTANCE.createColor();
		switch (color) {
		case "BLUE": {
			cl.setR(51);
			cl.setG(122);
			cl.setB(183);
			break;
		}
		case "DEFAULT": {
			cl.setR(204);
			cl.setG(204);
			cl.setB(204);
			break;
		}
		case "GREEN": {
			cl.setR(92);
			cl.setG(184);
			cl.setB(92);
			break;
		}
		case "LIGHTBLUE": {
			cl.setR(60);
			cl.setG(140);
			cl.setB(160);
			break;
		}
		case "RED": {
			cl.setR(217);
			cl.setG(83);
			cl.setB(79);
			break;
		}
		case "YELLOW": {
			cl.setR(240);
			cl.setG(200);
			cl.setB(78);
			break;
		}
		case "COMPLEXGREEN": {
			cl.setR(118);
			cl.setG(173);
			cl.setB(165);
			break;
		}
		case "PRIMORANGE": {
			cl.setR(170);
			cl.setG(110);
			cl.setB(0);
			break;
		}
		}
		return cl;
	}

	/**
	 * Returns a grey style color
	 * 
	 * @return
	 */
	private Color grey() {
		Color cl = StyleFactory.eINSTANCE.createColor();
		cl.setR(214);
		cl.setG(215);
		cl.setB(212);
		return cl;
	}

	/**
	 * Returns a black style color
	 * 
	 * @return
	 */
	private Color black() {
		Color cl = StyleFactory.eINSTANCE.createColor();
		cl.setB(0);
		cl.setG(0);
		cl.setR(0);
		return cl;
	}

	@Override
	public Appearance getAppearance(Edge edge, String s) {
		Appearance app = StyleFactory.eINSTANCE.createAppearance();
		app.setLineWidth(2);

		if (edge instanceof DataBinding) {
			app.setForeground(getColor("COMPLEXGREEN"));
			return app;
		}
		if (edge instanceof PrimitiveFOR) {
			app.setForeground(getColor("PRIMORANGE"));
			app.setLineStyle(LineStyle.DOT);
			return app;
		}

		if (edge instanceof IF) {
			IF if_edge = (IF) edge;
			if (if_edge.isNegate()) {
				app.setForeground(getColor("RED"));
			} else {
				app.setForeground(getColor("GREEN"));
			}
			return app;
		}
		if (edge instanceof Display) {
			app.setForeground(getColor("YELLOW"));
			return app;
		}

		if (edge instanceof AddToSubmission) {
			app.setLineStyle(LineStyle.SOLID);
			app.setForeground(getColor("PRIMORANGE"));
			return app;
		}

		if (edge instanceof FOR) {
			app.setForeground(getColor("BLUE"));
			app.setLineStyle(LineStyle.DASHDOT);
			return app;
		}

		if (edge instanceof ChoiceData) {
			app.setForeground(getColor("PRIMORANGE"));
			return app;
		}

		if (edge instanceof DataTarget) {
			app.setForeground(getColor("PRIMORANGE"));
			return app;
		}
		return app;

	}

}

