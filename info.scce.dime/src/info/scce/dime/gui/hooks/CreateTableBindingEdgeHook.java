/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks;


import graphmodel.Edge;
import graphmodel.Node;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeName;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataBinding;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.gui.Table;
import info.scce.dime.gui.gui.TableEntry;
import info.scce.dime.gui.gui.TableLoad;
import info.scce.dime.gui.gui.Variable;

/**
 * The create table binding edge hook is used to create
 * the columns for every attribute of the source complex variable
 * @author zweihoff
 *
 */
public class CreateTableBindingEdgeHook extends DIMEPostCreateHook<DataBinding>{

	/**
	 * Creates the columns for every attribute of the source complex variable
	 *
	 */
	@Override
	public void postCreate(DataBinding object) {
		Node target = object.getTargetElement();
		Node source = object.getSourceElement();
		GUI cGUI = object.getRootElement();
		if(object instanceof TableLoad && target instanceof Table && source instanceof Variable){
			TableLoad tableLoad = (TableLoad) object;
			if(((Variable)source).isIsList())return;
			if(!checkValidVariable((Variable)source, tableLoad, cGUI)){
				tableLoad.delete();
				return;
			}
			
			Table table = (Table) target;			
			Variable rootVar = getIteratingVariable((Variable) tableLoad.getSourceElement());
			
			if(rootVar == null) return;
			
			if(!rootVar.equals(source)) {
				tableLoad.delete();
			}
			
			
			if(rootVar instanceof ComplexVariable) {
				ComplexVariable ccv = (ComplexVariable)rootVar;
				ccv.newTableLoad(table);
			}
			
			if(source instanceof ComplexVariable) {
				
				ComplexVariable cv = (ComplexVariable) source;
				
				for(Attribute attribute:cv.getAttributes()) {
					if(attribute instanceof PrimitiveAttribute){
						createColumn(cGUI, table, (PrimitiveAttribute)attribute, tableLoad);
					}
					else if(attribute instanceof ComplexAttribute){
						createColumn(cGUI, table, (ComplexAttribute)attribute, tableLoad);
					}
				}
			}
		}
		
	}
	
	/**
	 * Returns the list variable 
	 * @param var
	 * @return
	 */
	private Variable getIteratingVariable(Variable var)
	{
		if(var.isIsList())return var;
		
		for(Edge edge:var.getIncoming()) {
			if(edge instanceof ComplexAttributeConnector) {
				return getIteratingVariable((Variable) edge.getSourceElement());
			}
			else if(edge instanceof ComplexListAttributeConnector) {
				ComplexListAttributeConnector clac = (ComplexListAttributeConnector)edge;
				if(clac.getAttributeName() == ComplexListAttributeName.CURRENT) {
					//Create Tableload on the root list variable
					return (Variable) clac.getSourceElement();
				}
				return getIteratingVariable((Variable) edge.getSourceElement());
			}
			
		}
		return null;
	}
	
	/**
	 * Checks, if the given variable is valid
	 * @param variable
	 * @param tl
	 * @param gui
	 * @return
	 */
	private boolean checkValidVariable(Variable variable, TableLoad tl,GUI gui)
	{
		if(variable.isIsList())
		{
			return true;
		}
		for(ComplexListAttributeConnector clac:variable.getIncoming(ComplexListAttributeConnector.class)) {
			if(clac.getAttributeName() == ComplexListAttributeName.CURRENT) {
				return true;
			}
			else
			{
				return checkValidVariable((Variable) clac.getSourceElement(), tl, gui);
			}
		}
		for(ComplexAttributeConnector clac:variable.getIncoming(ComplexAttributeConnector.class)) {
			return checkValidVariable((Variable) clac.getSourceElement(), tl, gui);
		}
		return false;
	}
	
	/**
	 * Creates a new column and connects it to the given attribute
	 * @param cGUI
	 * @param cTable
	 * @param pa
	 * @param object
	 */
	private void createColumn(GUI cGUI,Table cTable,Attribute pa,TableLoad object)
	{
		if(pa instanceof PrimitiveAttribute){
			PrimitiveAttribute gpa = (PrimitiveAttribute)pa;
			info.scce.dime.data.data.PrimitiveAttribute dpa = gpa.getAttribute();			
			TableEntry entry = cTable.newTableEntry(0, 0);
			entry.setLabel(convertLabel(dpa.getName()));
			gpa.newTableColumnLoad(entry);
		}
		if(pa instanceof ComplexAttribute){
			ComplexAttribute gca = (ComplexAttribute)pa;
			info.scce.dime.data.data.ComplexAttribute dca = gca.getAttribute();
			
			TableEntry ctce = cTable.newTableEntry(0, 0);
			ctce.setLabel(convertLabel(dca.getName()));
			gca.newTableColumnLoad(ctce);
		}
		
	}
	
	/**
	 * Returns the table column label depended on the given variable or attribute name
	 * @param label
	 * @return
	 */
	private String convertLabel(String label)
	{
		String humanReadable = label.replaceAll(
			      String.format("%s|%s|%s",
			         "(?<=[A-Z])(?=[A-Z][a-z])",
			         "(?<=[^A-Z])(?=[A-Z])",
			         "(?<=[A-Za-z])(?=[^A-Za-z])"
			      ),
			      " "
			   );
		return Character.toUpperCase(humanReadable.charAt(0)) + humanReadable.substring(1);
	}
	

}
