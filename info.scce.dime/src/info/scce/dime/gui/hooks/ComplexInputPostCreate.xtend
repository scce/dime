/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks

import graphmodel.Node
import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.process.helper.LayoutConstants

/** 
 * Adds input ports to a given IF SIB
 * @author zweihoff
 */
class ComplexInputPostCreate extends DIMEPostCreateHook<ComplexInputPort> {
	
	override postCreate(ComplexInputPort node) {
		if(node.container instanceof ControlSIB){
			
			
			val cIf = node.container as ControlSIB
			node.name = '''«node.dataType.name.toFirstLower»«cIf.IOs.size»'''
			node.width = (node.name.length + node.dataType.name.length+2) * 7
			//calculate next position
			val posY = 50+(cIf.IOs.size)*LayoutConstants.PORT_SPACE
			//move port to position
			node.moveTo(cIf,LayoutConstants.PORT_X,posY)
			//resize SIB
			cIf.resize(cIf.width,cIf.height+LayoutConstants.PORT_SPACE)
			//move arguments
			cIf.arguments.forEach[n|{
				n.y= n.y+LayoutConstants.PORT_SPACE
			}]
			//trigger layouter
			var CreateComponentHook cch = new CreateComponentHook()
			//cch.postCreate(node)
			cch.postCreate(cIf)
		}
		else{
			//cCIP.delete
		}
		
	}
	
}
