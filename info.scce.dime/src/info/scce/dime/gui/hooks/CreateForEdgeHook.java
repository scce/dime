/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks;

import graphmodel.Node;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttribute;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeName;
import info.scce.dime.gui.gui.Iteration;
import info.scce.dime.gui.gui.Variable;

/**
 * The create for edge hook is used to avoid the
 * creation of iteration edges from a non list variable or attribute
 * @author zweihoff
 *
 */
public class CreateForEdgeHook extends DIMEPostCreateHook<Iteration>{

	/**
	 * Avoids the creation of iteration edges from a non list variable or attribute
	 */
	@Override
	public void postCreate(Iteration object) {
		checkIndex(object);
	}
	
	/**
	 * Sets the index attribute of the iteration edge, if it iterates over primitive values
	 * @param iteration
	 */
	private void checkIndex(Iteration iteration)
	{
		if(isPartOfIteration(iteration.getSourceElement())){
			iteration.setIndex("");
		}
	}
	
	
	/**
	 * Checks, if the given node which can be a variable or attribute
	 * is a list
	 * @param node
	 * @return
	 */
	private boolean isPartOfIteration(Node node)
	{
		if(node instanceof Attribute){
			if(node instanceof ComplexListAttribute) {
				ComplexListAttribute ccla = (ComplexListAttribute)node;
				if(ccla.getAttributeName() == ComplexListAttributeName.CURRENT){
					return true;
				}
			}
			if(node.getContainer() instanceof Variable){
				return isPartOfIteration((Node)node.getContainer());
			}
		}
		else if(node instanceof Variable)
		{
			Variable cv = (Variable)node;
			for(ComplexListAttributeConnector clac: cv.getIncoming(ComplexListAttributeConnector.class))
			{
				if(clac.getAttributeName() == ComplexListAttributeName.CURRENT)return true;
				return isPartOfIteration(clac.getSourceElement());
			}
			for(ComplexAttributeConnector clac: cv.getIncoming(ComplexAttributeConnector.class))
			{
				return isPartOfIteration(clac.getSourceElement());
			}
		}
		return false;
	}

}
