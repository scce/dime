/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.gui.actions.AddPrimitivePort
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.ISSIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process

/** 
 * The IF SIB hook is used create the corresponding arguments for the
 * corresponding control SIB
 * @author zweihoff
 */
class ControlSIBHook extends DIMEPostCreateHook<ControlSIB> {
	/** 
	 * Creates the corresponding GUI SIB input ports for every input variable
	 * of the referenced GUI model.
	 * For every button and branch of embedded SIBs, a branch with the corresponding ports is added.
	 */
	override void postCreate(ControlSIB sib) {
		if(sib instanceof IFSIB){
			val arg = sib.newArgument(10,70)
			arg.blockName = "THEN"
		}
		if(sib instanceof ISSIB){
			val arg = sib.newArgument(10,70)
			arg.blockName = "THEN"
		}
		if(sib instanceof FORSIB){
			val arg = sib.newArgument(10,70)
			arg.blockName = "FOR"
		}
		if(sib instanceof SecuritySIB){
			val arg = sib.newArgument(10,70)
			arg.blockName = "GRANTED"
		}
		// Trigger Layouter
		layout(sib)
		//After layout actions
		if(sib instanceof ISSIB){
			val port =sib.newComplexInputPort((sib as ISSIB).dataType,0,0)
			port.name="condition"
		}
		if(sib instanceof FORSIB){
			val port =sib.newComplexInputPort((sib as FORSIB).dataType,0,0)
			port.isList = true
			port.name="iteration"
		}
		if(sib instanceof SecuritySIB){
			val secSib = sib 
			val process = secSib.proMod as Process
			
			
			process.startSIBs.get(0).outputs.filter(PrimitiveOutputPort).forEach[n|{
				sib.createPrimitive(n)
			}]
			
			process.startSIBs.get(0).outputs.filter(ComplexOutputPort).forEach[n|{
				sib.createComplex(n)
			}]
			
		}
	}
	
	def createPrimitive(SecuritySIB cSib,PrimitiveOutputPort n){
		val cpip = new AddPrimitivePort().createPrimitivePort(cSib)
		cpip.dataType = n.dataType.toData.toGUI
		cpip.name = n.name
		cpip.isList = n.isList
	}
	
	def createComplex(SecuritySIB cSib,ComplexOutputPort n){
		val port =cSib.newComplexInputPort(n.dataType,0,0)
		port.name = n.name
		port.isList = n.isList
	}

	def void layout(ControlSIB sib) {
		var CreateComponentHook cch = new CreateComponentHook()
		cch.postCreate(sib)
	}

}
