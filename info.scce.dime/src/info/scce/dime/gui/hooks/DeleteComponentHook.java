/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks;

import java.util.ArrayList;

import graphmodel.Container;
import graphmodel.GraphModel;
import graphmodel.Node;
import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.gui.gui.Branch;
import info.scce.dime.gui.gui.IO;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.Template;
import info.scce.dime.gui.helper.ElementCollector;

/**
 * The delete component hook is used to layout and reposition the components
 * in all parent components of the deleted component
 * @author zweihoff
 *
 */
public class DeleteComponentHook extends DIMEPreDeleteHook<MovableContainer>{

	public static int V_OFFSET_MIN = 30;
	public static int H_OFFSET_MIN = 10;
	public static int H_OFFSET = 10;
	public static int V_OFFSET = 10;
	public static int WIDTH_MIN = 100;
	
	public static int SIGN_1_X = 2;
	public static int SIGN_2_X = 17;
	public static int SIGN_Y = 2;
	
	/**
	 * Layouts and reposition the components
	 * in all parent components of the deleted component
	 *
	 */
	@Override
	public void preDelete(MovableContainer cNode) {
		
		try {
			if(cNode.getContainer() !=null){
				//Set the Moving signs depending on the container
				// Position the new element
				int width = WIDTH_MIN, height= LayoutHelper.getVOff(cNode);
				if(LayoutHelper.isHorizontalComponent((Container)cNode.getContainer())){
					width=LayoutHelper.getHorizontalWidth((Container)cNode.getContainer()) - cNode.getWidth() - H_OFFSET;
					width = width<WIDTH_MIN?WIDTH_MIN:width;
					height = ((Container)cNode.getContainer()).getHeight();
				}
				else{
					height=LayoutHelper.getVerticalHeight((Container)cNode.getContainer()) - cNode.getHeight() - V_OFFSET;
					height = height<LayoutHelper.getVOff(cNode)?LayoutHelper.getVOff(cNode):height;
					width = ((Container)cNode.getContainer()).getWidth();
				}
				LayoutHelper.resize((Container)cNode.getContainer(),width,height);
				//Resize parent Container if necessary
				layoutComponent((Container)cNode.getContainer(),cNode);						
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * layouts the given container mentioning the given removed container will be removed
	 * @param cContainer
	 * @param removedContainer
	 */
	private void layoutComponent(Container cContainer,Container removedContainer)
	{
		if(LayoutHelper.isHorizontalComponent(cContainer)){
			resize(cContainer,getHorizontalWidth(cContainer,removedContainer)+10,getHorizontalHeight(cContainer,removedContainer)+20);
			repositionContainingElementsHorizontal(cContainer,removedContainer);
		}
		else{
			resize(cContainer,getVerticalWidth(cContainer,removedContainer)+10,getVerticalHeight(cContainer,removedContainer)+20);
			repositionContainingElementsVertical(cContainer,removedContainer);
		}
		if(cContainer.getContainer() != null){
			if(!( cContainer.getContainer() instanceof GraphModel) ){
				layoutComponent((Container)cContainer.getContainer(),removedContainer);
			}
		}
	}
	
	/**
	 * Resizes the given container to fit the given width and height
	 * @param c
	 * @param width
	 * @param height
	 */
	private void resize(Container c,int width,int height)
	{
		if(c instanceof MovableContainer)
		{
			((MovableContainer) c).resize(width, height);
		}
		if(c instanceof Template)
		{
			((Template)c).resize(width, height);
		}
	}
	
	/**
	 * Returns the inner width of all nodes placed in the given container
	 * without the width of the given removed container 
	 * @param cContainer
	 * @param removed
	 * @return
	 */
	private int getHorizontalWidth(Container cContainer,Container removed)
	{
		int width = H_OFFSET_MIN;
		for(Node n:cContainer.getAllNodes()) {
			if(n instanceof MovableContainer || n instanceof Template){
				if(!n.equals(removed)) {
					width+=(n.getWidth()+H_OFFSET);				
				}				
			}
		}
		if(width < WIDTH_MIN) width = WIDTH_MIN;
		return width;
	}
	
	/**
	 * Returns the maximal height of all nodes placed in the given container
	 * without the width of the given removed container 
	 * @param cContainer
	 * @param removed
	 * @return
	 */
	private int getHorizontalHeight(Container cContainer,Container removed){
		 int height = LayoutHelper.getVOff(cContainer);
		 for(Node node:cContainer.getAllNodes()){
			 if(node instanceof MovableContainer || node instanceof Template){
				 if(!node.equals(removed)) {
					 if(node.getHeight() > height){
						 height = node.getHeight();
					 }
				 }
			 }
		 }
		 return height + V_OFFSET_MIN;
	}
	
	/**
	 * Returns the maximal width of all nodes placed in the given container
	 * without the width of the given removed container 
	 * @param cContainer
	 * @param removed
	 * @return
	 */
	private int getVerticalWidth(Container cContainer,Container removed){
		 int width = WIDTH_MIN;
		 for(Node node:cContainer.getAllNodes()){
			 if(node instanceof MovableContainer || node instanceof Template){
				 if(!node.equals(removed)) {
					 if(node.getWidth() > width){
						 width = node.getWidth();
					 }
				 }
			 }
		 }
		 return width + H_OFFSET_MIN;
	}
	
	/**
	 * Returns the inner height of all nodes placed in the given container
	 * without the width of the given removed container 
	 * @param cContainer
	 * @param removed
	 * @return
	 */
	private int getVerticalHeight(Container cContainer,Container removed)
	{
		int height = LayoutHelper.getVOff(cContainer);
		for(Node n:cContainer.getAllNodes()) {
			if(n instanceof MovableContainer || n instanceof Template || n instanceof Branch || n instanceof IO){
				 if(!n.equals(removed)) {
					 	height+=(n.getHeight()+V_OFFSET);
				 }
			}
		}
		return height;
	}
	
	/**
	 * Repositions all nodes placed in the given container in
	 * horizontal order ignoring the removed element
	 * @param cContainer
	 * @param removed
	 */
	private void repositionContainingElementsHorizontal(Container cContainer,Container removed){
		int x = H_OFFSET_MIN;
		for(Node n:ElementCollector.getElementsH(new ArrayList<Node>(cContainer.getAllNodes()))) {
			if(n instanceof MovableContainer || n instanceof Template){
				 if(!n.equals(removed)) {
					n.setX(x);
					x += H_OFFSET;
					x += n.getWidth();
				 }
			}
		}
	}
	
	/**
	 * Repositions all nodes placed in the given container in
	 * vertical order ignoring the removed element
	 * @param cContainer
	 * @param removed
	 */
	private void repositionContainingElementsVertical(Container cContainer,Container removed){
		int y = LayoutHelper.getVOff(cContainer);
		for(Node n:ElementCollector.getElementsV(new ArrayList<Node>(cContainer.getAllNodes()))) {
			if(n instanceof MovableContainer || n instanceof Template || n instanceof IO || n instanceof Branch){
				if(!n.equals(removed)) {
					if(!(n instanceof IO) && !(n instanceof Branch)){
						n.setY(y);				
					}
					y += V_OFFSET;
					y += n.getHeight();
				}
			}
					
		}
	}
	

	
	
}
