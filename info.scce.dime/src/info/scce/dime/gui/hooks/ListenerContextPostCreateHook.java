/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks;

import graphmodel.Container;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.gui.gui.ListenerContext;

public class ListenerContextPostCreateHook extends DIMEPostCreateHook<Container>{

	@Override
	public void postCreate(Container container) {
		if(container instanceof ListenerContext) {
			ListenerContext listenerContext = (ListenerContext) container;
			listenerContext.newEvent(20, 20);
		}
		
	}

}
