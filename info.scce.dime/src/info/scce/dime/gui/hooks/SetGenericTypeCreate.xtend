/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.data.data.Type
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.SetGenericType
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.InputGeneric
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.OutputGeneric

/** 
 * The create for edge hook is used to avoid the
 * creation of iteration edges from a non list variable or attribute
 * @author zweihoff
 */
class SetGenericTypeCreate extends DIMEPostCreateHook<SetGenericType> {
	/** 
	 * Avoids the creation of iteration edges from a non list variable or attribute
	 */
	override void postCreate(SetGenericType object) {
		val cSetGenericType = object
		val node = cSetGenericType.sourceElement
		val type = node.dataType
		val typeParameter = cSetGenericType.targetElement.typeParameter
		val cSIB = cSetGenericType.targetElement.container
		if(cSIB instanceof Branch){
			if(cSIB.container instanceof GUIPlugin){
				(cSIB.container as GUIPlugin).convertGenericPorts(typeParameter,type)
			}
		}
		if(cSIB instanceof EventListener){
			if(cSIB.container instanceof GUIPlugin){
				(cSIB.container as GUIPlugin).convertGenericPorts(typeParameter,type)
			}
		}
		if(cSIB instanceof GUIPlugin){
			cSIB.convertGenericPorts(typeParameter,type)
		}
	}
	
	def dispatch String getTypeParameter(InputGeneric node){
		return node.typeParameter
	}
	
	def dispatch String getTypeParameter(OutputGeneric node){
		return node.typeParameter
	}
	
	def void convertGenericPorts(GUIPlugin sib,String typeParameter,Type t){
		//look in inputs
		sib.IOs.filter(InputGeneric).filter[n|n.typeParameter.equals(typeParameter)].forEach[n|n.convert(t,sib)]
		//look in events
		sib.abstractBranchs.filter(EventListener).forEach[listener|listener.IOs.filter(InputGeneric).filter[n|n.typeParameter.equals(typeParameter)].forEach[n|n.convert(t,listener)]]
		//look in branches
		sib.abstractBranchs.filter(Branch).forEach[branch|branch.outputPorts.filter(OutputGeneric).filter[n|n.typeParameter.equals(typeParameter)].forEach[n|n.convert(t,branch)]]
		
	}
	
	def void convert(InputGeneric generic, Type type,GUIPlugin gp){
		val port = gp.newComplexInputPort(type,generic.x,generic.y)
		port.name = generic.name
		port.isList = generic.isList
		generic.delete
	}
	
	def void convert(InputGeneric generic, Type type,EventListener gp){
		val port = gp.newComplexInputPort(type,generic.x,generic.y)
		port.name = generic.name
		port.isList = generic.isList
		generic.delete
	}
	
	def void convert(OutputGeneric generic, Type type,Branch cb){
		val port = cb.newComplexOutputPort(type,generic.x,generic.y)
		port.name = generic.name
		port.isList = generic.isList
		generic.delete
	}
	
	def dispatch Type dataType(ComplexVariable node){
		(node as ComplexVariable).dataType
	}
	
	def dispatch Type dataType(ComplexAttribute node){
		(node as ComplexAttribute).attribute.dataType
	}
	
}
