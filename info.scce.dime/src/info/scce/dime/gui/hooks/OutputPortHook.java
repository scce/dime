/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks;

import static info.scce.dime.process.helper.LayoutConstants.BRANCH_FIRST_PORT_Y;
import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_X;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.gui.gui.ComplexOutputPort;
import info.scce.dime.gui.gui.OutputPort;
import info.scce.dime.gui.gui.PrimitiveOutputPort;
import info.scce.dime.process.helper.NodeLayout;

public class OutputPortHook extends DIMEPostCreateHook<OutputPort>{

	@Override
	public void postCreate(OutputPort port) {
		try {

//			OutputPort p = ElementCollector.findDeeply(port.getRootElement(),OutputPort.class).stream().filter(n->n.getId().equals(port.getId())).findFirst().get();
     		if (port.getContainer() instanceof info.scce.dime.gui.gui.Event) {
     			
     			info.scce.dime.gui.gui.Event cEvent = (info.scce.dime.gui.gui.Event)port.getContainer();
     			
     			// SIB's height is resized so that all input ports fit exactly into it
     			int inputPortAmount = cEvent.getOutputPorts().size();
     			cEvent.resize(cEvent.getWidth(), NodeLayout.getBranchHeight(inputPortAmount));
     		
     			int y = BRANCH_FIRST_PORT_Y + PORT_SPACE * (inputPortAmount - 1);
     			port.moveTo(cEvent, PORT_X, y);
     			
     			if (port instanceof PrimitiveOutputPort) {
     				PrimitiveOutputPort pip = (PrimitiveOutputPort) port;
     				port.setName(pip.getDataType().getName().toLowerCase() + inputPortAmount);
     				
     			}
     			else if (port instanceof ComplexOutputPort) {
     				ComplexOutputPort cip = (ComplexOutputPort) port;
     				port.setName(cip.getDataType().getName().toLowerCase() + inputPortAmount);
     			}
     			else {
     				throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
     			}
     			
     		}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
