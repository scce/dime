/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.action.CincoPostAttributeChangeHook
import graphmodel.internal.InternalContainer
import info.scce.dime.gui.gui.StaticContent
import info.scce.dime.gui.gui.internal.InternalBadge
import org.eclipse.emf.ecore.EStructuralFeature

class BadgeContentChange extends CincoPostAttributeChangeHook<StaticContent> {
	val OFFSET = 50
	override canHandleChange(StaticContent arg0, EStructuralFeature arg1) {
		true
	}

	override handleChange(StaticContent con, EStructuralFeature arg1) {
		if(arg1.name == "rawContent"){
			if(con.eContainer instanceof InternalBadge){
				var badge = con.eContainer as InternalBadge
				if (badge.container instanceof InternalContainer) {
					var container = badge.container as InternalContainer
					container.height = maxHeight(container) + OFFSET
				}
			}
		}
	}
	
	def maxHeight(InternalContainer container) {
		var maxHeight = 200
		for(element : container.modelElements){
			if(element instanceof InternalContainer){
				var baseElem = element as InternalContainer
				if(maxHeight < baseElem.height + baseElem.y){
					maxHeight = baseElem.height + baseElem.y
				}
			}
			
		}
		return maxHeight
	}

}
