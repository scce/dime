/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import info.scce.dime.gUIPlugin.AbstractParameter;
import info.scce.dime.gUIPlugin.ComplexInputParameter;
import info.scce.dime.gUIPlugin.ComplexParameter;
import info.scce.dime.gUIPlugin.EventListener;
import info.scce.dime.gUIPlugin.Function;
import info.scce.dime.gUIPlugin.GenericInputParameter;
import info.scce.dime.gUIPlugin.GenericParameter;
import info.scce.dime.gUIPlugin.InputParameter;
import info.scce.dime.gUIPlugin.Output;
import info.scce.dime.gUIPlugin.Placeholder;
import info.scce.dime.gUIPlugin.Plugin;
import info.scce.dime.gUIPlugin.PrimitiveInputParameter;
import info.scce.dime.gUIPlugin.PrimitiveParameter;
import info.scce.dime.gui.gui.Argument;
import info.scce.dime.gui.gui.GUIPlugin;

/**
 * The GUI plug in hook is used create the corresponding ports and branches
 * for the new GUI plug in SIB
 * @author zweihoff
 *
 */
public class GUIPluginHook extends AbstractPostCreateSIBHook<GUIPlugin> {

	
	/**
	 * Creates the corresponding ports and branches
	 * for the new GUI plug in SIB
	 */
	@Override
	public void postCreate(GUIPlugin sib) {
     		List<InputParameter> inputs = new ArrayList<InputParameter>();
     		
     		Function fun = (Function) sib.getFunction();
     		inputs.addAll(fun.getParameters().stream().filter(n->n instanceof InputParameter).map(n->(InputParameter)n).collect(Collectors.toList()));
     		
     		init(sib, fun.getFunctionName(), inputs.size());
     		for (InputParameter ip : inputs) {
     			if(ip instanceof PrimitiveInputParameter) {
     				addPrimitiveInputPort(((PrimitiveInputParameter) ip).getParameter().getName(),_gUIExtension.toPrimitiveType(((PrimitiveInputParameter) ip).getParameter().getType()),((PrimitiveInputParameter) ip).getParameter().isIsList());
     			}
     			else if(ip instanceof ComplexInputParameter) {
     				addComplexInputPort(((ComplexInputParameter) ip).getParameter().getName(), _gUIExtension.toComplexType(((ComplexInputParameter) ip).getParameter()), ((ComplexInputParameter) ip).getParameter().isIsList());
     			}
     			else if(ip instanceof GenericInputParameter) {
     				addGenericInputPort(((GenericInputParameter) ip).getParameter().getName(), ((GenericInputParameter) ip).getParameter().getTypeParameterName(),((GenericInputParameter) ip).getParameter().isIsList());
     			}
     		}
     		//Collecting Events
     		List<EventListener> events = fun.getEvents();
     		for(EventListener evt:events)
     		{
     			int outputPorts = evt.getParameters().size();
     			newEventListener(evt.getName(), outputPorts);
     			for(AbstractParameter port:evt.getParameters()) {
     				boolean isList = port.isIsList();
         			String name = port.getName();
         			if(port instanceof PrimitiveParameter) {
         				addEventPrimitiveInputPort(name,_gUIExtension.toPrimitiveType(((PrimitiveParameter) port).getType()),isList);
         			}
         			else if(port instanceof ComplexParameter) {
         				addEventComplexInputPort(name, _gUIExtension.toComplexType((ComplexParameter) port), isList);
         			}
         			else if(port instanceof GenericParameter) {
         				addEventGenericInputPort(name,((GenericParameter) port).getTypeParameterName(), isList);
         			}
     			}
     		}
     		
     		//Collecting Branches
     		for (Output output : fun.getOutputs()) {
		    		int outputPorts = output.getParameters().size();
			    	newBranch(output.getOutputName(), outputPorts);
			    	for(AbstractParameter port:output.getParameters())
			    	{
			    		boolean isList = port.isIsList();
         			String name = port.getName();
         			if(port instanceof PrimitiveParameter) {
         				addPrimitiveOutputPort(name,_gUIExtension.toPrimitiveType(((PrimitiveParameter) port).getType()),isList);
         			}
         			else if(port instanceof ComplexParameter) {
         				addComplexOutputPort(name, _gUIExtension.toComplexType((ComplexParameter) port), isList);
         			}
         			else if(port instanceof GenericParameter) {
         				addGenericOutputPort(name, ((GenericParameter) port).getTypeParameterName(), isList);
         			}
			    	}
	
     		}
     		
     		
     		finish();
     		//Trigger Layouter
 			Plugin plugin = (Plugin) fun.eContainer();
 			if(plugin.getTemplate()!=null){
 				if(plugin.getTemplate().getPlacholders()!=null){
 					for(Placeholder pl:plugin.getTemplate().getPlacholders().getPlaceholders()){
 						Argument ca = ((GUIPlugin) csib).newArgument(1, csib.getHeight());
 		 				ca.setBlockName(pl.getName());
 					}
 				}
 			}

     		
     		CreateComponentHook cch = new CreateComponentHook();
     		cch.postCreate(sib);
	}
	
	

}
