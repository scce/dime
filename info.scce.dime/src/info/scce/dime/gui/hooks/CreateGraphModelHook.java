/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.gui.gui.DataContext;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.Template;

/**
 * The create graph model hook is used to create the needed elements for the GUI model
 * like the first template and a data context
 * @author zweihoff
 *
 */
public class CreateGraphModelHook extends DIMEPostCreateHook<GUI>{

	private static final int MARGIN = 10;
	
	/**
	 * Creates the needed elements for the GUI model
	 * like the first template and a data context
	 */
	@Override
	public void postCreate(GUI graphModel) {
		try {
			String fileName = graphModel.eResource().getURI().lastSegment();
			String fileExtension = graphModel.eResource().getURI().fileExtension();
			String modelName = fileName.replace("." + fileExtension, "");
			
			graphModel.setTitle(modelName);
			
			DataContext dctx = graphModel.newDataContext(MARGIN, MARGIN);
			
			Template t = graphModel.newTemplate(MARGIN + dctx.getWidth() + 50, MARGIN);
			t.newRow(1, 1);
			
			//cGUI.newCExtensionPoint(10, 10);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
