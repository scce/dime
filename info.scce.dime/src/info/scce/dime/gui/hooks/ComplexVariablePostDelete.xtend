/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.hooks

import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.gui.gui.ComplexVariable

import static extension info.scce.dime.process.helper.AttributeExpandUtils.resizeAndLayout
import info.scce.dime.api.DIMEPostDeleteHook
import info.scce.dime.gui.gui.DataContext
import info.scce.dime.data.data.ExtensionAttribute

/** 
 * Deletes all expended attributes, which are complex variables connected
 * by complex (list) attribute connector edges in the data context
 * 
 * @author zweihoff
 */
class ComplexVariablePostDelete extends DIMEPostDeleteHook<ComplexVariable> {
	val OFFSET = 30
	
	def preDelete(ComplexVariable cVar) {
		// Collect attached successors
		val successors = cVar.complexVariableSuccessors
		
		// Delete edges first to prevent re-layout in preDelete hooks of successors
		cVar.outgoing.forEach[delete]
		successors.forEach[delete]
		
		// If predecessor is expanded, add self as ComplexAttribute and trigger layout
		cVar.complexVariablePredecessors
			.filter[isExpanded]
			.forEach[
				val attr = dataType.findComplexAttribute(cVar.name)
				if (attr != null) {
					newComplexAttribute(attr, 1, 1)
					resizeAndLayout
				} else {
					val extAttr = dataType.findExtensionAttribute(cVar.name)
					if(extAttr != null) {
						newComplexExtensionAttribute(extAttr,1,1)
						resizeAndLayout
					}
				}
			]
	}
	
	def layout (DataContext dataContext){
		var maxHeight = maxHeight(dataContext);
		dataContext.setHeight( maxHeight + OFFSET);
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
	def findComplexAttribute(Type it, String attrName) {
		inheritedAttributes.filter(ComplexAttribute).findFirst[name == attrName]
	}
	
	def findExtensionAttribute(Type it, String attrName) {
		inheritedAttributes.filter(ExtensionAttribute).findFirst[name == attrName]
	}
	
	override getPostDeleteFunction(ComplexVariable node) {
		preDelete(node)
		if(node.container instanceof DataContext){
			val dataContext  = node.container as DataContext
			return [dataContext.layout]
		}
	}
	
}
