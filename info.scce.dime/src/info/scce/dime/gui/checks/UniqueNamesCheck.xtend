/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks

import graphmodel.Container
import graphmodel.GraphModel
import graphmodel.ModelElementContainer
import info.scce.dime.checks.GUICheck
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.^FOR
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Variable
import java.util.List
import info.scce.dime.gui.helper.GUIBranchPort
import info.scce.dime.gui.helper.ComplexGUIBranchPort

class UniqueNamesCheck extends GUICheck {
	
	GUI model
	val contextVarNames = newHashSet
	val placeholderNames = newHashSet
	
	override void init() {
		contextVarNames.clear
		placeholderNames.clear
	}
	
	override check(GUI model) {
		this.model = model
		model => [
			checkBranches
			find(Variable).forEach[check]
			find(Button).forEach[check]
			find(^FOR).forEach[check]
			find(Placeholder).forEach[check]
		]
	}
	
	dispatch def check(Button it) {
		check[
			!GUIBranchPort.getPorts(it).containsDuplicatesByKey[name]
		].elseError("Port names must be unique")
	}
	
	// TODO III (incomprehensible inefficient implementation)
	dispatch def check(^FOR forObj) {
		var String index=forObj.getIndex() 
		if (index === null || "".equals(index)) {
			forObj.addError("index attribute not set") 
			return;
		}
		var ComplexVariable currentVar=null 
		if (forObj.getSourceElement().getOutgoing(ComplexListAttributeConnector).size() === 1) 
			currentVar = forObj.getSourceElement().getOutgoing(ComplexListAttributeConnector).get(0).getTargetElement() as ComplexVariable
		var MovableContainer mc=(forObj.getTargetElement() as MovableContainer) 
		var ModelElementContainer mec=mc.getContainer() 
		while (mec instanceof GraphModel === false) {
			if (mec instanceof Container)
			{
				var Container container=(mec as Container) 
				if (!container.getIncoming(^FOR).isEmpty()) {
					for (^FOR parentFor : container.getIncoming(^FOR)) {
						if (index.equals(parentFor.getIndex())) 
							forObj.addError("index attribute not unique") 
						var ComplexVariable parentCurrentVar = null 
						if (parentFor.getSourceElement().getOutgoing(ComplexListAttributeConnector).size() === 1) 
							parentCurrentVar = parentFor.getSourceElement().getOutgoing(ComplexListAttributeConnector).get(0).getTargetElement() as ComplexVariable
						if (currentVar !== null && parentCurrentVar !== null && currentVar.getName().equals(parentCurrentVar.getName())) 
							addError(adapter.getIdByString(currentVar.getId()), "name as iterator not unique") 
					}
				}
				mec=container.getContainer() 
				
			}
		}
	}
	
	/** 
	 * Checks if all button labels present in the GUI as well as all branch names of all embedded SIBs
	 * are uniquely named.
	 * @param gui
	 */
	// TODO III (incomprehensible inefficient implementation)
	def checkBranches(GUI gui) {
		gui.GUIBranchesMerged
		.filter[!ports.equallyNamedPorts.isEmpty]
		.forEach[n|n.ports.equallyNamedPorts
			.forEach[e|n.ports
				.filter[port|port.name.equals(e.name)]
				.toList
				.checkEqualPortTypes(n.name, n.ports.equallyNamedPorts.get(0).name)
			]
		]
	}

	// TODO III (incomprehensible inefficient implementation)
	def checkEqualPortTypes(List<GUIBranchPort> ports, String branchName, String portName)
	{
		val complexSize = ports.filter(ComplexGUIBranchPort).size
		//all are primitive
		if(complexSize==0) {
//			if(ports.filter[guiSIBbranchPortisPrimitive].map[guiSIBbranchPortPrimitiveType].map[literal].toSet.size!=1) {
//				addError(id,'''The port «portName» of branch «branchName» is not always same primitive type''')
//			}
		}
		//check type
		else {
			for(group:ports.filter(ComplexGUIBranchPort).groupBy[name].entrySet){
				val equallyNamedPorts = group.value.map[type]
				val superTypes = equallyNamedPorts.map[superTypes]
				var found = true
				for(type:equallyNamedPorts){
					for(suTypes:type.superTypes){
						val size = equallyNamedPorts.map[superTypes].filter[n|n.contains(suTypes)].size
						if(size !=0){
							found = true;
						}
					}
				}
				if(!found){
					model.addError('''The port «portName» of branch «branchName» is not always same complex type''')					
				}
				if(group.value.map[isList].toSet.size!=1) {
					model.addError('''The port «portName» of branch «branchName» is not always same list status''')
				}
			}
		}
	}
	
	dispatch def check(PrimitiveVariable it) {
		if (!contextVarNames.add(name)) {
			addError('''Name '«name»' is not unique''')
		}
	}
	
	dispatch def check(ComplexVariable it) {
		if (getIncoming(ComplexAttributeConnector).isEmpty
			&& getIncoming(ComplexListAttributeConnector).isEmpty
			&& !contextVarNames.add(name))
		{
			addError('''Name '«name»' is not unique''')
		}
	}
	
	dispatch def check(Placeholder it) {
		if (name.nullOrEmpty)
			addError("Name must not be empty")
		else if (!placeholderNames.add(name))
			addError('''Name '«name»' is not unique''')
	}
	
	
	// TODO Move to CollectionExtension in Cinco-Meta
	private def <T, U> containsDuplicatesByKey(Iterable<T> iterable, (T) => U keyExtractor) {
		!iterable.duplicatesByKey(keyExtractor).isEmpty
	}
}
