/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks

import info.scce.dime.checks.AbstractCheck
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ISSIB
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.mcam.adapter.GUIAdapter
import info.scce.dime.gui.mcam.adapter.GUIId
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.process.process.ProcessType

/** 
 * The event check is used to validate the event listeners
 * of all listener contexts of a GUI model
 * @author zweihoff
 */
class ControlSIBCheck extends AbstractCheck<GUIId, GUIAdapter> {
	GUIAdapter adapter
	protected extension DataExtension = DataExtension.instance

	/** 
	 * Checks, if the events in given GUI model
	 * are valid.
	 */
	override void doExecute(GUIAdapter arg0) {
		this.adapter = arg0
		for (GUIId id : adapter.getEntityIds()) {
			var Object obj = id.getElement()
			if(obj instanceof ISSIB){
				checkControlSIB(id,obj)
				checkISSIB(id,obj)
			}
			if(obj instanceof FORSIB){
				checkControlSIB(id,obj)
				checkForSIB(id,obj)
			}
			if(obj instanceof IFSIB){
				checkControlSIB(id,obj)
			}
			if(obj instanceof SecuritySIB){
				checkSecuritySIB(id,obj)
			}
		}
	}
	
	def checkISSIB(GUIId id, ISSIB issib) {
		if(!issib.IOs.filter(ComplexInputPort).empty){
			val inputPort = issib.IOs.filter(ComplexInputPort).get(0)
			if(inputPort.isList){
				id.addError('''complex input port has to be not list''')				
			}
			//check connected type
			if(!inputPort.getIncoming(Read).empty){
				val dataSource = inputPort.getIncoming(Read).get(0).sourceElement
				val superTypeNames = inputPort.dataType.superTypes.map[name] + inputPort.dataType.name
				
				if(dataSource instanceof ComplexVariable){
					val typeName = dataSource.dataType.name
					if(!superTypeNames.contains(typeName)){
						id.addError('''connected type is not in same hierarchy''')		
					}
				}
				else if(dataSource instanceof ComplexAttribute){
					val typeName = dataSource.attribute.dataType.name
					if(!superTypeNames.contains(typeName)){
						id.addError('''connected type is not in same hierarchy''')		
					}	
				}
				else {
					id.addError('''only complex objects can be validated''')	
				}
			}
			
		}
	}
	
	def checkForSIB(GUIId id, FORSIB forsib) {
		if(forsib.from<0){
			id.addError('''start index has to be greater than 0''')
		}
		if(forsib.to<-1){
			id.addError('''end index has to be greater than -1''')
		}
		if(forsib.from>forsib.to&&forsib.to>-1){
			id.addError('''start index has to less than end index''')
		}
		if(!forsib.IOs.filter(ComplexInputPort).empty){
			if(!forsib.IOs.filter(ComplexInputPort).get(0).isList){
				id.addError('''complex input port has to be list''')				
			}
			if(!forsib.IOs.filter(ComplexInputPort).get(0).getIncoming(Read).empty){
				val v = forsib.IOs.filter(ComplexInputPort).get(0).getIncoming(Read).get(0).sourceElement
				if(v instanceof ComplexVariable){
					if(!v.isIsList){
						id.addError('''iteration source has to be list''')
					}
				}
				if(v instanceof ComplexAttribute){
					if(!v.attribute.isIsList){
						id.addError('''iteration source has to be list''')
					}
				}
			}
		}
		
	}
	
	def checkControlSIB(GUIId id, ControlSIB issib) {
		if(issib.IOs.size!=1){
			id.addError('''one complex input port required''')
		}
		else{
			if(issib.IOs.get(0).getIncoming(Read).empty){
				id.addError('''read edge required''')
			}
		}
	}
	
	def checkSecuritySIB(GUIId id, SecuritySIB secSib) {
		if((secSib.proMod as info.scce.dime.process.process.Process).processType!=ProcessType.BASIC) {
			id.addError('''only interactbale process supported''')
		}
		if(!(secSib.proMod as info.scce.dime.process.process.Process).endSIBs.exists[n|n.branchName.equals("granted")]){
			id.addError('''granted end SIB required''')
		}
		if(!(secSib.proMod as info.scce.dime.process.process.Process).endSIBs.exists[n|n.branchName.equals("denied")]){
			id.addError('''denied end SIB required''')
		}
		if((secSib.proMod as info.scce.dime.process.process.Process).endSIBs.size!=2){
			id.addError('''only denied and granted end SIB allowed''')
		}
	}
	
	override init() {
	}
	
	
}
