/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.common.util.TreeIterator
import org.eclipse.emf.ecore.EObject
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.mcam.adapter.GUIAdapter
import info.scce.dime.gui.mcam.adapter.GUIId
import info.scce.dime.checks.AbstractCheck
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.ExtensionContext

/** 
 * The embedded models check is used the validate that no inductive
 * structure is defined by the GUI SIBs placed in a given GUI model
 * @author zweihoff
 */
class EmbeddedModelsCheck extends AbstractCheck<GUIId, GUIAdapter> {
	GUIAdapter adapter
	/** 
	 * Checks if the given GUI model and the embedded GUI SIB define
	 * no inductive structure
	 */
	override void doExecute(GUIAdapter arg0) {
		this.adapter=arg0 
		for (GUIId id : adapter.getEntityIds()) {
			var Object obj=id.getElement() 
			if (obj instanceof GUI) {
				checkCycleInGuiReferences(id, (obj as GUI), new ArrayList<String>()) 
			}
		}
	}
	override void init() {
	}
	/** 
	 * Checks if the given GUI is already known, so that a cycle is detected.
	 * Otherwise the check is repeated for all GUI SIBs placed in the model
	 * @param id
	 * @param gui
	 * @param knownGuiIds
	 */
	def private void checkCycleInGuiReferences(GUIId id, GUI gui, ArrayList<String> knownGuiIds) {
		if (knownGuiIds.contains(gui.getId())) {
			addError(id, "cycle in gui references found") 
			return;
		} else {
			knownGuiIds.add(gui.getId()) 
		}
		for (GUISIB containedGuiSib : getContainedGuiSibs(gui)) {
			checkCycleInGuiReferences(id, containedGuiSib.getGui(), new ArrayList<String>(knownGuiIds)) 
		}
	}
	/** 
	 * Returns a list of all GUI SIBs present anywhere in the given GUI model
	 * @param gui
	 * @return
	 */
	def private List<GUISIB> getContainedGuiSibs(GUI gui) {
		var ArrayList<GUISIB> list=new ArrayList<GUISIB>() 
		var TreeIterator<EObject> it=gui.eAllContents() 
		while (it.hasNext()) {
			var Object obj=it.next() 
			if (obj instanceof GUISIB){
				if(!(obj.container instanceof ExtensionContext)){
					var GUISIB guiSib=(obj as GUISIB) 
					list.add(guiSib) 
				}
			}
			
		}
		return list 
	}
}
