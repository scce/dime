/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks;


import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * The image check is used to validate image components
 * @author zweihoff
 *
 */
public class FileCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if all images in the given GUI model are valid
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof File)
				check(id, (File) obj);
		}
	}
	
	@Override
	public void init() {}

	/**
	 * Check, if a given file component is valid.
	 * If a display edge is connected to the file the value of the edge source variable or attribute
	 * has to be a file.
	 * If no display edge is present a file guard process has to be present.
	 * @param id
	 * @param te
	 */
	private void check(GUIId id, File te) {
		//Check file guards
		if(te.getGuardSIBs().isEmpty()){
			addError(id, "Unguarded file access");
		}
		else{
			//guard availbale
			if(te.getGuardSIBs().size()>1){
				addError(id, "only one file guard is allowed");
			}
		}
		
	}
	
}
