/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * The variable check is used to validate the input status of all varibales
 * in all data contexts of a GUI model
 * @author zweihoff
 *
 */
public class VariableCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if the input status of all varibales in the given GUI model
	 * is valid.
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof Variable)
				checkInputVariable(id, (Variable) obj);
		}
	}
	
	@Override
	public void init() {}

	/**
	 * Checks, if a given variable is an input variable and 
	 * than also top level variable
	 * @param id
	 * @param te
	 */
	private void checkInputVariable(GUIId id, Variable te) {
		//Check data Edges
		if(te.isIsInput()){
			if(!te.getIncoming(ComplexAttributeConnector.class).isEmpty()){
				addError(id, "cannot be an input variable");
			}
			if(!te.getIncoming(ComplexListAttributeConnector.class).isEmpty()){
				addError(id, "cannot be an input variable");
			}
		}
	}
}
