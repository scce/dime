/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks

import info.scce.dime.checks.GUICheck
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.^FOR
import info.scce.dime.gui.gui.^IF
import info.scce.dime.gui.gui.Iteration
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.IS

class StructuralDirectiveCheck extends GUICheck {
	
	override check(GUI it) {
		find(MovableContainer)
			.filter[!getIncoming(^FOR).isEmpty && !getIncoming(^IF).isEmpty]
			.forEach[addError("incoming IF and FOR at the same time is not supported")]
		find(Iteration)
			.map[sourceElement]
			.filter(Variable)
			.filter[!isIsList]
			.forEach[addError("Iteration can only be performed on a list")]
		find(Iteration)
			.map[sourceElement]
			.filter(ComplexAttribute)
			.filter[!attribute.isIsList]
			.forEach[addError("Iteration can only be performed on a list")]
		find(Iteration)
			.map[sourceElement]
			.filter(PrimitiveAttribute)
			.filter[!attribute.isIsList]
			.forEach[addError("Iteration can only be performed on a list")]
		find(IS)
			.filter[matchingType.nullOrEmpty]
			.forEach[addError("IS edge matching type is not specified")]
	}

}
