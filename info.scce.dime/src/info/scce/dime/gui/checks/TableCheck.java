/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;
import graphmodel.Node;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.DataExtension;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexExtensionAttribute;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataTarget;
import info.scce.dime.gui.gui.DropTarget;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Table;
import info.scce.dime.gui.gui.TableChoice;
import info.scce.dime.gui.gui.TableColumnLoad;
import info.scce.dime.gui.gui.TableEntry;
import info.scce.dime.gui.gui.TableEntryDefaultSort;
import info.scce.dime.gui.gui.TableLoad;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.helper.GUIExtension;
import info.scce.dime.gui.helper.GUIExtensionProvider;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.process.process.PrimitiveListAttribute;

/**
 * The table check is used to validate table components
 * present in a GUI model
 * @author zweihoff
 *
 */
public class TableCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;
	
	private DataExtension dataEx = DataExtension.getInstance();



	/**
	 * Checks, if all table components and their edges
	 * present in the given GUI model are valid
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof TableColumnLoad)
				checkTableColumnLoad(id, (TableColumnLoad) obj);
			if (obj instanceof Table)
				checkTable(id, (Table) obj);
			if (obj instanceof TableLoad)
				checkTableLoad(id, (TableLoad) obj);
		}
	}
	
	@Override
	public void init() {}

	/**
	 * Checks, if the given table is connected to a source list variable
	 * and if the table choice option is enabled whether a data target edge is present.
	 * Especially the list status of the connected data target is compared to the choice option
	 * @param id
	 * @param table
	 */
	private void checkTable(GUIId id, Table table) {
		if (table.getIncoming(TableLoad.class).size() < 1) {
			addError(id, "missing table load");
		} else if (table.getIncoming(TableLoad.class).size() > 1) {
			addError(id, "only one table load edge allowed");
		}
		//Check Choice Table
		if(table.getChoices()!=TableChoice.NONE){
			if(table.getIncoming(DataTarget.class).isEmpty()){
				addError(id, "missing table data target");
			}
			//Check Multiplicity
			else {
				for(Node node:table.getIncoming(DataTarget.class).stream().map(n->n.getSourceElement()).collect(Collectors.toList()))
				{
					if(node instanceof Variable){
						Variable var = (Variable) node;
						if(table.getChoices()==TableChoice.MULTIPLE && !var.isIsList()){
							addError(id, "data target should be list");
						}
						if(table.getChoices()==TableChoice.SINGLE && var.isIsList()){
							addError(id, "data target should be no list");
						}
					}
					if(node instanceof info.scce.dime.gui.gui.ComplexAttribute){
						info.scce.dime.gui.gui.ComplexAttribute var = (info.scce.dime.gui.gui.ComplexAttribute) node;
						if(table.getChoices()==TableChoice.MULTIPLE && !var.getAttribute().isIsList()){
							addError(id, "data target should be list");
						}
						if(table.getChoices()==TableChoice.SINGLE && var.getAttribute().isIsList()){
							addError(id, "data target should be no list");
						}
					}
					if(node instanceof info.scce.dime.gui.gui.PrimitiveAttribute){
						info.scce.dime.gui.gui.PrimitiveAttribute var = (info.scce.dime.gui.gui.PrimitiveAttribute) node;
						if(table.getChoices()==TableChoice.MULTIPLE && !var.getAttribute().isIsList()){
							addError(id, "data target should be list");
						}
						if(table.getChoices()==TableChoice.SINGLE && var.getAttribute().isIsList()){
							addError(id, "data target should be no list");
						}
					}
				}
			}
			
		}
		//Check Sorting
		if (table.isReSortable()) {
			if (table.isAutoSorted()) {
				addError(id, "reSortable table requires autoSorted table to be false");
			}
			
			for(TableEntry tableEntry : table.getTableEntrys()) {
				if (tableEntry.isSortable() 
						|| tableEntry.isFullTextSearch() 
						|| tableEntry.getDefaultSorting() != TableEntryDefaultSort.NONE) {
					addError(id, "reSortable table requires all table entries not to be sortable or filterable");
					break;
				}
			}
						
			// check if drop targets are also resortable
			final Map<String, Table> tableMap = ReferenceRegistry.getInstance().lookup(Table.class).stream()
					.collect(Collectors.toMap(Table::getId, Function.identity()));
					
			final DataExtension dataExtension = DataExtension.getInstance();
			for(DropTarget dt: table.getDropTargets()) {
				if (tableMap.containsKey(dt.getTableID())) {
					final Table targetTable = tableMap.get(dt.getTableID());
					if (!targetTable.isReSortable()) {
						addError(id, "table '" + targetTable.getName() + "' also needs to be resortable");
					}
					
					// check if two tables have each other as drop target that their table loads are different
					if (!table.getId().equals(targetTable.getId())) {
						for(DropTarget dt2: targetTable.getDropTargets()) {
							if (dt2.getTableID().equals(table.getId())
									&& table.getIncomingTableLoads().size() == 1
									&& targetTable.getIncomingTableLoads().size() == 1
									&& table.getIncomingTableLoads().get(0).getId().equals(targetTable.getIncomingTableLoads().get(0).getId())) {
								addError(id, "table '" + table.getName() + "' and '" + targetTable.getName() + "' may not share the same input from tableLoad");
							}
						}
					}
											
					// check if drop target has same complex input
					if (table.getIncomingTableLoads().size() != targetTable.getIncomingTableLoads().size()) {
						addError(id, "table '" + table.getName() + "' and table '" + targetTable.getName() + "' do not have the same amount of incoming table load edges");
					} else if (table.getIncomingTableLoads().size() == 1 && targetTable.getIncomingTableLoads().size() == 1) {
						final Node fromTableNode = table.getIncomingTableLoads().get(0).getSourceElement();
						final Node targetTableNode = targetTable.getIncomingTableLoads().get(0).getSourceElement();
						
						if (fromTableNode instanceof PrimitiveAttribute && targetTableNode instanceof PrimitiveAttribute) {
							final PrimitiveType t1 = ((PrimitiveAttribute) fromTableNode).getAttribute().getDataType();
							final PrimitiveType t2 = ((PrimitiveAttribute) targetTableNode).getAttribute().getDataType();
							if (!t2.equals(t1)) addError(id, "table and drop target have to have the same input data type");
						} else if (fromTableNode instanceof ComplexVariable && targetTableNode instanceof Variable) {
							final Type t1 = ((ComplexVariable) fromTableNode).getDataType();
							final Type t2 = ((ComplexVariable) targetTableNode).getDataType();
							if (!dataExtension.isTypeOf(t1, t2)) addError(id, "table and drop target have to have the same input data type");
						} else {
							addError(id, "table loads for tables '" + targetTable.getName() + "' and '" + table.getName() + "' are not the same");
						}
					}
				}				
			}		
		} else {
			if (!table.getDropTargets().isEmpty()) {
				addError(id, "table with drop targets needs to be resortable");
			}
		}
				
		int defaultSortCounter = 0;
		for(TableEntry tableEntry:table.getTableEntrys())
		{			
			boolean isSortable = false;
			if(tableEntry.isSortable()){
				if(tableEntry.getIncoming(TableColumnLoad.class).isEmpty()){
					addError(id, "sortable table column requires table colum load edge");
				} else {
					//check table column load type
					if(tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement() instanceof ComplexVariable) {
						ComplexVariable connectedCV = (ComplexVariable) tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement();
						if(!(connectedCV.getDataType() instanceof EnumType)) {
							addError(id, "table column has to load primitive attribute");							
						}
					}
					else if(tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement() instanceof ComplexAttribute) {
						ComplexAttribute connectedCA = (ComplexAttribute) tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement();
						if(!(connectedCA.getAttribute().getDataType() instanceof EnumType)) {
							addError(id, "table column has to load primitive attribute");							
						}
					}
					else if(tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement() instanceof ComplexExtensionAttribute) {
						ComplexExtensionAttribute connectedCEA = (ComplexExtensionAttribute) tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement();
						Type t = dataEx.getComplexExtensionAttributeType(connectedCEA.getAttribute());
						if(!(t instanceof EnumType)) {
							addError(id, "table column has to load primitive attribute");							
						}
					}
				}
				isSortable = true;
			}
			if(tableEntry.isFullTextSearch()){
				if(tableEntry.getIncoming(TableColumnLoad.class).isEmpty()){
					addError(id, "text search of column requires table colum load edge");
				}  else {
					//check table column load type
					if(tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement() instanceof ComplexVariable) {
						ComplexVariable connectedCV = (ComplexVariable) tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement();
						if(!(connectedCV.getDataType() instanceof EnumType)) {
							addError(id, "table column has to load primitive attribute");							
						}
					}
					else if(tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement() instanceof ComplexAttribute) {
						ComplexAttribute connectedCA = (ComplexAttribute) tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement();
						if(!(connectedCA.getAttribute().getDataType() instanceof EnumType)) {
							addError(id, "table column has to load primitive attribute");							
						}
					}
					else if(tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement() instanceof ComplexExtensionAttribute) {
						ComplexExtensionAttribute connectedCEA = (ComplexExtensionAttribute) tableEntry.getIncoming(TableColumnLoad.class).get(0).getSourceElement();
						Type t = dataEx.getComplexExtensionAttributeType(connectedCEA.getAttribute());
						if(!(t instanceof EnumType)) {
							addError(id, "table column has to load primitive attribute");							
						}
					}
				}
			}
			if(tableEntry.getDefaultSorting() != TableEntryDefaultSort.NONE){
				defaultSortCounter++;
				if(!isSortable){
					addError(id, "default sorting requires enabled sorting option");					
				}
			}
			
		}
		if(defaultSortCounter>1){
			addError(id, "only one default sort column can be selected");
		}
	}
	
	/**
	 * Checks, if the data source of the given table load edge is a list
	 * @param id
	 * @param tl
	 */
	private void checkTableLoad(GUIId id, TableLoad tl) {
		Node source = tl.getSourceElement();

		if (source instanceof ComplexAttribute) {
			ComplexAttribute cAttr = (ComplexAttribute) source;
			if (!cAttr.getAttribute().isIsList())
				addError(id, "source should be a list");
		} else if (source instanceof ComplexVariable) {
			ComplexVariable cVar = (ComplexVariable) source;
			if (!cVar.isIsList())
				addError(id, "source should be a list");
		} else if (source instanceof PrimitiveAttribute) {
			PrimitiveAttribute pAttr = (PrimitiveAttribute) source;
			if (!pAttr.getAttribute().isIsList())
				addError(id, "source should be a list");
		} else if (source instanceof PrimitiveVariable) {
			PrimitiveVariable pVar = (PrimitiveVariable) source;
			if (!pVar.isIsList())
				addError(id, "source should be a list");
		} else {
			addError(id, "wrong type of connected data");
		}
	}

	/**
	 * Checks, if the table column load edge connects to a primitive variable
	 * or attribute, so that the sorting and filtering function can be used
	 * @param id
	 * @param tcl
	 */
	private void checkTableColumnLoad(GUIId id, TableColumnLoad tcl) {
		if (tcl.getSourceElement() instanceof PrimitiveVariable == false
				&& tcl.getSourceElement() instanceof PrimitiveListAttribute
				&& tcl.getSourceElement() instanceof PrimitiveAttribute == false)
			addError(id, "source elements type is not primitive");
	}

}
