/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks

import info.scce.dime.checks.AbstractCheck
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.mcam.adapter.GUIAdapter
import info.scce.dime.gui.mcam.adapter.GUIId
import java.util.HashSet
import java.util.Set
import graphmodel.Node
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeName
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.data.helper.DataExtension

/** 
 * The event check is used to validate the event listeners
 * of all listener contexts of a GUI model
 * @author zweihoff
 */
class EventCheck extends AbstractCheck<GUIId, GUIAdapter> {
	GUIAdapter adapter
	Set<String> knownNames;
	
	extension DataExtension = DataExtension.instance

	/** 
	 * Checks, if the events in given GUI model
	 * are valid.
	 */
	override void doExecute(GUIAdapter arg0) {
		this.adapter = arg0
		for (GUIId id : adapter.getEntityIds()) {
			var Object obj = id.getElement()
			if(obj instanceof Event){
				checkEventNames(id,obj)
				checkWriteEdges(id,obj)
			}
			if(obj instanceof EventListener) {
				checkReadEdges(id,obj)
			}
		}
	}
	
	def checkWriteEdges(GUIId id, Event event) {
		val connectedData = event.outputPorts.map[outgoing].flatten.map[targetElement]
		connectedData.map[toRootScopeData].filter[n|n!=null].forEach[n|addError(id,'''«event.name» writes to inner scope data''')]
		
		//check type
		event.outputPorts.filter[!outgoing.empty].forEach[port|{
			port.outgoing.map[targetElement].forEach[data|{
				if(port instanceof ComplexOutputPort && data instanceof ComplexVariable) {
					if(port.isIsList!=(data as ComplexVariable).isIsList) {
						addError(id,'''«port.name» list state does not match «(data as ComplexVariable).name»''')
					}
					if(!(data as ComplexVariable).dataType.knownSubTypes.contains((port as ComplexOutputPort).dataType)) {
						addError(id,'''«port.name» type cannot be connected to «(data as ComplexVariable).dataType.name»''')
					}
				} else if(port instanceof ComplexOutputPort && data instanceof ComplexAttribute) {
					if(port.isIsList!=(data as ComplexAttribute).attribute.isIsList) {
						addError(id,'''«port.name» list state does not match «(data as ComplexAttribute).attribute.name»''')
					}
					if(!(data as ComplexAttribute).attribute.type.knownSubTypes.contains((port as ComplexOutputPort).dataType)) {
						addError(id,'''«port.name» type cannot be connected to «(data as ComplexAttribute).attribute.type.name»''')
					}
					
				} else if(port instanceof PrimitiveOutputPort && data instanceof PrimitiveVariable) {
					if(port.isIsList!=(data as PrimitiveVariable).isIsList) {
						addError(id,'''«port.name» list state does not match «(data as PrimitiveVariable).name»''')
					}
					if((port as PrimitiveOutputPort).dataType!=(data as PrimitiveVariable).dataType) {
						addError(id,'''«port.name» type cannot be connected to «(data as PrimitiveVariable).dataType.literal»''')
					}
				} else if(port instanceof PrimitiveOutputPort && data instanceof PrimitiveAttribute) {
					if(port.isIsList!=(data as PrimitiveAttribute).attribute.isIsList) {
						addError(id,'''«port.name» list state does not match «(data as PrimitiveAttribute).attribute.name»''')
					}
					if((port as PrimitiveOutputPort).dataType.literal!=(data as PrimitiveAttribute).attribute.dataType.literal) {
						addError(id,'''«port.name» type cannot be connected to «(data as PrimitiveAttribute).attribute.dataType.literal»''')
					}
				} else {
					addError(id,'''«port.name» incompatible connected variable''')
				}
				
				
			}]
		}]
	}
	
	def checkReadEdges(GUIId id, EventListener event) {
		val connectedData = event.inputPorts.map[incoming].flatten.map[sourceElement]
		connectedData.map[toRootScopeData].filter[n|n!=null].forEach[n|addError(id,'''«event.name» reads from inner scope data''')]
	}
	
	def Node toRootScopeData(Node node) {
		if(node instanceof ComplexVariable){
			// go up attributes
			if(!node.getIncoming(ComplexAttributeConnector).empty) {
				return toRootScopeData(node.getIncoming(ComplexAttributeConnector).get(0).sourceElement)
			}
			// go up list
			if(!node.getIncoming(ComplexListAttributeConnector).empty) {
				val edge = node.getIncoming(ComplexListAttributeConnector).get(0)
				// if current element, scope is the list
				if(edge.attributeName==ComplexListAttributeName.CURRENT)return edge.sourceElement
				//else go up
				return toRootScopeData(edge.sourceElement)
			}
		}
		else if(node instanceof Attribute) {
			return toRootScopeData(node.container as Node)
		}
		return null;
	}

	override void init() {
		knownNames = new HashSet
	}

	/** 
	 * Checks, if a given event has a unique name
	 * @param id
	 * @param te
	 */
	def private void checkEventNames(GUIId id, Event te) {
		if(!knownNames.add(te.name)){
			addError(id,'''Multiple events have the same name «te.name»''')
		}
		//check port name unique
		val knownPortNames = new HashSet
		// TODO replace with .drop[knownPortNames.add(name)] using 'drop' from CollectionExtension
		te.outputPorts.filter[n|!knownPortNames.add(n.name)].forEach[n|addError(id,'''Multiple ports have the same name «n.name»''')]
	}
}
