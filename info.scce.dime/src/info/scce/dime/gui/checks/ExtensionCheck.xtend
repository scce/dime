/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks

import de.jabc.cinco.meta.runtime.xapi.CodingExtension
import graphmodel.Container
import graphmodel.IdentifiableElement
import graphmodel.Node
import info.scce.dime.checks.GUICheck
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Variable

import static extension org.jooq.lambda.Seq.toMap

/**
 * The extension check is used to validate the signature
 * of the current GUI model, if it extends another GUI model.
 * 
 * @author zweihoff
 * @author neubauer
 */
class ExtensionCheck extends GUICheck {
	
	extension DataExtension = DataExtension.instance
	extension CodingExtension = new CodingExtension

		
	override check(GUI model) {
		val extensionContext = model?.extensionContexts?.head
		val guiSIBs = extensionContext?.GUISIBs
		
		if (guiSIBs?.size > 1) {
			(extensionContext ?: model).addError('''Only one GUI extension is allowed''')
			return
		}
		
		guiSIBs?.head?.with [
			checkSignature
			checkNoConnections
			checkNoSelfExtension
		]
	}
	
	private def void checkNoConnections(Node it) {
		if (!noEdges) addError('''No edges allowed on extension''')
		if (it instanceof Container) modelElements.filter(Node).forEach[checkNoConnections]
	}
	
	private def checkNoSelfExtension(GUISIB it) {
		// TODO JN: Perhaps check "no circle extension" instead?
		if (gui == rootElement) addError('''No self extension allowed''')
	}
	
	private def boolean getNoEdges(Node it) { incoming.empty && outgoing.empty }
	
	private def checkSignature(GUISIB parentGUISIB) {
		val guiModel = parentGUISIB.rootElement
		val parentGUI = parentGUISIB.gui
		// collect required inputs
		val requiredInputVariables = parentGUI.inputVariables.associateWithKey[name].toMap
		val presentInputVariables = guiModel.inputVariables
		
		// Compare inputs with same name and check whether the present input variable 
		// represents a real sub type of the required input variable's type.
		// At least one has to be specialized.
		val noMatchingInput = presentInputVariables.associateWithKey [
			requiredInputVariables.get(name)
		]
		.filter [
			v1 != null
		]
		.filter [
			parentGUISIB.checkListStateEq(v1, v2)
		]
		.filter [
			parentGUISIB.checkVariable(v1, v2)
		].isEmpty
		if(noMatchingInput) parentGUISIB.addError('''No (valid) overriding input found for sub GUI "«guiModel.title»".''')
	}
	
	private dispatch def checkVariable(IdentifiableElement it, Variable v1, Variable v2) {
		false
	}
	
	private dispatch def checkVariable(IdentifiableElement it, ComplexVariable v1, ComplexVariable v2) {
		if (v2.dataType == v1.dataType) {
			addInfo('''Overridden input "«v1.name»" has same type as overriding input. So it does not override for real.''')
			false
		}
		else if (v2.dataType.isTypeOf(v1.dataType)) true
		else { 
			addError('''The type of overriding input "«v1.name» is not a sub type of the overriden input's type."''')
			false
		}
	}
	
	private dispatch def checkVariable(IdentifiableElement it, ComplexVariable v1, Variable v2) {
		addError('''Overridden input "«v1.name»" represents a complex type, but overriding input not.''')
		false
	}
	
	private dispatch def checkVariable(IdentifiableElement it, Variable v1, ComplexVariable v2) {
		addError('''Overriding input "«v1.name»" represents a complex type, but overridden input not.''')
		false
	}
	
	private dispatch def checkVariable(IdentifiableElement it, PrimitiveVariable v1, PrimitiveVariable v2) {
		if(v1.dataType != v2.dataType) {
			addError('''Overridden input "«v1.name»" has not same primitive type as overriding input.''')
		}
		addInfo('''Overridden input "«v1.name»" has same type as overriding input. So it does not override for real.''')
		false
	}
	
	private dispatch def checkVariable(IdentifiableElement it, PrimitiveVariable v1, Variable v2) {
		addError('''Overridden input "«v1.name»" represents a primitive type, but overriding input not.''')
		false
	}
	
	private dispatch def checkVariable(IdentifiableElement it, Variable v1, PrimitiveVariable v2) {
		addError('''Overriding input "«v2.name»" represents a primitive type, but overridden input not.''')
		false
	}
	
	private def checkListStateEq(IdentifiableElement it, Variable v1, Variable v2) {
		if (v1.isList != v2.isList) { 
			addError('''List type status for input "«v1.name»" differ.''')
			return false
		}
		return true
	}
}
