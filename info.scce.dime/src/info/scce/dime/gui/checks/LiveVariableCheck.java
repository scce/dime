/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks;

import graphmodel.Container;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.impl.ComplexVariableImpl;
import info.scce.dime.gui.mcam.modules.checks.GUICheck;

/**
 * Check for live variables:
 * 	- List-Variables without any predecessor can no be live.
 */
public class LiveVariableCheck extends GUICheck {

	@Override
	public String getName() {
		return "LiveVariableCheck";
	}
	
	@Override
	public void check(GUI model) {
		this.adapter
			.getEntityIds()
			.stream()
			.map(id -> id.getElement())
			.filter(e -> e instanceof ComplexVariableImpl && ((ComplexVariableImpl) e).isIsLive())
			.forEach(e -> checkLiveVariable((Container) e));
	}
		
	private void checkLiveVariable(final Container container) {
		ComplexVariableImpl complexVariable = (ComplexVariableImpl) container;
		if(complexVariable.isIsList() 
			&& (complexVariable.getComplexVariablePredecessors() == null || complexVariable.getComplexVariablePredecessors().isEmpty())) {
			this.addError(container, "List without any predecessor can not be live.");
		}
	}

}
