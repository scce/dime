/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks;

import java.util.Arrays;

import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.checks.AbstractCheck;

/**
 * The not allowed names check validates the names of all variables
 * 
 * @author zweihoff
 *
 */
public class NotAllowedNamesCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private String[] notAllowedVariableNames = {"iteratorElement"};
	
	/**
	 * Checks, if any variable has a not allowed name
	 */
	@Override
	public void doExecute(GUIAdapter adapter) {
		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Variable) {
				Variable var = (Variable) obj;
				if (Arrays.asList(notAllowedVariableNames).contains(var.getName()))
					addError(id, var.getName()
							+ " not allowed");
			}
		}
	}
	
	@Override
	public void init() {}
}
