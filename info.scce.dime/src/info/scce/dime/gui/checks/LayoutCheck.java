/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks;

import info.scce.dime.gui.gui.Col;
import info.scce.dime.gui.gui.ColSize;
import info.scce.dime.gui.gui.Row;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.checks.AbstractCheck;

/**
 * The layout check is used to validate if a given row
 * contains too much columns
 * @author zweihoff
 *
 */
public class LayoutCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;

	/**
	 * Checks, if the row components present in the
	 * given GUI model contain to much columns
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof Row)
				checkRowWidth(id, (Row) obj);
		}
	}

	/**
	 * Checks, if the summed width of all columns in the row
	 * exceed the maximum width of 12
	 * @param id
	 * @param row
	 */
	private void checkRowWidth(GUIId id, Row row) {
		int width = 0;
		for (Col col : row.getCols()) {
			width += mapWidenessToInt(col.getWideness());
		}
		if (width > 12) {
			addWarning(id, "Sum of column widths > 12");
		}
	}
	
	@Override
	public void init() {}
	
	/**
	 * Converts the column wideness constant to a corresponding integer value
	 * @param size
	 * @return
	 */
	private int mapWidenessToInt(ColSize size) {
		switch (size) {
		case FULL:
			return 12;
		case ELEVEN_TWELFTH:
			return 11;
		case FIVE_SIXTH:
			return 10;
		case THREE_QUARTER:
			return 9;
		case TWO_THIRD:
			return 8;
		case SEVEN_TWELFTH:
			return 7;
		case HALF:
			return 6;
		case FIVE_TWELFTH:
			return 5;
		case THIRD:
			return 4;
		case QUARTER:
			return 3;
		case SIXTH:
			return 2;
		case TWELFTH:
			return 1;
		default:
			return 0;
		}
	}
}
