/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks

import graphmodel.Node
import info.scce.dime.checks.GUICheck
import info.scce.dime.gui.gui.AddToSubmission
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.FormLoadSubmit
import info.scce.dime.gui.gui.FormSubmit
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Read

class BranchCheck extends GUICheck {
	
	override check(GUI model) {
		
		// check that branch ports are not connected
		model.find(Branch).flatMap[outputPorts].forEach[
			check[outgoing.isEmpty].elseError('''No outgoing edges allowed for port «it.name»''')
		]
		
		// check that modified variables can be persisted
		model.allEdges
			.filter[it instanceof FormSubmit || it instanceof FormLoadSubmit]
			.map[sourceElement]
			.forEach[
				check[isReadOrSubmitted].elseError("Modified variable is not persisted")
			]
	}
	
	def boolean isReadOrSubmitted(Node it) {
		switch it {
			Attribute: (container as ComplexVariable).isReadOrSubmitted
			default:
				! findSuccessorsVia(Read, AddToSubmission).isEmpty
				|| findPredecessorsVia(
						ComplexAttributeConnector, ComplexListAttributeConnector
				   ).exists[isReadOrSubmitted]
		}
	}
}
