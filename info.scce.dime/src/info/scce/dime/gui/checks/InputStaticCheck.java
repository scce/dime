/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.checks;

import info.scce.dime.gui.gui.EventListener;
import info.scce.dime.gui.gui.GUIPlugin;
import info.scce.dime.gui.gui.GUISIB;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.gui.gui.TextInputStatic;
import info.scce.dime.gui.gui.TimestampInputStatic;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import graphmodel.ModelElementContainer;
import info.scce.dime.checks.AbstractCheck;

/**
 * Checks static input port values for all GUI SIB and GUI plug in SIBs
 * present in the given GUI model
 * @author zweihoff
 *
 */
public class InputStaticCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;

	public static String[] invalid = {"\""};
	public static String[] invalid_with_mustache = {"\"","\\'","{{","}}"};
	public static String[] escapable = {};


	/**
	 * Checks all static text input ports present in the given GUI model
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if(obj instanceof MovableContainer)
				checkMovableContainer(id,(MovableContainer)obj);
			if (obj instanceof TextInputStatic)
				checkText(id, (TextInputStatic) obj);
			if (obj instanceof TimestampInputStatic)
				checkTimeStamp(id, (TimestampInputStatic) obj);
		}
	}
	
	private void checkTimeStamp(GUIId id, TimestampInputStatic obj) {
		if(String.valueOf(obj.getValue()).length() > 10){
			addError(id, "Only 10 digits allowed");
		}
		if(obj.getValue()<0){
			addError(id, "Timestamp has to be positive");
		}
		
	}

	@Override
	public void init() {}

	/**
	 * Checks, if the specified static textual value of 
	 * the given static text input port contains illegal characters
	 * or invalid but escapable characters
	 * @param id
	 * @param te
	 */
	private void checkText(GUIId id, TextInputStatic te) {
		String value = te.getValue();
		ModelElementContainer container = te.getContainer();
		if (container instanceof GUISIB
				|| container instanceof ProcessSIB
				|| container instanceof GUIPlugin
				|| container instanceof EventListener) {
			checkTextContent(id, value, invalid);
		} else {
			checkTextContent(id, value, invalid_with_mustache);
		}
	}
	
	private void checkTextContent(GUIId id, String value, String[] invalid){
		//check invalid characters
		if(value==null) {
			return;
		}
		for(String i:invalid){
			if(value.contains(i)){
				addError(id, "value contains illegal chars: "+i);
			}
		}
		
		//check escapable characters
		for(String i:escapable){
			String subString = value;
			while(subString.indexOf(i)>-1){
				int indexOf = subString.indexOf(i);
				if(indexOf > 0){
					if(subString.charAt(indexOf-1)!='\\'){
						addError(id, "value char: "+i+" has to be escaped");
					}
				}else{
					addError(id, "value char: "+i+" has to be escaped");
				}
				subString = subString.substring(indexOf+1);		
			}
		}
	}
	
	
	
	/**
	 * Checks, if the specified static textual value of 
	 * the given static text input port contains illegal characters
	 * or invalid but escapable characters
	 * @param id
	 * @param te
	 */
	private void checkMovableContainer(GUIId id, MovableContainer mc) {
		if(mc.getGeneralStyle()!=null){
			if(mc.getGeneralStyle().getTooltip()!=null){
				mc.getGeneralStyle().getTooltip().getContent().forEach(sc->checkTextContent(id, sc.getRawContent(), invalid));
			}
			checkTextContent(id, mc.getGeneralStyle().getRawContent(), invalid);

		}
	}
	
	
}
