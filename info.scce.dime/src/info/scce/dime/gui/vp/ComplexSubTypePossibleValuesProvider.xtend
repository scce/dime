/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.gui.vp

import info.scce.dime.api.DIMEValuesProvider
import info.scce.dime.data.data.EnumType
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.IS

class ComplexSubTypePossibleValuesProvider extends DIMEValuesProvider<IS, String> {
	
	override getPossibleValues(IS conditionIs) {
		val dataType = switch it: conditionIs.sourceElement {
			ComplexVariable: dataType.originalType
			ComplexAttribute: attribute.dataType.originalType
		}
		if (dataType instanceof EnumType) {
			dataType.enumLiterals.toMap([id], [name])
		} else {
			dataType.knownSubTypes.toMap([id], [name])
		}
	}
}
