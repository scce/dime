/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.vp

import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.api.DIMEValuesProvider

import static java.util.stream.Collectors.toMap

import static extension org.jooq.lambda.Seq.*

class ComplexSuperAttrPossibleValuesProvider extends DIMEValuesProvider<ComplexAttribute, ComplexAttribute> {
	
	override getPossibleValues(ComplexAttribute editedAttribute) {
		
		val typeOfEditedAttribute = (editedAttribute.getContainer() as Type)
		
		// start with all editedAttribute's type's attributes ...
		val it = typeOfEditedAttribute.inheritedAttributeSeq.seq.
			// ... exclude attributes defined in the same type as the editedAttribute ...
			removeAll(typeOfEditedAttribute.attributes.seq).
			// ... exclude all attributes that are not complex ...
			filter(ComplexAttribute).
			// ... exclude all attributes that are of the wrong type ...
			filter[dataType == editedAttribute.dataType]
			// ... and duplicate them for map creation
			.duplicate
		v1.zip(v2.map[name]).collect(toMap([v1], [v2]))
	}
}
