/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.vp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.scce.dime.api.DIMEValuesProvider;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.Type; 

public class PrimitiveSuperAttrPossibleValuesProvider extends DIMEValuesProvider<PrimitiveAttribute, PrimitiveAttribute>{

	@Override
	public Map<PrimitiveAttribute,String> getPossibleValues(PrimitiveAttribute editedAttribute) {

		final Type typeOfEditedAttribute = (Type) editedAttribute.getContainer();
		List<Attribute> allAttributes = _dataExtension.getInheritedAttributes(typeOfEditedAttribute);
		
		Map<PrimitiveAttribute,String> collect = new HashMap<PrimitiveAttribute, String>(); 
			// start with all editedAttribute's type's attributes
			allAttributes.stream()
			//defined in the same type as the editedAttribute
			.filter(a -> !typeOfEditedAttribute.getAttributes().contains(a))
			// remove all attributes that are not primitive
			.filter(a->a instanceof PrimitiveAttribute)
			// remove all attributes that are of the wrong primtive type
			.filter(a -> ((PrimitiveAttribute)a).getPrimitiveAttributeView().getDataType().equals(editedAttribute.getPrimitiveAttributeView().getDataType()))
			.forEach(n->collect.put((PrimitiveAttribute) n, n.getName()));
		return collect;
	
	}

}
