/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.checks;

import java.util.Arrays;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.EnumLiteral;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.mcam.adapter.DataAdapter;
import info.scce.dime.data.mcam.adapter.DataId;

public class NotAllowedNamesCheck extends AbstractCheck<DataId, DataAdapter> {

	private String[] notAllowedAttributeNames = {"dywaName", "dywaId", "dywaVersion","current","last","first","size"};
	private String[] notAllowedTypeNames = {};
	private String[] notAllowedCharacters = {"-"};
	
	@Override
	public void doExecute(DataAdapter adapter) {
		for (DataId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Type) {
				Type type = (Type) obj;
				if (Arrays.asList(notAllowedTypeNames).contains(type.getName()))
					addError(id, type.getName()
							+ " not allowed");
				Arrays.asList(notAllowedCharacters).forEach((n)->{
					if(type.getName().contains(n)) {
						addError(id, type.getName()
								+ " contains forbidden char "+n);
					}
				});
				
				for (Attribute attribute : type.getAttributes()) {
					
					DataId attrId = adapter.getIdByString(attribute.getId());
					
//					Arrays.asList(notAllowedCharacters).forEach((n)->{
//						if(attribute.getName().contains(n)) {
//							addError(id, attribute.getName()
//									+ " contains forbidden char "+n);
//						}
//					});
					
					if(!(attribute instanceof EnumLiteral)){
						if(attribute.getName().substring(0, 1).equals(attribute.getName().substring(0, 1).toUpperCase())) {
							addError(attrId, " first letter of "+attribute.getName()+" has to be lower case");
						}						
					}
					
					if (Arrays.asList(notAllowedAttributeNames).contains(attribute.getName()))
						addError(attrId, attribute.getName()
								+ " not allowed");
				}
			}
		}
		
	}
	
	@Override
	public void init() {}

}
