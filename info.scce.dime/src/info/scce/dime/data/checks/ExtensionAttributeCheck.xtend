/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.checks

import info.scce.dime.data.data.Data
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.data.mcam.modules.checks.DataCheck
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.data.data.ReferencedExtensionAttribute

class ExtensionAttributeCheck extends DataCheck {
	
	extension DataExtension = DataExtension.instance
	
	override check(Data model) {
		model.types.flatMap[extensionAttributes].forEach[checkAtribute]
	}
	
	def checkAtribute(ExtensionAttribute model) {
		val process = switch model {
			ReferencedExtensionAttribute:
				model.referencedAttribute.process as info.scce.dime.process.process.Process
			default: model.process as info.scce.dime.process.process.Process
		}
		//check correct input requirements
		//check exactly one complex output port is present
		if(process.startSIBs.size!=1 || process.startSIBs.get(0).outputs.size!=1 || process.startSIBs.get(0).complexOutputPorts.size!=1) {
			model.addError('''process has not exactly 1 StartSIB with 1 ComplexOutputPort''')
			return
		}
		//check complex output port type equals (or is sub type of) attribute type
		val startType = process.startSIBs.get(0).complexOutputPorts.get(0).dataType.originalType
		val modelType = model.container.originalType
		if (!modelType.isTypeOf(startType)){
			model.addError('''process ComplexOutputPort dataType incompatible with type''')
			return
		}
		
		//check output requirements
		if(process.endSIBs.size!=1 || process.endSIBs.get(0).inputs.size!=1) {
			model.addError('''process has not exactly 1 EndSIB with 1 Input''')
			return
		}
		
		//check sync of datatype and list status
		val port = process.endSIBs.get(0).inputs.get(0)
		switch(port) {
			ComplexInputPort: {
				if(model.isIsList!=port.isIsList) {
					model.addError('''extension attribute "«model.name»" list status should be "«port.isIsList»"''')
				}
				if(!model.dataType.equals(port.dataType.name)){
					model.addError('''extension attribute "«model.name»" datatype should be "«port.dataType.name»"''')
				}
			}
			PrimitiveInputPort: {
				if(model.isIsList!=port.isIsList) {
					model.addError('''extension attribute "«model.name»" list status should be "«port.isIsList»"''')
				}
				if(!model.dataType.equals(port.dataType.literal)){
					model.addError('''extension attribute "«model.name»" datatype should be "«port.dataType.literal»"''')
				}
			}
			InputStatic: {
				if(model.isIsList!=false) {
					model.addError('''extension attribute "«model.name»" list status should be "false"''')
				}
				val primitiveTyeName = switch(port) {
					TextInputStatic: "Text"
					IntegerInputStatic: "Integer"
					RealInputStatic: "Real"
					TimestampInputStatic: "Timestamp"
					BooleanInputStatic: "Boolean"
				}
				if(!model.dataType.equals(primitiveTyeName)){
					model.addError('''extension attribute "«model.name»" datatype should be "«primitiveTyeName»"''')
				}
				
			}
		}
		

	}
}
