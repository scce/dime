/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.checks;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.data.data.UserType;
import info.scce.dime.data.hooks.UserTypePostCreate;
import info.scce.dime.data.mcam.adapter.DataAdapter;
import info.scce.dime.data.mcam.adapter.DataId;
import info.scce.dime.checks.AbstractCheck;

public class UserTypeCheck extends AbstractCheck<DataId, DataAdapter> {

	@Override
	public void doExecute(DataAdapter adapter) {
		for (DataId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof UserType) {
				boolean hasUsername = false;
				boolean hasPassword = false;
				
				for (Attribute attr : ((UserType) obj).getAttributes()) {
					if (UserTypePostCreate.attrUsernameName.equals(attr.getName()))
							if (attr instanceof PrimitiveAttribute) 
								if (((PrimitiveAttribute) attr).getDataType().equals(PrimitiveType.TEXT))
									hasUsername = true;
					
					if (UserTypePostCreate.attrPasswordName.equals(attr.getName()))
						if (attr instanceof PrimitiveAttribute) 
							if (((PrimitiveAttribute) attr).getDataType().equals(PrimitiveType.TEXT))
								hasPassword = true;
								
				}
				
				if (!hasUsername)
					addError(id, "missing primitive attribute '" + UserTypePostCreate.attrUsernameName + "' (TEXT)");
				if (!hasPassword)
					addError(id, "missing primitive attribute '" + UserTypePostCreate.attrPasswordName + "' (TEXT)");
			}
		}
		
	}

	@Override
	public void init() {}

}
