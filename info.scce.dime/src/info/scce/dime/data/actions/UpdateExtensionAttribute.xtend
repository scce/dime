/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.process.process.BooleanInputStatic

class UpdateExtensionAttribute extends DIMECustomAction<ExtensionAttribute> {
	
	override getName() {
		"Update Extension Attribute"
	}

	override canExecute(ExtensionAttribute attr) {
		attr.process !== null &&
		!(attr.process as info.scce.dime.process.process.Process).endSIBs.empty &&
		!(attr.process as info.scce.dime.process.process.Process).endSIBs.get(0).inputs.empty
	}

	override void execute(ExtensionAttribute attr) {
		val process = attr.process as info.scce.dime.process.process.Process
		val port = process.endSIBs.get(0).inputs.get(0)
		switch(port) {
			ComplexInputPort: {
				attr.isList = port.isIsList
				attr.dataType = port.dataType.name
			}
			PrimitiveInputPort: {
				attr.isList = port.isIsList
				attr.dataType = port.dataType.literal
			}
			InputStatic: {
				attr.isList = false
				attr.dataType = switch(port) {
					TextInputStatic: "Text"
					IntegerInputStatic: "Integer"
					RealInputStatic: "Real"
					TimestampInputStatic: "Timestamp"
					BooleanInputStatic: "Boolean"
				}
			}
		}
	}
}
