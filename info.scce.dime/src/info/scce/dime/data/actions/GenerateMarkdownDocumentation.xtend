/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumLiteral
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.Type
import java.io.File
import java.io.IOException
import org.apache.commons.io.FileUtils

class GenerateMarkdownDocumentation<T extends Data> extends DIMECustomAction<T> {
	
	override getName() {
		"Create Documentation"
	}

	override canExecute(Data d) {
		true
	}
	
	override hasDoneChanges() {
		false
	}

	override void execute(Data d) {
		val filePath = d.file.project.location.append('''../documentation/data/«d.modelName».md'''.toString).toString
		createFile(createDoc(d).toString,filePath)
		
		d.types.forEach[t|{
			val typeFilePath = d.file.project.location.append('''../documentation/data/«d.modelName»/«t.name».md'''.toString).toString
			createFile(getTypeDoc(t).toString,typeFilePath)
		}]
		
	}
	
	/**
	 * Helper method to create a file with the given content on the given path.
	 * @param content
	 * @param path
	 * @throws IOException
	 */
	def void createFile(String content,String path) throws IOException
	{
		val File f = new File(path);
		f.getParentFile().mkdirs(); 
		f.createNewFile();
		
		FileUtils.writeStringToFile(f,content);
	}
	
	def createDoc(Data d)
	'''
	# «d.modelName»
	 
	«d.documentation.doc»
	 
	### User Types
	 
	«d.userTypes.map['''* [«name»](./«d.rootElement.modelName»/«name».md)'''].join("\n")»
	 
	### Abstract Types
	 
	«d.abstractTypes.map['''* [«name»](./«d.rootElement.modelName»/«name».md)'''].join("\n")»
	 
	### Concrete Types
	 
	«d.concreteTypes.map['''* [«name»](./«d.rootElement.modelName»/«name».md)'''].join("\n")»
	 
	### Enumerations
	 
	«d.enumTypes.map['''* [«name»](./«d.rootElement.modelName»/«name».md)'''].join("\n")»
	 
	### Referenced Types
	 
	«d.referencedTypes.map['''* [«name»](./«d.rootElement.modelName»/«name».md)'''].join("\n")»
	
	'''
	
	def getTypeDoc(Type t)'''
	# «t.typeName» «IF! t.superTypes.empty»extends «t.superTypes.map['''[«it.name»](«it.name».md)'''].toList.join(", ")»«ENDIF»
	 
	«t.documentation.doc»
	 
	«IF !t.superTypes.empty»
	«t.superTypes.map[attributes].flatten.map[attributeDoc].join("\n")»
	«ENDIF»
	«IF !t.attributes.empty»
	«t.attributes.map[attributeDoc].join("\n")»
	«ENDIF»
	'''
	
	def attributeDoc(Attribute attr)
	'''
	* *«attr.name»* «IF !(attr instanceof EnumLiteral)»: «attr.typeName»«ENDIF»«IF !attr.documentation.nullOrEmpty» «attr.documentation»«ENDIF»
	'''
	
	def typeName(Type t) {
		var s =""
		switch(t) {
			AbstractType: s='''_(abstract)_ '''
			EnumType: s='''_(enumeration)_ '''
			ReferencedType: s='''_(referenced)_ '''
		}
		return s+t.name
	}
	
	def typeName(Attribute t) {
		var s =''''''
		switch(t) {
			PrimitiveAttribute: s='''«t.dataType.literal»'''
			ComplexAttribute: s='''[«t.dataType.name»](./«s».md)«IF t.superAttr!=null» -> «t.superAttr.name» of [«t.superAttr.type.name»](./«t.superAttr.type.name».md)«ENDIF»'''
		}
		return '''«IF t.isIsList»List<«ENDIF»«s»«IF t.isIsList»>«ENDIF»'''
	}
	
	def doc(String s){
		if(!s.nullOrEmpty) {
			return '''
			>>>
			«s»
			<<<
			'''
		}
		""
	}
	
}
