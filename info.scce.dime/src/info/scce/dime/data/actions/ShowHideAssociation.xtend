/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.Association
import info.scce.dime.data.data.BidirectionalAssociation
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.Type
import java.util.ArrayList

class ShowHideAssociation<T extends ComplexAttribute> extends DIMECustomAction<T> {
	
	override getName() {
		"Toggle Show/Hide Association"
	}
	
	override canExecute(ComplexAttribute ca) {
		ca.correspondingAssociation !== null
	}

	// TODO beautify after auto conversion to Xtend class
	
	override void execute(ComplexAttribute ca) {
		println(ca.isIsList)
		var Association asso = getCorrespondingAssociation(ca)
		asso.setIsHidden(!asso.isIsHidden())
		
	}

	def private getCorrespondingAssociation(ComplexAttribute ca) {
		val type = (ca.getContainer() as Type)
		val allAssociations = new ArrayList<Association>(type.getIncoming(typeof(Association)))
		allAssociations.addAll(type.getOutgoing(typeof(Association)))
		for (Association asso : allAssociations) {
			if(((asso as Association)).getSourceAttr().equals(ca))
				return (asso as Association)
			if(asso instanceof BidirectionalAssociation)
				if(((asso as BidirectionalAssociation)).getTargetAttr().equals(ca))
					return (asso as Association)
		}
		return null
	}
}
