/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.hooks;

import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.TypeLayoutUtils;

public class UserTypePostCreate extends TypePostCreate {
	
	public static String attrUsernameName = "username";
	public static String attrPasswordName = "password";

	@Override
	public void postCreate(Type ut) {
		super.postCreate(ut);
		
		PrimitiveAttribute cAttrName = ut.newPrimitiveAttribute(1, 1);
		cAttrName.getPrimitiveAttributeView().setName(attrUsernameName);
		cAttrName.getPrimitiveAttributeView().setDataType(PrimitiveType.TEXT);
		cAttrName.getPrimitiveAttributeView().setIsList(false);
		
		PrimitiveAttribute cAttrPassword = ut.newPrimitiveAttribute(1, 1);
		cAttrPassword.getPrimitiveAttributeView().setName(attrPasswordName);
		cAttrPassword.getPrimitiveAttributeView().setDataType(PrimitiveType.TEXT);
		cAttrPassword.getPrimitiveAttributeView().setIsList(false);
		
		TypeLayoutUtils.resizeAndLayout((Type) ut);
	}

}
