/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.hooks;

import java.util.List;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.BidirectionalAttribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.InheritorType;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.ReferencedBidirectionalAttribute;
import info.scce.dime.data.data.ReferencedComplexAttribute;
import info.scce.dime.data.data.ReferencedExtensionAttribute;
import info.scce.dime.data.data.ReferencedPrimitiveAttribute;
import info.scce.dime.data.data.ReferencedType;
import info.scce.dime.data.data.ReferencedUserAttribute;
import info.scce.dime.data.data.ReferencedUserType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserAttribute;
import info.scce.dime.data.helper.TypeLayoutUtils;

public class ReferencedTypePostCreate extends DIMEPostCreateHook<Type>{

	@Override
	public void postCreate(Type referencedType) {
//			CData rootModel = DataWrapper.wrapGraphModel(referencedType.getRootElement(), getDiagram());
//			CType cReferencedType = rootModel.findCType(referencedType);
			
			Type lc = getReferencedType(referencedType);
			
			List<Attribute> attributes = _dataExtension.getInheritedAttributes(lc);
			
			referencedType.setName(lc.getName());
			
			for (Attribute lcAttribute : attributes) {
				if (lcAttribute instanceof UserAttribute) {
					ReferencedUserAttribute refAttr = referencedType.newReferencedUserAttribute(lcAttribute, 1, 1);
					refAttr.setName(lcAttribute.getName());
				}
				else if (lcAttribute instanceof BidirectionalAttribute) {
					ReferencedBidirectionalAttribute refAttr = referencedType.newReferencedBidirectionalAttribute(lcAttribute, 1, 1);
					refAttr.setName(lcAttribute.getName());
				}
				else if (lcAttribute instanceof ComplexAttribute) {
					ReferencedComplexAttribute refAttr = referencedType.newReferencedComplexAttribute(lcAttribute, 1, 1);
					refAttr.setName(lcAttribute.getName());
				}
				else if (lcAttribute instanceof PrimitiveAttribute) {
					ReferencedPrimitiveAttribute refAttr = referencedType.newReferencedPrimitiveAttribute(lcAttribute, 1, 1);
					refAttr.setName(lcAttribute.getName());
				}
				else if (lcAttribute instanceof ExtensionAttribute) {
					ExtensionAttribute ae = (ExtensionAttribute) lcAttribute;
					ReferencedExtensionAttribute refAttr = referencedType.newReferencedExtensionAttribute(lcAttribute, 1, 1);
					refAttr.setIsList(ae.isIsList());
//					refAttr.setDataType(ae.getDataType());
					refAttr.setName(lcAttribute.getName());
				}
				else {
					throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
				}
				
			}
			TypeLayoutUtils.resizeAndLayout(referencedType);
	}
	
	private Type getReferencedType(Type t) {
		if (t instanceof ReferencedType) {
			return ((ReferencedType) t).getReferencedType();
		}
		if (t instanceof ReferencedUserType) {
			return ((ReferencedUserType) t).getReferencedType();
		}
		return null;
	}

}
