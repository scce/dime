/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Association;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.Type;

public class AssociationInitialize extends DIMEPostCreateHook<Association>{

	@Override
	public void postCreate(Association association) {
		try {
//			CData rootModel = DataWrapper.wrapGraphModel(association.getRootElement(), getDiagram());
//			CAssociation cAssociation = rootModel.findCAssociation(association);
			
			//TODO: remove cast once incoming/outgoing with inheritance has been fixed
     		Type sourceType = (Type) association.getSourceElement();

			ComplexAttribute cComplexAttribute = sourceType.newComplexAttribute(1,1);

			// TODO why not CType ?!?
			//TODO: remove cast once incoming/outgoing with inheritance has been fixed
			cComplexAttribute.getComplexAttributeView().setDataType((Type) association.getTargetElement());
			
			//TODO: remove cast once incoming/outgoing with inheritance has been fixed
			final String targetTypeName = ((Type)association.getTargetElement()).getName();
			cComplexAttribute.setName(targetTypeName.substring(0, 1).toLowerCase() + targetTypeName.substring(1));
			
			// TODO why isn't here CComplexAttribute necessary?!?
			// answer: C-API seems broken for newly re-introduced direct
			// referencing of nodes and edges.
			// TODO: repair workaround once #15424 is resolved
			association.setSourceAttr(cComplexAttribute);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
