/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Data
import java.util.Comparator
import java.util.LinkedList
import java.util.List

class AttributePostDelete extends CincoPostDeleteHook<Attribute> {
	
	override getPostDeleteFunction(Attribute attribute) {
		val data = attribute.getRootElement()
		return [data.sortOrder]
	}
	
	def sortOrder(Data data) {
		for( type: data.getTypes()){
			var List<Attribute> allAttributes = type.getAttributes().toList;
			var LinkedList<Attribute> myAttr = new LinkedList<Attribute>();
			myAttr.addAll(allAttributes);
			myAttr.sort(new Comparator<Attribute>() {
				
				override compare(Attribute o1, Attribute o2) {
					return o1.getName().compareTo(o2.getName());
				}

				
			});
			//new psotions
			var offset = 32;
			for(Attribute attr : myAttr){
				attr.moveTo(attr.getContainer(), attr.getX(), offset);
				offset+= 18;
			}
		}
	}
}
