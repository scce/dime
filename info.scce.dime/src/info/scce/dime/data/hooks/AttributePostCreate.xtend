/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.data.actions.UpdateExtensionAttribute
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.helper.TypeLayoutUtils
import info.scce.dime.process.process.Process
import info.scce.dime.util.DataID
import info.scce.dime.data.data.ReferencedExtensionAttribute

class AttributePostCreate extends DIMEPostCreateHook<Attribute> {
	
	override postCreate(Attribute attribute) {
		
		assertUniqueDataID(attribute)
		
		val extattr = switch attribute {
			ReferencedExtensionAttribute: attribute.referencedAttribute
			ExtensionAttribute: attribute
		}
		
		if (extattr instanceof ExtensionAttribute) {
			attribute.name = (extattr.process as Process).modelName
			val uea = new UpdateExtensionAttribute
			if (uea.canExecute(extattr)) {
				uea.execute(extattr)
			}
		}
		
		TypeLayoutUtils.resizeAndLayout(attribute.container)
		sortOrder(attribute)
	}
	
	def assertUniqueDataID(Attribute attr) {
		val model = attr.rootElement
		val attrs = 
			model.find(Attribute)
				.filter[it.findParents(ReferencedType).isEmpty]
				.filter[it.findParents(ReferencedUserType).isEmpty]
				.filter[it.findParents(ReferencedEnumType).isEmpty]
				.filter[it != attr]
				.toSet
		
		DataID.assertUniqueDataID(attr, attrs)
	}

	def sortOrder(Attribute attribute) {
		val data = attribute.rootElement
		for (type : data.types) {
			val sortedAttrs = type.attributes.sortBy[it.name]
			var y = 32
			for (attr : sortedAttrs) {
				attr.moveTo(attr.container, attr.x, y)
				y += 18
			}
		}
	}
}
