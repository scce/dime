/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostSaveHook
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Data
import java.util.Comparator
import java.util.LinkedList

class DataPostSave extends CincoPostSaveHook<Data>{
	
	override postSave(Data data) {
		for(type: data.types){
			var allAttributes = type.attributes
			var myAttr = new LinkedList
			myAttr.addAll(allAttributes.toList)
			myAttr.sort(new Comparator<Attribute>{
				
				override compare(Attribute o1, Attribute o2) {
					return o1.name.compareTo(o2.name)
				}
				
			})
			//new psotions
			var offset = 32;
			for(attr : myAttr){
				attr.moveTo(attr.container, attr.x, offset)
				offset+= 18
			}
			
		}
	}
	
}
