/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserAssociation;
import info.scce.dime.data.data.UserAttribute;
import info.scce.dime.data.helper.TypeLayoutUtils;

public class UserAssociationInitialize extends DIMEPostCreateHook<UserAssociation>{

	@Override
	public void postCreate(UserAssociation association) {

//		CData rootModel = DataWrapper.wrapGraphModel(association.getRootElement(), getDiagram());
//		CUserAssociation cAssociation = rootModel.findCUserAssociation(association);

		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		Type sourceType = (Type) association.getSourceElement();
		UserAttribute cSourceAttribute = sourceType.newUserAttribute(1, 1);
		TypeLayoutUtils.resizeAndLayout(sourceType);
		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		cSourceAttribute.setDataType((Type) association.getTargetElement());
		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		final String targetTypeName = ((Type)association.getTargetElement()).getName();
		cSourceAttribute.setName(targetTypeName.substring(0, 1).toLowerCase() + targetTypeName.substring(1));
		cSourceAttribute.setIsList(true);

		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		Type targetType = (Type) association.getTargetElement();
		UserAttribute cTargetAttribute = targetType.newUserAttribute(1, 1);
		TypeLayoutUtils.resizeAndLayout(targetType);
		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		cTargetAttribute.setDataType((Type) association.getSourceElement());
		final String sourceTypeName = ((Type)association.getSourceElement()).getName();
		cTargetAttribute.setName(sourceTypeName.substring(0, 1).toLowerCase() + sourceTypeName.substring(1));

		// TODO: repair workaround once #15424 is resolved
		UserAttribute sourceAttribute = (UserAttribute)cSourceAttribute;
		UserAttribute targetAttribute = (UserAttribute)cTargetAttribute;

		cSourceAttribute.setOppositeAttribute(targetAttribute);
		cTargetAttribute.setOppositeAttribute(sourceAttribute);

		association.setTargetAttr(sourceAttribute);
		association.setSourceAttr(targetAttribute);

	}

}
