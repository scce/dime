/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.helper;

import static info.scce.dime.process.helper.LayoutConstants.*;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.EnumLiteral;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.TypeAttribute;
import info.scce.dime.data.data.UserAttribute;
import info.scce.dime.process.helper.NodeLayout;

public class TypeLayoutUtils {
	
	
	
	/**
	 * 
	 * Resizes cType's height so that all contained {@link CAttribute}s (minus the one
	 * that is about to be deleted) fit, positions them within cType and resizes their
	 * width according to cType's width. 
	 * 
	 * @param cType the layouted type
	 * @param doomedAttribute The attribute that is about to be deleted. Will be ignored for layouting.
	 */
	public static void resizeAndLayoutBeforeDelete(Type cType, Attribute doomedAttribute) {

		int x = ATTR_X;
		int y = TYPE_FIRST_ATTR_Y;

		int attributeAmount = doomedAttribute == null ? cType.getAttributes().size() : cType.getAttributes().size() -1;
		cType.resize(cType.getWidth(), NodeLayout.getTypeHeight(attributeAmount));
		
		if (cType instanceof EnumType) {
			// In Enums, the Literals are layouted before all other attributes.
			for (Attribute cAttribute : cType.getAttributes()) {
				if (!cAttribute.equals(doomedAttribute) && cAttribute instanceof EnumLiteral) {
					cAttribute.moveTo(cType, x, y);
					cAttribute.resize(cType.getWidth()-2*ATTR_X, cAttribute.getHeight());
					y += ATTR_SPACE;
				}
			}
			for (Attribute cAttribute : cType.getAttributes()) {
				if (!cAttribute.equals(doomedAttribute) && cAttribute instanceof TypeAttribute) {
					cAttribute.moveTo(cType, x, y);
					cAttribute.resize(cType.getWidth()-2*ATTR_X, cAttribute.getHeight());
					y += ATTR_SPACE;
				}
			}
		}
		else {
			// in all other types, the userAttribute (if present) is layouted first
			for (Attribute cAttribute : cType.getAttributes()) {
				if (!cAttribute.equals(doomedAttribute) && cAttribute instanceof UserAttribute) {
					cAttribute.moveTo(cType, x, y);
					cAttribute.resize(cType.getWidth()-2*ATTR_X, cAttribute.getHeight());
					y += ATTR_SPACE;
				}
			}
			for (Attribute cAttribute : cType.getAttributes()) {
				if (!cAttribute.equals(doomedAttribute) && !(cAttribute instanceof UserAttribute)) {
					cAttribute.moveTo(cType, x, y);
					cAttribute.resize(cType.getWidth()-2*ATTR_X, cAttribute.getHeight());
					y += ATTR_SPACE;
				}
			}
		}
		
	}
	
	/**
	 * Resizes cType's height so that all contained {@link CAttribute}s fit,
	 * positions them within cType and resizes their width according to cType's width.
	 * 
	 * @param cType
	 */
	public static void resizeAndLayout(Type cType) {
		resizeAndLayoutBeforeDelete(cType, null);
	}

}
