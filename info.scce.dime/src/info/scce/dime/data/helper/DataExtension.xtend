/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.helper

import de.jabc.cinco.meta.runtime.xapi.CollectionExtension
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Inheritance
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.ReferencedBidirectionalAttribute
import info.scce.dime.data.data.ReferencedComplexAttribute
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedPrimitiveAttribute
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserAttribute
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserType
import java.util.List
import org.jooq.lambda.Seq

import static extension org.jooq.lambda.Seq.*
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.data.data.ReferencedExtensionAttribute
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic

class DataExtension {
	
	protected extension GraphModelExtension = new GraphModelExtension
	protected extension CollectionExtension = new CollectionExtension
	
	/*
	 * Singleton pattern (keep this class stateless!)
	 */
	static DataExtension _instance
	static def getInstance() {
		_instance ?: (_instance = new DataExtension)
	}
	private new() {}
	
	/**
	 * Sorts the given sequence of types topologically by the inheritance
	 * relation ascending. This means, the leaves in the inheritance DAG (we support
	 * multiple inheritance) come before their parent types and so on.
	 * 
	 * If you iterate through such a list from left to right, more specific types are 
	 * evaluated before less specific types, so that no "shadowing" occurs.
	 * 
	 * @param it the sequence of types.
	 * @return a sorted sequence containing all sub types of the given type in the same data model.
	 */
	def List<Type> sortTopologically(Seq<Type> it) {
		sorted[ a, b |
			if (a == b) 0
			else if (a.superTypes.contains(b)) -1
			else if (b.superTypes.contains(a)) 1
			else 0
		].toList
	}
	
	/**
	 * Collects all sub types of the given type in the data model of the type.
	 * 
	 * @param it the (super) type. All sub types in the same data model are returned.
	 * @return a sequence containing all sub types of the given type in the same data model.
	 */
	def Seq<Type> getKnownSubTypes(Type it) {
		knownSubTypesRecursive.seq.distinct
	}
	
	private def List<Type> getKnownSubTypesRecursive(Type it) {
		Seq.of(it).concat(
			getIncoming(Inheritance).seq.flatMap[sourceElement.knownSubTypesRecursive]
		).map[originalType].toList
	}
	
	/**
	 * Collects all attributes of type, including the ones that are inherited from parent types
	 * and returns them.
	 * 
	 * Does not add those inherited attributes that are specialized/overriden, i.e., it only adds the 
	 * most specific one.
	 * 
	 * @param type the type to search for attributes.
	 * @return the sequence of leaf attributes.
	 */
	def Seq<Attribute> getInheritedAttributeSeq(Type type) {
		val originalType = type.originalType
		val types = Seq.of(originalType).concat(originalType.superTypes.map[it.originalType])
		val attrs = types.flatMap[attributes].toMap[name].values
		
		
		
		val attrDupes = attrs.duplicate
		val attrDupes2 = attrDupes.v1.duplicate
		
		val attrs1 = attrDupes.v2
		val attrs2 = attrDupes2.v1
		val attrs3 = attrDupes2.v2
		
		// The result should be a sequence of attributes.
		Seq.<Attribute>empty
		// Add only complex attributes that are "overriding leaves".
		.concat(
			attrs1.ofType(ComplexAttribute).removeByAssociation[superAttr]
		)
		// Add only primitive attributes that are "overriding leaves".
		.concat(
			attrs2.ofType(PrimitiveAttribute).removeByAssociation[superAttr]
		)
		// Add all attributes that are neither complex nor primitive
		.concat(
			attrs3.filter[!(it instanceof ComplexAttribute)].filter[!(it instanceof PrimitiveAttribute)]
		)
	}
	
	/**
	 * Collects all attributes of type, including the ones that are inherited from parent types
	 * and adds them to collectedAttributes.
	 * 
	 * Does not add those inherited attributes that are specialized, i.e., it only adds the 
	 * most specific one.
	 * 
	 * @param type the type to search attributes for.
	 * @return the list of attributes (as described above).
	 */
	def List<Attribute> getInheritedAttributes(Type type) {
		type.inheritedAttributeSeq.toList
	}
	
	/**
	 * Retrieves all super types of the given type, recursively.
	 * 
	 * @param the type to search super types for.
	 * @return the sequence of super types recursively until the root type.
	 */
	def Seq<Type> getSuperTypes(Type type) {
		type.findSuccessorsVia(Inheritance).filter(Type).seq
	}
	
	/**
	 * Retrieves root super types of the given type, recursively. 
	 * If the type has no super type, the type itself is returned.
	 * 
	 * @param the type to search root types for.
	 * @return the sequence of root super types.
	 */
	def Seq<Type> getRootTypes(Type type) {
		Seq.of(type).concat(type.superTypes).filter[
			directSuperTypes.empty
		]
	}
	
	
	/**
	 * Retrieves all direct super types (<strong>not</strong> super types of the super types).
	 * 
	 * @param teh type to search direct super types for.
	 * @return the sequence of direct super types.
	 */
	def Seq<Type> getDirectSuperTypes(Type type) {
		type.originalType.getOutgoing(Inheritance).seq.map[targetElement.originalType]
	}
	
	/**
	 * Checks whether the given type is an abstract type.
	 * 
	 * @param type the type to check.
	 * @return whether the given type is an abstract type.  
	 */
	def isAbstract(Type type) {
		type.originalType instanceof AbstractType
	}
	
	/**
	 * Checks whether the given <code>type</code> is a sub type of the given <code>superType</code>.
	 * 
	 * @param type the type to check whether it is a sub type.
	 * @param superType the potential super type.
	 * @return whether the given <code>type</code> is a sub type of the given <code>superType</code>.
	 */
	def boolean isTypeOf(Type type, Type superType) {
		val originalType = type.originalType
		val originalSuperType = superType.originalType
		if (originalType == originalSuperType) return true
		else originalType.superTypes.contains(originalSuperType);
	}
	
	def isBooleanAttribute(Attribute attr) {
		attr.hasPrimitiveType(PrimitiveType.BOOLEAN)
	}
	
	def isFileAttribute(Attribute attr) {
		attr.hasPrimitiveType(PrimitiveType.FILE)
	}
	
	def isIntegerAttribute(Attribute attr) {
		attr.hasPrimitiveType(PrimitiveType.INTEGER)
	}
	
	def isRealAttribute(Attribute attr) {
		attr.hasPrimitiveType(PrimitiveType.REAL)
	}
	
	def isTextAttribute(Attribute attr) {
		attr.hasPrimitiveType(PrimitiveType.TEXT)
	}
	
	def isTimestampAttribute(Attribute attr) {
		attr.hasPrimitiveType(PrimitiveType.TIMESTAMP)
	}
	
	def hasPrimitiveType(Attribute attr, PrimitiveType type) {
		switch attr {
			PrimitiveAttribute: attr.dataType == type
		}
	}
	
	/**
	 * Returns types of extension attribute
	 */
	def getExtensionAttributePort(ExtensionAttribute ea) {
		val p = ea.process as info.scce.dime.process.process.Process
		if(p.endSIBs.empty || p.endSIBs.get(0).inputs.empty) {
			return null
		}
		p.endSIBs.get(0).inputs.get(0)
	}
	
	def getExtensionAttributePort(ReferencedExtensionAttribute ea) {
		getExtensionAttributePort(ea.referencedAttribute)
	}
	
	def getComplexExtensionAttributeType(ExtensionAttribute ea) {
		val input = ea.extensionAttributePort
		if(input !== null && input instanceof ComplexInputPort) {
			return (input as ComplexInputPort).dataType.originalType			
		}
		null
	}
	
	def getComplexExtensionAttributeType(ReferencedExtensionAttribute ea) {
		getComplexExtensionAttributeType(ea.referencedAttribute)
	}
	
	def PrimitiveType getPrimitiveExtensionAttributeType(ExtensionAttribute ea) {
		val port = ea.extensionAttributePort
		if(port === null) {
			return null
		}
		if(port instanceof PrimitiveInputPort) {
			return port.dataType.toData
		}
		if(port instanceof InputStatic) {
			return port.toData
		}
		null
	}
	
	def PrimitiveType getPrimitiveExtensionAttributeType(ReferencedExtensionAttribute ea) {
		getPrimitiveExtensionAttributeType(ea.referencedAttribute)
	}
	
	def getComplexDataType(Attribute attribute) {
		if(attribute instanceof ExtensionAttribute) {
			return attribute.complexExtensionAttributeType
		}
		if(attribute instanceof ComplexAttribute) {
			return attribute.dataType
		}
		null
	}
	
	def PrimitiveType getPrimitiveDataType(Attribute attribute) {
		if(attribute instanceof ExtensionAttribute) {
			return attribute.primitiveExtensionAttributeType
		}
		if(attribute instanceof PrimitiveAttribute) {
			return attribute.dataType
		}
		null
	}
	
	def boolean isPrimitive(Attribute attribute) {
		if(attribute instanceof PrimitiveAttribute) {
			return true
		}
		if(attribute instanceof ExtensionAttribute) {
			return attribute.primitiveExtensionAttributeType !== null
		}
		
		return false
	}
	def boolean isComplex(Attribute attribute) {
		if(attribute instanceof ComplexAttribute) {
			return true
		}
		if(attribute instanceof ExtensionAttribute) {
			return attribute.complexExtensionAttributeType !== null
		}
		
		return false
	}
	
	
	/**
	 * Get the original type of this (potentially) referenced type.
	 * 
	 * @param type the (potentially) referenced type.
	 * @return the original type if the type is a referenced type or itself.
	 */
	def Type getOriginalType(Type type) {
		if(type===null) {
			throw new IllegalStateException("Type null")
		}
		switch it : type {
			ReferencedType: referencedType.originalType
			ReferencedUserType: referencedType.originalType
			ReferencedEnumType: referencedType.originalType
			default: it	
		}
	}
	
	/**
	 * Maps a primitive type (i.e. the enum value) of gui models to 
	 * primitive types of data models.
	 * 
	 * @param it the primitive type enum value of gui.
	 * @return the corresponding primitive type enum value of data. 
	 */
	def toData(info.scce.dime.gui.gui.PrimitiveType it) {
		switch it {
			case info.scce.dime.gui.gui.PrimitiveType.BOOLEAN: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			case info.scce.dime.gui.gui.PrimitiveType.FILE: info.scce.dime.data.data.PrimitiveType.FILE
			case info.scce.dime.gui.gui.PrimitiveType.INTEGER: info.scce.dime.data.data.PrimitiveType.INTEGER
			case info.scce.dime.gui.gui.PrimitiveType.REAL: info.scce.dime.data.data.PrimitiveType.REAL
			case info.scce.dime.gui.gui.PrimitiveType.TEXT: info.scce.dime.data.data.PrimitiveType.TEXT
			case info.scce.dime.gui.gui.PrimitiveType.TIMESTAMP: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}
	
	/**
	 * Maps a primitive type (i.e. the enum value) of process models to 
	 * primitive types of data models.
	 * 
	 * @param it the primitive type enum value of process.
	 * @return the corresponding primitive type enum value of data. 
	 */
	def toData(info.scce.dime.process.process.PrimitiveType it) {
		switch it {
			case info.scce.dime.process.process.PrimitiveType.BOOLEAN: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			case info.scce.dime.process.process.PrimitiveType.FILE: info.scce.dime.data.data.PrimitiveType.FILE
			case info.scce.dime.process.process.PrimitiveType.INTEGER: info.scce.dime.data.data.PrimitiveType.INTEGER
			case info.scce.dime.process.process.PrimitiveType.REAL: info.scce.dime.data.data.PrimitiveType.REAL
			case info.scce.dime.process.process.PrimitiveType.TEXT: info.scce.dime.data.data.PrimitiveType.TEXT
			case info.scce.dime.process.process.PrimitiveType.TIMESTAMP: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}
	
	/**
	 * Maps a primitive type (i.e. the enum value) of gui models to 
	 * primitive types of data models.
	 * 
	 * @param it the primitive type enum value of gui.
	 * @return the corresponding primitive type enum value of data. 
	 */
	def toData(InputStatic it) {
		switch it {
			case BooleanInputStatic: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			case IntegerInputStatic: info.scce.dime.data.data.PrimitiveType.INTEGER
			case RealInputStatic: info.scce.dime.data.data.PrimitiveType.REAL
			case TextInputStatic: info.scce.dime.data.data.PrimitiveType.TEXT
			case TimestampInputStatic: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}
	
	/**
	 * Get the original type of this (potentially) referenced attribute.
	 * 
	 * @param attr the (potentially) referenced attribute.
	 * @return the original type if the attribute is a referenced attribute or itself.
	 */
	def getOriginalType(ComplexAttribute attribute) {
		attribute.dataType.originalType
	}
	
	/**
	 * Get the original attribute of this (potentially) referenced attribute.
	 * 
	 * @param attr the (potentially) referenced attribute.
	 * @return the original attribute if the given attribute is a referenced attribute or itself.
	 */
	def Attribute getOriginalAttribute(Attribute attr) {
		switch it: attr {
			ReferencedComplexAttribute: referencedAttribute.originalAttribute
			ReferencedBidirectionalAttribute: referencedAttribute.originalAttribute
			ReferencedUserAttribute: referencedAttribute.originalAttribute
			ReferencedPrimitiveAttribute: referencedAttribute.originalAttribute
			ReferencedExtensionAttribute: referencedAttribute.originalAttribute
			default: it
		}
	}
	
	/**
	 * Returns the type of the given attribute.
	 * 
	 * @param attr the attribute.
	 * @return the attribute's type.
	 */
	def Type getType(Attribute attr) {
		val mec = attr.getContainer()
		if (mec instanceof Type) return mec.originalType
		throw new IllegalStateException('''Attribute «attr?.name» without type container found... should never happen.''');
	}
	
	
	def Type getConcreteUserType(UserType it) {
		userAttributes.head.dataType
	}
}
