/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.data.aps;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.BidirectionalAttribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import style.Appearance;
import style.StyleFactory;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;

public class AttributeAppearance implements StyleAppearanceProvider<Attribute> {

	@Override
	public Appearance getAppearance(Attribute a, String element) {
		
		Appearance appearance = StyleFactory.eINSTANCE.createAppearance();
		

		if ("extensionPort".equals(element)) {
			if (a instanceof ExtensionAttribute) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		if ("inheritanceTriangle".equals(element)) {
			if (a instanceof ComplexAttribute && ((ComplexAttribute)a).getSuperAttr() != null) {
				appearance.setTransparency(0.0);
			}
			else if (a instanceof PrimitiveAttribute && ((PrimitiveAttribute)a).getSuperAttr() != null) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		else 
		if ("upperPort".equals(element)) {
			if (a instanceof ComplexAttribute) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		else 
		if ("lowerPort".equals(element)) {
			if (a instanceof BidirectionalAttribute) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		
		
		return appearance;
	}

}
	
	
