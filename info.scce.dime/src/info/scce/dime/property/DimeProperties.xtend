/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.property

import java.util.Properties
import org.eclipse.core.resources.IProject
import java.nio.file.Paths
import java.io.FileInputStream

class DimeProperties extends Properties {
	
	// name of the Dime config folder to be expected in the home of the user
	public static val CONFIG_FOLDER = ".dime";
	
	// name of the properties file to be expected in the root of a project
	public static val PROPERTIES_FILE_NAME = "dime.properties";
	
	// Pairs: Property key -> Default value
	static val FRONTEND_PORT = "frontendPort" -> (System.getenv("DIME_FRONTEND_PORT") ?: 8080)
	static val APP_URL = "appURL" -> "http://localhost:" + frontendPort
	
	// singleton pattern
	private new() {}
	static DimeProperties INSTANCE
	static def newInstance() { INSTANCE = new DimeProperties => [load] }
	static def getInstance() { INSTANCE ?: newInstance }

	/*
	 * Convenient Getters
	 */	
	static def getFrontendPort() {
		FRONTEND_PORT.intValue
	}

	static def getAppURL() {
		APP_URL.strValue
	}

	/**
	 * Load Dime properties from user home
	 */
	def load() {
		val home = System.getProperty("user.home")
		if (home !== null) try {
			val file = Paths.get(home).resolve(CONFIG_FOLDER).resolve(PROPERTIES_FILE_NAME).toFile
			if (file.exists) {
				load(new FileInputStream(file))
			}
		} catch(Exception e) {
			e.printStackTrace
		}
	}
	
	/**
	 * Load properties from properties file in the specified project.
	 */
	 // TODO find way for loading Properties
	def load(IProject project) {
		println("Loading DIME Properties...")
		val file = project.getFile(PROPERTIES_FILE_NAME)
		if (file.exists) {
			super.load(file.contents)
		}
		println("DIME properties: " + this)
	}
	
	private static def String getStrValue(Pair<String,String> p) {
		instance.getProperty(p.key) ?: p.value
	}
	
	private static def Integer getIntValue(Pair<String,?> p) {
		val value = instance.getProperty(p.key)
		if (value !== null) 
			Integer.parseInt(value)
		else Integer.parseInt(p.value?.toString)
	}
	
}
