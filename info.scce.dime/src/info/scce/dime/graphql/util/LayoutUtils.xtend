/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.util

import graphmodel.Container
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass

class LayoutUtils extends CincoRuntimeBaseClass {
	val MARGIN = 20
	
	def void layout(Container container) {
		val children = container.allContainers;
		
		var totalHeight = 30;
		
		for(child : children) {
			child.width = container.width - MARGIN;
			child.x = MARGIN/2;
			child.y = totalHeight;
			totalHeight += child.height + MARGIN;
		}
		container.height = totalHeight;
	}
}
