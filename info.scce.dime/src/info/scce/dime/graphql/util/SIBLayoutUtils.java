/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.util;

import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_X;
import static info.scce.dime.process.helper.LayoutConstants.SIB_FIRST_PORT_Y;
import static info.scce.dime.process.helper.LayoutConstants.TYPE_FIRST_ATTR_Y;

import graphmodel.Container;
import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.graphql.api.api.Branch;
import info.scce.dime.graphql.api.api.EndSIB;
import info.scce.dime.graphql.api.api.IO;
import info.scce.dime.graphql.api.api.Input;
import info.scce.dime.graphql.api.api.Output;
import info.scce.dime.graphql.api.api.SIB;
import info.scce.dime.graphql.api.api.StartSIB;
import info.scce.dime.process.helper.NodeLayout;

public class SIBLayoutUtils {
	
	@SafeVarargs
	private static void resizeAndLayoutBeforeDelete(Container container, Node doomedNode, int xMargin, int initialY, int ySpace, Class<? extends Node> ... layoutedTypes) {
		int y = initialY;
		
		int ioAmount = 0;
		for (Class<? extends Node> layoutedType : layoutedTypes) {
			ioAmount += container.getModelElements(layoutedType).size();
		}
		
		ioAmount -= (doomedNode == null ? 0 : 1); 
		
		int height = NodeLayout.getTypeHeight(ioAmount + 2);
		container.resize(container.getWidth(), height);
		
		for (Class<? extends Node> layoutedType : layoutedTypes) {
			for (Node node: container.getModelElements(layoutedType)) {
				if (node != doomedNode) {
					node.moveTo(container, xMargin, y);
					node.resize(container.getWidth() - 2 * xMargin, node.getHeight());
					y += ySpace;
				}
			}
		}
	}
	

	/**
	 *
	 * convenience wrapper for {@link #resizeAndLayoutBeforeDelete(CModelElementContainer, CIO)} with
	 * doomedInputOutput = null
	 * 
	 */
	public static void resizeAndLayout(ModelElementContainer container) {
		resizeAndLayoutBeforeDelete(container, null);
	}

	/**
	 * 
	 * Resizes container's height so that all contained elements (minus the one
	 * that is about to be deleted) fit, positions them within the containerand
	 * resizes their width according to container's width. 
	 * 
	 * This only works for a predefined set of containers:
	 * 	{@link CStartSIB}, {@link CEndSIB}, {@link CSIB}, {@link CAbstractBranch} 
	 * 
	 * @param container the layouted container
	 * @param doomedInputOutput The data port that is about to be deleted. Will be ignored for layouting. 
	 * 		Can be null if nothing is going to be deleted
	 */
	public static void resizeAndLayoutBeforeDelete(ModelElementContainer container, IO doomedInputOutput) {
		if (container instanceof StartSIB) {
			resizeAndLayoutBeforeDelete((StartSIB)container, doomedInputOutput);
		}
		else if (container instanceof EndSIB) {
			resizeAndLayoutBeforeDelete((EndSIB)container, doomedInputOutput);
		}
		else if (container instanceof Branch) {
			resizeAndLayoutBeforeDelete((Branch)container, doomedInputOutput);
		}
		else if (container instanceof SIB) {
			resizeAndLayoutBeforeDelete((SIB)container, doomedInputOutput);
		}
		
	}

	private static void resizeAndLayoutBeforeDelete(StartSIB startSIB, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(startSIB, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, Output.class);
	}

	private static void resizeAndLayoutBeforeDelete(EndSIB endSIB, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(endSIB, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, IO.class);
	}
	
	private static void resizeAndLayoutBeforeDelete(Branch branch, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(branch, doomedInputOutput, PORT_X, TYPE_FIRST_ATTR_Y, PORT_SPACE, Output.class);
	}
	
	private static void resizeAndLayoutBeforeDelete(SIB cSib, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(cSib, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, Input.class);
	}
}
