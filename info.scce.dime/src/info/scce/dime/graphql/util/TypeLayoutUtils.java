/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.util;

import static info.scce.dime.process.helper.LayoutConstants.*;

import info.scce.dime.graphql.schema.graphql.Attribute;
import info.scce.dime.graphql.schema.graphql.Type;
import info.scce.dime.process.helper.NodeLayout;

public class TypeLayoutUtils {
	
	/**
	 * 
	 * Resizes cType's height so that all contained {@link CAttribute}s (minus the one
	 * that is about to be deleted) fit, positions them within cType and resizes their
	 * width according to cType's width. 
	 * 
	 * @param cType the layouted type
	 * @param doomedAttribute The attribute that is about to be deleted. Will be ignored for layouting.
	 */
	public static void resizeAndLayout(Type cType) {

		int x = ATTR_X;
		int y = TYPE_FIRST_ATTR_Y;

		int attributeAmount = cType.getAttributes().size() ;
		cType.resize(cType.getWidth(), NodeLayout.getTypeHeight(attributeAmount));
		
		for (Attribute cAttribute : cType.getAttributes()) {
			cAttribute.moveTo(cType, x, y);
			cAttribute.resize(cType.getWidth()-2*ATTR_X, cAttribute.getHeight());
			y += ATTR_SPACE;
		}
	}
}
