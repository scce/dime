/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.actions;

import static info.scce.dime.process.helper.LayoutConstants.ATTR_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.ATTR_X;
import static info.scce.dime.process.helper.LayoutConstants.TYPE_FIRST_ATTR_Y;

import java.util.List;
import info.scce.dime.graphql.api.api.Branch;
import info.scce.dime.graphql.api.api.Input;
import info.scce.dime.graphql.api.api.Output;
import info.scce.dime.graphql.api.api.PrimitiveOutputPort;
import info.scce.dime.graphql.api.api.PrimitiveType;
import info.scce.dime.graphql.api.api.ProcessSIB;
import info.scce.dime.api.DIMECustomAction;
import info.scce.dime.process.helper.NodeLayout;
import info.scce.dime.process.process.BooleanInputStatic;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.IntegerInputStatic;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.RealInputStatic;
import info.scce.dime.process.process.StartSIB;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;

public class ProcessSIBUpdate extends DIMECustomAction<ProcessSIB> {

	ProcessSIB processSIB;

	@Override
	public String getName() {
		return "Update SIB";
	}

	@Override
	public void execute(ProcessSIB sib) {
		processSIB = sib;
		addAttributes(sib);
		layout(sib);
		createBranches(sib);

		List<Branch> branches = sib.getBranchSuccessors();
		for (Branch branch : branches) {
			layoutBranchAttributes(branch);
		}
	}

	public void createBranches(ProcessSIB sib) {
		Process primeModel = (Process) sib.getProMod();
		sib.setLabel(primeModel.getModelName());
		List<EndSIB> endSibs = primeModel.getEndSIBs();
		// Counting endSibs for branch positioning
		int endSibCount = -endSibs.size() / 2;
		// Adding Branch Attributes & arranging branch Position

		removeUnusedBranches(endSibs);

		Branch target;
		for (EndSIB endsib : endSibs) {
			// Creates new Branch if there isn't one with the name of the endSIB
			if (!branchExists(endsib.getBranchName())) {
				target = sib.getContainer().getRootElement().newBranch(sib.getX() + endSibCount * sib.getWidth(),
						sib.getY() + sib.getHeight() + 15);
				target.setName(endsib.getBranchName());
				sib.newBranchConnector(target);
				endSibCount++;
			} else {
				// Saves a BranchSuccessor with the name of the endSIB
				target = processSIB.getBranchSuccessors().stream()
						.filter(c -> c.getName().equals(endsib.getBranchName())).findFirst().orElse(null);
				;

			}

			// Deletes all Ports that do not exist any more
			for (info.scce.dime.graphql.api.api.OutputPort outPort : target.getOutputPorts()) {
				boolean delete = true;
				if(outPort instanceof info.scce.dime.graphql.api.api.PrimitiveOutputPort) {
					info.scce.dime.graphql.api.api.PrimitiveOutputPort out = (info.scce.dime.graphql.api.api.PrimitiveOutputPort) outPort;
					for(info.scce.dime.process.process.PrimitiveInputPort inPort : endsib.getPrimitiveInputPorts()) {
						if (out.getName().equals(inPort.getName()) && out.getDataType().getName().equals(inPort.getDataType().getName())) {
							delete = false;
						}
					}
				}
				else if(outPort instanceof info.scce.dime.graphql.api.api.ComplexOutputPort) {
					info.scce.dime.graphql.api.api.ComplexOutputPort out = (info.scce.dime.graphql.api.api.ComplexOutputPort) outPort;
					for(info.scce.dime.process.process.ComplexInputPort inPort : endsib.getComplexInputPorts()) {
						if (out.getName().equals(inPort.getName()) && out.getDataType().equals(inPort.getDataType())) {
							delete = false;
						}
					}
				}
				if(delete)
					outPort.delete();
			}

			// Creates Ports that do not exist yet
			for (PrimitiveInputPort inpPort : endsib.getPrimitiveInputPorts()) {
				boolean add = true;
				for (info.scce.dime.graphql.api.api.PrimitiveOutputPort port : target.getPrimitiveOutputPorts()) {
					if (port.getName().equals(inpPort.getName())
							&& inpPort.getDataType().getName().equals(port.getDataType().getName())) {
						add = false;
					}
				}
				if(add) {
					PrimitiveOutputPort primoutPort = target.newPrimitiveOutputPort(1, 1);
					primoutPort.setDataType(PrimitiveType.values()[inpPort.getDataType().getValue()]);
					primoutPort.setName(inpPort.getName());
					if (inpPort.isIsList()) {
						primoutPort.setIsList(true);
					}
				}

			}
			for (ComplexInputPort inpPort : endsib.getComplexInputPorts()) {
				boolean add = true;
				for (info.scce.dime.graphql.api.api.ComplexOutputPort port : target.getComplexOutputPorts()) {
					if (port.getName().equals(inpPort.getName())
							&& inpPort.getDataType().equals(port.getDataType())) {
						add = false;
					}
				}
				if(add) {
					info.scce.dime.graphql.api.api.ComplexOutputPort compoutPort = target
							.newComplexOutputPort(inpPort.getDataType(), 1, 1);
					compoutPort.setName(inpPort.getName());
	
					if (inpPort.isIsList()) {
						compoutPort.setIsList(true);
					}
				}
			}

			for (InputStatic inp : endsib.getInputStatics()) {
				PrimitiveOutputPort primoutPort = target.newPrimitiveOutputPort(1, 1);

				primoutPort.setName(inp.getName());

				if (inp instanceof TextInputStatic) {
					primoutPort.setDataType(PrimitiveType.TEXT);
				} else if (inp instanceof IntegerInputStatic) {
					primoutPort.setDataType(PrimitiveType.INTEGER);
				} else if (inp instanceof RealInputStatic) {
					primoutPort.setDataType(PrimitiveType.REAL);
				} else if (inp instanceof BooleanInputStatic) {
					primoutPort.setDataType(PrimitiveType.BOOLEAN);
				} else if (inp instanceof TimestampInputStatic) {
					primoutPort.setDataType(PrimitiveType.TIMESTAMP);
				} else {
					primoutPort.setDataType(PrimitiveType.FILE);
				}

			}
		}

	}

	// Deletes all Branches not in the ProcessSIB
	private void removeUnusedBranches(List<EndSIB> endSibs) {

		for (Branch branch : processSIB.getBranchSuccessors()) {
			boolean exists = false;
			for (EndSIB sib : endSibs) {
				if (branch.getName().equals(sib.getBranchName())) {
					exists = true;
					break;
				}
			}

			if (!exists) {
				branch.delete();
			}
			exists = false;
		}

	}

	// Arranges the Attributes with the correct coordinates to fit the Branch of a
	// ProcessSib
	public void layoutBranchAttributes(Branch branch) {
		int x = ATTR_X;
		int y = TYPE_FIRST_ATTR_Y;

		int attributeAmount = branch.getOutputs().size() + 2;// Changed to fit size
		branch.resize(branch.getWidth(), NodeLayout.getTypeHeight(attributeAmount));
		for (Output output : branch.getOutputs()) {
			output.moveTo(branch, x, y);
			output.resize(branch.getWidth() - 2 * ATTR_X, output.getHeight());
			y += ATTR_SPACE;
		}
	}

	// Arranges the Attributes with the correct coordinates to fit the ProcessSIB
	public void layout(ProcessSIB sib) {
		int x = ATTR_X;
		int y = TYPE_FIRST_ATTR_Y + 35;// Changed to fit size

		int attributeAmount = sib.getInputs().size() + 2;// Changed to fit size
		sib.resize(sib.getWidth(), NodeLayout.getTypeHeight(attributeAmount));
		for (Input input : sib.getInputs()) {
			input.moveTo(sib, x, y);
			input.resize(sib.getWidth() - 2 * ATTR_X, input.getHeight());
			y += ATTR_SPACE;
		}
	}

	// Adds the Attributes to the ProccessSIB from the Inputs from the StartSIB
	// TODO Check if we maybe need more Attributes than DataType and Name. (Maybe
	// ID?)
	public void addAttributes(ProcessSIB sib) {
		Process primeModel = (Process) sib.getProMod();
		List<StartSIB> startSibs = primeModel.getStartSIBs();
		
		
		// Do not delete (still) existing
		for (Input input:sib.getInputs()) {
			boolean delete = true;
			if(input instanceof info.scce.dime.graphql.api.api.PrimitiveInputPort) {
				info.scce.dime.graphql.api.api.PrimitiveInputPort in = (info.scce.dime.graphql.api.api.PrimitiveInputPort) input;
				for(info.scce.dime.process.process.PrimitiveOutputPort outPort : startSibs.get(0).getPrimitiveOutputPorts()) {
					if (in.getName().equals(outPort.getName()) && in.getDataType().getName().equals(outPort.getDataType().getName())) {
						delete = false;
					}
				}
			}
			else if(input instanceof info.scce.dime.graphql.api.api.ComplexInputPort) {
				info.scce.dime.graphql.api.api.ComplexInputPort in = (info.scce.dime.graphql.api.api.ComplexInputPort) input;
				for(info.scce.dime.process.process.ComplexOutputPort outPort : startSibs.get(0).getComplexOutputPorts()) {
					if (in.getName().equals(outPort.getName()) && in.getDataType().equals(outPort.getDataType())) {
						delete = false;
					}
				}
			}
			if(delete)
				input.delete();
		}
		
		
		//TODO Only add used
		for (StartSIB startSib : startSibs) {

			for (info.scce.dime.process.process.PrimitiveOutputPort outputPort : startSib.getPrimitiveOutputPorts()) {
				if (!primPortExists(outputPort)) {
					info.scce.dime.graphql.api.api.PrimitiveInputPort primInPort = sib
							.newPrimitiveInputPort(sib.getX(), sib.getY());
					primInPort.setDataType(PrimitiveType.values()[outputPort.getDataType().getValue()]);
					primInPort.setName(outputPort.getName());
				}

			}

			for (ComplexOutputPort outputPort : startSib.getComplexOutputPorts()) {
				if (!compPortExists(outputPort)) {
					info.scce.dime.graphql.api.api.ComplexInputPort compInPort = sib
							.newComplexInputPort(outputPort.getDataType(), sib.getX(), sib.getY());
					compInPort.setName(outputPort.getName());
				}
			}
		}
	}

	private boolean compPortExists(ComplexOutputPort outputPort) {
		for (info.scce.dime.graphql.api.api.ComplexInputPort port : processSIB.getComplexInputPorts()) {
			if (port.getName().equals(outputPort.getName()) && port.getDataType().equals(outputPort.getDataType())) {
				return true;
			}
		}

		return false;
	}

	private boolean primPortExists(info.scce.dime.process.process.PrimitiveOutputPort outputPort) {
		for (info.scce.dime.graphql.api.api.PrimitiveInputPort port : processSIB.getPrimitiveInputPorts()) {
			if (port.getName().equals(outputPort.getName())
					&& outputPort.getDataType().getName().equals(port.getDataType().getName())) {
				return true;
			}
		}

		return false;
	}

	private boolean branchExists(String name) {
		for (Branch branch : processSIB.getBranchSuccessors()) {
			if (branch.getName().equals(name)) {
				return true;
			}
		}

		return false;
	}

}
