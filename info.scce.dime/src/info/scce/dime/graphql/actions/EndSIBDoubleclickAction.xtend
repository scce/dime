/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.helper.NodeLayout
import info.scce.dime.graphql.api.api.EndSIB
import info.scce.dime.graphql.api.api.APIoutputPort

import static info.scce.dime.process.helper.LayoutConstants.ATTR_X
import static info.scce.dime.process.helper.LayoutConstants.TYPE_FIRST_ATTR_Y
import static info.scce.dime.process.helper.LayoutConstants.ATTR_SPACE

class EndSIBDoubleclickAction extends DIMECustomAction<EndSIB> {
	
	override getName() {
		"New primitive Input"
	}

	override execute(EndSIB sib) {
		// create port
		val port = sib.newPrimitiveAPIoutput(1, 1)
		port.name = "output"
		
		// layout
		var x = ATTR_X
		var y = TYPE_FIRST_ATTR_Y + 35
		var attributeAmount = sib.APIoutputPorts.size() + 2;
		sib.resize(sib.getWidth(), NodeLayout.getTypeHeight(attributeAmount));
		for (APIoutputPort output : sib.APIoutputPorts) {
			output.moveTo(sib, x, y);
			output.resize(sib.getWidth() - 2 * ATTR_X, output.getHeight());
			y += ATTR_SPACE;
		}
	}
}
