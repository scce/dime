/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.graphql.schema.graphql.Type
import info.scce.dime.graphql.schema.graphql.APISIB
import info.scce.dime.graphql.schema.graphql.ApiError
import info.scce.dime.graphql.schema.graphql.ApiInput
import info.scce.dime.graphql.schema.graphql.ApiOutput
import info.scce.dime.graphql.util.LayoutUtils
import info.scce.dime.graphql.api.api.IO
import info.scce.dime.graphql.api.api.PrimitiveAPIoutput
import info.scce.dime.graphql.api.api.PrimitiveAPIinput
import info.scce.dime.graphql.api.api.ComplexAPIoutput

class APISIBUpdate extends DIMECustomAction<APISIB>{
	
	protected extension LayoutUtils = new LayoutUtils()
	
	override String getName() {
		return "Update SIB";
	}
	
	override execute(APISIB apiSIB) {		
		for (ApiInput input : apiSIB.apiInputs){
			input.delete;
		}
		
		for (ApiOutput output : apiSIB.apiOutputs){
			output.delete;
		}
		
		for (ApiError error : apiSIB.apiErrors){
			error.delete;
		}
		
		update(apiSIB)
	}
	
	
	def update(APISIB apisib){
		var x = 5;
		var y = 35;
		
		val startSibs = apisib.model.startSIBs
		if(!startSibs.nullOrEmpty){
			val inputPorts = startSibs.head.primitiveAPIinputs
			
			for(port : inputPorts) {				
				val apiInput = apisib.newApiInput(x, y)
				apiInput.name = getPortName(port)
				apiInput.required = port.required
				
				y += 15
			}
		}
		
		y += 5;
		
		val endSibs = apisib.model.endSIBs
		if(!endSibs.nullOrEmpty){
			val outputPorts = endSibs.head.APIoutputPorts
			
			for(port : outputPorts) {				
				val apiOutput = apisib.newApiOutput(x, y)
				apiOutput.name = getPortName(port)
				apiOutput.notNull = port.notNull
				
				y += 15
			}
		}
		
		y += 5;
		
		val errorSibs = apisib.model.errorSIBs
		if(!errorSibs.nullOrEmpty){
			for(errorSib: errorSibs) {
				val errorMessage = '''[Code «errorSib.errorCode»] «errorSib.errorMessage»'''
				val errorOutput = apisib.newApiError(x,y) 
				errorOutput.name = errorMessage
				
				y+=15	
			}
			
		}
		
		apisib.height = y+10;
		apisib.container.layout
		val toBeWidth = apisib.width - 10
		apisib.allNodes.forEach[width=toBeWidth]
	}
	
	
	def getPortName(IO port) {
		switch (port) {
			PrimitiveAPIoutput: {
				getListFormat(port.name, port.dataType.getName, port.isIsList) 
			}
			PrimitiveAPIinput: {
				getListFormat(port.name, port.dataType.getName, port.isIsList) 
			}
			ComplexAPIoutput: {
				getListFormat(port.name, (port.dataType as Type).name, port.isIsList)
			}
			default: {
				"None" 
			}
		}
	}
	
	def getListFormat(String name, String typ, boolean isList){
		if(isList) return '''«name»: [«typ»]'''
		else return '''«name»: «typ»'''
	}
	
}
