/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */

package info.scce.dime.graphql.checks

import java.io.File
import de.jabc.cinco.meta.plugin.mcam.runtime.core.CincoCheckModule
import info.scce.dime.graphql.schema.mcam.adapter.GraphQLId
import info.scce.dime.graphql.schema.graphql.GraphQL
import info.scce.dime.graphql.schema.mcam.adapter.GraphQLAdapter
import info.scce.dime.graphql.api.api.ComplexAPIinput
import info.scce.dime.graphql.schema.graphql.ForbiddenType
import info.scce.dime.graphql.schema.graphql.ForbiddenUserType
import info.scce.dime.graphql.schema.graphql.APISIB
import info.scce.dime.data.data.Type

/**
 * Check if complex properties that are used as input arguments for queries and mutations
 * are present in the Data container and are marked as input.
 */
class DeclaredInputsCheck extends CincoCheckModule<GraphQLId, GraphQL, GraphQLAdapter>  {
	
	override check(GraphQL model) {	
		// all types that are declared as input	
		val inputTypes = model.forbiddenTypess.get(0).types
			.filter[it.isInput]
			.map[
				if (it instanceof ForbiddenType) {
					it.referencedType
				} else if (it instanceof ForbiddenUserType) {
					it.referencedType
				}
			]

		// find types in queries and mutations that are not declared as input		
		checkUndeclaredInputs(inputTypes, model.queriess.get(0).APISIBs, "query")
		checkUndeclaredInputs(inputTypes, model.mutationss.get(0).APISIBs, "mutation")
	}
	
	def checkUndeclaredInputs(Iterable<Type> inputTypes, Iterable<APISIB> sibs, String type) {
		sibs.forEach[sib|
			sib.model.startSIBs.get(0).APIinputPorts
				.filter[it instanceof ComplexAPIinput]
				.map[it as ComplexAPIinput]
				.filter[!inputTypes.exists[t|t.id.equals(it.dataType.id)]]
				.forEach[input|
					addError(
						adapter.getIdByString(sib.id), 
						'''type "«input.dataType.name»" in «type» "«sib.queryOrMutationName»" is not an input type'''
					)
				]
		]
	}
	
	def queryOrMutationName(APISIB sib) {
		sib.model.eResource.file.name.split(File.separator).last
	}
}