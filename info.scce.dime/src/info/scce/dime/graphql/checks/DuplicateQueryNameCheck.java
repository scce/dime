/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */

package info.scce.dime.graphql.checks;

import java.util.Set;
import java.util.HashSet;

import de.jabc.cinco.meta.plugin.mcam.runtime.core.CincoCheckModule;
import info.scce.dime.graphql.schema.graphql.APISIB;
import info.scce.dime.graphql.schema.graphql.GraphQL;
import info.scce.dime.graphql.schema.mcam.adapter.GraphQLAdapter;
import info.scce.dime.graphql.schema.mcam.adapter.GraphQLId;

/**
 * Checks if query names are unique.
 */
public class DuplicateQueryNameCheck extends CincoCheckModule<GraphQLId, GraphQL, GraphQLAdapter> {
	
	@Override
	public void check(GraphQL model) {
		final Set<String> seenQueryNames = new HashSet<String>();
				
		for (APISIB query: model.getQueriess().get(0).getAPISIBs()) {
			String queryName = query.getModel().getModelName();
			if (seenQueryNames.contains(queryName)) {
				addError(adapter.getIdByString(query.getId()), "Duplicate query name: " + queryName);
			} else {
				seenQueryNames.add(queryName);
			}
		}
	}
}
