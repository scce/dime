/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.checks;


import java.util.ArrayList;

//import org.eclipse.emf.common.util.BasicEList;
//import org.eclipse.emf.common.util.EList;
import java.util.List;
import info.scce.dime.graphql.api.api.API;
import info.scce.dime.graphql.api.api.ProcessSIB;
import info.scce.dime.graphql.api.mcam.modules.checks.APICheck;
import info.scce.dime.process.process.Process;


public class ProcessHasGUISIBCheck extends APICheck {

	@Override
	public void check(API model) {
		for (ProcessSIB sib : model.getProcessSIBs()) {
			checkGUIInitial(sib);
		}
	}

	private void checkGUIInitial(ProcessSIB processSIB) {
		if (!((Process) processSIB.getProMod()).getGUISIBs().isEmpty()) {
			addError(adapter.getIdByString(processSIB.getId()), processSIB.getLabel()+" contains GUISIB");
		}

		List<info.scce.dime.process.process.ProcessSIB> processList = ((Process) processSIB.getProMod()).getProcessSIBs();
		List<info.scce.dime.process.process.ProcessSIB> markedList = new ArrayList<>();

		for (info.scce.dime.process.process.ProcessSIB childProcess : processList) {
			checkGUI(childProcess, markedList, processSIB.getId());
		}
	}

	private void checkGUI(
		info.scce.dime.process.process.ProcessSIB processSIB,
		List<info.scce.dime.process.process.ProcessSIB> markedProcesses,
		String apiID
	) {
		//Adding the Error Message
		if (!processSIB.getProMod().getGUISIBs().isEmpty()) {
			addError(adapter.getIdByString(apiID),"ProcessSIB somewhere contains the Process "+ processSIB.getLabel()+", which contains GUISIB");
		}

		//Creating a copy of the ProcessList to then remove all markedProcesses from this list
		List<info.scce.dime.process.process.ProcessSIB> processList = ((Process) processSIB.getProMod()).getProcessSIBs();
		ArrayList<info.scce.dime.process.process.ProcessSIB> processListCopy = new ArrayList<>();
		processListCopy.addAll(processList);
		processListCopy.removeAll(markedProcesses);

		//Adding the process to the markedProcesses and calling the function checkGUI recursively
		for (info.scce.dime.process.process.ProcessSIB childProcess : processListCopy) {
			markedProcesses.add(childProcess);
			checkGUI(childProcess, markedProcesses, apiID);
		}
	}
}
