/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.graphql.checks

import de.jabc.cinco.meta.plugin.mcam.runtime.core.CincoCheckModule
import info.scce.dime.graphql.schema.mcam.adapter.GraphQLId
import info.scce.dime.graphql.schema.graphql.GraphQL
import info.scce.dime.graphql.schema.mcam.adapter.GraphQLAdapter
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.PrimitiveInputPort

class ResolverProcessSignatureCheck extends CincoCheckModule<GraphQLId, GraphQL, GraphQLAdapter> {
	
	override check(GraphQL model) {
		model.resolverss.get(0).resolvers.forEach[resolver|
			val referencedType = resolver.referencedType
			
			resolver.resolverAttributes.forEach[resolverAttribute|
				val referencedAttribute = resolverAttribute.referencedAttribute		
				if (!resolverAttribute.resolverProcesss.nullOrEmpty) {
					val referencedProcess = resolverAttribute.resolverProcesss.get(0).resolverProcess
					
					// must have one EndSIB
					if (referencedProcess.endSIBs.length !== 1) {
						addError(referencedProcess, '''Process in "«referencedType.name»::«referencedAttribute.name»" must have exaclty one EndSIB''')
					} else {
						val startSIB = referencedProcess.startSIBs.get(0)
						val endSIB = referencedProcess.endSIBs.get(0)
						
						// StartSIB must have one input of the referenced data type
						if (startSIB.outputPorts.length !== 1) {
							addError(referencedProcess, '''StartSIB in process "«referencedProcess.displayName»" must have exaclty one output port''')
						} else {
							val output = startSIB.outputPorts.get(0)
							if (!(output instanceof ComplexOutputPort) 
								|| referencedType.name !== (output as ComplexOutputPort).dataType.name
							) {
								addError(referencedProcess, '''Input of process "«referencedProcess.displayName»" must by of type "«referencedType.name»"''')
							}
						}
						
						// Each EndSIB must have one InputPort
						if (endSIB.inputPorts.length !== 1) {
							addError(referencedProcess, '''StartSIB in process "«referencedProcess.displayName»" must have exaclty one input port''')
						} else {
							val input = endSIB.inputPorts.get(0)
							
							// EndSIB must have one output of the referenced attribute type	
							if (referencedAttribute instanceof PrimitiveAttribute) {
								if (!(input instanceof PrimitiveInputPort) 
									|| (input as PrimitiveInputPort).dataType.getName !== referencedAttribute.dataType.getName
								) {
									addError(referencedProcess, '''Output in process "«referencedProcess.displayName»" must be a primitive attribute of type "«(input as PrimitiveInputPort).dataType»"''')
								}
							} else if (referencedAttribute instanceof ComplexAttribute) {
								if (!(input instanceof ComplexInputPort) 
									|| (input as ComplexInputPort).dataType.name !== referencedAttribute.dataType.name
								) {
									addError(referencedProcess, '''Output in process "«referencedProcess.displayName»" must be a complex attribute of type "«(input as ComplexInputPort).dataType.name»"''')
								}
							}
							
							// The name of the EndSIB must be "success"
							if (endSIB.branchName !== "success") {
								addError(referencedProcess, '''EndSIB in process "«referencedProcess.displayName»" must be labeled with "success"''')
							}
						}
					}
				}	
			]
		]
	}	
}