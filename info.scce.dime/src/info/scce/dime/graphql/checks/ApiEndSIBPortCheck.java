/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.checks;

import info.scce.dime.graphql.api.api.API;
import info.scce.dime.graphql.api.api.APIoutputPort;
import info.scce.dime.graphql.api.api.EndSIB;
import info.scce.dime.graphql.api.mcam.modules.checks.APICheck;

public class ApiEndSIBPortCheck extends APICheck{
	
	@Override
	public void check(API model) {
		for (EndSIB sib:model.getEndSIBs()) {
			checkEndSIB(sib);
		}
	}

	private void checkEndSIB(EndSIB sib) {
		if (!sib.getAPIoutputPorts().isEmpty()) {
			APIoutputPort outputPort = sib.getAPIoutputPorts().get(0);
			if (outputPort.getIncomingDataFlows().isEmpty()) {
				addWarning(adapter.getIdByString(sib.getId()), "EndSIB output port " + outputPort.getName() + " is not connected");
			}
		}
	}
}
