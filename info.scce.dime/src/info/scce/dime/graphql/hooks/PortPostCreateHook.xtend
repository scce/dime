/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.graphql.hooks

import info.scce.dime.graphql.api.api.IO;
import info.scce.dime.api.DIMEPostCreateHook;

import static info.scce.dime.process.helper.LayoutConstants.ATTR_SPACE
import static info.scce.dime.process.helper.LayoutConstants.ATTR_X
import static info.scce.dime.process.helper.LayoutConstants.TYPE_FIRST_ATTR_Y
import info.scce.dime.graphql.api.api.StartSIB
import info.scce.dime.process.helper.NodeLayout

class PortPostCreateHook extends DIMEPostCreateHook<IO> {

	override postCreate(IO port) {
		var x = ATTR_X
		var y = TYPE_FIRST_ATTR_Y + 35
		
		val container = port.container
		
		if (container instanceof StartSIB) {						
			var attributeAmount = container.APIinputPorts.size() + 2
			
			container.resize(container.getWidth(), NodeLayout.getTypeHeight(attributeAmount))
			for (input : container.APIinputPorts) {
				input.moveTo(container, x, y)
				y += ATTR_SPACE
			}
		}
	}
}
	