/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.graphql.api.api.APIoutputPort
import info.scce.dime.graphql.api.api.ComplexAPIoutput
import info.scce.dime.graphql.api.api.DataFlowTarget
import info.scce.dime.graphql.api.api.EndSIB
import info.scce.dime.process.helper.NodeLayout

import static info.scce.dime.process.helper.LayoutConstants.ATTR_SPACE
import static info.scce.dime.process.helper.LayoutConstants.ATTR_X
import static info.scce.dime.process.helper.LayoutConstants.TYPE_FIRST_ATTR_Y

class APIoutputPortHook extends DIMEPostCreateHook<ComplexAPIoutput> {
	
	def layout(DataFlowTarget node) {
		var x = ATTR_X
		var y = TYPE_FIRST_ATTR_Y + 35

		if (node instanceof EndSIB) {
			var attributeAmountEndSib = node.APIoutputPorts.size() + 2
			node.resize(node.getWidth(), NodeLayout.getTypeHeight(attributeAmountEndSib))
			for (APIoutputPort output : node.APIoutputPorts) {
				output.moveTo(node, x, y)
				output.resize(node.getWidth() - 2 * ATTR_X, output.getHeight())
				y += ATTR_SPACE
			}
		}
	}
	
	override postCreate(ComplexAPIoutput inPort) {
		layout(inPort.container)
	}	
}
