/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.hooks;

import info.scce.dime.graphql.api.api.ComplexOutputPort
import info.scce.dime.graphql.api.api.DataFlowSource
import info.scce.dime.graphql.api.api.Output
import info.scce.dime.graphql.api.api.StartSIB
import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.process.helper.NodeLayout

import static info.scce.dime.process.helper.LayoutConstants.ATTR_SPACE
import static info.scce.dime.process.helper.LayoutConstants.ATTR_X
import static info.scce.dime.process.helper.LayoutConstants.TYPE_FIRST_ATTR_Y
import info.scce.dime.graphql.api.api.Branch

class OutputPortHook extends DIMEPostCreateHook<ComplexOutputPort> {

	def layout(DataFlowSource node) {
		var x = ATTR_X
		var y = TYPE_FIRST_ATTR_Y + 35

		// Going through List of Nodes, checking if Node is a StartSIB
		// Arranging Attributes for ComplexAttributes
		if (node instanceof StartSIB) {
			var attributeAmountStartSib = node.outputs.size() + 2;
			node.resize(node.getWidth(), NodeLayout.getTypeHeight(attributeAmountStartSib));
			for (Output output : node.outputs) {
				output.moveTo(node, x, y);
				output.resize(node.getWidth() - 2 * ATTR_X, output.getHeight());
				y += ATTR_SPACE;
			}
		}
		if (node instanceof Branch) {
			y = TYPE_FIRST_ATTR_Y
			var attributeAmountBranch = node.outputs.size() + 2;
			node.resize(node.getWidth(), NodeLayout.getTypeHeight(attributeAmountBranch));
			for (Output output : node.outputs) {
				output.moveTo(node, x, y);
				output.resize(node.getWidth() - 2 * ATTR_X, output.getHeight());
				y += ATTR_SPACE;
			}
		}

	}

	override postCreate(ComplexOutputPort outPort) {
		layout(outPort.container)
	}
}
