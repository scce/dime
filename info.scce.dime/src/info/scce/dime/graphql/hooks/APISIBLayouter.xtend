/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.hooks

import static de.jabc.cinco.meta.runtime.layout.LayoutConfiguration.*
import de.jabc.cinco.meta.runtime.layout.ListLayouter
import info.scce.dime.graphql.schema.graphql.Entrypoints
import info.scce.dime.graphql.schema.graphql.APISIB

class APISIBLayouter extends ListLayouter {

     static APISIBLayouter _instance
     static def getInstance() {
         _instance ?: (_instance = new APISIBLayouter)
     }

     dispatch def getLayoutConfiguration(Entrypoints it) {
         #{
         MIN_HEIGHT -> 60,
         PADDING_TOP -> 30,
         PADDING_Y -> 10
        
         }
     }

     dispatch def getInnerOrder(Entrypoints entrypoints) {
         #[APISIB]
     }

}
