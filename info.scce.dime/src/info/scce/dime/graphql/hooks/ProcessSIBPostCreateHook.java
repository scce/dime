/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.hooks;

import java.util.List;
import static info.scce.dime.process.helper.LayoutConstants.ATTR_X;
import static info.scce.dime.process.helper.LayoutConstants.TYPE_FIRST_ATTR_Y;
import static info.scce.dime.process.helper.LayoutConstants.ATTR_SPACE;

import info.scce.dime.graphql.api.api.Branch;
import info.scce.dime.graphql.api.api.Input;
import info.scce.dime.graphql.api.api.Output;
import info.scce.dime.graphql.api.api.PrimitiveOutputPort;
import info.scce.dime.graphql.api.api.PrimitiveType;
import info.scce.dime.graphql.api.api.ProcessSIB;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.process.helper.NodeLayout;
import info.scce.dime.process.process.BooleanInputStatic;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.IntegerInputStatic;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.RealInputStatic;
import info.scce.dime.process.process.StartSIB;

public class ProcessSIBPostCreateHook extends DIMEPostCreateHook<ProcessSIB>{


	//Creating the branches for each EndSIB in the ProcessSIB when the PostCreateHook is called
	public void createBranches(ProcessSIB sib)
	{
		Process primeModel = (Process) sib.getProMod();
		sib.setLabel(primeModel.getModelName());
		List<EndSIB> endSibs = primeModel.getEndSIBs();
		//Counting endSibs for branch positioning
		int endSibCount = -endSibs.size()/2;
		//Adding Branch Attributes & arranging branch Position
		for (EndSIB endsib : endSibs) 
		{			
			Branch target = sib.getContainer().getRootElement().newBranch(sib.getX()+endSibCount*sib.getWidth(), sib.getY()+sib.getHeight()+15);			
			target.setName(endsib.getBranchName());
			sib.newBranchConnector(target);
			endSibCount++;
			
			for (PrimitiveInputPort inpPort:endsib.getPrimitiveInputPorts()) {
				PrimitiveOutputPort primoutPort = target.newPrimitiveOutputPort(1, 1);
				primoutPort.setDataType(PrimitiveType.values()[inpPort.getDataType().getValue()]);
				primoutPort.setName(inpPort.getName());
				if (inpPort.isIsList()) {
					primoutPort.setIsList(true);
				}
			}
			
			for (ComplexInputPort inpPort:endsib.getComplexInputPorts()) {
				info.scce.dime.graphql.api.api.ComplexOutputPort compoutPort = target.newComplexOutputPort(inpPort.getDataType(),1, 1);
				compoutPort.setName(inpPort.getName());
				
				if (inpPort.isIsList()) {
					compoutPort.setIsList(true);
				}
			}
			
			for (InputStatic inp : endsib.getInputStatics()) {
				PrimitiveOutputPort primoutPort = target.newPrimitiveOutputPort(1, 1);
				
				primoutPort.setName(inp.getName());
				
				if (inp instanceof TextInputStatic) {
					primoutPort.setDataType(PrimitiveType.TEXT);
				}
				else if (inp instanceof IntegerInputStatic) {
					primoutPort.setDataType(PrimitiveType.INTEGER);
				}
				else if (inp instanceof RealInputStatic) {
					primoutPort.setDataType(PrimitiveType.REAL);
				}
				else if (inp instanceof BooleanInputStatic) {
					primoutPort.setDataType(PrimitiveType.BOOLEAN);
				}
				else if (inp instanceof TimestampInputStatic) {
					primoutPort.setDataType(PrimitiveType.TIMESTAMP);
				}
				else {
					primoutPort.setDataType(PrimitiveType.FILE);
				}
				
			}
			
		}
	}
	
	//Arranges the Attributes with the correct coordinates to fit the Branch of a ProcessSib
	public void layoutBranchAttributes(Branch branch) {
		int x = ATTR_X;
		int y = TYPE_FIRST_ATTR_Y;
		
		int attributeAmount = branch.getOutputs().size()+2;//Changed to fit size
		branch.resize(branch.getWidth(), NodeLayout.getTypeHeight(attributeAmount));
			for (Output output : branch.getOutputs())
			{
				output.moveTo(branch, x, y);
				output.resize(branch.getWidth()-2*ATTR_X, output.getHeight());
					y += ATTR_SPACE;
			}
	}
	
	//Arranges the Attributes with the correct coordinates to fit the ProcessSIB
	public void layout(ProcessSIB sib) {
		int x = ATTR_X;
		int y = TYPE_FIRST_ATTR_Y+35;//Changed to fit size

		
		int attributeAmount = sib.getInputs().size()+2;//Changed to fit size
		sib.resize(sib.getWidth(), NodeLayout.getTypeHeight(attributeAmount));
			for (Input input : sib.getInputs())
			{
					input.moveTo(sib, x, y);
					input.resize(sib.getWidth()-2*ATTR_X, input.getHeight());
					y += ATTR_SPACE;
			}
	}
	
	//Adds the Attributes to the ProccessSIB from the Inputs from the StartSIB
	//TODO Check if we maybe need more Attributes than DataType and Name. (Maybe ID?)
	public void addAttributes(ProcessSIB sib)
	{
		Process primeModel = (Process) sib.getProMod();
		List<StartSIB> startSibs = primeModel.getStartSIBs();
		for(StartSIB startSib:startSibs)
		{
			for(info.scce.dime.process.process.PrimitiveOutputPort outputPort:startSib.getPrimitiveOutputPorts())
			{
				info.scce.dime.graphql.api.api.PrimitiveInputPort primInPort = sib.newPrimitiveInputPort(sib.getX(), sib.getY());
				primInPort.setDataType(PrimitiveType.values()[outputPort.getDataType().getValue()]);
				primInPort.setName(outputPort.getName());
			}
			
			for(ComplexOutputPort outputPort:startSib.getComplexOutputPorts()) {
				info.scce.dime.graphql.api.api.ComplexInputPort compInPort = sib.newComplexInputPort(outputPort.getDataType(), sib.getX(), sib.getY());
				compInPort.setName(outputPort.getName());
			}
		}
	}
	
	@Override
	public void postCreate(ProcessSIB sib) 
	{
		addAttributes(sib);
		layout(sib);
		createBranches(sib);
		List<Branch> branches = sib.getBranchSuccessors();
		for(Branch branch : branches)
		{
			layoutBranchAttributes(branch);
		}
	}
}

