/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.hooks;

import java.util.List;


import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.graphql.schema.graphql.*;
import info.scce.dime.graphql.util.LayoutUtils;
import info.scce.dime.graphql.util.TypeLayoutUtils;

public class ReferencedTypePostCreate extends DIMEPostCreateHook<Type>{
	
	protected LayoutUtils layoutUtils = new LayoutUtils();

	@Override
	public void postCreate(Type forbiddenType) {
		clearContained(forbiddenType);
		
		info.scce.dime.data.data.Type lc = getReferencedType(forbiddenType);
		forbiddenType.setName(lc.getName());
		
		List<Attribute> attributes = _dataExtension.getInheritedAttributes(lc);
		
		for (Attribute lcAttribute : attributes) {
			if (lcAttribute instanceof ComplexAttribute) {
				forbiddenType.newForbiddenComplexAttribute(lcAttribute, 1, 1);
			} else if (lcAttribute instanceof PrimitiveAttribute) {
				forbiddenType.newForbiddenPrimitiveAttribute(lcAttribute, 1, 1);
			} else {
				throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
			}
		}
		
		// Do not question this. It works. That's all we need to know.
		TypeLayoutUtils.resizeAndLayout(forbiddenType);		
		layoutUtils.layout(forbiddenType.getContainer());
		TypeLayoutUtils.resizeAndLayout(forbiddenType);	
	}
	
	private info.scce.dime.data.data.Type getReferencedType(Type t) {
		if (t instanceof ForbiddenType) {
			return ((info.scce.dime.data.data.Type)((ForbiddenType) t).getReferencedType());
		} else if (t instanceof ForbiddenUserType) {
			return ((info.scce.dime.data.data.Type)((ForbiddenUserType) t).getReferencedType());
		} else {
			return null;
		}
	}

	private boolean clearContained(Type forbiddenType){
		GraphQL container = (GraphQL) forbiddenType.getRootElement();
		List<ForbiddenType> forbiddenTypes = container.getForbiddenTypess().get(0).getForbiddenTypes();
		List<ForbiddenUserType> forbiddenUserTypes = container.getForbiddenTypess().get(0).getForbiddenUserTypes();
	
		for(ForbiddenType forbidden : forbiddenTypes){
			if(getReferencedType(forbidden).getId()==getReferencedType(forbiddenType).getId() && !forbidden.equals(forbiddenType)){
				forbidden.delete();
				return true;
			}
		}
		
		for(ForbiddenUserType forbiddenUser : forbiddenUserTypes){
			if(getReferencedType(forbiddenUser).getId()==getReferencedType(forbiddenType).getId() && !forbiddenUser.equals(forbiddenType)){
				forbiddenUser.delete();
				return true;
			}
		}
		
		return false;
	}
		
}
