/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.graphql.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.graphql.schema.graphql.GraphQL

class GraphQLInit extends DIMEPostCreateHook<GraphQL>{
	
	override postCreate(GraphQL model) {
		val queryContainer = model.newQueries(30,150)
		val mutationContainer = model.newMutations(299,150)
		val forbiddenTypesContainer = model.newForbiddenTypes(568,150)
		val resolversContainer = model.newResolvers(897,150)
		
		queryContainer.width = 270
		queryContainer.height = 106
		
		mutationContainer.width = 270
		mutationContainer.height = 106
		
		forbiddenTypesContainer.width = 330
		forbiddenTypesContainer.height = 106
		
		resolversContainer.width = 330
		resolversContainer.height = 106
	}
	
}
