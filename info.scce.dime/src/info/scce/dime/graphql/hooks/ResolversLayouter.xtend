/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.graphql.hooks

import info.scce.dime.graphql.schema.graphql.Resolvers

class ResolversLayouter {
	
	def layout(Resolvers resolversContainer) {
		
		val offsetTop = 30
		val offsetBottom = 15
		val offsetY = 10
		val offsetX = 10
		
		var curY = offsetTop
		
		for (resolver: resolversContainer.resolvers) {
			resolver.y = curY
			resolver.x = offsetX
			resolver.width = resolversContainer.width - 2 * offsetX
			
			var curAttributeY = 40
			for (attribute: resolver.resolverAttributes) {
				attribute.y = curAttributeY
				attribute.x = offsetX
				attribute.width = resolver.width - 2 * offsetX
				
				if (!attribute.resolverProcesss.empty) {
					val referencedProcess = attribute.resolverProcesss.get(0)
					if (referencedProcess !== null) {
						referencedProcess.y = 40
						referencedProcess.x = offsetX
						referencedProcess.width = attribute.width - 2 * offsetX						
						attribute.height = 30 + referencedProcess.height + offsetY + offsetBottom
					}
				} else {
					attribute.height = 30
				}
				
				curAttributeY = curAttributeY + attribute.height + offsetY
			}
			
			if (!resolver.resolverAttributes.empty) {
				resolver.height = 40 + resolver.resolverAttributes.map[it.height].reduce[p1, p2|p1 + p2 + offsetY] + offsetBottom
			} else {
				resolver.height = 30
			}
						
			curY = curY + resolver.height + offsetY
		}
				
		resolversContainer.height = offsetTop + offsetBottom;
		
		if (!resolversContainer.resolvers.empty) {
			resolversContainer.height = resolversContainer.height + resolversContainer.resolvers
			.map[it.height]
			.reduce[p1, p2|p1 + p2 + offsetY]
		}
	}
}