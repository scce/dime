/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 package info.scce.dime.graphql.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.graphql.schema.graphql.ResolverAttribute
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.ComplexAttribute

class ResolverAttributePostCreateHook extends DIMEPostCreateHook<ResolverAttribute> {
		
	override postCreate(ResolverAttribute attribute) {				
		val resolver = attribute.container
		val dataType = resolver.referencedType
		
		// attribute already exists for resolvable field
		if (resolver.resolverAttributes.filter[it !== attribute].any[it|isSameAttribute(it.referencedAttribute, attribute.referencedAttribute)]) {
			attribute.delete
			return
		}
		
		// attribute does not exist on referenced data type				
		if (!dataType.attributes.any[it|isSameAttribute(it, attribute.referencedAttribute)]) {
			attribute.delete
			return
		}
					
		new ResolversLayouter().layout(attribute.rootElement.resolverss.get(0))	
	}
		
	def isSameAttribute(Attribute a, Attribute b) {
		if (a.name !== b.name) return false
		
		if ((a.isIsList && !b.isIsList) || (!a.isIsList && b.isIsList)) return false
		
		if (a instanceof PrimitiveAttribute) {
			if (b instanceof PrimitiveAttribute) {
				return a.dataType === b.dataType
			}
		}
		
		if (a instanceof ComplexAttribute) {
			if (b instanceof ComplexAttribute) {
				return a.dataType.name === b.dataType.name
			}
		}
		
		return false
	}
}