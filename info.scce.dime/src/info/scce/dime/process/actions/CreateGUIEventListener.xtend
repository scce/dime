/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.EventListener
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.TextInputStatic
import java.util.List
import de.jabc.cinco.meta.core.utils.messages.CincoMessageHandler
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.PrimitiveInputPort

//TODO beautify after auto conversion to Xtend class 
class CreateGUIEventListener extends DIMECustomAction<GUISIB> {
	
	override getName() {
		"Update Event Listeners"
	}

	override boolean canExecute(GUISIB guiSib) throws ClassCastException {
		if (guiSib.getRootElement().getProcessType() !== ProcessType::BASIC) {
			return false
		}
		var gui = guiSib.gui;
		if(gui.listenerContexts.map[events].flatten.isEmpty&&guiSib.eventListenerSuccessors.isEmpty)
		{
			return false;
		}
		return true
	}

	override void execute(GUISIB guiSib) {
		var gui = guiSib.gui;
		if (gui === null) {
			showDialog("Reference is null", "Reference is null!")
			return;
		}
		val events = gui.listenerContexts.map[events].flatten
		var x = guiSib.eventListenerSuccessors.maxX + 140;
		var y= guiSib.eventListenerSuccessors.maxY;
		for(evt:events)
		{
			//is the listener not already present?
			val guiListener = guiSib.eventListenerSuccessors.findFirst[name.equals(evt.name)]
			if(guiListener===null)
			{
				//create event listener
				var el = guiSib.rootElement.newEventListener(x,y);
				el.name = evt.name;
				//add ports
				for(port:evt.outputPorts){
					if(port instanceof ComplexOutputPort) {
						var p = el.newComplexInputPort(port.dataType,0,0);
						p.isList = port.isList
						p.name = port.name;	
					}
					if(port instanceof PrimitiveOutputPort) {
						var p = el.newPrimitiveInputPort(0,0);
						p.name = port.name;
						p.isList = port.isList
						p.dataType = port.dataType.toData.toProcess;
					}
				}
				x+=140;
				guiSib.newEventConnector(el);
			} else {
				//listener exists -> update ports
				for(guiPort:evt.outputPorts) {
					val evtPort = guiListener.inputs.findFirst[name.equals(guiPort.name)]
					//check present
					if(evtPort===null) {
						//port missing -> add new
						if(guiPort instanceof ComplexOutputPort) {
							var p = guiListener.newComplexInputPort(guiPort.dataType,0,0);
							p.isList = guiPort.isList
							p.name = guiPort.name;	
						}
						if(guiPort instanceof PrimitiveOutputPort) {
							var p = guiListener.newPrimitiveInputPort(0,0);
							p.name = guiPort.name;
							p.isList = guiPort.isList
							p.dataType = guiPort.dataType.toData.toProcess;
						}
					} else {
						//port present -> check type
						if(guiPort instanceof ComplexOutputPort && !(evtPort instanceof ComplexInputPort)) {
							evtPort.delete
							var p = guiListener.newComplexInputPort((guiPort as ComplexOutputPort).dataType,0,0);
							p.isList = guiPort.isList
							p.name = guiPort.name;
						}
						if(guiPort instanceof PrimitiveOutputPort && (evtPort instanceof ComplexInputPort)) {
							evtPort.delete
							var p = guiListener.newPrimitiveInputPort(0,0);
							p.name = guiPort.name;
							p.isList = guiPort.isList
							p.dataType = (guiPort as PrimitiveOutputPort).dataType.toData.toProcess;
						}
						
						//check static input
						if(evtPort instanceof InputStatic) {
							val pop = guiPort as PrimitiveOutputPort
							val needsRebuild = switch(evtPort) {
								IntegerInputStatic: pop.dataType.toData != PrimitiveType.INTEGER
								TextInputStatic: pop.dataType.toData != PrimitiveType.TEXT
								RealInputStatic: pop.dataType.toData != PrimitiveType.REAL
								TimestampInputStatic: pop.dataType.toData != PrimitiveType.TIMESTAMP
								BooleanInputStatic: pop.dataType.toData != PrimitiveType.BOOLEAN
							}
							if(needsRebuild) {
								evtPort.delete
								var p = guiListener.newPrimitiveInputPort(0,0);
								p.name = pop.name;
								p.isList = pop.isList
								p.dataType = pop.dataType.toData.toProcess;
							}
						}
						if(evtPort instanceof InputPort) {
							// migrate list status
							evtPort.isList = guiPort.isIsList
							//check data type matching, if not -> delete and recreate
							if(evtPort instanceof ComplexInputPort && (evtPort as ComplexInputPort).dataType.originalType !== (guiPort as ComplexOutputPort).dataType.originalType) {
								evtPort.delete
								var p = guiListener.newComplexInputPort((guiPort as ComplexOutputPort).dataType,0,0);
								p.isList = guiPort.isList
								p.name = guiPort.name;
							}
							if(evtPort instanceof PrimitiveInputPort && (evtPort as PrimitiveInputPort).dataType.toData !== (guiPort as PrimitiveOutputPort).dataType.toData) {
								evtPort.delete
								var p = guiListener.newPrimitiveInputPort(0,0);
								p.name = guiPort.name;
								p.isList = guiPort.isList
								p.dataType = (guiPort as PrimitiveOutputPort).dataType.toData.toProcess;
							}
						}
					}
					
				}
				//remove unneeded ports
				guiListener.inputs.filter[i|!evt.outputPorts.exists[name.equals(i.name)]].forEach[delete]
				
			}
		}
		//remove unneeded events
		guiSib.eventListenerSuccessors.filter[l|!events.exists[name.equals(l.name)]].forEach[delete]
	}
	
	def int maxY(List<EventListener> list){
		var maxY = 0
		for(event : list){
			if(event.y > maxY){
				maxY = event.y
			}
		}
		return maxY
	}
	
	def int maxX(List<EventListener> list){
		var maxX = 0
		for(event : list){
			if(event.x > maxX){
				maxX = event.x
			}
		}
		return maxX
	}

	def private boolean showDialog(String title, String msg) {
		return CincoMessageHandler.showQuestion(msg,title,true);
	}

}
