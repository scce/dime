/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.DirectDataFlow

import static info.scce.dime.process.helper.EdgeLayoutUtils.getManhattanPoints

import static extension info.scce.dime.process.helper.EdgeLayoutUtils.*

class SimpleTwoPointRoute<T extends DirectDataFlow> extends DIMECustomAction<DirectDataFlow> {
	
	override getName() {
		"Convert to Two-Point Route"
	}
	
	override execute(DirectDataFlow edge) {
//		val model = wrapGraphModel(edge.rootElement, edge.diagram)
//		val cEdge = model.findCDirectDataFlow(edge)
		val source = edge.sourceElement
		val target = edge.targetElement
		
		for (i: 0 ..< edge.bendpoints.size)
			edge.removeBendpoint(0)
			
		for (point : getManhattanPoints(source, target)) {
			edge.addBendpoint(point.x, point.y)
		}
	}
	 
}
