/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import graphmodel.Edge
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ComplexDirectDataFlow
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexRead
import info.scce.dime.process.process.ControlFlow
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.PrimitiveDirectDataFlow
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveRead
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType
import java.util.List
import java.util.Optional
import java.util.stream.Collectors
import de.jabc.cinco.meta.core.utils.messages.CincoMessageHandler

//TODO beautify after auto conversion to Xtend class
class SecureBasic extends DIMECustomAction<ProcessSIB> {
	

	override getName() {
		"Secure Process"
	}

	override boolean canExecute(ProcessSIB processSib) throws ClassCastException {
		if (processSib.getRootElement().getProcessType() !== ProcessType::BASIC) {
			return false
		}
		if (processSib.getContainer() instanceof GuardContainer) {
			return false
		}
		var Process eObj = processSib.getProMod()
		if(eObj === null) return false
		if(eObj.getProcessType() !== ProcessType::BASIC) return false
		return true
	}

	//TODO create/use extension methods for this stuff
	override void execute(ProcessSIB processSib) {
		System::out.println("Secure Basic Process SIB")
		val Process eObj = processSib.getProMod()
		if (eObj === null) {
			showDialog("Reference is null", "Secure failed. Reference is null!")
			return;
		}
		val Process root = processSib.getRootElement()
		// Create GuardContainer
		val GuardContainer cgc = root.newGuardContainer(processSib.getX() - 5, processSib.getY() - 5, processSib.getWidth() + 10,
			processSib.getHeight() + 10)
		// Add Process Sib to GuardContainer
		// Ugly Workarround, because of missing moveTo Methods on Containers
		var GuardedProcessSIB newPSib = cgc.newGuardedProcessSIB(eObj, 5, 5)
		newPSib.setLabel(processSib.getLabel())
		// Reconnect Input Data edges
		for (InputPort n : processSib.getInputs().stream().filter([n|n instanceof InputPort]).map([n|(n as InputPort)]).
			collect(Collectors::toList())) {
			for (Edge e : n.getIncoming()) {
				// Find corresponding input port on new guardedPSib
				var Optional<Input> newPortOpt = newPSib.getInputs().stream().filter([f|(f instanceof InputPort)]).
					filter([f|f.getName().equals(n.getName())]).findFirst()
				if (newPortOpt.isPresent()) {
					var InputPort newPort = (newPortOpt.get() as InputPort)
					if (e instanceof PrimitiveRead) {
						var PrimitiveRead cr = (e as PrimitiveRead)
						cr.reconnectTarget((newPort as PrimitiveInputPort))
					}
					if (e instanceof ComplexRead) {
						var ComplexRead cr = (e as ComplexRead)
						cr.reconnectTarget((newPort as ComplexInputPort))
					}
					if (e instanceof ComplexDirectDataFlow) {
						var ComplexDirectDataFlow cr = (e as ComplexDirectDataFlow)
						cr.reconnectTarget((newPort as ComplexInputPort))
					}
					if (e instanceof PrimitiveDirectDataFlow) {
						var PrimitiveDirectDataFlow cr = (e as PrimitiveDirectDataFlow)
						cr.reconnectTarget((newPort as PrimitiveInputPort))
					}
				}
			}
		}
		// Remove Max Count
		newPSib.getInputs().stream().filter([n|n.getName().equals("maxUsageCount")]).findFirst().ifPresent[delete]
		// Remove Branches
		cgc.getAbstractBranchSuccessors().forEach([n|n.delete()])
		// Reconnect Branches from the Process Sib to the GuardContainer
		var List<BranchConnector> branches = processSib.getOutgoing(typeof(BranchConnector))
		branches.stream().forEach([n|n.reconnectSource(cgc)])
		processSib.getIncoming(typeof(ControlFlow)).stream().forEach([n|n.reconnectTarget(cgc)])
		// Delete Process Sib
		processSib.delete()
		PublishBasic::removeDublicatedBranches(cgc as GuardContainer)
	}

	def private boolean showDialog(String title, String msg) {
		return CincoMessageHandler.showQuestion(msg, title,true)
	}
}
