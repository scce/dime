/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.EventListener

/** 
 * Context menu action, which converts all static ports to
 * input ports
 * @author zweihoff
 */
class EventMultiStaticToInput extends DIMECustomAction<EventListener> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Convert to input ports"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(EventListener guiSib) {
		//at least one primitive input port without incoming edge
		!guiSib.inputStatics.empty
	}

	override void execute(EventListener guiSib) {
		guiSib.inputStatics.forEach[n|
			val p = new StaticToPrimitivePort
			p.execute(n)
		]
	}
}
