/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import graphmodel.Node
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.DataContext
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveVariable
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.Variable
import java.util.List
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeVariable
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.EventListener

class CreateInputDataContext extends CincoCustomAction<Node> {
	
	override getName() '''Create/Update input data context'''
	
	override canExecute(Node node) {
		node.container instanceof Process // TODO handle other containers than Process model
		&& node.inputPorts.exists[!predecessors.exists[it instanceof Variable]]
	}
	
	override execute(Node sib) {
		val container = sib.container as Process
		val ports =
			sib.inputPorts
				.filter[!predecessors.exists[it instanceof Variable]]
				.sortBy[it.y]
		val dataContext =
			getInputDataContext(sib.inputPorts)
			?: container.newDataContext(sib.x - 30, sib.y, 30, sib.height)
		for (port : ports) {
			val variable = dataContext.newVariable(port) => [
				resize(10,10)
				name = port.name + port.id
				isList = port.isList
			]
			val source = port.incoming.head?.sourceElement
			if (source instanceof OutputPort) {
				port.incoming.head.delete
				source.newUpdate(variable)
			}
			variable.newRead(port)
		}
		var xOffset = 10
		for (port : sib.inputPorts) {
			val variable = port.inputVariable
			if (variable?.isGenerated) {
				variable.move(xOffset, port.alignedY)
				xOffset += 10
			}
		}
		val width = 20 + dataContext.variables.size * 10
		dataContext.move(sib.x - width, sib.y)
		dataContext.resize(width, sib.height)
	}
	
	def getInputPorts(Node node) {
		switch node {
			DataFlowTarget: node.inputPorts
			EventListener: node.inputPorts
			default: #[]
		}
	}
	
	def getInputDataContext(Iterable<InputPort> inputPorts) {
		for (port : inputPorts) {
			val variable = port.inputVariable
			if (variable?.isGenerated) {
				return variable.container
			}
		}
	}
	
	def getInputVariable(InputPort port) {
		port.predecessors.filter(Variable).head
	}
	
	def isGenerated(Variable variable) {
		variable.width == 10 && variable.height == 10
	}
	
	def layout(List<Branch> branches, int newY) {
		for (branch : branches) {
			branch.moveTo(branch.rootElement, branch.x, newY)
		}
	}
	
	
	dispatch def newVariable(DataContext dataContext, ComplexInputPort port) {
		dataContext.newComplexVariable(port.dataType, 10, port.alignedY)
	}
	
	dispatch def newVariable(DataContext dataContext, PrimitiveInputPort port) {
		dataContext.newPrimitiveVariable(10, port.alignedY) => [
			dataType = port.dataType
		]
	}
	
	dispatch def newVariable(DataContext dataContext, JavaNativeInputPort port) {
		dataContext.newJavaNativeVariable(port.dataType, 10, port.alignedY) => [
		]
	}
	
	dispatch def newVariable(DataContext dataContext, InputPort port) {
		warn("Unmatched input port: " + port)
	}
	
	dispatch def newUpdate(ComplexOutputPort source, ComplexVariable target) {
		source.newComplexUpdate(target)
	}
	
	dispatch def newUpdate(PrimitiveOutputPort source, PrimitiveVariable target) {
		source.newPrimitiveUpdate(target)
	}
	
	dispatch def newUpdate(JavaNativeOutputPort source, JavaNativeVariable target) {
		source.newJavaNativeUpdate(target)
	}
	
	dispatch def newUpdate(OutputPort source, Variable target) {
		warn("Unmatched types: " + source.class + " and " + target.class)
	}
	
	dispatch def newRead(ComplexVariable source, ComplexInputPort target) {
		source.newComplexRead(target)
	}
	
	dispatch def newRead(PrimitiveVariable source, PrimitiveInputPort target) {
		source.newPrimitiveRead(target)
	}
	
	dispatch def newRead(JavaNativeVariable source, JavaNativeInputPort target) {
		source.newJavaNativeRead(target)
	}
	
	dispatch def newRead(Variable source, InputPort target) {
		warn("Unmatched types: " + source.class + " and " + target.class)
	}
	
	def getAlignedY(Node node) {
		node.y + node.height/2 - 5
	}
}
