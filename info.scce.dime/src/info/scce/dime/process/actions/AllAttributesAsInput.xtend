/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.Attribute
import info.scce.dime.process.helper.PortUtils.Sync
import info.scce.dime.process.process.CreateSIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.TransientCreateSIB

import static extension info.scce.dime.process.helper.PortUtils.addInput
import info.scce.dime.data.data.ExtensionAttribute

/**
 * Context menu action, which adds all attributes of the created type as input
 * ports, so that a "full constructor" can automatically be initialized.
 * 
 * @author naujokat
 */
class AllAttributesAsInput<T extends SIB> extends DIMECustomAction<SIB> {
	val OFFSET = 30

	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Add All Attributes as Input"
	}

	override canExecute(SIB sib) {
		val sibPortNames = sib.inputs.map[name]
		val type = sib.createdType
		type != null && type.inheritedAttributes.map[name].exists[!sibPortNames.contains(it)]
	}

	override void execute(SIB sib) {
		val sync = new Sync<Input, Attribute> => [
			equals = [input, attr|input.name == attr.name]
			add = [attr|sib.addInput(attr)]
		]
		sync.apply(sib.inputs, sib.createdType.inheritedAttributes.filter[!(it instanceof ExtensionAttribute)])

		layoutBranch(sib)
	}

	def layoutBranch(SIB sib) {
		var newY = sib.y + sib.height + OFFSET
		for (branch : sib.branchSuccessors) {
			branch.moveTo(sib.rootElement, branch.x, newY)
		}
	}

	def getCreatedType(SIB it) {
		switch it {
			CreateSIB: createdType
			TransientCreateSIB: createdType
		}
	}
}
