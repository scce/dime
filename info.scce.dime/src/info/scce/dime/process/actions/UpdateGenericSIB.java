/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions;

import static info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider.getSupportedModelElement;

import org.eclipse.emf.ecore.EObject;
import de.jabc.cinco.meta.core.utils.messages.CincoMessageHandler;

import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericSIBBuild;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.UpdateGenericSIBBuild;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.process.build.BranchCreator;
import info.scce.dime.process.build.UpdateBranchCreator;
import info.scce.dime.process.process.GenericSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.SIB;

public class UpdateGenericSIB extends UpdateSIBAction<GenericSIB> {

	protected Process cModel;
	protected BranchCreator branchCreator;
	protected GenericSIBBuild<EObject> sibBuild;

	@Override
	public void update(GenericSIB sib) {
		cModel = sib.getRootElement();
		SIB cSib = sib;
		sibBuild = getGenericSIBBuild(sib);
		if (sib.getReferencedObject() == null) {
			letUserKnow("Reference is null", "Update failed. Reference not found!");
			sib.delete();
			return;
		}
		branchCreator = new UpdateBranchCreator(cSib);
		UpdateGenericSIBBuild updateGenericSIBBuild = new UpdateGenericSIBBuild(sibBuild, sib, branchCreator);
		updateGenericSIBBuild.update();
	}

	
	private GenericSIBBuild<EObject> getGenericSIBBuild(GenericSIB sib) {
		EObject sibReference = sib.getReferencedObject();
		IModeltrafoSupporter<EObject> modelTrafoSupporter = getSupportedModelElement(sibReference);
		if (modelTrafoSupporter != null) {
			IGenericSIBBuilder<EObject> modellingProvider = modelTrafoSupporter.getModellingProvider();
			GenericSIBBuild<EObject> sibBuild = modellingProvider.getSIBBuild(sibReference);
			return sibBuild;
		} 
		return null;
	}
	
	private void letUserKnow(String title, String msg) {
		CincoMessageHandler.showMessage(msg, title);
	}

}
