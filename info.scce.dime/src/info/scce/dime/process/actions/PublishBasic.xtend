/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ControlFlow
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.SIB
import java.util.HashMap
import java.util.Map
import de.jabc.cinco.meta.core.utils.messages.CincoMessageHandler

//TODO beautify after auto conversion to Xtend class
class PublishBasic extends DIMECustomAction<GuardedProcessSIB> {
	
	override getName() {
		"Publish Process"
	}

	override boolean canExecute(GuardedProcessSIB processSib) throws ClassCastException {
		if (processSib.getRootElement().getProcessType() !== ProcessType::BASIC) {
			return false
		}
		if (!(processSib.getContainer() instanceof GuardContainer)) {
			return false
		}
		val Process eObj = processSib.getProMod()
		if(eObj === null) return false
		if(eObj.getProcessType() !== ProcessType::BASIC) return false
		return true
	}

	override void execute(GuardedProcessSIB processSib) {
		val Process eObj = processSib.getProMod()
		if (eObj === null) {
			showDialog("Reference is null", "Secure failed. Reference is null!")
			return;
		}
		val Process root = processSib.getRootElement()
		// Find GuardContainer
		val GuardContainer cgc = (processSib.getContainer() as GuardContainer)
		// Remove denied branch
		// Create ProcessSIB
		val ProcessSIB newCProcess = root.newProcessSIB(processSib.getProMod(), cgc.getX() + 5, cgc.getY() + 5)
		// Move all Inputs
		processSib.getInputs().forEach([n|
			// FIXME C-API currently has no `moveTo()` method for containers...
			n.moveTo(newCProcess, n.getX(), n.getY())
		])
		// Reconnect Incomming Controllflow
		cgc.getIncoming(typeof(ControlFlow)).forEach([n|n.reconnectTarget(newCProcess)])
		// Reconnect Outgoing BranchConnectors
		cgc.getOutgoing(typeof(BranchConnector)).forEach([n|n.reconnectSource(newCProcess)])
		cgc.delete()
		removeDublicatedBranches(newCProcess)
		newCProcess.getAbstractBranchSuccessors().stream().filter([n|n.getName().equals("denied")]).forEach([n |
			n.delete()
		])
	}

	def private boolean showDialog(String title, String msg) {
		return CincoMessageHandler.showQuestion(msg, title, true)
	}

	def static void removeDublicatedBranches(SIB sib) {
		// Remove Duplicated Branches
		val Map<String, AbstractBranch> knownBranchNames = new HashMap<String, AbstractBranch>()
		// Find already used branches
		sib.getAbstractBranchSuccessors().forEach([n |
			{
				if (!n.getOutgoing(typeof(ControlFlow)).isEmpty()) {
					knownBranchNames.put(n.getName(), n)
				} else if (n.getOutputs().stream().filter([e|!(e.getOutgoing().isEmpty())]).findAny().isPresent()) {
					knownBranchNames.put(n.getName(), n)
				}
			}
		])
		sib.getAbstractBranchSuccessors().forEach([n |
			{
				if (knownBranchNames.containsKey(n.getName())) {
					if (!knownBranchNames.get(n.getName()).equals(n)) {
						n.delete()
					}
				} else {
					knownBranchNames.put(n.getName(), n)
				}
			}
		])
	}
}
