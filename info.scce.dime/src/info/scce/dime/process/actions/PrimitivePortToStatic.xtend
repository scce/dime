/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import graphmodel.Container
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveType

import static extension info.scce.dime.process.helper.PortUtils.addStaticPort
import static extension info.scce.dime.process.helper.PortUtils.reconnectIncoming

class PrimitivePortToStatic extends DIMECustomAction<PrimitiveInputPort> {
	
	override getName() {
		"Convert Input to Static Value"
	}

	override canExecute(PrimitiveInputPort port) {
		PrimitiveType::FILE !== port.dataType
	}

	override execute(PrimitiveInputPort port) {
		try {
			val container = port.container as Container
			val oldPorts = newArrayList
			// re-add all ports to preserve their order
			container.allNodes.filter(Input).forEach[oldPort |
				if (oldPort === port) {
					container.addStaticPort(port)
				} else {
					var oldClone = oldPort.clone(container)
					oldPort.reconnectIncoming(oldClone)
				}
				oldPorts.add(oldPort)
			]
			oldPorts.forEach[delete]
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
}
