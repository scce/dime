/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.StartSIB

class StartSIBNewPrimitiveInput extends DIMECustomAction<StartSIB> {
	val OFFSET = 30
	override getName() {
		"New Primitive Input"
	}

	//TODO beautify after auto conversion to Xtend class

	override execute(StartSIB sib) {
		
		try {
			sib.newPrimitiveOutputPort(1, 1)
			layout(sib)
		} catch (Exception e) {
			e.printStackTrace()
		}

	}
	
	def layout(StartSIB sib) {
		var newY = sib.y + sib.height + OFFSET
		for (branch : sib.successors) {
			branch.moveTo(sib.rootElement, branch.x, newY)
		}
	}
}
