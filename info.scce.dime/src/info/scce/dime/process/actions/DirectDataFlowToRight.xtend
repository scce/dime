/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.DirectDataFlow

import static extension info.scce.dime.process.helper.EdgeLayoutUtils.getBendpoints
import static extension info.scce.dime.process.helper.EdgeLayoutUtils.moveBendpoint

class DirectDataFlowToRight<T extends DirectDataFlow> extends DIMECustomAction<DirectDataFlow> {

	override getName() {
		"Move right"
	}
	
	override canExecute(DirectDataFlow edge) {
		!edge.bendpoints.isEmpty
	}
	
	override execute(DirectDataFlow edge) {
		val bps = edge.bendpoints
		for (i: 0 ..< bps.size) {
			val bp = bps.get(i)
			edge.moveBendpoint(i, bp.x + 10, bp.y)
		}
	}
}
