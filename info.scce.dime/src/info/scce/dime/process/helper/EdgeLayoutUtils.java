/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.graphiti.features.IAddBendpointFeature;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IMoveBendpointFeature;
import org.eclipse.graphiti.features.IRemoveBendpointFeature;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.impl.AddBendpointContext;
import org.eclipse.graphiti.features.context.impl.MoveBendpointContext;
import org.eclipse.graphiti.features.context.impl.RemoveBendpointContext;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.platform.IDiagramBehavior;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.services.GraphitiUi;

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension;
import graphmodel.Edge;
import graphmodel.ModelElementContainer;
import graphmodel.Node;
import graphmodel.ModelElement;


// TODO transform to extension class
public class EdgeLayoutUtils {
	
	public static int EDGE_OFFSET_LEFT = 15;
	public static WorkbenchExtension workbench = new WorkbenchExtension();
	
	public static List<Location> getManhattanPoints(Node source, Node target) {
		Location sLoc = getAbsoluteLocation(source);
		Location tLoc = getAbsoluteLocation(target);
		
		int x = Math.min(sLoc.x - EDGE_OFFSET_LEFT, tLoc.x - EDGE_OFFSET_LEFT);
		
		List<Location> bps = new ArrayList<>();
		bps.add(new Location(x, sLoc.y + source.getHeight() / 2));
		bps.add(new Location(x, tLoc.y + target.getHeight() / 2));
		
		return bps;
	}
	
	public static Location getAbsoluteLocation(Node cnode) {
		Location pos = new Location(cnode.getX(), cnode.getY());
		ModelElementContainer ccontainer = cnode.getContainer();
		while (ccontainer instanceof Node) {
			pos.x += ((Node) ccontainer).getX();
			pos.y += ((Node) ccontainer).getY();
			ccontainer = ((Node) ccontainer).getContainer();
		}
		return pos;
	}
	
	public static FreeFormConnection getConnection(de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CEdge cedge) {
		PictogramElement pe = cedge.getPictogramElement();
		if (pe instanceof FreeFormConnection)
			return (FreeFormConnection) pe;
		return null;
	}
	
	public static FreeFormConnection getConnection(Edge edge) {
		
		PictogramElement pe = workbench.getPictogramElement(edge);
		if (pe instanceof FreeFormConnection)
			return (FreeFormConnection) pe;
		return null;
	}
	
	public static double getDistance(Location l1, Location l2) {
		return Math.sqrt(Math.pow((l2.x - l1.x), 2) + Math.pow((l2.y - l1.y), 2));
	}
	
	public static PictogramElement[] getSelectedPictograms() {
		DiagramEditor activeEditor = workbench.getActiveDiagramEditor();
		return activeEditor != null
				? activeEditor.getSelectedPictogramElements()
				: new PictogramElement[0];
	}
	
	public static List<FreeFormConnection> getMovableEdges(de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement element) {
		return getMovableEdges((PictogramElement)element.getPictogramElement());
	}
	
	public static List<FreeFormConnection> getMovableEdges(ModelElement element) {
		PictogramElement pe = workbench.getPictogramElement(element);
		return getMovableEdges(pe);
	}
	
	public static List<FreeFormConnection> getMovableEdges(PictogramElement movingPe) {
		List<FreeFormConnection> list = new ArrayList<>();
		List<Anchor> anchors = getAnchors(movingPe);
		PictogramElement[] selection = getSelectedPictograms();
		if (selection != null) {
			for (PictogramElement pe : selection) {
				for (Anchor toAnchor : getAnchors(pe)) {
					for (Connection con : toAnchor.getIncomingConnections()) {
						if (con instanceof FreeFormConnection
								&& anchors.contains(con.getStart())) {
							list.add((FreeFormConnection) con);
						}
					}
					for (Connection con : toAnchor.getOutgoingConnections()) {
						if (con instanceof FreeFormConnection
								&& anchors.contains(con.getEnd())) {
							list.add((FreeFormConnection) con);
						}
					}
				}
			}
		}
		return list;
	}
	
	public static List<Anchor> getAnchors(PictogramElement pe) {
		List<Anchor> anchors = new ArrayList<>();
		if (pe instanceof AnchorContainer) {
			anchors.addAll(((AnchorContainer) pe).getAnchors());
		}
		if (pe instanceof ContainerShape) {
			ContainerShape container = (ContainerShape) pe;
			List<Shape> children = container.getChildren();
			for (Shape child : children) {
				if (child instanceof ContainerShape) {
					anchors.addAll(getAnchors(child));
				} else {
					anchors.addAll(child.getAnchors());
				}
			}
		}
		return anchors;
	}
	
	public static List<Point> getBendpoints(Edge edge) {
		FreeFormConnection connection = getConnection(edge);
		if (connection != null)
			return connection.getBendpoints();
		return new ArrayList<>();
	}
	
	public static List<Point> getBendpoints(de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CEdge cedge) {
		FreeFormConnection connection = getConnection(cedge);
		if (connection != null)
			return connection.getBendpoints();
		return new ArrayList<>();
	}
	
	public static void addBendpoint(de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CEdge cEdge, int x, int y) {
		addBendpoint((Edge) cEdge, x, y);
	}
	
	public static void addBendpoint(Edge edge, int x, int y) {
		int index = getBendpoints(edge).size();
		AddBendpointContext ctx = new AddBendpointContext(getConnection(edge), x, y, index);
		IFeatureProvider fp = getFeatureProvider(edge);
		IAddBendpointFeature ftr = fp.getAddBendpointFeature(ctx);
		executeFeature(ftr, ctx);
	}
	
	public static void moveBendpoint(de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CEdge cEdge, int bendpointIndex, int x, int y) {
		moveBendpoint((Edge) cEdge, bendpointIndex, x, y);
	}
	
	public static void moveBendpoint(Edge edge, int bendpointIndex, int x, int y) {
		MoveBendpointContext ctx = new MoveBendpointContext(null);
		ctx.setConnection(getConnection(edge));
		ctx.setBendpointIndex(bendpointIndex);
		ctx.setLocation(x, y);
		
		IFeatureProvider fp = getFeatureProvider(edge);
		IMoveBendpointFeature ftr = fp.getMoveBendpointFeature(ctx);
		executeFeature(ftr, ctx);
	}
	
	public static void removeBendpoint(de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CEdge cEdge, int bendpointIndex) {
		removeBendpoint((Edge) cEdge, bendpointIndex);
	}
	
	public static void removeBendpoint(Edge edge, int bendpointIndex) {
		RemoveBendpointContext ctx = new RemoveBendpointContext(getConnection(edge), null);
		ctx.setBendpointIndex(bendpointIndex);
		
		IFeatureProvider fp = getFeatureProvider(edge);
		IRemoveBendpointFeature ftr = fp.getRemoveBendpointFeature(ctx);
		executeFeature(ftr, ctx);
	}
	
	public static IFeatureProvider getFeatureProvider(ModelElement elm) {
		Diagram diagram = workbench.getDiagram(elm);
		return GraphitiUi.getExtensionManager().createFeatureProvider(diagram);
	}
	
	public static Object executeFeature(IFeature ftr, IContext ctx) {
		IDiagramBehavior db = ftr.getFeatureProvider().getDiagramTypeProvider().getDiagramBehavior();
		return db.executeFeature(ftr, ctx);
	}
	
	public static class Location {
		
		public int x = 0;
		public int y = 0;
		
		public static Location from(org.eclipse.graphiti.mm.algorithms.styles.Point point) {
			return new Location(point.getX(), point.getY());
		}
		
		public static List<Location> from(Collection<org.eclipse.graphiti.mm.algorithms.styles.Point> points) {
			if (points == null)
				return new ArrayList<>();
			return points.stream().map(Location::from).collect(Collectors.toList());
		}
		
		public Location(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
}
