/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.helper

import de.jabc.cinco.meta.runtime.active.Memoizable
import de.jabc.cinco.meta.runtime.xapi.CollectionExtension
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension

import graphmodel.Container
import graphmodel.IdentifiableElement
import graphmodel.Node

import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.AbstractIterateSIB
import info.scce.dime.process.process.Attribute
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ComplexAttribute
import info.scce.dime.process.process.ComplexAttributeConnector
import info.scce.dime.process.process.ComplexDirectDataFlow
import info.scce.dime.process.process.ComplexExtensionAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexListAttribute
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.DirectDataFlow
import info.scce.dime.process.process.EntryPointProcessSIB
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.EventListener
import info.scce.dime.process.process.GenericSIB
import info.scce.dime.process.process.ExtensionAttribute
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.GuardProcessSIB
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.JavaNativeVariable
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveAttribute
import info.scce.dime.process.process.PrimitiveDirectDataFlow
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveListAttribute
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.PrimitiveVariable
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.Read
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.StartSIB
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.process.process.Update
import info.scce.dime.process.process.Variable

import java.util.HashMap
import java.util.List
import java.util.Set

import static info.scce.dime.process.process.PrimitiveType.*

class ProcessExtension {
	
	extension CollectionExtension = new CollectionExtension
	extension DataExtension = DataExtension.instance
	
	static public final HashMap<PrimitiveType,info.scce.dime.data.data.PrimitiveType>
		DATA_PRIMITIVE_TYPE = newHashMap(
			BOOLEAN		-> info.scce.dime.data.data.PrimitiveType::BOOLEAN,
			FILE		-> info.scce.dime.data.data.PrimitiveType::FILE,
			INTEGER		-> info.scce.dime.data.data.PrimitiveType::INTEGER,
			REAL		-> info.scce.dime.data.data.PrimitiveType::REAL,
			TEXT		-> info.scce.dime.data.data.PrimitiveType::TEXT,
			TIMESTAMP	-> info.scce.dime.data.data.PrimitiveType::TIMESTAMP
		)
		
	// lambda expression to resolve prime references
	val followPrimeRefs = [IdentifiableElement elm | switch it : elm {
			
			// Process
			info.scce.dime.process.process.GUISIB: gui
			info.scce.dime.process.process.ProcessSIB: proMod
			info.scce.dime.process.process.GuardedProcessSIB: proMod
			info.scce.dime.process.process.GuardProcessSIB: securityProcess
			
			// GUI
			info.scce.dime.gui.gui.GUISIB: gui
			info.scce.dime.gui.gui.ProcessSIB: proMod as info.scce.dime.process.process.Process
			info.scce.dime.gui.gui.SecuritySIB: proMod as info.scce.dime.process.process.Process
			info.scce.dime.gui.gui.GuardSIB: process as info.scce.dime.process.process.Process
			
			// DAD
			info.scce.dime.dad.dad.ProcessComponent: model
			info.scce.dime.dad.dad.LoginComponent: model
			info.scce.dime.dad.dad.FindLoginUserComponent: model
			info.scce.dime.dad.dad.DataComponent: model
	}]
		
	def isIgnoredBranch(SIB sib, String branchName) {
		sib.ignoredBranch.map[name].contains(branchName)
	}
	
	@Memoizable
	def List<Input> getProcessInputs(ProcessSIB it) {
		inputs.sortBy[name]
	}
	
	@Memoizable
	def List<Input> getProcessInputs(GuardProcessSIB it) {
		inputs.sortBy[name]
	}
	
	@Memoizable
	def List<Input> getProcessInputs(GuardedProcessSIB it) {
		inputs.sortBy[name]
	}
	
	@Memoizable
	def Type getConcreteUserType(Process it) {
		(processInputs.findFirst[name.equals("currentUser")] as ComplexOutputPort)?.dataType
	}
	
	@Memoizable
	def Iterable<Container> getProcessAllInputs(Process it) {
		(processInputs + processInputStatics).filter(Container).sortBy [
			switch it {
				OutputPort: name
				ProcessPlaceholderSIB: label
			}
		]
	}
	
	@Memoizable
	def Iterable<Container> getProcessEntryPointAllInputs(EntryPointProcessSIB it) {
		(processInputs + processInputStatics).filter(Container).sortBy [
			switch it {
				InputPort: name
			}
		]
	}
	
	@Memoizable
	def List<ProcessPlaceholderSIB> getProcessInputStatics(Process it) {
		processPlaceholderSIBs.distinctByKey [ label ].sortBy [ label ]
	}
	
	@Memoizable
	def List<OutputPort> getProcessInputs(Process it) {
		startSIB.outputPorts.sortBy [ name ]
	}
	
	@Memoizable
	def Iterable<OutputPort> getProcessInputsInUse(Process process) {
		process.processInputs.filter[!outgoingDataFlows.isEmpty]
	}
	
	@Memoizable
	def StartSIB getStartSIB(Process process) {
		process.startSIBs.head
	}
	
	@Memoizable
	def Iterable<DataFlow> getOutgoingDataFlows(Iterable<OutputPort> outputPorts) {
		outputPorts.flatMap[outgoingDataFlows]
	}
	
	@Memoizable
	def Iterable<DataFlowTarget> getDataFlowTargets(Process process) {
		process.allNodes.filter(DataFlowTarget)
	}
	
	@Memoizable
	def List<Process> getSubProcesses(Process process) {
		process.processSIBs.map[proMod]
	}
	
	@Memoizable
	def ComplexVariable getComplexVariablePredecessor(ComplexVariable cv) {
		cv.complexVariablePredecessors.head
	}
	
	@Memoizable
	def Iterable<Variable> getVariables(Process process) {
		process.dataContexts.map[variables].flatten.filter[v | !(v instanceof ComplexVariable) || (v as ComplexVariable).complexVariablePredecessor === null]
	}
	
	/**
	 * Identifies the order in which write operations related to data flow edges must
	 * be executed. In the resulting sequence, all edges appear *before* any edge that
	 * may depend on them. Moreover, Update edges appear before DirectDataFlow edges.
	 */
	@Memoizable
	def List<DataFlow> sortTopologically(Iterable<DataFlow> dataFlows) {
		dataFlows.sortWith[ a, b |
			if (a == b) 0
			else if (a.targetElement == b.targetElement) 0
			else if (a instanceof Update) {
				if (b instanceof Update) {
					if (b.targetVarPathToRoot.exists[it?.id == a.targetElement?.id]) -1
					else if (a.targetVarPathToRoot.exists[it?.id == b.targetElement?.id]) 1
				} else -1
			}
			else if (b instanceof Update) 1
			else 0
		]
	}
	
	private def getTargetVarPathToRoot(DataFlow dataFlow) {
		switch v : dataFlow.targetElement {
			PrimitiveAttribute: v.pathToRootVariable
			ComplexAttribute: v.pathToRootVariable
			ComplexVariable: v.pathToRootVariable
			default: #[]
		}
	}
	
	private def getPathToRootVariable(Attribute ca) {
		(ca.container as ComplexVariable).pathToRootVariable
	}
	
	@Memoizable
	def Iterable<ComplexVariable> getPathToRootVariable(ComplexVariable cv) {
		#[cv] + (cv.complexVariablePredecessor?.pathToRootVariable ?: #[])
	}
	
	@Memoizable
	def Set<OutputPort> getUniqueSources(Iterable<DirectDataFlow> ddfs) {
		ddfs.map[source].toSet
	}
	
	@Memoizable
	def boolean passesGUISIB(DirectDataFlow ddf) {
		val sourceBranch = ddf.sourceElement.container as Node
		val targetSIB = ddf.targetElement.container as Node
		sourceBranch.existsPathWithGUISIB(targetSIB)
	}
	
	@Memoizable
	def boolean existsPathWithGUISIB(Node source, Node targetNode) {
		extension val GraphModelExtension = new GraphModelExtension
		var target =
			if (targetNode instanceof EventListener) {
				targetNode.GUISIBPredecessors.head
			} else targetNode
		if (target instanceof GUISIB) {
			if (target.defaultContent !== null // major/minor setting
					|| !target.ignoredBranch.isEmpty // immediate loop
					|| !target.findPathsTo(target).isEmpty) { // loop
				return true
			}
		}
		val targetNodes = (#[target] + target.findParents(DataFlowTarget)).toList
		val sibs = targetNodes.flatMap[
			source.findNodesViaControlFlowTo(it) + it.findNodesViaControlFlowTo(it)
		]
		val sibsOnPaths = sibs.filter(SIB).toSet => [removeAll(targetNodes)]
		return sibsOnPaths.exists[it.isOrContainsGUISIB]
	}
	
	def Set<Node> findNodesViaControlFlowTo(Node source, Node target) {
		findNodesViaControlFlowTo(source, target, newHashSet)
	}
	
	@Memoizable
	def Set<Node> findNodesViaControlFlowTo(Node source, Node target, Set<Node> seen) {
		extension val GraphModelExtension = new GraphModelExtension
		val nodes1 = source.findPathsTo(target).flatten.toSet
		val nodes2 = if (source.rootElement.nodes.exists[it instanceof EventListener]) {
			source.findPathsTo(EventListener).flatMap[ path |
				val evt = IterableExtensions.last(path) as EventListener
				val guiSib = evt.GUISIBPredecessors.head
				if (seen.add(guiSib)) {
					guiSib.findNodesViaControlFlowTo(target, seen) => [
						add(guiSib)
						addAll(path)
					]
				} else (#[guiSib] + path)
			].toSet
		} else #[]
		nodes1 => [addAll(nodes2)]
	}
	
	@Memoizable
	def boolean isOrContainsGUISIB(Node node) {
		extension val GraphModelExtension = new GraphModelExtension
		switch node {
			GUISIB: true
			ProcessSIB, GuardedProcessSIB: !node.findDeeply(GUISIB, followPrimeRefs).isEmpty
			GuardContainer: node.guardedProcessSIBs.exists[it.isOrContainsGUISIB]
			default: false
		}
	}
	
	@Memoizable
	def boolean isUsedBeforeAndAfterGUI(Variable variable) {
		extension val GraphModelExtension = new GraphModelExtension
		val usingSIBs =
			(#[variable] + variable.findSuccessorsVia(ComplexAttributeConnector))
				.filter(Variable)
				.flatMap[#[it] + it.find(Attribute)]
				.flatMap[it.findSuccessorsVia(DataFlow) + it.findPredecessorsVia(DataFlow)]
				.map[it.container as Node]
				.toSet
		usingSIBs.exists[ usingSIB | 
			usingSIBs.exists[ otherUsingSIB |
				otherUsingSIB.existsPathWithGUISIB(usingSIB)
				|| usingSIB.existsPathWithGUISIB(otherUsingSIB)
			]
		]
	}
	
	def getSource(DirectDataFlow ddf) {
		ddf.sourceElement as OutputPort
	}
	
	@Memoizable
	def AbstractBranch getBranchByName(SIB sib, String name) {
		sib.branches.filter[b | b.name == name].head
	}
	
	@Memoizable
	def Iterable<AbstractBranch> getBranches(DataFlowTarget sib) {
		sib.getOutgoing(BranchConnector).map[targetElement].filter(AbstractBranch)
	}
	
	@Memoizable
	def DataFlow getDataEdge(InputPort inputPort) {
		inputPort.incoming.filter(DataFlow).filter[x | x instanceof Read || x instanceof DirectDataFlow].head
	}
	
	@Memoizable
	def Iterable<OutputPort> getOutputsInUse(AbstractBranch branch) {
		branch.outputPorts.filter[!outgoingDataFlows.isEmpty]
	}
	
	@Memoizable
	def Iterable<InputStatic> getStaticModelBranchOutputs(EndSIB endSIB) {
		endSIB.inputs.filter(InputStatic)
	}
	
	@Memoizable
	def Iterable<InputPort> getInputsInUse(SIB it) {
		complexInputsInUse + primitiveInputsInUse + javaNativeInputsInUse 
	}
	
	@Memoizable
	def Iterable<PrimitiveInputPort> getPrimitiveInputsInUse(SIB sib) {
		sib.primitiveInputPorts.filter[dataEdge !== null]
	}
	
	@Memoizable
	def Iterable<JavaNativeInputPort> getJavaNativeInputsInUse(SIB sib) {
		sib.javaNativeInputPorts.filter[dataEdge !== null]
	}
	
	@Memoizable
	def Iterable<ComplexInputPort> getComplexInputsInUse(SIB sib) {
		sib.complexInputPorts.filter[dataEdge !== null]
	}
	
	@Memoizable
	def Iterable<AbstractIterateSIB> getIterateSIBsInUse(Process process) {
		process.abstractIterateSIBs.filter[inputsInUse.size > 0]
	}
	
	def getDataType(InputStatic input) {
		switch input {
			BooleanInputStatic: BOOLEAN
			IntegerInputStatic: INTEGER
			RealInputStatic: REAL
			TextInputStatic: TEXT
			TimestampInputStatic: TIMESTAMP
		}
	}
	
	@Memoizable
	def Iterable<DirectDataFlow> getDirectDataFlows(Process process) {
		(process.getModelElements(PrimitiveDirectDataFlow) + process.getModelElements(ComplexDirectDataFlow)).toSet
	}
	
	@Memoizable 
	def Iterable<InputPort> getModelBranchOutputsInUse(EndSIB endSIB) {
		endSIB.inputPorts.filter[dataEdge !== null]
	}
	
	def getMajorBranch(SIB it) {
		switch it {
			GUISIB: majorBranch
			GUIBlueprintSIB: majorBranch
			GenericSIB: guiOptions.majorBranch
		}
	}
	
	def isOfType(Process process, ProcessType type) {
		process?.processType == type
	}
	
	def getPrimitiveType(Node it) {
		switch it {
			PrimitiveAttribute: attribute.dataType
			PrimitiveListAttribute: DATA_PRIMITIVE_TYPE.get(it.primitiveType)
			PrimitiveVariable: DATA_PRIMITIVE_TYPE.get(it.dataType)
			PrimitiveInputPort: DATA_PRIMITIVE_TYPE.get(it.dataType)
			PrimitiveOutputPort: DATA_PRIMITIVE_TYPE.get(it.dataType)
			InputStatic: getPrimitiveType(it as InputStatic)
			info.scce.dime.process.process.PrimitiveExtensionAttribute: (it as info.scce.dime.process.process.PrimitiveExtensionAttribute).attribute.dataType.primitiveType
			default: throw new SwitchException
		}
		
	}
	
	def getPrimitiveType(InputStatic input) {
		switch input {
			BooleanInputStatic: info.scce.dime.data.data.PrimitiveType::BOOLEAN
			TextInputStatic: info.scce.dime.data.data.PrimitiveType::TEXT
			IntegerInputStatic: info.scce.dime.data.data.PrimitiveType::INTEGER
			RealInputStatic: info.scce.dime.data.data.PrimitiveType::REAL
			TimestampInputStatic: info.scce.dime.data.data.PrimitiveType::TIMESTAMP
			default: throw new SwitchException
		}
	}
	
	def getPrimitiveType(String input) {
		switch input {
			case "Boolean": info.scce.dime.data.data.PrimitiveType::BOOLEAN
			case "Text": info.scce.dime.data.data.PrimitiveType::TEXT
			case "Integer": info.scce.dime.data.data.PrimitiveType::INTEGER
			case "Real": info.scce.dime.data.data.PrimitiveType::REAL
			case "Timestamp": info.scce.dime.data.data.PrimitiveType::TIMESTAMP
			case "File": info.scce.dime.data.data.PrimitiveType::FILE
			default: throw new SwitchException
		}
	}
	
	def Type getComplexType(Node it) {
		switch it {
			ComplexAttribute: 
				if(attribute!==null){
					attribute.dataType
				}
			ComplexListAttribute: listType
			ComplexVariable: dataType
			ComplexInputPort: dataType
			ComplexOutputPort: dataType
			ComplexExtensionAttribute: attribute.complexExtensionAttributeType
		default: throw new SwitchException }
	}
	
	def getNativeType(Node it) {
		switch it {
			JavaNativeVariable: dataType
			JavaNativeInputPort: dataType
			JavaNativeOutputPort: dataType
		default: throw new SwitchException }
		as info.scce.dime.siblibrary.Type
	}
	
	def isListType(Node it) {
		switch it {
			InputPort: isList
			OutputPort: isList
			Variable: isList
			PrimitiveAttribute: 
				if(attribute !== null){
					attribute.isList
				}
			ComplexAttribute: 
				if(attribute !== null){
					attribute.isList
				}
			PrimitiveListAttribute: false
			ComplexListAttribute: false
			InputStatic: false
			ExtensionAttribute:attribute.isIsList
		default: throw new SwitchException }
	}
	
	def isMajor(SIB it) {
		switch it {
			GUISIB: isMajorPage
			GUIBlueprintSIB: isMajorPage
			default: false
		}
	}
	
	def isMinor(SIB it) {
		switch it {
			GUISIB: !isMajorPage
			GUIBlueprintSIB: !isMajorPage
			GenericSIB : {
				if (guiOptions !== null) {
					!guiOptions.majorPage
				}
				false
			}
			default: false
		}
	}
	
	def hasDefaultContent(SIB sib, SIB contentSib) {
		switch it:sib {
			GUISIB: defaultContent == contentSib
			GUIBlueprintSIB: defaultContent == contentSib
		}
	}
	
	def setDefaultContent(SIB sib, SIB contentSib) {
		switch it:sib {
			GUISIB: defaultContent = contentSib
			GUIBlueprintSIB: defaultContent = contentSib
			default: throw new IllegalArgumentException("Failed to set default content for " + sib?.class)
		}
	}
	
	def hasMajorBranch(SIB sib, AbstractBranch branch) {
		switch it:sib {
			GUISIB: majorBranch == branch
			GUIBlueprintSIB: majorBranch == branch
		}
	}
	
	def setMajorBranch(SIB sib, AbstractBranch branch) {
		switch it:sib {
			GUISIB: majorBranch = branch
			GUIBlueprintSIB: majorBranch = branch
			default: throw new IllegalArgumentException("Failed to set major branch for " + sib?.class)
		}
	}
	
	static class SwitchException extends IllegalStateException {
		
		new() {
			super("Default case in exhaustive switch; implementation broken?")
		}
		
		new(Object obj) {
			super("Default case in exhaustive switch for " + obj?.class?.simpleName + "; implementation broken?")
		}
	}
}
