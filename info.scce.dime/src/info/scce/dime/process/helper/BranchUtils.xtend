/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.helper

import static extension info.scce.dime.process.build.PrimeSIBBuild.getBuild

import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.SIB
import org.eclipse.emf.ecore.EObject
import java.util.Map.Entry
import java.util.List

class BranchUtils {
	
	static def getReferencedComponent(Branch branch) {
		val sib = branch.getPredecessors(SIB)?.head
		val build = sib.build
		if (build != null)
			((build.getBranchReferences(sib)
				.filter[build.isBranchReference(branch,it)]
				.head as Entry)?.value as List)?.head as EObject
		// TODO cast to EObject should not be necessary, fix the PrimeSIBBuild
	}
	
	static def getBranchReference(SIB sib, Branch branch) {
		val build = sib.build
		if (build != null)
			build.getBranchReferences(sib)
				.filter[build.isBranchReference(branch,it)].head
	}
}
