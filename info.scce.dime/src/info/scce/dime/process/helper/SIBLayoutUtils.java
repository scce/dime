/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.helper;

import static info.scce.dime.process.helper.LayoutConstants.BRANCH_FIRST_PORT_Y;
import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_X;
import static info.scce.dime.process.helper.LayoutConstants.SIB_FIRST_PORT_Y;

import graphmodel.Container;
import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.process.process.AbstractBranch;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.EventListener;
import info.scce.dime.process.process.IO;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.SIB;
import info.scce.dime.process.process.StartSIB;

public class SIBLayoutUtils {
	
	private static void resize(Node node, int width, int height) {
		
		node.resize(width, height);
		
//		if (node instanceof CSIB) {
//			((CSIB)node).resize(width, height);
//		}
//		else if (node instanceof CStartSIB) {
//			((CStartSIB)node).resize(width, height);
//		}
//		else if (node instanceof CEndSIB) {
//			((CEndSIB)node).resize(width, height);
//		}
//		else if (node instanceof CAbstractBranch) {
//			((CAbstractBranch)node).resize(width, height);
//		}
//		else if (node instanceof CVariable) {
//			((CVariable)node).resize(width, height);
//		}
//		else if (node instanceof CIO) {
//			((CIO)node).resize(width, height);
//		}
//		else if (node instanceof CEventListener) {
//			((CEventListener)node).resize(width, height);
//		}
	}
	
	private static void moveTo(Node node, Container container, int x, int y) {
		node.moveTo(container, x, y);
//		if (node instanceof CInput && container instanceof CDataFlowTarget) {
//			((CInput)node).moveTo((CDataFlowTarget) container, x, y);
//		}
//		else if (node instanceof CInput && container instanceof CEventListener) {
//			((CInput)node).moveTo((CEventListener) container, x, y);
//		}
//		else if (node instanceof COutput && container instanceof CDataFlowSource) {
//			((COutput)node).moveTo((CDataFlowSource) container, x, y);
//		}
//		else if (node instanceof CTypeInput && container instanceof CDataFlowTarget) {
//			((CTypeInput)node).moveTo((CDataFlowTarget) container, x, y);
//		}
//		else if (node instanceof CTypeOutput && container instanceof CStartSIB) {
//			((CTypeOutput)node).moveTo((CStartSIB) container, x, y);
//		}
	}
	
	
	@SafeVarargs
	private static void resizeAndLayoutBeforeDelete(Container container, Node doomedNode, int xMargin, int initialY, int ySpace, Class<? extends Node> ... layoutedTypes) {
		int y = initialY;
		
		int ioAmount = 0;
		for (Class<? extends Node> layoutedType : layoutedTypes) {
			ioAmount += container.getModelElements(layoutedType).size();
		}
		
		ioAmount -= (doomedNode == null ? 0 : 1); 
		
		resize(container, container.getWidth(), NodeLayout.getHeight(ioAmount, container.getClass()));
		
		for (Class<? extends Node> layoutedType : layoutedTypes) {
			for (Node node: container.getModelElements(layoutedType)) {
				if (node != doomedNode) {
					moveTo(node, container, xMargin, y);
					resize(node, container.getWidth()-2*xMargin, node.getHeight());
					y += ySpace;
				}
			}
		}
	}
	

	/**
	 *
	 * convenience wrapper for {@link #resizeAndLayoutBeforeDelete(CModelElementContainer, CIO)} with
	 * doomedInputOutput = null
	 * 
	 */
	public static void resizeAndLayout(ModelElementContainer container) {
		resizeAndLayoutBeforeDelete(container, null);
	}

	/**
	 * 
	 * Resizes container's height so that all contained elements (minus the one
	 * that is about to be deleted) fit, positions them within the containerand
	 * resizes their width according to container's width. 
	 * 
	 * This only works for a predefined set of containers:
	 * 	{@link CStartSIB}, {@link CEndSIB}, {@link CSIB}, {@link CAbstractBranch} 
	 * 
	 * @param container the layouted container
	 * @param doomedInputOutput The data port that is about to be deleted. Will be ignored for layouting. 
	 * 		Can be null if nothing is going to be deleted
	 */
	public static void resizeAndLayoutBeforeDelete(ModelElementContainer container, IO doomedInputOutput) {
		
		
		if (container instanceof StartSIB) {
			resizeAndLayoutBeforeDelete((StartSIB)container, doomedInputOutput);
		}
		else if (container instanceof EndSIB) {
			resizeAndLayoutBeforeDelete((EndSIB)container, doomedInputOutput);
		}
		else if (container instanceof SIB) {
			resizeAndLayoutBeforeDelete((SIB)container, doomedInputOutput);
		}
		else if (container instanceof EventListener) {
			resizeAndLayoutBeforeDelete((EventListener)container, doomedInputOutput);
		}
		else if (container instanceof AbstractBranch) {
			resizeAndLayoutBeforeDelete((AbstractBranch)container, doomedInputOutput);
		}
		
	}

	private static void resizeAndLayoutBeforeDelete(StartSIB startSIB, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(startSIB, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, Output.class);
	}

	private static void resizeAndLayoutBeforeDelete(EndSIB endSIB, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(endSIB, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, IO.class);
	}

	private static void resizeAndLayoutBeforeDelete(SIB cSib, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(cSib, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, Input.class);
	}
	
	private static void resizeAndLayoutBeforeDelete(AbstractBranch cBranch, IO doomedOutput) {
		resizeAndLayoutBeforeDelete(cBranch, doomedOutput, PORT_X, BRANCH_FIRST_PORT_Y, PORT_SPACE, Output.class);
	}
	
	private static void resizeAndLayoutBeforeDelete(EventListener cBranch, IO doomedOutput) {
		resizeAndLayoutBeforeDelete(cBranch, doomedOutput, PORT_X, BRANCH_FIRST_PORT_Y, PORT_SPACE, Input.class);
	}
	
	

}
