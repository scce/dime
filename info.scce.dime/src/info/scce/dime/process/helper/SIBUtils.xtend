/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.helper

import info.scce.dime.process.process.AtomicSIB
import info.scce.dime.process.process.ContainsSIB
import info.scce.dime.process.process.CreateSIB
import info.scce.dime.process.process.CreateUserSIB
import info.scce.dime.process.process.EnumSwitchSIB
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.GuardProcessSIB
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.IterateSIB
import info.scce.dime.process.process.NativeFrontendSIBReference
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.PutComplexToContextSIB
import info.scce.dime.process.process.RetrieveEnumLiteralSIB
import info.scce.dime.process.process.RetrieveOfTypeSIB
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.SetAttributeValueSIB
import info.scce.dime.process.process.TransientCreateSIB
import info.scce.dime.process.process.UnsetAttributeValueSIB
import info.scce.dime.process.process.EntryPointProcessSIB
import info.scce.dime.process.process.LinkProcessSIB
import info.scce.dime.dad.dad.URLProcess
import info.scce.dime.dad.dad.ProcessComponent
import info.scce.dime.dad.dad.ProcessEntryPointComponent
import info.scce.dime.process.process.GenericSIB

class SIBUtils {
	
	static def model(URLProcess it) {
		switch it {
			ProcessComponent: model
			ProcessEntryPointComponent: entryPoint.rootElement
		}
	}
	
	static def getReferencedComponent(SIB it) {
		switch it {
			AtomicSIB: sib
			EntryPointProcessSIB: proMod
			LinkProcessSIB: (proMod as URLProcess).model
			ProcessSIB: proMod
			GuardedProcessSIB: proMod
			GuardProcessSIB: securityProcess
			CreateSIB: createdType
			TransientCreateSIB: createdType
			CreateUserSIB: createdType
			SetAttributeValueSIB: attribute
			UnsetAttributeValueSIB: attribute
			ContainsSIB: listType
			IterateSIB: iteratedType
			PutComplexToContextSIB: putType
			RetrieveOfTypeSIB: retrievedType
			EnumSwitchSIB: switchedType
			RetrieveEnumLiteralSIB: retrievedLiteral
			NativeFrontendSIBReference : referencedSib
			GUISIB: gui
			GenericSIB: referencedObject
		}
	}
}
