/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.helper;

import static info.scce.dime.process.helper.LayoutConstants.ATTR_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_SPACE;

import graphmodel.Container;
import info.scce.dime.process.process.AbstractBranch;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.EventListener;
import info.scce.dime.process.process.SIB;
import info.scce.dime.process.process.StartSIB;
import info.scce.dime.process.process.Variable;

public class NodeLayout {
	
	public static int getSIBHeight (int portAmount) {
		return (65 + portAmount*PORT_SPACE + (portAmount > 0 ? 7 : 0));
	}

	public static int getBranchHeight (int portAmount) {
		return (25 + portAmount*PORT_SPACE + (portAmount > 0 ? 7 : 0));
	}
	
	public static int getVariableHeight (int attrAmount) {
		return (25 + attrAmount*VAR_ATTR_SPACE + (attrAmount> 0 ? -1 : 0));
	}

	public static int getTypeHeight (int attrAmount) {
		return (30 + attrAmount*ATTR_SPACE + (attrAmount > 0 ? 7 : 0));
	}
	
	
	public static int getHeight (int amount, Class<? extends Container> containerType) {
		if (SIB.class.isAssignableFrom(containerType)) {
			return getSIBHeight(amount);
		}
		else if (StartSIB.class.isAssignableFrom(containerType)) {
			return getSIBHeight(amount);
		}
		else if (EndSIB.class.isAssignableFrom(containerType)) {
			return getSIBHeight(amount);
		}
		else if (AbstractBranch.class.isAssignableFrom(containerType)) {
			return getBranchHeight(amount);
		}
		else if (Variable.class.isAssignableFrom(containerType)) {
			return getVariableHeight(amount);
		}
		else if (EventListener.class.isAssignableFrom(containerType)) {
			return getVariableHeight(amount);
		}
		else {
			// should not happen...
			throw new RuntimeException(
					String.format(
							"We can't calculate the height for \"%s\"",
							containerType.getClass()
					)
			);
		}
	}

}
