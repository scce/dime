/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.helper;

public class LayoutConstants {

	/*
	 *  CONSTANTS FOR PROCESS MODEL LAYOUT
	 */
	
	// how far ports are inserted from the left side
	//public static final int PORT_PADDING_LEFT = 8;
	
	// how far ports are inserted from the bottom (10 is port height)
	//public static final int PORT_PADDING_BOTTOM = 10 + PORT_PADDING_LEFT;
	
	public static final int SIB_WIDTH = 120;
	
	public static final int SIB_FIRST_PORT_Y = 67;
	public static final int BRANCH_FIRST_PORT_Y = 27;

	public static final int PORT_X = 5;

	// distance between ports (10 is port width)
	public static final int PORT_SPACE = 18;
	
	public static final int BRANCH_WIDTH = 120;
	
	// how far from the SIB the branches are inserted 
	public static final int BRANCH_H_DISTANCE = 70;
	public static final int BRANCH_V_DISTANCE = 10;
	public static final int BRANCH_H_SPACE = 40;
	
	// the number of branches to be centrally aligned with the SIB
	public static final int BRANCH_THRESHOLD = 4;
	
	/*
	 * PROCESS MODEL DATA CONTEXT
	 */

	public static final int VAR_ATTR_X = 10;
	public static final int VAR_FIRST_ATTR_Y = 23;
	public static final int VAR_ATTR_SPACE = 27;
	
	
	/*
	 *  CONSTANTS FOR DATA MODEL LAYOUT
	 */
	
	public static final int ATTR_X = 5;
	public static final int ATTR_SPACE = 18;
	public static final int TYPE_FIRST_ATTR_Y = 32;
	

}
