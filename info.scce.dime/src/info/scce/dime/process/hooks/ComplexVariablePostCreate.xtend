/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import graphmodel.Node
import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.process.process.ComplexVariable

import static java.lang.Math.min
import static java.lang.Math.max
import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_X
import info.scce.dime.process.process.DataContext

class ComplexVariablePostCreate extends DIMEPostCreateHook<ComplexVariable> {
	val OFFSET = 30
	override postCreate(ComplexVariable it) {
		val containerWidth = (container as Node).width
		
		// width should not be smaller than 10 
		width = max(containerWidth - 2 * VAR_ATTR_X, 10)
		
		// offset should not move it outside the container
		x = min(VAR_ATTR_X, max((containerWidth - width) / 2, 0))
		
		// name needs to be set after resize to have the label resized, too
		name = dataType.name.toLowerCase
		
		if(it.container instanceof DataContext){
			var dataContext = it.container as DataContext
			var maxHeight = maxHeight(dataContext);
			dataContext.setHeight( maxHeight + OFFSET);
			
		}
	}
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
}
