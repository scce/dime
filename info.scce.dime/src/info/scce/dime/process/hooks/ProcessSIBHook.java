/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import static info.scce.dime.process.process.ProcessType.ASYNCHRONOUS;
import static info.scce.dime.process.process.ProcessType.BASIC;
import static info.scce.dime.process.process.ProcessType.FILE_DOWNLOAD_SECURITY;
import static info.scce.dime.process.process.ProcessType.LONG_RUNNING;
import static info.scce.dime.process.process.ProcessType.NATIVE_FRONTEND_SIB_LIBRARY;
import static info.scce.dime.process.process.ProcessType.SECURITY;
import static info.scce.dime.process.process.ProcessType.UNSPECIFIED;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import info.scce.dime.process.build.ProcessSIBBuild;
import info.scce.dime.process.helper.NodeLayout;
import info.scce.dime.process.process.GuardContainer;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessSIB;
import info.scce.dime.process.process.ProcessType;
import info.scce.dime.process.process.SIB;
import info.scce.dime.process.process.StartSIB;

public class ProcessSIBHook extends AbstractPostCreateSIBHook<ProcessSIB>{

	static final HashMap<ProcessType,List<ProcessType>> TYPES = new HashMap<>();
	static {
		TYPES.put(BASIC,                  Arrays.asList(BASIC,               SECURITY, LONG_RUNNING, ASYNCHRONOUS));
		TYPES.put(SECURITY,               Arrays.asList(BASIC,               SECURITY, LONG_RUNNING              ));
		TYPES.put(FILE_DOWNLOAD_SECURITY, Arrays.asList(BASIC,               SECURITY, LONG_RUNNING              ));
		TYPES.put(LONG_RUNNING,           Arrays.asList(BASIC,				SECURITY, LONG_RUNNING, ASYNCHRONOUS));
		TYPES.put(ASYNCHRONOUS,           Arrays.asList(BASIC,               SECURITY, LONG_RUNNING, ASYNCHRONOUS));
		TYPES.put(NATIVE_FRONTEND_SIB_LIBRARY,  Lists.newArrayList());
		TYPES.put(UNSPECIFIED,  Lists.newArrayList());

	}
	
	@Override
	void preProcess(ProcessSIB sib) {
		super.preProcess(sib);
		
		ProcessType lcProcessType = sib.getProMod().getProcessType();
		ProcessType thisProcessType = sib.getRootElement().getProcessType();

		if (thisProcessType == UNSPECIFIED) {
			sib.delete();
			stop(String.format(
				"Failed to insert sub process. Initialize process type first.",
				lcProcessType, thisProcessType));
		}
		
		List<ProcessType> allowed = TYPES.get(thisProcessType);
		if (allowed == null) {
			throw new IllegalStateException(
				"default case in exhaustive switch; implementation broken?");
		}
		if (!allowed.contains(lcProcessType)) {
			sib.delete();
			stop(String.format(
				"Inserting a %s process into this %s process is not allowed.",
				lcProcessType, thisProcessType));
		}
		
		// special case: initialize guardContainer w/ GuardedProcessSIB
		if (thisProcessType == LONG_RUNNING && lcProcessType == BASIC) {
			initializeGuard(sib);
			sib.delete();
			stop(null);
		}
	}
	
	@Override
	boolean initialize(ProcessSIB sib) {
			
		Process primeModel = sib.getProMod();

		setLabel(primeModel.getModelName());
		

		return ProcessSIBBuild.initialize(sib);
	}

	private void initializeGuard(SIB cSib) {
		Process process = cSib.getRootElement();
		Process lcProcess = ((ProcessSIB) cSib).getProMod();
		GuardContainer guardContainer = process.newGuardContainer(
				cSib.getX(), cSib.getY(),
				cSib.getWidth()+20,
				NodeLayout.getSIBHeight(lcProcess.getStartSIBs().stream().map(StartSIB::getOutputs).flatMap(List::stream).collect(Collectors.toList()).size()) + 20
				//NodeLayout.getSIBHeight(lcProcess.getStartSIBs().stream().flatMap(start -> start.getOutputs().stream()).collect(Collectors.counting())) + 20
				);
		guardContainer.newGuardedProcessSIB(lcProcess, 10, 10);
	}

}
