/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.EdgeLayoutUtils.getAbsoluteLocation;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getBendpoints;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getConnection;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getDistance;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getMovableEdges;
import static info.scce.dime.process.helper.EdgeLayoutUtils.moveBendpoint;

import java.util.List;

import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement;
import graphmodel.Edge;
import graphmodel.ModelElementContainer;
import info.scce.dime.api.DIMEPostMoveHook;
import info.scce.dime.process.helper.EdgeLayoutUtils.Location;
import info.scce.dime.process.process.EventListener;
import info.scce.dime.process.process.Input;

public class EventListenerDataFlowSourcePostMove extends DIMEPostMoveHook<EventListener> {

	@Override
	public void postMove(EventListener dfTarget, ModelElementContainer sourceContainer,
			ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		
		postProcessEdges(dfTarget, deltaX, deltaY);
	}
	
	
	void postProcessEdges(EventListener dfTarget, int deltaX, int deltaY) {
		// find out which edges have already been moved completely
		List<FreeFormConnection> movables = getMovableEdges((CModelElement)dfTarget);
		
		for (Input input : dfTarget.getInputs()) {
			Location absLoc = getAbsoluteLocation(input);
			Location oldLoc = new Location(absLoc.x + 6 - deltaX, absLoc.y + 6 - deltaY);
			for (Edge edge : input.getIncoming()) {
				if (!movables.contains(getConnection(edge))) {
					List<Point> points = getBendpoints(edge);
					for (int i = 0; i < points.size(); i++) {
						Point point = points.get(i);
						if (getDistance(oldLoc, Location.from(point)) < 60) {
							moveBendpoint(edge, i, point.getX() + deltaX, point.getY() + deltaY);
						}
					}
				}
			}
		}
	}

}
