/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.data.data.InheritorType
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.process.process.CreateSIB
import info.scce.dime.process.process.CreateUserSIB
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.RetrieveOfTypeSIB
import info.scce.dime.process.process.TransientCreateSIB
import graphmodel.IdentifiableElement


import static info.scce.dime.process.process.PrimitiveType.*
import info.scce.dime.process.process.SIB
import info.scce.dime.data.data.Inheritance
import java.util.Set


abstract class AbstractAttributePortHook<T extends IdentifiableElement> extends DIMEPostCreateHook<T> {
	def getInputs(SIB sib) { (sib as DataFlowTarget).inputs }
	
	def getType(SIB sib) {
		switch(it: sib) {
			CreateSIB: createdType
			TransientCreateSIB: createdType
			CreateUserSIB: createdType 
			RetrieveOfTypeSIB: retrievedType
		}
	}
	
	def getTypeHierarchy(SIB sib) {
		val type = sib.type
		if (type != null)
			#[type] + type.superTypes
	}
	
	def Iterable<Type> getSuperTypes(Type type, Set<Type> collection) {
		switch it:type {
			InheritorType: findTargetsOf(Inheritance).filter(Type).map[#[it] + superTypes].flatten
			default: #[]
		}
	}
	
	def isTypeSIB(SIB sib) {
		switch(it: sib) {
			CreateSIB,
			TransientCreateSIB,
			CreateUserSIB, 
			RetrieveOfTypeSIB: true
			default: false
		}
	}
	
	def toProcessPrimitive(PrimitiveType type) {
		switch (type) {
			case BOOLEAN: BOOLEAN
			case INTEGER: INTEGER
			case REAL: REAL
			case TEXT: TEXT
			case TIMESTAMP: TIMESTAMP
			case FILE: FILE
			default: throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.")
		}
	}
	
	def showError(String title, String body) {
		showErrorDialog(title,body)
	}
}
