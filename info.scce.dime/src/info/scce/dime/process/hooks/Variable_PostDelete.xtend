/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import info.scce.dime.process.process.Variable
import info.scce.dime.process.process.DataContext
import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook

class Variable_PostDelete extends CincoPostDeleteHook<Variable>{
		val OFFSET = 30
	
	def layout(DataContext dataContext) {
		var maxHeight = maxHeight(dataContext);
		dataContext.setHeight(maxHeight + OFFSET);
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
	override getPostDeleteFunction(Variable node) {
		if(node.container instanceof DataContext){
			val dataContext  = node.container as DataContext
			return [dataContext.layout]
		}
	}
	
}
