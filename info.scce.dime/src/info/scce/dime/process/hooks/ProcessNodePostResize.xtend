/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.Direction
import graphmodel.ModelElement
import graphmodel.Node
import info.scce.dime.api.DIMEPostResizeHook
import info.scce.dime.process.process.DataContext

import static java.lang.Math.abs
import static java.lang.Math.max

class ProcessNodePostResize<E extends ModelElement> extends DIMEPostResizeHook<E> {
	
	override postResize(E node, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction) {
		if (node instanceof Node) {
			updateDataContext(node as Node, oldHeight - node.height)
		}
	}
	
	def updateDataContext(Node resizedNode, int deltaHeight) {
		if (new WorkbenchExtension().getDisplay() === null) {
			return // only resize if triggered by changes in the UI editor
		}
		val model = resizedNode.rootElement
		val nodes = model.allNodes.drop(DataContext)
		if (!nodes.nullOrEmpty) {
			val botNode = nodes.sortBy[y + height].reverse.head
			val scndBotNode = nodes.filter[it != resizedNode].sortBy[y + height].reverse.head ?: botNode
			val oldMax = max(resizedNode.y + resizedNode.height - deltaHeight, scndBotNode.y + scndBotNode.height)
			for (dataContext : model.getNodes(DataContext)) {
				val oldDataContextBot = dataContext.y + dataContext.height
				val minHeight = (dataContext.allNodes.map[y + height].sort.reverse.head ?: 0) + 17
				if (abs(oldMax - oldDataContextBot) < 2 || dataContext.height == minHeight) {
					// data context has been aligned to the bottom of the bottom node => resize
					val dY = botNode.y + botNode.height - dataContext.y - dataContext.height
					if (dY != 0) {
						println('''Resize data context height from «dataContext.height» to «botNode.y + botNode.height - dataContext.y» by dY=«dY»''')
						dataContext.height = max(minHeight, dataContext.height + dY)
					}
				}
			}
		}
	}
	
}
