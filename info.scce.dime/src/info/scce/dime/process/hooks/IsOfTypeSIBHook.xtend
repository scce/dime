/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import info.scce.dime.process.process.IsOfTypeSIB
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension

class IsOfTypeSIBHook extends AbstractPostCreateSIBHook<IsOfTypeSIB> {
	
	extension WorkbenchExtension = new WorkbenchExtension
	
	override package boolean initialize(IsOfTypeSIB sib) {
				
		val checkedType = sib.checkedType
		
		setLabel('''Is a «checkedType.name»''')

		val rootType = if(checkedType.rootTypes.size == 1) {
			checkedType.rootTypes.head
		}
		else {
			val rootTypes = checkedType.rootTypes.toList
			rootTypes.get(
				showCustomQuestionDialog(
					"Please Choose", 
					"Choose a root type please...", 
					rootTypes.map[name]
				)
			)
		}
		
		addComplexInputPort("instance", rootType, false)

		addBranch("yes")
		addComplexOutputPort("casted", checkedType, false)
		
		addBranch("no")
		
		true
	}
}
