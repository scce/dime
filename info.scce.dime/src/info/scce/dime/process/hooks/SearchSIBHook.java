/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import java.util.Arrays;

import info.scce.dime.process.process.SearchSIB;
import info.scce.dime.search.search.Search;
import info.scce.dime.search.search.SearchInterface;

public class SearchSIBHook extends AbstractPostCreateSIBHook<SearchSIB> {

	@Override
	boolean initialize(SearchSIB sib) {

		Search lcSearch = sib.getSearch();
		setLabel(lcSearch.getModelName());

		SearchInterface searchInterface = lcSearch.getSearchInterfaces().get(0);
		
		addInputPortsForReferences(searchInterface.getInputParameters());
		
		addBranch("found");
		addOutputPortsForReferences(Arrays.asList(searchInterface.getOutputParameters().get(0)));

		addBranch("not found");
		
		return true;
	}

}
