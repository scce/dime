/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import java.util.List;

import graphmodel.Node;
import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.process.process.AbstractBranch;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.DataContext;
import info.scce.dime.process.process.GUISIB;
import info.scce.dime.process.process.InputPort;
import info.scce.dime.process.process.JavaNativeVariable;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.process.process.SIB;

public class DeleteBranchesOnSIBDelete extends DIMEPreDeleteHook<SIB> {

	@Override
	public void preDelete(SIB sib) {
		List<AbstractBranch> branches = sib.getAbstractBranchSuccessors();
		branches.forEach(branch -> branch.delete());
		if(sib instanceof GUISIB){
			((GUISIB) sib).getEventListenerSuccessors().forEach(n->n.delete());
		}
		
	}

}
