/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.PortUtils.calcName;

import graphmodel.Node;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.process.helper.SIBLayoutUtils;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.OutputPort;

public class OutputPortHook extends DIMEPostCreateHook<OutputPort>{

	final static int OFFSET = 30;
	
	@Override
	public void postCreate(OutputPort port) {
		try {
			port.setName(calcName(port));
			SIBLayoutUtils.resizeAndLayout(port.getContainer());
     		if (port.getContainer() instanceof DataFlowSource) {
     			DataFlowSource sib = (DataFlowSource) port.getContainer();
     			layout(sib);
     		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void layout(DataFlowSource sib) {
		int newY = sib.getY() + sib.getHeight() + OFFSET;
		for (Node node : sib.getSuccessors()) {
			if(
					node.getY()<=(sib.getY()+sib.getHeight()) &&
					node.getY()>=sib.getY() &&
					node.getX()<=(sib.getX()+sib.getWidth()) &&
					node.getX()>=sib.getX()
			) {
				//branch is grown into the successor
				node.moveTo(sib.getRootElement(), node.getX(), newY);
			}
		}
	}
}
