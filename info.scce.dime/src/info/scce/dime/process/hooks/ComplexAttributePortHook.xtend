/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import info.scce.dime.data.data.Type
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.process.process.ComplexAttributePort
import info.scce.dime.process.process.SIB
import graphmodel.internal.InternalModelElement
import info.scce.dime.process.process.DataFlowTarget

class ComplexAttributePortHook extends AbstractAttributePortHook<ComplexAttributePort> {
	
	/**
	 * Replaces the {@link ComplexAttributePort} with a {@link ComplexInputPort}. 
	 */
	override postCreate(ComplexAttributePort port) {
		val attr = port.attribute
		val type = attr.container as Type
		val it = port.container as DataFlowTarget
		
		val ime = port.internalElement_ as InternalModelElement
		port.delete
		// FIXME re-setting the ime is a workaround for not causing an exception when deleting model elements in post-create hooks 
		port.internalElement_ = ime
		if(it instanceof SIB) {
			
			if (!isTypeSIB) {
				showError("Operation not Possible",
					'''You may drop an attribute only on data SIBs (like create, retrieve, ...) and not on a sib of type '«eClass.name»'.''')
				return
			}
			
			if (!typeHierarchy.exists[it == type]) {
				showError("Operation not Possible", 
					'''Inserting an attribute of type '«type?.name ?: "null"»' into a sib for type '«type.name»' is not allowed.''')
				return
			}
		}
		
		if (inputs.map[name].contains(attr.name)) {
			showError("Operation not Possible", 
				'''The sib already contains an attribute with name '«attr.name»'.''')
			return
		}
		
		newComplexInputPort(attr.dataType, 0, 0) => [
			name = attr.name
			isList = attr.isList
		]
	}
}
