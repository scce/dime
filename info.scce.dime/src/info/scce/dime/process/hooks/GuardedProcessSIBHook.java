/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import info.scce.dime.process.build.GuardedProcessSIBBuild;
import info.scce.dime.process.process.GuardedProcessSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

public class GuardedProcessSIBHook extends AbstractPostCreateSIBHook<GuardedProcessSIB>{

	@Override
	void preProcess(GuardedProcessSIB sib) {
		super.preProcess(sib);
		
		if (sib.getProMod().getProcessType() != ProcessType.BASIC) {
			sib.delete();
			stop("Only Basic processes can be guarded.");
		}
	}
	
	@Override
	public boolean initialize(GuardedProcessSIB sib) {
		
		Process primeModel = sib.getProMod();
		
		setLabel(primeModel.getModelName());
		
		return GuardedProcessSIBBuild.initialize(sib);
	}

}
