/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import graphmodel.GraphModel
import graphmodel.ModelElement
import graphmodel.Node
import info.scce.dime.api.DIMEPostDeleteHook
import info.scce.dime.process.process.DataContext

import static java.lang.Math.abs
import static java.lang.Math.max
import static java.lang.Math.min
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension

class ProcessNodePostDelete<E extends ModelElement> extends DIMEPostDeleteHook<E> {
	
	override getPostDeleteFunction(E node) {
		if (node instanceof Node) {
			val model = node.rootElement
			val delNodeY = node.y
			val delNodeHeight = node.height
			return [updateDataContext(model, delNodeY, delNodeHeight)]
		}
	}
	
	def updateDataContext(GraphModel model, int delNodeY, int delNodeHeight) {
		if (new WorkbenchExtension().getDisplay() === null) {
			return // only resize if triggered by changes in the UI editor
		}
		val nodes = model.allNodes.drop(DataContext)
		if (!nodes.nullOrEmpty) {
			val topNode = nodes.sortBy[y].head
			val oldMin = min(delNodeY, topNode.y)
			val botNode = nodes.sortBy[y + height].reverse.head
			val oldMax = max(delNodeY + delNodeHeight, botNode.y + botNode.height)
			for (dataContext : model.getNodes(DataContext)) {
				val oldDataContextBot = dataContext.y + dataContext.height
				if (abs(oldMin - dataContext.y) < 2) {
					// data context has been aligned to the y of the top node => resize
					val maxY = dataContext.y + (dataContext.allNodes.map[y].sort.head ?: 0) - 20
					val dY = min(maxY, topNode.y) - dataContext.y
					if (dY != 0) {
						println('''Resize data context y/height from «dataContext.y»/«dataContext.height» to «min(maxY, topNode.y)»/«(dataContext.height - dY)» by dY=«dY»''')
						dataContext.y = dataContext.y + dY
						for (innerNode : dataContext.allNodes) {
							innerNode.move(innerNode.x, max(20, innerNode.y - dY))
						}
						dataContext.height = dataContext.height - dY
					}
				}
				val minHeight = (dataContext.allNodes.map[y + height].sort.reverse.head ?: 0) + 17
				if (abs(oldMax - oldDataContextBot) < 2 || dataContext.height == minHeight) {
					// data context has been aligned to the bottom of the bottom node => resize
					val dY2 = botNode.y + botNode.height - dataContext.y - dataContext.height
					if (dY2 != 0) {
						println('''Resize data context height from «dataContext.height» to «botNode.y + botNode.height - dataContext.y» by dY=«dY2»''')
						dataContext.height = max(minHeight, dataContext.height + dY2)
					}
				}
			}
		}
	}
}
