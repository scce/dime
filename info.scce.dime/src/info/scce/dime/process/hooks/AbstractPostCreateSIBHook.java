/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.LayoutConstants.BRANCH_H_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.BRANCH_THRESHOLD;
import static info.scce.dime.process.helper.LayoutConstants.BRANCH_V_DISTANCE;
import static info.scce.dime.process.helper.LayoutConstants.BRANCH_WIDTH;
import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_X;
import static info.scce.dime.process.helper.LayoutConstants.SIB_FIRST_PORT_Y;
import static info.scce.dime.process.helper.LayoutConstants.SIB_WIDTH;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import info.scce.dime.data.data.Type;
import info.scce.dime.process.helper.PortUtils;
import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.BranchConnector;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.IO;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.JavaNativeInputPort;
import info.scce.dime.process.process.JavaNativeOutputPort;
import info.scce.dime.process.process.OutputPort;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.SIB;

//TODO convert to Xtend to use extension providers more elegantly
public abstract class AbstractPostCreateSIBHook<T extends SIB> extends ProcessNodePostCreate<T>{
	
	protected Process model;
	protected T sib;

	private int sibInputPortY;
	
	private Integer branchX;
	private Integer branchY;
	
	private int branchOutputPortY;
	
	private Branch currentBranch;
	
	@Override
	public final void postCreate(T sib) {
		super.postCreate(sib);
		init(sib);
		try {
			preProcess(sib);
			if (initialize(sib)) {
				postProcess(sib);
				finish();				
			}
		} catch(StopHookExecution e) {
			System.out.println("Exeecution of " + getClass().getSimpleName() + " stopped.");
		}
	}
	
	/**
	 * Any kind of pre-processing, e.g. to decide whether the creation of the SIB
	 * is allowed. If not, the execution of this hook should be stopped.
	 * <p>To stop the execution, call {@link #stop(String)}</p>
	 * <p>An error message can be shown via {@link #showError(String)}</p>
	 * <p>If necessary, the SIB can be deleted via {@link CSIB#delete() cSib.delete()}</p>
	 * <p> Phase: <b>Pre-Processing</b> -> Processing -> Post-Processing</p>
	 * 
	 * @param sib - the SIB to be processed.
	 * @param cSib - the C-API pendant of the SIB to be processed.
	 */
	void preProcess(T sib) throws StopHookExecution {
		
	}
	
	/**
	 * Do the actual work that is necessary to enrich the SIB.
	 * <p>
	 * This class provides some convenient mehtods to add Input Ports and
	 * Branches as well as Output Ports for the latter.
	 * </p>
	 * <p>To stop the execution, call {@link #stop(String)}</p>
	 * <p>An error message can be shown via {@link #showError(String)}</p>
	 * <p>If necessary, the SIB can be deleted via {@link CSIB#delete()}</p>
	 * <p>Phase: Pre-Processing -> <b>Processing</b> -> Post-Processing</p>
	 * 
	 * @see #addInputPortForReference(Object)
	 * @see #addBranch(String)
	 * @see #addOutputPortForReference(Object)
	 * 
	 * @param sib - the SIB to be processed.
	 * @param cSib - the C-API pendant of the SIB to be processed.
	 */
	abstract boolean initialize(T sib);
	
	/**
	 * Any kind of post-processing, e.g. to apply a custom layout.
	 * <p>To stop the execution, call {@link #stop(String)}</p>
	 * <p>An error message can be shown via {@link #showError(String)}</p>
	 * <p>If necessary, the SIB can be deleted via {@link CSIB#delete()}</p>
	 * <p> Phase: Pre-Processing -> Processing -> <b>Post-Processing</b></p>
	 * 
	 * @param sib - the SIB to be processed.
	 * @param cSib - the C-API pendant of the SIB to be processed.
	 */
	void postProcess(T sib) {
		// do nothing per default
	}
	
	/**
	 * finishes the sib creation. currently only sets the SIB width to 150.
	 */
	protected void finish() {
		sib.resize(SIB_WIDTH, sib.getHeight());
		
		int y = calcBranchY(sib);
		for (Branch cBranch : sib.getSuccessors(Branch.class)) {
			cBranch.setY(y);
		}
	}
	
	/**
	 * Convenience wrapper for {@link #init(SIB, String, int, int, boolean)} with resize=true
	 * @deprecated
	 */
	protected void init(SIB sib, String label, int inputPortAmount, int branchAmount, boolean resize) {
		init(sib, label, inputPortAmount, branchAmount, true);
	}
	
	/**
	 * Initializes the creation/autolayouting of the SIB.
	 * 
	 * @param sib The SIB that is created
	 */
	protected void init(T createdSib) {
		model = (Process) createdSib.getRootElement();
		sib = createdSib;
		sibInputPortY = SIB_FIRST_PORT_Y;
	}
	
	private int calcBranchX(SIB csib) {
		return csib.getX() + SIB_WIDTH / 2 - BRANCH_WIDTH / 2;
	}
	
	private int calcBranchY(SIB csib) {
		return csib.getY() + csib.getHeight() + BRANCH_V_DISTANCE;
	}
	
	protected void setLabel(String label) {
		sib.setLabel(label);
	}
	
	protected void showError(String message) {
		_workbenchExtension.showErrorDialog("Operation canceled", message);
	}
	
	protected void stop(String message) throws StopHookExecution {
		if (message != null)
			_workbenchExtension.showErrorDialog("Operation canceled", message);
		throw new StopHookExecution();
	}
	
	protected IO addInputPortForReference(Object portReference) {
		return PortUtils.addInput(sib, portReference);
	}
	
	protected IO addInputPortForReference(String name, Object portReference) {
		IO port = PortUtils.addInput(sib, portReference);
		if (port instanceof Input)
			((Input) port).setName(name);
		return port;
	}
	
	protected List<IO> addInputPortsForReferences(Iterable<?> portReferences) {
		List<IO> inputs = new ArrayList<>();
		for (Object ref : portReferences)
			inputs.add(addInputPortForReference(ref));
		return inputs;
	}
		
	protected Branch addBranch(String name) {
		if (branchX == null) {
			branchX = calcBranchX(sib);
		}
		if (branchY == null) {
			branchY = calcBranchY(sib);
		}
		EList<Branch> branches = sib.getSuccessors(Branch.class);
		int numBranches = branches.size();
		
		if (numBranches < BRANCH_THRESHOLD) {
			for (Branch branch : branches) {
				branch.setX(branch.getX() - BRANCH_WIDTH / 2 - BRANCH_H_SPACE / 2);
			}
		}
		
		currentBranch = model.newBranch(branchX, branchY);
		currentBranch.setName(name);
	    currentBranch.resize(BRANCH_WIDTH, currentBranch.getHeight());

	    	BranchConnector connector = sib.newBranchConnector(currentBranch);
	    	if (numBranches >= BRANCH_THRESHOLD) {
	    		connector.addBendpoint(
					currentBranch.getX() + currentBranch.getWidth() / 2,
					sib.getY() + sib.getHeight() / 2);
	    	}
	    	
	    	if ((numBranches + 1) < BRANCH_THRESHOLD) {
			branchX += BRANCH_WIDTH / 2 + BRANCH_H_SPACE / 2;
		}
		else {
	    		branchX += BRANCH_WIDTH + BRANCH_H_SPACE;
	    	}
	    	
	    	return currentBranch;
	}
	/**
	 * 
	 * <p>
	 * <b>Attention!</b> Output Ports are added to the branch that has last been added.
	 * In particular, it makes no sense to add Output Ports prior to adding any branch.
	 * 
	 * </p>
	 * 
	 * @see #addBranch(String)
	 */
	protected OutputPort addOutputPortForReference(Object portReference) {
		return new PortUtils().addOutput(currentBranch, portReference);
	}
	
	protected OutputPort addOutputPortForReference(String name, Object portReference) {
		OutputPort port = new PortUtils().addOutput(currentBranch, portReference);
		port.setName(name);
		return port;
	}
	
	protected List<OutputPort> addOutputPortsForReferences(Iterable<?> portReferences) {
		List<OutputPort> ports = new ArrayList<>();
		for (Object ref : portReferences)
			ports.add(addOutputPortForReference(ref));
		return ports;
	}
	
	protected void addPrimitiveOutputPort(String name, PrimitiveType type, boolean isList) {
		PrimitiveOutputPort outputPort = currentBranch.newPrimitiveOutputPort(PORT_X, branchOutputPortY);
		outputPort.setDataType(type);
		outputPort.setIsList(isList);
		outputPort.setName(name);
		branchOutputPortY += PORT_SPACE;
	}
	
	protected void addComplexOutputPort(String name, Type type, boolean isList) {
		ComplexOutputPort outputPort = currentBranch.newComplexOutputPort(type, PORT_X, branchOutputPortY);
		outputPort.setName(name);
		outputPort.setIsList(isList);
		branchOutputPortY += PORT_SPACE;
	}

	protected void addJavaNativeOutputPort(String name, info.scce.dime.siblibrary.Type type, boolean isList) {
		JavaNativeOutputPort outputPort = currentBranch.newJavaNativeOutputPort(type, PORT_X, branchOutputPortY);
		outputPort.setName(name);
		outputPort.setIsList(isList);
		branchOutputPortY += PORT_SPACE;
	}

	protected void addPrimitiveInputPort(String name, PrimitiveType type, boolean isList) {
		PrimitiveInputPort inputPort = sib.newPrimitiveInputPort(PORT_X, sibInputPortY);
		inputPort.setName(name);
		inputPort.setIsList(isList);
		inputPort.setDataType(type);
		sibInputPortY += PORT_SPACE;
	}

	protected void addComplexInputPort(String name, Type type, boolean isList) {
		ComplexInputPort inputPort = sib.newComplexInputPort(type, PORT_X, sibInputPortY);
		inputPort.setName(name);
		inputPort.setIsList(isList);
		sibInputPortY += PORT_SPACE;
	}

	protected void addJavaNativeInputPort(String name, info.scce.dime.siblibrary.Type type, boolean isList) {
		JavaNativeInputPort inputPort = sib.newJavaNativeInputPort(type, PORT_X, sibInputPortY);
		inputPort.setName(name);
		inputPort.setIsList(isList);
		sibInputPortY += PORT_SPACE;
	}
	
	protected void addPrimitiveOutputPort(String name, info.scce.dime.data.data.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveOutputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveOutputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveOutputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveOutputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveOutputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		case FILE:
			addPrimitiveOutputPort(name, PrimitiveType.FILE, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
	}

	protected void addPrimitiveOutputPort(String name, info.scce.dime.siblibrary.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveOutputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveOutputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveOutputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveOutputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveOutputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		case FILE:
			addPrimitiveOutputPort(name, PrimitiveType.FILE, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
		
	}

	protected void addPrimitiveOutputPort(String name, info.scce.dime.gui.gui.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveOutputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveOutputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveOutputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveOutputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveOutputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		case FILE:
			addPrimitiveOutputPort(name, PrimitiveType.FILE, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
	}

	protected void addPrimitiveOutputPort(String name, info.scce.dime.search.search.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveOutputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveOutputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveOutputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveOutputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveOutputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
		
	}
	
	protected void addPrimitiveInputPort(String name, info.scce.dime.data.data.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveInputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveInputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveInputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveInputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveInputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		case FILE:
			addPrimitiveInputPort(name, PrimitiveType.FILE, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
	}
	
	protected void addPrimitiveInputPort(String name, info.scce.dime.siblibrary.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveInputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveInputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveInputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveInputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveInputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		case FILE:
			addPrimitiveInputPort(name, PrimitiveType.FILE, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
	}

	protected void addPrimitiveInputPort(String name, info.scce.dime.gui.gui.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveInputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveInputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveInputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveInputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveInputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
	}

	protected void addPrimitiveInputPort(String name, info.scce.dime.search.search.PrimitiveType type, boolean isList) {
		switch (type) {
		case BOOLEAN:
			addPrimitiveInputPort(name, PrimitiveType.BOOLEAN, isList);
			break;
		case INTEGER:
			addPrimitiveInputPort(name, PrimitiveType.INTEGER, isList);
			break;
		case REAL:
			addPrimitiveInputPort(name, PrimitiveType.REAL, isList);
			break;
		case TEXT:
			addPrimitiveInputPort(name, PrimitiveType.TEXT, isList);
			break;
		case TIMESTAMP:
			addPrimitiveInputPort(name, PrimitiveType.TIMESTAMP, isList);
			break;
		default:
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
	}
	
	
	static class StopHookExecution extends RuntimeException {

		/* generated */
		private static final long serialVersionUID = 1320759743733279249L;
	}
}
