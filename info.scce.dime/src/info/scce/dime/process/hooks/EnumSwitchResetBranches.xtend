/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.dime.process.process.EnumSwitchSIB
import info.scce.dime.process.build.EnumSwitchSIBBuild
import java.util.ArrayList

class EnumSwitchResetBranches extends CincoCustomAction<EnumSwitchSIB>{
	
	
	override execute(EnumSwitchSIB sib) {
		sib.branchSuccessors.filter[name == "else"].head.delete
		val eBuilder = new EnumSwitchSIBBuild(sib)
		var missingBranches = findMissingBranches(sib)
		missingBranches.forEach[eBuilder.addBranches(it)]
	}
	
	def findMissingBranches(EnumSwitchSIB sib) {
		var missingBranches = new ArrayList
		for(literal : sib.switchedType.enumLiterals){
			if(!sib.branchSuccessors.exists[name == literal.name]){
				missingBranches.add(literal.name)
			}
		}
		return missingBranches
	}
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Reset branches"
	}
	
	
}
