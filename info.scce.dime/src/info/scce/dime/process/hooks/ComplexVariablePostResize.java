/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import graphmodel.Direction;
import info.scce.dime.api.DIMEPostResizeHook;
import info.scce.dime.process.helper.LayoutConstants;
import info.scce.dime.process.process.Attribute;
import info.scce.dime.process.process.ComplexVariable;

public class ComplexVariablePostResize extends DIMEPostResizeHook<ComplexVariable>{

	@Override
	public void postResize(ComplexVariable cVar, int oldWidth, int oldHeight, int oldX, int oldY,
			Direction direction) {
		for (Attribute a : cVar.getAttributes()) {
			a.resize(cVar.getWidth() - LayoutConstants.VAR_ATTR_X*2, a.getHeight());
		}
	}

}
