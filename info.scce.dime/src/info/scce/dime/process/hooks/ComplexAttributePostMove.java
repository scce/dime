/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_X;

import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.api.DIMEPostMoveHook;
import info.scce.dime.data.data.Type;
import info.scce.dime.process.helper.AttributeExpandUtils;
import info.scce.dime.process.process.ComplexAttribute;
import info.scce.dime.process.process.ComplexAttributeConnector;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.DataContext;

public class ComplexAttributePostMove extends DIMEPostMoveHook<ComplexAttribute> {
	final int OFFSET = 30;
	@Override
	public void postMove(ComplexAttribute attribute, ModelElementContainer sourceContainer, ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		
		if (sourceContainer.equals(targetContainer)) {
			// do nothing if Attribute moved within the same ComplexVariable

			// undo the move operation would be nice here, but if manually moved back 
			// to original position, postMove would be triggered again.
			// TODO: file feature request

		}
		else if (targetContainer instanceof DataContext){
			DataContext context = (DataContext) targetContainer;
			ComplexVariable originatingVariable = (ComplexVariable) sourceContainer;
			
			//FIXME: this should not be necessary. See #15472
			info.scce.dime.data.data.ComplexAttribute referencedAttr = 
					attribute.getAttribute();
			
			Type dataType = referencedAttr.getDataType();
			
			// TODO: correct positioning should be done by postCreate hook, but
			// moveTo methods currently missing in C-API.
			ComplexVariable newVar = context.newComplexVariable(dataType, VAR_ATTR_X, y);
			ComplexAttributeConnector connector = originatingVariable.newComplexAttributeConnector(newVar);
			connector.setAttributeName(referencedAttr.getName());
			
			newVar.setName(referencedAttr.getName());
			newVar.setIsList(referencedAttr.isIsList());
			newVar.resize(originatingVariable.getWidth(), originatingVariable.getHeight());
			
			AttributeExpandUtils.expand(newVar);
			attribute.delete();
			AttributeExpandUtils.resizeAndLayout(originatingVariable);
			layout(newVar);
		}
		else {
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
		
		
	}

	private void layout(ComplexVariable node) {
		if(node.getContainer() instanceof DataContext){
			DataContext dataContext = (DataContext)node.getContainer();
			int maxHeight = maxHeight(dataContext);
			dataContext.setHeight( maxHeight + OFFSET);
		}
		
	}
	

	private int maxHeight(DataContext context) {
		int maxLowerBound= 50;
		for(Node node : context.getNodes()){
			if(node.getY() + node.getHeight() > maxLowerBound){
				maxLowerBound = node.getY() + node.getHeight(); 
			}
		}
		return maxLowerBound;
	}

}
