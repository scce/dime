/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.hooks;

import java.util.Optional;

import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.UserAssociation;
import info.scce.dime.process.process.RetrieveCurrentUserSIB;

public class RetrieveCurrentUserSIBHook extends AbstractPostCreateSIBHook<RetrieveCurrentUserSIB>{

	@Override
	boolean initialize(RetrieveCurrentUserSIB sib) {
		
		setLabel("Retrieve Current User");
		
		addBranch("success");
		
		Optional<UserAssociation> userAssoc = 
				sib.getCurrentUser().getOutgoing(UserAssociation.class).stream().findFirst();
		if (userAssoc.isPresent() && userAssoc
				.map(assoc -> assoc.getTargetElement() instanceof ConcreteType
						&& ((ConcreteType) assoc.getTargetElement()).getIncoming(Inheritance.class).isEmpty())
				.orElse(false)) {
			
			addComplexOutputPort("currentUser", ((ConcreteType) userAssoc.get().getTargetElement()), false);
		}

		addBranch("not authenticated");
		
		return true;
	}

}
