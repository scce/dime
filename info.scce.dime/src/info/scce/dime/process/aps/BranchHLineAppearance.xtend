/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.aps

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.process.process.AbstractBranch
import style.StyleFactory

class BranchHLineAppearance implements StyleAppearanceProvider<AbstractBranch> {

	extension StyleFactory = StyleFactory.eINSTANCE

	val visible = createColor => [r=0 g=0 b=0]
	val invisible = createColor => [r=245 g=245 b=245]

	override getAppearance(AbstractBranch branch, String elementName) {
		if (elementName == "hline") {
			createAppearance => [
				foreground =
					if (!branch.rootElement.dataFlowView || branch.outputPorts.isEmpty) 
						invisible
					else
						HLineColor
			]
		}
	}

	def getHLineColor() {
		visible
	}
}
