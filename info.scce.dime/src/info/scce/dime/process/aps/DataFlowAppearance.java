/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.aps;

import java.util.Optional;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;
import graphmodel.ModelElement;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.process.process.ComplexAttribute;
import info.scce.dime.process.process.ComplexExtensionAttribute;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.PrimitiveAttribute;
import info.scce.dime.process.process.PrimitiveExtensionAttribute;
import info.scce.dime.process.process.Process;
import style.Appearance;
import style.BooleanEnum;
import style.Color;
import style.StyleFactory;

public class DataFlowAppearance implements StyleAppearanceProvider<ModelElement> {

	@Override
	public Appearance getAppearance(ModelElement e, String element) {
		
		
		Appearance appearance = StyleFactory.eINSTANCE.createAppearance();
		
		
		if(e instanceof ComplexExtensionAttribute || e instanceof PrimitiveExtensionAttribute) {
			Appearance input = StyleFactory.eINSTANCE.createAppearance();
			input.setFilled(BooleanEnum.TRUE);
			input.setLineInVisible(false);
			input.setLineWidth(1);
			input.setTransparency(0);
			Color cl1 = StyleFactory.eINSTANCE.createColor();
			cl1.setR(202);
			cl1.setG(202);
			cl1.setB(202);
			input.setBackground(cl1);
			return input;
		}
		
		if(e instanceof ComplexAttribute || e instanceof PrimitiveAttribute) {
			Appearance input = StyleFactory.eINSTANCE.createAppearance();
			input.setFilled(BooleanEnum.TRUE);
			input.setLineInVisible(false);
			input.setLineWidth(1);
			input.setTransparency(0);
			Color cl1 = StyleFactory.eINSTANCE.createColor();
			cl1.setR(210);
			cl1.setG(239);
			cl1.setB(235);
			input.setBackground(cl1);
			return input;
		}
		
		if(e instanceof ComplexVariable){
			//check if the variable is complex attribute
			ComplexVariable cv = (ComplexVariable) e;
			if(!cv.getIncomingComplexAttributeConnectors().isEmpty()) {
				//check if the variable corresponds to an extension attribute
				ComplexVariable parent = cv.getComplexVariablePredecessors().get(0);
				String attr = cv.getIncomingComplexAttributeConnectors().get(0).getAttributeName();
				Optional<Attribute> attrOpt = parent.getDataType().getAttributes().stream().filter(n->n.getName().equals(attr)).findAny();
				if(attrOpt.isPresent() && attrOpt.get() instanceof ExtensionAttribute) {
					//complex variable corresponds to an extension attribute
					Appearance input = StyleFactory.eINSTANCE.createAppearance();
					input.setFilled(BooleanEnum.TRUE);
					input.setLineInVisible(false);
					input.setLineWidth(1);
					input.setTransparency(0);
					Color cl1 = StyleFactory.eINSTANCE.createColor();
					cl1.setR(202);
					cl1.setG(202);
					cl1.setB(202);
					input.setBackground(cl1);
					return input;
				}
				//variable corresponds to an attribute
				Appearance input = StyleFactory.eINSTANCE.createAppearance();
				input.setFilled(BooleanEnum.TRUE);
				input.setLineInVisible(false);
				input.setLineWidth(1);
				input.setTransparency(0);
				Color cl1 = StyleFactory.eINSTANCE.createColor();
				cl1.setR(210);
				cl1.setG(239);
				cl1.setB(235);
				input.setBackground(cl1);
				return input;
				
			}
		}
		
		
		//getRootElement can be null during create
		Process processModel = (Process)e.getRootElement();
		boolean df = processModel != null ? processModel.isDataFlowView() : true;
		
		appearance.setTransparency(df ? 0.0 : 1.0);
		
		return appearance;
	}

}
	
	
