/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.aps

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter
import info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider
import info.scce.dime.process.process.GenericSIB
import org.eclipse.emf.ecore.EObject
import style.Appearance
import style.StyleFactory

import static info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider.*

class GenericSIBAppearance implements StyleAppearanceProvider<GenericSIB> {
	
	extension StyleFactory = StyleFactory.eINSTANCE
	
	override Appearance getAppearance(GenericSIB genericSIB, String attribute) {
		if (genericSIB.referencedObject !== null) {
			var IModeltrafoSupporter<EObject> trafoSupporter = getSupportedModelElement(genericSIB.referencedObject)
			if (trafoSupporter !== null) {
				val iconPath = trafoSupporter.iconPath
				if (!iconPath.nullOrEmpty) {				
					if (attribute.equals("icon")) {
						createAppearance => [
							imagePath = iconPath
						]
					}
				} else {
					if (GenericSIBGenerationProvider.isGUI(genericSIB.referencedObject)) {
						createAppearance => [
							imagePath = "icons/guiSIB.png"
						]
					}
				}
			}
		}
	}
}
