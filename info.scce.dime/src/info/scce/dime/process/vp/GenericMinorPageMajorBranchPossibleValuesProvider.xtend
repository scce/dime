/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.vp

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.GenericSIB
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.GUISIBOptions
import java.util.Map
import graphmodel.internal.InternalNode

class GenericMinorPageMajorBranchPossibleValuesProvider extends CincoValuesProvider<GUISIBOptions, AbstractBranch>{
	
	override Map<AbstractBranch, String> getPossibleValues(GUISIBOptions options) {
		val GenericSIB genericSIB = (options.eContainer as InternalNode).element as GenericSIB
		if (options.isMajorPage)
			#{} 
		else genericSIB.rootElement.find(AbstractBranch)
			.filter[ switch it:SIBPredecessors.head {
				GUISIB case isMajorPage && defaultContent !== null: true
				GUIBlueprintSIB case isMajorPage && defaultContent !== null: true
				GenericSIB case guiOptions !== null && guiOptions.isMajorPage && guiOptions.defaultContent !== null: true
				default: false
			}].toSet.toMap(
				[it], ['''«it.SIBPredecessors.head.label»: «it.name»'''.toString]
			)
	}
}
