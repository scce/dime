/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.vp

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import graphmodel.internal.InternalNode
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.helper.GUIExtensionProvider
import info.scce.dime.process.process.IgnoreBranch
import info.scce.dime.process.process.SIB
import java.util.Map

import static java.util.Comparator.comparing

class IgnoreBranchPossibleValuesProvider extends CincoValuesProvider<IgnoreBranch, String> {
	
	extension GUIExtension = GUIExtensionProvider.guiextension
	
	override Map<String, String> getPossibleValues(IgnoreBranch ib) {
		val sib = ((ib.eContainer as InternalNode).element as SIB)
		val ignorableBranches = <String,String> newTreeMap(comparing[it?.toLowerCase])
		sib.getIgnorableBranches(ignorableBranches)
		
		val ignoredBranches = sib.ignoredBranch.map[name].filterNull
		val usedBranches = sib.branchSuccessors.map[name].filterNull
		for (branch : ignoredBranches + usedBranches) {
			if (branch != ib.name) {
				ignorableBranches.remove(branch)
			}
		}
		
		return ignorableBranches
	}
}
