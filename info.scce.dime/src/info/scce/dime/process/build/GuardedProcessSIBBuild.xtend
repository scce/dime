/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import info.scce.dime.process.helper.PortUtils.Sync
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessFactory
import info.scce.dime.process.process.ProcessType
import java.util.LinkedList
import java.util.List

class GuardedProcessSIBBuild extends PrimeSIBBuild<
		GuardedProcessSIB, /* SIB type */
		               IO, /* SIB port reference */
		           EndSIB, /* Branch reference */
		            Input  /* Branch port reference */ > {
	
	static def initialize(GuardedProcessSIB sib) {
		new GuardedProcessSIBBuild(sib).initialize
	}
	
	static def update(GuardedProcessSIB sib) {
		new GuardedProcessSIBBuild(sib).update
	}
	
	new(GuardedProcessSIB sib) {
		super(sib)
	}
	
	override getSIBReference(GuardedProcessSIB sib) {
		sib.proMod
	}
	
	override isSIBReferenceValid(GuardedProcessSIB sib) {
		sib.container instanceof GuardContainer
		 	&& !sib.proMod.hasErrors
	}
	
	override getSIBPortReferences(GuardedProcessSIB sib) {
		val List<IO> outputs = new LinkedList
		// maxUsageCount is existing implicitly
		if(sib.rootElement.processType==ProcessType.LONG_RUNNING) {
			val maxUsage = ProcessFactory.eINSTANCE.createPrimitiveOutputPort => [
					dataType = PrimitiveType::INTEGER
					isList = false
					name = "maxUsageCount"
				]
				
			outputs += newArrayList(maxUsage)
		}
		val startSIB = sib.proMod.startSIB
		outputs.addAll(startSIB?.outputs ?: newArrayList)
		return outputs
	}
	
	override isSIBPortReference(Input sibPort, IO output) {
		if (output instanceof Output) {
			sibPort.name == output.name
		}
		else {
			false
		}
	}
	
	override getBranchReferences(GuardedProcessSIB sib) {
		sib.proMod.endSIBs
	}
	
	override isBranchReference(Branch branch, EndSIB endSib) {
		branch.name == endSib.branchName
	}
	
	override getBranchName(EndSIB endSib) {
		endSib.branchName
	}
	
	override getBranchPortReferences(EndSIB endSib) {
		endSib.inputs
	}
	
	override isBranchPortReference(Output branchPort, Input endSibPort) {
		branchPort.name == endSibPort.name
	}
	
	override initialize() {
		if(super.initialize()){
			branchCreator = new InitializationBranchCreator(cSib.container as GuardContainer)
			return true		
		}
		false
	}
	
	override update() {
		super.update()
		branchCreator = new UpdateBranchCreator(cSib.container as GuardContainer)
	}
	
	override package initializeBranchesInternal() {
		branchCreator = new InitializationBranchCreator(cSib.container as GuardContainer)
		super.initializeBranchesInternal
	}
	
	override updateBranches() {
		branchCreator = new UpdateBranchCreator(cSib.container as GuardContainer)
		val sync = new Sync<Branch,EndSIB> => [
			equals = [ branch,ref | branch.isBranchReference(ref) ]
			add    = [        ref | addAndUpdateBranch(ref) ]
			update = [ branch,ref | branch.update(ref) ]
			delete = [ branch     | branch.delete ]
		]
		val gc = sib.container as GuardContainer
		sync.apply(gc.getSuccessors(Branch), sib.branchReferences)
		
		// add implicit branch 'denied' if needed
		if (!gc.guardProcessSIBs.isEmpty && gc.rootElement.processType==ProcessType.BASIC) {
			addBranch("denied")
		}
	}
	
	/*
	 * override because
	 *  - SIB is inside guard container, not the process model
	 *  - branches are connected with the container, not the SIB
	 */
	override addBranch(String branchName) {
		if (cSib.container instanceof GuardContainer == false) {
			throw new IllegalStateException("unknown container of sib");
		}
		val guardCont = cSib.container as GuardContainer
		val container = guardCont.container
		if (container instanceof Process) {
			val newBranch = branchCreator.newBranch
			newBranch.setName(branchName)
			//fix: the branchCreator refers to the guarded SIB instead of the guard container
			newBranch.getIncoming(BranchConnector).get(0).reconnectSource(guardCont)
			return newBranch as Branch
		} else {
			throw new IllegalStateException("unknown container of branch");
		}
	}
	
}
