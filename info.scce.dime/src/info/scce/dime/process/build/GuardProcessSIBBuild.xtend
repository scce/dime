/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.GuardProcessSIB
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import java.util.List

class GuardProcessSIBBuild extends PrimeSIBBuild<
		GuardProcessSIB, /* SIB type */
		             IO, /* SIB port reference */
		         EndSIB, /* Branch reference */
		          Input  /* Branch port reference */ > {
	
	static def initialize(GuardProcessSIB sib) {
		new GuardProcessSIBBuild(sib).initialize
	}
	
	static def update(GuardProcessSIB sib) {
		new GuardProcessSIBBuild(sib).update
	}
	
	new(GuardProcessSIB sib) {
		super(sib)
	}
	
	override getSIBReference(GuardProcessSIB sib) {
		sib.securityProcess
	}
	
	override isSIBReferenceValid(GuardProcessSIB sib) {
		!sib.securityProcess.hasErrors
	}
	
	override getSIBPortReferences(GuardProcessSIB sib) {
		val List<IO> outputs = newArrayList
		val startSIB = sib.securityProcess.startSIB
		outputs.addAll(startSIB?.outputs?.filter[name != "currentUser"] ?: newArrayList)
		return outputs
	}
	
	override isSIBPortReference(Input port, IO output) {
		if (output instanceof Output) {
			port.name == output.name
		}
		else {
			false
		}
	}
	
	override getBranchReferences(GuardProcessSIB sib) {
		/* none */
	}
	
	override isBranchReference(Branch branch, EndSIB branchRef) {
		false // there are no branches
	}
	
	override getBranchName(EndSIB branchRef) {
		/* none */
	}
	
	override getBranchPortReferences(EndSIB branchRef) {
		/* none */
	}
	
	override isBranchPortReference(Output branchPort, Input portRef) {
		false // there are no branches
	}
	
}
