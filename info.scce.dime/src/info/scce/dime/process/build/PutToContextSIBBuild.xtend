/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PutToContextSIB

class PutToContextSIBBuild extends PrimeSIBBuild<
		PutToContextSIB, /* SIB type */
		          Input, /* SIB port reference */
		         String, /* Branch reference */
		          Input  /* Branch port reference */ > {
	
	static def initialize(PutToContextSIB sib) {
		new PutToContextSIBBuild(sib).initialize
	}
	
	static def update(PutToContextSIB sib) {
		new PutToContextSIBBuild(sib).update
	}
	
	new(PutToContextSIB sib) {
		super(sib)
	}
	
	override getSIBReference(PutToContextSIB sib) {
		sib
	}
	
	override isSIBReferenceValid(PutToContextSIB sib) {
		!sib.inputs.isEmpty
	}
	
	override getSIBPortReferences(PutToContextSIB sib) {
		sib.inputs // just keep the existing ones
	}
	
	override isSIBPortReference(Input port, Input portRef) {
		port == portRef
	}
	
	override getBranchReferences(PutToContextSIB sib) {
		newArrayList("success") // single static branch
	}
	
	override isBranchReference(Branch branch, String staticBranchName) {
		branch.name == staticBranchName
	}
	
	override getBranchName(String staticBranchName) {
		staticBranchName
	}
	
	override getBranchPortReferences(String branchRef) {
		sib.inputs
	}
	
	override isBranchPortReference(Output branchPort, Input sibPort) {
		branchPort.name == sibPort.name
	}
	
}
