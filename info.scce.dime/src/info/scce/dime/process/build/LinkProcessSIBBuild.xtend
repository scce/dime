/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import info.scce.dime.dad.dad.ProcessComponent
import info.scce.dime.dad.dad.ProcessEntryPointComponent
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.LinkProcessSIB
import info.scce.dime.process.process.Output
import java.util.Collections
import info.scce.dime.dad.dad.URLProcess

class LinkProcessSIBBuild extends PrimeSIBBuild<
		LinkProcessSIB, /* SIB type */
		               Output, /* SIB port reference */
		           EndSIB, /* Branch reference */
		            Input  /* Branch port reference */ > {
	
	static def initialize(LinkProcessSIB sib) {
		new LinkProcessSIBBuild(sib).initialize
	}
	
	static def update(LinkProcessSIB sib) {
		new LinkProcessSIBBuild(sib).update
	}
	
	new(LinkProcessSIB sib) {
		super(sib)
	}
	
	override getSIBReference(LinkProcessSIB sib) {
		sib.proMod
	}
	
	override isSIBReferenceValid(LinkProcessSIB sib) {
		if(sib.proMod instanceof URLProcess) {
			val URLProcess url = sib.proMod as URLProcess
			if(url instanceof ProcessComponent) {
				return !url.model.hasErrors
			}
			if(url instanceof ProcessEntryPointComponent) {
				return !url.entryPoint.proMod.hasErrors
			}
		}
		false
	}
	
	override getSIBPortReferences(LinkProcessSIB sib) {
		if(sib.proMod instanceof URLProcess) {
			val URLProcess url = sib.proMod as URLProcess
			if(url instanceof ProcessComponent) {
				return url.model.startSIB.outputs
			}
			if(url instanceof ProcessEntryPointComponent) {
				return url.entryPoint.proMod.startSIB.outputs
			}
		}
		return #[]
		
	}
	
	override isSIBPortReference(Input sibPort, Output output) {
		sibPort.name == output.name
	}
	
	override getBranchReferences(LinkProcessSIB sib) {
		Collections.EMPTY_LIST
	}
	
	override isBranchReference(Branch branch, EndSIB endSib) {
		false
	}
	
	override getBranchName(EndSIB endSib) {
		null
	}
	
	override getBranchPortReferences(EndSIB endSib) {
		Collections.EMPTY_LIST
	}
	
	override isBranchPortReference(Output branchPort, Input endSibPort) {
		false
	}
	
	override initialize() {
		if(super.initialize()){
			branchCreator = new InitializationBranchCreator(cSib)
			return true		
		}
		false
	}
	
	override update() {
		super.update()
		branchCreator = new UpdateBranchCreator(cSib)
	}
	
	override package initializeBranchesInternal() {
		branchCreator = new InitializationBranchCreator(cSib)
		super.initializeBranchesInternal
	}
	
	override updateBranches() {
	}
	
}
