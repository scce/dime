/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.ModelElement
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.helper.PortUtils.Sync
import info.scce.dime.process.mcam.cli.ProcessExecution
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.SIB
import org.eclipse.emf.ecore.EObject

import static extension info.scce.dime.process.helper.PortUtils.*
import info.scce.dime.process.helper.PortUtils
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.IO
import org.eclipse.emf.ecore.util.EcoreUtil
import info.scce.dime.siblibrary.Port

abstract class PrimeSIBBuild<SIBType extends SIB, SIBPortRef, BranchRef, BranchPortRef> {
	
	val OFFSET = 30
	protected extension WorkbenchExtension = new WorkbenchExtension
	protected extension ResourceExtension = new ResourceExtension
	protected extension GUIExtension = new GUIExtension
	
	protected extension PortUtils = new PortUtils
	
	protected SIBType sib
	protected SIB cSib
	protected Process cModel
	
	protected BranchCreator branchCreator
	
	static def <S extends SIB> getBuild(S sib) {
		switch sib {
			GUISIB: new GuiSIBBuild(sib) as PrimeSIBBuild
		}
	}
	
	new(SIBType sib) {
		this.sib = sib
	}
	
	def EObject getSIBReference(SIBType sib)
	
	def boolean isSIBReferenceValid(SIBType sib)
	
	def Iterable<SIBPortRef> getSIBPortReferences(SIBType sib)
	
	def boolean isSIBPortReference(Input port, SIBPortRef portRef)
	
	def Iterable<BranchRef> getBranchReferences(SIBType sib)
	
	def boolean isBranchReference(Branch branch, BranchRef branchRef)
	
	def String getBranchName(BranchRef branchRef)
	
	def Iterable<BranchPortRef> getBranchPortReferences(BranchRef branchRef)
	
	def boolean isBranchPortReference(Output branchPort, BranchPortRef portRef)
	
	def initialize() {
		if(initializeInternal) {
			initializeSIBInternal
			initializeBranchesInternal		
			return true	
		}
		false
	}
	
	def initializeSIB() {
		if(initializeInternal) {
			initializeSIBInternal		
			return true	
		}
		false
	}
	
	private def initializeInternal() {
		cModel = sib.rootElement
		cSib = sib
		branchCreator = new InitializationBranchCreator(cSib)
		
		if (sib.SIBReference == null) {
			letUserKnow("Reference is null", "Initialization failed. Reference not found!")
			cSib.delete
			return false
		}
		
		return true
	}
	
	def update() {
		cModel = sib.rootElement
		cSib = sib
		branchCreator = new UpdateBranchCreator(cSib)
		
		if (sib.SIBReference == null) {
			letUserKnow("Reference is null", "Update failed. Reference not found!")
			return
		}
		
		updateSIB
		updateBranches
	}
	
	
	
	private def initializeSIBInternal() {
		val sync = new Sync<Input,SIBPortRef> => [
			add    = [ ref | addInput(ref) ]
		]
		sync.apply(sib.inputs, sib.SIBPortReferences)
	}
	
	def updateSIB() {
		val sync = new Sync<Input,SIBPortRef> => [
			equals = [ input,ref | input.isSIBPortReference(ref) ]
			add    = [       ref | addInput(ref) ]
			update = [ input,ref | updateInput(input,ref) ]
			delete = [ input     | input.delete ]
		]
		sync.apply(sib.inputs, sib.SIBPortReferences)
	}
	
	def addInput(SIBPortRef ref) {
		cSib.addInput(ref)?.postProcessNewSIBPort(ref)
	}
	
	def updateInput(Input input,SIBPortRef ref) {
		input.update(ref)
		input.postProcessUpdateSIBPort(ref)
	}
	
	dispatch def void update(Input sibPort, Object ref) {
		val cInput = sibPort
		val cInputNew = cSib.addInput(ref)
		for (cEdge : cInput.incoming.toNewList) {
			cEdge.reconnectTarget(cInputNew)
		}
		cInput.delete
		EcoreUtil.setID(cInputNew, sibPort.id)
	}
	
	dispatch def void update(InputStatic input, Object ref) {
		val staticInputType = switch it:ref {
			ModelElement : primitiveType?.toStaticInputType
			Port: primitiveType?.toStaticInputType
		}
		if (staticInputType?.isInstance(input)) {
			val inputClone = cSib.cloneStaticPort(input)
			EcoreUtil.setID(inputClone, input.id)
		} else {
			cSib.addInput(ref)
		}
		input.delete
	}
	
	def void postProcessNewSIBPort(IO port, Object ref) {
		/* default: do nothing */
	}
	
	def void postProcessUpdateSIBPort(IO port, Object ref) {
		/* default: do nothing */
	}
	
	package def initializeBranchesInternal() {
		val sync = new Sync<Branch,BranchRef> => [
			add    = [        ref | addAndUpdateBranch(ref) ]
		]
		sync.apply(sib.getSuccessors(Branch), sib.branchReferences)
	}
	
	def updateBranches() {
		val sync = new Sync<Branch,BranchRef> => [
			equals = [ branch,ref | branch.isBranchReference(ref) ]
			add    = [        ref | addAndUpdateBranch(ref) ]
			update = [ branch,ref | branch.update(ref) ]
			delete = [ branch     | branch.delete ]
		]
		sync.apply(sib.getSuccessors(Branch), sib.branchReferences)
	}
	
	def addAndUpdateBranch(BranchRef ref) {
		addBranch(ref.getBranchName()).update(ref)
	}
	
	def addBranch(String branchName) {
		val newBranch = branchCreator.newBranch
		newBranch.setName(branchName)
		return newBranch as Branch
	}
	
	def update(Branch branch, BranchRef branchRef) {
		val sync = new Sync<Output,BranchPortRef> => [
			equals = [ port,ref | port.isBranchPortReference(ref) ]
			add    = [      ref | branch.addOutputPort(ref) ]
			update = [ port,ref | port.update(ref, branch) ]
			delete = [ port     | port.delete ]
		]
		sync.apply(branch.outputs, branchRef.branchPortReferences)
	}
	
	def addOutputPort(Branch branch, BranchPortRef ref) {
		branch.addOutput(ref)
	}
	
	def update(Output branchPort, Object portRef, Branch branch) {
		val cOutput = branchPort
		val cOutputNew = branch.addOutput(portRef)
//		FIXME: Setting the id explicitly is not possible anymore. Use EcoreUtil
//		for this purpose
//		Old code: cOutputNew.id = branchPort.id
		for (cEdge : cOutput.outgoing.toNewList) {
			reconnect(cEdge as DataFlow, cOutputNew, null)
		}
		cOutput.delete
		EcoreUtil.setID(cOutputNew, branchPort.id)
	}
	
	def delete(Input input) {
		input?.delete
	}
	
	def delete(Branch branch) {
		branch?.delete
	}
	
	def delete(Output output) {
		output?.delete
	}
	
	def boolean hasErrors(Process model) {
		val file = model.eResource.file.rawLocation.toFile
		val fe = new ProcessExecution
		val adapter = fe.initApiAdapter(file)
		val check = fe.executeCheckPhase(adapter)
		if (check.hasErrors || check.hasWarnings)
			println(check)
		return check.hasErrors
	}
	
	def void letUserKnow(String title, String msg) {
		showInfoDialog(title,msg);
	}
	
	def boolean letUserConfirm(String title, String msg) {
		return showConfirmDialog(title,msg);
	}
	
	def <X> toNewList(Iterable<X> list) {
		val newList = newArrayList
		if (list != null)
			newList.addAll(list)
		return newList
	}
	
	def getStartSIB(Process process) {
		if (process.startSIBs.isEmpty) {
			showError("Referenced process model does not contain a Start SIB");
			return null
		}
		if (process.startSIBs.size > 1) {
			showError("Referenced process model contains multiple Start SIBs");
		}
		process.startSIBs.head
	}
	
	def showError(String message) {
		showErrorDialog("Operation canceled", message);
	}
	
	
}
