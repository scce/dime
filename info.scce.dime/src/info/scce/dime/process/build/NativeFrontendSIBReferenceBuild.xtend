/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.NativeFrontendSIBReference
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.NativeFrontendBranch

class NativeFrontendSIBReferenceBuild extends PrimeSIBBuild<NativeFrontendSIBReference, /* SIB type */ InputPort, /* SIB port reference */ NativeFrontendBranch, /* Branch reference */ OutputPort /* Branch port reference */ > {
	
	new(NativeFrontendSIBReference sib) {
		super(sib)

	}

	override getSIBReference(NativeFrontendSIBReference sib) {
		sib.referencedSib
	}

	override isSIBReferenceValid(NativeFrontendSIBReference sib) {
		true
	}

	override getSIBPortReferences(NativeFrontendSIBReference sib) {
		sib.referencedSib.inputPorts
	}

	override isSIBPortReference(Input port, InputPort portRef) {
		port.name==portRef.name
	}

	override getBranchReferences(NativeFrontendSIBReference sib) {
		sib.referencedSib.nativeFrontendBranchSuccessors
	}

	override isBranchReference(Branch branch, NativeFrontendBranch branchRef) {
		branch.name==branchRef.name
	}

	override getBranchName(NativeFrontendBranch branchRef) {
		branchRef.name
	}

	override getBranchPortReferences(NativeFrontendBranch branchRef) {
		branchRef.outputPorts
	}

	override isBranchPortReference(Output branchPort, OutputPort portRef) {
		branchPort.name==portRef.name
	}
	
	
	static def initialize(NativeFrontendSIBReference sib) {
		new NativeFrontendSIBReferenceBuild(sib).initialize
	}
	
	static def update(NativeFrontendSIBReference sib) {
		new NativeFrontendSIBReferenceBuild(sib).update
	}
}
