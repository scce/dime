/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import info.scce.dime.process.process.AtomicSIB
import info.scce.dime.siblibrary.Branch
import info.scce.dime.siblibrary.Port
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.siblibrary.SIB
import info.scce.dime.process.process.ProcessFactory

class AtomicSIBBuild extends PrimeSIBBuild<
		AtomicSIB,  /* SIB type */
		     Port,  /* SIB port reference */
		   Branch,  /* Branch reference */
		     Port   /* Branch port reference */ > {
	
	new(AtomicSIB sib) {
		super(sib)
	}
	
	static def initialize(AtomicSIB sib) {
		if((sib.sib as SIB).branches.exists[name.equals("noresult")]) {
			val ib = ProcessFactory.eINSTANCE.createIgnoreBranch
			ib.name = "noresult"
			sib.ignoredBranch.add(ib)
		}
		if((sib.sib as SIB).branches.exists[name.equals("failure")]) {
			val ib = ProcessFactory.eINSTANCE.createIgnoreBranch
			ib.name = "failure"
			sib.ignoredBranch.add(ib)
		}
		new AtomicSIBBuild(sib).initialize
	}
	
	static def update(AtomicSIB sib) {
		new AtomicSIBBuild(sib).update
	}
	
	
	override getSIBReference(AtomicSIB sib) {
		sib.getSib
	}
	
	override isSIBReferenceValid(AtomicSIB sib) {
		true
	}
	
	override getSIBPortReferences(AtomicSIB sib) {
		(sib.getSib as SIB).inputPorts
	}
	
	override isSIBPortReference(Input port, Port portRef) {
		port.name == portRef.name
	}
	
	override getBranchReferences(AtomicSIB sib) {
		(sib.getSib as SIB).branches.filter[b|sib.ignoredBranch.map[name].filter[it!==null].findFirst[equals(b.name)]===null]
	}
	
	override isBranchReference(info.scce.dime.process.process.Branch branch, Branch branchRef) {
		branch.name == branchRef.name
	}
	
	override getBranchName(Branch branchRef) {
		branchRef.name
	}
	
	override getBranchPortReferences(Branch branchRef) {
		branchRef.outputPorts
	}
	
	override isBranchPortReference(Output branchPort, Port portRef) {
		branchPort.name == portRef.name
	}
	
}
