/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.build

import de.jabc.cinco.meta.runtime.xapi.CollectionExtension
import graphmodel.ModelElement
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.ProcessSIB
import java.util.List

class ProcessSIBBuild<T extends ProcessSIB> extends PrimeSIBBuild<
				 T, /* SIB type */
	  ModelElement, /* SIB port reference */
		    EndSIB, /* Branch reference */
		     Input  /* Branch port reference */ > {
		     	
	extension CollectionExtension = new CollectionExtension
	
	static def initialize(ProcessSIB sib) {
		new ProcessSIBBuild(sib).initialize
	}
	
	static def update(ProcessSIB sib) {
		new ProcessSIBBuild(sib).update
	}
	
	new(T sib) {
		super(sib)
	}
	
	override getSIBReference(T sib) {
		sib.proMod
	}
	
	override isSIBReferenceValid(T sib) {
		!sib.proMod.hasErrors
	}
	
	override getSIBPortReferences(T sib) {
		val List<ModelElement> references = newArrayList
		val startSIB = sib.proMod.startSIB
		references => [
			addAll(startSIB?.outputs ?: #[])
			addAll(sib.proMod.processPlaceholderSIBs.distinctByKey[label])
		]
	}
	
	override isSIBPortReference(Input sibPort, ModelElement element) {
		sibPort.name == switch element {
			Output:                element.name
			ProcessPlaceholderSIB: element.label
		}
	}
	
	override getBranchName(EndSIB endSib) {
		endSib.branchName
	}
	
	override getBranchReferences(T sib) {
		sib.proMod.endSIBs.filter[b|sib.ignoredBranch.map[name].filter[it!==null].findFirst[equals(b.branchName)]===null]
	}
	
	override isBranchReference(Branch branch, EndSIB endSib) {
		branch.name == endSib.branchName
	}
	
	override getBranchPortReferences(EndSIB endSib) {
		endSib.inputs
	}
	
	override isBranchPortReference(Output branchPort, Input endSibPort) {
		branchPort.name == endSibPort.name
	}
}
