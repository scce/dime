/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks
import java.util.ArrayList
import java.util.List
import java.util.stream.Collectors
import info.scce.dime.checks.AbstractCheck
import info.scce.dime.data.data.Type
import info.scce.dime.process.mcam.adapter.ProcessAdapter
import info.scce.dime.process.mcam.adapter.ProcessId
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.EntryPointProcessSIB
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessInputStatic
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.helper.ProcessExtension

class ProcessInterfaceCheck extends AbstractCheck<ProcessId, ProcessAdapter> {
	ProcessUtils processUtils=new ProcessUtils()
	extension ProcessExtension = new ProcessExtension
	ProcessAdapter adapter
	override void doExecute(ProcessAdapter adapter) {
		this.adapter=adapter 
		for (ProcessId id : adapter.getEntityIds()) {
			var Object obj=id.getElement() 
			if (obj instanceof ProcessSIB) {
				var ProcessSIB sib=(obj as ProcessSIB) 
				if (sib.getProMod() === null) {
					addError(id, "Referenced process does not exist") 
				} else {
					checkInputs(id, sib) 
					checkBranches(id, sib) 
					if (obj instanceof EntryPointProcessSIB) {
						checkInputs(id, (obj as EntryPointProcessSIB)) 
					}
				}
			}
		}
		processResults() 
	}
	override void init() {
	}
	def private void checkInputs(ProcessId id, EntryPointProcessSIB sib) {
		sib.getInputPorts().stream().filter([n | n.getIncoming().isEmpty()]).forEach([n | addError(id, '''Input '«»«n.getName()»' requires incoming dataflow ''')]) 
	}
	def private void checkInputs(ProcessId id, ProcessSIB sib) {
		var List<Input> inputs=sib.getInputs().stream().filter([i | !(i instanceof ProcessInputStatic)]).collect(Collectors.toList()) 
		var Process process=sib.getProMod() 
		for (Output output : process.getStartSIBs().get(0).getOutputs()) {
			var int count=0 
			var Input oInput=null 
			for (Input input : inputs) {
				if (input.getName().equals(output.getName())) {
					oInput=input 
					count++ 
				}
			}
			if (count > 1) {
				addError(id, '''Input '«»«output.getName()»' exists «count» times''') 
			} else if (count <= 0) {
				addError(id, '''Input '«»«output.getName()»' not found''') 
			} else {
				checkTypeOfIO(adapter.getIdByString(oInput.getId()), oInput, output) 
				inputs.remove(oInput) 
			}
		}
		for (Input input : inputs) {
			addError(id, '''Reference for '«»«input.getName()»' not found''') 
		}
	}
	def private void checkBranches(ProcessId sibId, ProcessSIB sib) {
		var ArrayList<Branch> branches=new ArrayList<Branch>(sib.getSuccessors(Branch)) 
		var Process process=sib.getProMod() 
		for (EndSIB endSib : process.getEndSIBs()) {
			var int count=0 
			var Branch oBranch=null 
			for (Branch branch : branches) {
				if (branch.getName().equals(endSib.getBranchName())) {
					oBranch=branch 
					count++ 
				}
			}
			if (count > 1) {
				addError(sibId, '''Branch '«»«endSib.getBranchName()»' exists «count» times''') 
			} else if (count <= 0) {
				if(!sib.isIgnoredBranch(endSib.branchName)) {
					addError(sibId, '''Branch '«»«endSib.getBranchName()»' not found''') 					
				}
			} else {
				checkOutputs(adapter.getIdByString(oBranch.getId()), oBranch, endSib) 
				branches.remove(oBranch) 
			}
		}
		for (Branch branch : branches) {
			addError(sibId, '''Reference for Branch '«»«branch.getName()»' not found''') 
		}
	}
	def private void checkOutputs(ProcessId branchId, Branch branch, EndSIB endSib) {
		var ArrayList<Output> outputs=new ArrayList<Output>(branch.getOutputs()) 
		for (Input input : endSib.getInputs()) {
			var int count=0 
			var Output oOutput=null 
			for (Output output : outputs) {
				if (output.getName().equals(input.getName())) {
					oOutput=output 
					count++ 
				}
			}
			if (count > 1) {
				addError(branchId, '''Output '«»«input.getName()»' exists «count» times''') 
			} else if (count <= 0) {
				addError(branchId, '''Output '«»«input.getName()»' not found''') 
			} else {
				checkTypeOfIO(adapter.getIdByString(oOutput.getId()), oOutput, input) 
				outputs.remove(oOutput) 
			}
		}
		for (Output output : outputs) {
			addError(branchId, '''Reference for '«»«output.getName()»' not found''') 
		}
	}
	def private void checkTypeOfIO(ProcessId thisId, IO thisIO, IO referencedIO) {
		var Object thisType=null 
		var Object refType=null 
		if (thisIO instanceof PrimitiveOutputPort || thisIO instanceof PrimitiveInputPort) thisType=processUtils.getPrimitiveTypeString(thisIO)  else if (thisIO instanceof ComplexOutputPort || thisIO instanceof ComplexInputPort) thisType=processUtils.getComplexType(thisIO)  else if (thisIO instanceof JavaNativeOutputPort || thisIO instanceof JavaNativeInputPort) thisType=processUtils.getNativeType(thisIO)  else if (thisIO instanceof InputStatic) thisType=processUtils.getStaticTypeString(thisIO)  else throw new RuntimeException("Could not find port type");
		if (referencedIO instanceof PrimitiveOutputPort || referencedIO instanceof PrimitiveInputPort) refType=processUtils.getPrimitiveTypeString(referencedIO)  else if (referencedIO instanceof ComplexOutputPort || referencedIO instanceof ComplexInputPort) refType=processUtils.getComplexType(referencedIO)  else if (referencedIO instanceof JavaNativeOutputPort || referencedIO instanceof JavaNativeInputPort) refType=processUtils.getNativeType(referencedIO)  else if (referencedIO instanceof InputStatic) refType=processUtils.getStaticTypeString(referencedIO)  else throw new RuntimeException("Could not find port type");
		if (thisType instanceof String && refType instanceof String) {
			if (!((thisType as String)).equals(refType)) addError(thisId, '''Type mismatch, should be '«»«(refType as String)»'«»''') 
		} else if (thisType instanceof Type && refType instanceof Type) {
			if (!((thisType as Type)).getId().equals(((refType as Type)).getId())) addError(thisId, '''Type mismatch, should be '«»«((refType as Type)).getName()»'«»''') 
		} else {
			addError(thisId, '''Type mismatch, should be '«»«refType.getClass().getSimpleName()»'«»''') 
		}
		if (processUtils.isListType(thisIO) !== processUtils.isListType(referencedIO)) addError(thisId, '''isList property mismatch, should be «processUtils.isListType(referencedIO)»''') 
	}
}
