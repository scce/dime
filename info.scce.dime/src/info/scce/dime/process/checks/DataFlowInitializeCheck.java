/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.Attribute;
import info.scce.dime.process.process.BranchConnector;
import info.scce.dime.process.process.ComplexAttributeConnector;
import info.scce.dime.process.process.ControlFlow;
import info.scce.dime.process.process.DataContext;
import info.scce.dime.process.process.DataFlow;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.DirectDataFlow;
import info.scce.dime.process.process.GuardContainer;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.SIB;
import info.scce.dime.process.process.StartSIB;
import info.scce.dime.process.process.Variable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class DataFlowInitializeCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	private ProcessAdapter adapter;

	private ArrayList<Node> knownElements = new ArrayList<>();
	private HashMap<String, Variable> variables = new HashMap<String, Variable>();
	private HashSet<String> usedVariables = new HashSet<String>();
	private HashMap<String, HashSet<String>> initializedVariables = new HashMap<String, HashSet<String>>();

	private boolean changed = true;

	@Override
	public void doExecute(ProcessAdapter adapter) {
//		System.err.println("[DataFlowInitializeCheck] doExecute");
		try {
			this.adapter = adapter;
			
			for (ProcessId id : adapter.getEntityIds()) {
				Object obj = id.getElement();

//				System.err.println("[DataFlowInitializeCheck] obj: " + obj);
				if (obj instanceof Process) {

//					System.err.println("[DataFlowInitializeCheck] Process: " + obj);
					Process process = (Process) obj;
					variables = collectVariables(process);
	
	//				System.out.println("==========================");
					// calculate initSets
					while (changed) {
	//					System.out.println("[[ analyzing availability... ]]");
						changed = false;
						knownElements = new ArrayList<>();
						for (StartSIB startSib : process.getStartSIBs()) {
							analyzeAvailability(startSib);
						}
					}
					
					addAutoInitializeForListVariables();
	
					usedVariables = new HashSet<String>();
					knownElements = new ArrayList<>();
					for (StartSIB startSib : process.getStartSIBs()) {
						analyzeUsage(startSib);
					}
	
					for (Variable var : variables.values()) {
//						System.out.println("Variable " + var.getId());
//						System.out.println("Variable.internal " + var.getInternalElement().getId());
						if (!usedVariables.contains(var.getName()))
							addWarning(adapter.getIdByString(var.getId()), "'"
									+ var.getName() + "' not used");
					}
				}
			}
			processResults();
		} catch(Exception e) {
//			e.printStackTrace();
			addError(adapter.getIdByString(adapter.getModel().getId()), "check execution failed");
//			throw e;
		}
	}
	
	@Override
	public void init() {
		knownElements.clear();
		variables.clear();
		usedVariables.clear();
		initializedVariables.clear();
		changed = true;
	}

	private void analyzeUsage(Node node) {
		// dont visit nodes twice
		if (knownElements.contains(node))
			return;
		knownElements.add(node);

		// var getter
		if (node instanceof DataFlowTarget) {
			DataFlowTarget dft = (DataFlowTarget) node;

			for (Input input : dft.getInputs()) {
				for (DataFlow df : input.getIncoming(DataFlow.class)) {
					if (df instanceof DirectDataFlow)
						continue;

					Variable var = getRootVariable(df.getSourceElement());
					if (!initializedVariables.get(node.getId()).contains(
							var.getName()))
						addWarning(adapter.getIdByString(node.getId()),
								"'" + var.getName()
										+ "' on port '" + input.getName() + "' not initialized correctly");
					usedVariables.add(var.getName());
				}
			}
		}

		// var setter
		if (node instanceof DataFlowSource) {
			DataFlowSource dfs = (DataFlowSource) node;

			for (Output output : dfs.getOutputs()) {
				for (DataFlow df : output.getOutgoing(DataFlow.class)) {
					if (df instanceof DirectDataFlow)
						continue;

					Node dfNode = df.getTargetElement();
					Variable var = getRootVariable(dfNode);
					if (!dfNode.getId().equals(var.getId())) {
						if (!initializedVariables.get(node.getId()).contains(
								var.getName()))
							addWarning(adapter.getIdByString(node.getId()),
									"'" + var.getName()
									+ "' on port '" + output.getName() + "' not initialized correctly");
						usedVariables.add(var.getName());
					}
				}
			}
		}

		// recursive call
		HashSet<Node> nextNodes = getNextNodes(node);
		for (Node nextNode : nextNodes) {
			analyzeUsage(nextNode);
		}
	}

	private void analyzeAvailability(Node node) {

		// dont visit nodes twice
		if (knownElements.contains(node))
			return;
		knownElements.add(node);

		// init map
		if (initializedVariables.get(node.getId()) == null)
			initializedVariables.put(node.getId(), new HashSet<String>());

		// calculate retain
		HashMap<Node, HashSet<String>> tempInitVars = new HashMap<Node, HashSet<String>>();
		HashSet<Node> prevNodes = getPrevNodes(node);
		for (Node prevNode : prevNodes) {
			if (initializedVariables.get(prevNode.getId()) != null)
				tempInitVars.put(prevNode, new HashSet<String>(
						initializedVariables.get(prevNode.getId())));
		}
		
		// calculate retain
		HashSet<String> tempSet = new HashSet<String>();
		int i = 0;
		for (Node prevNode : tempInitVars.keySet()) {
			if (i == 0)
				tempSet = tempInitVars.get(prevNode);
			else
				tempSet.retainAll(tempInitVars.get(prevNode));
			i++;
		}

		// var setter
		if (node instanceof DataFlowSource) {
			DataFlowSource dfs = (DataFlowSource) node;

			for (Output output : dfs.getOutputs()) {
				for (DataFlow df : output.getOutgoing(DataFlow.class)) {
					if (df instanceof DirectDataFlow)
						continue;

					Node dfNode = df.getTargetElement();
					Variable var = getRootVariable(dfNode);
					if (dfNode.getId().equals(var.getId())) {
						if (!tempSet.contains(var.getName())) {
							tempSet.add(var.getName());
						}
					}
				}
			}
		}

		if (!initializedVariables.get(node.getId()).containsAll(tempSet)
				|| !tempSet.containsAll(initializedVariables.get(node.getId()))) {
//			 System.out.println("CHANGE at: " + node);
//			 System.out.println("initSet"
//			 + initializedVariables.get(node.getId()));
//			 System.out.println("tempSet" + tempSet);

			initializedVariables
					.put(node.getId(), new HashSet<String>(tempSet));
			changed = true;
		}

		// recursive call
		HashSet<Node> nextNodes = getNextNodes(node);
		for (Node nextNode : nextNodes) {
			analyzeAvailability(nextNode);
		}
	}
	
	private HashSet<Node> getNextNodes(Node node) {
		HashSet<Node> nodeSet = new HashSet<Node>();
		for (ControlFlow edge : node.getOutgoing(ControlFlow.class)) {
			nodeSet.add(edge.getTargetElement());
		}
		for (BranchConnector edge : node.getOutgoing(BranchConnector.class)) {
			nodeSet.add(edge.getTargetElement());
		}
		if (node instanceof GuardContainer) {
			GuardContainer gc = (GuardContainer) node;
			nodeSet.addAll(gc.getGuardedProcessSIBs());
			nodeSet.addAll(gc.getGuardProcessSIBs());
		}
		return nodeSet;
	}
	
	private HashSet<Node> getPrevNodes(Node node) {
		HashSet<Node> nodeSet = new HashSet<Node>();
		for (ControlFlow edge : node.getIncoming(ControlFlow.class)) {
			nodeSet.add(edge.getSourceElement());
		}
		for (BranchConnector edge : node.getIncoming(BranchConnector.class)) {
			nodeSet.add(edge.getSourceElement());
		}
		if (node.getContainer() instanceof SIB) {
			nodeSet.add((SIB) node.getContainer());
		}
		return nodeSet;
	}

	private void addAutoInitializeForListVariables() {
		for (String varName : variables.keySet()) {
			Variable var = variables.get(varName);
			if (var.isIsList()) {
				for (String nodeId : initializedVariables.keySet()) {
					initializedVariables.get(nodeId).add(varName);
				}
			}
		}
	}

	private Variable getRootVariable(Node node) {
		if (node instanceof Attribute) {
			Attribute attr = (Attribute) node;
			ModelElementContainer container = attr.getContainer();
			if (container instanceof Node)
				return getRootVariable((Node) attr.getContainer());
		} else if (node instanceof Variable) {
			Variable var = (Variable) node;
			if (!var.getIncoming(ComplexAttributeConnector.class).isEmpty()) {
				Node sourceVar = var
						.getIncoming(ComplexAttributeConnector.class).get(0)
						.getSourceElement();
				return getRootVariable(sourceVar);
			}
			if (!var.getIncoming(ComplexListAttributeConnector.class).isEmpty()) {
				Node sourceVar = var
						.getIncoming(ComplexListAttributeConnector.class)
						.get(0).getSourceElement();
				return getRootVariable(sourceVar);
			}
			return var;
		}
		throw new IllegalStateException(
				"could not identify root variable, maybe missing implementation");
	}

	private HashMap<String, Variable> collectVariables(Process process) {
		variables = new HashMap<String, Variable>();
		for (DataContext dc : process.getDataContexts()) {
			for (Variable var : dc.getVariables()) {
				if (!var.getIncoming(ComplexAttributeConnector.class).isEmpty())
					continue;
				if (!var.getIncoming(ComplexListAttributeConnector.class)
						.isEmpty())
					continue;
				variables.put(var.getName(), var);
			}
		}
		return variables;
	}

}
