/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import graphmodel.Node;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.data.data.Type;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.GuardContainer;
import info.scce.dime.process.process.GuardedProcessSIB;
import info.scce.dime.process.process.IO;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.JavaNativeInputPort;
import info.scce.dime.process.process.JavaNativeOutputPort;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessFactory;
import info.scce.dime.process.process.ProcessType;

import java.util.ArrayList;

public class GuardedProcessInterfaceCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	private ProcessUtils processUtils = new ProcessUtils();

	private ProcessAdapter adapter;

	@Override
	public void doExecute(ProcessAdapter adapter) {
		this.adapter = adapter;
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof GuardedProcessSIB) {
				GuardedProcessSIB sib = (GuardedProcessSIB) obj;
//				checkName(id, sib);
				checkInputs(id, sib);
				checkBranches(id, sib);
			}
		}
	}
	
	@Override
	public void init() {}

	private void checkName(ProcessId id, GuardedProcessSIB sib) {
		Process process = sib.getProMod();
		if (!sib.getLabel().equals(process.getModelName()))
			addError(id, "name mismatch, should be '" + process.getModelName()
					+ "'");
	}

	private void checkInputs(ProcessId id, GuardedProcessSIB sib) {
		Process process = sib.getProMod();
		ArrayList<Input> inputs = new ArrayList<Input>(sib.getInputs());
		ArrayList<Output> outputs = new ArrayList<Output>(process
				.getStartSIBs().get(0).getOutputs());

		// maxUsageCount is implicit existing
		if(sib.getRootElement().getProcessType()==ProcessType.LONG_RUNNING){
			PrimitiveOutputPort maxUsageCountOutput = ProcessFactory.eINSTANCE
					.createPrimitiveOutputPort();
			maxUsageCountOutput.setDataType(PrimitiveType.INTEGER);
			maxUsageCountOutput.setIsList(false);
			maxUsageCountOutput.setName("maxUsageCount");
			outputs.add(maxUsageCountOutput);			
		}

		for (Output output : outputs) {
			int count = 0;
			Input oInput = null;
			for (Input input : inputs) {
				if (input.getName().equals(output.getName())) {
					oInput = input;
					count++;
				}
			}
			if (count > 1) {
				addError(id, "Output '" + output.getName() + "' exists " + count
						+ " times");
			} else if (count <= 0) {
				addWarning(id, "Output '" + output.getName() + "' not found");
			} else {
				checkTypeOfIO(adapter.getIdByString(oInput.getId()), oInput,
						output);
				inputs.remove(oInput);
			}
		}
		for (Input input : inputs) {
			addError(id, "Reference for '" + input.getName() + "' not found");
		}
	}

	private void checkBranches(ProcessId id, GuardedProcessSIB sib) {
		ArrayList<Branch> branches = new ArrayList<Branch>(
				((Node) sib.getContainer()).getSuccessors(Branch.class));
		Process process = sib.getProMod();

		for (EndSIB endSib : process.getEndSIBs()) {
			int count = 0;
			Branch oBranch = null;
			for (Branch branch : branches) {
				if (branch.getName().equals(endSib.getBranchName())) {
					oBranch = branch;
					count++;
				}
			}
			if (count > 1) {
				addError(id, "Branch '" + endSib.getBranchName() + "' exists "
						+ count + " times");
			} else if (count <= 0) {
				addError(id, "Branch '" + endSib.getBranchName() + "' not found");
			} else {
				checkOutputs(adapter.getIdByString(oBranch.getId()), oBranch,
						endSib);
				branches.remove(oBranch);
			}
		}
		if(branches.stream().filter(n->n.getName().equals("denied")).count()>1){
			addError(id, "only one denied branch can be present");
		}
		
		for (Branch branch : branches) {
			if(branch.getName().equals("denied")){
				if(sib.getContainer() instanceof GuardContainer){
					GuardContainer gc = (GuardContainer) sib.getContainer();
					if(!gc.getGuardProcessSIBs().isEmpty())continue;
				}
			}
			addError(id, "Reference for Branch '" + branch.getName()
					+ "' not found");
		}
	}

	private void checkOutputs(ProcessId branchId, Branch branch, EndSIB endSib) {
		ArrayList<Output> outputs = new ArrayList<Output>(branch.getOutputs());

		for (Input input : endSib.getInputs()) {
			int count = 0;
			Output oOutput = null;
			for (Output output : outputs) {
				if (output.getName().equals(input.getName())) {
					oOutput = output;
					count++;
				}
			}
			if (count > 1) {
				addError(branchId, "Input '" + input.getName() + "' exists " + count
						+ " times");
			} else if (count <= 0) {
				addError(branchId, "Input '" + input.getName() + "' not found");
			} else {
				checkTypeOfIO(adapter.getIdByString(oOutput.getId()), oOutput,
						input);
				outputs.remove(oOutput);
			}
		}
		for (Output output : outputs) {
			addError(branchId, "Reference for '" + output.getName() + "' not found");
		}
	}

	private void checkTypeOfIO(ProcessId thisId, IO thisIO, IO referencedIO) {
		Object thisType = null;
		Object refType = null;

		if (thisIO instanceof PrimitiveOutputPort
				|| thisIO instanceof PrimitiveInputPort)
			thisType = processUtils.getPrimitiveTypeString(thisIO);
		else if (thisIO instanceof ComplexOutputPort
				|| thisIO instanceof ComplexInputPort)
			thisType = processUtils.getComplexType(thisIO);
		else if (thisIO instanceof JavaNativeOutputPort
				|| thisIO instanceof JavaNativeInputPort)
			thisType = processUtils.getNativeType(thisIO);
		else if (thisIO instanceof InputStatic)
			thisType = processUtils.getStaticTypeString(thisIO);
		else
			throw new RuntimeException("Could not find port type");

		if (referencedIO instanceof PrimitiveOutputPort
				|| referencedIO instanceof PrimitiveInputPort)
			refType = processUtils.getPrimitiveTypeString(referencedIO);
		else if (referencedIO instanceof ComplexOutputPort
				|| referencedIO instanceof ComplexInputPort)
			refType = processUtils.getComplexType(referencedIO);
		else if (referencedIO instanceof JavaNativeOutputPort
				|| referencedIO instanceof JavaNativeInputPort)
			refType = processUtils.getNativeType(referencedIO);
		else if (referencedIO instanceof InputStatic)
			refType = processUtils.getStaticTypeString(referencedIO);
		else
			throw new RuntimeException("Could not find port type");

		if (thisType instanceof String && refType instanceof String) {
			if (!((String) thisType).equals(refType))
				addError(thisId, "Type mismatch, should be '" + (String) refType
						+ "'");
		} else if (thisType instanceof Type && refType instanceof Type) {
			if (!((Type) thisType).getId().equals(((Type) refType).getId()))
				addError(thisId, "Type mismatch, should be '"
						+ ((Type) refType).getName() + "'");
		} else {
			addError(thisId, "Type mismatch, should be '"
					+ refType.getClass().getSimpleName() + "'");
		}

		if (processUtils.isListType(thisIO) != processUtils
				.isListType(referencedIO))
			addError(thisId, "isList property mismatch, should be "
					+ processUtils.isListType(referencedIO));
	}
}
