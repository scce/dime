/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;

public class InputStaticCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof TextInputStatic) {
				checkText(id, (TextInputStatic) obj);	
			}
			if (obj instanceof TimestampInputStatic) {
				checkTimeStamp(id, (TimestampInputStatic) obj);	
			}
		}
	}
	
	@Override
	public void init() {}
	
	private void checkTimeStamp(ProcessId id, TimestampInputStatic obj) {
		if(String.valueOf(obj.getValue()).length() > 10){
			addError(id, "Only 10 digits allowed");
		}
		if(obj.getValue()<0){
			addError(id, "Timestamp has to be positive");
		}
		
	}
	
	private void checkText(ProcessId id, TextInputStatic te) {
		String value = te.getValue();
		
		if (value != null) {
			//check escapable characters
			for(String i:info.scce.dime.gui.checks.InputStaticCheck.escapable){
				String subString = value;
				while(subString.indexOf(i)>-1){
					int indexOf = subString.indexOf(i);
					if(indexOf > 0){
						if(subString.charAt(indexOf-1)!='\\'){
							addError(id, "value char: "+i+" has to be escaped");
						}
					}else{
						addError(id, "value char: "+i+" has to be escaped");
					}
					subString = subString.substring(indexOf+1);
				}
			}

		}
	}

}
