/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import info.scce.dime.api.DIMECheck;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.EnumLiteral;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.TypeAttribute;
import info.scce.dime.data.data.UserType;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.helper.GUIExtension;
import info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericComplexPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericOutputBranch;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPrimitivePort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.process.helper.ProcessExtension;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.BooleanInputStatic;
import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.ContainsPrimitiveSIB;
import info.scce.dime.process.process.ContainsSIB;
import info.scce.dime.process.process.CreateSIB;
import info.scce.dime.process.process.CreateUserSIB;
import info.scce.dime.process.process.DataFlow;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.DeleteSIB;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.EnumSwitchSIB;
import info.scce.dime.process.process.GenericSIB;
import info.scce.dime.process.process.GUISIB;
import info.scce.dime.process.process.IO;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.InputPort;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.IntegerInputStatic;
import info.scce.dime.process.process.IsOfTypeSIB;
import info.scce.dime.process.process.IteratePrimitiveSIB;
import info.scce.dime.process.process.IterateSIB;
import info.scce.dime.process.process.JavaNativeInputPort;
import info.scce.dime.process.process.JavaNativeOutputPort;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.OutputPort;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.PutComplexToContextSIB;
import info.scce.dime.process.process.PutPrimitiveToContextSIB;
import info.scce.dime.process.process.PutToContextSIB;
import info.scce.dime.process.process.RealInputStatic;
import info.scce.dime.process.process.RemoveFromListSIB;
import info.scce.dime.process.process.RetrieveCurrentUserSIB;
import info.scce.dime.process.process.RetrieveEnumLiteralSIB;
import info.scce.dime.process.process.RetrieveOfTypeSIB;
import info.scce.dime.process.process.SIB;
import info.scce.dime.process.process.SetAttributeValueSIB;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;
import info.scce.dime.process.process.UnsetAttributeValueSIB;

public class SibValidationCheck extends DIMECheck<ProcessId, ProcessAdapter> {

	private ProcessAdapter adapter;

	private ProcessUtils processUtils = new ProcessUtils();

	@Override
	public void doExecute(ProcessAdapter adapter) {
		this.adapter = adapter;
		for (ProcessId id : adapter.getEntityIds()) {
			EObject obj = id.getElement();
			if (obj instanceof DataFlowTarget) {
				checkLabel(id, (DataFlowTarget) obj);
			}
			if (obj instanceof SIB) {
				checkSIBIgnoreBranches(id, (SIB) obj);
			}
			if (obj instanceof PutToContextSIB)
				checkPutToContextSIB(id, (PutToContextSIB) obj);
			if (obj instanceof PutPrimitiveToContextSIB)
				checkPutPrimitiveToContext(id, (PutPrimitiveToContextSIB) obj);
			if (obj instanceof PutComplexToContextSIB)
				checkPutComplexToContextSIB(id, (PutComplexToContextSIB) obj);
			if (obj instanceof RetrieveOfTypeSIB)
				checkRetrieveOfTypeSIB(id, (RetrieveOfTypeSIB) obj);
			if (obj instanceof DeleteSIB)
				checkDeleteSIB(id, (DeleteSIB) obj);
			if (obj instanceof CreateSIB)
				checkCreateSIB(id, (CreateSIB) obj);
			if (obj instanceof CreateUserSIB)
				checkCreateUserSIB(id, (CreateUserSIB) obj);
			if (obj instanceof IterateSIB)
				checkIterateSIB(id, (IterateSIB) obj);
			if (obj instanceof SetAttributeValueSIB)
				checkSetAttributeValueSIB(id, (SetAttributeValueSIB) obj);
			if (obj instanceof UnsetAttributeValueSIB)
				checkUnsetAttributeValueSIB(id, (UnsetAttributeValueSIB) obj);
			if (obj instanceof RetrieveEnumLiteralSIB)
				checkRetrieveEnumLiteralSIB(id, (RetrieveEnumLiteralSIB) obj);
			if (obj instanceof EnumSwitchSIB)
				checkEnumSwitchSIB(id, (EnumSwitchSIB) obj);
			if (obj instanceof RemoveFromListSIB)
				checkRemoveFromListSIB(id, (RemoveFromListSIB) obj);
			if (obj instanceof GUISIB)
				checkGUISIB(id, (GUISIB) obj);
			if (obj instanceof ContainsPrimitiveSIB)
				checkContainsPrimitiveSIB(id, (ContainsPrimitiveSIB) obj);
			if (obj instanceof ContainsSIB)
				checkContainsSIB(id, (ContainsSIB) obj);
			if (obj instanceof IteratePrimitiveSIB)
				checkIteratePrimitiveSIB(id, (IteratePrimitiveSIB) obj);
			if (obj instanceof IsOfTypeSIB)
				checkIsOfTypeSIB(id, (IsOfTypeSIB) obj);
			if (obj instanceof RetrieveCurrentUserSIB)
				checkRetrieveCurrentUserSIB(id, (RetrieveCurrentUserSIB) obj);
			if (obj instanceof GenericSIB) {
				checkGenericSIB(id, (GenericSIB) obj);
			}
		}

		processResults();
	}

	private void checkContainsPrimitiveSIB(ProcessId id, ContainsPrimitiveSIB obj) {
		if (obj.getInputs().size() != 2) {
			addError(id, "exactly two inputs required");
		}
		// check list
		Optional<PrimitiveInputPort> listPort = obj.getPrimitiveInputPorts().stream()
				.filter((n) -> n.getName().equals("list")).findFirst();
		if (!listPort.isPresent()) {
			addError(id, "list port missing");
		} else {
			// check list
			if (!listPort.get().isIsList()) {
				addError(id, "list port has to be list");
			}
		}
		Optional<Input> elementPort = obj.getInputs().stream().filter((n) -> n.getName().equals("element")).findFirst();
		if (!elementPort.isPresent()) {
			addError(id, "element port missing");
		} else {
			// check static
			if (elementPort.get() instanceof InputStatic) {
				// check equal types
				if (elementPort.get() instanceof IntegerInputStatic && listPort.isPresent()
						&& (listPort.get().getDataType() != PrimitiveType.INTEGER)) {
					addError(id, "list and element port type do not match");
				} else if (elementPort.get() instanceof TextInputStatic && listPort.isPresent()
						&& (listPort.get().getDataType() != PrimitiveType.TEXT)) {
					addError(id, "list and element port type do not match");
				} else if (elementPort.get() instanceof RealInputStatic && listPort.isPresent()
						&& (listPort.get().getDataType() != PrimitiveType.REAL)) {
					addError(id, "list and element port type do not match");
				} else if (elementPort.get() instanceof TimestampInputStatic && listPort.isPresent()
						&& (listPort.get().getDataType() != PrimitiveType.TIMESTAMP)) {
					addError(id, "list and element port type do not match");
				} else if (elementPort.get() instanceof BooleanInputStatic && listPort.isPresent()
						&& (listPort.get().getDataType() != PrimitiveType.BOOLEAN)) {
					addError(id, "list and element port type do not match");
				} else {
					addError(id, "list and element port type do not match");
				}
			} else if (elementPort.get() instanceof PrimitiveInputPort) {
				// check primitives ports match
				if (listPort.isPresent()) {
					if (((PrimitiveInputPort) elementPort.get()).getDataType() != listPort.get().getDataType()) {
						addError(id, "list and element port type do not match");
					}
				}
			} else {
				addError(id, "element port has to be primitive");
			}
		}

		// check branches
		if (obj.getBranchSuccessors().size() != 2
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("yes"))
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("no"))) {
			addError(id, "branches 'yes' and 'no' are required");
		}
		// check ports
		if (obj.getBranchSuccessors().stream().anyMatch((n) -> !n.getOutputs().isEmpty())) {
			addError(id, "no ports allowed");
		}
	}

	private void checkGenericSIB(ProcessId id, GenericSIB obj) {
		EObject sibRef = obj.getReferencedObject();
		IModeltrafoSupporter<EObject> modeltrafoSupporter = ModeltrafoExtensionProvider
				.getSupportedModelElement(sibRef);
		IGenericSIBBuilder<EObject> modellingProvider = modeltrafoSupporter.getModellingProvider();
		List<GenericPort> expectedInputPorts = modellingProvider.getInputPorts(sibRef);
		List<GenericOutputBranch> expectedOutputBranches = modellingProvider.getOutputBranches(sibRef);

		for (GenericPort expectedInputPort : expectedInputPorts) {
			Optional<Input> matchingInput = obj.getInputs().stream()
					.filter((n) -> n.getName().equals(expectedInputPort.getName())).findFirst();
			if (!matchingInput.isPresent()) {
				addError(id, "'" + expectedInputPort.getName() + "' port missing");
			} else {
				Input presentMatchingInput = matchingInput.get();
				if (presentMatchingInput instanceof InputPort) {
					if (presentMatchingInput instanceof PrimitiveInputPort) {
						PrimitiveInputPort matchingPrimitiveInputPort = (PrimitiveInputPort) presentMatchingInput;
						if (expectedInputPort instanceof GenericPrimitivePort) {
							GenericPrimitivePort expectedPrimitiveInputPort = (GenericPrimitivePort) expectedInputPort;
							if (!matchingPrimitiveInputPort.getDataType().getLiteral()
									.equals(expectedPrimitiveInputPort.getType().getLiteral())) {
								addError(id,
										"'" + matchingPrimitiveInputPort.getDataType().getLiteral()
												+ "' does not match the input type '"
												+ expectedPrimitiveInputPort.getType().getLiteral() + "'");
							}
						} else {
							addError(id, "'" + matchingPrimitiveInputPort.getDataType().getLiteral()
									+ "' does not match the input type of input '" + expectedInputPort.getName() + "'");
						}
					} else if (presentMatchingInput instanceof ComplexInputPort) {
						ComplexInputPort matchingComplexInputPort = (ComplexInputPort) presentMatchingInput;
						if (expectedInputPort instanceof GenericComplexPort) {
							GenericComplexPort expectedComplexInputPort = (GenericComplexPort) expectedInputPort;
							if (!(matchingComplexInputPort.getDataType() == expectedComplexInputPort.getType())) {
								addError(id,
										"'" + matchingComplexInputPort.getDataType().getName()
												+ "' does not match the input type '"
												+ expectedComplexInputPort.getType().getName() + "'");
							}
						} else {
							addError(id, "'" + matchingComplexInputPort.getDataType().getName()
									+ "' does not match the input type of input '" + expectedInputPort.getName() + "'");
						}
					}
				}
			}
		}
		
		// check branches
		for (GenericOutputBranch expectedOutputBranch : expectedOutputBranches) {
			Optional<Branch> matchingBranch = obj.getBranchSuccessors().stream()
					.filter((n) -> n.getName().equals(expectedOutputBranch.getName())).findFirst();
			if (!matchingBranch.isPresent()) {
				addError(id, "'" + expectedOutputBranch.getName() + "' branch missing");
			} else {
				Branch presentMatchingBranch = matchingBranch.get();
				for (GenericPort expectedPort : expectedOutputBranch.getOutputPorts()) {
					Optional<OutputPort> matchingPort = presentMatchingBranch.getOutputPorts().stream()
							.filter((n) -> n.getName().equals(expectedPort.getName())).findFirst();
					if (!matchingPort.isPresent()) {
						addError(id, "'" + expectedPort.getName() + "' port of branch'" + expectedOutputBranch.getName() + "'missing");
					} else {
						OutputPort presentMatchingPort = matchingPort.get();
						if (presentMatchingPort instanceof PrimitiveOutputPort) {
							PrimitiveOutputPort matchingPrimitivePort = (PrimitiveOutputPort) presentMatchingPort;
							if (expectedPort instanceof GenericPrimitivePort) {
								GenericPrimitivePort expectedPrimitivePort = (GenericPrimitivePort) expectedPort;
								if (!matchingPrimitivePort.getDataType().getLiteral()
										.equals(expectedPrimitivePort.getType().getLiteral())) {
									addError(id,
											"'" + matchingPrimitivePort.getDataType().getLiteral()
													+ "' does not match the input type '"
													+ expectedPrimitivePort.getType().getLiteral() + "'");
								}
							} else {
								addError(id, "'" + matchingPrimitivePort.getDataType().getLiteral()
										+ "' does not match the input type of input '" + expectedPort.getName() + "'");
							}
						} else if (presentMatchingPort instanceof ComplexOutputPort) {
							ComplexOutputPort matchingComplexPort = (ComplexOutputPort) presentMatchingPort;
							if (expectedPort instanceof GenericComplexPort) {
								GenericComplexPort expectedComplexPort = (GenericComplexPort) expectedPort;
								if (!(matchingComplexPort.getDataType() == expectedComplexPort.getType())) {
									addError(id,
											"'" + matchingComplexPort.getDataType().getName()
													+ "' does not match the input type '"
													+ expectedComplexPort.getType().getName() + "'");
								}
							} else {
								addError(id, "'" + matchingComplexPort.getDataType().getName()
										+ "' does not match the input type of input '" + expectedPort.getName() + "'");
							}
						}
					}
				}
			}
		}
		
		// check branches ports
	}

	private void checkContainsSIB(ProcessId id, ContainsSIB obj) {
		if (obj.getInputs().size() != 2) {
			addError(id, "exactly two inputs required");
		}
		// check list
		Optional<ComplexInputPort> listPort = obj.getComplexInputPorts().stream()
				.filter((n) -> n.getName().equals("list")).findFirst();
		if (!listPort.isPresent()) {
			addError(id, "list port missing");
		} else {
			// check list
			if (!listPort.get().isIsList()) {
				addError(id, "list port has to be list");
			}
		}
		Optional<ComplexInputPort> elementPort = obj.getComplexInputPorts().stream()
				.filter((n) -> n.getName().equals("element")).findFirst();
		if (!elementPort.isPresent()) {
			addError(id, "element port missing");
		} else {
			if (listPort.isPresent()) {
				if (!elementPort.get().getDataType().equals(listPort.get().getDataType())) {
					addError(id, "list and element port type do not match");
				}
			}
		}

		// check branches
		if (obj.getBranchSuccessors().size() != 2
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("yes"))
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("no"))) {
			addError(id, "branches 'yes' and 'no' are required");
		}
		// check ports
		if (obj.getBranchSuccessors().stream().anyMatch((n) -> !n.getOutputs().isEmpty())) {
			addError(id, "no ports allowed");
		}
	}

	private void checkRetrieveCurrentUserSIB(ProcessId id, RetrieveCurrentUserSIB obj) {
		if (obj.getInputs().size() != 0) {
			addError(id, "no inputs allowed");
		}

		ProcessExtension pe = new ProcessExtension();
		// check ports
		if (pe.isIgnoredBranch(obj, "not authenticated")) {
			if (obj.getBranchSuccessors().size() != 1
					|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("success"))) {
				addError(id, "branches 'success' is required");
			}
		} else {
			// check branches
			if (obj.getBranchSuccessors().size() != 2
					|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("success"))
					|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("not authenticated"))) {
				addError(id, "branches 'success' and 'not authenticated' are required");
			}

		}

		// check ports
		if (obj.getBranchSuccessors().stream().filter((n) -> n.getName().equals("not authenticated"))
				.anyMatch((n) -> !n.getOutputs().isEmpty())) {
			addError(id, "no ports allowed on not authenticated branch");
		}
		if (obj.getBranchSuccessors().stream().filter((n) -> n.getName().equals("success"))
				.anyMatch((n) -> n.getComplexOutputPorts().isEmpty())) {
			addError(id, "currentUser port is required");
		} else {
			Branch successBranch = obj.getBranchSuccessors().stream().filter((n) -> n.getName().equals("success"))
					.findFirst().get();
			ComplexOutputPort currentUserPort = successBranch.getComplexOutputPorts().stream()
					.filter((n) -> n.getName().equals("currentUser")).findFirst().get();
			if (currentUserPort.isIsList()) {
				addError(id, "currentUser port has to be no list");
			}
		}
	}

	private void checkIteratePrimitiveSIB(ProcessId id, IteratePrimitiveSIB obj) {
		if (obj.getInputs().size() != 1) {
			addError(id, "exactly one inputs required");
		}
		// check list
		Optional<PrimitiveInputPort> listPort = obj.getPrimitiveInputPorts().stream()
				.filter((n) -> n.getName().equals("list")).findFirst();
		if (!listPort.isPresent()) {
			addError(id, "list port missing");
		} else {
			// check list
			if (!listPort.get().isIsList()) {
				addError(id, "list port has to be list");
			}
		}
		// check branches
		if (obj.getBranchSuccessors().size() != 2
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("exit"))
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("next"))) {
			addError(id, "branches 'exit' and 'next' are required");
		}
		// check exit no ports
		if (obj.getBranchSuccessors().stream().anyMatch((n) -> n.getName().equals("exit"))) {
			if (!obj.getBranchSuccessors().stream().filter((n) -> n.getName().equals("exit")).findFirst().get()
					.getOutputs().isEmpty()) {
				addError(id, "no ports allowed on exit branch");
			}
		}

		// check next matching port
		if (obj.getBranchSuccessors().stream().anyMatch((n) -> n.getName().equals("next"))) {
			Optional<PrimitiveOutputPort> elementPort = obj.getBranchSuccessors().stream()
					.filter((n) -> n.getName().equals("next")).findFirst().get().getPrimitiveOutputPorts().stream()
					.filter((n) -> n.getName().equals("element")).findFirst();
			if (!elementPort.isPresent()) {
				addError(id, "element port is required on next branch");
			} else {
				// check matching type
				if (listPort.isPresent()) {
					if (elementPort.get().getDataType() != listPort.get().getDataType()) {
						addError(id, "list and element port types do not match");
					}
				}
			}
		}

	}

	private void checkIsOfTypeSIB(ProcessId id, IsOfTypeSIB obj) {
		if (obj.getInputs().size() != 1) {
			addError(id, "exactly one inputs required");
		}
		// check list
		Optional<ComplexInputPort> listPort = obj.getComplexInputPorts().stream()
				.filter((n) -> n.getName().equals("instance")).findFirst();
		if (!listPort.isPresent()) {
			addError(id, "instance port missing");
		} else {
			// check list
			if (listPort.get().isIsList()) {
				addError(id, "instance port has to be no list");
			}
		}
		// check branches
		if (obj.getBranchSuccessors().size() != 2
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("yes"))
				|| obj.getBranchSuccessors().stream().noneMatch((n) -> n.getName().equals("no"))) {
			addError(id, "branches 'yes' and 'no' are required");
		}
		// check exit no ports
		if (obj.getBranchSuccessors().stream().anyMatch((n) -> n.getName().equals("no"))) {
			if (!obj.getBranchSuccessors().stream().filter((n) -> n.getName().equals("no")).findFirst().get()
					.getOutputs().isEmpty()) {
				addError(id, "no ports allowed on no branch");
			}
		}

		// check next matching port
		if (obj.getBranchSuccessors().stream().anyMatch((n) -> n.getName().equals("yes"))) {
			Optional<ComplexOutputPort> elementPort = obj.getBranchSuccessors().stream()
					.filter((n) -> n.getName().equals("yes")).findFirst().get().getComplexOutputPorts().stream()
					.filter((n) -> n.getName().equals("casted")).findFirst();
			if (!elementPort.isPresent()) {
				addError(id, "casted port is required on next branch");
			} else {
				// check matching type
				if (listPort.isPresent()) {
					if (elementPort.get().getDataType() != obj.getCheckedType()) {
						addError(id, "instance and casted port types do not match");
					}
				}
			}
		}

	}

	private void checkSIBIgnoreBranches(ProcessId id, SIB s) {
		// check no duplicated ignore
		if (s.getIgnoredBranch().size() > s.getIgnoredBranch().stream().map((n) -> n.getName())
				.collect(Collectors.toSet()).size()) {
			addError(id, "Duplicated ignored branches");
		}
		// check invalid ignored branches
		GUIExtension ge = new GUIExtension();
		final Map<String, String> m = new HashMap<>();
		ge.getIgnorableBranches(s, m);
		s.getIgnoredBranch().stream().filter((n) -> !m.containsKey(n.getName()))
				.forEach((n) -> addError(id, "Ignored Branch " + n.getName() + " not found"));
	}

	private void checkGUISIB(ProcessId id, GUISIB te) {
		// check Inputstaticports
		GUI gui = te.getGui();
		if (gui != null) {
			Iterable<PrimitiveVariable> primitiveVariables = _graphModelExtension.find(gui, PrimitiveVariable.class);
			for (InputStatic inputStatic : te.getInputStatics()) {
				PrimitiveVariable correspondingVariable = getCorrespondingVariable(primitiveVariables, inputStatic);
				if (correspondingVariable == null) {
					addError(id, "GUISIB '" + te.getName() + "': corresponding variable with name '"
							+ inputStatic.getName() + "' is missing");
				} else { // corresponding variable exists
					String correspondingType = correspondingVariable.getDataType().getName();
					boolean correspondingIsList = correspondingVariable.isIsList();
					if (inputStatic instanceof IntegerInputStatic) {
						if (correspondingType != "Integer") {
							addError(id,
									"Type mismatch of IntegerInputStatic in GUISIB '" + te.getName()
											+ "': corresponding variable '" + correspondingVariable.getName()
											+ "' does not have an integer type");
						}
						if (correspondingIsList) {
							addError(id, "List type mismatch of IntegerInputStatic in GUISIB '" + te.getName()
									+ "':  corresponding variable '" + correspondingVariable.getName() + "' is a list");
						}
					}
					if (inputStatic instanceof BooleanInputStatic) {
						if (correspondingType != "Boolean") {
							addError(id,
									"Type mismatch of BooleanInputStatic in GUISIB '" + te.getName()
											+ "': corresponding variable '" + correspondingVariable.getName()
											+ "' does not have a boolean type");
						}
						if (correspondingIsList) {
							addError(id, "List type mismatch of BooleanInputStatic in GUISIB '" + te.getName()
									+ "': corresponding variable '" + correspondingVariable.getName() + "' is a list");
						}
					}
					if (inputStatic instanceof RealInputStatic) {
						if (correspondingType != "Real") {
							addError(id,
									"Type mismatch of RealInputStatic in GUISIB '" + te.getName()
											+ "': corresponding variable '" + correspondingVariable.getName()
											+ "' does not have a real type");
						}
						if (correspondingIsList) {
							addError(id, "List type mismatch of RealInputStatic in GUISIB '" + te.getName()
									+ "': corresponding variable '" + correspondingVariable.getName() + "' is a list");
						}
					}
					if (inputStatic instanceof TextInputStatic) {
						if (correspondingType != "Text") {
							addError(id,
									"Type mismatch of TextInputStatic in GUISIB '" + te.getName()
											+ "': corresponding variable '" + correspondingVariable.getName()
											+ "' does not have a text type");
						}
						if (correspondingIsList) {
							addError(id, "List type mismatch of TextInputStatic in GUISIB '" + te.getName()
									+ "': corresponding variable '" + correspondingVariable.getName() + "' is a list");
						}
					}
					if (inputStatic instanceof TimestampInputStatic) {
						if (correspondingType != "Timestamp") {
							addError(id,
									"Type mismatch of TimestampInputStatic in GUISIB '" + te.getName()
											+ "': corresponding variable '" + correspondingVariable.getName()
											+ "' does not have a timestamp type");
						}
						if (correspondingIsList) {
							addError(id, "List type mismatch of TimestampInputStatic in GUISIB '" + te.getName()
									+ "': corresponding variable '" + correspondingVariable.getName() + "' is a list");
						}
					}
				}

			}

		}
	}

	private PrimitiveVariable getCorrespondingVariable(Iterable<PrimitiveVariable> variables, InputStatic inputStatic) {
		for (PrimitiveVariable variable : variables) {
			if (variable.getName() != null && variable.getName().equals(inputStatic.getName())) {
				return variable;
			}
		}
		return null;
	}

	private void checkLabel(ProcessId id, DataFlowTarget obj) {
		if (obj instanceof SIB) {
			if (((SIB) obj).getLabel() == null) {
				addError(id, "label has to be provided");
			}
		}
		if (obj instanceof EndSIB) {
			if (((EndSIB) obj).getBranchName() == null) {
				addError(id, "label has to be provided");
			}
		}
	}

	private void checkEnumSwitchSIB(ProcessId sibId, EnumSwitchSIB sib) {
		String inputDefault = "enum";
		if (!sib.getInputs().isEmpty()) {
			inputDefault = sib.getInputPorts().get(0).getName();
		}
		String[] inputList = new String[] { inputDefault };
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		checkSIB(sibId, sib, inputList, branchMap, true, false, false);

		EnumType enumType = sib.getSwitchedType();
		if (enumType == null) {
			addError(sibId, "reference not found / is null");
			return;
		}

		// check enum type
		for (Input input : sib.getInputs()) {
			if ("enum".equals(input.getName())) {
				if (input instanceof ComplexInputPort == false) {
					addError(sibId, "input port type incorrect");
					continue;
				}
				ComplexInputPort complexInput = (ComplexInputPort) input;
				if (!complexInput.getDataType().getId().equals(enumType.getId()))
					addError(sibId, "input port type incorrect");
			}
		}

		// check branches
		List<String> literals = enumType.getEnumLiterals().stream().map(l -> l.getName()).collect(Collectors.toList());
		literals.add("else");
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if (!literals.contains(branch.getName())) {
				addError(sibId, "unallowed branch '" + branch.getName() + "'");
				continue;
			}
			if (branch.getOutputs().size() > 0)
				addError(sibId, "unallowed outputs in branch '" + branch.getName() + "'");
			literals.remove(branch.getName());
		}
		for (String literal : literals) {
			if ((getMatchCount(sib) != (sib.getSwitchedType().getEnumLiterals().size())) && literal != "else"
					&& !elseExists(sib)) { //
				addError(sibId, "missing branch for literal '" + literal + "' or 'else'");
			}
		}

	}

	private int getMatchCount(EnumSwitchSIB sib) {
		int match = 0;
		for (EnumLiteral literal : sib.getSwitchedType().getEnumLiterals()) {
			for (Branch branch : sib.getBranchSuccessors()) {
				if (branch.getName().equals(literal.getName())) {
					match++;
				}
			}
		}
		return match;
	}

	private boolean elseExists(EnumSwitchSIB sib) {
		for (Branch branch : sib.getBranchSuccessors()) {
			if (branch.getName().equals("else")) {
				return true;
			}
		}
		return false;
	}

	private void checkRetrieveEnumLiteralSIB(ProcessId sibId, RetrieveEnumLiteralSIB sib) {

		String[] inputList = new String[] {};
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		checkSIB(sibId, sib, inputList, branchMap, true, false, false);

		EnumLiteral enumLiteral = sib.getRetrievedLiteral();
		if (enumLiteral == null) {
			addError(sibId, "reference not found / is null");
			return;
		}

		// check branches
		boolean foundBranch = false;
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if (!enumLiteral.getName().equals(branch.getName())) {
				addError(sibId, "unallowed branch '" + branch.getName() + "'");
				continue;
			}
			boolean foundOutput = false;
			for (Output output : branch.getOutputs()) {
				if (!"literal".equals(output.getName())) {
					addError(sibId, "unallowed output '" + output.getName() + "'");
					continue;
				}
				if (output instanceof ComplexOutputPort == false) {
					addError(sibId, "incorrect type of output '" + output.getName() + "'");
					continue;
				}
				ComplexOutputPort complexOutput = (ComplexOutputPort) output;
				if (!complexOutput.getDataType().getId().equals(enumLiteral.getContainer().getId()))
					addError(sibId, "incorrect type of output '" + output.getName() + "'");

				foundOutput = true;
			}
			if (!foundOutput)
				addError(sibId, "missing output for literal '" + enumLiteral.getName() + "'");

			foundBranch = true;
		}
		if (!foundBranch)
			addError(sibId, "missing branch for literal '" + enumLiteral.getName() + "'");
	}

	private void checkSetAttributeValueSIB(ProcessId sibId, SetAttributeValueSIB sib) {
		TypeAttribute attr = sib.getAttribute();
		Type type = (Type) attr.getContainer();
		String[] inputList = new String[] { type.getName().toLowerCase(), attr.getName().toLowerCase() };

		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("success", (new String[] {}));

		checkSIB(sibId, sib, inputList, branchMap);
	}

	private void checkUnsetAttributeValueSIB(ProcessId sibId, UnsetAttributeValueSIB sib) {
		TypeAttribute attr = sib.getAttribute();
		Type type = (Type) attr.getContainer();
		String[] inputList = new String[] { type.getName().toLowerCase() };

		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("success", new String[] {});

		checkSIB(sibId, sib, inputList, branchMap);
	}

	private void checkIterateSIB(ProcessId sibId, IterateSIB sib) {
		String[] inputList = new String[] { "list", };
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("next", (new String[] { "element" }));
		branchMap.put("exit", (new String[] {}));
		checkSIB(sibId, sib, inputList, branchMap);

		Type type = sib.getIteratedType();
		if (type == null) {
			addError(sibId, "reference not found / is null");
			return;
		}

		// check input
		for (Input input : sib.getInputs()) {
			if ("list".equals(input.getName())) {
				if (input instanceof ComplexInputPort == false) {
					addError(sibId, "input port type incorrect");
					continue;
				}
				ComplexInputPort complexInput = (ComplexInputPort) input;
				if (!complexInput.getDataType().getId().equals(type.getId()))
					addError(sibId, "input port type incorrect");
				if (!complexInput.isIsList())
					addError(sibId, "input port must be a list");
			}
		}
		// check output
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if ("next".equals(branch.getName())) {
				for (Output output : branch.getOutputs()) {
					if ("element".equals(output.getName())) {
						if (output instanceof ComplexOutputPort == false) {
							addError(sibId, "output port type incorrect");
							continue;
						}
						ComplexOutputPort complexOutput = (ComplexOutputPort) output;
						if (!complexOutput.getDataType().getId().equals(type.getId()))
							addError(sibId, "output port type incorrect");
						if (complexOutput.isIsList())
							addError(sibId, "output port must not be a list");
					}
				}
			}
		}
	}

	private void checkCreateSIB(ProcessId sibId, CreateSIB sib) {
		List<String> inputList = new LinkedList<String>();
		inputList.addAll(sib.getInputs().stream().map(n -> n.getName()).collect(Collectors.toList()));
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("success", (new String[] { "created" }));
		checkSIB(sibId, sib, inputList.toArray(new String[inputList.size()]), branchMap, false, true, true);

		ConcreteType type = sib.getCreatedType();

		List<Attribute> attributes = _dataExtension.getInheritedAttributes(type);
		for (Input input : sib.getInputs()) {
			if ("internalName".equals(input.getName()))
				continue;

			boolean found = false;
			for (Attribute attribute : attributes) {
				if (!input.getName().equals(attribute.getName()))
					continue;

				if (input instanceof ComplexInputPort && attribute instanceof ComplexAttribute) {
					ComplexInputPort complexInput = (ComplexInputPort) input;
					ComplexAttribute complexAttr = (ComplexAttribute) attribute;
					found = true;
					if (!complexInput.getDataType().getId().equals(complexAttr.getDataType().getId()))
						addError(sibId,
								"input '" + input.getName() + "' has wrong type dependent on attribute. should be '"
										+ complexAttr.getDataType().getName() + "'");
					if (complexInput.isIsList() != complexAttr.isIsList())
						addError(sibId,
								"input '" + input.getName()
										+ "' has wrong list state dependent on attribute. should be '"
										+ complexAttr.isIsList() + "'");
				} else if (input instanceof PrimitiveInputPort && attribute instanceof PrimitiveAttribute) {
					PrimitiveInputPort primitiveInput = (PrimitiveInputPort) input;
					PrimitiveAttribute primitiveAttr = (PrimitiveAttribute) attribute;
					found = true;
					if (!primitiveInput.getDataType().getLiteral().equals(primitiveAttr.getDataType().getLiteral()))
						addError(sibId,
								"input '" + input.getName() + "' has wrong type dependent on attribute. should be '"
										+ primitiveAttr.getDataType().getLiteral() + "'");
					if (primitiveInput.isIsList() != primitiveAttr.isIsList())
						addError(sibId,
								"input '" + input.getName()
										+ "' has wrong list state dependent on attribute. should be '"
										+ primitiveAttr.isIsList() + "'");
				} else if (input instanceof InputStatic && attribute instanceof PrimitiveAttribute) {
					PrimitiveAttribute primitiveAttr = (PrimitiveAttribute) attribute;
					found = true;
					if (!processUtils.getStaticTypeString(input).equals(primitiveAttr.getDataType().getLiteral()))
						addError(sibId,
								"input '" + input.getName() + "' has wrong type dependent on attribute. should be '"
										+ primitiveAttr.getDataType().getLiteral() + "'");
					if (false != primitiveAttr.isIsList())
						addError(sibId,
								"input '" + input.getName()
										+ "' has wrong list state dependent on attribute. should be '"
										+ primitiveAttr.isIsList() + "'");
				} else {
					addError(sibId, "intput '" + input.getName() + "' and attribute have completly different types");
				}
			}
			if (!found)
				addError(sibId, "attribute for input '" + input.getName() + "' not found");
		}
	}

	private void checkCreateUserSIB(ProcessId sibId, CreateUserSIB sib) {
		String[] inputList = new String[] { "internalName" };
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("success", (new String[] { "created" }));
		checkSIB(sibId, sib, inputList, branchMap, false, true, true);

		UserType type = sib.getCreatedType();

		List<Attribute> attributes = _dataExtension.getInheritedAttributes(type);
		for (Input input : sib.getInputs()) {
			if ("internalName".equals(input.getName()))
				continue;

			boolean found = false;
			for (Attribute attribute : attributes) {
				if (!input.getName().equals(attribute.getName()))
					continue;

				if (input instanceof ComplexInputPort && attribute instanceof ComplexAttribute) {
					ComplexInputPort complexInput = (ComplexInputPort) input;
					ComplexAttribute complexAttr = (ComplexAttribute) attribute;
					found = true;
					if (!complexInput.getDataType().getId().equals(complexAttr.getDataType().getId()))
						addError(sibId, "input '" + input.getName() + "' has wrong type. should be '"
								+ complexAttr.getDataType().getName() + "'");
					if (complexInput.isIsList() != complexAttr.isIsList())
						addError(sibId, "input '" + input.getName() + "' has wrong list state. should be '"
								+ complexAttr.isIsList() + "'");
				} else if (input instanceof PrimitiveInputPort && attribute instanceof PrimitiveAttribute) {
					PrimitiveInputPort primitiveInput = (PrimitiveInputPort) input;
					PrimitiveAttribute primitiveAttr = (PrimitiveAttribute) attribute;
					found = true;
					if (!primitiveInput.getDataType().getLiteral().equals(primitiveAttr.getDataType().getLiteral()))
						addError(sibId, "input '" + input.getName() + "' has wrong type. should be '"
								+ primitiveAttr.getDataType().getLiteral() + "'");
					if (primitiveInput.isIsList() != primitiveAttr.isIsList())
						addError(sibId, "input '" + input.getName() + "' has wrong list state. should be '"
								+ primitiveAttr.isIsList() + "'");
				} else if (input instanceof InputStatic && attribute instanceof PrimitiveAttribute) {
					PrimitiveAttribute primitiveAttr = (PrimitiveAttribute) attribute;
					found = true;
					if (!processUtils.getStaticTypeString(input).equals(primitiveAttr.getDataType().getLiteral()))
						addError(sibId, "input '" + input.getName() + "' has wrong type. should be '"
								+ primitiveAttr.getDataType().getLiteral() + "'");
					if (false != primitiveAttr.isIsList())
						addError(sibId, "input '" + input.getName() + "' has wrong list state. should be '"
								+ primitiveAttr.isIsList() + "'");
				} else {
					addError(sibId, "intput '" + input.getName() + "' and anttribute have completly different types");
				}
			}
			if (!found)
				addError(sibId, "attribute for input '" + input.getName() + "' not found");
		}
	}

	private void checkDeleteSIB(ProcessId sibId, DeleteSIB sib) {
		String[] inputList = new String[] {};
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("deleted", (new String[] {}));
		checkSIB(sibId, sib, inputList, branchMap, false, true, true);

		for (Input input : sib.getInputs()) {
			if (input instanceof ComplexInputPort == false)
				addError(sibId, "input port type incorrect");
		}
	}

	private void checkRemoveFromListSIB(ProcessId sibId, RemoveFromListSIB sib) {
		String[] inputList = new String[] { "list", "element" };
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("removed", (new String[] {}));
		ProcessExtension pe = new ProcessExtension();
		if (!pe.isIgnoredBranch(sib, "not found")) {
			branchMap.put("not found", (new String[] {}));
		}
		checkSIB(sibId, sib, inputList, branchMap, true, true, true);

		Type type = sib.getListType();
		for (Input input : sib.getInputs()) {
			String portName = input.getName();
			if (input instanceof ComplexInputPort == false) {
				addError(sibId, "input port '" + portName + "' is not complex");
				continue;
			}

			ComplexInputPort cInput = (ComplexInputPort) input;

			if (!cInput.getDataType().getId().equals(type.getId()))
				addError(sibId, "input port '" + portName + "' type incorrect");

			if (input.getName().equals("list")) {
				if (!cInput.isIsList())
					addError(sibId, "input port '" + portName + "' is not a list");
				if (cInput.getIncoming(DataFlow.class).isEmpty())
					addError(sibId, "input port '" + portName + "' must be connected");
			}

			if (input.getName().equals("element")) {
				if (cInput.isIsList())
					addError(sibId, "input port '" + portName + "' is a list");
			}
		}
	}

	private void checkRetrieveOfTypeSIB(ProcessId sibId, RetrieveOfTypeSIB sib) {
		String[] inputList = new String[] {};
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("success", (new String[] { "retrieved" }));
		ProcessExtension pe = new ProcessExtension();
		if (!pe.isIgnoredBranch(sib, "none found")) {
			branchMap.put("none found", (new String[] {}));
		}
		List<String> names = sib.getInputs().stream().map(n -> n.getName()).collect(Collectors.toList());
		inputList = names.toArray(new String[names.size()]);
		checkSIB(sibId, sib, inputList, branchMap);

		Type type = sib.getRetrievedType();
		if (type == null) {
			addError(sibId, "reference not found / is null");
			return;
		}
		// Check criteria
		List<Attribute> attributes = _dataExtension.getInheritedAttributes(type);
		for (Input input : sib.getInputs()) {

			boolean found = false;
			for (Attribute attribute : attributes) {
				if (!input.getName().equals(attribute.getName()))
					continue;

				if (input instanceof ComplexInputPort && attribute instanceof ComplexAttribute) {
					ComplexInputPort complexInput = (ComplexInputPort) input;
					ComplexAttribute complexAttr = (ComplexAttribute) attribute;
					found = true;
					if (!complexInput.getDataType().getId().equals(complexAttr.getDataType().getId()))
						addError(sibId, "input '" + input.getName() + "' has wrong type. should be '"
								+ complexAttr.getDataType().getName() + "'");
					if (complexInput.isIsList() != complexAttr.isIsList())
						addError(sibId, "input '" + input.getName() + "' has wrong list state. should be '"
								+ complexAttr.isIsList() + "'");
				} else if (input instanceof PrimitiveInputPort && attribute instanceof PrimitiveAttribute) {
					PrimitiveInputPort primitiveInput = (PrimitiveInputPort) input;
					PrimitiveAttribute primitiveAttr = (PrimitiveAttribute) attribute;
					found = true;
					if (!primitiveInput.getDataType().getLiteral().equals(primitiveAttr.getDataType().getLiteral()))
						addError(sibId, "input '" + input.getName() + "' has wrong type. should be '"
								+ primitiveAttr.getDataType().getLiteral() + "'");
					if (primitiveInput.isIsList() != primitiveAttr.isIsList())
						addError(sibId, "input '" + input.getName() + "' has wrong list state. should be '"
								+ primitiveAttr.isIsList() + "'");
				} else if (input instanceof InputStatic && attribute instanceof PrimitiveAttribute) {
					PrimitiveAttribute primitiveAttr = (PrimitiveAttribute) attribute;
					found = true;
					if (!processUtils.getStaticTypeString(input).equals(primitiveAttr.getDataType().getLiteral()))
						addError(sibId, "input '" + input.getName() + "' has wrong type. should be '"
								+ primitiveAttr.getDataType().getLiteral() + "'");
					if (false != primitiveAttr.isIsList())
						addError(sibId, "input '" + input.getName() + "' has wrong list state. should be '"
								+ primitiveAttr.isIsList() + "'");
				} else {
					addError(sibId, "intput '" + input.getName() + "' and attribute have completly different types");
				}
			}
			if (!found)
				addError(sibId, "attribute for input '" + input.getName() + "' not found");
		}

		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if ("success".equals(branch.getName())) {
				for (Output output : branch.getOutputs()) {
					if ("retrieved".equals(output.getName())) {
						if (output instanceof ComplexOutputPort == false) {
							addError(sibId, "output port type incorrect");
							continue;
						}
						ComplexOutputPort complexOutput = (ComplexOutputPort) output;
						if (!complexOutput.getDataType().getId().equals(type.getId()))
							addError(sibId, "output port type incorrect");
					}
				}
			}
		}
	}

	private void checkPutToContextSIB(ProcessId sibId, PutToContextSIB sib) {
		String[] inputList = new String[] {};
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("success", (new String[] {}));
		checkSIB(sibId, sib, inputList, branchMap, false, true, false);

		// collect inputs
		HashMap<String, Input> inputs = new HashMap<String, Input>();
		for (Input input : sib.getInputs()) {
			inputs.put(input.getName(), input);
		}

		// get success branch
		Branch successBranch = null;
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if (branch.getName().equals("success"))
				successBranch = branch;
		}
		if (successBranch == null)
			return;

		// collect outputs
		HashMap<String, Output> outputs = new HashMap<String, Output>();
		for (Output output : successBranch.getOutputs()) {
			outputs.put(output.getName(), output);
		}

		// check missing outputs
		ArrayList<String> missingOutputs = new ArrayList<String>(inputs.keySet());
		missingOutputs.removeAll(outputs.keySet());
		for (String name : missingOutputs) {
			addError(adapter.getIdByString(successBranch.getId()), "missing output '" + name + "'");
		}

		// check missing inputs
		ArrayList<String> missingInputs = new ArrayList<String>(outputs.keySet());
		missingInputs.removeAll(inputs.keySet());
		for (String name : missingInputs) {
			addError(adapter.getIdByString(successBranch.getId()), "missing input '" + name + "'");
		}

		// check existing ports
		ArrayList<String> checkIOs = new ArrayList<String>(inputs.keySet());
		checkIOs.retainAll(outputs.keySet());
		for (String name : checkIOs) {
			Input input = inputs.get(name);
			Output output = outputs.get(name);
			checkTypeOfIO(adapter.getIdByString(output.getId()), input, output);
		}
	}

	private void checkPutComplexToContextSIB(ProcessId sibId, PutComplexToContextSIB sib) {
		Type type = sib.getPutType();
		if (type == null) {
			addError(sibId, "reference not found / is null");
			return;
		}

		// check input
		List<InputPort> inputPorts = sib.getInputPorts();
		for (InputPort inputPort : inputPorts) {
			if (inputPort instanceof ComplexInputPort == false) {
				addError(sibId, "input port type incorrect");
				continue;
			}
		}
		// check output
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if ("success".equals(branch.getName())) {
				List<OutputPort> outputPorts = branch.getOutputPorts();
				boolean exactlyOnePort = (inputPorts.size() == 1 && outputPorts.size() == 1);
				for (OutputPort outputPort : outputPorts) {
					if (outputPort instanceof ComplexOutputPort == false) {
						addError(sibId, "output port type incorrect");
						continue;
					}
					InputPort inputPort = null;
					if (exactlyOnePort) {
						inputPort = inputPorts.get(0);
					} else {
						Optional<InputPort> optInput = inputPorts.stream().filter((Input input) -> {
							String name = input.getName();
							return name != null && name.equals(outputPort.getName());
						}).findAny();
						if (!optInput.isPresent()) {
							addError(sibId, "No input found for ouput name '" + outputPort.getName() + "'");
							continue;
						} else {
							inputPort = optInput.get();
						}
					}
					if (inputPort.isIsList() != outputPort.isIsList()) {
						addError(sibId, "Input and output have different list states");
						continue;
					}
					if (((ComplexInputPort) inputPort).getDataType() != ((ComplexOutputPort) outputPort)
							.getDataType()) {
						addError(sibId, "Input and output have different data types");
						continue;
					}
				}
			}
		}
	}

	private void checkPutPrimitiveToContext(ProcessId sibId, PutPrimitiveToContextSIB sib) {
		String[] inputList = new String[] { "in" };
		HashMap<String, String[]> branchMap = new HashMap<String, String[]>();
		branchMap.put("success", (new String[] { "out" }));
		checkSIB(sibId, sib, inputList, branchMap);

		PrimitiveInputPort targetInput = null;
		PrimitiveOutputPort targetOutput = null;
		// check input
		for (Input input : sib.getInputs()) {
			if ("in".equals(input.getName())) {
				if (input instanceof PrimitiveInputPort == false) {
					addError(sibId, "input port type incorrect");
					continue;
				}
				targetInput = (PrimitiveInputPort) input;
			}
		}
		// check output
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if ("success".equals(branch.getName())) {
				for (Output output : branch.getOutputs()) {
					if ("out".equals(output.getName())) {
						if (output instanceof PrimitiveOutputPort == false) {
							addError(sibId, "output port type incorrect");
							continue;
						}
						targetOutput = (PrimitiveOutputPort) output;
					}
				}
			}
		}

		if (targetInput == null || targetOutput == null) {
			addError(sibId, "could not identify ports properly");
			return;
		}

		// check list state
		if (targetInput.isIsList() != targetOutput.isIsList())
			addError(sibId, "in and out have different list state");
		// check type
		if (!targetInput.getDataType().equals(targetOutput.getDataType()))
			addError(sibId, "in and out have different primitive types");
	}

	private void checkSIB(ProcessId sibId, DataFlowTarget dft, String[] inputList,
			HashMap<String, String[]> branchMap) {
		checkSIB(sibId, dft, inputList, branchMap, true, true, true);
	}

	private void checkSIB(ProcessId sibId, DataFlowTarget dft, String[] inputList, HashMap<String, String[]> branchMap,
			boolean exlusiveInputs, boolean exclusiveBranches, boolean exclusiveOutputs) {

		/*
		 * Checking types of ooutputports: refereneced type has to be equal to 'created'
		 * type
		 */
		if (dft instanceof CreateSIB) {
			CreateSIB sib = (CreateSIB) dft;
			ConcreteType referencedType = sib.getCreatedType();
			for (Branch currentBranch : dft.getSuccessors(Branch.class)) {
				if (currentBranch.getName().equals("success")) {
					EList<ComplexOutputPort> ports = currentBranch.getComplexOutputPorts();
					for (ComplexOutputPort port : ports) {
						if (port.getName().equals("created")) {
							Type createdDatatype = port.getDataType();
							if (createdDatatype == null) {
								addError(sibId, "type for '" + sib.getLabel() + "' does not exist");
								return;
							}
							if (!createdDatatype.equals(referencedType)) {
								addError(sibId, "type mismatch: 'created' port of '" + sib.getLabel()
										+ "' has to be of type '" + referencedType.getName() + "'");
							}
						}
					}
				}
			}
		}

		/*
		 * Checking types of ooutputports: refereneced type has to be equal to 'created'
		 * type
		 */
		if (dft instanceof CreateSIB) {
			CreateSIB sib = (CreateSIB) dft;
			ConcreteType referencedType = sib.getCreatedType();
			for (Branch currentBranch : dft.getSuccessors(Branch.class)) {
				if (currentBranch.getName().equals("success")) {
					EList<ComplexOutputPort> ports = currentBranch.getComplexOutputPorts();
					for (ComplexOutputPort port : ports) {
						if (port.getName().equals("created")) {
							Type createdDatatype = port.getDataType();
							if (!createdDatatype.equals(referencedType)) {
								addError(sibId, "type mismatch: 'created' port of '" + sib.getLabel()
										+ "' has to be of type '" + referencedType.getName() + "'");
							}
						}
					}
				}
			}
		}

		/*
		 * Checking inputs
		 */
		ArrayList<String> inputs = new ArrayList<>(Arrays.asList(inputList));
		for (Input input : dft.getInputs()) {
			if (!Arrays.asList(inputList).contains(input.getName()) && exlusiveInputs)
				addError(sibId, "unallowed input '" + input.getName() + "'");
			inputs.remove(input.getName());
		}
		for (String inputName : inputs) {
			addError(sibId, "missing input '" + inputName + "'");
		}
		// Check duplicated input Names
		List<String> portNames = new LinkedList<String>();
		for (Input input : dft.getInputs()) {
			if (portNames.contains(input.getName())) {
				addError(sibId, "duplicated input name '" + input.getName() + "'");
			} else {
				portNames.add(input.getName());
			}
		}

		/*
		 * Checking branch
		 */
		ArrayList<String> branches = new ArrayList<>(branchMap.keySet());
		for (Branch branch : dft.getSuccessors(Branch.class)) {
			if (!branchMap.keySet().contains(branch.getName())) {
				if (exclusiveBranches)
					addError(sibId, "unallowed branch '" + branch.getName() + "'");
			} else {
				/*
				 * Checkiong outputs
				 */
				ArrayList<String> outputs = new ArrayList<>(Arrays.asList(branchMap.get(branch.getName())));
				for (Output output : branch.getOutputs()) {
					if (!Arrays.asList(branchMap.get(branch.getName())).contains(output.getName())
							&& exclusiveOutputs) {
						addError(sibId, "unallowed output '" + output.getName() + "'");
					}
					outputs.remove(output.getName());
				}
				for (String outputName : outputs) {
					addError(sibId, "missing output '" + outputName + "'");
				}
			}
			branches.remove(branch.getName());
		}
		for (String branchName : branches) {
			addError(sibId, "missing branch '" + branchName + "'");
		}
	}

	private void checkTypeOfIO(ProcessId thisId, IO thisIO, IO referencedIO) {
		Object thisType = null;
		Object refType = null;

		if (thisIO instanceof PrimitiveOutputPort || thisIO instanceof PrimitiveInputPort)
			thisType = processUtils.getPrimitiveTypeString(thisIO);
		else if (thisIO instanceof ComplexOutputPort || thisIO instanceof ComplexInputPort)
			thisType = processUtils.getComplexType(thisIO);
		else if (thisIO instanceof JavaNativeOutputPort || thisIO instanceof JavaNativeInputPort)
			thisType = processUtils.getNativeType(thisIO);
		else if (thisIO instanceof InputStatic)
			thisType = processUtils.getStaticTypeString(thisIO);
		else
			throw new RuntimeException("Could not find port type");

		if (referencedIO instanceof PrimitiveOutputPort || referencedIO instanceof PrimitiveInputPort)
			refType = processUtils.getPrimitiveTypeString(referencedIO);
		else if (referencedIO instanceof ComplexOutputPort || referencedIO instanceof ComplexInputPort)
			refType = processUtils.getComplexType(referencedIO);
		else if (referencedIO instanceof JavaNativeOutputPort || referencedIO instanceof JavaNativeInputPort)
			refType = processUtils.getNativeType(referencedIO);
		else if (referencedIO instanceof InputStatic)
			refType = processUtils.getStaticTypeString(referencedIO);
		else
			throw new RuntimeException("Could not find port type");

		if (thisType instanceof String && refType instanceof String) {
			if (!((String) thisType).equals(refType))
				addError(thisId, "Type mismatch, should be '" + (String) refType + "'");
		} else if (thisType instanceof Type && refType instanceof Type) {
			if (!((Type) thisType).getId().equals(((Type) refType).getId()))
				addError(thisId, "Type mismatch, should be '" + ((Type) refType).getName() + "'");
		} else {
			addError(thisId, "Type mismatch, should be '" + refType.getClass().getSimpleName() + "'");
		}

		if (processUtils.isListType(thisIO) != processUtils.isListType(referencedIO))
			addError(thisId, "isList property mismatch, should be " + processUtils.isListType(referencedIO));
	}
}
