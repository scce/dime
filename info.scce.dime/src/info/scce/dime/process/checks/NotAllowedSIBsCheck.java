/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.AtomicSIB;
import info.scce.dime.process.process.CreateSIB;
import info.scce.dime.process.process.CreateUserSIB;
import info.scce.dime.process.process.DeleteSIB;
import info.scce.dime.process.process.EnumSwitchSIB;
import info.scce.dime.process.process.GUISIB;
import info.scce.dime.process.process.IsOfTypeSIB;
import info.scce.dime.process.process.IterateSIB;
import info.scce.dime.process.process.JavaNativeVariable;
import info.scce.dime.process.process.NativeFrontendSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessBlueprintSIB;
import info.scce.dime.process.process.ProcessSIB;
import info.scce.dime.process.process.ProcessType;
import info.scce.dime.process.process.PutComplexToContextSIB;
import info.scce.dime.process.process.PutPrimitiveToContextSIB;
import info.scce.dime.process.process.PutToContextSIB;
import info.scce.dime.process.process.RemoveFromListSIB;
import info.scce.dime.process.process.RetrieveCurrentUserSIB;
import info.scce.dime.process.process.RetrieveEnumLiteralSIB;
import info.scce.dime.process.process.RetrieveOfTypeSIB;
import info.scce.dime.process.process.SearchSIB;
import info.scce.dime.process.process.SetAttributeValueSIB;
import info.scce.dime.process.process.UnsetAttributeValueSIB;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

public class NotAllowedSIBsCheck extends AbstractCheck<ProcessId, ProcessAdapter> {
	
	public enum sibType {
		eventSib, unknown, varNative, processBasic, processSecurity, guiFileSecurity, processLongRunning, sibFork, sibJoin, sibSaveDB, sibSearch, sibCreate, sibCreateUser, sibIterate, sibSetAttributeValue, sibUnsetAttributeValue, sibAtomic, sibGui, sibEnumSwitch, sibPutComplexToContext, sibPutPrimitiveToContext, sibPutToContext, sibRetrieveAll, sibRetrieveCurrentUser, sibDelete, sibRemoveFromList, sibRetrieveEnumLiteral , processNativeFrontend, nativeFrontend, sibIsOfType
	}

	private ArrayList<sibType> allTypes = new ArrayList<>();
	private ArrayList<sibType> deprecatedTypes = new ArrayList<>();

	private ArrayList<sibType> basicTypes = new ArrayList<>();
	private ArrayList<sibType> securityTypes = new ArrayList<>();
	private ArrayList<sibType> longrunningTypes = new ArrayList<>();
	private Set<sibType> asynchronousTypes = new HashSet<>();
	private ArrayList<sibType> fileSecurityTypes = new ArrayList<>();

	private ArrayList<sibType> nativeFrontendTypes = new ArrayList<>();


	public NotAllowedSIBsCheck() {
		super();
	}

	private void initSibTypeArrays() {
		allTypes = new ArrayList<>();
		for (sibType sibType : sibType.values()) {
			allTypes.add(sibType);
		}

		// init deprecated
		deprecatedTypes = new ArrayList<>();
		deprecatedTypes.add(sibType.sibSaveDB);
		deprecatedTypes.add(sibType.sibPutPrimitiveToContext);

		// init basic
		basicTypes = new ArrayList<>();
		basicTypes.addAll(allTypes);
		basicTypes.remove(sibType.sibSaveDB);
		basicTypes.remove(sibType.guiFileSecurity);

		// init security
		securityTypes = new ArrayList<>();
		securityTypes.addAll(allTypes);
		securityTypes.remove(sibType.sibSaveDB);
		securityTypes.remove(sibType.sibGui);
		securityTypes.remove(sibType.guiFileSecurity);
		securityTypes.remove(sibType.eventSib);
		
		// init file security
		fileSecurityTypes = new ArrayList<>();
		fileSecurityTypes.addAll(allTypes);
		fileSecurityTypes.remove(sibType.sibSaveDB);
		fileSecurityTypes.remove(sibType.sibGui);
		fileSecurityTypes.remove(sibType.eventSib);

		// init longrunning
		longrunningTypes = new ArrayList<>();
		longrunningTypes.addAll(allTypes);
		longrunningTypes.remove(sibType.varNative);
		longrunningTypes.remove(sibType.sibSaveDB);
		longrunningTypes.remove(sibType.sibGui);
		longrunningTypes.remove(sibType.guiFileSecurity);
		longrunningTypes.remove(sibType.eventSib);
		
		// init asynchronous
		asynchronousTypes = new HashSet<>();
		asynchronousTypes.addAll(allTypes);
		asynchronousTypes.remove(sibType.sibSaveDB);
		asynchronousTypes.remove(sibType.sibGui);
		asynchronousTypes.remove(sibType.guiFileSecurity);
		asynchronousTypes.remove(sibType.eventSib);
		
		// init nativeFrontend
		nativeFrontendTypes = new ArrayList<>();
		nativeFrontendTypes.add(sibType.nativeFrontend);
	}
	
	@Override
	public void init() {
		initSibTypeArrays();
	}

	@Override
	public void doExecute(ProcessAdapter adapter) {
		ProcessType pType = adapter.getModel().getProcessType();

		for (ProcessId id : adapter.getEntityIds()) {
			EObject obj = id.getElement();
			sibType sType = mapEObjectToSibType(obj);
			if (sType != sibType.unknown) {
				switch (pType) {
				case BASIC:
					if (!basicTypes.contains(sType))
						addError(id, " not allowed in process type "
								+ pType.getLiteral());
					break;
				case LONG_RUNNING:
					if (!longrunningTypes.contains(sType))
						addError(id, " not allowed in process type "
								+ pType.getLiteral());
					break;
				case ASYNCHRONOUS:
					if(!asynchronousTypes.contains(sType))
						addError(id, " not allowed in process type "
								+ pType.getLiteral());
					break;
				case SECURITY:
					if (!securityTypes.contains(sType))
						addError(id, " not allowed in process type "
								+ pType.getLiteral());
					break;
				case FILE_DOWNLOAD_SECURITY:
					if (!fileSecurityTypes.contains(sType))
						addError(id, " not allowed in process type "
                                                                + pType.getLiteral());
                                        break;
				case NATIVE_FRONTEND_SIB_LIBRARY:
					if(!nativeFrontendTypes.contains(sType))
						addError(id, " not allowed in process type "
								+ pType.getLiteral());
					break;
				case UNSPECIFIED:
				default:
					break;
				}

				if (deprecatedTypes.contains(sType)) 
					addWarning(id, " is deprecated");
			}
		}
	}

	private sibType mapEObjectToSibType(EObject eObj) {
		if (eObj instanceof ProcessSIB) {
			Process process = ((ProcessSIB) eObj).getProMod();
			if (process == null) {
				System.err.println("Process is null for ProcessSIB " + eObj);
				return sibType.unknown;
			} else switch (process.getProcessType()) {
			case BASIC:
				return sibType.processBasic;
			case LONG_RUNNING:
				return sibType.processLongRunning;
			case SECURITY:
				return sibType.processSecurity;
			case FILE_DOWNLOAD_SECURITY:
				return sibType.guiFileSecurity;
			case NATIVE_FRONTEND_SIB_LIBRARY:
				return sibType.processNativeFrontend;
			default:
				return sibType.unknown;
			}
		}

		if (eObj instanceof ProcessBlueprintSIB) {
			switch (((ProcessBlueprintSIB) eObj).getProcessType()) {
			case BASIC:
				return sibType.processBasic;
			case LONG_RUNNING:
				return sibType.processLongRunning;
			case SECURITY:
				return sibType.processSecurity;
			case FILE_DOWNLOAD_SECURITY:
				return sibType.guiFileSecurity;
			default:
				return sibType.unknown;
			}
		}


		if (eObj instanceof SearchSIB)
			return sibType.sibSearch;

		if (eObj instanceof CreateSIB)
			return sibType.sibCreate;
		if (eObj instanceof CreateUserSIB)
			return sibType.sibCreateUser;

		if (eObj instanceof IterateSIB)
			return sibType.sibIterate;

		if (eObj instanceof SetAttributeValueSIB)
			return sibType.sibSetAttributeValue;
		if (eObj instanceof UnsetAttributeValueSIB)
			return sibType.sibUnsetAttributeValue;

		if (eObj instanceof NativeFrontendSIB){
			return sibType.nativeFrontend;
		}
		if (eObj instanceof GUISIB)
			return sibType.sibGui;
		if (eObj instanceof AtomicSIB)
			return sibType.sibAtomic;

		if (eObj instanceof EnumSwitchSIB)
			return sibType.sibEnumSwitch;

		if (eObj instanceof PutComplexToContextSIB)
			return sibType.sibPutComplexToContext;
		if (eObj instanceof PutPrimitiveToContextSIB)
			return sibType.sibPutPrimitiveToContext;
		if (eObj instanceof PutToContextSIB)
			return sibType.sibPutToContext;

		if (eObj instanceof RetrieveOfTypeSIB)
			return sibType.sibRetrieveAll;
		if (eObj instanceof RetrieveCurrentUserSIB)
			return sibType.sibRetrieveCurrentUser;

		if (eObj instanceof DeleteSIB)
			return sibType.sibDelete;

		if (eObj instanceof RemoveFromListSIB)
			return sibType.sibRemoveFromList;

		if (eObj instanceof RetrieveEnumLiteralSIB)
			return sibType.sibRetrieveEnumLiteral;

		if (eObj instanceof JavaNativeVariable)
			return sibType.varNative;
		
		if (eObj instanceof IsOfTypeSIB)
			return sibType.sibIsOfType;

		return sibType.unknown;
	}
}
