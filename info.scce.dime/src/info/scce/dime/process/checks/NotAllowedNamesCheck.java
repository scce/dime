/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.Variable;

import java.util.Arrays;

public class NotAllowedNamesCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	private String[] notAllowedVariableNames = {"var"};
	
	private String[] notAllowedEndSibPortNames = {"var", "iteratorElement"};
	
	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Variable) {
				Variable var = (Variable) obj;
				if (Arrays.asList(notAllowedVariableNames).contains(var.getName()))
					addError(id, var.getName()
							+ " not allowed");
			}
			if (obj instanceof EndSIB) {
				EndSIB endSib = (EndSIB) obj;
				for (Input input : endSib.getInputs()) {
					if (Arrays.asList(notAllowedEndSibPortNames).contains(input.getName()))
						addError(id, input.getName()
								+ " not allowed");
				}
			}
		}
	}
	
	@Override
	public void init() {}

}
