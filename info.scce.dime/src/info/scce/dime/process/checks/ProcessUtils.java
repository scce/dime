/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import graphmodel.Node;
import info.scce.dime.data.data.Type;
import info.scce.dime.process.helper.ProcessExtension;
import info.scce.dime.process.process.BooleanInputStatic;
import info.scce.dime.process.process.ComplexAttribute;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexListAttribute;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.InputPort;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.IntegerInputStatic;
import info.scce.dime.process.process.JavaNativeInputPort;
import info.scce.dime.process.process.JavaNativeOutputPort;
import info.scce.dime.process.process.JavaNativeVariable;
import info.scce.dime.process.process.OutputPort;
import info.scce.dime.process.process.PrimitiveAttribute;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveListAttribute;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.process.process.RealInputStatic;
import info.scce.dime.process.process.SIB;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;
import info.scce.dime.process.process.Variable;

import java.util.regex.Pattern;

public class ProcessUtils {
	
	public static boolean isIgnoredBranch(SIB sib, String branchName) {
		return new ProcessExtension().isIgnoredBranch(sib, branchName);
	}

	// TODO: moved to ProcessExtension
	public boolean isListType(Node node) {
		if (node instanceof InputPort) {
			return ((InputPort) node).isIsList();
		} else if (node instanceof OutputPort) {
			return ((OutputPort) node).isIsList();
		} else if (node instanceof Variable) {
			return ((Variable) node).isIsList();
		} else if (node instanceof PrimitiveAttribute) {
			info.scce.dime.data.data.PrimitiveAttribute pAttr = ((PrimitiveAttribute) node)
					.getAttribute();
			return pAttr.isIsList();
		} else if (node instanceof ComplexAttribute) {
			ComplexAttribute cAttr1 = (ComplexAttribute) node;
			info.scce.dime.data.data.ComplexAttribute cAttr2 = cAttr1
					.getAttribute();
			return cAttr2.isIsList();
		} else if (node instanceof PrimitiveListAttribute) {
			return false;
		} else if (node instanceof ComplexListAttribute) {
			return false;
		} else if (node instanceof InputStatic) {
			return false;
		} else
			throw new RuntimeException("Class unknown of " + node);
	}
	
	// TODO: moved to ProcessExtension
	public String getPrimitiveTypeString(Node node) {
		if (node instanceof PrimitiveAttribute) {
			info.scce.dime.data.data.PrimitiveAttribute pAttr = ((PrimitiveAttribute) node)
					.getAttribute();
			return pAttr.getDataType().getLiteral();
		} else if (node instanceof PrimitiveListAttribute) {
			return ((PrimitiveListAttribute) node).getPrimitiveType()
					.getLiteral();
		} else if (node instanceof PrimitiveVariable) {
			PrimitiveVariable pVar = (PrimitiveVariable) node;
			return pVar.getDataType().getLiteral();
		} else if (node instanceof PrimitiveInputPort) {
			PrimitiveInputPort pPort = (PrimitiveInputPort) node;
			return pPort.getDataType().getLiteral();
		} else if (node instanceof PrimitiveOutputPort) {
			PrimitiveOutputPort pPort = (PrimitiveOutputPort) node;
			return pPort.getDataType().getLiteral();
		} else
			throw new RuntimeException("Class unknown of " + node);
	}

	// TODO: moved to ProcessExtension
	public Type getComplexType(Node node) {
		if (node instanceof ComplexAttribute) {
			ComplexAttribute cAttr1 = (ComplexAttribute) node;
			info.scce.dime.data.data.ComplexAttribute cAttr2 = cAttr1
					.getAttribute();
			return cAttr2.getDataType();
		} else if (node instanceof ComplexListAttribute) {
			return ((ComplexListAttribute) node).getListType();
		} else if (node instanceof ComplexVariable) {
			ComplexVariable cVar = (ComplexVariable) node;
			return cVar.getDataType();
		} else if (node instanceof ComplexInputPort) {
			ComplexInputPort cPort = (ComplexInputPort) node;
			return cPort.getDataType();
		} else if (node instanceof ComplexOutputPort) {
			ComplexOutputPort cPort = (ComplexOutputPort) node;
			return cPort.getDataType();
		} else
			throw new RuntimeException("Class unknown of " + node);
	}

	// TODO: moved to ProcessExtension
	public String getNativeType(Node node) {
		if (node instanceof JavaNativeVariable) {
			info.scce.dime.siblibrary.Type type = (info.scce.dime.siblibrary.Type) ((JavaNativeVariable) node)
					.getDataType();
			return type.getName();
		} else if (node instanceof JavaNativeInputPort) {
			info.scce.dime.siblibrary.Type type = (info.scce.dime.siblibrary.Type) ((JavaNativeInputPort) node)
					.getDataType();
			return type.getName();
		} else if (node instanceof JavaNativeOutputPort) {
			info.scce.dime.siblibrary.Type type = (info.scce.dime.siblibrary.Type) ((JavaNativeOutputPort) node)
					.getDataType();
			return type.getName();
		} else
			throw new RuntimeException("Class unknown of " + node);
	}

	public String getStaticTypeString(Node node) {
		if (node instanceof IntegerInputStatic)
			return PrimitiveType.INTEGER.getLiteral();
		else if (node instanceof BooleanInputStatic)
			return PrimitiveType.BOOLEAN.getLiteral();
		else if (node instanceof TextInputStatic)
			return PrimitiveType.TEXT.getLiteral();
		else if (node instanceof RealInputStatic)
			return PrimitiveType.REAL.getLiteral();
		else if (node instanceof TimestampInputStatic)
			return PrimitiveType.TIMESTAMP.getLiteral();
		else
			throw new RuntimeException("Class unknown of " + node);
	}
	
	public Pattern getExpressionPattern() {
		// "^(?'part'(?'name'(?:[^\\.\\[\\]])+)(?'index'\\[[0-9]+\\])?\\.?)*$"
		//Pattern outerPattern = Pattern.compile("^((([^\\.\\[\\]])+)(\\[[0-9]+\\])?\\.?)*?$");
		
		// (([^.[]])+)([[0-9]+])?.?
		return Pattern.compile("(([^\\.\\[\\]])+)(\\[[0-9]+\\])?\\.?");
	}
	
	public Pattern getExpressionStringPattern() {
		return Pattern.compile("(.*?)\\{\\{(.*?)\\}\\}");
	}

}
