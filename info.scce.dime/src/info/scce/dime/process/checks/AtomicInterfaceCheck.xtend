/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks

import info.scce.dime.checks.AbstractCheck
import info.scce.dime.data.data.Type
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.mcam.adapter.ProcessAdapter
import info.scce.dime.process.mcam.adapter.ProcessId
import info.scce.dime.process.process.AtomicSIB
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.siblibrary.DataTypePort
import info.scce.dime.siblibrary.JavaType
import info.scce.dime.siblibrary.JavaTypePort
import info.scce.dime.siblibrary.Port
import info.scce.dime.siblibrary.PrimitivePort
import info.scce.dime.siblibrary.SIB
import java.util.ArrayList

class AtomicInterfaceCheck extends AbstractCheck<ProcessId, ProcessAdapter> {
	ProcessUtils processUtils = new ProcessUtils()
	ProcessAdapter adapter
	extension ProcessExtension = new ProcessExtension

	override void doExecute(ProcessAdapter adapter) {
		this.adapter = adapter
		for (ProcessId id : adapter.getEntityIds()) {
			var Object obj = id.getElement()
			if (obj instanceof AtomicSIB) {
				var AtomicSIB sib = (obj as AtomicSIB)
				checkInputs(id, sib)
				checkBranches(id, sib)
			}
		}
		processResults()
	}

	override void init() {
	}

	def private void checkInputs(ProcessId id, AtomicSIB sib) {
		var ArrayList<Input> inputs = new ArrayList<Input>(sib.getInputs())
		var SIB origSib = (sib.getSib() as SIB)
		if (origSib === null) {
			addError(id, String.format("SIB Reference of '%s' not found.", sib.getLabel()))
			return;
		}
		for (Port port : origSib.getInputPorts()) {
			var int count = 0
			var Input oInput = null
			for (Input input : inputs) {
				if (input.getName().equals(port.getName())) {
					oInput = input
					count++
				}
			}
			if (count > 1) {
				addError(id, '''Input '«»«port.getName()»' exists «count» times''')
			} else if (count <= 0) {
				addError(id, '''Input '«»«port.getName()»' not found''')
			} else {
				checkTypeOfIO(adapter.getIdByString(oInput.getId()), oInput, port)
				inputs.remove(oInput)
			}
		}
		for (Input input : inputs) {
			addError(id, '''Reference for '«»«input.getName()»' not found''')
		}
	}

	def private void checkBranches(ProcessId sibId, AtomicSIB sib) {
		var ArrayList<Branch> branches = new ArrayList<Branch>(sib.getSuccessors(Branch))
		var SIB origSib = (sib.getSib() as SIB)
		if (origSib === null) {
			addError(sibId, String.format("SIB Reference of '%s' not found.", sib.getLabel()))
			return;
		}
		for (info.scce.dime.siblibrary.Branch origBranch : origSib.getBranches()) {
			var int count = 0
			var Branch oBranch = null
			for (Branch branch : branches) {
				if (branch.getName().equals(origBranch.getName())) {
					oBranch = branch
					count++
				}
			}
			if (count > 1) {
				addError(sibId, '''Branch '«origBranch.getName()»' exists «count» times''')
			} else if (count <= 0) {
				if(!sib.isIgnoredBranch(origBranch.getName())) {
					addError(sibId, '''Branch '«origBranch.getName()»' not found''')				
				}
			} else {
				checkOutputs(adapter.getIdByString(oBranch.getId()), oBranch, origBranch)
				branches.remove(oBranch)
			}
		}
		for (Branch branch : branches) {
			addError(sibId, '''Reference for Branch '«»«branch.getName()»' not found''')
		}
	}

	def private void checkOutputs(ProcessId branchId, Branch branch, info.scce.dime.siblibrary.Branch origBranch) {
		var ArrayList<Output> outputs = new ArrayList<Output>(branch.getOutputs())
		for (Port origPort : origBranch.getOutputPorts()) {
			var int count = 0
			var Output oOutput = null
			for (Output output : outputs) {
				if (output.getName().equals(origPort.getName())) {
					oOutput = output
					count++
				}
			}
			if (count > 1) {
				addError(branchId, '''Output '«»«origPort.getName()»' exists «count» times''')
			} else if (count <= 0) {
				addError(branchId, '''Output '«»«origPort.getName()»' not found''')
			} else {
				checkTypeOfIO(adapter.getIdByString(oOutput.getId()), oOutput, origPort)
				outputs.remove(oOutput)
			}
		}
		for (Output output : outputs) {
			addError(branchId, '''Reference for '«»«output.getName()»' not found''')
		}
	}

	def private void checkTypeOfIO(ProcessId thisId, IO thisIO, Port port) {
		var Object thisType = null
		var Object refType = null
		if(thisIO instanceof PrimitiveOutputPort || thisIO instanceof PrimitiveInputPort) thisType = processUtils.
			getPrimitiveTypeString(thisIO) else if(thisIO instanceof ComplexOutputPort ||
			thisIO instanceof ComplexInputPort) thisType = processUtils.
			getComplexType(thisIO) else if(thisIO instanceof JavaNativeOutputPort ||
			thisIO instanceof JavaNativeInputPort) thisType = processUtils.
			getNativeType(thisIO) else if(thisIO instanceof InputStatic) thisType = processUtils.
			getStaticTypeString(thisIO) else throw new RuntimeException("Could not find port type");
		if (port instanceof PrimitivePort)
			refType = ((port as PrimitivePort)).getType().getLiteral()
		else if (port instanceof JavaTypePort) {
			var JavaTypePort tPort = (port as JavaTypePort)
			if (tPort.getType() instanceof JavaType) {
				var JavaType jType = (tPort.getType() as JavaType)
				refType = jType.getName()
				if(jType.getName().equals("Object")) return;
			}
		} else if (port instanceof DataTypePort) {
			refType = ((port as DataTypePort)).getType()
		} else
			throw new RuntimeException("Could not find port type");
		if (thisType instanceof String && refType instanceof String) {
			if(!((thisType as String)).equals(refType)) addError(
				thisId, '''Type mismatch, should be '«»«(refType as String)»'«»''')
		} else if (thisType instanceof Type && refType instanceof Type) {
			if(!((thisType as Type)).getId().equals(((refType as Type)).getId())) addError(
				thisId, '''Type mismatch, should be '«»«((refType as Type)).getName()»'«»''')
		} else {
			addError(thisId, '''Type mismatch, should be '«»«refType.getClass().getSimpleName()»'«»''')
		}
		if(processUtils.isListType(thisIO) !== port.isIsList()) addError(
			thisId, '''isList property mismatch, should be «port.isIsList()»''')
	}
}
