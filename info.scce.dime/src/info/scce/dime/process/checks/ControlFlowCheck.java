/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.AbstractBranch;
import info.scce.dime.process.process.BranchConnector;
import info.scce.dime.process.process.ControlFlow;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.GUISIB;
import info.scce.dime.process.process.GuardProcessSIB;
import info.scce.dime.process.process.GuardedProcessSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.SIB;

public class ControlFlowCheck extends AbstractCheck<ProcessId, ProcessAdapter> {
	

	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof AbstractBranch) {
				if (((AbstractBranch) obj).getIncoming(BranchConnector.class)
						.size() <= 0) {
					addError(id, "no branch connector");
				}
			}
			if (obj instanceof DataFlowSource) {
				if (!(((DataFlowSource) obj).getContainer() instanceof Process))
					continue;
				
				if (	((DataFlowSource) obj).getOutgoing(ControlFlow.class)
						.size() <= 0
					) {
					addError(id, "no outgoing control flow");
				}
			}
			if (obj instanceof DataFlowTarget) {
				if (!(((DataFlowTarget) obj).getContainer() instanceof Process))
					continue;
				if (obj instanceof GuardedProcessSIB || obj instanceof GuardProcessSIB)
					continue;
				
				if (((DataFlowTarget) obj).getIncoming(ControlFlow.class)
						.size() <= 0) {
					addWarning(id, "no incoming control flow");
				}
			}
			
		}

		processResults();
	}
	
	@Override
	public void init() {}
	

}
