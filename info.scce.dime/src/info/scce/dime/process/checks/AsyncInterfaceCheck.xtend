/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks

import info.scce.dime.process.mcam.modules.checks.ProcessCheck
import static info.scce.dime.process.process.ProcessType.*
import info.scce.dime.process.process.Process

/**
 * The async interface check tests, whether it is a valid asynchronous process 
 * (i.e., has no complex inputs, at most one end sib with branchname "success", and no outputs). 
 */
class AsyncInterfaceCheck extends ProcessCheck {
	
	override check(Process it) {
		if(processType != ASYNCHRONOUS) return;
		
		startSIBs.map[complexOutputPorts].flatten.forEach [
			addError('''Async processes are not allowed to have complex types in their interface.''')
		]
		
		if (endSIBs.size > 1)
			addError('''Async processes are allowed to have at most one end sib.''')
			
		endSIBs.filter[branchName != "success"].forEach[
			addError('''Async processes' branch has to be named "success". Found: «branchName»''')
		]
		
		endSIBs.map[inputPorts].flatten.forEach [
			addError('''Async processes are not allowed to have outputs''')
		]
	}
	
}
									
