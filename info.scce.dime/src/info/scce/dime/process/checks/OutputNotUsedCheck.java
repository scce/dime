/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.DataFlow;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.Process;

public class OutputNotUsedCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof DataFlowSource) {
				if (!(((DataFlowSource) obj).getContainer() instanceof Process))
					continue;
				for (Output output : ((DataFlowSource) obj).getOutputs()) {
					if (output.getOutgoing(DataFlow.class).size() <= 0)
						addWarning(adapter.getIdByString(output.getId()), "not used");
				}
					
			}
		}
	}
	
	@Override
	public void init() {}
}
