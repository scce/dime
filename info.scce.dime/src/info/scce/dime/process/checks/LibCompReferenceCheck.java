/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class LibCompReferenceCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	@Override
	public void doExecute(ProcessAdapter adapter) {

		EObject eObj = adapter.getModel();

		if (eObj instanceof GraphModel == false)
			return;

		GraphModel gModel = (GraphModel) eObj;

		TreeIterator<EObject> it = gModel.eAllContents();
		while (it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof ModelElement == false)
				continue;
			ModelElement me = (ModelElement) obj;
			EStructuralFeature libCompFeature = me.eClass()
					.getEStructuralFeature("libraryComponentUID");
			if (libCompFeature != null) {
				Object val = me.eGet(libCompFeature);
				if (val == null) {
					addError(adapter.getIdByString(me.getId()),
							"Reference is NULL");
				}
			}
		}
	}
	
	@Override
	public void init() {}
}
