/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.BranchBlueprint;
import info.scce.dime.process.process.ComplexAttributeConnector;
import info.scce.dime.process.process.ComplexListAttributeConnector;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.DataContext;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.EntryPointProcessSIB;
import info.scce.dime.process.process.GenericSIB;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.ProcessBlueprintSIB;
import info.scce.dime.process.process.StartSIB;
import info.scce.dime.process.process.Variable;

public class UniqueNamesCheck extends AbstractCheck<ProcessId, ProcessAdapter> {
	
	private ArrayList<String> contextVarNames = new ArrayList<>();

	@Override
	public void doExecute(ProcessAdapter adapter) {
		Set<String> branchNames = new HashSet<>();
		Set<String> entryPointNames = new HashSet<>();
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof StartSIB)
				checkDFS(adapter, (StartSIB) obj);
			if (obj instanceof EndSIB) {
				EndSIB endSib = (EndSIB) obj;
				checkDFT(adapter, endSib);
				if (!branchNames.add(endSib.getBranchName()))
					addError(id, endSib.getBranchName() + " not unique");
			}
			if (obj instanceof EntryPointProcessSIB) {
				EntryPointProcessSIB entryPointSIB = (EntryPointProcessSIB) obj;
				if (!entryPointNames.add(entryPointSIB.getLabel()))
					addError(id, entryPointSIB.getLabel() + " not unique");
			}
			if (obj instanceof ProcessBlueprintSIB) {
				ProcessBlueprintSIB processBP = (ProcessBlueprintSIB) obj;
				checkDFT(adapter, processBP);
				Set<String> names = new HashSet<>();
				for (BranchBlueprint branchBP : processBP.getSuccessors(BranchBlueprint.class)) {
					if (!names.add(branchBP.getName()))
						addError(adapter.getIdByString(branchBP.getId()), branchBP.getName() + " not unique");
				}
			}
			if (obj instanceof GenericSIB) {
				GenericSIB externSIB = (GenericSIB) obj;
				checkDFT(adapter, externSIB);
				Set<String> names = new HashSet<>();
				for (Branch branch : externSIB.getSuccessors(Branch.class)) {
					if (!names.add(branch.getName()))
						addError(adapter.getIdByString(branch.getId()), branch.getName() + " not unique");
				}
			}
			if (obj instanceof BranchBlueprint)
				checkDFS(adapter, (BranchBlueprint) obj);
			if (obj instanceof DataContext)
				checkDataContext(adapter, (DataContext) obj);
				
		}
	}
	
	@Override
	public void init() {
		contextVarNames.clear();
	}

	private void checkDataContext(ProcessAdapter adapter, DataContext context) {
		for (Variable var : context.getVariables()) {
			if (var instanceof ComplexVariable) {
				if (((ComplexVariable) var).getIncoming(ComplexAttributeConnector.class).size() > 0)
					continue;
				if (((ComplexVariable) var).getIncoming(ComplexListAttributeConnector.class).size() > 0)
					continue;
			}
			if (contextVarNames.contains(var.getName()))
				addError(adapter.getIdByString(var.getId()), var.getName() + " not unique");
			contextVarNames.add(var.getName());
		}
	}

	private void checkDFT(ProcessAdapter adapter, DataFlowTarget sib) {
		ArrayList<String> names = new ArrayList<>();
		for (Input inPort : sib.getInputs()) {
			if (names.contains(inPort.getName()))
				addError(adapter.getIdByString(inPort.getId()), inPort.getName() + " not unique");
			names.add(inPort.getName());
		}
	}

	private void checkDFS(ProcessAdapter adapter, DataFlowSource sib) {
		ArrayList<String> names = new ArrayList<>();
		for (Output outPort : sib.getOutputs()) {
			if (names.contains(outPort.getName()))
				addError(adapter.getIdByString(outPort.getId()), outPort.getName() + " not unique");
			names.add(outPort.getName());
		}
	}
}
