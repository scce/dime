/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import java.util.ArrayList;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.ProcessEntryPointComponent;
import info.scce.dime.data.data.Type;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.IO;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.JavaNativeInputPort;
import info.scce.dime.process.process.JavaNativeOutputPort;
import info.scce.dime.process.process.LinkProcessSIB;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.Process;

public class LinkProcessInterfaceCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	private ProcessUtils processUtils = new ProcessUtils();

	private ProcessAdapter adapter;

	@Override
	public void doExecute(ProcessAdapter adapter) {
		this.adapter = adapter;
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof LinkProcessSIB) {
				LinkProcessSIB sib = (LinkProcessSIB) obj;
				checkInputs(id, sib);
			}
		}
	}
	
	@Override
	public void init() {}


	private void checkInputs(ProcessId id, LinkProcessSIB sib) {
		Process process = null;
		if(sib.getProMod() instanceof ProcessComponent) {
			process = ((ProcessComponent)sib.getProMod()).getModel();
		}
		if(sib.getProMod() instanceof ProcessEntryPointComponent) {
			process = ((ProcessEntryPointComponent)sib.getProMod()).getEntryPoint().getProMod();
		}
		ArrayList<Input> inputs = new ArrayList<Input>(sib.getInputs());
		ArrayList<Output> outputs = new ArrayList<Output>(process
				.getStartSIBs().get(0).getOutputs());

		for (Output output : outputs) {
			int count = 0;
			Input oInput = null;
			for (Input input : inputs) {
				if (input.getName().equals(output.getName())) {
					oInput = input;
					count++;
				}
			}
			if (count > 1) {
				addError(id, "Output '" + output.getName() + "' exists " + count
						+ " times");
			} else if (count <= 0) {
				addWarning(id, "Output '" + output.getName() + "' not found");
			} else {
				checkTypeOfIO(adapter.getIdByString(oInput.getId()), oInput,
						output);
				inputs.remove(oInput);
			}
		}
		for (Input input : inputs) {
			addError(id, "Reference for '" + input.getName() + "' not found");
		}
	}

	private void checkTypeOfIO(ProcessId thisId, IO thisIO, IO referencedIO) {
		Object thisType = null;
		Object refType = null;

		if (thisIO instanceof PrimitiveOutputPort
				|| thisIO instanceof PrimitiveInputPort)
			thisType = processUtils.getPrimitiveTypeString(thisIO);
		else if (thisIO instanceof ComplexOutputPort
				|| thisIO instanceof ComplexInputPort)
			thisType = processUtils.getComplexType(thisIO);
		else if (thisIO instanceof JavaNativeOutputPort
				|| thisIO instanceof JavaNativeInputPort)
			thisType = processUtils.getNativeType(thisIO);
		else if (thisIO instanceof InputStatic)
			thisType = processUtils.getStaticTypeString(thisIO);
		else
			throw new RuntimeException("Could not find port type");

		if (referencedIO instanceof PrimitiveOutputPort
				|| referencedIO instanceof PrimitiveInputPort)
			refType = processUtils.getPrimitiveTypeString(referencedIO);
		else if (referencedIO instanceof ComplexOutputPort
				|| referencedIO instanceof ComplexInputPort)
			refType = processUtils.getComplexType(referencedIO);
		else if (referencedIO instanceof JavaNativeOutputPort
				|| referencedIO instanceof JavaNativeInputPort)
			refType = processUtils.getNativeType(referencedIO);
		else if (referencedIO instanceof InputStatic)
			refType = processUtils.getStaticTypeString(referencedIO);
		else
			throw new RuntimeException("Could not find port type");

		if (thisType instanceof String && refType instanceof String) {
			if (!((String) thisType).equals(refType))
				addError(thisId, "Type mismatch, should be '" + (String) refType
						+ "'");
		} else if (thisType instanceof Type && refType instanceof Type) {
			if (!((Type) thisType).getId().equals(((Type) refType).getId()))
				addError(thisId, "Type mismatch, should be '"
						+ ((Type) refType).getName() + "'");
		} else {
			addError(thisId, "Type mismatch, should be '"
					+ refType.getClass().getSimpleName() + "'");
		}

		if (processUtils.isListType(thisIO) != processUtils
				.isListType(referencedIO))
			addError(thisId, "isList property mismatch, should be "
					+ processUtils.isListType(referencedIO));
	}
}
