/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.process.checks;

import java.util.ArrayList;
import java.util.Optional;

import info.scce.dime.api.DIMECheck;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.Attribute;
import info.scce.dime.process.process.ComplexAttribute;
import info.scce.dime.process.process.ComplexAttributeConnector;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.PrimitiveAttribute;

public class DataModelCheck extends DIMECheck<ProcessId, ProcessAdapter> {

	private ProcessAdapter adapter;

	private ArrayList<Attribute> brokenAttributes;

	@Override
	public void doExecute(ProcessAdapter adapter) {
		this.adapter = adapter;
		
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof ComplexVariable) {
				ComplexVariable cv = (ComplexVariable) obj;
				if (!isComplexListVariable(cv)) {					
					checkComplexVariable(id, cv);
				}
				//check no updates on expanded extension attribute complex variables
				checkNoUpdateOnExtensionAttributes(id, cv);
			}
		}
	}
	
	@Override
	public void beforeCheck() {
		brokenAttributes = new ArrayList<Attribute>();
	}

	private void checkComplexVariable(ProcessId id, ComplexVariable cv) {
		Type rType = cv.getDataType();
		if (rType == null) {
			addError(id, "Type reference broken");
		} else {
			brokenAttributes = getBrokenAttributes(cv);
			checkAttributeReferences(id, cv, rType);
		}
	}
	
	private void checkNoUpdateOnExtensionAttributes(ProcessId id,ComplexVariable cv) {
		//is attribute of complex type and is update
		if(!cv.getIncomingComplexAttributeConnectors().isEmpty() && !cv.getIncomingUpdates().isEmpty()) {
			//get root type
			ComplexVariable rootVar = cv.getIncomingComplexAttributeConnectors().get(0).getSourceElement();
			Optional<info.scce.dime.data.data.Attribute> optExt = _dataExtension.getInheritedAttributes(rootVar.getDataType()).stream().filter(n->n instanceof ExtensionAttribute).filter(n->n.getName().equals(cv.getName())).findFirst();
			if(optExt.isPresent()) {
				addError(id, "No updates allowed on Extension Attribute "+cv.getName());
			}
		}
	}

	private void checkAttributeReferences(ProcessId id, ComplexVariable cv, Type rType) {
		ArrayList<String> cvAttributesNames = getAttributeNames(cv);
		
		// if (!cv.getName().equals(rType.getName()))
		// cvMsg.add("Type name differs, should be '" + rType.getName() +
		// "'");

		for (info.scce.dime.data.data.Attribute rAttr : _dataExtension.getInheritedAttributes(rType)) {
			int count = 0;
			String oAttrName = null;
			for (Attribute attr : cv.getAttributes()) {
				if (brokenAttributes.contains(attr))
					continue;

				if (attr instanceof PrimitiveAttribute) {
					info.scce.dime.data.data.PrimitiveAttribute refAttr = ((PrimitiveAttribute) attr)
							.getAttribute();
					if (_dataExtension.getOriginalAttribute(refAttr).getId()
							.equals(rAttr.getId())) {
						count++;
						oAttrName = refAttr.getName();
					}
				}
				if (attr instanceof ComplexAttribute) {
					info.scce.dime.data.data.ComplexAttribute refAttr = ((ComplexAttribute) attr)
							.getAttribute();
					if (_dataExtension.getOriginalAttribute(refAttr).getId()
							.equals(rAttr.getId())) {
						count++;
						oAttrName = refAttr.getName();
					}
				}
			}
			for (ComplexAttributeConnector cac : cv
					.getOutgoing(ComplexAttributeConnector.class)) {
				if (cac.getAttributeName().equals(rAttr.getName())) {
					count++;
					oAttrName = cac.getAttributeName();
				}
			}
			if (count > 1) {
				addError(id, "Attribute '" + rAttr.getName() + "' exists " + count
						+ " times");
			} else if (count <= 0) {
//				cvMsg.add("Attribute '" + rAttr.getName() + "' not found");
			} else {
				cvAttributesNames.remove(oAttrName);
			}
		}
		
		for (String attrName : cvAttributesNames) {
			addError(id, "Reference for '" + attrName + "' not found");
		}
		
		for (Attribute attr : brokenAttributes) {
			addError(adapter.getIdByString(attr.getId()), "Reference not found");
		}
	}

	private boolean isComplexListVariable(ComplexVariable cv) {
		if (cv.getIncoming(ComplexAttributeConnector.class).size() > 0) {
			ComplexAttributeConnector cac = cv.getIncoming(
					ComplexAttributeConnector.class).get(0);
			String attrName = cac.getAttributeName();
			Type parentType = ((ComplexVariable) cac.getSourceElement()).getDataType();
			for (info.scce.dime.data.data.Attribute attribute : parentType
					.getAttributes()) {
				if (attribute.getName().equals(attrName)
						&& attribute.isIsList())
					return true;
			}
		}
		return false;
	}

	private ArrayList<Attribute> getBrokenAttributes(ComplexVariable cv) {
		ArrayList<Attribute> brokenAttributes = new ArrayList<>();
		for (Attribute attr : cv.getAttributes()) {
			if (attr instanceof PrimitiveAttribute) {
				info.scce.dime.data.data.PrimitiveAttribute refAttr = ((PrimitiveAttribute) attr)
						.getAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
			if (attr instanceof ComplexAttribute) {
				info.scce.dime.data.data.ComplexAttribute refAttr = ((ComplexAttribute) attr)
						.getAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
		}
		return brokenAttributes;
	}

	private ArrayList<String> getAttributeNames(ComplexVariable cv) {
		ArrayList<String> names = new ArrayList<>();
		for (Attribute cvAttr : cv.getAttributes()) {
			if (brokenAttributes.contains(cvAttr))
				continue;
			if (cvAttr instanceof PrimitiveAttribute) {
				info.scce.dime.data.data.PrimitiveAttribute refAttr = ((PrimitiveAttribute) cvAttr)
						.getAttribute();
				names.add(refAttr.getName());
			}
			if (cvAttr instanceof ComplexAttribute) {
				info.scce.dime.data.data.ComplexAttribute refAttr = ((ComplexAttribute) cvAttr)
						.getAttribute();
				names.add(refAttr.getName());
			}
		}
		for (ComplexAttributeConnector cac : cv
				.getOutgoing(ComplexAttributeConnector.class)) {
			names.add(cac.getAttributeName());
		}
		return names;
	}
}
