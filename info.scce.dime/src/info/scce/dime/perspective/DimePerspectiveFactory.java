/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.perspective;


import org.eclipse.graphiti.ui.internal.editor.ThumbNailView;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class DimePerspectiveFactory implements IPerspectiveFactory {

	@Override
		public void createInitialLayout(IPageLayout layout) {
			layout.addView(IPageLayout.ID_PROJECT_EXPLORER, IPageLayout.LEFT, 0.25f, IPageLayout.ID_EDITOR_AREA); 
			
			IFolderLayout rightFolder = layout.createFolder("info.scce.dime.rightfolder.modelview", IPageLayout.RIGHT, 0.85f, IPageLayout.ID_EDITOR_AREA);
			rightFolder.addView("info.scce.dime.libcompviews.views.Data");
			rightFolder.addView("info.scce.dime.libcompviews.views.Control");
			rightFolder.addView("info.scce.dime.libcompviews.views.UI");
			rightFolder.addView("info.scce.dime.libcompviews.views.Link");
			rightFolder.addView("info.scce.dime.libcompviews.views.ModelHierarchyView");
			
			IFolderLayout propertiesLayout = layout.createFolder("info.scce.dime.property", IPageLayout.BOTTOM, 0.75f, IPageLayout.ID_EDITOR_AREA);
			propertiesLayout.addView("de.jabc.cinco.meta.core.ui.propertyview");
			propertiesLayout.addView(IPageLayout.ID_PROBLEM_VIEW);
			propertiesLayout.addView("info.scce.dime.ui.deployment.view.DeploymentView");
			
			IFolderLayout leftFolder = layout.createFolder("info.scce.dime.leftfolder.minviews", IPageLayout.BOTTOM, 0.55f, IPageLayout.ID_PROJECT_EXPLORER);
			leftFolder.addView(ThumbNailView.VIEW_ID);
			leftFolder.addView("de.jabc.cinco.meta.plugin.mcam.runtime.views.CheckView");
			leftFolder.addView("de.jabc.cinco.meta.plugin.mcam.runtime.views.ProjectCheckView");
			leftFolder.addView(IPageLayout.ID_OUTLINE);
		}

}
