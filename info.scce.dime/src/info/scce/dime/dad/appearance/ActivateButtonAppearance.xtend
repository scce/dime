/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.dad.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.dad.dad.ProfileIconButton
import style.StyleFactory
import info.scce.dime.dad.dad.ProfileSIB

class ActivateButtonAppearance implements StyleAppearanceProvider<ProfileIconButton> {
	
	static val factory = StyleFactory.eINSTANCE
	static val transparent = factory.createAppearance => [ transparency = 1.0 ]
	static val visible = factory.createAppearance => [ transparency = 0.0]
	
	override getAppearance(ProfileIconButton button, String shapeId) {
		if (button.container instanceof ProfileSIB) {
			val profileSIB = button.container as ProfileSIB
			if (shapeId == "imageActive") {
				return if (profileSIB.isActive) visible else transparent
			}
			if (shapeId == "imageInactive") {
				return if (profileSIB.isActive) transparent else visible
			}
		}
	}
	
	
}
