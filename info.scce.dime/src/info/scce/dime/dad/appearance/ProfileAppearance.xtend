/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.dad.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.dad.dad.ProfileSIB
import style.StyleFactory
import style.LineStyle

class ProfileAppearance implements StyleAppearanceProvider<ProfileSIB> {
	
	static val factory = StyleFactory.eINSTANCE
	
	static val black = factory.createAppearance => [ foreground = factory.createColor => [r=0 g=0 b=0] ]
	static val darkGray = factory.createAppearance => [ foreground = factory.createColor => [r=80 g=80 b=80] ]
	static val lightGray = factory.createAppearance => [ foreground = factory.createColor => [r=180 g=180 b=180] ]
	static val invalid = factory.createAppearance => [foreground = factory.createColor => [r=255] lineStyle = LineStyle.DASH ]
	
	override getAppearance(ProfileSIB sib, String shapeId) {
		if (shapeId == "rootShape") {
			if (sib.referencedProfile === null) {
				return invalid
			}
			return if (sib.isActive) black else darkGray
		}
		else if (shapeId == "label") {
			return if (sib.isActive) black else lightGray
		}
	}
	
}
