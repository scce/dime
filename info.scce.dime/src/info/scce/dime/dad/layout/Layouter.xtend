/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.dad.layout

import de.jabc.cinco.meta.runtime.layout.ListLayouter
import graphmodel.Node
import info.scce.dime.dad.dad.ProfileContainer
import info.scce.dime.dad.dad.ProfileIconButton
import info.scce.dime.dad.dad.ProfileSIB
import java.util.List

import static de.jabc.cinco.meta.runtime.layout.LayoutConfiguration.*

class Layouter extends ListLayouter {
	
	static Layouter _instance
	static def getInstance() {
		_instance ?: (_instance = new Layouter)
	}
	
	dispatch def getLayoutConfiguration(ProfileContainer container) {#{
		PADDING_TOP -> 30,
		PADDING_BOTTOM -> 10,
		PADDING_Y -> 5,
		MIN_HEIGHT -> 100,
		MIN_WIDTH -> 150
	}}
	
	dispatch def getLayoutConfiguration(ProfileSIB sib) {#{
		PADDING_TOP -> 0
	}}
	
	dispatch def List<? extends Class<? extends Node>> getIgnoredChildren(ProfileSIB container) {#[
		ProfileIconButton
	]}
}
