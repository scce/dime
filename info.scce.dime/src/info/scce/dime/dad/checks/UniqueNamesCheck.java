/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.dad.checks;

import java.util.HashSet;
import java.util.Set;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.dad.dad.StartupProcessPointer;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.dad.mcam.adapter.DADAdapter;
import info.scce.dime.dad.mcam.adapter.DADId;

public class UniqueNamesCheck extends AbstractCheck<DADId, DADAdapter> {

	@Override
	public void doExecute(DADAdapter adapter) {
		Set<String> urls = new HashSet<>();
		for (DADId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof URLProcess && ((URLProcess) obj).getIncoming(StartupProcessPointer.class).isEmpty()) {
				URLProcess processCmponent = (URLProcess) obj;
				if (!urls.add(processCmponent.getUrl()))
					addError(id, processCmponent.getUrl() + " not unique");
			}
		}
	}
	
	@Override
	public void init() {}

}
