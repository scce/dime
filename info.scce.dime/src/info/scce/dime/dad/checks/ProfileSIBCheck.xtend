/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.dad.checks

import graphmodel.GraphModel
import graphmodel.IdentifiableElement
import graphmodel.Node
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.dad.mcam.modules.checks.DADCheck
import info.scce.dime.process.process.Process
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.profile.Profile
import info.scce.dime.profile.util.ReplacementStrategy

import static extension java.util.Collections.disjoint
import info.scce.dime.profile.profile.ReplacementSIB

class ProfileSIBCheck extends DADCheck {
	
	extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	extension ProfileExtension = new ProfileExtension
	extension ReplacementStrategy = new ReplacementStrategy
	
	val followPrimeRefs = [IdentifiableElement elm | elm.primeReferencedContainer]
	
	override check(DAD it) {
		find(ProfileSIB).forEach[checkIntegrity]
		checkProfileUniqueness
		checkProfileReachability
		checkProfileConflicts
	}
	
	def checkIntegrity(ProfileSIB it) {
		if (referencedProfile === null) {
			addError('''Referenced profile '«cachedReferencedProfileName»' does not exist''')
			return;
		}
	}
	
	def checkProfileUniqueness(DAD it) {
		find(ProfileSIB)
			.filter[referencedProfile !== null]
			.groupBy[referencedProfile]
			.values
			.drop[size <= 1]
			.forEach[ duplicates |
				addError('''These Profile SIBs reference the same Profile: «duplicates.map[cachedReferencedProfileName].join(", ")»''')
			]
	}
	
	def checkProfileReachability(DAD dad) {
		val usedModels = dad.findDeeply(GraphModel, followPrimeRefs).toSet
		val replModels
			= dad.find(ProfileSIB)
				.filter[isActive]
				.filter[referencedProfile !== null]
				.map[referencedProfile]
				.flatMap[find(ReplacementSIB)]
				.filter[isInferredReplacement]
				.map[referencedObject]
				.flatMap[findDeeply(GraphModel, followPrimeRefs)]
		usedModels.addAll(replModels)
		
		dad.find(ProfileSIB)
			.filter[isActive]
			.filter[referencedProfile !== null]
			.forEach[ profileSIB |
				val profile = profileSIB.referencedProfile
				val notUsed = profile.blueprintSIBs
					.map[referencedObject].filter(Node)
					.map[rootElement].filter(Process)
					.filter[!usedModels.contains(it)]
				if (!notUsed.isEmpty) {
					profileSIB.addError('''The referenced profile contains replacements for models that are not used: «notUsed.map[it.modelName].join(", ")»''')
				}
			]
	}
	
	def checkProfileConflicts(DAD it) {
		val activeProfileSIBs
			= find(ProfileSIB)
				.filter[referencedProfile !== null]
				.filter[isActive]
				.toSet
		
		activeProfileSIBs
			.map[ sib | (#[sib] + sib.getConlictedProfileSIBs(activeProfileSIBs.filter[it != sib])).toSet ]
			.drop[ size <= 1 ]
			.toSet
			.forEach[ conflicted |
				addError('''These Profiles contain replacement rules for the same Blueprint SIBs: «conflicted.map[cachedReferencedProfileName].join(", ")»''')
			]
	}
	
	def getConlictedProfileSIBs(ProfileSIB sib, Iterable<ProfileSIB> otherSIBs) {
		otherSIBs.filter[ referencedProfile.isConflictedWith(sib.referencedProfile) ]
	}
	
	def getConlictedProfiles(Profile profile, Iterable<Profile> profiles) {
		profiles.filter[ isConflictedWith(profile) ]
	}
	
	def isConflictedWith(Profile profile, Profile other) {
		!profile.referencedBlueprintSIBs.disjoint(other.referencedBlueprintSIBs)
	}
	
	def getReferencedBlueprintSIBs(Profile profile) {
		profile.blueprintSIBs.map[referencedObject].filterNull.toSet
	}
}
