/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.dad.checks;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.IteratorUtils;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.RootInteractionPointer;
import info.scce.dime.dad.dad.Start;
import info.scce.dime.dad.dad.StartupProcessPointer;
import info.scce.dime.dad.mcam.adapter.DADAdapter;
import info.scce.dime.dad.mcam.adapter.DADId;
import info.scce.dime.data.data.UserType;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.helper.GUIBranchPort;
import info.scce.dime.gui.helper.GUIExtension;
import info.scce.dime.gui.helper.GUIExtensionProvider;
import info.scce.dime.gui.helper.PrimitiveGUIBranchPort;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

public class CorrectnessCheck extends AbstractCheck<DADId, DADAdapter> {

	DADAdapter adapter;
	
	GUIExtension guiExtension;

	@Override
	public void doExecute(DADAdapter arg0) {
		this.adapter = arg0;

		for (DADId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof DAD) {
				DAD dad = (DAD) obj;
				checkDAD(id, dad);
			}
		}
	}

	private void checkDAD(DADId id, DAD dad) {
		if (dad.getStarts().size() < 1 || dad.getStarts().size() > 1)
			addError(id, "Only 1 Start allowed");
		else
			checkStart(adapter.getIdByString(dad.getStarts().get(0).getId()),
					dad.getStarts().get(0));

		if (dad.getSystemUsers().size() < 1 || dad.getSystemUsers().size() > 1)
			addError(id, "SystemUser is required");
		
		//check find user component
		dad.getFindLoginUserComponents().forEach((n)->{
			//check signature
			//one input, text, username
			Process p = n.getModel();
			if(p.getStartSIBs().isEmpty()){
				addError(id, "Invalid Signature. Start SIB missing");
			} else {
				if(p.getStartSIBs().get(0).getOutputPorts().stream().filter(pi->pi instanceof PrimitiveInputPort).filter(pi->((PrimitiveInputPort)pi).getDataType()==PrimitiveType.TEXT&&((PrimitiveInputPort)pi).getName().equals("username")).count()==1) {
					addError(id, "Invalid Signature. Only 'username' primitive text port required");
				}
			}
			List<String> branchnames = new LinkedList<String>(Arrays.asList("Success","Not Found"));
			if(p.getEndSIBs().stream().filter(e->!branchnames.contains(e.getBranchName())).count()!=0) {
				addError(id, "Invalid Signature. Only End SIBs 'Success' and 'Not Found' required");
			} else {
				//success end sib with port user of UserType
				EndSIB success = p.getEndSIBs().stream().filter(e->e.getBranchName().equals("Success")).findAny().get();
				if(success.getInputPorts().size()!=1) {
					addError(id, "Invalid Signature. Success needs one UserType port named 'user'");
				} else {
					if(!success.getInputPorts().get(0).getName().equals("user") || success.getInputPorts().get(0).isIsList()) {
						addError(id, "Invalid Signature. Success needs one UserType port named 'user'");
					} else {
						if(!(success.getInputPorts().get(0) instanceof ComplexInputPort)) {
							addError(id, "Invalid Signature. Success needs one UserType port named 'user'");
						} else {
							if(!(((ComplexInputPort)success.getInputPorts().get(0)).getDataType() instanceof UserType)) {
								addError(id, "Invalid Signature. Success needs one UserType port named 'user'");
							}
						}
					}
				}
				EndSIB notFound = p.getEndSIBs().stream().filter(e->e.getBranchName().equals("Not Found")).findAny().get();
				if(!notFound.getInputs().isEmpty()) {
					addError(id, "Invalid Signature. 'Not Found' End SIB requires no ports");
				}
				
			}
			
		});
		
		//check login component
		//check find user component
		dad.getLoginComponents().forEach((n)->{
			//check signature
			//no input or current user
			GUI gui = n.getModel();
			List<Variable> inputVar = IteratorUtils.toList(guiExtension.inputVariables(gui).iterator());
			if(inputVar.size()!=1) {
				addError(id, "Invalid Signature. One input required");
			} else {
				if(!(inputVar.get(0) instanceof PrimitiveVariable)) {
					addError(id, "Invalid Signature. One primitive input required");
				} else {
					if(((PrimitiveVariable)inputVar.get(0)).getDataType()!=info.scce.dime.gui.gui.PrimitiveType.BOOLEAN) {
						addError(id, "Invalid Signature. One primitive boolean input required");
					}
					if(!((PrimitiveVariable)inputVar.get(0)).getName().equals("notCorrect")) {
						addError(id, "Invalid Signature. One primitive boolean input 'notCorrect' required");
					}
				}
			}
			if(guiExtension.getGUIBranches(gui,true).stream().filter(e->e.getName().equals("login")).count()!=1) {
				addError(id, "Invalid Signature. Only branch 'login' required and allowed");
			} else {
				//get login branch
				 List<GUIBranchPort> branchPorts = guiExtension.getGUIBranches(gui,true).stream().filter(e->e.getName().equals("login")).findFirst().get().getPorts();
				 if(branchPorts.size()!=2) {
					 addError(id, "Invalid Signature. Only two port on branch 'login' required and allowed");
				 } else {
					 List<String> portNames = branchPorts.stream()
							 .filter(bp->bp instanceof PrimitiveGUIBranchPort)
							 .filter(bp->!bp.isList())
							 .filter(bp->((PrimitiveGUIBranchPort)bp).getType()==info.scce.dime.data.data.PrimitiveType.TEXT)
							 .map(bp->bp.getName()).collect(Collectors.toList());
					 if(!(portNames.size()==2&&portNames.contains("username")&&portNames.contains("password"))) {
						 addError(id, "Invalid Signature. Two text ports 'username' and 'password' are required on 'login' branch");
					 }
				 }
				
			}
		});
	}

	private void checkStart(DADId id, Start start) {
		if (start.getOutgoing(RootInteractionPointer.class).size() < 1)
			addError(id, "missing root interaction");
		else if (start.getOutgoing(RootInteractionPointer.class).size() > 1)
			addError(id, "only 1 root interaction allowed");
		else {
			ProcessComponent target = (ProcessComponent) start.getOutgoing(RootInteractionPointer.class).get(0).getTargetElement();
			Process process = target.getModel();
			if (!process.getProcessType().equals(ProcessType.BASIC))
				addError(id, "root interaction process type mismatch. should be BASIC");
		}
		
		if (start.getOutgoing(StartupProcessPointer.class).size() > 1)
			addError(id, "only 1 startup process allowed");
		else if (start.getOutgoing(StartupProcessPointer.class).size() == 1) {
			ProcessComponent target = (ProcessComponent) start.getOutgoing(StartupProcessPointer.class).get(0).getTargetElement();
			Process process = target.getModel();
			if (!process.getProcessType().equals(ProcessType.BASIC) && !process.getProcessType().equals(ProcessType.BASIC))
				addError(id, "startup process type mismatch. should be BASIC");
		}
	}

	@Override
	public void init() {
		guiExtension = GUIExtensionProvider.guiextension();
	}
}
