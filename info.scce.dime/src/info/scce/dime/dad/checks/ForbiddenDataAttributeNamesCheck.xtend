/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
 
package info.scce.dime.dad.checks

import info.scce.dime.dad.mcam.modules.checks.DADCheck
import info.scce.dime.dad.dad.DAD

/**
 * If GraphQL is enabled, verify that property names of data types are not reserved 
 * keywords for auto generated properties of the GraphQL API.
 */
class ForbiddenDataAttributeNamesCheck extends DADCheck {
	
	static val forbiddenAttributeNames = newArrayList("dywaId", "dywaType", "__typename")
	
	override check(DAD model) {
		if (!model.graphQLComponents.empty) {
			model.graphQLComponents.get(0).outgoingGraphQLDataPointers
				.map[it.targetElement.internalDataComponent.model.types]
				.flatten
				.forEach[type|
					type.attributes
						.filter[forbiddenAttributeNames.contains(it)]
						.forEach[attr|
							addError(type, '''«type.name» cannot have attribute with name "«attr.name»"''')
						]
				]
		}
	}
}