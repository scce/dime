/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.dad.hooks
// TODO find highlight 
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlight
import de.jabc.cinco.meta.runtime.hook.CincoPostSelectHook

import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.dad.dad.ProfileIconButton

import static org.eclipse.graphiti.util.IColorConstant.*

class ActivateButtonOnClick extends CincoPostSelectHook<ProfileIconButton> {
	
	override postSelect(ProfileIconButton button) {
		button => [
			animateClick
			diagram.clearSelection
			onClick
		]
	}
	
	def onClick(ProfileIconButton button) {
		switch it:button?.container {
			ProfileSIB: active = !isActive
		}
	}
	
	def animateClick(ProfileIconButton button) {
		var fgColor = GRAY
		var bgColor = WHITE
		if (button.container instanceof ProfileSIB) {
			val profileSIB = button.container as ProfileSIB
			fgColor = if (profileSIB.isActive) RED else GREEN
			bgColor = if (profileSIB.isActive) RED else GREEN
		}
		new Highlight(fgColor, bgColor).add(button).flash(0.5)
	}
}
