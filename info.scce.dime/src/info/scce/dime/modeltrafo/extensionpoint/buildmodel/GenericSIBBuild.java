/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

public class GenericSIBBuild<T extends EObject> {

	private final List<GenericPort> inputPorts;
	private final List<GenericOutputBranch> outputBranches;
	private final T modelObject;
	private final String name;
	private final String label;

	public GenericSIBBuild(T modelObject, List<GenericPort> inputPorts, List<GenericOutputBranch> outputBranches, String name, String label) {
		this.modelObject = modelObject;
		this.inputPorts = inputPorts;
		this.outputBranches = outputBranches;
		this.name = name;
		this.label = label;
	}

	public List<GenericPort> getInputPorts() {
		return inputPorts;
	}

	public String getName() {
		return name;
	}
	
	public String getLabel() {
		return label;
	}

	public T getModelObject() {
		return modelObject;
	}

	public List<GenericOutputBranch> getOutputBranches() {
		return outputBranches;
	}
}
