/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.modeltrafo.extensionpoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.jabc.cinco.meta.util.xapi.FileExtension;
import de.jabc.cinco.meta.util.xapi.WorkspaceExtension;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;

@SuppressWarnings("restriction")
public class ModeltrafoExtensionProvider implements IModeltrafoExtensionConstants {

	private final static Map<EClass, IModeltrafoSupporter<EObject>> supportedElements = getSupportedModelElements();
	private final static WorkspaceExtension workspaceHelper = new WorkspaceExtension();
	private final static FileExtension fileHelper = new FileExtension();
	
	public static List<String> getAllSupportedExtensions() {
		final List<String> result = new ArrayList<String>();
		for (IModeltrafoSupporter<EObject> supporter : supportedElements.values()) {
			result.add(supporter.getModelExtension());
		}
		return result;
	}
	
	public static <T extends EObject> Collection<IResource> getAllSupportedResourcesForProject(IModeltrafoSupporter<T> provider, IProject project) {
		List<IFile> files = workspaceHelper.getFiles(project, new Predicate<IFile>() {
			@Override
			public boolean test(IFile file) {
				if (file.getFileExtension() != null) {					
					return file.getFileExtension().equals(provider.getModelExtension());
				}
				return false;
			}
		});
		List<IResource> resources = new ArrayList<IResource>();
		for (IFile file : files) {
			resources.add(file);
		}
		return resources;
	}
	
	public static <T extends EObject> Collection<EObject> getAllSupportedElementsInResource(IResource resource,
			IModeltrafoSupporter<T> provider) {
		List<EObject> models = new ArrayList<EObject>();
		if (resource instanceof IFile) {
			EClass type = provider.getModelType();
			IFile file = (IFile) resource;
			Resource emfResource = fileHelper.getResource(file);
			TreeIterator<EObject> allContents = emfResource.getAllContents();
			if (!allContents.hasNext()) {
				while (allContents.hasNext()) {
					EObject content = allContents.next();
					if (provider.getModelType().isInstance(content)) {
						models.add(content);
					}
				}
			} else {
				List<EObject> contents = emfResource.getContents();
				for (EObject content : contents) {
					if (type.isInstance(content)) {
						models.add(content);
					} else {
						getAllSupportElementInElement(content, type, models);
					}
				}
			}
		}

		return models;
	}

	private static void getAllSupportElementInElement(EObject element, EClass type, Collection<EObject> models) {
		List<EObject> contents = element.eContents();
		if (!contents.isEmpty()) {
			for (EObject content : contents) {
				if (type.isInstance(content)) {
					models.add(content);
				} else {
					getAllSupportElementInElement(content, type, models);
				}
			}
		}
	}
	
	public static Collection<IModeltrafoSupporter<EObject>> getAllModeltrafoSupporter() {
		return supportedElements.values();
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> IModeltrafoSupporter<T> getSupportedModelElement(EObject ref) {
		IModeltrafoSupporter<T> trafoSupporter = null;
		for (EClass key : supportedElements.keySet()) {
			if (key.isInstance(ref)) {
				trafoSupporter = (IModeltrafoSupporter<T>) supportedElements.get(key);
			}
		}
		return trafoSupporter;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> IModeltrafoSupporter<T> getSupportedModelElement(String fileExtension) {
		for (IModeltrafoSupporter<EObject> provider : supportedElements.values()) {
			if (provider.getModelExtension().equals(fileExtension)) {
				return (IModeltrafoSupporter<T>) provider;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static <T extends EObject> Map<EClass, IModeltrafoSupporter<T>> getSupportedModelElements() {
		final Map<EClass, IModeltrafoSupporter<T>> isSupported = new HashMap<>();
		final IExtensionRegistry registry = Platform.getExtensionRegistry();
		final IConfigurationElement[] config = registry.getConfigurationElementsFor(EXTENSIONPOINT_ID);
		try {
			for (IConfigurationElement e : config) {
				if (e.getName().equals(MODELTRAFO)) {
					final Object modelType = e.createExecutableExtension(MODELTRAFO_MODELTRAFOSUPPORTER);
					IModeltrafoSupporter<T> modelTrafoSupporter = null;
					if (modelType instanceof IModeltrafoSupporter) {
						modelTrafoSupporter = (IModeltrafoSupporter<T>) modelType;
					} else {
						throw new RuntimeException("The class '" + modelType.getClass().toString()
								+ "' doesn't implement the interface 'IModeltrafoSupporter'!");
					}
					isSupported.put(modelTrafoSupporter.getModelType(), modelTrafoSupporter);
				}
			}
		} catch (CoreException ex) {
			System.out.println(ex.getMessage());
		}
		return isSupported;
	}
}
