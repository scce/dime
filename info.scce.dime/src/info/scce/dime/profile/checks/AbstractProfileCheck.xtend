/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.checks

import graphmodel.Node
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.mcam.modules.checks.ProfileCheck
import info.scce.dime.profile.profile.Profile
import info.scce.dime.profile.profile.SIB

abstract class AbstractProfileCheck extends ProfileCheck {
	
	protected extension ProfileExtension = new ProfileExtension
	
	override check(Profile model) {
		model.find(SIB).forEach[
			if (checkIntegrity) checkReplacement
		]
	}
	
	dispatch def boolean checkIntegrity(Node node) {
		true // add dispatch methods in sub-classes
	}
	
	dispatch def boolean checkReplacement(Node node) {
		true // add dispatch methods in sub-classes
	}
}
