/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.layout

import de.jabc.cinco.meta.runtime.layout.ListLayouter
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.profile.OutputPort
import info.scce.dime.profile.profile.SIB

import static de.jabc.cinco.meta.runtime.layout.LayoutConfiguration.*

class Layouter extends ListLayouter {
	
	static Layouter _instance
	static def getInstance() {
		_instance ?: (_instance = new Layouter)
	}

	dispatch def getLayoutConfiguration(SIB sib) {#{
		PADDING_TOP -> 30
	}}
	
	dispatch def getLayoutConfiguration(Branch container) {#{
		MIN_WIDTH -> 100,
		MIN_HEIGHT -> 20,
		PADDING_TOP -> 24
	}}
	
	dispatch def getLayoutConfiguration(InputPort container) {#{
		MIN_HEIGHT -> 18,
		PADDING_TOP -> 24,
		PADDING_BOTTOM -> 5
	}}
	
	dispatch def getLayoutConfiguration(OutputPort container) {#{
		MIN_HEIGHT -> 18,
		PADDING_TOP -> 24
	}}
	
	dispatch def getLayoutConfiguration(BlueprintSIB sib) {#{
		MIN_HEIGHT -> 34,
		PADDING_TOP -> 38
	}}
	
	dispatch def getInnerOrder(SIB sib) {
		#[InputPort, Branch]
	}
}
