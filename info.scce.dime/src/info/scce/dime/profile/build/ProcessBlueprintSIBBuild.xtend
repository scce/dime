/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.build

import info.scce.dime.profile.profile.ProcessBlueprintSIB
import info.scce.dime.profile.profile.OutputPort
import info.scce.dime.process.process.BranchBlueprint
import info.scce.dime.profile.profile.BlueprintBranch
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.layout.Layouter

class ProcessBlueprintSIBBuild extends PrimeSIBBuild<
	                      ProcessBlueprintSIB, /* SIB type */
	                                InputPort, /* SIB port type */
	 info.scce.dime.process.process.InputPort, /* SIB port reference */
	                          BlueprintBranch, /* Branch type */
	                          BranchBlueprint, /* Branch reference */
	                               OutputPort, /* Branch port type */
    info.scce.dime.process.process.OutputPort  /* Branch port reference */ > {
	
	extension ProfileExtension = new ProfileExtension
	extension Layouter layouter = Layouter.instance
	
	// ++++++++++++++++++
	//
	//      Statics
	//
	// ++++++++++++++++++
	
	static def initialize(ProcessBlueprintSIB sib) {
		new ProcessBlueprintSIBBuild(sib).initialize
	}
	
	static def update(ProcessBlueprintSIB sib) {
		new ProcessBlueprintSIBBuild(sib).update
	}
	
	// ++++++++++++++++++
	//
	//     Constructor
	//
	// ++++++++++++++++++
	
	new(ProcessBlueprintSIB sib) {
		super(sib)
	}
	
	// ++++++++++++++++++
	//
	//         SIB
	//
	// ++++++++++++++++++
	
	override info.scce.dime.process.process.ProcessBlueprintSIB getSIBReference(ProcessBlueprintSIB sib) {
		sib.referencedProcessBlueprintSIB as info.scce.dime.process.process.ProcessBlueprintSIB
	}
	
	override layout(ProcessBlueprintSIB sib) {
		layouter.layout(sib)
	}
	
	// ++++++++++++++++++
	//
	//      SIB Ports
	//
	// ++++++++++++++++++
	
	override getSIBPorts(ProcessBlueprintSIB sib) {
		sib.inputPorts
	}
	
	override getSIBPortReferences(ProcessBlueprintSIB sib) {
		sib.SIBReference?.inputPorts
	}
	
	override isSIBPortReference(InputPort sibPort, info.scce.dime.process.process.InputPort portRef) {
		portRef == sibPort.referencedObject
	}
	
	override addSIBPort(info.scce.dime.process.process.InputPort portRef) {
		println("Add input for: " + portRef)
		switch it:portRef {
			info.scce.dime.process.process.PrimitiveInputPort: sib.newPrimitiveInputPortReferencingInputPort(it, 1, sib.height - 1)
			info.scce.dime.process.process.ComplexInputPort: sib.newComplexInputPortReferencingInputPort(it, 1, sib.height - 1)
		}
	}
	
	// ++++++++++++++++++
	//
	//      Branch
	//
	// ++++++++++++++++++
	
	override getBranches(ProcessBlueprintSIB sib) {
		sib.blueprintBranchs
	}
	
	override getBranchReferences(ProcessBlueprintSIB sib) {
	sib.SIBReference.branchBlueprintSuccessors
	}
	
	override isBranchReference(BlueprintBranch branch, BranchBlueprint branchRef) {
		branchRef == branch.referencedObject
	}
	
	override addBranch(BranchBlueprint branchRef) {
		println("BranchRef: " + branchRef)
		sib.newBlueprintBranch(branchRef, 1, sib.height - 1)
	}
	
	override layout(BlueprintBranch branch) {
		layouter.layout(branch)
	}
	
	// ++++++++++++++++++
	//
	//    Branch Ports
	//
	// ++++++++++++++++++
	
	override getBranchPorts(BlueprintBranch branch) {
		branch.outputPorts
	}
	
	override getBranchPortReferences(BranchBlueprint branchRef) {
		branchRef.outputPorts
	}
	
	override isBranchPortReference(OutputPort port, info.scce.dime.process.process.OutputPort portRef) {
		portRef == port.referencedObject
	}
	
	override addBranchPort(BlueprintBranch branch, info.scce.dime.process.process.OutputPort portRef) {
		println("Add output for: " + portRef)
		switch it:portRef {
			info.scce.dime.process.process.PrimitiveOutputPort: branch.newPrimitiveOutputPortReferencingOutputPort(it, 1, sib.height - 1)
			info.scce.dime.process.process.ComplexOutputPort: branch.newComplexOutputPortReferencingOutputPort(it, 1, sib.height - 1)
		}
	}
	
}
