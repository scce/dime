/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.build

import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import info.scce.dime.profile.profile.ProcessSIB
import info.scce.dime.profile.profile.EndSIBReferencingBranch
import info.scce.dime.profile.profile.InputPortReferencingOutputPort
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.api.ProfileExtension
import java.util.List
import info.scce.dime.profile.layout.Layouter

class ProcessSIBBuild extends PrimeSIBBuild<
                       ProcessSIB, /* SIB type */
                        InputPort, /* SIB port type */
                           Output, /* SIB port reference */
          EndSIBReferencingBranch, /* Branch type */
                           EndSIB, /* Branch reference */
   InputPortReferencingOutputPort, /* Branch port type */
                            Input  /* Branch port reference */ > {
				     	
	extension ProfileExtension = new ProfileExtension
	extension Layouter layouter = Layouter.instance
	
	// ++++++++++++++++++
	//
	//      Statics
	//
	// ++++++++++++++++++
	
	static def initialize(ProcessSIB sib) {
		new ProcessSIBBuild(sib).initialize
	}
	
	static def update(ProcessSIB sib) {
		new ProcessSIBBuild(sib).update
	}
	
	// ++++++++++++++++++
	//
	//     Constructor
	//
	// ++++++++++++++++++
	
	new(ProcessSIB sib) {
		super(sib)
	}
	
	// ++++++++++++++++++
	//
	//         SIB
	//
	// ++++++++++++++++++
	
	override getSIBReference(ProcessSIB sib) {
		sib.referencedProcess
	}
	
	override layout(ProcessSIB sib) {
		layouter.layout(sib)
	}
	
	// ++++++++++++++++++
	//
	//      SIB Ports
	//
	// ++++++++++++++++++
	
	override getSIBPorts(ProcessSIB sib) {
		sib.inputPorts
	}
	
	override getSIBPortReferences(ProcessSIB sib) {
		val List<Output> references = newArrayList
		val startSIB = (sib.referencedProcess as Process).startSIB
		println("StartSIB: " + startSIB)
		references => [
			addAll(startSIB?.outputs ?: #[])
//			addAll(startSIB?.typeOutputs ?: #[])
//			addAll((sib.referencedProcess as Process).processPlaceholderSIBs.distinctByKey[label])
			forEach[println("StartSIB.output: " + it)]
		]
	}
	
	override isSIBPortReference(InputPort sibPort, Output portRef) {
		portRef == sibPort.referencedObject
	}
	
	override addSIBPort(Output ref) {
		println("Add input for: " + ref)
		switch it:ref {
			PrimitiveOutputPort: sib.newPrimitiveOutputPortReferencingInputPort(it, 1, sib.height - 1)
			ComplexOutputPort: sib.newComplexOutputPortReferencingInputPort(it, 1, sib.height - 1)
		}
	}
	
	// ++++++++++++++++++
	//
	//      Branch
	//
	// ++++++++++++++++++
	
	override getBranches(ProcessSIB sib) {
		sib.endSIBReferencingBranchs
	}
	
	override getBranchReferences(ProcessSIB sib) {
		val referencedProcess = (sib.referencedProcess as Process)
		println("Process: " + referencedProcess?.modelName)
		val endSIBs = referencedProcess.endSIBs
		println("EndSIBs: " + endSIBs?.map[branchName]?.join(", "))
		return endSIBs
	}
	
	override isBranchReference(EndSIBReferencingBranch branch, EndSIB endSib) {
		endSib == branch.referencedObject
	}
	
	override addBranch(EndSIB ref) {
		println("BranchRef: " + EndSIB)
		sib.newEndSIBReferencingBranch(ref, 1, sib.height - 1)
	}
	
	override layout(EndSIBReferencingBranch branch) {
		layouter.layout(branch)
	}
	
	// ++++++++++++++++++
	//
	//    Branch Ports
	//
	// ++++++++++++++++++
	
	override getBranchPorts(EndSIBReferencingBranch sib) {
		sib.inputPortReferencingOutputPorts
	}
	
	override getBranchPortReferences(EndSIB endSib) {
		endSib.inputs
	}
	
	override isBranchPortReference(InputPortReferencingOutputPort port, Input portRef) {
		portRef == port.referencedObject
	}
	
	override addBranchPort(EndSIBReferencingBranch branch, Input ref) {
		println("Add output for: " + ref)
		switch it:ref {
			PrimitiveInputPort: branch.newPrimitiveInputPortReferencingOutputPort(it, 1, sib.height - 1)
			ComplexInputPort: branch.newComplexInputPortReferencingOutputPort(it, 1, sib.height - 1)
		}
	}
	
	// ++++++++++++++++++
	//
	//     Utilities
	//
	// ++++++++++++++++++
	
	def getStartSIB(Process process) {
		if (process.startSIBs.isEmpty) {
			showError("Referenced process model does not contain a Start SIB");
			return null
		}
		if (process.startSIBs.size > 1) {
			showError("Referenced process model contains multiple Start SIBs");
		}
		process.startSIBs.head
	}
	
}
