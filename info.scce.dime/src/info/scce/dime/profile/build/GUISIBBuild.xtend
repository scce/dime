/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.build

import info.scce.dime.gUIPlugin.AbstractParameter
import info.scce.dime.gUIPlugin.Output
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.OutputGeneric
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.helper.PortUtils
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.layout.Layouter
import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.GUISIB
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.profile.OutputPort
import java.util.Collection
import org.eclipse.emf.ecore.EObject

class GUISIBBuild extends PrimeSIBBuild<
             GUISIB, /* SIB type */
          InputPort, /* SIB port type */
           Variable, /* SIB port reference */
             Branch, /* Branch type */
            EObject, /* Branch reference */
         OutputPort, /* Branch port type */
            EObject  /* Branch port reference */ > {
	
	protected extension ProfileExtension = new ProfileExtension
	protected extension GUIExtension = new GUIExtension
	protected extension PortUtils = new PortUtils
	extension Layouter layouter = Layouter.instance
	
	// ++++++++++++++++++
	//
	//      Statics
	//
	// ++++++++++++++++++
	
	static def initialize(GUISIB sib) {
		new GUISIBBuild(sib).initialize
	}
	
	static def update(GUISIB sib) {
		new GUISIBBuild(sib).update
	}
	
	static def validate(GUISIB sib) {
		new GUISIBBuild(sib).validate
	}
	
	// ++++++++++++++++++
	//
	//     Constructor
	//
	// ++++++++++++++++++
	
	new(GUISIB sib) {
		super(sib)
	}
	
	// ++++++++++++++++++
	//
	//         SIB
	//
	// ++++++++++++++++++
	
	override GUI getSIBReference(GUISIB sib) {
		sib.referencedGUI as GUI
	}
	
	override layout(GUISIB sib) {
		layouter.layout(sib)
	}
	
	// ++++++++++++++++++
	//
	//      SIB Ports
	//
	// ++++++++++++++++++
	
	override getSIBPorts(GUISIB sib) {
		sib.inputPorts
	}
	
	override getSIBPortReferences(GUISIB sib) {
		newArrayList => [
			addAll(
				sib.SIBReference.dataContexts
					.map[variables]
					.flatten
					.filter[isIsInput]
					.filter[getIncoming(ComplexAttributeConnector).isEmpty]
					.filter[getIncoming(ComplexListAttributeConnector).isEmpty])
		]
	}
	
	override isSIBPortReference(InputPort sibPort, Variable portRef) {
		portRef == sibPort.referencedObject
	}
	
	override addSIBPort(Variable ref) {
		println("Add input for: " + ref)
		switch ref {
			// InputPort => GUI model elements
			PrimitiveVariable: sib.newPrimitiveVariableReferencingInputPort(ref, 1, sib.height - 1)
			ComplexVariable: sib.newComplexVariableReferencingInputPort(ref, 1, sib.height - 1)
			// InputPort => Process model elements
			PrimitiveInputPort: sib.newPrimitiveInputPortReferencingInputPort(ref, 1, sib.height - 1)
			ComplexInputPort: sib.newComplexInputPortReferencingInputPort(ref, 1, sib.height - 1)
			PrimitiveOutputPort: sib.newPrimitiveOutputPortReferencingInputPort(ref, 1, sib.height - 1)
			ComplexOutputPort: sib.newComplexOutputPortReferencingInputPort(ref, 1, sib.height - 1)
		}
	}
	
	// ++++++++++++++++++
	//
	//      Branch
	//
	// ++++++++++++++++++
	
	override getBranches(GUISIB sib) {
		sib.branchs
	}
	
	Collection<GUIBranch> _branchElements
	
	def getBranchElements(GUISIB sib) {
		_branchElements ?: (_branchElements = sib.SIBReference.getGUIBranches(true))
	}
	
	override getBranchReferences(GUISIB sib) {
		sib.branchElements.map[element]
	}
	
	override isBranchReference(Branch branch, EObject ref) {
		println("Is BranchRef? " + ref)
		ref == branch.referencedObject
	}
	
	override addBranch(EObject ref) {
		println("BranchRef: " + ref)
		switch ref {
			info.scce.dime.gui.gui.Branch: sib.newBranchReferencingBranch(ref, 1, sib.height - 1)
			Button: sib.newButtonReferencingBranch(ref, 1, sib.height - 1)
			EndSIB: sib.newEndSIBReferencingBranch(ref, 1, sib.height - 1)
			Output: sib.newOutputReferencingBranch(ref, 1, sib.height - 1)
		}
	}
	
	override layout(Branch branch) {
		layouter.layout(branch)
	}
	
	// ++++++++++++++++++
	//
	//    Branch Ports
	//
	// ++++++++++++++++++
	
	override getBranchPorts(Branch branch) {
		println("getBranchPorts: " + branch)
		branch.outputPorts
	}
	
	override getBranchPortReferences(EObject branchRef) {
		sib.branchElements.filter[it.element == branchRef].flatMap[ports.notEquallyNamedPorts].map[portNode]
	}
	
	override isBranchPortReference(OutputPort port, EObject ref) {
		println("Is BranchRef? " + ref)
		ref == port.referencedObject
	}
	
	override addBranchPort(Branch branch, EObject ref) {
		println("Add output for: " + ref)
		switch ref {
			// OutputPort => GUI model elements
			AbstractParameter: branch.newAbstractParameterReferencingOutputPort(ref, 1, sib.height - 1)
			PrimitiveAttribute: branch.newPrimitiveAttributeReferencingOutputPort(ref, 1, sib.height - 1)
			ComplexAttribute: branch.newComplexAttributeReferencingOutputPort(ref, 1, sib.height - 1)
			info.scce.dime.gui.gui.PrimitiveInputPort: branch.newGUIPrimitiveInputPortReferencingOutputPort(ref, 1, sib.height - 1)
			info.scce.dime.gui.gui.ComplexInputPort: branch.newGUIComplexInputPortReferencingOutputPort(ref, 1, sib.height - 1)
			info.scce.dime.gui.gui.PrimitiveOutputPort: branch.newGUIPrimitiveOutputPortReferencingOutputPort(ref, 1, sib.height - 1)
			info.scce.dime.gui.gui.ComplexOutputPort: branch.newGUIComplexOutputPortReferencingOutputPort(ref, 1, sib.height - 1)
			OutputGeneric: branch.newOutputGenericReferencingOutputPort(ref, 1, sib.height - 1)
			PrimitiveVariable: branch.newPrimitiveVariableReferencingOutputPort(ref, 1, sib.height - 1)
			ComplexVariable: branch.newComplexVariableReferencingOutputPort(ref, 1, sib.height - 1)
			// OutputPort => Process model elements
			PrimitiveInputPort: branch.newPrimitiveInputPortReferencingOutputPort(ref, 1, sib.height - 1)
			ComplexInputPort: branch.newComplexInputPortReferencingOutputPort(ref, 1, sib.height - 1)
			PrimitiveOutputPort: branch.newPrimitiveOutputPortReferencingOutputPort(ref, 1, sib.height - 1)
			ComplexOutputPort: branch.newComplexOutputPortReferencingOutputPort(ref, 1, sib.height - 1)
		}
	}
	
}
