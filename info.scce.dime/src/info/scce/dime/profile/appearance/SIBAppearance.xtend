/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import graphmodel.Node
import info.scce.dime.profile.api.ProfileRuntimeBaseClass
import info.scce.dime.profile.util.PrimeNameCacher
import style.Appearance
import style.LineStyle
import style.StyleFactory

class SIBAppearance<T extends Node> extends ProfileRuntimeBaseClass implements StyleAppearanceProvider<T> {
	
	extension PrimeNameCacher = new PrimeNameCacher
	
	static val factory = StyleFactory.eINSTANCE
	static val appearances = <Node,Appearance> newHashMap
	
	override getAppearance(T node, String shapeId) {
		if (shapeId == "rootShape") { // compute only once for each style
			val isValid = cacheNames(node)
			if (isValid) {
				// TODO maybe somehow mark replacements with green border color?
			} else if (!node.container.hasInvalidPrimeReference) {
				appearances.put(node, factory.createAppearance => [
					foreground = factory.createColor => [r=255]
					lineStyle = LineStyle.DASH
				])
			} else appearances.remove(node)
		}
		return appearances.get(node)
	}
	
}
