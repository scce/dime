/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import graphmodel.Edge
import info.scce.dime.profile.api.ProfileRuntimeBaseClass
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.SIBReplacement
import info.scce.dime.profile.util.ReplacementStrategy
import style.StyleFactory

class SIBReplacementAppearance<T extends Edge> extends ProfileRuntimeBaseClass implements StyleAppearanceProvider<T> {
	
	static val factory = StyleFactory.eINSTANCE
	static val thickLineAppearance = factory.createAppearance => [ lineWidth = 3 ]
	
	extension ReplacementStrategy = new ReplacementStrategy
	
	override getAppearance(T edge, String shapeId) {
		val targetNode = edge.targetElement
		val blueprintSIB = targetNode.findPredecessorsVia(SIBReplacement).filter(BlueprintSIB).head
		if (blueprintSIB !== null) {
			val replacementPath = blueprintSIB.replacementPath
			if (!replacementPath.exists[!hasInvalidPrimeReference]) {
				return null
			}
			for (node : replacementPath) {
				if (node == targetNode) {
					return thickLineAppearance
				}
				if (!node.hasInvalidPrimeReference) {
					return null
				}
			}
		}
	}
	
}
