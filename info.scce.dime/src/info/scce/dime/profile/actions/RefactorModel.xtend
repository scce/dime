/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.actions

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.dime.profile.profile.ProcessSIB
import info.scce.dime.process.process.Process
import org.eclipse.emf.ecore.EObject
import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import graphmodel.GraphModel
import graphmodel.ModelElementContainer
import graphmodel.IdentifiableElement
import info.scce.dime.gui.gui.GUI
// TODO Find replacement for ProgressMonitor
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.NullProgressMonitor
import java.util.Collection

import java.util.List
import info.scce.dime.dad.dad.DAD
import de.jabc.cinco.meta.plugin.dsl.ProjectDescription
import de.jabc.cinco.meta.plugin.dsl.FolderDescription
import org.eclipse.core.runtime.Path
import de.jabc.cinco.meta.plugin.dsl.ProjectType

import static java.util.Collections.disjoint
import java.io.File
import org.eclipse.core.resources.IFile
import info.scce.dime.profile.profile.ReplacementSIB
import info.scce.dime.profile.api.ProfileExtension

class RefactorModel extends CincoCustomAction<ReplacementSIB> {
	
	extension ProfileExtension = new ProfileExtension
	
	override getName() {
		"Refactor Model"
	}
	
	override execute(ReplacementSIB sib) {
		println("[" + class.simpleName + hashCode + "] Collecting models...")
		val model = sib.referencedObject as GraphModel
		
		val modelDeepModels = model.findModelsDeeply
		println("Model.deepModels: " + modelDeepModels.size)
		modelDeepModels.map[file].forEach[
//			println(" > " + it)
		]
		
		val usedModels = lookup(DAD).flatMap[findModelsDeeply].toSet
		println("DADs.usedModels: " + usedModels.size)
		usedModels.map[file].forEach[
//			println(" > " + it)
		]

		val usedModelsExceptModel = lookup(DAD).flatMap[it.findModelsDeeply(/*except*/model)].toSet
		println("DADs.usedModels.exceptModel: " + usedModelsExceptModel.map[file].size)
		usedModelsExceptModel.map[file].forEach[
//			println(" > " + it)
		]
		
		val modelOnlyModels = modelDeepModels.drop[usedModelsExceptModel.contains(it)].toSet
		println("Model-only models: " + modelOnlyModels.size)
		modelOnlyModels.map[file].forEach[
//			println(" > " + it)
		]
		
		val extendingModels = allExtendingModels
		println("All extending models: " + extendingModels.size)
		extendingModels.map[file].forEach[
//			println(" > " + it)
		]
		
		val modelOnlyExtendingModels = extendingModels.filter[
			val superTypes = extensionHierarchy
			superTypes.containsAnyOf(modelOnlyModels)
				&& !superTypes.containsAnyOf(usedModelsExceptModel)
		].toSet
		println("Models extending model-only models: " + modelOnlyExtendingModels.size)
		modelOnlyExtendingModels.map[file].forEach[
//			println(" > " + it)
		]
		
		modelOnlyModels.addAll(modelOnlyExtendingModels)
		println("Model-only models now: " + modelOnlyModels.size)
		
		val unusedModels = lookup(GraphModel)
			.drop[usedModels.contains(it)]
			.drop[extensionHierarchy.containsAnyOf(usedModels)]
			.toSet
		println("Unused models: " + unusedModels.size)
		unusedModels.map[file].forEach[
//			println(" > " + it)
		]
		
		val unusedModelsReferencingModel = unusedModels.filter[findModelsDeeply.containsAnyOf(modelOnlyModels)].toSet
		println("Unused models referencing model: " + unusedModelsReferencingModel.size)
		unusedModelsReferencingModel.map[file].forEach[
//			println(" > " + it.projectRelativePath)
//			val targetPath = new Path(model.modelName).append(projectRelativePath)
//			println("   > " + targetPath)
		]
		modelOnlyModels.addAll(unusedModelsReferencingModel)
		println("Model-only models now: " + modelOnlyModels.size)
		
		
//		val newProjectBaseName = model.modelName?.trim?.replace(" ", "_")
		val newProjectBaseName = sib.cachedReferencedObjectName 
		var newProjectName = newProjectBaseName
		var i = 2;
		while (workspaceRoot.getProject(newProjectName).exists) {
			newProjectName = newProjectBaseName + i++
		}
		println("Create project " + newProjectName)
		val newProject = (new ProjectDescription(newProjectName) => [type = ProjectType.JAVA]).create
		
		modelOnlyModels.filter(GUI).forEach[ gui |
			gui.additionalStylesheets.drop[startsWith("http")].forEach[ sheet |
				
				try {
					gui.project.getFile(new Path(sheet)).copyTo(newProject)
				} catch(Exception e) {
					e.printStackTrace
				}
			]
		]
		
		modelOnlyModels.map[println("Model: " + it) file].forEach[moveTo(newProject)]
		
		workspaceRoot.refreshLocal(IProject.DEPTH_INFINITE, new NullProgressMonitor)
	}
	
	def copyTo(IFile file, IProject targetProject) {
		println("Create folder " + file.projectRelativePath.removeLastSegments(1))
		targetProject.createFolder(file.projectRelativePath.removeLastSegments(1))
		val targetPath = targetProject.fullPath.append(file.projectRelativePath)
		println("Copy file " + file + " to " + targetPath)
		file.copy(targetPath, true, new NullProgressMonitor)
	}
	
	def moveTo(IFile file, IProject targetProject) {
		println("Move file " + file)
		println(" > create folder: " + file.projectRelativePath.removeLastSegments(1))
		targetProject.createFolder(file.projectRelativePath.removeLastSegments(1))
		val targetPath = targetProject.fullPath.append(file.projectRelativePath)
		println(" > target path: " + targetPath)
		file.move(targetPath, true, new NullProgressMonitor)
	}
	
	def <T extends EObject> lookup(Class<T> clazz) {
		ReferenceRegistry.instance.lookup(clazz)
	}
	
	def findModelsDeeply(GraphModel model) {
		 model.findDeeply(GraphModel, primerefs).toSet
	}
	
	def findModelsDeeply(GraphModel model, GraphModel except) {
		 model.findDeeply(GraphModel, primerefs(except)).toSet
	}
	
	def findProcessesDeeply(GraphModel model) {
		 model.findDeeply(Process, primerefs).toSet
	}
	
	def findProcessesDeeply(GraphModel model, GraphModel except) {
		 model.findDeeply(Process, primerefs(except)).toSet
	}
	
	def findGUIsDeeply(GraphModel model) {
		 model.findDeeply(GUI, primerefs).toSet
	}
	
	def findGUIsDeeply(GraphModel model, GraphModel except) {
		 model.findDeeply(GUI, primerefs(except)).toSet
	}
	
	def primerefs() {
		primerefs(null)
	}
	
	def (IdentifiableElement)=>ModelElementContainer primerefs(GraphModel except) {
		[switch it {
			
			// other GUI prime nodes
//			ComplexInputPort
//			ComplexOutputPort
//			ComplexVariable
//			PrimitiveAttribute
//			ComplexAttribute
//			PrimitiveListAttribute
//			ComplexListAttribute
//			GUIPlugin
//			ISSIB
//			FORSIB
			
			info.scce.dime.gui.gui.GUISIB: gui
			info.scce.dime.gui.gui.ProcessSIB: proMod as Process
			info.scce.dime.gui.gui.SecuritySIB: proMod as Process
			info.scce.dime.gui.gui.GuardSIB: process as Process
			
			info.scce.dime.process.process.GUISIB: gui
			info.scce.dime.process.process.ProcessSIB: proMod
			info.scce.dime.process.process.GuardedProcessSIB: proMod
			info.scce.dime.process.process.GuardProcessSIB: securityProcess
			
			// other Process prime nodes
//			SearchSIB
//			CreateSIB
//			TransientCreateSIB
//			CreateUserSIB
//			ContainsSIB
//			ContainsJavaNativeSIB
//			IsOfTypeSIB
//			IterateSIB
//			IterateJavaNativeSIB
//			PutComplexToContextSIB
//			RetrieveOfTypeSIB
//			RetrieveCurrentUserSIB
//			RemoveFromListSIB
//			EnumSwitchSIB
//			RetrieveEnumLiteralSIB
//			SetAttributeValueSIB
//			UnsetAttributeValueSIB
//			AtomicSIB
//			ComplexInputPort
//			InputGeneric
//			ComplexAttributePort
//			PrimitiveAttributePort
//			JavaNativeInputPort
//			ProcessInputStatic
//			ComplexOutputPort
//			OutputGeneric
//			JavaNativeOutputPort
//			JavaNativeVariable
//			ComplexVariable
//			PrimitiveAttribute
//			ComplexAttribute
//			PrimitiveListAttribute
//			ComplexListAttribute
//			TypeVariable
//			TypeInput
//			NativeFrontendSIBReference
//			BasicProcessSIB
//			InteractableProcessSIB
//			InteractionProcessSIB
//			SecurityProcessSIB
			
			
			info.scce.dime.dad.dad.ProcessComponent: model
			info.scce.dime.dad.dad.LoginComponent: model
			info.scce.dime.dad.dad.FindLoginUserComponent: model
			
		}?.ifNot(except)]
	}
	
	def <T extends GraphModel> ifNot(T a, GraphModel b) {
		if (a != b) a else null
	}
	
	def containsAnyOf(Collection<?> c1, Collection<?> c2) {
		!disjoint(c1, c2)
	}
	
	def getExtendedModel(GraphModel it) {
		switch it {
			GUI: extendedGUI
		}
	}
	
	def getExtendedGUI(GUI it) {
		extensionContexts?.flatMap[find(info.scce.dime.gui.gui.GUISIB)]?.head?.gui
	}
	
	
	def getExtendingGUIs(GUI gui) {
		lookup(GUI).filter[extendedModel == gui].toSet
	}
	
	def getAllExtendingGUIs() {
		lookup(GUI).filter[extendedModel !== null].toSet
	}
	
	def getExtendingProcesses(Process process) {
		lookup(Process).filter[extendedModel == process].toSet
	}
	
	def getAllExtendingProcesses() {
		lookup(Process).filter[extendedModel !== null].toSet
	}
	
	def getExtendingModels(GraphModel model) {
		lookup(GraphModel).filter[extendedModel == model].toSet
	}
	
	def getAllExtendingModels() {
		lookup(GraphModel).filter[extendedModel !== null].toSet
	}
	
	def List<GraphModel> getExtensionHierarchy(GraphModel model) {
		(#[model] + (model.extendedModel?.extensionHierarchy ?: #[])).toList
	}
	
	def Iterable<GUI> getExtensionHierarchy(GUI gui) {
		(#[gui] + (gui.extendedGUI?.extensionHierarchy ?: #[])).toList
	}
	
	def Iterable<Process> getExtensionHierarchy(Process process) {
		(#[process]).toList
	}
}
