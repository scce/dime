/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.BranchBlueprint
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.profile.ProcessBlueprintSIB
import info.scce.dime.profile.profile.ProcessSIB
import info.scce.dime.profile.profile.SIB
import info.scce.dime.profile.profile.GUIBlueprintSIB
import info.scce.dime.profile.profile.GUISIB
import info.scce.dime.profile.util.ReplacementStrategy

class Replace extends DIMECustomAction<SIB> {
	
	extension ProfileExtension = new ProfileExtension
	extension ReplacementStrategy = new ReplacementStrategy
	
	var saveModel = false
	var success = false
	
	override getName() {
		"Perform Replacement"
	}
	
	override hasDoneChanges() {
		false
	}
	
	override execute(SIB sib) {
		val refSib = sib.referencedObject as info.scce.dime.process.process.BlueprintSIB
		if (refSib !== null) {
			val model = refSib.rootElement
			saveModel = true
			sib.performReplacement
			if (success) {
				val yes = showQuestionDialog("Replacement", "Replacement successful. Open model now?")
				if (yes) [
					model.openEditor
				].onException[
					showWarningDialog("Open Model", "Opening model failed, sorry.")
				]
			} else {
				showWarningDialog("Replacement", "Replacement failed, sorry.")
			}
		}
	}
	
	def getReferencedElement(ProcessBlueprintSIB sib) {
		sib.referencedProcessBlueprintSIB as info.scce.dime.process.process.ProcessBlueprintSIB
	}
	
	def getReferencedElement(GUIBlueprintSIB sib) {
		sib.referencedGUIBlueprintSIB as info.scce.dime.process.process.GUIBlueprintSIB
	}
	
	dispatch def performReplacement(GUISIB sib) {
		showWarningDialog("Replacement", "Only blueprints can be replaced.")
	}
	
	dispatch def performReplacement(ProcessSIB sib) {
		showWarningDialog("Replacement", "Only blueprints can be replaced.")
	}
	
	dispatch def performReplacement(GUIBlueprintSIB sib) {
		val refSib = sib.referencedElement
		if (refSib !== null) {
			val replSib = sib.getInferredReplacement
			if (!(replSib instanceof GUISIB)) {
				return null;
			}
			val replGUI = (replSib as GUISIB)?.referencedObject
			sib.performReplacement(replGUI as GUI)
		}
	}
	
	def performReplacement(GUIBlueprintSIB sib, GUI replGUI) {
		val refSib = sib.referencedElement
		if (refSib !== null) {
			val model = refSib.rootElement
			
			val defaultContentSIBs
				= model.find(info.scce.dime.process.process.SIB)
					.filter[hasDefaultContent(refSib)]
					.toSet
			
			val newSib = model.newGUISIB(replGUI, refSib.x, refSib.y, refSib.width, refSib.height) => [
				label = refSib.label
				name = refSib.name
				majorBranch = refSib.majorBranch
				majorPage = refSib.majorPage
				defaultContent = refSib.defaultContent
			]
			
			for (edge : refSib.incoming) {
				edge.reconnectTarget(newSib)
			}
			
			for (port : sib.inputPorts) {
				val replPort = port.inferredReplacement
				if (replPort !== null) {
					val refPort = port.referencedObject as info.scce.dime.process.process.InputPort
					val newPort = newSib.findThe(InputPort)[name == replPort.referencedObjectName]
					for (edge : refPort.incoming) {
						edge.reconnectTarget(newPort)
					}
				} else {
					warn('''InputPort '«port.name»' of SIB '«sib.name»' has no replacement''')
				}
			}
			
			for (branch : sib.blueprintBranchs) {
				val replBranch = branch.inferredReplacement
				if (replBranch !== null) {
					val refBranch = branch.referencedBranch as BranchBlueprint
					
					val majorBranchSIBs
						= model.find(info.scce.dime.process.process.SIB)
							.filter[hasMajorBranch(refBranch)]
							.toSet
							
					val branchName = replBranch.cachedReferencedObjectName
					val newBranch = newSib.branchSuccessors.filter[name == branchName].head
					newBranch.moveTo(newBranch.container, refBranch.x, refBranch.y)
					
					for (edge : refBranch.outgoingControlFlows) {
						edge.reconnectSource(newBranch)
					}
					
					for (port : branch.outputPorts) {
						val replPort = port.inferredReplacement
						if (replPort !== null) {
							val refPort = port.referencedObject as info.scce.dime.process.process.OutputPort
							val newPort = newBranch.findThe(OutputPort)[name == replPort.cachedReferencedObjectName]
							for (edge : refPort.outgoing) {
								edge.reconnectSource(newPort)
							}
						} else {
							warn('''OutputPort '«port.name»' of branch '«branch.name»' of SIB '«branch.sib.name»' has no replacement''')
						}
					}
					
					majorBranchSIBs.forEach[majorBranch = newBranch]
					
				} else {
					warn('''Branch '«branch.name»' of SIB '«branch.sib.name»' has no replacement''')
				}
			}
			
			defaultContentSIBs.forEach[defaultContent = newSib]
			
			refSib.delete
			
			if (saveModel) model.save
			success = true
		}
	}
	
	dispatch def performReplacement(ProcessBlueprintSIB sib) {
		val refSib = sib.referencedElement
		if (refSib !== null) {
			val model = refSib.rootElement
			
			val defaultContentSIBs
				= model.find(info.scce.dime.process.process.SIB)
					.filter[hasDefaultContent(refSib)]
					.toSet
					
			val replSib = sib.getInferredReplacement
			if (!(replSib instanceof ProcessSIB)) {
				return null;
			}
			val replProcess = (replSib as ProcessSIB)?.referencedObject
			
			val newSib = model.newProcessSIB(replProcess, refSib.x, refSib.y, refSib.width, refSib.height) => [
				label = refSib.label
				name = refSib.name
			]
			
			for (edge : refSib.incoming) {
				edge.reconnectTarget(newSib)
			}
			
			for (port : sib.inputPorts) {
				val replPort = port.inferredReplacement
				if (replPort !== null) {
					val refPort = port.referencedObject as info.scce.dime.process.process.InputPort
					val newPort = newSib.findThe(InputPort)[name == replPort.referencedObjectName]
					for (edge : refPort.incoming) {
						edge.reconnectTarget(newPort)
					}
				} else {
					warn('''InputPort '«port.name»' of SIB '«sib.name»' has no replacement''')
				}
			}
			
			for (branch : sib.blueprintBranchs) {
				val replBranch = branch.inferredReplacement
				if (replBranch !== null) {
					val refBranch = branch.referencedBranch as BranchBlueprint
					
					val majorBranchSIBs
						= model.find(info.scce.dime.process.process.SIB)
							.filter[hasMajorBranch(refBranch)]
							.toSet
					
					val branchName = replBranch.cachedReferencedObjectName
					val newBranch = newSib.branchSuccessors.filter[name == branchName].head
					newBranch.moveTo(newBranch.container, refBranch.x, refBranch.y)
					
					for (edge : refBranch.outgoingControlFlows) {
						edge.reconnectSource(newBranch)
					}
					
					for (port : branch.outputPorts) {
						val replPort = port.inferredReplacement
						if (replPort !== null) {
							val refPort = port.referencedObject as info.scce.dime.process.process.OutputPort
							val newPort = newBranch.findThe(OutputPort)[name == replPort.cachedReferencedObjectName]
							for (edge : refPort.outgoing) {
								edge.reconnectSource(newPort)
							}
						} else {
							warn('''OutputPort '«port.name»' of branch '«branch.name»' of SIB '«branch.sib.name»' has no replacement''')
						}
					}
					
					majorBranchSIBs.forEach[majorBranch = newBranch]
					
				} else {
					warn('''Branch '«branch.name»' of SIB '«branch.sib.name»' has no replacement''')
				}
			}
			
			defaultContentSIBs.forEach[defaultContent = newSib]
			
			refSib.delete
			
			if (saveModel) model.save
			success = true
		}
	}
	
	
}
