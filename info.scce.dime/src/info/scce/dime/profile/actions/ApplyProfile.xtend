/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.profile.actions

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.Profile
import info.scce.dime.profile.util.ReplacementStrategy
import org.eclipse.emf.common.util.URI

class ApplyProfile  extends CincoCustomAction<Profile> {
	
	extension ProfileExtension = new ProfileExtension
	extension ReplacementStrategy = new ReplacementStrategy
	
	val replacementAffectedURIs = <URI> newHashSet
	
	override getName() {
		"Apply Profile"
	}
	
	override execute(Profile profile) {
		profile.blueprintSIBs
			.partition[isReplacable]
			.matching[
				forEach[replace]
			]
			.nonMatching[
				forEach[println(
					"Skipping replacement of Blueprint SIB " + it?.name
					+ " in Profile " + profile.name
					+ " (replacement conditions not fulfilled)"
				)]
			]
	}
	
	def isReplacable(BlueprintSIB it) {
		replacementConditionsFulfilled
	}
	
	def replace(BlueprintSIB blueprintSIB) {
		println("[INFO] Replacing " + blueprintSIB.cachedReferencedBlueprintSIBLabel)
		val refSib = blueprintSIB.getReferencedObject
		val uri = refSib?.eResource?.URI
		if (uri !== null) {
			replacementAffectedURIs.add(uri)
		}
		new Replace().performReplacement(blueprintSIB)
	}
	
	def cleanupRegistry() {
		replacementAffectedURIs.forEach[ uri |
			println("[INFO] Cleanup replacements of " + uri)
			val ids = ReferenceRegistry.getInstance().unregister(uri)
			ids?.forEach[
				ReferenceRegistry.getInstance().unregister(it)
			]
			ReferenceRegistry.getInstance().enqueue(uri)
		]
	}
}
