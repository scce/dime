stealth import "platform:/resource/info.scce.dime/model/DAD.mgl" as dad
stealth import "platform:/resource/info.scce.dime/model/API.mgl" as api
stealth import "platform:/resource/info.scce.dime/model/Data.mgl" as data 
stealth import "platform:/resource/info.scce.dime/model/Process.mgl" as process 

id info.scce.dime.graphql.schema
stylePath "model/GraphQL.style"

@postCreate("info.scce.dime.graphql.hooks.GraphQLInit")
@mcam("check")
@mcam_checkmodule("info.scce.dime.graphql.checks.DuplicateQueryNameCheck")
@mcam_checkmodule("info.scce.dime.graphql.checks.DuplicateMutationNameCheck")
@mcam_checkmodule("info.scce.dime.graphql.checks.DeclaredInputsCheck")
@mcam_checkmodule("info.scce.dime.graphql.checks.ResolverProcessSignatureCheck")
graphModel GraphQL {
	iconPath "icons/gql.png"
	diagramExtension "gql"

	containableElements (Queries[0,1], Mutations[0,1], ForbiddenTypes[0,1], Resolvers[0,1])

	attr EString as modelName := "GraphQL"
	
}
	
abstract container Entrypoints {
	containableElements(APISIB)
}

@disable(create, move, resize, delete)
container Queries extends Entrypoints {
	style queries	
}


@disable(create, move, resize, delete)
container Mutations extends Entrypoints {
	style mutations	
}


@disable(create, move, resize, delete)
container ForbiddenTypes {
	style forbiddenTypes
	containableElements(Type)
}

/**
 * Container for defining field resolvers.
 */
@disable(create, move, resize, delete)
container Resolvers {
	style resolvers	
	containableElements(Resolver)
}

/**
 * Container for a specific data type that field resolvers are defined for.
 */
@postCreate("info.scce.dime.graphql.hooks.ResolverPostCreateHook")
@postDelete("info.scce.dime.graphql.hooks.ResolverPostDeleteHook")
container Resolver {
	style referencedTypeStyle("${referencedType.name}")
		
	prime data::Type as referencedType
	
	containableElements(ResolverAttribute[1,*])
}

/**
 * Container for a specific data attribute that field resolvers are defined for.
 */
@postCreate("info.scce.dime.graphql.hooks.ResolverAttributePostCreateHook")
@postDelete("info.scce.dime.graphql.hooks.ResolverAttributePostDeleteHook")
container ResolverAttribute {
	style resolverAttribute(
		"${referencedAttribute.name}",
		"${referencedAttribute.isList ? '[' : ''}${referencedAttribute.dataType.name}${referencedAttribute.isList ? ']' : ''}")
	
	prime data::TypeAttribute as referencedAttribute
	
	containableElements(ResolverProcess[1,1])
}

/** 
 * The reference to the process that is called for a data attribute.
 */
@postCreate("info.scce.dime.graphql.hooks.ResolverProcessPostCreateHook")
@postDelete("info.scce.dime.graphql.hooks.ResolverProcessPostDeleteHook")
@doubleClickAction("info.scce.dime.graphql.actions.OpenModel")
container ResolverProcess {
	style resolverProcess("${label}")
	
	attr EString as label := "Process"
		
	prime process::Process as resolverProcess
}

@doubleClickAction("info.scce.dime.graphql.hooks.OpenApiModel")
@postCreate("info.scce.dime.graphql.hooks.APISIBPostCreateHook")
@postDelete("info.scce.dime.graphql.hooks.APISIBPostDeleteHook")
@contextMenuAction("info.scce.dime.graphql.actions.APISIBUpdate")
@disable(move, resize)
container APISIB {
	style apiSIB("${model.modelName}")
	
	attr EBoolean as authenticated:="true"
	prime api::API as model
	
	containableElements(
		ApiInput[0,*],
		ApiOutput[0,*],
		ApiError[0,*]
	)
}


abstract node APISIBIO {
	@mcam_label
	@readOnly
	attr EString as name := "IOelement"
}

@disable(create, move, resize, delete)
node ApiInput extends APISIBIO {
	style apiInput("${name}")
	@readOnly
	attr EBoolean as required := false
}

@disable(create, move, resize, delete)
node ApiOutput extends APISIBIO {
	style apiOutput("${name}")
	@readOnly
	attr EBoolean as notNull := false
}

@disable(create, move, resize, delete)
node ApiError extends APISIBIO {
	style apiError("${name}")
}


/**
 * Super type for all types (e.g. abstract/concrete/enum). 
 * Contains attributes and is connected to other Types with Associations
 */

abstract container Type {
	
	@mcam_label
	@readOnly
	attr EString as name := "Type"
	
	attr EBoolean as input := "false"
	
	@multiLine
	attr EString as documentation

	containableElements(
		Attribute[0,*]
	)
}

  
/*
 * Super type for anything that is placed under the horizontal line
 * of a Type. This comprises primitive attributes, attributes that represent
 * an association (i.e. complex attributes) as well as literals of an EnumType
 */
abstract node Attribute {
	attr EBoolean as forbidden := "false"
	attr EBoolean as required := "false"
}

@postCreate("info.scce.dime.graphql.hooks.ReferencedTypePostCreate")
@postDelete("info.scce.dime.graphql.hooks.ReferencedTypePostDelete")
@disable(move, resize)
container ForbiddenType extends Type {
	style referencedTypeStyle("${referencedType.name}")
		
	prime data::InheritorType as referencedType
	containableElements ( Attribute )
}

@postCreate("info.scce.dime.graphql.hooks.ReferencedTypePostCreate")
@postDelete("info.scce.dime.graphql.hooks.ReferencedTypePostDelete")
@disable(move, resize)
container ForbiddenUserType extends Type {
	style referencedUserTypeStyle("${referencedType.name}")
	prime data::UserType as referencedType
	containableElements ( Attribute )
}


/*
 * Super type for all attributes that instantiate a type. This is everything
 * except EnumLiteral
 */
abstract node TypeAttribute extends Attribute {
}

@icon("icons/attribute.png")
@disable(create, move, resize, delete)
node ForbiddenPrimitiveAttribute extends TypeAttribute {
	style attribute("${attribute.name}", "${attribute.isList ? '[' : ''}${attribute.dataType.name}${attribute.isList ? ']' : ''}${required ? '!' : ''}")
	
    prime data::PrimitiveAttribute as attribute 
}


@disable(create, move, resize, delete)
node ForbiddenComplexAttribute extends TypeAttribute {
	style attribute("${attribute.name}", "${attribute.isList ? '[' : ''}${attribute.dataType.name}${attribute.isList ? ']' : ''}${required ? '!' : ''}")
	
	prime data::ComplexAttribute as attribute
}
