stealth import "platform:/resource/info.scce.dime/model/Process.mgl" as process 
stealth import "platform:/resource/info.scce.dime/model/Data.mgl" as data 

id info.scce.dime.graphql.api
stylePath "model/API.style"

@postCreate("info.scce.dime.graphql.hooks.APIInit")
@mcam("check")
@mcam_checkmodule("info.scce.dime.graphql.checks.ProcessHasGUISIBCheck")
@mcam_checkmodule("info.scce.dime.graphql.checks.ApiEndSIBPortCheck")
@mcam_checkmodule("info.scce.dime.graphql.checks.GraphQLIdentifierCheck")
graphModel API {
	iconPath "icons/graphql/apiSIB.png"
	diagramExtension "api"
	
	containableElements(StartSIB[1,1], EndSIB[1,1], ProcessSIB[1,1], ErrorSIB[0,*], AbstractBranch[0,*])
	
	attr EString as modelName := "unnamed model"
}
 

// super type for Input, Output, and TypeIO
abstract container IO {
}

edge ControlFlow {
	style controlFlow
	
	@propertiesViewHidden
	attr EBoolean as concurrent := true
}

abstract container Input extends IO {
	@mcam_label
	attr EString as name := "output" // Invert because output of StartSIB is input of GraphQL
}

abstract container DataFlowTarget {
	incomingEdges (ControlFlow[1,*])
}

abstract container Output extends IO {
	@mcam_label
	attr EString as name := "input" // Invert because input of EndSIB is output of GraphQL
}

abstract container DataFlowSource {
	containableElements (Output[0,*])
	outgoingEdges (ControlFlow[1,1])
}


@disable(delete,reconnect)
edge BranchConnector {
	style branchConnector
	
	@propertiesViewHidden
	attr EBoolean as concurrent := true
}


abstract container AbstractBranch extends DataFlowSource {
	@mcam_label
	attr EString as name
	incomingEdges (BranchConnector[1,1])
}


@disable(create)
container Branch extends AbstractBranch {
	style branch("${name}")
}

abstract container SIB extends DataFlowTarget {
	containableElements (Input[0,*])
	outgoingEdges (BranchConnector[0,*])
	@mcam_label
	attr EString as label := "label"
	attr EString as name
}


@palette("Interface SIBs")
@doubleClickAction("info.scce.dime.graphql.actions.StartSIBDoubleclickAction") 
@postResize("info.scce.dime.graphql.hooks.APIStartSIBPostResizeHook")
@icon("icons/process/palette/startSIB.png")
container StartSIB {
	style startSIB
	
	containableElements (APIinputPort[0,*])
	outgoingEdges (ControlFlow[1,1])
	
	attr EString as branchName := "start"
}

@palette("Interface SIBs")
@doubleClickAction("info.scce.dime.graphql.actions.EndSIBDoubleclickAction")
@postResize("info.scce.dime.graphql.hooks.APIEndSIBPostResizeHook")
@icon("icons/process/palette/endSIB.png")
container EndSIB extends DataFlowTarget {
	style endSIB("${branchName}")
	
	containableElements (APIoutputPort[1,1])
	attr EString as branchName := "success"
}

@palette("Interface SIBs")
@icon("icons/graphql/errorPort.png")
container ErrorSIB extends DataFlowTarget {
	style errorSIB("${branchName}")
	
	attr EString as branchName := "error"
	attr EString as errorCode := "errorCode"
	attr EString as errorMessage := "errorMessage"
} 


@preDelete("info.scce.dime.graphql.hooks.ProcessSIBPreDelete")
@doubleClickAction("info.scce.dime.graphql.actions.OpenModel")
@contextMenuAction("info.scce.dime.graphql.actions.ProcessSIBUpdate")
@postCreate("info.scce.dime.graphql.hooks.ProcessSIBPostCreateHook")
container ProcessSIB extends SIB {
	style processSIB("${label}")
	
	prime process::Process as proMod
}


enum PrimitiveType {
	Text
	Integer
	Real
	Boolean
	Timestamp
	File
}

abstract container OutputPort extends Output {
	attr EBoolean as isList := "false"
}

// For StartSIB
abstract container APIinputPort extends Output {
	attr EBoolean as isList := "false"
	attr EBoolean as required := "false"
}

abstract edge DataFlow {
	@propertiesViewHidden
	attr EBoolean as dfViewWorkaround := true
}

abstract edge DirectDataFlow extends DataFlow { }


@postCreate("info.scce.dime.graphql.hooks.DirectDataFlowPostCreate")
edge PrimitiveDirectDataFlow extends DirectDataFlow {
	style dataFlow
}

@disable(create,move,resize)
@palette("Ports")
@icon("icons/process/palette/outputPort.png")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
container PrimitiveOutputPort extends OutputPort {
	style outputPort("${name}", "${isList ? '[' : ''}", "${dataType}", "${isList ? ']' : ''}")
	
	attr PrimitiveType as dataType
	outgoingEdges (PrimitiveDirectDataFlow[0,*])
}


// For StartSIB
@disable(move,resize)
@palette("Ports")
@icon("icons/process/palette/outputPort.png")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
container PrimitiveAPIinput extends APIinputPort {
	style APIinputPort("${name}", "${isList ? '[' : ''}", "${dataType}", "${isList ? ']' : ''}")
	
	attr PrimitiveType as dataType
	outgoingEdges (PrimitiveDirectDataFlow[0,*])
}

@disable(move,resize)
@palette("Ports")
@icon("icons/process/palette/outputPort.png")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
@postCreate("info.scce.dime.graphql.hooks.PortPostCreateHook")
container ComplexAPIinput extends APIinputPort {
	style APIinputPort("${name}", "${isList ? '[' : ''}", "${dataType.name}", "${isList ? ']' : ''}")
	
	outgoingEdges ({ComplexDirectDataFlow}[0,*])
	prime data::Type as dataType
}


@disable(move,resize)
@postCreate("info.scce.dime.graphql.hooks.OutputPortHook")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
container ComplexOutputPort extends OutputPort {
	style outputPort("${name}", "${isList ? '[' : ''}", "${dataType.name}", "${isList ? ']' : ''}")

	outgoingEdges ({ComplexDirectDataFlow}[0,*])
	prime data::Type as dataType
}


//Starting here everything is imported for InputPorts

abstract container InputPort extends Input {
    attr EBoolean as isList := "false"
}
 
// For EndSIB
abstract container APIoutputPort extends Input {
	attr EBoolean as notNull := "false"
    attr EBoolean as isList := "false"
}

@disable(create,move,resize)
@palette("Ports")
@icon("icons/process/palette/inputPort.png")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
container PrimitiveInputPort extends InputPort {
	style inputPort("${name}", "${isList ? '[' : ''}", "${dataType}", "${isList ? ']' : ''}")

    attr PrimitiveType as dataType
    incomingEdges (PrimitiveDirectDataFlow[0,1])
}

@postCreate("info.scce.dime.graphql.hooks.DirectDataFlowPostCreate")
edge ComplexDirectDataFlow extends DirectDataFlow {
	style dataFlow
}

@disable(move,resize)
@postCreate("info.scce.dime.graphql.hooks.InputPortHook")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
container ComplexInputPort extends InputPort {
	style inputPort("${name}", "${isList ? '[' : ''}", "${dataType.name}", "${isList ? ']' : ''}")
	
	incomingEdges ({ComplexDirectDataFlow}[0,1])
	prime data::Type as dataType
}

// For EndSIB
@disable(create,move,resize)
@palette("Ports")
@icon("icons/process/palette/inputPort.png")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
container PrimitiveAPIoutput extends APIoutputPort {
	style APIoutputPort("${name}", "${isList ? '[' : ''}", "${dataType}", "${isList ? ']' : ''}")

    attr PrimitiveType as dataType
    incomingEdges (PrimitiveDirectDataFlow[0,1])
}

@disable(move,resize)
@postCreate("info.scce.dime.graphql.hooks.APIoutputPortHook")
@preDelete("info.scce.dime.graphql.hooks.PortPreDeleteHook")
container ComplexAPIoutput extends APIoutputPort {
	style APIoutputPort("${name}", "${isList ? '[' : ''}", "${dataType.name}", "${isList ? ']' : ''}")
	
	incomingEdges ({ComplexDirectDataFlow}[0,1])
	prime data::Type as dataType
}