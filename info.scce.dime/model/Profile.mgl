stealth import "platform:/resource/info.scce.dime/model/GUI.mgl" as gui
stealth import "platform:/resource/info.scce.dime/model/Process.mgl" as process
stealth import "platform:/resource/info.scce.dime.gui.plugin/model/GUIPlugin.ecore" as guiplugin

id info.scce.dime.profile
stylePath "model/Profile.style"

@mcam("check")
@mcam_checkmodule("info.scce.dime.profile.checks.ReplacementCheck")

@postCreate("info.scce.dime.profile.hooks.PostCreateProfile")
graphModel Profile {
	iconPath "icons/dime-green.png"
	diagramExtension "profile"
	
	containableElements(*)
	
	attr EString as name := "New Profile"
}

// ++++++++++++++++++
//
//    Input Ports
//
// ++++++++++++++++++

abstract container IO {
	@propertiesViewHidden attr EString as cachedReferencedObjectName
	@propertiesViewHidden attr EString as cachedReferencedObjectDataTypeName
}

abstract container InputPort extends IO {
	outgoingEdges(InputPortMapping[0,*])
	incomingEdges(InputPortMapping[0,*])
}

abstract container InputPortReferencingInputPort extends InputPort {}

@disable(create,move,resize,delete)
container PrimitiveInputPortReferencingInputPort extends InputPortReferencingInputPort {
	style inputPort(
		"${ empty referencedPrimitiveInputPort.name ? cachedReferencedObjectName : referencedPrimitiveInputPort.name }",
		"${ empty referencedPrimitiveInputPort ? '' : referencedPrimitiveInputPort.isList ? '[' : '' }",
		"${ empty referencedPrimitiveInputPort.dataType ? cachedReferencedObjectDataTypeName : referencedPrimitiveInputPort.dataType }",
		"${ empty referencedPrimitiveInputPort ? '' : referencedPrimitiveInputPort.isList ? ']' : '' }"
		)
	
	prime process::InputPort as referencedPrimitiveInputPort
}

@disable(create,move,resize,delete)
container ComplexInputPortReferencingInputPort extends InputPortReferencingInputPort {
	style inputPort(
		"${ empty referencedComplexInputPort.name ? cachedReferencedObjectName : referencedComplexInputPort.name }",
		"${ empty referencedComplexInputPort ? '' : referencedComplexInputPort.isList ? '[' : '' }",
		"${ empty referencedComplexInputPort.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexInputPort.dataType.name }",
		"${ empty referencedComplexInputPort ? '' : referencedComplexInputPort.isList ? ']' : '' }"
		)
	prime process::InputPort as referencedComplexInputPort
}

abstract container VariableReferencingInputPort extends InputPort {}

@disable(create,move,resize,delete)
container PrimitiveVariableReferencingInputPort extends VariableReferencingInputPort {
	style inputPort(
		"${ empty referencedPrimitiveVariable.name ? cachedReferencedObjectName : referencedPrimitiveVariable.name }",
		"${ empty referencedPrimitiveVariable ? '' : referencedPrimitiveVariable.isList ? '[' : '' }",
		"${ empty referencedPrimitiveVariable.dataType ? cachedReferencedObjectDataTypeName : referencedPrimitiveVariable.dataType }",
		"${ empty referencedPrimitiveVariable ? '' : referencedPrimitiveVariable.isList ? ']' : '' }"
		)
	
	prime gui::Variable as referencedPrimitiveVariable
}

@disable(create,move,resize,delete)
container ComplexVariableReferencingInputPort extends VariableReferencingInputPort {
	style inputPort(
		"${ empty referencedComplexVariable.name ? cachedReferencedObjectName : referencedComplexVariable.name }",
		"${ empty referencedComplexVariable ? '' : referencedComplexVariable.isList ? '[' : '' }",
		"${ empty referencedComplexVariable.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexVariable.dataType.name }",
		"${ empty referencedComplexVariable ? '' : referencedComplexVariable.isList ? ']' : '' }"
		)
	
	prime gui::Variable as referencedComplexVariable
}

abstract container OutputPortReferencingInputPort extends InputPort {}

@disable(create,move,resize,delete)
container PrimitiveOutputPortReferencingInputPort extends OutputPortReferencingInputPort {
	style inputPort(
		"${ empty referencedPrimitiveOutputPort.name ? cachedReferencedObjectName : referencedPrimitiveOutputPort.name }",
		"${ empty referencedPrimitiveOutputPort ? '' : referencedPrimitiveOutputPort.isList ? '[' : '' }",
		"${ empty referencedPrimitiveOutputPort.dataType ? cachedReferencedObjectDataTypeName : referencedPrimitiveOutputPort.dataType }",
		"${ empty referencedPrimitiveOutputPort ? '' : referencedPrimitiveOutputPort.isList ? ']' : '' }"
		)
	
	prime process::OutputPort as referencedPrimitiveOutputPort
}

@disable(create,move,resize,delete)
container ComplexOutputPortReferencingInputPort extends OutputPortReferencingInputPort {
	style inputPort(
		"${ empty referencedComplexOutputPort.name ? cachedReferencedObjectName : referencedComplexOutputPort.name }",
		"${ empty referencedComplexOutputPort ? '' : referencedComplexOutputPort.isList ? '[' : '' }",
		"${ empty referencedComplexOutputPort.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexOutputPort.dataType.name }",
		"${ empty referencedComplexOutputPort ? '' : referencedComplexOutputPort.isList ? ']' : '' }"
		)
	
	prime process::OutputPort as referencedComplexOutputPort
}

// ++++++++++++++++++
//
//    Output Ports
//
// ++++++++++++++++++

abstract container OutputPort extends IO {
	outgoingEdges(OutputPortMapping[0,1])
	incomingEdges(OutputPortMapping[0,*])
}

abstract container OutputPortReferencingOutputPort extends OutputPort {}

@disable(create,move,resize,delete)
container PrimitiveOutputPortReferencingOutputPort extends OutputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedPrimitiveOutputPort.name ? cachedReferencedObjectName  : referencedPrimitiveOutputPort.name }",
		"${ empty referencedPrimitiveOutputPort ? '' : referencedPrimitiveOutputPort.isList ? '[' : '' }",
		"${ empty referencedPrimitiveOutputPort.dataType ? cachedReferencedObjectDataTypeName  : referencedPrimitiveOutputPort.dataType }",
		"${ empty referencedPrimitiveOutputPort ? '' : referencedPrimitiveOutputPort.isList ? ']' : '' }"
		)
	
	prime process::OutputPort as referencedPrimitiveOutputPort
}

@disable(create,move,resize,delete)
container ComplexOutputPortReferencingOutputPort extends OutputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedComplexOutputPort.name ? cachedReferencedObjectName : referencedComplexOutputPort.name }",
		"${ empty referencedComplexOutputPort ? '' : referencedComplexOutputPort.isList ? '[' : '' }",
		"${ empty referencedComplexOutputPort.dataType.name  ? cachedReferencedObjectDataTypeName : referencedComplexOutputPort.dataType.name }",
		"${ empty referencedComplexOutputPort ? '' : referencedComplexOutputPort.isList ? ']' : '' }"
		)
	
	prime process::OutputPort as referencedComplexOutputPort
}

abstract container InputPortReferencingOutputPort extends OutputPort {}

@disable(create,move,resize,delete)
container PrimitiveInputPortReferencingOutputPort extends InputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedPrimitiveInputPort.name ? cachedReferencedObjectName : referencedPrimitiveInputPort.name }",
		"${ empty referencedPrimitiveInputPort ? '' : referencedPrimitiveInputPort.isList ? '[' : '' }",
		"${ empty referencedPrimitiveInputPort.dataType ? cachedReferencedObjectDataTypeName : referencedPrimitiveInputPort.dataType }",
		"${ empty referencedPrimitiveInputPort ? '' : referencedPrimitiveInputPort.isList ? ']' : '' }"
		)
	
	prime process::InputPort as referencedPrimitiveInputPort
}

@disable(create,move,resize,delete)
container ComplexInputPortReferencingOutputPort extends InputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedComplexInputPort.name ? cachedReferencedObjectName : referencedComplexInputPort.name }",
		"${ empty referencedComplexInputPort ? '' : referencedComplexInputPort.isList ? '[' : '' }",
		"${ empty referencedComplexInputPort.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexInputPort.dataType.name }",
		"${ empty referencedComplexInputPort ? '' : referencedComplexInputPort.isList ? ']' : '' }"
		)
	
	prime process::InputPort as referencedComplexInputPort
}

abstract container GUIInputPortReferencingOutputPort extends OutputPort {}

@disable(create,move,resize,delete)
container GUIPrimitiveInputPortReferencingOutputPort extends GUIInputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedPrimitiveInputPort.name ? cachedReferencedObjectName : referencedPrimitiveInputPort.name }",
		"${ empty referencedPrimitiveInputPort ? '' : referencedPrimitiveInputPort.isList ? '[' : '' }",
		"${ empty referencedPrimitiveInputPort.dataType.name ? cachedReferencedObjectDataTypeName : referencedPrimitiveInputPort.dataType.name }",
		"${ empty referencedPrimitiveInputPort ? '' : referencedPrimitiveInputPort.isList ? ']' : '' }"
		)
	
	prime gui::PrimitiveInputPort as referencedPrimitiveInputPort 
}

@disable(create,move,resize,delete)
container GUIComplexInputPortReferencingOutputPort extends GUIInputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedComplexInputPort.name ? cachedReferencedObjectName : referencedComplexInputPort.name }",
		"${ empty referencedComplexInputPort ? '' : referencedComplexInputPort.isList ? '[' : '' }",
		"${ empty referencedComplexInputPort.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexInputPort.dataType.name }",
		"${ empty referencedComplexInputPort ? '' : referencedComplexInputPort.isList ? ']' : '' }"
		)
	prime gui::ComplexInputPort as referencedComplexInputPort 
}

abstract container GUIOutputPortReferencingOutputPort extends OutputPort {}

@disable(create,move,resize,delete)
container GUIPrimitiveOutputPortReferencingOutputPort extends GUIOutputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedPrimitiveOutputPort.name ? cachedReferencedObjectName : referencedPrimitiveOutputPort.name }",
		"${ empty referencedPrimitiveOutputPort ? '' : referencedPrimitiveOutputPort.isList ? '[' : '' }",
		"${ empty referencedPrimitiveOutputPort.dataType.name ? cachedReferencedObjectDataTypeName : referencedPrimitiveOutputPort.dataType.name }",
		"${ empty referencedPrimitiveOutputPort ? '' : referencedPrimitiveOutputPort.isList ? ']' : '' }"
		)
	
	prime gui::PrimitiveOutputPort as referencedPrimitiveOutputPort 
}

@disable(create,move,resize,delete)
container GUIComplexOutputPortReferencingOutputPort extends GUIOutputPortReferencingOutputPort {
	style outputPort(
		"${ empty referencedComplexOutputPort.name ? cachedReferencedObjectName : referencedComplexOutputPort.name }",
		"${ empty referencedComplexOutputPort ? '' : referencedComplexOutputPort.isList ? '[' : '' }",
		"${ empty referencedComplexOutputPort.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexOutputPort.dataType.name }",
		"${ empty referencedComplexOutputPort ? '' : referencedComplexOutputPort.isList ? ']' : '' }"
		)
	
	prime gui::ComplexOutputPort as referencedComplexOutputPort 
}

abstract container AttributeReferencingOutputPort extends OutputPort {}

@disable(create,move,resize,delete)
container PrimitiveAttributeReferencingOutputPort extends AttributeReferencingOutputPort {
	style outputPort(
		"${ empty referencedPrimitiveAttribute.name ? cachedReferencedObjectName : referencedPrimitiveAttribute.name }",
		"${ empty referencedPrimitiveAttribute ? '' : referencedPrimitiveAttribute.isList ? '[' : '' }",
		"${ empty referencedPrimitiveAttribute.dataType.name ? cachedReferencedObjectDataTypeName : referencedPrimitiveAttribute.dataType.name }",
		"${ empty referencedPrimitiveAttribute ? '' : referencedPrimitiveAttribute.isList ? ']' : '' }"
		)
	
	prime gui::PrimitiveAttribute as referencedPrimitiveAttribute 
}

@disable(create,move,resize,delete)
container ComplexAttributeReferencingOutputPort extends AttributeReferencingOutputPort {
	style outputPort(
		"${ empty referencedComplexAttribute.name ? cachedReferencedObjectName : referencedComplexAttribute.name }",
		"${ empty referencedComplexAttribute ? '' : referencedComplexAttribute.isList ? '[' : '' }",
		"${ empty referencedComplexAttribute.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexAttribute.dataType.name }",
		"${ empty referencedComplexAttribute ? '' : referencedComplexAttribute.isList ? ']' : '' }"
		)
	
	prime gui::ComplexAttribute as referencedComplexAttribute 
}

abstract container VariableReferencingOutputPort extends OutputPort {}

@disable(create,move,resize,delete)
container PrimitiveVariableReferencingOutputPort extends VariableReferencingOutputPort {
	style outputPort(
		"${ empty referencedPrimitiveVariable.name ? cachedReferencedObjectName : referencedPrimitiveVariable.name }",
		"${ empty referencedPrimitiveVariable ? '' : referencedPrimitiveVariable.isList ? '[' : '' }",
		"${ empty referencedPrimitiveVariable.dataType ? cachedReferencedObjectDataTypeName : referencedPrimitiveVariable.dataType }",
		"${ empty referencedPrimitiveVariable ? '' : referencedPrimitiveVariable.isList ? ']' : '' }"
		)
	
	prime gui::PrimitiveVariable as referencedPrimitiveVariable
}

@disable(create,move,resize,delete)
container ComplexVariableReferencingOutputPort extends VariableReferencingOutputPort {
	style outputPort(
		"${ empty referencedComplexVariable.name ? cachedReferencedObjectName : referencedComplexVariable.name }",
		"${ empty referencedComplexVariable ? '' : referencedComplexVariable.isList ? '[' : '' }",
		"${ empty referencedComplexVariable.dataType.name ? cachedReferencedObjectDataTypeName : referencedComplexVariable.dataType.name }",
		"${ empty referencedComplexVariable ? '' : referencedComplexVariable.isList ? ']' : '' }"
		)
	
	prime gui::ComplexVariable as referencedComplexVariable
}

@disable(create,move,resize,delete)
container OutputGenericReferencingOutputPort extends VariableReferencingOutputPort {
	style outputPort(
		"${ empty referencedOutputGeneric.name ? cachedReferencedObjectName : referencedOutputGeneric.name }",
		"${ empty referencedOutputGeneric ? '' : referencedOutputGeneric.isList ? '[' : '' }",
		"${ empty referencedOutputGeneric.dataType.name ? cachedReferencedObjectDataTypeName : referencedOutputGeneric.dataType.name }",
		"${ empty referencedOutputGeneric ? '' : referencedOutputGeneric.isList ? ']' : '' }"
		)
	
	prime gui::OutputGeneric as referencedOutputGeneric
}

@disable(create,move,resize,delete)
container AddToSubmissionReferencingOutputPort extends OutputPort {
	style outputPort(
		"${ empty referencedAddToSubmission.name ? cachedReferencedObjectName : referencedAddToSubmission.name }",
		"${ empty referencedAddToSubmission ? '' : referencedAddToSubmission.isList ? '[' : '' }",
		"${ empty referencedAddToSubmission.dataType.name ? cachedReferencedObjectDataTypeName : referencedAddToSubmission.dataType.name }",
		"${ empty referencedAddToSubmission ? '' : referencedAddToSubmission.isList ? ']' : '' }"
		)
	
	prime gui::AddToSubmission as referencedAddToSubmission
}

@disable(create,move,resize,delete)
container AbstractParameterReferencingOutputPort extends OutputPort {
	style outputPort(
		"${ empty referencedAbstractParameter.name ? cachedReferencedObjectName : referencedAbstractParameter.name }",
		"${ empty referencedAbstractParameter ? '' : referencedAbstractParameter.isList ? '[' : '' }",
		"${ empty referencedAbstractParameter.dataType.name ? cachedReferencedObjectDataTypeName : referencedAbstractParameter.dataType.name }",
		"${ empty referencedAbstractParameter ? '' : referencedAbstractParameter.isList ? ']' : '' }"
		)
	
	prime guiplugin.AbstractParameter as referencedAbstractParameter
}

// ++++++++++++++++++
//
//        SIBs
//
// ++++++++++++++++++

abstract container SIB {
	containableElements(InputPort[0,*], Branch[0,*])
	outgoingEdges(SIBReplacement[0,1], ReplacementCondition[0,*])
}

abstract container ReplacementSIB extends SIB {
	incomingEdges(SIBReplacement[0,*], ReplacementCondition[0,*])
	
	@mcam_label
	@propertiesViewHidden
	attr EString as cachedReferencedObjectName
}

abstract container BlueprintSIB extends SIB {
	
	@mcam_label
	@propertiesViewHidden
	attr EString as cachedReferencedBlueprintSIBLabel
	
	@propertiesViewHidden
	attr EString as cachedReferencedBlueprintSIBsModelName
}

@postCreate("info.scce.dime.profile.hooks.PostCreateSIB")
@postResize("info.scce.dime.profile.hooks.PostResizeSIB")
@doubleClickAction("info.scce.dime.profile.actions.OpenModel")
@contextMenuAction("info.scce.dime.profile.actions.UpdateSIB")
@contextMenuAction("info.scce.dime.profile.actions.Replace")
@contextMenuAction("info.scce.dime.profile.actions.RefactorModel")
container GUISIB extends ReplacementSIB {
	style guiSIB(
		"${ empty referencedGUI.title ? cachedReferencedObjectName : referencedGUI.title }"
		)
	
	prime gui::GUI as referencedGUI
}

@postCreate("info.scce.dime.profile.hooks.PostCreateSIB")
@postResize("info.scce.dime.profile.hooks.PostResizeSIB")
@doubleClickAction("info.scce.dime.profile.actions.OpenModel")
@contextMenuAction("info.scce.dime.profile.actions.UpdateSIB")
@contextMenuAction("info.scce.dime.profile.actions.Replace")
@contextMenuAction("info.scce.dime.profile.actions.RefactorModel")
container ProcessSIB extends ReplacementSIB {
	style processSIB(
		"${ empty referencedProcess.modelName ? cachedReferencedObjectName : referencedProcess.modelName }"
		)
	
	prime process::Process as referencedProcess
}

// ++++++++++++++++++
//
//     Branches
//
// ++++++++++++++++++

abstract container Branch {
	containableElements (OutputPort[0,*])
	outgoingEdges(BranchReplacement[0,*])
	
	@mcam_label
	@propertiesViewHidden
	attr EString as cachedReferencedObjectName
}

abstract container ReplacementBranch extends Branch {
	incomingEdges (BranchReplacement[0,*])
}

@disable(create,move,resize,delete)
container BranchReferencingBranch extends ReplacementBranch {
	style branch(
		"${ empty referencedBranch.name ? cachedReferencedObjectName : referencedBranch.name }"
		)
	
	prime gui::Branch as referencedBranch
}

@disable(create,move,resize,delete)
container ButtonReferencingBranch extends ReplacementBranch {
	style branch(
		"${ empty referencedButton.label ? cachedReferencedObjectName : referencedButton.label }"
		)
	
	prime gui::Button as referencedButton
}

@disable(create,move,resize,delete)
container EndSIBReferencingBranch extends ReplacementBranch {
	style branch(
		"${ empty referencedEndSIB.branchName ? cachedReferencedObjectName : referencedEndSIB.branchName }"
		)
	
	prime process::EndSIB as referencedEndSIB
}

@disable(create,move,resize,delete)
container OutputReferencingBranch extends ReplacementBranch {
	style branch(
		"${ empty referencedOutput.outputName ? cachedReferencedObjectName : referencedOutput.outputName }"
		)
	
	prime guiplugin.Output as referencedOutput
}

// ++++++++++++++++++
//
//     Blueprints
//
// ++++++++++++++++++

@postCreate("info.scce.dime.profile.hooks.PostCreateSIB")
@postResize("info.scce.dime.profile.hooks.PostResizeSIB")
@doubleClickAction("info.scce.dime.profile.actions.OpenModel")
@contextMenuAction("info.scce.dime.profile.actions.UpdateSIB")
@contextMenuAction("info.scce.dime.profile.actions.Replace")
container ProcessBlueprintSIB extends BlueprintSIB {
	style processBlueprintSIB(
		"${ empty referencedProcessBlueprintSIB.rootElement.modelName ? cachedReferencedBlueprintSIBsModelName : referencedProcessBlueprintSIB.rootElement.modelName }",
		"${ empty referencedProcessBlueprintSIB.label ? cachedReferencedBlueprintSIBLabel : referencedProcessBlueprintSIB.label }"
		)
	
	prime process::ProcessBlueprintSIB as referencedProcessBlueprintSIB
}

@postCreate("info.scce.dime.profile.hooks.PostCreateSIB")
@postResize("info.scce.dime.profile.hooks.PostResizeSIB")
@doubleClickAction("info.scce.dime.profile.actions.OpenModel")
@contextMenuAction("info.scce.dime.profile.actions.UpdateSIB")
@contextMenuAction("info.scce.dime.profile.actions.Replace")
container GUIBlueprintSIB extends BlueprintSIB {
	style guiBlueprintSIB(
		"${ empty referencedGUIBlueprintSIB.rootElement.modelName ? cachedReferencedBlueprintSIBsModelName : referencedGUIBlueprintSIB.rootElement.modelName }",
		"${ empty referencedGUIBlueprintSIB.label ? cachedReferencedBlueprintSIBLabel : referencedGUIBlueprintSIB.label }"
		)
	
	prime process::GUIBlueprintSIB as referencedGUIBlueprintSIB
}


@disable(create,move,resize,delete)
container BlueprintBranch extends Branch {
	style branch(
		"${ empty referencedBranch.name ? cachedReferencedObjectName : referencedBranch.name }"
		)
	
	prime process::BranchBlueprint as referencedBranch
}

// ++++++++++++++++++
//
//    Static Nodes
//
// ++++++++++++++++++

node DoNotReplace {
	style doNotReplace
	
	incomingEdges(SIBReplacement[1,*])
}

// ++++++++++++++++++
//
//       Edges
//
// ++++++++++++++++++

edge SIBReplacement {
	style sibReplacement
}

edge BranchReplacement {
	style branchReplacement
}

edge InputPortMapping {
	style inputPortMapping
}

edge OutputPortMapping {
	style outputPortMapping
}

edge ReplacementCondition {
	style replacementCondition("${negate ? 'IF NOT' : 'IF'}")
	
	attr EBoolean as negate
}

