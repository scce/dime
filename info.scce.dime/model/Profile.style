appearance defaultEdge {
	lineWidth 2
}

appearance sibReplacementAppearance extends defaultEdge {
	foreground (5,146,40) // dark green
	lineWidth 3
}

appearance branchReplacementAppearance extends defaultEdge {
	foreground (120,120,120) // dark grey
}

appearance inputPortMappingAppearance extends defaultEdge {
	foreground (50,114,185) // blue
}

appearance outputPortMappingAppearance extends defaultEdge {
	foreground (255,170,0) // orange
}

appearance ifEdgeAppearance extends defaultEdge {
	foreground (80,80,80) // dark grey
	lineWidth 2
	lineStyle DOT
}

appearance greyItalicLabel {
	font ("Arial-ItalicMT",10)
	foreground (180,180,180) // grey
}

appearance defaultLabel {
	font ("Sans",10)
}

appearance blueprintLabel {
	font ("Sans",10)
	foreground (41,39,180)
}

appearance branch {
	foreground (120,120,120)
	background (245,245,245)
}

nodeStyle labeledCircle (1){
	ellipse {
		appearance default
		size(40,40)
		text {
			position ( CENTER, MIDDLE )
			value "%s"
		}
	}
}

nodeStyle guiSIB(1) {
	appearanceProvider("info.scce.dime.profile.appearance.SIBAppearance")
	roundedRectangle rootShape {
		size (120,24)
		corner (8,8)
		image {
			position (LEFT 4, TOP 4 )
			size (fix 16, fix 16)
			path ("icons/guiSIB.png")
		}
		text {
			appearance defaultLabel
			position ( LEFT 24, TOP 6 )
			value "%s"
		}
        polyline {
			points [ (7,25) (113,25) ]
		}
	}	
}

nodeStyle processSIB(1) {
	appearanceProvider("info.scce.dime.profile.appearance.SIBAppearance")
	roundedRectangle rootShape {
		size (140,24)
		corner (8,8)
		image {
			position (LEFT 4, TOP 4 )
			size (fix 16, fix 16)
			path ("icons/processSIB.png")
		}
		text {
			appearance defaultLabel
			position ( LEFT 24, TOP 6 )
			value "%s"
		}
        polyline {
			points [ (7,25) (113,25) ]
		}
	}	
}

nodeStyle processBlueprintSIB(2) {
	appearanceProvider("info.scce.dime.profile.appearance.SIBAppearance")
	roundedRectangle rootShape {
		appearance {
			foreground (41,39,180) // blueprint blue
		}
		size (140,34)
		corner (8,8)
		text {
			appearance greyItalicLabel
			position ( RIGHT -5, TOP 2 )
			value "%1$s"
		}
		image {
			position (LEFT 4, TOP 11 )
			size (fix 16, fix 16)
			path ("icons/processBlueprintSIB.png")
		}
		text {
			appearance blueprintLabel
			position ( LEFT 24, TOP 13 )
			value "%2$s"
		}
        polyline {
			points [ (7,34) (113,34) ]
		}
	}	
}

nodeStyle guiBlueprintSIB(2) {
	appearanceProvider("info.scce.dime.profile.appearance.SIBAppearance")
	roundedRectangle rootShape {
		appearance {
			foreground (41,39,180) // blueprint blue
		}
		size (140,34)
		corner (8,8)
		text {
			appearance greyItalicLabel
			position ( RIGHT -5, TOP 2 )
			value "%1$s"
		}
		image {
			position (LEFT 4, TOP 11 )
			size (fix 16, fix 16)
			path ("icons/guiBlueprintSIB.png")
		}
		text {
			appearance blueprintLabel
			position ( LEFT 24, TOP 13 )
			value "%2$s"
		}
        polyline {
			points [ (7,34) (113,34) ]
		}
	}	
}

nodeStyle inputPort(4) {
	appearanceProvider("info.scce.dime.profile.appearance.SIBAppearance")
	rectangle rootShape {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/inputPort.png" )
		}
		text {
			position ( LEFT 16, MIDDLE 1)
			value "%s :%s%s%s"			
		}
	}
}

nodeStyle outputPort(4) {
	appearanceProvider("info.scce.dime.profile.appearance.SIBAppearance")
	rectangle rootShape {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/outputPort.png" )
		}
		text {
			position ( LEFT 16, MIDDLE 1)
			value "%s :%s%s%s"			
		}
	}
}

nodeStyle branch(1) {
	appearanceProvider("info.scce.dime.profile.appearance.SIBAppearance")
	rectangle rootShape {
		appearance branch
		size (120,20)
		text {
			appearance defaultLabel 
			position ( CENTER, TOP 4)
			value "%s"
		}
        polyline hline {
        	appearance branch
			points [ (7,21) (113,21) ]
		}
	}
}

nodeStyle doNotReplace {
	rectangle rootShape {
		size (120,24)
		text {
			appearance {
				font ("Times-Italic",12)
			}
			position ( CENTER, MIDDLE )
			value "do not replace"
		}
	}	
}

edgeStyle sibReplacement {
	// FIXME Appearance provider is not consulted for decorators :(
//	appearanceProvider("info.scce.dime.profile.appearance.SIBReplacementAppearance")
	appearance sibReplacementAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance sibReplacementAppearance
	}
}

edgeStyle branchReplacement {
	appearance branchReplacementAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance branchReplacementAppearance 
	}
}

edgeStyle inputPortMapping {
	appearance inputPortMappingAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance inputPortMappingAppearance 
	}
}

edgeStyle outputPortMapping {
	appearance outputPortMappingAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance outputPortMappingAppearance 
	}
}

edgeStyle replacementCondition(1) {
	appearance ifEdgeAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance ifEdgeAppearance
	}
	decorator {
		movable
		text {
			appearance extends default {
				font ("Sans",10)
			}
			value "%s"
		}
		location (0.5)
	}
}

