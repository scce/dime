appearance default {
	lineWidth 1
	background (255,255,255) // white
}

appearance transparent extends default {
	transparency 1.0
}

appearance labelFont {
	font ("Sans",8)
}

appearance iconButton extends default {
	foreground (255,255,255) // white
}

nodeStyle queries {
	rectangle rootShape{
		appearance extends default {
			background (162, 255, 145)
		}
		size (180,100)
		text label {
			appearance {
				font ("Sans",8)
				foreground (0,0,0)
			}
			position (CENTER,TOP 6)
			value "Queries"
		}
	}
}

nodeStyle mutations {
	rectangle rootShape{
		appearance extends default {
			background (90, 250, 226)
		}
		size (180,100)
		text label {
			appearance {
				font ("Sans",8)
				foreground (0,0,0)
			}
			position (CENTER,TOP 6)
			value "Mutations"
		}
	}
}

nodeStyle forbiddenTypes {
	rectangle rootShape{
		appearance extends default {
			background (250, 115, 97)
		}
		size (180,100)
		text label {
			appearance {
				font ("Sans",8)
				foreground (0,0,0)
			}
			position (CENTER,TOP 6)
			value "Data"
		}
	}
}

nodeStyle resolvers {
	rectangle rootShape{
		appearance extends default {
			background (186, 219, 255)
		}
		size (180,100)
		text label {
			appearance {
				font ("Sans",8)
				foreground (0,0,0)
			}
			position (CENTER,TOP 6)
			value "Resolvers"
		}
	}
}

nodeStyle apiSIB(1) {
	roundedRectangle {
		appearance default
		size (241,71)
		corner (8,8)
		text "label" {
			appearance labelFont
			position ( LEFT 24, TOP 4 )
			value "%s"
		}
        polyline {
        	appearance {
				foreground (102,102,102)
        	}
			points [ (7,30) (234,30) ]
		}
	}
}

nodeStyle apiInput(1) {
	appearanceProvider ( "info.scce.dime.graphql.util.ApiInputRequiredAppearance" ) 
	rectangle {
		appearance {
			transparency 1.0
		}
		size (130,19)
		image {
			position (LEFT 2, MIDDLE) 
			size (fix 16, fix 15)
		 	path ( "icons/process/palette/inputPort.png" )
		}
		text {
			position (LEFT 20, MIDDLE)
			value "%s"
		}
	}
}

nodeStyle processSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/processSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle resolverAttribute(2) {
	roundedRectangle {
		appearance default
		size (140,30)
		corner(10,10)
		text {
			appearance extends labelFont {
				foreground (0,0,0)
			}
		 	position ( LEFT 27, TOP 7 )	
		 	value "%s: %s"
		}
		image {
			position (LEFT 7, TOP 7)
		 	size (fix 16, fix 16)	
		 	path ( "icons/attribute.png" )
		}
        polyline {
        	appearance {
				foreground (102,102,102)
				background (255,251,232)
        	}
			points [ (7,29) (133,29) ]
		}
	}
}

nodeStyle resolverProcess(1) {
	roundedRectangle {
		appearance default
		size (140,30)
		corner(10,10)
		text {
			appearance extends labelFont {
				foreground (0,0,0)
			}
		 	position ( LEFT 27, TOP 7 )	
		 	value "%s"
		}
		image {
			position (LEFT 7, TOP 7)
		 	size (fix 16, fix 16)	
		 	path ( "icons/processSIB.png" )
		}
        polyline {
        	appearance {
				foreground (102,102,102)
				background (255,251,232)
        	}
			points [ (7,29) (133,29) ]
		}
	}
}

nodeStyle apiError(1) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (130,19)
		image {
			position (LEFT 2, MIDDLE) 
			size (fix 16, fix 15)
		 	path ( "icons/graphql/errorPort.png" )
		}
		text {
			position (LEFT 20, MIDDLE)
			value "%s"
		}
	}
}

nodeStyle apiOutput(1) {
	appearanceProvider ( "info.scce.dime.graphql.util.ApiOutputRequiredAppearance" )
	rectangle {
		appearance {
			transparency 1.0
		}
		size (130,19)
		image {
			position (LEFT 2, MIDDLE) 
			size (fix 16, fix 15)
		 	path ( "icons/process/palette/outputPort.png" )
		}
		text {
			position (LEFT 20, MIDDLE)
			value "%s"
		}
	}
}

nodeStyle forbiddenType(1) {
	roundedRectangle {
		appearance default
		size (120,24)
		corner (8,8)
		text "label" {
			appearance labelFont
			position ( LEFT 24, TOP 6 )
			value "%s"
		}
	}
}

nodeStyle referencedUserTypeStyle(1) {
	roundedRectangle {
		appearance default
		size (140,30)
		corner(10,10)
		text {
			appearance extends labelFont {
				foreground (102,102,102)
			}
		 	position ( LEFT 27, TOP 7 )	
		 	value "%s"
		}
		image {
			position (LEFT 7, TOP 7)
		 	size (fix 16, fix 16)	
		 	path ( "icons/userType.png" )
		}
        polyline {
        	appearance {
				foreground (102,102,102)
        	}
			points [ (7,29) (133,29) ]
		}
	}
}

nodeStyle referencedTypeStyle(1) {
	roundedRectangle {
		appearance default
		size (140,30)
		corner(10,10)
		text {
			appearance extends labelFont {
				foreground (102,102,102)
			}
		 	position ( LEFT 27, TOP 7 )	
		 	value "%s"
		}
		image {
			position (LEFT 7, TOP 7)
		 	size (fix 16, fix 16)	
		 	path ( "icons/concreteType.png" )
		}
        polyline {
        	appearance {
				foreground (102,102,102)
				background (255,251,232)
        	}
			points [ (7,29) (133,29) ]
		}
	}
}

nodeStyle referencedAttributeStyle(1) {
	roundedRectangle {
		appearance default
		size (140,30)
		corner(10,10)
		text {
			appearance extends labelFont {
				foreground (102,102,102)
			}
		 	position ( LEFT 27, TOP 7 )	
		 	value "%s"
		}
		image {
			position (LEFT 7, TOP 7)
		 	size (fix 16, fix 16)	
		 	path ( "icons/concreteType.png" )
		}
        polyline {
        	appearance {
				foreground (102,102,102)
        	}
			points [ (7,29) (133,29) ]
		}
	}
}

nodeStyle attribute(2) {
	appearanceProvider ( "info.scce.dime.graphql.util.ForbiddenAttributeAppearance" ) 
	rectangle {
		appearance {
			transparency 1.0
		}
		size (130,19)
		image {
			position (LEFT 2, MIDDLE) 
			size (fix 16, fix 15)
		 	path ( "icons/gui/ForbiddenFalse.png" )
		}
		text {
			position (LEFT 20, MIDDLE)
			value "%s: %s"
		}
	}
}


// This is here to pre-load conditional icons for the forbidden/non-forbidden attributes.
// DIME is not able to load those images after initialization, so this is necessary.
nodeStyle imageLoader(1) {
	rectangle {
		size (140,30)
		image {
			position (LEFT 7, TOP 7)
		 	size (fix 15, fix 14)	
		 	path ( "icons/gui/ForbiddenTrue.png" )
		}
	}
}


