appearance default {
	lineWidth 2
	background (229,229,229)
}

appearance labelFont {
	font ("Sans",10)
}

appearance labelFontItalics {
	font ("Sans",ITALIC,10)
}

appearance controlFlowAppearance {
	lineWidth 2
}

appearance eventFlowAppearance {
	lineWidth 2
	foreground (37,137,11)
}

appearance dataFlowAppearance extends default {
	lineStyle DOT
	foreground (144,144,144)
}

nodeStyle redBox {
	rectangle {
		appearance {
			background (255,0,0)	
		}
		size (50,50)
	}
}

nodeStyle documentation (1) {
	rectangle {
		appearance {
			background (247,242,192)
			foreground (251,234,104)
			lineWidth 2
		}
		size (100, 40)
		text {
			appearance extends labelFont {
				foreground (0,21,194)
			}
			position ( CENTER, MIDDLE)
			value "%s"
		}
	}
}

nodeStyle labeledCircle (1){
	ellipse {
		appearance default
		size(40,40)
		text {
			position ( CENTER, MIDDLE )
			value "%s"
		}
	}
}

nodeStyle circle {
	ellipse {
		size (14,14)
	}	
}

nodeStyle inputStatic(2) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/inputStatic.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s = %s"			
		}
	}
}

nodeStyle inputProcessStatic(2) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/inputProcessStatic.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s = %s"			
		}
	}
}

nodeStyle inputPort (4) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/inputPort.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s :%s%s%s"			
		}
	}
}

nodeStyle outputPort(4) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/outputPort.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s :%s%s%s"			
		}
	}
}

nodeStyle genericPort (4) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/typeInOut.png" )
		}
		text {
			appearance {
				filled false
			}
			position ( LEFT 16, MIDDLE)
			value "%s :%s<%s>%s"
		}
	}
}

nodeStyle box {
	rectangle {
		size (100,100)
	}
}

nodeStyle dataContext {
	appearanceProvider ( "info.scce.dime.process.aps.DataFlowAppearance" ) 
	rectangle {
		appearance {
			background  (245,245,245)
			lineWidth 1
		}
		size (180,600)
		text { 
			appearance labelFont
			position ( CENTER, TOP 4)
			value "DATA"
		}
	}
}

nodeStyle extensionContext {
	roundedRectangle {
		appearance extends darkPanel {
			background (255,255,255)
			foreground (0,0,0)
		}
		size (300,300)
		corner (4,4)
		text {
			appearance extends darkPanel {
				font ("Helvetica",10)
				foreground (0,0,0)
				background (255,255,255)
			}
			position (CENTER,TOP 2)
			value "EXTENSION"
		}
		roundedRectangle {
			appearance extends darkPanel {
				background (255,255,255)
				foreground (0,0,0)
			}
			position (CENTER,BOTTOM -5) 
			size (290,270)
			corner (4,4)
			
		}
	}
}

nodeStyle nativeVariable(2) {
	appearanceProvider ( "info.scce.dime.process.aps.DataFlowAppearance" ) 
	roundedRectangle {
		appearance {
			background (139,123,172)
		}
		size (140,25)
		corner (25,25)
		text {
			appearance labelFont
			position (CENTER, TOP 4)
			value "%1$s :%2$s"
		}
		/*
        polyline {
			points [ (7,24) (133,24) ]
		}
		*/
	}
}

nodeStyle variable(2) {
	appearanceProvider ( "info.scce.dime.process.aps.DataFlowAppearance" ) 
	roundedRectangle {
		appearance {
			background (182,211,207)
		}
		size (140,25)
		corner (25,25)
		text {
			appearance labelFont
			position (CENTER, TOP 4)
			value "%1$s :%2$s"
		}
		/*
        polyline {
			points [ (7,24) (133,24) ]
		}
		*/
	}
}

nodeStyle attribute(2) {
	appearanceProvider ( "info.scce.dime.process.aps.DataFlowAppearance" ) 
	roundedRectangle {
		appearance {
			background (198,226,222)
		}
		size (100,25)
		corner (25,25)
		text {
			appearance labelFont
			position (CENTER, TOP 4)
			value "%1$s :%2$s"
		}
	}
}

nodeStyle guiSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/guiSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle forkSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/forkSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle saveToDBSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/dataSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle joinSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/joinSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle searchSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/searchSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle processBlueprintSIB(1) {
	roundedRectangle {
		appearance {
        		foreground (42,40,180)
		}
		size (120,65)
		corner (10,10)
		text {
//			FIXME font type is not inherited here :(
//			appearance extends labelFont {
			appearance {
				font ("Sans",10)
        		foreground (42,40,180)
			}
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/processBlueprintSIB.png" )
		}
        polyline {
        	appearance {
        		foreground (42,40,180)
        	}
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle guiBlueprintSIB(1) {
	roundedRectangle {
		appearance {
        		foreground (42,40,180)
		}
		size (120,65)
		corner (10,10)
		text {
//			FIXME font type is not inherited here :(
//			appearance extends labelFont {
			appearance {
				font ("Sans",10)
        		foreground (42,40,180)
			}
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/guiBlueprintSIB.png" )
		}
        polyline {
        	appearance {
        		foreground (42,40,180)
        	}
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle processSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/processSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle genericSIB(1) {
	appearanceProvider ("info.scce.dime.process.aps.GenericSIBAppearance")
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image icon {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/processSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle entryProcessSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/entryProcessSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle linkSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/linkSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle processPlaceholderSIB(1) {
	roundedRectangle {
		appearance{
			background (255,255,255)
			foreground (0,0,0)
			lineWidth 1
			filled true
		}
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)
		 	// TODO: add a separate icon here	
		 	path ( "icons/dispatchedGuiSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle atomicSIB(1) {
	appearanceProvider ("info.scce.dime.process.aps.AtomicSIBAppearance")
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image icon {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/atomicSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}


nodeStyle nativeFrontendSIBLibrary(2) {
	roundedRectangle {
		appearance {
        		foreground (22,21,92)
        	}
		size (100,65)
		corner (5,5)
		text {
			appearance extends labelFont {
				foreground (22,21,92)
			}
			position ( CENTER, TOP 4 )	
		 	value "%s: %s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/nativeFrontendSIB.png" )
		}
        polyline {
        	appearance {
        		foreground (22,21,92)
        	}
			points [ (7,64) (93,64) ]
		}
	}
}

nodeStyle nativeFrontendSIB(2) {
	roundedRectangle {
		appearance {
        		foreground (22,21,92)
        	}
		size (100,65)
		corner (10,10)
		text {
			appearance extends labelFont {
				foreground (22,21,92)
			}
			position ( CENTER, TOP 4 )	
		 	value "%s %s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/nativeFrontendSIB.png" )
		}
        polyline {
        	appearance {
        		foreground (22,21,92)
        	}
			points [ (7,64) (93,64) ]
		}
	}
}

nodeStyle nativeFrontendBranch(1) {
	roundedRectangle {
		appearance {
			foreground (22,21,92)	
		}
		size (100,25)
		corner (10,10)
		text {
			appearance extends labelFont {
				foreground (22,21,92)
			}
			position ( CENTER, TOP 4)
			value "%s"
		}
        polyline {
			appearance {
				foreground (22,21,92)	
			}
			points [ (7,24) (93,24) ]
		}
	}
}

nodeStyle enumSwitchSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/enumSwitchSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle getEnumLiteralSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/getEnumLiteralSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle containsSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/searchSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle iterateSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/iterateSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle dataSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/dataSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle removeFromListSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/removeFromListSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle deleteSIB(1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/deleteSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle startSIB {
	rectangle {
		appearance {
			background (245,245,245)
		}
		size (100,65)
//		corner(10,10)
		text {
			appearance labelFontItalics
		 	position ( CENTER, TOP 4 )	
		 	value "start"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/startSIB.png" )
		}
        polyline {
			points [ (7,64) (93,64) ]
		}
	}
}
nodeStyle endSIB(1) {
	rectangle {
		appearance {
			background (245,245,245)
		}
		size (100,65)
//		corner(10,10)
		text {
			appearance labelFont
		 	position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/endSIB.png" )
		}
        polyline {
			points [ (7,64) (93,64) ]
		}
	}
}

nodeStyle branchBlueprint(1) {
	appearanceProvider ( "info.scce.dime.process.aps.BranchBlueprintHLineAppearance" )
	rectangle {
		appearance {
			foreground (245,245,245)
			background (245,245,245)
		}
		size (120,25)
		text {
//			FIXME font type is not inherited here :(
//			appearance extends labelFont {
			appearance {
				font ("Sans",10)
				foreground (42,40,180)	
			}
			position ( CENTER, TOP 4)
			value "%s"
		}
        polyline hline {
			appearance {
				foreground (42,40,180)	
			}
			points [ (7,24) (113,24) ]
		}
	}
}

nodeStyle branch(1) {
	appearanceProvider ( "info.scce.dime.process.aps.BranchHLineAppearance" )
	rectangle {
		appearance {
			foreground (245,245,245)
			background (245,245,245)
		}
		size (120,25)
		text {
			appearance labelFont 
			position ( CENTER, TOP 4)
			value "%s"
		}
        polyline hline {
			points [ (7,24) (113,24) ]
		}
	}
}

nodeStyle event(1) {
	roundedRectangle {
		appearance {
			background (225,240,221)
		}
		size (120,25)
		corner (0,0)
		text {
			appearance labelFont
			position ( CENTER, TOP 4)
			value "%s"
		}
        polyline {
			points [ (7,24) (93,24) ]
		}
	}
}


edgeStyle branchConnector {
	appearance controlFlowAppearance
}

edgeStyle eventConnector {
	appearance eventFlowAppearance
}  

edgeStyle branchConnectorBlueprint {
	appearance extends controlFlowAppearance {
		foreground (42,40,180)
	}
} 

edgeStyle complexAttributeConnector {
	appearance controlFlowAppearance
	decorator {
		ellipse {
			appearance extends controlFlowAppearance {
				background (0,0,0)
			}
			size (12,12)	
		}
		location (0)	
		
	}
} 

edgeStyle controlFlow {	
	appearance controlFlowAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance default 
	}
}

edgeStyle eventFlow {	
	appearance eventFlowAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance eventFlowAppearance 
	}
}

edgeStyle simpleArrow {	
	appearance default
	
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance default 
	}
}


edgeStyle dataFlow {	
	appearanceProvider ( "info.scce.dime.process.aps.DataFlowAppearance" ) 
	appearance dataFlowAppearance
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance default 
	}
}

/*
 * 
 * GENERICS STUFF BELOW
 * 
 */

edgeStyle typeFlow {	
	appearance extends dataFlowAppearance {
		foreground (255,118,118)	
		lineStyle DOT // TODO: should be inherited from dataFlowAppearance -> file bug
	}
	decorator {
		location (1.0) // at the end of the edge
		ARROW
		appearance extends default {
			foreground (255,118,118)	
		}
	}
}

nodeStyle typeContext {
	rectangle {
		appearance {
			background (255,205,205)
			lineWidth 1
		}
		size (140,160)
		text { 
			appearance labelFont
			position ( CENTER, TOP 4)
			value "TYPES"
		}
	}
}

edgeStyle typeInstance {	
	appearance {
		foreground (144,144,144)
		lineWidth 1
	}
	decorator {
		location (1.0) // at the end of the edge
		TRIANGLE
		appearance {
			foreground (144,144,144)
			lineWidth 1
		}
	}
}

nodeStyle genericVariable(4) {
	roundedRectangle {
		appearance {
			background (255,182,110)
		}
		size (100,34)
		corner (34,34)
		text {
			position (CENTER, TOP 3)
			value "%1$s%2$s%3$s"
		}
		multiText {
			appearance labelFont
			position (CENTER, TOP 8)
			value "%4$s"
		}
	}
}

nodeStyle typeVariable(1) {
	
	// polygon currently not working. see #15428
	//polygon {
	ellipse {
		appearance {
			background (255,118,118)	
		}
		size (100,34)
		// polygon currently not working. see #15428
		//points [ (20,0) (80,0) (100,17) (80,34) (20,34) (0,17) ]
		multiText {
			appearance labelFont
			position (CENTER, MIDDLE)
			value "%1$s"
		}
	
	}
}


nodeStyle genericInputPort (5) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/genericInputPort.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s :%s%s<%s>%s"			
		}
		
	}
}

nodeStyle genericOutputPort(5) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/genericOutputPort.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s :%s%s<%s>%s"			
		}
		
	}
}

nodeStyle typeInput(3) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/typeInOut.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s<%s<%s>>"			
		}
		
	}
}

nodeStyle typeOutput(2) {
	rectangle {
		appearance {
			transparency 1.0
		}
		size (90,18)
		image {
			position (LEFT, MIDDLE)
			size (fix 12,fix 12)
			path ( "icons/typeInOut.png" )
		}
		text {
			position ( LEFT 16, MIDDLE)
			value "%s<%s>"			
		}
		
	}
}

nodeStyle specialProcess(1) {
	rectangle {
		appearance {
			background  (252,252,252)
			lineWidth 0
		}
		size (600,600)
		text { 
			appearance labelFont
			position ( LEFT 4, TOP 4)
			value "%s"
		}
	}
}

nodeStyle guardContainer {
	rectangle {
		appearance {
			background  (147,147,232)
			lineWidth 1
		}
		size (150,300)
	}
}

nodeStyle guardProcessSIB (1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/guardProcessSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}

nodeStyle eventTrigger (1) {
	roundedRectangle {
		size (120,65)
		corner (10,10)
		text {
			appearance labelFont
			position ( CENTER, TOP 4 )	
		 	value "%s"
		}
		image {
			position (CENTER, TOP 24)
		 	size (fix 32, fix 32)	
		 	path ( "icons/triggerEventSIB.png" )
		}
        polyline {
			points [ (7,64) (113,64) ]
		}
	}
}