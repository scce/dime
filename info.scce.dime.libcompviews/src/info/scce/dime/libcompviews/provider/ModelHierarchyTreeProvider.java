/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.libcompviews.provider;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.dad.dad.FindLoginUserComponent;
import info.scce.dime.dad.dad.LoginComponent;
import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.libcompviews.nodes.ContainerTreeNode;
import info.scce.dime.libcompviews.nodes.GraphModelTreeNode;
import info.scce.dime.libcompviews.nodes.PrimeTreeNode;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.utils.GraphModelUtils;
import info.scce.dime.process.process.EntryPointProcessSIB;
import info.scce.dime.process.process.GUISIB;
import info.scce.dime.process.process.GuardProcessSIB;
import info.scce.dime.process.process.GuardedProcessSIB;
import info.scce.dime.process.process.ProcessSIB;
import info.scce.dime.process.process.SearchSIB;

public class ModelHierarchyTreeProvider extends LibCompTreeProvider {

	private String[] fileExtensions = { "dad", "process", "gui" };

	private ContainerTreeNode root;
	
	private ArrayList<String> visitedModelIds = new ArrayList<String>();

	public ModelHierarchyTreeProvider() {
		super();
		this.utils = new GraphModelUtils(fileExtensions);
	}

	@Override
	public TreeNode getTree() {
		return root;
	}

	public String[] getFileExtensions() {
		return fileExtensions;
	}

	@Override
	public void loadData(IProject project) {
		final long timeStart = System.currentTimeMillis();
		modelToResource = new HashMap<>();
		resourceToModel = new HashMap<>();

		ArrayList<IResource> contents = new ArrayList<IResource>();
		try {
			for (IResource res : project.members()) {
				contents.addAll(utils.loadRecursive(res));
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}

		modelReferencedIn = new HashMap<String, ArrayList<EObject>>();
		for (IResource iResource : contents) {
			if (utils.isModelResource(iResource)) {
				EObject model = utils.loadModel(iResource);
				modelToResource.put(model, iResource);
				resourceToModel.put(iResource, model);
				if (isShowReferencedInInfo()) {
					utils.collectReferences(model, modelReferencedIn);
				}
			}
		}
		final long timeLoad = System.currentTimeMillis();

		root = new ContainerTreeNode(null, "root");
		visitedModelIds.clear();
		for (EObject model : modelToResource.keySet()) {
			if (model instanceof DAD) {
				buildHierarchyForDadTree((DAD) model, root);
			}
		}

		final long timeBuild = System.currentTimeMillis();
		System.out.println("ModelHierarchyView - load Models: " + (timeLoad - timeStart) + " ms / create Tree: "
				+ (timeBuild - timeLoad) + " ms");
	}

	private TreeNode getNode(Object obj) {
		TreeNode node = null;

		if (obj instanceof DAD) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
		}
		
		if(obj instanceof LoginComponent) {
			LoginComponent me = (LoginComponent) obj;
			node = new PrimeTreeNode(me, me.getModel(), modelToResource.get(me.getRootElement()));
		}
		
		if(obj instanceof FindLoginUserComponent) {
			FindLoginUserComponent me = (FindLoginUserComponent) obj;
			node = new PrimeTreeNode(me, me.getModel(), modelToResource.get(me.getRootElement()));
		}

		if (obj instanceof ProcessComponent) {
			ProcessComponent me = (ProcessComponent) obj;
			node = new PrimeTreeNode(me, me.getModel(), modelToResource.get(me.getRootElement()));
		}
		
		/*
		 * Process Hierarchy
		 */

		if (obj instanceof ProcessSIB) {
			ProcessSIB me = (ProcessSIB) obj;
			node = new PrimeTreeNode(me, me.getProMod(), modelToResource.get(me.getRootElement()));
		}
		
		if (obj instanceof EntryPointProcessSIB) {
			EntryPointProcessSIB me = (EntryPointProcessSIB) obj;
			node = new PrimeTreeNode(me, me.getProMod(), modelToResource.get(me.getRootElement()));
		}

		if (obj instanceof GUISIB) {
			GUISIB me = (GUISIB) obj;
			node = new PrimeTreeNode(me, me.getGui(), modelToResource.get(me.getRootElement()));
		}
		
		if (obj instanceof GuardProcessSIB) {
			GuardProcessSIB me = (GuardProcessSIB) obj;
			node = new PrimeTreeNode(me, me.getSecurityProcess(), modelToResource.get(me.getRootElement()));
		}
		
		if (obj instanceof GuardedProcessSIB) {
			GuardedProcessSIB me = (GuardedProcessSIB) obj;
			node = new PrimeTreeNode(me, me.getProMod(), modelToResource.get(me.getRootElement()));
		}
		
		if (obj instanceof SearchSIB) {
			SearchSIB me = (SearchSIB) obj;
			node = new PrimeTreeNode(me, me.getSearch(), modelToResource.get(me.getRootElement()));
		}
		
		/*
		 * GUI Hierarchy
		 */

		if (obj instanceof info.scce.dime.gui.gui.GUISIB) {
			info.scce.dime.gui.gui.GUISIB me = (info.scce.dime.gui.gui.GUISIB) obj;
			node = new PrimeTreeNode(me, me.getGui(), modelToResource.get(me.getRootElement()));
		}
		
		if (obj instanceof info.scce.dime.gui.gui.ProcessSIB) {
			info.scce.dime.gui.gui.ProcessSIB me = (info.scce.dime.gui.gui.ProcessSIB) obj;
			node = new PrimeTreeNode(me, (GraphModel) me.getProMod(), modelToResource.get(me.getRootElement()));
		}
		
		if (obj instanceof info.scce.dime.gui.gui.SecuritySIB) {
			info.scce.dime.gui.gui.SecuritySIB me = (info.scce.dime.gui.gui.SecuritySIB) obj;
			node = new PrimeTreeNode(me, (GraphModel) me.getProMod(), modelToResource.get(me.getRootElement()));
		}
		
		if (obj instanceof info.scce.dime.gui.gui.GuardSIB) {
			info.scce.dime.gui.gui.GuardSIB me = (info.scce.dime.gui.gui.GuardSIB) obj;
			node = new PrimeTreeNode(me, (GraphModel) me.getProcess(), modelToResource.get(me.getRootElement()));
		}

		return node;
	}

	private void addNodeToTree(TreeNode parent, TreeNode node) {
		if (node == null || parent == null)
			return;
		if (parent.find(node.getId()) == null) {
			parent.getChildren().add(node);
			node.setParent(parent);
		}
	}

	private void buildHierarchyForDadTree(Object obj, TreeNode parentNode) {
		if (obj instanceof GraphModel) {
			GraphModel gModel = (GraphModel) obj;
			
			if (visitedModelIds.contains(gModel.getId()))
				return;
			visitedModelIds.add(gModel.getId());
			
			TreeNode modelNode = getNode(gModel);
			addNodeToTree(parentNode, modelNode);
			if (modelNode != null)
				parentNode = modelNode;
			
			TreeIterator<EObject> it = gModel.getInternalElement_().eAllContents();
			while (it.hasNext()) {
				Object objIt = it.next();
				if (objIt instanceof ModelElement == false)
					continue;
				ModelElement me = (ModelElement) objIt;
				EStructuralFeature libCompFeature = me.getInternalElement_().eClass()
						.getEStructuralFeature("libraryComponentUID");

				if (libCompFeature != null) {
					buildHierarchyForDadTree(me, parentNode);
				}
			}
		}
		if (obj instanceof ModelElement) {
			TreeNode elementNode = getNode(obj);
			addNodeToTree(parentNode, elementNode);
			if (elementNode instanceof PrimeTreeNode)
			buildHierarchyForDadTree(((PrimeTreeNode) elementNode).getModel(), elementNode);
		}
	}

}
