/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.libcompviews.views;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.google.common.collect.Sets;

import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.ProcessEntryPointComponent;
import info.scce.dime.libcompviews.ResourceChangeListener.ListenerEvents;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.pages.LinkCompPage;

public class LinkCompView extends LibCompView<LinkCompPage> {
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "info.scce.dime.libcompviews.views.Link";


	public LinkCompView() {
		Map<String, Set<ListenerEvents>> obsDefinition = new HashMap<>();
	
		obsDefinition.put("dad",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED,ListenerEvents.CONTENT_CHANGE));

		this.obsDefinition = obsDefinition;
		this.genericParameterClass = LinkCompPage.class;
	}

	@Override
	protected void fillLocalPullDown(IMenuManager manager) {
	}

	@Override
	protected void fillContextMenu(IMenuManager manager) {
		manager.add(openModelAction);
		// manager.add(new Separator());
		// // Other plug-ins can contribute there actions here
		// manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	@Override
	protected void fillLocalToolBar(IToolBarManager manager) {
		// manager.add(createFileAction);
		manager.add(reloadAction);
		manager.add(expandAllAction);
		manager.add(collapseAllAction);
		manager.add(linkWithEditorAction);
		manager.add(new Separator());
	}

	@Override
	protected void makeActions() {
		super.makeActions();

		

		/*
		 * ------------------------------------------
		 */
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				if (obj instanceof TreeNode) {
					activePage.storeTreeState();
					Object data = ((TreeNode) obj).getData();
					if (data instanceof ProcessComponent) {
						activePage.openAndHighlight(((ProcessComponent) data).getModel());
					} else if (data instanceof ProcessEntryPointComponent) {
						activePage.openAndHighlight(((ProcessEntryPointComponent) data).getEntryPoint().getRootElement());
					} else {
						activePage.toggleExpand(obj);
						activePage.storeTreeState();
					}
				}
			}
		};

	}

	@Override
	protected void reloadViewState() {

		activePage.getTreeViewer().refresh();
	}

	@Override
	protected void editorChanged() {
		activePage.calculateModelTypesToHide(activeFile);
		super.editorChanged();
		activePage.getTreeViewer().refresh();
		activePage.restoreTreeState();
	}

}
