/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.libcompviews.views;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import info.scce.dime.dad.dad.DAD;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.libcompviews.ResourceChangeListener.ListenerEvents;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.pages.ControlCompPage;
import info.scce.dime.libcompviews.provider.ControlCompTreeProvider.ViewType;
import info.scce.dime.libcompviews.utils.LibCompUtils;
import info.scce.dime.process.process.Process;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.google.common.collect.Sets;

public class ControlCompView extends LibCompView<ControlCompPage> {
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "info.scce.dime.libcompviews.views.Control";

	private IAction createFileAction;
	private IAction showFolderStructViewAction;
	private IAction showCategoryViewAction;
	private IAction filterAllowedTypesAction;
	private IAction showReferencedInViewAction;

	public ControlCompView() {
		Map<String, Set<ListenerEvents>> obsDefinition = new HashMap<>();
		obsDefinition.put("process",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED, ListenerEvents.CONTENT_CHANGE));
		obsDefinition.put("gui",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED, ListenerEvents.CONTENT_CHANGE));
		obsDefinition.put("dad",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED, ListenerEvents.CONTENT_CHANGE));
		obsDefinition.put("sibs",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED, ListenerEvents.CONTENT_CHANGE));

		this.obsDefinition = obsDefinition;
		this.genericParameterClass = ControlCompPage.class;
	}

	@Override
	protected void fillLocalPullDown(IMenuManager manager) {
		IMenuManager filterSubmenu = new MenuManager("Filters");
		filterSubmenu.add(filterAllowedTypesAction);

		IMenuManager viewSubmenu = new MenuManager("Display Mode");
		viewSubmenu.add(showFolderStructViewAction);
		viewSubmenu.add(showCategoryViewAction);

		manager.add(filterSubmenu);
		manager.add(viewSubmenu);

		manager.add(new Separator());

		manager.add(showReferencedInViewAction);
	}

	@Override
	protected void fillContextMenu(IMenuManager manager) {
		manager.add(openModelAction);
		// manager.add(new Separator());
		// // Other plug-ins can contribute there actions here
		// manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	@Override
	protected void fillLocalToolBar(IToolBarManager manager) {
		// manager.add(createFileAction);
		manager.add(reloadAction);
		manager.add(expandAllAction);
		manager.add(collapseAllAction);
		manager.add(linkWithEditorAction);
		manager.add(new Separator());
	}

	@Override
	protected void makeActions() {
		super.makeActions();

		createFileAction = new Action() {
			public void run() {
				// LibCompUtils
				// .openWizard("info.scce.dime.process.wizard.process");
			}
		};
		createFileAction.setText("Create process model");
		createFileAction.setToolTipText("Create process model");
		createFileAction.setImageDescriptor(ImageDescriptor
				.createFromImage(iconCreateFile));

		/*
		 * ------------------------------------------
		 */
		showFolderStructViewAction = new Action() {
			public void run() {
				activePage.getDataProvider().setActiveView(
						ViewType.FOLDERSTRUCT);
				LibCompUtils.runBusy(new Runnable() {
							public void run() {
								activePage.reload();
								reloadViewState();
							}
						});
			}
		};
		showFolderStructViewAction.setText("by Folder Structure");
		showFolderStructViewAction.setToolTipText("show folder structure view");
		showFolderStructViewAction.setChecked(true);

		/*
		 * ------------------------------------------
		 */
		showCategoryViewAction = new Action() {
			public void run() {
				activePage.getDataProvider().setActiveView(ViewType.CATEGORY);
				LibCompUtils.runBusy(new Runnable() {
					public void run() {
						activePage.reload();
						reloadViewState();
					}
				});
			}
		};
		showCategoryViewAction.setText("by Category");
		showCategoryViewAction.setToolTipText("show category view");
		showCategoryViewAction.setChecked(false);

		/*
		 * ------------------------------------------
		 */
		showReferencedInViewAction = new Action() {
			public void run() {
				boolean temp = activePage.getDataProvider()
						.isShowReferencedInInfo();
				activePage.getDataProvider().setShowReferencedInInfo(!temp);
				LibCompUtils.runBusy(new Runnable() {
					public void run() {
						activePage.reload();
						reloadViewState();
					}
				});
			}
		};
		showReferencedInViewAction.setText("Show referenced info");
		showReferencedInViewAction.setToolTipText("Show referenced info");
		showReferencedInViewAction.setChecked(false);

		/*
		 * ------------------------------------------
		 */
		filterAllowedTypesAction = new Action() {
			public void run() {
				if (!activePage.isShowOnlyAllowedTypes()) {
					activePage.getTreeViewer().addFilter(
							activePage.getModelTypeFilter());
					activePage.setShowOnlyAllowedTypes(true);
				} else {
					activePage.getTreeViewer().removeFilter(
							activePage.getModelTypeFilter());
					activePage.setShowOnlyAllowedTypes(false);
				}
				reloadViewState();
			}
		};
		filterAllowedTypesAction.setText("filter allowed types");
		filterAllowedTypesAction.setToolTipText("filter allowed types");
		filterAllowedTypesAction.setChecked(false);

		/*
		 * ------------------------------------------
		 */
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				if (obj instanceof TreeNode) {
					activePage.storeTreeState();
					Object data = ((TreeNode) obj).getData();
					if (data instanceof Process) {
						activePage.openAndHighlight(data);
					} else if (data instanceof GUI) {
						activePage.openAndHighlight(data);
					} else if (data instanceof DAD) {
						activePage.openAndHighlight(data);
					} else {
						activePage.toggleExpand(obj);
						activePage.storeTreeState();
					}
				}
			}
		};

	}

	@Override
	protected void reloadViewState() {
		filterAllowedTypesAction
				.setChecked(activePage.isShowOnlyAllowedTypes());

		showCategoryViewAction.setChecked(false);
		showFolderStructViewAction.setChecked(false);

		switch (activePage.getDataProvider().getActiveView()) {
		case CATEGORY:
			showCategoryViewAction.setChecked(true);
			break;

		case FOLDERSTRUCT:
			showFolderStructViewAction.setChecked(true);
			break;

		default:
			break;
		}

		showReferencedInViewAction.setChecked(activePage.getDataProvider()
				.isShowReferencedInInfo());

		activePage.getTreeViewer().refresh();
	}

	@Override
	protected void editorChanged() {
		activePage.calculateModelTypesToHide(activeFile);
		super.editorChanged();
		activePage.getTreeViewer().refresh();
		activePage.restoreTreeState();
	}

}
