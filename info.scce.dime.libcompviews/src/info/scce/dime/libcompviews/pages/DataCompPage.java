/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.libcompviews.pages;

import java.io.IOException;
import java.util.HashMap;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorPart;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.data.data.AbstractType;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.Data;
import info.scce.dime.data.data.EnumLiteral;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.ReferencedBidirectionalAttribute;
import info.scce.dime.data.data.ReferencedComplexAttribute;
import info.scce.dime.data.data.ReferencedEnumType;
import info.scce.dime.data.data.ReferencedExtensionAttribute;
import info.scce.dime.data.data.ReferencedPrimitiveAttribute;
import info.scce.dime.data.data.ReferencedType;
import info.scce.dime.data.data.ReferencedUserAttribute;
import info.scce.dime.data.data.ReferencedUserType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserType;
import info.scce.dime.libcompviews.provider.DataCompTreeProvider;
import info.scce.dime.libcompviews.utils.LibCompUtils;
import info.scce.dime.siblibrary.SIBLibrary;

public class DataCompPage extends LibCompPage {

	private DataCompTreeProvider data = new DataCompTreeProvider();

	private HashMap<String, Image> icons = new HashMap<>();

	private boolean showInheritanceInfo = true;
	private boolean hideAbstractTypes = false;

	private DataAbstractTypeFilter filterAbstractType = new DataAbstractTypeFilter();
	private DataLabelProvider labelProvider = new DataLabelProvider();
	private DataNameSorter nameSorter = new DataNameSorter();

	/*
	 * Getter / Setter
	 */

	@Override
	public DataCompTreeProvider getDataProvider() {
		return data;
	}

	public boolean isShowInheritanceInfo() {
		return showInheritanceInfo;
	}

	public void setShowInheritanceInfo(boolean showInheritanceInfo) {
		this.showInheritanceInfo = showInheritanceInfo;
	}

	public boolean isHideAbstractTypes() {
		return hideAbstractTypes;
	}

	public void setHideAbstractTypes(boolean hideAbstractTypes) {
		this.hideAbstractTypes = hideAbstractTypes;
	}

	public DataAbstractTypeFilter getFilterAbstractType() {
		return filterAbstractType;
	}

	@Override
	public LabelProvider getDefaultLabelProvider() {
		return labelProvider;
	}

	@Override
	public ViewerSorter getDefaultSorter() {
		return nameSorter;
	}

	/*
	 * Methods
	 */
	@Override
	protected void loadIcons() {
		try {
			super.loadIcons();

			// icons.put("Folder", PlatformUI.getWorkbench().getSharedImages()
			// .getImage(ISharedImages.IMG_OBJ_FOLDER));

			icons.put(
					"DataModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle,
									new Path("icons/DataModel.png"), true)));

			icons.put(
					"AbstractType",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/a_type.png"),
									true)));

			icons.put(
					"ConcreteType",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/c_type.png"),
									true)));

			icons.put(
					"PrimitiveAttribute",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/p_attr.png"),
									true)));
			icons.put(
					"ExtensionAttribute",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/ProcessModel.png"),
									true)));

			icons.put(
					"ComplexAttribute",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/c_attr1.png"),
									true)));

			icons.put(
					"ComplexAttributeBi",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/c_attr2.png"),
									true)));

			icons.put(
					"EnumLiteral",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/e_attr.png"),
									true)));

			icons.put(
					"EnumType",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/e_type.png"),
									true)));

			icons.put(
					"ReferencedType",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/r_type.png"),
									true)));
			icons.put(
					"ReferencedUserType",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/u_type.png"),
									true)));

			icons.put(
					"UserType",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/u_type.png"),
									true)));

			icons.put(
					"UserAttribute",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/u_attr.png"),
									true)));

			icons.put(
					"AtomicSibLibrary",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/AtomicSibLib.png"), true)));

			icons.put(
					"AtomicSibLibraryContent",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/r_attr.png"),
									true)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void reload() {
		storeTreeState();
		data.reset();
		treeViewer.setInput(parentViewPart.getViewSite());
		restoreTreeState();
	}

	@Override
	public void openAndHighlight(Object obj) {
		obj = getTreeNodeData(obj);
		if (obj instanceof ModelElement) {
			ModelElement me = (ModelElement) obj;
			IEditorPart editorPart = openEditor(me.getRootElement());
//			Data wrapper = data.getUtils().getDataWrapperFromEditor(editorPart);
//			data.getUtils().highlightElement(wrapper, me.getId());
		}
		if (obj instanceof GraphModel) {
			openEditor((GraphModel) obj);
		}
	}

	/*
	 * Provider / Classes for TreeViewer
	 */

	private class DataLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			obj = getTreeNodeData(obj);
			if (obj instanceof Data) {
				return data.getModelToResource().get(obj).getName();
			}
			else if (obj instanceof ReferencedType) {
				String label = ((ReferencedType) obj).getReferencedType().getName();
				if (showInheritanceInfo
						&& data.getUtils().getSuperType((Type) ((ReferencedType) obj).getReferencedType()) != null)
					label += " <"
							+ data.getUtils().getSuperType((Type) ((ReferencedType) obj).getReferencedType()).getName() + ">";
				return label;
			}
			else if (obj instanceof ReferencedUserType) {
				String label = ((ReferencedUserType) obj).getReferencedType().getName();
				if (showInheritanceInfo
						&& data.getUtils().getSuperType((Type) ((ReferencedUserType) obj).getReferencedType()) != null)
					label += " <"
							+ data.getUtils().getSuperType((Type) ((ReferencedUserType) obj).getReferencedType()).getName() + ">";
				return label;
			}
			else if (obj instanceof Type) {
				String label = ((Type) obj).getName();
				if (showInheritanceInfo
						&& data.getUtils().getSuperType((Type) obj) != null)
					label += " <"
							+ data.getUtils().getSuperType((Type) obj).getName() + ">";
				return label;
			}
			else if (obj instanceof ReferencedPrimitiveAttribute) {
				ReferencedPrimitiveAttribute pa = (ReferencedPrimitiveAttribute) obj;
				String label = pa.getName() + " : ";
				String type = pa.getReferencedAttribute().getDataType().getLiteral();
				if (pa.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(pa) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(pa).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof ReferencedExtensionAttribute) {
				ReferencedExtensionAttribute pa = (ReferencedExtensionAttribute) obj;
				String label = pa.getName() + " : ";
				String type = pa.getReferencedAttribute().getDataType();
				if (pa.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(pa) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(pa).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof PrimitiveAttribute) {
				PrimitiveAttribute pa = (PrimitiveAttribute) obj;
				String label = pa.getName() + " : ";
				String type = pa.getDataType().getLiteral();
				if (pa.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(pa) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(pa).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof ExtensionAttribute) {
				ExtensionAttribute pa = (ExtensionAttribute) obj;
				String label = pa.getName() + " : ";
				String type = pa.getDataType();
				if (pa.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(pa) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(pa).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof ReferencedComplexAttribute) {
				ReferencedComplexAttribute ca = (ReferencedComplexAttribute) obj;
				String label = ca.getName() + " : ";
				String type = ca.getReferencedAttribute().getDataType().getName();
				
				if (ca.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(ca) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(ca).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof ReferencedBidirectionalAttribute) {
				ReferencedBidirectionalAttribute ca = (ReferencedBidirectionalAttribute) obj;
				String label = ca.getName() + " : ";
				String type = ca.getReferencedAttribute().getDataType().getName();
				
				if (ca.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(ca) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(ca).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof ReferencedUserAttribute) {
				ReferencedUserAttribute ca = (ReferencedUserAttribute) obj;
				String label = ca.getName() + " : ";
				String type = ca.getReferencedAttribute().getDataType().getName();
				
				if (ca.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(ca) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(ca).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof ComplexAttribute) {
				
				ComplexAttribute ca = (ComplexAttribute) obj;
				String label = ca.getName() + " : ";
				String type = ca.getDataType().getName();
				
				if (ca.isIsList())
					type = "[" + type + "]";
				label += type;
				if (showInheritanceInfo
						&& data.getUtils().getTypeForAttribute(ca) != null)
					label += " <"
							+ data.getUtils().getTypeForAttribute(ca).getName()
							+ ">";
				return label;
			}
			else if (obj instanceof ReferencedEnumType) {
				ReferencedEnumType el = (ReferencedEnumType) obj;
				String label = el.getReferencedType().getName();
				return label;
			}
			else if (obj instanceof EnumLiteral) {
				EnumLiteral el = (EnumLiteral) obj;
				String label = el.getName();
				return label;
			}

			else if (obj instanceof SIBLibrary)
				return data.getModelToResource().get(obj).getName();

			else if (obj instanceof info.scce.dime.siblibrary.Type)
				return ((info.scce.dime.siblibrary.Type) obj).getName();

			return "unknown";
		}

		public Image getImage(Object obj) {
			obj = getTreeNodeData(obj);
			if (obj instanceof Data)
				return icons.get("DataModel");
			if (obj instanceof AbstractType)
				return icons.get("AbstractType");
			if (obj instanceof ConcreteType)
				return icons.get("ConcreteType");
			if (obj instanceof PrimitiveAttribute)
				return icons.get("PrimitiveAttribute");
			if (obj instanceof ExtensionAttribute)
				return icons.get("ExtensionAttribute");
			if (obj instanceof ReferencedExtensionAttribute)
				return icons.get("ExtensionAttribute");
			if (obj instanceof ComplexAttribute) {
				switch (data.getUtils().getAssociationTypeFromAttribute(
						(ComplexAttribute) obj)) {
				default:
				case UNIDIRECTIONAL:
					return icons.get("ComplexAttribute");
				case BIDIRECTIONAL:
					return icons.get("ComplexAttributeBi");
				case USER:
					return icons.get("UserAttribute");
				}
			}
			if (obj instanceof EnumType)
				return icons.get("EnumType");
			if (obj instanceof EnumLiteral)
				return icons.get("EnumLiteral");
			if (obj instanceof ReferencedType)
				return icons.get("ReferencedType");
			if (obj instanceof ReferencedUserType)
				return icons.get("ReferencedUserType");
			if (obj instanceof UserType)
				return icons.get("UserType");
			if (obj instanceof SIBLibrary)
				return icons.get("AtomicSibLibrary");
			if (obj instanceof info.scce.dime.siblibrary.Type)
				return icons.get("AtomicSibLibraryContent");
			
			return null;
		}
	}

	private class DataAbstractTypeFilter extends ViewerFilter {

		@Override
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {

			parentElement = getTreeNodeData(parentElement);
			element = getTreeNodeData(element);

			if (element instanceof AbstractType)
				return false;
			return true;
		}
	}

	private class DataNameSorter extends ViewerSorter {

		@Override
		public int category(Object element) {
			if (element instanceof Data)
				return 3;
			if (element instanceof Type)
				return 2;
			if (element instanceof Attribute)
				return 1;

			if (element instanceof SIBLibrary)
				return 3;
			if (element instanceof info.scce.dime.siblibrary.Type)
				return 2;
			return 4;
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {

			e1 = getTreeNodeData(e1);
			e2 = getTreeNodeData(e2);

			int cat1 = category(e1);
			int cat2 = category(e2);
			if (cat1 != cat2)
				return cat1 - cat2;

			if (e1 instanceof Data && e2 instanceof Data) {
				IResource res1 = data.getModelToResource().get(e1);
				IResource res2 = data.getModelToResource().get(e2);
				return res1.getName().toLowerCase()
						.compareTo(res2.getName().toLowerCase());
			}

			if (e1 instanceof Type && e2 instanceof Type) {
				Type type1 = (Type) e1;
				Type type2 = (Type) e2;
				return type1.getName().toLowerCase()
						.compareTo(type2.getName().toLowerCase());
			}

			if (e1 instanceof Attribute && e2 instanceof Attribute) {
				Attribute attr1 = (Attribute) e1;
				Attribute attr2 = (Attribute) e2;
				return attr1.getName().toLowerCase()
						.compareTo(attr2.getName().toLowerCase());
			}

			if (e1 instanceof SIBLibrary && e2 instanceof SIBLibrary) {
				IResource res1 = data.getModelToResource().get(e1);
				IResource res2 = data.getModelToResource().get(e2);
				return res1.getName().toLowerCase()
						.compareTo(res2.getName().toLowerCase());
			}

			if (e1 instanceof info.scce.dime.siblibrary.Type
					&& e2 instanceof info.scce.dime.siblibrary.Type) {
				info.scce.dime.siblibrary.Type type1 = (info.scce.dime.siblibrary.Type) e1;
				info.scce.dime.siblibrary.Type type2 = (info.scce.dime.siblibrary.Type) e2;
				return type1.getName().toLowerCase()
						.compareTo(type2.getName().toLowerCase());
			}

			return super.compare(viewer, e1, e2);
		}

	}

}
