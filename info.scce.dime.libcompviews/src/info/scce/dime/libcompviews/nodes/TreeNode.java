/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.libcompviews.nodes;

import java.util.ArrayList;
import java.util.List;

public abstract class TreeNode {

	protected Object data = null;
	private TreeNode parent = null;
	private List<TreeNode> children = new ArrayList<TreeNode>();

	TreeNode(Object data) {
		super();
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public TreeNode getParent() {
		return parent;
	}

	public void setParent(TreeNode parent) {
		this.parent = parent;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	abstract public String getId();

	public String getPathIdentifier() {
		String ident = getId();
		if (parent != null)
			ident = parent.getPathIdentifier() + "/" + ident;
		return ident;
	}

	public TreeNode find(String id) {
		if (id.equals(getId()))
			return this;
		for (TreeNode child : children) {
			TreeNode result = child.find(id);
			if (result != null)
				return result;
		}
		return null;
	}

	@Override
	public String toString() {
		String output = "TreeNode of: " + data + "\n";
		for (TreeNode treeNode : children) {
			output += " - " + treeNode.toString();
		}
		return output;
	}
}
