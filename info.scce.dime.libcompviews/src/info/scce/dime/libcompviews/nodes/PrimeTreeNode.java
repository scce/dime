/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.libcompviews.nodes;

import org.eclipse.core.resources.IResource;

import graphmodel.GraphModel;
import graphmodel.ModelElement;

public class PrimeTreeNode extends GraphModelTreeNode {
	
	private ModelElement element;
	private GraphModel model;

	public PrimeTreeNode(ModelElement element, GraphModel model, IResource resource) {
		super(element, resource);
		this.element = element;
		this.model = model;
	}
	
	public ModelElement getElement() {
		return element;
	}

	public GraphModel getModel() {
		return model;
	}

	@Override
	public String getId() {
		return element.getId() + "-" + model.getId();
	}

}
