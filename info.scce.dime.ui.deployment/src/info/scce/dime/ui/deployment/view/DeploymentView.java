/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.deployment.view;

import info.scce.dime.property.DimeProperties;
import info.scce.dime.ui.deployment.DeploymentStateEnum;
import info.scce.dime.ui.deployment.EnvironmentFactory;
import info.scce.dime.ui.deployment.ProjectFactory;
import info.scce.dime.ui.deployment.handler.DeploymentHandler;
import info.scce.dime.ui.deployment.handler.ApplicationDeployment;
import info.scce.dime.ui.deployment.handler.RuntimeEnvironmentDeployment;
import info.scce.dime.ui.deployment.os.DefaultOperatingSystemName;
import info.scce.dime.ui.deployment.os.DefaultOperatingSystemDetector;
import info.scce.dime.ui.deployment.handler.DeploymentObserverInterface;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import de.jabc.cinco.meta.runtime.xapi.FileExtension;
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;
import java.io.File;
import java.util.Arrays;

import info.scce.gantry.Environment;
import info.scce.gantry.Environment.EnvironmentException;

public class DeploymentView extends ViewPart implements DeploymentObserverInterface {

	private final Workspace workspace;
	private DeploymentHandler deploymentHandler;
	private final WorkspaceListener workspaceListener;

	public static final String ID = "info.scce.dime.ui.deployment.view.DeploymentView"; //$NON-NLS-1$
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());

	private final String runtimeEnvironmentStart = "Start";
	private final String runtimeEnvironmentStop = "Stop";

	private Button buttonRuntimeEnvironment;
	private Button buttonRedeployApplication;
	private Button buttonPurgeApplication;

	private Label labelRuntimeEnvironmentState;
	private Label labelApplicationState;

	private Image processing = ResourceManager.getPluginImage("info.scce.dime.ui.deployment", "resources/icons/processing.png");
	private Image running = ResourceManager.getPluginImage("info.scce.dime.ui.deployment", "resources/icons/running.png");
	private Image stopped = ResourceManager.getPluginImage("info.scce.dime.ui.deployment", "resources/icons/stopped.png");
	private Image docker = ResourceManager.getPluginImage("info.scce.dime.ui.deployment", "resources/icons/docker.png");
	private Image warning = ResourceManager.getPluginImage("info.scce.dime.ui.deployment", "resources/icons/warning32.png");

	private Label appNameLabel;
	private Label workingDirLabel;
	private Link linkApplication;
	private ScrolledComposite scrolledComposite;
	private RuntimeEnvironmentDeployment runtimeEnvironmentDeployment;
	private ApplicationDeployment applicationDeployment;
	private Environment environment;

	public DeploymentView() {
		workspace = new Workspace(new FileExtension(), new WorkspaceExtension());
		workspaceListener = new WorkspaceListener() {
			@Override
			void onProjectChanged(IProject project) {
				disposeEnvironment();
				try {
					initEnvironment();
					startListener();
				} catch(Exception e) {
					throw new RuntimeException("Failed to initialize environment", e);
				} finally {
					updateUI();
				}
				// TODO handle all DIME projects being closed
			}
		};
		workspace.registerListener(workspaceListener);
	}
	
	private void initEnvironment() {
		ProjectFactory projectFactory = new ProjectFactory(
				new DefaultOperatingSystemDetector(
						new DefaultOperatingSystemName()
				),
				workspace,
				"target"
		);
		EnvironmentFactory environmentFactory = new EnvironmentFactory(projectFactory);
		try {
			environment = environmentFactory.environment();
		} catch (Exception e) {
			throw new RuntimeException("Failed to create DeplyomentView", e);
		}
		runtimeEnvironmentDeployment = new RuntimeEnvironmentDeployment(
				this,
				Arrays.asList(environment.container("postgres"), environment.container("mailcatcher"))
		);
		applicationDeployment = new ApplicationDeployment(
				this,
				Arrays.asList(environment.container("wildfly"))
		);
		deploymentHandler = new DeploymentHandler(
				this,
				runtimeEnvironmentDeployment,
				applicationDeployment
		);
		workspace.loadDimeProperties();
	}

	/**
	 * Create contents of the view part.
	 *
	 * @param Composite parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		try {
			initEnvironment();
			environment.checkPreconditions();
		} catch (EnvironmentException e) {
			throw new RuntimeException(
					"The Deployment View is not supported on a runtime environment without Docker",
					e
			);
		}
		startListener();
		drawComponents(parent);
		createActions();
		setStateRuntimeEnvironmentStopped();
	}

	@Override
	public void setFocus() {
	}

	@Override
	public void updateRuntimeEnvironment(DeploymentStateEnum state) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(() -> {
			switch (state) {
			case RUNNING:
				setStateRuntimeEnvrionmentRunning();
				break;
			case PROCESSING:
				setStateRuntimeEnvironmentProcessing();
				break;
			case STOPPED:
				setStateRuntimeEnvironmentStopped();
				break;
			default:
				break;
			}
		});
	}

	@Override
	public void updateApplication(DeploymentStateEnum state) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(() -> {
			switch (state) {
			case RUNNING:
				setStateApplicationRunning();
				break;
			case PROCESSING:
				setStateApplicationProcessing();
				break;
			case STOPPED:
				setStateApplicationStopped();
				break;
			default:
				break;
			}
		});
	}
	
	public void disposeEnvironment() {
		runtimeEnvironmentDeployment.deleteObserver(this);
		runtimeEnvironmentDeployment.quit();
		applicationDeployment.deleteObserver(this);
		applicationDeployment.quit();
	}

	@Override
	public void dispose() {
		disposeEnvironment();
		workspace.unregisterListener(workspaceListener);
		toolkit.dispose();
		super.dispose();
	}

	public void handlePurgeApplicationSelection() {
		deploymentHandler.purgeAndDeploy();
	}

	public void disableButtons() {
		setEnable(false, buttonPurgeApplication, buttonRedeployApplication, buttonRuntimeEnvironment);
	}

	public void enableButtons() {
		setEnable(true, buttonPurgeApplication, buttonRedeployApplication, buttonRuntimeEnvironment);
	}

	private void drawComponents(Composite parent) {
		scrolledComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		toolkit.adapt(scrolledComposite);
		toolkit.paintBordersFor(scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite container = toolkit.createComposite(scrolledComposite, SWT.BORDER);
		toolkit.paintBordersFor(container);
		container.setLayout(new GridLayout(4, false));

		Label logoLabel = new Label(container, SWT.SHADOW_NONE | SWT.CENTER);
		logoLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 2));
		logoLabel.setImage(docker);
		toolkit.adapt(logoLabel, true, true);

		// Row: Runtime Environment
		
		Label labelRuntimeEnvironment = new Label(container, SWT.NONE);
		labelRuntimeEnvironment.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		labelRuntimeEnvironment.setAlignment(SWT.RIGHT);
		labelRuntimeEnvironment.setFont(SWTResourceManager.getFont("Noto Sans", 12, SWT.NORMAL));
		labelRuntimeEnvironment.setBounds(0, 0, 100, 17);
		toolkit.adapt(labelRuntimeEnvironment, true, true);
		labelRuntimeEnvironment.setText("Runtime Environment:");

		labelRuntimeEnvironmentState = new Label(container, SWT.NONE);
		GridData labelRuntimeEnvironmentStateGrid = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		labelRuntimeEnvironmentStateGrid.widthHint = 80;
		labelRuntimeEnvironmentState.setLayoutData(labelRuntimeEnvironmentStateGrid);
		toolkit.adapt(labelRuntimeEnvironmentState, true, true);

		buttonRuntimeEnvironment = new Button(container, SWT.NONE);
		buttonRuntimeEnvironment.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		buttonRuntimeEnvironment.setBounds(0, 0, 63, 17);
		buttonRuntimeEnvironment.setText(runtimeEnvironmentStart);
		buttonRuntimeEnvironment.setFont(SWTResourceManager.getFont("Noto Sans", 12, SWT.NORMAL));
		toolkit.adapt(buttonRuntimeEnvironment, true, true);

		// Row: Deployment state
		
		Label labelApplication = new Label(container, SWT.NONE);
		labelApplication.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		labelApplication.setAlignment(SWT.RIGHT);
		labelApplication.setBounds(0, 0, 100, 17);
		labelApplication.setFont(SWTResourceManager.getFont("Noto Sans", 12, SWT.NORMAL));
		labelApplication.setText("Deployment State:");
		toolkit.adapt(labelApplication, true, true);

		labelApplicationState = new Label(container, SWT.NONE);
		GridData labelApplicationStateGrid = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		labelApplicationStateGrid.widthHint = 80;
		labelApplicationState.setLayoutData(labelApplicationStateGrid);
		toolkit.adapt(labelApplicationState, true, true);

		buttonRedeployApplication = new Button(container, SWT.NONE);
		buttonRedeployApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		buttonRedeployApplication.setBounds(0, 0, 63, 31);
		buttonRedeployApplication.setText("Redeploy");
		buttonRedeployApplication.setFont(SWTResourceManager.getFont("Noto Sans", 12, SWT.NORMAL));
		toolkit.adapt(buttonRedeployApplication, true, true);

		// Row: Purge and Deploy
		
		Label labelDataModel = new Label(container, SWT.NONE);
		labelDataModel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		labelDataModel.setImage(warning);
		labelDataModel.setFont(SWTResourceManager.getFont("Noto Sans", 10, SWT.BOLD));
		labelDataModel.setBounds(0, 0, 0, 17);
		toolkit.adapt(labelDataModel, true, true);

		Label labelDataModelState = new Label(container, SWT.WRAP);
		GridData labelDataModelStateGrid = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		labelDataModelStateGrid.widthHint = 80;
		labelDataModelState.setLayoutData(labelDataModelStateGrid);
		labelDataModelState.setBounds(0, 0, 100, 34);
		labelDataModelState.setText(
				"If you have made major changes (e.g deletion of "
				+ "data types) use the \"Purge and Deploy\" action!");
		labelDataModelState.setFont(SWTResourceManager.getFont("Noto Sans", 10, SWT.NORMAL));
		toolkit.adapt(labelDataModelState, true, true);

		buttonPurgeApplication = new Button(container, SWT.NONE);
		buttonPurgeApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		buttonPurgeApplication.setBounds(0, 0, 111, 31);
		buttonPurgeApplication.setText("Purge and Deploy");
		buttonPurgeApplication.setFont(SWTResourceManager.getFont("Noto Sans", 12, SWT.NORMAL));
		toolkit.adapt(buttonPurgeApplication, true, true);
		
		// Line separator
		
		Label separator = new Label(container, SWT.HORIZONTAL | SWT.SEPARATOR | SWT.SHADOW_OUT);
	    separator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
	    separator.setBounds(0, 0, 0, 3);
	    toolkit.adapt(separator, true, true);
	    
		// Row: app name label
		
		appNameLabel = new Label(container, SWT.NONE);
		appNameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		appNameLabel.setFont(SWTResourceManager.getFont("Noto Sans", 10, SWT.NONE));
		appNameLabel.setText("App: " + appName());
		appNameLabel.setBounds(0, 0, 0, 17);
		toolkit.adapt(appNameLabel, true, true);

		// Row: working directory label
		
		workingDirLabel = new Label(container, SWT.NONE);
		workingDirLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		workingDirLabel.setFont(SWTResourceManager.getFont("Noto Sans", 10, SWT.NONE));
		workingDirLabel.setText("Working Directory: " + targetFolder());
		workingDirLabel.setBounds(0, 0, 0, 17);
		toolkit.adapt(workingDirLabel, true, true);

		// Row: URL label

		linkApplication = new Link(container, SWT.NONE);
		linkApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		linkApplication.setFont(SWTResourceManager.getFont("Noto Sans", 10, SWT.NONE));
		linkApplication.setText("URL: <A>" + DimeProperties.getAppURL() + "</A>");
		linkApplication.setBounds(0, 0, 0, 17);
		toolkit.adapt(linkApplication, true, true);
		
		scrolledComposite.setContent(container);
		scrolledComposite.setMinSize(container.computeSize(SWT.DEFAULT, 180));
	}
	
	private String appName() {
		String appName = null;
		try {
			appName = workspace.appName();
		} catch(Exception e) {
			appName = "<unknown>";
			if (e.getMessage() != null && !e.getMessage().trim().isEmpty()) {
				appName += " (" + e.getMessage() + ")";
			}
		}
		if (appName == null || appName.trim().isEmpty()) {
			appName = "<unnamed>";
		}
		return appName;
	}
	
	private String targetFolder() {
		String pathName = null;
		try {
			File targetFolder = workspace.targetFolder();
			IPath location= Path.fromOSString(targetFolder.getAbsolutePath());
			IFile iFile = ResourcesPlugin.getWorkspace().getRoot().getFileForLocation(location);
			pathName = iFile.getFullPath().toOSString();
		} catch(Exception e) {
			pathName = "<unknown>";
			if (e.getMessage() == null || e.getMessage().isEmpty()) {
				pathName += " (" + e.getMessage() + ")";
			}
		}
		if (pathName == null || pathName.trim().isEmpty()) {
			pathName = "<unknown>";
		}
		return pathName;
	}

	private void startListener() {
		runtimeEnvironmentDeployment.start();
		applicationDeployment.start();
	}

	private void handleRuntimeEnvSelection() {
		if (buttonRuntimeEnvironment.getText().equals(runtimeEnvironmentStop)) {
			deploymentHandler.stop();
		} else {
			deploymentHandler.start();
		}
	}

	private void handleDeploySelection() {
		setEnable(false, buttonPurgeApplication, buttonRuntimeEnvironment);
		deploymentHandler.redeploy();
		setEnable(true, buttonPurgeApplication, buttonRuntimeEnvironment);
	}

	private void setEnable(boolean enbl, Button... btns) {
		PlatformUI.getWorkbench().getDisplay().syncExec(() -> {
			for (Button b : btns) {
				b.setEnabled(enbl);
			}
		});
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		linkApplication.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Program.launch(e.text);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonRuntimeEnvironment.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleRuntimeEnvSelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonRedeployApplication.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleDeploySelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonPurgeApplication.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handlePurgeApplicationSelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	private void setStateRuntimeEnvironmentStopped() {
		asnyc(() -> {
			buttonRuntimeEnvironment.setText(runtimeEnvironmentStart);
			labelRuntimeEnvironmentState.setImage(stopped);
			setEnable(false, buttonRedeployApplication);
			setEnable(true, buttonRuntimeEnvironment, buttonPurgeApplication);
			buttonRuntimeEnvironment.setText(runtimeEnvironmentStart);
		});
	}

	private void setStateRuntimeEnvironmentProcessing() {
		asnyc(() -> {
			labelRuntimeEnvironmentState.setImage(processing);
			setEnable(false, buttonRuntimeEnvironment, buttonPurgeApplication, buttonRedeployApplication);
		});
	}

	private void setStateRuntimeEnvrionmentRunning() {
		asnyc(() -> {
			labelRuntimeEnvironmentState.setImage(running);
			buttonRuntimeEnvironment.setText(runtimeEnvironmentStop);
			setEnable(true, buttonRuntimeEnvironment, buttonRedeployApplication);
		});
	}

	private void setStateApplicationStopped() {
		asnyc(() -> {
			labelApplicationState.setImage(stopped);
			linkApplication.setEnabled(false);
		});
	}

	private void setStateApplicationProcessing() {
		asnyc(() -> {
			labelApplicationState.setImage(processing);
			linkApplication.setEnabled(false);
		});
	}

	private void setStateApplicationRunning() {
		asnyc(() -> {
			labelApplicationState.setImage(running);
			linkApplication.setEnabled(true);
		});
	}
	
	private void updateUI() {
		updateAppNameLabel();
		updateWorkingDirLabel();
		setStateRuntimeEnvironmentStopped();
		setStateApplicationStopped();
	}

	private void updateAppNameLabel() {
		final String appName = appName();
		asnyc(() -> {
			appNameLabel.setText("App: " + appName);
		});
	}

	private void updateWorkingDirLabel() {
		final String targetFolder = targetFolder();
		asnyc(() -> {
			workingDirLabel.setText("Working Directory: " + targetFolder);
		});
	}
	
	private void asnyc(Runnable runnable) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(runnable);
	}
}
