/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.deployment;

import info.scce.dime.ui.deployment.os.OperatingSystemDetector;
import info.scce.dime.ui.deployment.view.Workspace;
import info.scce.gantry.Project;
import info.scce.gantry.impl.DockerClientFactory;
import info.scce.gantry.impl.DefaultDockerClientFactory;
import info.scce.gantry.impl.DockerProject;
import info.scce.gantry.impl.WindowsDockerClientFactory;

public class ProjectFactory {
	private final OperatingSystemDetector operatingSystemDetector;
	private final Workspace workspace;
	private final String projectName;

	public ProjectFactory(OperatingSystemDetector operatingSystemDetector, Workspace worspace, String projectName) {
		this.operatingSystemDetector = operatingSystemDetector;
		this.workspace = worspace;
		this.projectName = projectName;
	}

	public Project project() throws Exception {
		DockerClientFactory dockerClientFactory = new DefaultDockerClientFactory();
		if (operatingSystemDetector.isWindows()) {
			dockerClientFactory = new WindowsDockerClientFactory();
		}
		return new DockerProject(workspace.targetFolder(), projectName, dockerClientFactory.build());
	}
}
