/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.deployment.handler;

import de.jabc.cinco.meta.core.utils.job.ReiteratingThread;
import info.scce.dime.ui.deployment.DeploymentStateEnum;
import info.scce.gantry.Container;
import info.scce.gantry.Container.ContainerException;
import info.scce.gantry.ContainerStatus.ContainerStatusException;
import info.scce.gantry.ContainerStatusEnum;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDeployment extends ReiteratingThread {

	private boolean starting = false;
	private boolean stopping = false;
	private final List<DeploymentObserverInterface> observers = new ArrayList<>();
	private final List<Container> containerList;
	protected DeploymentStateEnum deploymentState = DeploymentStateEnum.STOPPED;

	public AbstractDeployment(List<DeploymentObserverInterface> observers, List<Container> containerList) {
		super(1000);
		this.containerList = containerList;
		this.observers.addAll(observers);
	}

	public void startContainer() {
		starting = true;
		try {
			prepareImages();
			for (Container container : containerList) {
				container.start();
			}
		} catch (ContainerException e) {
			throw new RuntimeException("We failed starting the application", e);
		}
		starting = false;
	}

	public void stopContainer() {
		stopping = true;
		try {
			for (Container container : containerList) {
				container.stop();
			}
		} catch (ContainerException e) {
			throw new RuntimeException("We failed stopping the runtime", e);
		}
		stopping = false;
	}

	public void removeContainer() {
		try {
			for (Container container : containerList) {
				container.remove();
			}
		} catch (ContainerException e) {
			throw new RuntimeException("We failed stopping the runtime", e);
		}
	}

	public void removeVolumes() {
		try {
			for (Container container : containerList) {
				container.removeProvidedVolumes();
			}
		} catch (ContainerException e) {
			throw new RuntimeException("We failed stopping the runtime", e);
		}
	}

	public void deleteObserver(DeploymentObserverInterface o) {
		observers.remove(o);
	}

	@Override
	protected void prepare() {
		deploymentState = deploymentStateEnum();
	}

	@Override
	protected void work() {
		deploymentState = deploymentStateEnum();
		update();
	}

	protected DeploymentStateEnum deploymentStateEnum() {
		try {
			if (isProcessing()) {
				return DeploymentStateEnum.PROCESSING;
			} else if (isRunning()) {
				return DeploymentStateEnum.RUNNING;
			}
			return DeploymentStateEnum.STOPPED;
		} catch (ContainerException | ContainerStatusException e) {
			throw new RuntimeException("We failed determine the deployment state", e);
		}
	}

	protected abstract void updateObserver(DeploymentObserverInterface observer);

	protected void update() {
		for (DeploymentObserverInterface observer : observers) {
			updateObserver(observer);
		}
	}

	private boolean isRunning() throws ContainerStatusException, ContainerException {
		for (Container container : containerList) {
			if (isContainerNotRunning(container)) {
				return false;
			}
		}
		return true;
	}

	private boolean isContainerNotRunning(Container container) throws ContainerException, ContainerStatusException {
		return !container.containerStatus().status().equals(ContainerStatusEnum.RUNNING);
	}

	private boolean isProcessing() {
		return starting || stopping;
	}

	private void prepareImages() throws ContainerException {
		for (Container container : containerList) {
			container.prepareImage();
		}
	}
}
