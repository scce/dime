/*-
 * #%L
 * DIME
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.dime.ui.deployment;

import java.util.Arrays;
import java.util.Collections;

import info.scce.gantry.Project;
import info.scce.gantry.Environment;

public class EnvironmentFactory {

	private final ProjectFactory projectFactory;

	public EnvironmentFactory(ProjectFactory projectFactory) {
		this.projectFactory = projectFactory;
	}

    public Environment environment() throws Exception {
	    Project project = projectFactory.project();
        return project.environment(
                project.container(
                        project.pulledImage("postgres:14.4"),
                        project.containerName("postgres"),
                        project.containerConfig(
                                Collections.emptyList(),
                                project.command(
                                        "-c", "max_connections=300",
                                        "-c", "max_prepared_transactions=200"
                                ),
                                Arrays.asList(
                                        project.environmentVariable(
                                                "POSTGRES_DB",
                                                "dywa"
                                        ),
                                        project.environmentVariable(
                                                "POSTGRES_USER",
                                                "sa"
                                        ),
                                        project.environmentVariable(
                                                "POSTGRES_PASSWORD",
                                                "sa"
                                        )
                                ),
                                Collections.emptyList(),
                                Collections.singletonList(
                                        project.portSpec(
                                                "127.0.0.1:5432",
                                                "5432"
                                        )
                                ),
                                Collections.emptyList(),
                                Collections.singletonList(
                                        project.providedVolume(
                                                "postgres",
                                                "/var/lib/postgresql/data"
                                        )
                                )
                        )
                ),
                project.container(
                        project.pulledImage("registry.gitlab.com/scce/docker-images/mailcatcher:0.7.1"),
                        project.containerName("mailcatcher"),
                        project.containerConfig(
                                Collections.emptyList(),
                                Collections.emptyList(),
                                Collections.emptyList(),
                                Collections.emptyList(),
                                Collections.singletonList(
                                        project.portSpec(
                                                "127.0.0.1:8888",
                                                "1080"
                                        )
                                ),
                                Collections.emptyList(),
                                Collections.emptyList()
                        )
                ),
                project.container(
                        project.builtImage("wildfly", "development.Dockerfile"),
                        project.containerName("wildfly"),
                        project.containerConfig(
                        		Collections.emptyList(),
                        		Collections.emptyList(),
                                Arrays.asList(
                                        project.environmentVariable(
                                                "DIME_APP_ENCRYPT_DISABLE",
                                                "false"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_ENCRYPT_PASSPHRASE",
                                                "kiiMeiPa4heidoh1"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_MAIL_PORT",
                                                "1025"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_MAIL_SERVER",
                                                "mailcatcher"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_SERVER_TIER",
                                                "local"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_SERVER_URL",
                                                "http://127.0.0.1:8080/"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_CONFIG",
                                                "{}"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_LOG_LEVEL",
                                                "DEBUG"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_AUTH_PRIVATE_KEY",
                                                "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDOyw1U7Wx9ZVwb9rVE7wuSgPCa5EUEhe7kqjq7U93yucNzhpth8svz5DgK62iUqEpOcLMO8L+UzZ5XfFHj6+NbYhSTN8LhhhCSuUg6+rXnBPURkwkgFVXVyTLmlF8e+KYCEo4WsqmFxi+wgDJ4L+kXyyO10tn0sDW0PVgm0lGO83PX7DRNufUeZe9aGIYAj2IawMGoq9AhdYHrIP9yy/+5VYJyOUpU07Ju7nudajpOGc9hb4nYYu/Y2ruHdNMZ6ctbQN75kkQ7dH/umRSVUGlN/Nv1+65/OZFuKBf4CtzbBrcUtRZ9A3FnLbyG45lAzXv10GwuL2+QpXmLIHF2PYQfAgMBAAECggEBAIChsdNINJnzuM30MmGEyJADAKd4oq3jmexHL21jB8Z6YuezrWfKHa/CIFI6iL0fycjtNZRvkNxA4uPMgyvhiXVIlz/UCbc7K4f5FMMLPKdNYBvkeGKqos/u7WiclmaviKP1YskfrW93DiceN0anlrikNbxeQhGAoodROUo8vVrLWVUAtgnN/hb8M7bmgriOrinPGzKbNjS1N4KXiUiXjB4eOQjotxIxndD52UZQh50jFvLjaCcqw7+0AaWglz5xh+h3mUvet7996dRFZ0ag80eOCbLr6vwKhAfmHCl/MkUkAeC9fOrNJYT0LoOdS70nCjaenusSJRDZwu68RXXw50ECgYEA/znktRLiNl0ZA/G16G+G424Kf9IeQEfYWk8O3sMtUBFK9OFm7OFRKZXdrdHZsITlgNw7RyytNNvyPkkbQzEDfmwNm5TNmlj6CSMgbzWD+JAwMj22xh3ll4COvO/eeIWIIxf+9Jf9ySgfpB09yCB/bpZZwp2CPqjk50qORl909m8CgYEAz2uQo9menNnTyp7d6hJ80PYaAsdaejXnlLPaFomCWPXbj4sCHcgOWMEA9v1AS86WgkDD6Mdl1XKkbvc5V3LJDBoxyGB10UuHLbKLpr408s3kd8b6jmERQoTz2KeFBpNF1ynELRIcOX8ChNPTqb/X2KmyiU1AItuG0vsPKLn6pVECgYEArzVRGjUycduLVrfSBxS45twd/Q2DkuE/Vw+6x5X5P2P/rwglniw3iXLGUZMj/BffbYzCbOPwq77qF3QccQ3uthT7anjGhFTcoPUqSO3WAQcK7xKKrIrNmCm6011fviD9CgppDgRhRnxy7DjetsoIcSRpzj5OsTFEorr93LWfF6UCgYAzHegkaSIst4X5XlOENxApkyzv2F9U1OoAfJ5XhYXpFJYKdxCLdF0MxGvPcrQguoXfDcT6HgHbq4gYjLbF9VTUtbPlFNiUPs/OlDzUV/XGjLUsS449/m/5e7h4gThIWY0RrIpbLSZliQZ+45E7OTshexizu6T9sio03ohq2gKKMQKBgQDljG/GI+HZsEZ5BqA4RDx3+WscrpEL4keMOnr7i4eoGG7bwb0jcdQjczDJh6Nn+fSZPVOJ90zb8NGpZPSktuotDETXDWRJgeWuEbs298hjeWc9nwrgCBTPaS5ZaqF8SA4Svwr2MfeEki4GtkCmBtdSGgHvOgkzovK/zRJU6WtTFw=="
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_AUTH_PUBLIC_KEY",
                                                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzssNVO1sfWVcG/a1RO8LkoDwmuRFBIXu5Ko6u1Pd8rnDc4abYfLL8+Q4CutolKhKTnCzDvC/lM2eV3xR4+vjW2IUkzfC4YYQkrlIOvq15wT1EZMJIBVV1cky5pRfHvimAhKOFrKphcYvsIAyeC/pF8sjtdLZ9LA1tD1YJtJRjvNz1+w0Tbn1HmXvWhiGAI9iGsDBqKvQIXWB6yD/csv/uVWCcjlKVNOybu57nWo6ThnPYW+J2GLv2Nq7h3TTGenLW0De+ZJEO3R/7pkUlVBpTfzb9fuufzmRbigX+Arc2wa3FLUWfQNxZy28huOZQM179dBsLi9vkKV5iyBxdj2EHwIDAQAB"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_SERVER_NAME",
                                                "postgres"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_DATABASE_NAME",
                                                "dywa"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_USER",
                                                "sa"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_PASSWORD",
                                                "sa"
                                        ),
                                        project.environmentVariable(
                                                "ALLOWED_ORIGINS",
                                                "*"
                                        ),
                                        project.environmentVariable(
                                                "JAVA_OPTS",
                                                "-Xms64m -Xmx2g -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=512m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
                                        )
                                ),
                                Arrays.asList(
                                        project.link("postgres"),
                                        project.link("mailcatcher")
                                ),
                                Collections.singletonList(
                                        project.portSpec(
                                                "127.0.0.1:8080",
                                                "8080"
                                        )
                                ),
                                Collections.emptyList(),
                                Collections.singletonList(
                                        project.providedVolume(
                                                "dywaapp",
                                                "/opt/jboss/wildfly/standalone/data/files"
                                        )
                                )
                        )
                )
       );
    }
}
